<?xml version="1.0" encoding="utf-8"?>
<!--<schema xmlns="http://www.ascc.net/xml/schematron">-->
<schema 
  xmlns="http://purl.oclc.org/dsdl/schematron"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:saxon="http://saxon.sf.net/"
  xmlns:res="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources/TEE-conf"
  queryBinding="xslt2"
  id="validationEcmResourcesFile">
  
  <title>Schematron for Flash ECM resources config file</title>
  
  <ns prefix="xsl" uri="http://www.w3.org/1999/XSL/Transform"/>
  <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
  <ns prefix="saxon" uri="http://saxon.sf.net/"/>
  <ns prefix="res" uri="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources/TEE-conf"/>
  
  <xsl:key name="getResByType" match="*[res:isRes(.)]" use="@type"/>
  
  <xsl:variable name="resourcesFiles.dir" select="resolve-uri('../../resources/', static-base-uri())"/>
  
  <xsl:variable name="teeType" select="/res:resources/@tee-type"/>
  
  <!--<phase id="deployed">
    <active pattern="unique-resourceType-constraint"/>
    <!-\-<active pattern="resource-file-exists"/>-\->
  </phase>-->
  
  <pattern id="unique-resourceType-constraint">
    <rule context="*[res:isRes(.)][@type]">
      <assert test="count(key('getResByType', current()/@type)) = 1">
        @type="<value-of select="@type"/>" must be setted only once
      </assert>
    </rule>
  </pattern>
  
  <pattern id="resource-file-exists">
    <!--when testing on the cloned git repo, we only can check files in this repo
    We have to delete "xf-ecmRes" path wich is an added dir in the jar (as ELS convention we always create a dir whose name = artefactId)-->
    <rule context="*[res:isRes(.)][starts-with(@jar-href, 'xf-ecmRes/')]">
      <xsl:variable name="fileUri" select="resolve-uri(substring-after(@jar-href, 'xf-ecmRes/'), $resourcesFiles.dir)" as="xs:anyURI"/>
      <assert test="unparsed-text-available($fileUri)">
        File <value-of select="$fileUri"/> not available
      </assert>
    </rule>
  </pattern>
  
  <pattern id="tee-type-all">
    <rule context="res:resources">
      <assert test="res:resource-xslt[@type = 'ecm.calculateMetadata']">
        resource-xslt "ecm.calculateMetadata" must be declared
      </assert>
      <assert test="res:resource-xslt[@type = 'ecm.htmlSimplePreview']">
        resource-xslt "ecm.htmlSimplePreview" must be declared
      </assert>
      <assert test="res:resource-xslt[@type = 'ecm.htmlFullPreview']">
        resource-xslt "ecm.htmlFullPreview" must be declared
      </assert>
    </rule>
  </pattern>
  
  <pattern id="tee-type-all-2">
    <rule context="res:resources[*[res:isRes(.)][starts-with(@type, 'edition')]]">
      <assert test="res:resource-schema[@type = 'edition.schema']">
        When an "edition" resource is declared, "edition.schema" must be declared
      </assert>
      <assert test="res:resource-xslt[@type = 'edition.xml2html']">
        When an "edition" resource is declared, "edition.xml2html" must be declared
      </assert>
      <assert test="res:resource-xslt[@type = 'edition.html2xml']">
        When an "edition" resource is declared, "edition.html2xml" must be declared
      </assert>
      <assert test="res:resource-css[@type = 'edition.style']">
        When an "edition" resource is declared, "edition.style" must be declared
      </assert>
    </rule>
  </pattern>
  
  
  <pattern id="tee-type-tree">
    <rule context="res:resources[@tee-type = 'tree']/*[res:isRes(.)]">
      <!--<report test="starts-with(@type, 'edition.')">
        No "edition" ressources are allowed on a TEE type "tree"
      </report>
      => Si pour éditer les contentNode-->
      <!--<report test="starts-with(@type, 'ecm.html')">
        No "html" ressources (preview, toc) are allowed on a TEE type "tree"
      </report> 
      => Si pour l'affichage dans le panneau de droite de résultats de recherche-->
      <report test="@type = 'ecm.htmlToc'">
        No "ecm.htmlToc" ressource allowed on a TEE type "tree"
      </report>
    </rule>
  </pattern>
  
  <pattern id="tee-type-text">
    <rule context="res:resources[@tee-type = 'text']">
      <assert test="res:resource-schema[@type = 'ecm.contentNode.schema']">
        resource-schema "ecm.contentNode.schema" must be declared
      </assert>
    </rule>
  </pattern>
  
  <xsl:function name="res:isRes" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:sequence select="starts-with(local-name($e), 'resource-')"/>
  </xsl:function>
  
</schema>
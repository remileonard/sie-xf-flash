<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  version="2.0">

  <xsl:import href="_common/el2html.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Génère la prévisualisation complète d'un EE de type infoCommentaire</xd:p>
      <xd:p>Les xsl:imports ci-dessus suffisent</xd:p>
    </xd:desc>
  </xd:doc>
  
</xsl:stylesheet>
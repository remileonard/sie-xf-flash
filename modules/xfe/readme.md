# Principe du projet #
 
Il s’agit d’éditer du XML en passant par du HTML. Cela permets d’utiliser TinyMCE comme éditeur (customisable).

Avant toute chose, le schema du XML (DTD ou XSD) doit être converti en schema Relax NG.

Ce schema est ensuite converti en [Relax NG simplifié](http://www.relaxng.org/spec-20011203.html#simplification), car c’est plus simple à parser (nous lui avons donné l’extension non officielle « .srng »).

**XFE contient 3 conversions :**

1. xfe-XML2HTML.xsl : converti le XML en HTML en s’appuyant sur 
     * le fichier de conf : xfe-conf.xml
     * le schéma SRNG

2. xfe-xmlSrng2HtmlRNG.xsl : converti le SRNG du XML en RNG pour le HTML (.html.rng) généré ci-dessus en s’appuyant sur :
    * xfe-conf.xml : fichier de conf 

3. xfe-HTML2XML.xsl : rétro-converti le HTML en XML en s’appuyant sur :
    * xfe-conf.xml : fichier de conf 
    * le schéma SRNG
    * une xsl spécifique
 
La conversion XML2HTML génère par défaut des div (ou span pour les éléments inline selon le schema) avec : 

  * @class="nomElementXML" 
  * chaque attribut devient @data-xml-NomAttributXML="valeurAttributXML"

Le fichier de conf sert à surcharger ce comportement, on peut lui indiquer plusieurs choses :

  * qu’un élément xml <al> (alinéa) devient un <p> en html, sans @class="al" par exemple (si c’est le seul élément xml transformé en <p> alors la rétro-conv pourra faire l’inverse sans ambiguïté)
  * qu’un élément <niveau> ne doit pas être converti en balise XML (mais son contenu oui) => dans conf : @html-name="void")
  * etc.

Le schema HTML.RNG va également s’adapter à ce qui est explicité dans le fichier de conf.
La rétro-conv également, dans la mesure du possible. 

Après la conversion HTML, le fichier html est mis à dispo du rédacteur sous TinyMCE : il peut le modifier, créé des éléments etc. Mais l’interface TinyMCE ne permets pas de modifier le HTML sans qu’il reste valide selon le schema HTML.RNG généré.
 
Après modification du HTML, la retro-conv doit permettre de régénéré un fichier XML valide.
Les éléments « void » sont parfois difficile à reconstitué : 

- nous avons mis des attributs dans le HTML pour pouvoir le faire
- Mais lorsque le rédacteur modifie le fichier, on ne peut plus réellement se base sur ces attributs

Il y a donc une partie du traitement que l’on ne peut pas rendre générique : c’est une xsl construite à la main qui détermine comment remettre les éléments void, en fonction de l’analyse du schema et de l’instance HTML récupéré : il peut y avoir des ambiguité dans ce traitements (cette xsl fait donc certains choix arbitraires mais le xml en sortie doit toujours être valide)
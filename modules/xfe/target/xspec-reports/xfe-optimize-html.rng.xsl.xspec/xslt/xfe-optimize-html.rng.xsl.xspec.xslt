<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:test="http://www.jenitennison.com/xslt/unit-test"
                xmlns:x="http://www.jenitennison.com/xslt/xspec"
                xmlns:__x="http://www.w3.org/1999/XSL/TransformAliasAlias"
                xmlns:pkg="http://expath.org/ns/pkg"
                xmlns:impl="urn:x-xspec:compile:xslt:impl"
                xmlns:rng="http://relaxng.org/ns/structure/1.0"
                xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
                version="2.0"
                exclude-result-prefixes="pkg impl">
   <xsl:import href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/xsl/xfe-optimize-html.rng.xsl"/>
   <xsl:import href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/generate-tests-utils.xsl"/>
   <xsl:namespace-alias stylesheet-prefix="__x" result-prefix="xsl"/>
   <xsl:variable name="x:stylesheet-uri"
                 as="xs:string"
                 select="'file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/xsl/xfe-optimize-html.rng.xsl'"/>
   <xsl:output name="x:report" method="xml" indent="yes"/>
   <xsl:param select="false()" name="xfe:debug"/>
   <xsl:template name="x:main">
      <xsl:message>
         <xsl:text>Testing with </xsl:text>
         <xsl:value-of select="system-property('xsl:product-name')"/>
         <xsl:text> </xsl:text>
         <xsl:value-of select="system-property('xsl:product-version')"/>
      </xsl:message>
      <xsl:result-document format="x:report">
         <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/format-xspec-report.xsl"</xsl:processing-instruction>
         <x:report stylesheet="{$x:stylesheet-uri}" date="{current-dateTime()}">
            <xsl:call-template name="x:d549e3"/>
            <xsl:call-template name="x:d549e9"/>
            <xsl:call-template name="x:d549e15"/>
            <xsl:call-template name="x:d549e18"/>
            <xsl:call-template name="x:d549e25"/>
            <xsl:call-template name="x:d549e28"/>
            <xsl:call-template name="x:d549e31"/>
            <xsl:call-template name="x:d549e34"/>
            <xsl:call-template name="x:d549e37"/>
         </x:report>
      </xsl:result-document>
   </xsl:template>
   <xsl:template name="x:d549e3">
      <xsl:message>xfe-test.html.rng &gt; xfe-test.html.optimized.rng</xsl:message>
      <x:scenario>
         <x:label>xfe-test.html.rng &gt; xfe-test.html.optimized.rng</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/_test/xfe-test.html.rng"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/_test/xfe-test.html.rng')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d549e5">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d549e5">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>xfe-test.html.optimized.rng</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/_test/xfe-test.html.optimized.rng')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>xfe-test.html.optimized.rng</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d549e9">
      <xsl:message>xf_infoCommentaire.html.rng &gt; xf_infoCommentaire.html.optimized.rng</xsl:message>
      <x:scenario>
         <x:label>xf_infoCommentaire.html.rng &gt; xf_infoCommentaire.html.optimized.rng</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/efl/infoCommentaire/xf_infoCommentaire.html.rng"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/efl/infoCommentaire/xf_infoCommentaire.html.rng')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d549e11">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d549e11">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>xf_infoCommentaire.html.optimized.rng</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/efl/infoCommentaire/xf_infoCommentaire.html.optimized.rng')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>xf_infoCommentaire.html.optimized.rng</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d549e15">
      <xsl:message>xf_REVUES.html.rng &gt; xf_REVUES.html.optimized.rng</xsl:message>
      <x:scenario>
         <x:label>xf_REVUES.html.rng &gt; xf_REVUES.html.optimized.rng</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/dalloz/revues/xf_REVUES.html.rng"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/dalloz/revues/xf_REVUES.html.rng')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d549e17">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d549e17">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>xf_REVUES.html.optimized.rng</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/dalloz/revues/xf_REVUES.html.optimized.rng')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>xf_REVUES.html.optimized.rng</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d549e18">
      <xsl:message>xf_Ouvrages_v1.html.rng &gt; xf_Ouvrages_v1.html.optimized.rng</xsl:message>
      <x:scenario>
         <x:label>xf_Ouvrages_v1.html.rng &gt; xf_Ouvrages_v1.html.optimized.rng</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/dalloz/ouvrages/xf_Ouvrages_v1.html.rng"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/dalloz/ouvrages/xf_Ouvrages_v1.html.rng')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d549e20">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d549e20">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>xf_Ouvrages_v1.html.optimized.rng</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/dalloz/ouvrages/xf_Ouvrages_v1.html.optimized.rng')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>xf_Ouvrages_v1.html.optimized.rng</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d549e25">
      <xsl:message>xf_etudered.html.rng &gt; xf_etudered.html.optimized.rng</xsl:message>
      <x:scenario>
         <x:label>xf_etudered.html.rng &gt; xf_etudered.html.optimized.rng</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/etudes/xf_etudered.html.rng"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/etudes/xf_etudered.html.rng')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d549e27">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d549e27">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>xf_etudered.html.optimized.rng</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/etudes/xf_etudered.html.optimized.rng')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>xf_etudered.html.optimized.rng</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d549e28">
      <xsl:message>xf_GB_FP.html.rng &gt; xf_GB_FP.html.optimized.rng</xsl:message>
      <x:scenario>
         <x:label>xf_GB_FP.html.rng &gt; xf_GB_FP.html.optimized.rng</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_FP.html.rng"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_FP.html.rng')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d549e30">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d549e30">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>xf_GB_FP.html.optimized.rng</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_FP.html.optimized.rng')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>xf_GB_FP.html.optimized.rng</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d549e31">
      <xsl:message>xf_GB_FPRO.html.rng &gt; xf_GB_FPRO.html.optimized.rng</xsl:message>
      <x:scenario>
         <x:label>xf_GB_FPRO.html.rng &gt; xf_GB_FPRO.html.optimized.rng</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_FPRO.html.rng"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_FPRO.html.rng')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d549e33">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d549e33">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>xf_GB_FPRO.html.optimized.rng</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_FPRO.html.optimized.rng')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>xf_GB_FPRO.html.optimized.rng</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d549e34">
      <xsl:message>xf_GB_QR.html.rng &gt; xf_GB_QR.html.optimized.rng</xsl:message>
      <x:scenario>
         <x:label>xf_GB_QR.html.rng &gt; xf_GB_QR.html.optimized.rng</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_QR.html.rng"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_QR.html.rng')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d549e36">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d549e36">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>xf_GB_QR.html.optimized.rng</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_QR.html.optimized.rng')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>xf_GB_QR.html.optimized.rng</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d549e37">
      <xsl:message>xf_GB_MT.html.rng &gt; xf_GB_MT.html.optimized.rng</xsl:message>
      <x:scenario>
         <x:label>xf_GB_MT.html.rng &gt; xf_GB_MT.html.optimized.rng</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_MT.html.rng"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_MT.html.rng')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d549e39">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d549e39">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>xf_GB_MT.html.optimized.rng</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/grammars/el/fiches/xf_GB_MT.html.optimized.rng')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>xf_GB_MT.html.optimized.rng</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
</xsl:stylesheet>

<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:test="http://www.jenitennison.com/xslt/unit-test"
                xmlns:x="http://www.jenitennison.com/xslt/xspec"
                xmlns:__x="http://www.w3.org/1999/XSL/TransformAliasAlias"
                xmlns:pkg="http://expath.org/ns/pkg"
                xmlns:impl="urn:x-xspec:compile:xslt:impl"
                xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
                version="2.0"
                exclude-result-prefixes="pkg impl">
   <xsl:import href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/xspec/_xspec.override-xslt/xfe-XML2HTML.xsl"/>
   <xsl:import href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/generate-tests-utils.xsl"/>
   <xsl:namespace-alias stylesheet-prefix="__x" result-prefix="xsl"/>
   <xsl:variable name="x:stylesheet-uri"
                 as="xs:string"
                 select="'file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/xspec/_xspec.override-xslt/xfe-XML2HTML.xsl'"/>
   <xsl:output name="x:report" method="xml" indent="yes"/>
   <xsl:param select="resolve-uri('../../main/conf/dalloz/revues/xfe-conf.xml', base-uri(/))"
              name="xfe:conf.uri"/>
   <xsl:param select="false()" name="xfe:debug"/>
   <xsl:param select="false()" name="xfe:addHTML5DOCTYPE"/>
   <xsl:param select="false()" name="xfe:addXmlModelPi"/>
   <xsl:param select="true()" name="xfe:fillHeadTitle"/>
   <xsl:template name="x:main">
      <xsl:message>
         <xsl:text>Testing with </xsl:text>
         <xsl:value-of select="system-property('xsl:product-name')"/>
         <xsl:text> </xsl:text>
         <xsl:value-of select="system-property('xsl:product-version')"/>
      </xsl:message>
      <xsl:result-document format="x:report">
         <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/format-xspec-report.xsl"</xsl:processing-instruction>
         <x:report stylesheet="{$x:stylesheet-uri}" date="{current-dateTime()}">
            <xsl:call-template name="x:d2710314e8"/>
         </x:report>
      </xsl:result-document>
   </xsl:template>
   <xsl:template name="x:d2710314e8">
      <xsl:message>DALLOZ &gt; revues :</xsl:message>
      <x:scenario>
         <x:label>DALLOZ &gt; revues : </x:label>
         <xsl:call-template name="x:d2710314e9"/>
         <xsl:call-template name="x:d2710314e12"/>
         <xsl:call-template name="x:d2710314e15"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2710314e9">
      <xsl:message>..sample.xml &gt; sample.html</xsl:message>
      <x:scenario>
         <x:label>sample.xml &gt; sample.html</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/dalloz/revues/sample.xml"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/dalloz/revues/sample.xml')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2710314e11">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2710314e11">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>sample.html</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/dalloz/revues/sample.html')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>sample.html</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2710314e12">
      <xsl:message>..572-Dossier_art._1_AHuber.xml &gt; 572-Dossier_art._1_AHuber.html</xsl:message>
      <x:scenario>
         <x:label>572-Dossier_art._1_AHuber.xml &gt; 572-Dossier_art._1_AHuber.html</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/dalloz/revues/572-Dossier_art._1_AHuber.xml"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/dalloz/revues/572-Dossier_art._1_AHuber.xml')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2710314e14">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2710314e14">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>572-Dossier_art._1_AHuber.html</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/dalloz/revues/572-Dossier_art._1_AHuber.html')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>572-Dossier_art._1_AHuber.html</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2710314e15">
      <xsl:message>..603-Infractions-Exces_de_vistesse_JP_Cere.xml &gt; 603-Infractions-Exces_de_vistesse_JP_Cere.html</xsl:message>
      <x:scenario>
         <x:label>603-Infractions-Exces_de_vistesse_JP_Cere.xml &gt; 603-Infractions-Exces_de_vistesse_JP_Cere.html</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/dalloz/revues/603-Infractions-Exces_de_vistesse_JP_Cere.xml"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/dalloz/revues/603-Infractions-Exces_de_vistesse_JP_Cere.xml')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2710314e17">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2710314e17">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>603-Infractions-Exces_de_vistesse_JP_Cere.html</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/dalloz/revues/603-Infractions-Exces_de_vistesse_JP_Cere.html')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>603-Infractions-Exces_de_vistesse_JP_Cere.html</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
</xsl:stylesheet>

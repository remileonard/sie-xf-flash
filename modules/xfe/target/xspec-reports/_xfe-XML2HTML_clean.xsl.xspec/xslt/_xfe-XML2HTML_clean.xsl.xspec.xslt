<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:test="http://www.jenitennison.com/xslt/unit-test"
                xmlns:x="http://www.jenitennison.com/xslt/xspec"
                xmlns:__x="http://www.w3.org/1999/XSL/TransformAliasAlias"
                xmlns:pkg="http://expath.org/ns/pkg"
                xmlns:impl="urn:x-xspec:compile:xslt:impl"
                xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
                version="2.0"
                exclude-result-prefixes="pkg impl">
   <xsl:import href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/xsl/xfe-HTML2XML_clean.xsl"/>
   <xsl:import href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/generate-tests-utils.xsl"/>
   <xsl:namespace-alias stylesheet-prefix="__x" result-prefix="xsl"/>
   <xsl:variable name="x:stylesheet-uri"
                 as="xs:string"
                 select="'file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/main/xsl/xfe-HTML2XML_clean.xsl'"/>
   <xsl:output name="x:report" method="xml" indent="yes"/>
   <xsl:template name="x:main">
      <xsl:message>
         <xsl:text>Testing with </xsl:text>
         <xsl:value-of select="system-property('xsl:product-name')"/>
         <xsl:text> </xsl:text>
         <xsl:value-of select="system-property('xsl:product-version')"/>
      </xsl:message>
      <xsl:result-document format="x:report">
         <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/format-xspec-report.xsl"</xsl:processing-instruction>
         <x:report stylesheet="{$x:stylesheet-uri}" date="{current-dateTime()}">
            <xsl:call-template name="x:d2718487e2"/>
         </x:report>
      </xsl:result-document>
   </xsl:template>
   <xsl:template name="x:d2718487e2">
      <xsl:message>Clean HTML TinyMCE</xsl:message>
      <x:scenario>
         <x:label>Clean HTML TinyMCE</x:label>
         <x:context>
            <html xmlns="http://www.w3.org/1999/xhtml">
               <body>
                  <div class="titre ms-selectedByNav"
                       data-xfe-label="Titre"
                       data-idbranche-ms="0">
                     <xsl:text>Text </xsl:text>
                     <em>
                        <xsl:text>ital</xsl:text>
                     </em>
                     <a class="toto">
                        <xsl:text>link</xsl:text>
                     </a>
                  </div>
                  <div class="titre    ms-selectedByNav" data-idbranche-mstoto="0"/>
                  <div class="ms-selectedByNav titre" data-ms="0"/>
               </body>
            </html>
         </x:context>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc" as="document-node()">
               <xsl:document>
                  <html xmlns="http://www.w3.org/1999/xhtml">
                     <body>
                        <div class="titre ms-selectedByNav"
                             data-xfe-label="Titre"
                             data-idbranche-ms="0">
                           <xsl:text>Text </xsl:text>
                           <em>
                              <xsl:text>ital</xsl:text>
                           </em>
                           <a class="toto">
                              <xsl:text>link</xsl:text>
                           </a>
                        </div>
                        <div class="titre    ms-selectedByNav" data-idbranche-mstoto="0"/>
                        <div class="ms-selectedByNav titre" data-ms="0"/>
                     </body>
                  </html>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="impl:context" select="$impl:context-doc/node()"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2718487e14">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2718487e14">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>OK</xsl:message>
      <xsl:variable name="impl:expected-doc" as="document-node()">
         <xsl:document>
            <html xmlns="http://www.w3.org/1999/xhtml">
               <body>
                  <div class="titre" data-xfe-label="Titre">
                     <xsl:text>Text </xsl:text>
                     <em>
                        <xsl:text>ital</xsl:text>
                     </em>
                     <a class="toto">
                        <xsl:text>link</xsl:text>
                     </a>
                  </div>
                  <div class="titre" data-idbranche-mstoto="0"/>
                  <div class="titre" data-ms="0"/>
               </body>
            </html>
         </xsl:document>
      </xsl:variable>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/node()"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>OK</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
</xsl:stylesheet>

<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:test="http://www.jenitennison.com/xslt/unit-test"
                xmlns:x="http://www.jenitennison.com/xslt/xspec"
                xmlns:__x="http://www.w3.org/1999/XSL/TransformAliasAlias"
                xmlns:pkg="http://expath.org/ns/pkg"
                xmlns:impl="urn:x-xspec:compile:xslt:impl"
                xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
                version="2.0"
                exclude-result-prefixes="pkg impl">
   <xsl:import href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/xspec/_xspec.override-xslt/xfe-XML2HTML.xsl"/>
   <xsl:import href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/generate-tests-utils.xsl"/>
   <xsl:namespace-alias stylesheet-prefix="__x" result-prefix="xsl"/>
   <xsl:variable name="x:stylesheet-uri"
                 as="xs:string"
                 select="'file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/xspec/_xspec.override-xslt/xfe-XML2HTML.xsl'"/>
   <xsl:output name="x:report" method="xml" indent="yes"/>
   <xsl:param select="resolve-uri('../../main/conf/_test/xfe-conf.xml', base-uri(/))"
              name="xfe:conf.uri"/>
   <xsl:param select="false()" name="xfe:debug"/>
   <xsl:param select="false()" name="xfe:addHTML5DOCTYPE"/>
   <xsl:param select="false()" name="xfe:addXmlModelPi"/>
   <xsl:param select="true()" name="xfe:fillHeadTitle"/>
   <xsl:template name="x:main">
      <xsl:message>
         <xsl:text>Testing with </xsl:text>
         <xsl:value-of select="system-property('xsl:product-name')"/>
         <xsl:text> </xsl:text>
         <xsl:value-of select="system-property('xsl:product-version')"/>
      </xsl:message>
      <xsl:result-document format="x:report">
         <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/format-xspec-report.xsl"</xsl:processing-instruction>
         <x:report stylesheet="{$x:stylesheet-uri}" date="{current-dateTime()}">
            <xsl:call-template name="x:d2718425e8"/>
         </x:report>
      </xsl:result-document>
   </xsl:template>
   <xsl:template name="x:d2718425e8">
      <xsl:message>XFE &gt; xfe-test :</xsl:message>
      <x:scenario>
         <x:label>XFE &gt; xfe-test : </x:label>
         <xsl:call-template name="x:d2718425e9"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2718425e9">
      <xsl:message>..xfe-test.xml &gt; xfe-test.html</xsl:message>
      <x:scenario>
         <x:label>xfe-test.xml &gt; xfe-test.html</x:label>
         <x:context href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/_test/xfe-test.xml"/>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc"
                          as="document-node()"
                          select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/_test/xfe-test.xml')"/>
            <xsl:variable name="impl:context" select="$impl:context-doc"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2718425e11">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2718425e11">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>xfe-test.html</xsl:message>
      <xsl:variable name="impl:expected-doc"
                    as="document-node()"
                    select="doc('file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xfe/src/test/samples/_test/xfe-test.html')"/>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/(/*)"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>xfe-test.html</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <xsl:import href="../../main/xsl/xfe-xmlSrng2HtmlRNG.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Permets de faciliter les test xspec</xd:p>
    </xd:desc>
  </xd:doc>
  
  <xsl:template match="/">
    <xsl:variable name="step1" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="." mode="xfe:snrgXML2rngHtml"/>
      </xsl:document>
    </xsl:variable>
    <xsl:apply-templates select="$step1" mode="adaptForXspec"/>
  </xsl:template>
  
  <xsl:template match="/*" mode="adaptForXspec">
    <xsl:copy copy-namespaces="yes">
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="adaptForXspec">
    <xsl:copy copy-namespaces="no">
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
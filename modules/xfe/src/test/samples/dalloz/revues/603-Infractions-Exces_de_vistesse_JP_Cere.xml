<?xml version="1.0" encoding="utf-8"?>
<!--<!DOCTYPE ARTICLE PUBLIC "DALLOZ//DTD REVUES//FR" "file:/C:/DTD Dalloz/REVUES/REVUES.DTD">-->
<?xml-model href="../../../../main/grammars/dalloz/revues/xf_REVUES.srng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<ARTICLE xmlns="http://www.lefebvre-sarrut.eu/ns/dalloz/revues"
	       ID="AJPEN/JURIS/2015/0446" TYPE="JURIS" TYPE2="Arrêt" ETAT="PREPA">
	<METAS>
		<!--<RENVOIS>
			<RENV-JURIS>
				<LIENJUR TYPE="Jurisp">
					<JURIDIC>
						<JURI>Cour de cassation</JURI>
						<CHAM>crim.</CHAM>
					</JURIDIC>
					<DATE>13-05-2015</DATE>
					<NAFF>14-83.559</NAFF>
				</LIENJUR>
			</RENV-JURIS>
		</RENVOIS>-->
		<COMPO>Article suivant</COMPO>
	</METAS>
	<INDEX>
		<NOME>
			<MOT>CIRCULATION ROUTIERE</MOT>
			<MOCL>
				<MOT>Vitesse</MOT>
				<MOCL>
					<MOT>Contrôle</MOT>
					<MOCL>
						<MOT>Radars automatiques</MOT>
						<MOCL>
							<MOT>Lieu de l'infraction</MOT>
						</MOCL>
					</MOCL>
				</MOCL>
			</MOCL>
		</NOME>
	</INDEX>
	<PARTIE>
		<TITRE-PART>Jurisprudence</TITRE-PART>
		<SPARTIE>Jurisprudence</SPARTIE>
	</PARTIE>
	<RUBRIQUE>
		<TITRE-RUBR>Infractions</TITRE-RUBR>
		<SRUBR>Excès de vitesse</SRUBR>
	</RUBRIQUE>
	<INTITULE>
		<TITRE-ART>Radars tronçon : le lieu de commission d'un excès de vitesse est celui où se trouve implanté l'appareil de sortie du tronçon</TITRE-ART>
	</INTITULE>
	<SIGNATURES>
		<SIGNATURE>
			<AUTEUR ID="CERE_JEAN_PAUL">
				<PRENOM>Jean-Paul</PRENOM>
				<NOM>Céré</NOM>
			</AUTEUR>
			<QUALITE>Maître de conférences à l'université de Pau et des pays de l'Adour, président de l'Association française de droit pénal</QUALITE>
		</SIGNATURE>
	</SIGNATURES>
	<DECISION ID="AJPEN/JURIS/2015/0447" TYPE="Arrêt">
		<PARADECI><JURIDIC><JURI>Cour de cassation</JURI><CHAM>crim.</CHAM></JURIDIC>, <DATE>13 mai 2015</DATE>, n<EXP>o</EXP>
			<NAFF>14-83.559</NAFF></PARADECI>
		<PARADECI>
			<FONDLEG>
				<CODES>
					<CODE>
						<TITRCODE>Code de la route</TITRCODE>
						<ARTICLES>
							<NOART>L. 130-9</NOART>
						</ARTICLES>
					</CODE>
				</CODES>
			</FONDLEG>
		</PARADECI>
		<SOMMAIRE>
			<P>Le titulaire du certificat d'immatriculation d'un véhicule est déclaré responsable pécuniairement de l'amende encourue pour un excès de vitesse constaté par un « radar tronçon ». Il conteste notamment l'imprécision du lieu exact de commission de l'infraction sur le procès-verbal, ce dernier ne comprenant qu'une mention « PK 358+800 ». Selon le requérant, cette indication ne lui permettait pas de connaître l'endroit exact où l'infraction avait été commise.</P>
		</SOMMAIRE>
		<INTEGRAL>
			<P>« Attendu qu'en l'état de ces motifs, la juridiction a fait l'exacte application, d'une part, des dispositions de l'article L. 130-9, dernier alinéa, du code de la route, dès lors que, lorsque l'excès de vitesse est constaté par le relevé d'une vitesse moyenne, entre deux points d'une voie de circulation, supérieure à la vitesse maximale autorisée entre ces deux points, le lieu de commission de l'infraction est celui où a été réalisée la seconde constatation et, d'autre part, de celles de l'article L. 121-3 du code de la route, dès lors que le titulaire du certificat d'immatriculation du véhicule, qui ne soutenait pas l'existence d'un vol ou d'un événement de force majeure, n'apportait pas les éléments permettant d'établir qu'il n'était pas l'auteur véritable de l'infraction ».</P>
		</INTEGRAL>
	</DECISION>
	<CORPS>
		<DEVLPMT>
			<P>À l'heure du développement de nouvelles générations de radars, l'arrêt commenté apporte une importante précision sur les indications devant figurer sur un procès-verbal de constatation d'un excès de vitesse sur une voie de circulation où un radar tronçon a été installé. Ce type de radar automatique présente la caractéristique de contrôler la vitesse de circulation moyenne entre deux points. Dans cette affaire, le procès-verbal comprenait une mention PK 358+800 qui correspondait précisément à l'endroit où se trouvait implanté l'appareil de sortie du tronçon.</P>
			<P>Il est clair qu'un procès-verbal d'infraction, en matière d'excès de vitesse, ne doit pas contenir d'imprécisions manifestes, qui seraient de nature à empêcher le juge de vérifier les conditions d'emploi du cinémomètre. Ceci ne manquerait pas de porter atteinte aux droits de la défense du prévenu. Le procès-verbal doit donc concrètement indiquer le lieu de l'infraction, c'est-à-dire mentionner, notamment, le point kilométrique ou le lieu précis de l'infraction. La localisation d'un point précis est déterminée au mètre près depuis 1924 pour l'ensemble du réseau routier français par rapport au point kilométrique zéro situé sur le parvis de Notre-Dame de Paris. En somme, il faut que le juge puisse, au regard des énonciations du procès-verbal, s'assurer de la détermination du lieu de matérialisation de l'infraction, ce qui permet de vérifier si la vitesse autorisée, à cet endroit, est, ou non, dépassée. Ainsi, cette condition n'est pas remplie et la relaxe doit donc intervenir quand le procès-verbal indique « quai de Bercy » sans autre précision (T. pol. Paris, 2 juin 2003) ; « tunnel des Cévennes » alors que celui n'est pas répertorié sur le plan de Paris (Paris, 27 nov. 2003) ; voie Georges-Pompidou dans le VIII<EXP>e</EXP> arrondissement alors qu'il est établi que l'infraction a été commise quai de Bercy dans le XII<EXP>e</EXP> arrondissement (Paris, 14 mars 2002) ; ou <ITAL>a fortiori</ITAL> si l'ensemble des rubriques du procès-verbal ne sont pas renseignées (Amiens, 13 oct. 2006).</P>
			<P>Dès lors néanmoins, que l'assurance de la vitesse maximale autorisée existe, il n'est même pas nécessaire de d'indiquer le point kilométrique. Ainsi lorsque le procès-verbal mentionne uniquement la voie où a été effectué le contrôle (boulevard) sans mention exact du lieu, l'infraction est réalisée dans la mesure où la voie visée se situe intégralement en zone urbaine, avec une limitation de vitesse à 50km/h prévue par l'article 413-3, alinéa 1, du code de la route (Crim. 27 nov. 2012).</P>
			<P>Ici, le procès-verbal désignait précisément l'endroit où se trouvait implanté l'appareil de contrôle de sortie de tronçon sur lequel s'était appliqué le contrôle. Il paraît logique de retenir cette solution car ce n'est qu'une fois dépassé le second point de contrôle que l'on est en mesure de savoir si la vitesse a été ou non dépassée sur la zone comprise entre les deux radars.</P>
			<P STYLE="encadre">
				<GRAS>À retenir</GRAS>
			</P>
			<P STYLE="encadre">Le lieu de commission d'un excès de vitesse est celui où se trouve implanté l'appareil de sortie du tronçon de contrôle.</P>
			<P STYLE="plusloin">
				<GRAS>Pour aller plus loin</GRAS>
			</P>
			<P STYLE="plusloin"><GRAS>Jurisprudence</GRAS> : T. Pol. Paris, 2 juin 2003, Gaz. Pal. 2007, n<EXP>o</EXP> 76, p. 12 ; CA Paris, 27 nov. 2003, Gaz. Pal. 2007, n<EXP>o</EXP> 76, p. 11 ; CA Paris, 14 mars 2002, Gaz. Pal. 2007, n<EXP>o</EXP> 76, p. 11 ; CA Amiens, 13 oct. 2006, Gaz. Pal. 2007, n<EXP>o</EXP> 76, p. 10 ; Crim. 27 nov. 2012, n<EXP>o</EXP> 12-80.627. – <GRAS>Doctrine</GRAS> : Rép. pénal, <ITAL>V<EXP>o</EXP></ITAL> Vitesse, par J.-P. Céré.</P>
		</DEVLPMT>
	</CORPS>
</ARTICLE>
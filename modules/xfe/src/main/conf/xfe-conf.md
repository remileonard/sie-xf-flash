## XFE Configuration File

This configuration file is used for each of the following XSLT :
- xfe-XML2HTML.xsl
- xfe-xmlSrng2HtmlRng.xsl
- xfe-HTML2XSL.xsl

### Schema

The schema is located at : src/main/grammars/xfe.

It contains a rng schema and a schematron, simply add those xml-model processing-instruction at the beginning of your xfe-conf.xml file :

```xml
<?xml-model href="../../../../grammars/xfe/xfe-conf.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="../../../../grammars/xfe/xfe-conf.sch" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
```

### Inline and block elements

If your schema defines an element that can be inline of block according to the context, then the html.rng schema generated will not be valid (the name of the element will be "spanOrDiv?").
You have to remove this ambiguity !
- First, check if you can change the schema, because it's not a good thing  to have this.
- If you cannot change the schema, then you have to define the differents contexts and declare a conf element for each :
For example :

```xml
<element xpath="foo/bar" html-name="span"/>
<element xpath="bar" html-name="div"/>
```
**TIP !**

 You can use xquery to get every inline context :
```xml
declare default element namespace "http://relaxng.org/ns/structure/1.0";
{
let $doc.uri := 'xf_GB_MT.srng'
let $define.name := 'Commentaire'
let $items := doc($doc.uri)//define[.//ref[@name=$define.name]][.//text[not(ancestor::attribute)]]
for $item in $items return
<element xpath="{distinct-values($item/@name)}/{$define.name}" html-name="span"/>
}
```

Or this one for inline and block context :
```xml
declare default element namespace "http://relaxng.org/ns/structure/1.0";
<out>
{
let $doc.uri := 'xf_GB_MT.srng'
let $define.name := 'HIDE'
let $refs := doc($doc.uri)//define[.//ref[@name=$define.name]]
for $ref in $refs return
<element xpath="{distinct-values($ref/@name)}/{$define.name}" html-name="{if ($ref//text[not(ancestor::attribute)]) then ('span') else ('div')}"/>
}
</out>
```

<?xml version="1.0" encoding="utf-8"?>
<!--<schema xmlns="http://www.ascc.net/xml/schematron">-->
<schema xmlns="http://purl.oclc.org/dsdl/schematron"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:saxon="http://saxon.sf.net/"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xml:lang="en"
  queryBinding="xslt2"
  id="validate-xfe-conf">
  
  <title>Schematron for XMLfirst Editor's conf file</title>
  
  <ns prefix="xsl" uri="http://www.w3.org/1999/XSL/Transform"/>
  <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
  <ns prefix="saxon" uri="http://saxon.sf.net/"/>
  <ns prefix="html" uri="http://www.w3.org/1999/xhtml"/>
  <ns prefix="xf" uri="http://www.lefebvre-sarrut.eu/ns/xmlfirst"/>
  
  <xsl:key name="getElementById" match="*[@id]" use="@id"/>
  <xsl:key name="getElementByXfId" match="*[@xf:id]" use="@xf:id"/>
  <xsl:key name="getElementByXfIdRef" match="*[@xf:idRef]" use="@xf:idRef"/>
  
  <xsl:variable name="NCName.reg" select="'[\i-[:]][\c-[:]]*'"/><!--(cf. xsd de xsd)-->
  
  <!--=============================================================================-->
  <!--MAIN-->
  <!--=============================================================================-->
  
  <pattern id="common">
    <rule context="element[@html-xmlNameAsClass = 'false']">
      <let name="html-name" value="@html-name"/>
      <assert test="count(preceding-sibling::element[@html-name = $html-name]) = 0">
        when @html-xmlNameAsClass="false" the element's html-name="<value-of select="$html-name"/>" must be unique within the conf
      </assert>
    </rule>
  </pattern>
  
  <pattern id="common_2">
    <rule context="element">
      <report test="@html-name = ('ins', 'del')">
        <value-of select="@html-name"/> are reserved for annotation, don't use them in the conf.
      </report>
      <xsl:variable name="xpath.reg">^(/?<xsl:value-of select="$NCName.reg"/>)+$</xsl:variable>
      <assert test="matches(@xpath, $xpath.reg)">
        @xpath must be formated as : "a/b/c" or "/root/a/b/c" ou "c", etc. (FIXME : it should be named "xml-path" ?)
      </assert>
    </rule>
  </pattern>
  
  <pattern id="xpath">
    <rule context="element[ tokenize(@xpath, '/')[last()] = preceding-sibling::element/tokenize(@xpath, '/')[last()] ]">
      <assert test="count(tokenize(., '/')) le 2">
        When multiple conf elements matching the same xml element, the @xpath implementation is limited to the parent
      </assert>
    </rule>
  </pattern>
  
  <pattern id="xpath-filter">
    <rule context="element/@xpath-filter">
      <report test="matches(., '@[\i-[:]][\c-[:]]+\s*!=')" role="warning">
        Using @x != 'value' within xpath-filter will work only if the @x is present : use (@x, '')[1] != 'value' instead
      </report>
    </rule>
  </pattern>
  
  <pattern id="normalize-conf">
    <rule context="element[starts-with(@type, 'xfe:title-')]">
      <report test="contains(@html-addClass, 'xfe_title-')">
        element type xfe:noteRef must not have a value containing "xfe_title-xx" in html-addClass attribute, it will be calculated during normalization
      </report>
    </rule>
    <rule context="element[starts-with(@type, 'xfe:noteRef(')]">
      <assert test="(@html-name, 'a')[1] = 'a'">
        element type xfe:noteRef must not have a html-name attribute, or equal to "a"
      </assert>
      <report test="@html-attribute-href">
        element type xfe:noteRef must not have a html-attribute-href attribute, it will be calculated during normalization
      </report>
      <report test="contains(@html-addClass, 'xfe_noteRef')">
        element type xfe:noteRef must not have a value containing "xfe_notRef" in html-addClass attribute, it will be calculated during normalization
      </report>
    </rule>
    <rule context="element[@type = 'xfe:inlineNote']">
      <assert test="(@html-name, 'a')[1] = 'a'">
        element type xfe:inlineNote must not have a html-name attribute, or equal to "a"
      </assert>
      <report test="@html-attribute-href">
        element type xfe:inlineNote must not have a html-attribute-href attribute, it will be calculated during normalization
      </report>
      <report test="contains(@html-addClass, 'xfe_noteRef')">
        element type xfe:inlineNote must not have a value containing "xfe_notRef" in html-addClass attribute, it will be calculated during normalization
      </report>
    </rule>
    
  </pattern>
  
</schema>
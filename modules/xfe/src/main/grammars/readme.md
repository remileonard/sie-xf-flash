## Génération des fichiers `xf_[modele].srng`

NB : Remplacez [modele] par le nom de votre modèle.

1. Récupérer le fichier RNG pour XMLfirst d'origine&nbsp;: 

    `SIE_XML-MODELS\SIE-[PUBLISHER]-MODELS\src\main\grammars\[...]\xmlFirst\rng\xf_[modele].rng`

2. Lancer une conversion SRNG avec jing (avec redirection de la sortie sdtout dans un fichier)&nbsp;: 

    `java -jar jing.jar -s xf_[modele].rng > xf_[modele].srng`

3. Sous Windows, rajouter les caractères non ASCII perdus lors de la redirection&nbsp;:

 - xf_infoCommentaire.srng : 
	 - Remplacer "qualit" par "qualité"
	 - Remplacer "astrisme" par "astérisme"
 - autres ...

4. Appliquer le mode xslt "rng:reorder" sur le fichier, (pour faciliter les diff ultérieurs) :
  - Dans oXygen, créer un scenario global "SIE-XSL-LIB : rng-common.xsl > rng:reorder" de transformation XSLT avec :
	  - XSLT : `file:/[pathTo]/SIE-LIB-XSL-COMMON/src/main/xsl/rng-common.xsl`
	  - Transfromer : `Saxon-EE`
		  - Mode (-im) : `{http://relaxng.org/ns/structure/1.0}reorder`
	  - Output : `${cfn}.reordered.srng`

5. Remplacer le contenu de `xf_[modele].srng` par celui de `xf_[modele].reordered.srng`
 
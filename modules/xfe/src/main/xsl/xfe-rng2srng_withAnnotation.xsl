<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:rng="http://relaxng.org/ns/structure/1.0"
  xmlns:rng2srng="http://relaxng.org/ns/rng2srng"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
  xmlns="http://relaxng.org/ns/structure/1.0"
  xpath-default-namespace="http://relaxng.org/ns/structure/1.0"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <xsl:import href="xslLib:/xslLib/rng2srng.xsl"/>
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  <xsl:import href="xslLib:/xslLib/rng-common.xsl"/>


  <!--
    /!\ FIXME : À FINIR
      - il y a des group, des optional, qui semble suppimée par jing ou pas etc. le diff n'est pas bon
  -->

  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>XSL qui appele rng2srng.xsl en :</xd:p>
      <xd:ul>
        <xd:li>Conservant les annotation et les PI</xd:li>
        <xd:li>Formatage du SRNG proche de ce que fait jing</xd:li>
      </xd:ul>
    </xd:desc>
  </xd:doc>
  
  <xsl:variable name="log.uri" select="resolve-uri('log/', base-uri(/*))" as="xs:anyURI"/>
  
  <!-- ******************************************************************* -->
  <!--                           MAIN                                      -->
  <!-- ******************************************************************* -->
  
  <xsl:template match="/">
    <xsl:variable name="rng2srng" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="." mode="rng2srng:main"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="rng2srng.log.uri" select="resolve-uri(concat(els:getFileName(base-uri()), '.srng'), $log.uri)" as="xs:anyURI"/>
    <xsl:if test="$xfe:debug">
      <xsl:message>[INFO] writing <xsl:value-of select="$rng2srng.log.uri"/></xsl:message>
      <xsl:result-document href="{$rng2srng.log.uri}">
        <xsl:sequence select="$rng2srng"/>
      </xsl:result-document>
    </xsl:if>
    <xsl:variable name="srngAdapted.log.uri" select="resolve-uri(concat(els:getFileName(base-uri()), 'adapted.srng'), $log.uri)" as="xs:anyURI"/>
    <xsl:variable name="srngAdapted" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$rng2srng" mode="xfe:adaptSrng"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:message>[INFO] writing <xsl:value-of select="$srngAdapted.log.uri"/></xsl:message>
      <xsl:result-document href="{$srngAdapted.log.uri}">
        <xsl:sequence select="$srngAdapted"/>
      </xsl:result-document>
    </xsl:if>
    <xsl:apply-templates select="$srngAdapted" mode="rng:reorder">
      <xsl:with-param name="rng:reorder_addRootNamespaces" as="node()*">
        <xsl:namespace name="xfe">http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor</xsl:namespace>
        <xsl:namespace name="rng">http://relaxng.org/ns/structure/1.0</xsl:namespace>
        <xsl:namespace name="rng2srng">http://relaxng.org/ns/rng2srng</xsl:namespace>
      </xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>
  
  <!-- === OVERRIDE RNG2SRNG TO KEEP ANNOTATIONS AND PI === -->
  
  <xsl:template match="processing-instruction() | @xfe:*" 
    mode="rng2srng:step7.2 rng2srng:step7.3 rng2srng:step7.4 rng2srng:step7.5 rng2srng:step7.6 rng2srng:step7.7 rng2srng:step7.8 rng2srng:step7.9 
          rng2srng:step7.10 rng2srng:step7.11 rng2srng:step7.12 rng2srng:step7.13 rng2srng:step7.14 rng2srng:step7.15 rng2srng:step7.16 rng2srng:step7.17 rng2srng:step7.18 rng2srng:step7.19 
          rng2srng:step7.20 rng2srng:step7.21 rng2srng:step7.22">
    <xsl:copy/>
  </xsl:template>
  
  <!-- === DO IT LINK JING SRNG === -->
  
  <!--bypass the steps 7.15 and 7.16 that replace optional an choice by complex structure with empty-->
  
  <xsl:template mode="rng2srng:step7.15" match="optional">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template mode="rng2srng:step7.16" match="zeroOrMore">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template mode="rng2srng:step7.13" match="*">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- ******************************************************************* -->
  <!--                    MODE xfe:adaptSrng                               -->
  <!-- ******************************************************************* -->
  
  <xsl:template match="/*" mode="xfe:adaptSrng">
    <xsl:copy copy-namespaces="yes">
      <xsl:attribute name="datatypeLibrary" select="'http://www.w3.org/2001/XMLSchema-datatypes'"/>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--On estime qu'on utilise toujours le datatypeLibrary http://www.w3.org/2001/XMLSchema-datatypes-->
  <xsl:template match="@datatypeLibrary" mode="xfe:adaptSrng"/>
  
  <xsl:template match="@ns[normalize-space(.) = '']" mode="xfe:adaptSrng"/>
  
  <xsl:template match="element[name]" mode="xfe:adaptSrng">
    <xsl:copy copy-namespaces="no">
      <!--name become an attribute of the element-->
      <xsl:attribute name="name" select="name"/>
      <!--copy attributes of name on element--> 
      <xsl:for-each select="name/@*">
        <xsl:apply-templates mode="#current"/>
      </xsl:for-each>
      <xsl:apply-templates select="@*" mode="#current"/>
      <!-- DO IT LINK JING SRNG -->
      <group>
        <xsl:apply-templates select="node()" mode="#current"/>
      </group>
    </xsl:copy>
  </xsl:template>
  
  <!-- DO IT LINK JING SRNG -->
  <!--<xsl:template match="group[count(*) = 1][interleave]" mode="xfe:adaptSrng">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>-->
  
  <xsl:template match="group[count(*) = 1]" mode="xfe:adaptSrng">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="group[not(parent::choice)]" mode="xfe:adaptSrng" priority="1">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="optional[count(rng:*) = count(optional)]" mode="xfe:adaptSrng" priority="1">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="element/name" mode="xfe:adaptSrng"/>
  
  <xsl:template match="attribute[name]" mode="xfe:adaptSrng">
    <xsl:copy copy-namespaces="no">
      <!--name become an attribute of the element-->
      <xsl:attribute name="name" select="name"/>
      <!--copy attributes of name on element--> 
      <xsl:for-each select="name/@*">
        <xsl:apply-templates mode="#current"/>
      </xsl:for-each>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="attribute/name" mode="xfe:adaptSrng"/>
  
<!--  <xsl:mode name="xfe:adaptSrng" on-no-match="shallow-copy"/>-->
  <xsl:template match="." mode="xfe:adaptSrng">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:apply-templates select="node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <!--TMP pour diff-->
  <!--<xsl:template match="@ns" mode="xfe:adaptSrng"/>
  <xsl:template match="processing-instruction()" mode="xfe:adaptSrng"/>
  <xsl:template match="@xfe:*" mode="xfe:adaptSrng"/>-->
  
</xsl:stylesheet>
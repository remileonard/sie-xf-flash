=== XMLfirst Editor ===

Ce projet permet de : 
- [xfe-XML2HTML.xsl] convertir 1 XML en HTML de mani�re g�n�rique (div ou span si inline) avec surcharge dans fichier de conf
- [xfe-xmlSrng2HtmlRNG.xsl] g�n�rer un mod�le RNG pour le HTML : mod�le d'�dition qui garantie qu'on pourra repasser en XML
- [xfe-HTML2XML.xsl] (r�tro)convertir le HTML g�n�r� en XML valide

Le HTML a vocation a �tre �dit� dans tinyMCE (POC en cours avec MaeSpirit) en suivant les r�gles du mod�le g�n�r�

Pour faire tourner ces programmes, il faut une version Relax NG du mod�le d'origine.
- si XSD => utiliser MSV (+ fix avec util/fixMsvErrorWhenConvertingXSD2RNG.xsl)
- sinon => utiliser trang 

Dans la conf, on peut d�finir des �l�ments dont le html-name est "void" ce qui signifie "� exclure".
De ce fait il est quasi impossible de r�tro-convertir le html en xml automatiquement, 
notamment quand les attributs voidAncestor ajout� au HTML ne sont pas pr�sent (ce sera le cas lors de l'�dition tinyMCE)
D'o� la mise en place de modules sp�cifiques, par exemple modules/uiCommentaire.xsl
Pour analyser les impacts et les ambiguit� cr�� par la suppression des ces �l�ments : utiliser util/expandSRNGvoidElements.xsl
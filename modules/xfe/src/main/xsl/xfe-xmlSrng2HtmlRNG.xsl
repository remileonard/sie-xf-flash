<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://relaxng.org/ns/structure/1.0"
  xmlns:saxon="http://saxon.sf.net/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:rng="http://relaxng.org/ns/structure/1.0"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
  xmlns:snrgXML2rngHtml="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor/snrgXML2rngHtml"
  xpath-default-namespace="http://relaxng.org/ns/structure/1.0"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <xsl:import href="xfe:/xsl/xfe-common.xsl"/>
  
  <!--================================-->
  <!--INIT-->
  <!--================================-->
  
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfe:snrgXML2rngHtml"/>
  </xsl:template>
  
  <!--================================-->
  <!--MAIN-->
  <!--================================-->
  
  <xsl:template match="/" mode="xfe:snrgXML2rngHtml">
    <xsl:call-template name="xfe:log.normalizeConf"/>
    <!--======= STEP 0 =========-->
    <xsl:variable name="step0" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="/" mode="xfe:snrgXML2rngHtml_main"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
       <xsl:variable name="xfe:snrgXML2rngHtml_main" select="resolve-uri('SNRGXML2RNGHTML.step0.main.log.xml', $log.uri)" as="xs:anyURI"/>
       <xsl:message>[INFO] writing <xsl:value-of select="$xfe:snrgXML2rngHtml_main"/></xsl:message>
       <xsl:result-document href="{$xfe:snrgXML2rngHtml_main}">
         <xsl:sequence select="$step0"/>
       </xsl:result-document>
    </xsl:if>
    <!--======= STEP 1 =========-->
    <xsl:variable name="step1" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step0" mode="xfe:snrgXML2rngHtml_duplicateDefineByXpathContext"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="xfe:snrgXML2rngHtml_duplicateDefineByXpathContext" select="resolve-uri('SNRGXML2RNGHTML.step1.duplicateDefineByXpathContext.log.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$xfe:snrgXML2rngHtml_duplicateDefineByXpathContext"/></xsl:message>
      <xsl:result-document href="{$xfe:snrgXML2rngHtml_duplicateDefineByXpathContext}">
        <xsl:sequence select="$step1"/>
      </xsl:result-document>
    </xsl:if>
    <!--======= STEP 2 =========-->
    <xsl:variable name="step2" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step1" mode="xfe:snrgXML2rngHtml_removeVoid"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
       <xsl:variable name="xfe:snrgXML2rngHtml_removeVoid" select="resolve-uri('SNRGXML2RNGHTML.step2.removeVoid.log.xml', $log.uri)" as="xs:anyURI"/>
       <xsl:message>[INFO] writing <xsl:value-of select="$xfe:snrgXML2rngHtml_removeVoid"/></xsl:message>
       <xsl:result-document href="{$xfe:snrgXML2rngHtml_removeVoid}">
         <xsl:sequence select="$step2"/>
       </xsl:result-document>
    </xsl:if>
    <!--======= STEP 3 =========-->
    <xsl:variable name="step3" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step2" mode="xfe:snrgXML2rngHtml_IdIdref"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
       <xsl:variable name="xfe:snrgXML2rngHtml_IdIdref" select="resolve-uri('SNRGXML2RNGHTML.step3.IdIdref.log.xml', $log.uri)" as="xs:anyURI"/>
       <xsl:message>[INFO] writing <xsl:value-of select="$xfe:snrgXML2rngHtml_IdIdref"/></xsl:message>
       <xsl:result-document href="{$xfe:snrgXML2rngHtml_IdIdref}">
         <xsl:sequence select="$step3"/>
       </xsl:result-document>
    </xsl:if>
    <!--======= STEP 4 =========-->
    <xsl:variable name="step4" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step3" mode="xfe:snrgXML2rngHtml_adaptCalsFormat"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
       <xsl:variable name="xfe:snrgXML2rngHtml_adaptCalsFormat" select="resolve-uri('SNRGXML2RNGHTML.step4.adaptCalsFormat.log.xml', $log.uri)" as="xs:anyURI"/>
       <xsl:message>[INFO] writing <xsl:value-of select="$xfe:snrgXML2rngHtml_adaptCalsFormat"/></xsl:message>
       <xsl:result-document href="{$xfe:snrgXML2rngHtml_adaptCalsFormat}">
         <xsl:sequence select="$step4"/>
       </xsl:result-document>
    </xsl:if>
    <!--======= STEP 5 =========-->
    <xsl:variable name="step5" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step4" mode="xfe:snrgXML2rngHtml_moveInlineNotes"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
       <xsl:variable name="xfe:snrgXML2rngHtml_moveInlineNotes" select="resolve-uri('SNRGXML2RNGHTML.step5.moveInlineNotes.log.xml', $log.uri)" as="xs:anyURI"/>
       <xsl:message>[INFO] writing <xsl:value-of select="$xfe:snrgXML2rngHtml_moveInlineNotes"/></xsl:message>
       <xsl:result-document href="{$xfe:snrgXML2rngHtml_moveInlineNotes}">
         <xsl:sequence select="$step5"/>
       </xsl:result-document>
    </xsl:if>
    <!--======= STEP 6 =========-->
    <xsl:variable name="step6" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step5" mode="xfe:snrgXML2rngHtml_tinyMceAnnotation"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="xfe:snrgXML2rngHtml_tinyMceAnnotation" select="resolve-uri('SNRGXML2RNGHTML.step6.tinyMceAnnotation.log.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$xfe:snrgXML2rngHtml_tinyMceAnnotation"/></xsl:message>
      <xsl:result-document href="{$xfe:snrgXML2rngHtml_tinyMceAnnotation}">
        <xsl:sequence select="$step6"/>
      </xsl:result-document>
    </xsl:if>
    <!--======= FINALY =========-->
    <xsl:apply-templates select="$step6" mode="rng:clean"/>
  </xsl:template>
  
  <!--================================-->
  <!--STEP 0 : SNRGXML2RNGHTML : main -->
  <!--================================-->
  
  <!--  on essaie de suivre le chemin xpath de manière à pouvoir faire le lien avec le fichier de conf.
    => c'est vain, il y a une infinité de xpath, ça obligerait à répéter plein de fois les définitions,
    et c'est assez normal si le fichier de conf, basé sur xpath (et non le modèle), 
    induit que la même définition a plusieurs possibilité de balise html
    (titre par exemple)
    => Donc c'est une mauvaise piste de partir sur du xpath dans la conf.
    => le top serait de partir des définitions dans le srng directement, donc d'annoter le schéma (rng ou srng ?)
    => pour les éléments dont le html dépend de valeurs d'attributs (ex : niveau/titre[@type='titre2'] => h2), il faut rajouter un xpath-predicate dans le srng.
    => une autre approche serait de générer dynamiquement un sous-schema pour chaque nœud,
    à la demande dans tinyMCE (service web)
    dans ce cas on connait le contexte du nœud, il est plus simple d'aller matcher le fichier de conf.
  -->
  
  <xsl:template match="grammar" mode="xfe:snrgXML2rngHtml_main">
    <xsl:copy copy-namespaces="yes">
      <xsl:namespace name="xfe" select="'http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor'"/>
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="ns" select="'http://www.w3.org/1999/xhtml'"/>
      <xsl:attribute name="datatypeLibrary" select="'http://www.w3.org/2001/XMLSchema-datatypes'"/>
      <start>
        <element name="html">
          <ref name="head"/>
          <xsl:choose>
            <xsl:when test="not($xfe:srng.grammar.rootIsSingle)">
              <element name="body">
                <xsl:copy-of select="start/node()"/>
              </element>
            </xsl:when>
            <xsl:otherwise>
              <xsl:copy-of select="start/node()"/>
            </xsl:otherwise>
          </xsl:choose>
        </element>
      </start>
      <define name="head">
        <element name="head">
          <zeroOrMore>
            <choice>
              <element name="title">
                <data type="string"/>
              </element>
              <element name="meta">
                <optional><attribute name="http-equiv"/></optional>
                <optional><attribute name="name"/></optional>
                <attribute name="content"/>
              </element>
              <element name="link">
                <attribute name="type"/>
                <attribute name="rel"/>
                <attribute name="href"/>
              </element>
              <element name="script">
                <attribute name="type"/>
                <attribute name="src"/>
              </element>
            </choice>
          </zeroOrMore>
        </element>
      </define>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="start" mode="xfe:snrgXML2rngHtml_main"/>
  
  <xsl:template match="element" mode="xfe:snrgXML2rngHtml_main">
    <xsl:variable name="self" select="self::*" as="element(element)"/>
    <xsl:variable name="confMappingElementsFromSrng" select="xf:getConfMappingElementsFromSrng(.)" as="element()*"/>
    <xsl:choose>
      <xsl:when test="count($confMappingElementsFromSrng) gt 1">
        <!--FIXME : filtrer sur le dernier si 2 règles xpath identiques ou tout prévoir avec un choice et tant pis ?-->
        <choice>
          <!--choice + boucle sur toutes les surcharges possibles : les différents xpath seront résolus dans le mode "duplicateDefineByXpathContext" --> 
          <xsl:for-each select="$confMappingElementsFromSrng">
            <xsl:apply-templates select="$self" mode="xfe:snrgXML2rngHtml_main_with-param-confMappingElementFromSrng">
              <xsl:with-param name="confMappingElementFromSrng" select="."/>
            </xsl:apply-templates>
          </xsl:for-each>
        </choice>
      </xsl:when>
      <xsl:when test="count($confMappingElementsFromSrng) = 1">
        <!-- tester aussi que le xpath correspond ? => non c'est déjà fait dans la conf normalisée--> 
        <xsl:apply-templates select="$self" mode="xfe:snrgXML2rngHtml_main_with-param-confMappingElementFromSrng">
          <xsl:with-param name="confMappingElementFromSrng" select="$confMappingElementsFromSrng" as="element()"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="$self" mode="xfe:snrgXML2rngHtml_main_with-param-confMappingElementFromSrng"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="element" mode="xfe:snrgXML2rngHtml_main_with-param-confMappingElementFromSrng">
    <xsl:param name="confMappingElementFromSrng" as="element()?"/>
    <xsl:variable name="isContentStart" select="parent::define/@name = ancestor::grammar/start//ref/@name" as="xs:boolean"/>
    <xsl:variable name="xfe:label" select="($confMappingElementFromSrng/@label, @name)[1]" as="xs:string"/>
    <xsl:variable name="isExternalLink" select="starts-with($confMappingElementFromSrng/@type, 'xfe:externalLinkType')" as="xs:boolean"/>
    <xsl:variable name="externalLinkType" select="replace($confMappingElementFromSrng/@type, '^xfe:externalLinkType\(?(.*?)\)?$', '$1')" as="xs:string?"/>
    <xsl:copy copy-namespaces="no">
      <xsl:apply-templates select="@*" mode="xfe:snrgXML2rngHtml_main"/>
      <!--<xsl:if test="$isContentStart">
        <xsl:attribute name="xfe:contentStart" select="'true'"/>
      </xsl:if>-->
      <xsl:attribute name="xfe:label" select="$xfe:label"/>
      <!-- Adaptation pour les liens externes avec un type xfe:externalLinkType dans la conf -->
      <xsl:if test="$isExternalLink">
        <xsl:attribute name="xfe:externalLinkType" select="($externalLinkType[normalize-space(.)], 'A Definir')[1]"/>
      </xsl:if>
      <!--<xsl:if test="$confMappingElementFromSrng/@type">
        <xsl:attribute name="xfe:type" select="$confMappingElementFromSrng/@type"/>
      </xsl:if>-->
      <xsl:variable name="name" as="xs:string">
        <xsl:choose>
          <!--On estime qu'on a toujours un div sur la racine xml (ou les racine multiples)-->
          <xsl:when test="$isContentStart">
            <xsl:choose>
              <xsl:when test="$xfe:srng.grammar.rootIsSingle">
                <xsl:text>body</xsl:text>
              </xsl:when>
              <xsl:otherwise>        
                <xsl:text>div</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <!--Il y a une surcharge dans la conf (si plusieurs => cf. boucle ci-dessus, à ce stade on traite une par une)-->
          <xsl:when test="count($confMappingElementFromSrng) = 1 and $confMappingElementFromSrng/@html-name != ''">
            <xsl:value-of select="$confMappingElementFromSrng/@html-name"/>
          </xsl:when>
          <!--la transfo cals2html fait toujours un div sur table-->
          <xsl:when test="count($confMappingElementFromSrng) = 1 and $confMappingElementFromSrng/@xml-format = 'cals:table' and @name = ('table', 'TABLE', 'Table')">
            <xsl:text>div</xsl:text>
          </xsl:when>
          <xsl:when test="rng:isInlineOnly(.)">
            <xsl:text>span</xsl:text>
          </xsl:when>
          <xsl:when test="rng:isBlockOnly(.)">
            <xsl:text>div</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>spanOrDiv?</xsl:text>
            <xsl:message terminate="no">[ERROR] rng:element "<xsl:value-of select="@name"/>" is inline or block depending on the context. The html.rng schema will be wrong (unvalid).
              Please remove this ambiguity (see the documentation at main/conf/xfe-conf.md) </xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:attribute name="name" select="$name"/>
      <xsl:processing-instruction name="xfe-srng">
        <xsl:text>name="</xsl:text>
        <xsl:value-of select="@name"/>
        <xsl:text>"</xsl:text>
      </xsl:processing-instruction>
      <xsl:if test="exists($confMappingElementFromSrng)">
        <xsl:processing-instruction name="xfe-conf">
          <xsl:for-each select="$confMappingElementFromSrng/@*">
            <xsl:value-of select="local-name(.)"/>
            <xsl:text>="</xsl:text>
            <xsl:value-of select="."/>
            <xsl:text>"</xsl:text>
            <xsl:if test="not(position() = last())">
              <xsl:text> </xsl:text>
            </xsl:if>
          </xsl:for-each>
        </xsl:processing-instruction>
      </xsl:if>
      <!--FLAS-46 : pour une question de perf tinyMCE on ajoute le label dans le HTML-->
      <optional>
        <attribute name="{$xfe:dataAttribute.prefix}xfe-label">
          <!--<data type="string">-->
            <value><xsl:value-of select="$xfe:label"/></value>
          <!--</data>-->
        </attribute>
      </optional>
      <!-- Adaptation pour les listes avec un type xfe:list dans la conf -->
      <xsl:if test="starts-with(string($confMappingElementFromSrng/@type), 'xfe:list')">
        <attribute name="{$xfe:dataAttribute.prefix}xfe-listType">
          <choice>
            <xsl:for-each select="tokenize(normalize-space($confMappingElementFromSrng/@listTypeValue), '\s*;\s*')">
              <value type="token"><xsl:value-of select="."/></value>
            </xsl:for-each>
          </choice>
        </attribute>
      </xsl:if>
      <!-- Adaptation pour les link avec un type xfe:externalLinkType dans la conf -->
      <!--<xsl:if test="$externalLinkType = ('resolve', 'resolveAndQualify')">-->
      <xsl:if test="$isExternalLink and $name = 'a'">
        <xsl:if test="not($confMappingElementFromSrng/@html-attribute-href)">
          <attribute name="href"/>
        </xsl:if>
        <optional>
          <choice>
            <attribute name="{concat($xfe:dataAttribute.prefix, 'xfe-idRef')}"/>
            <attribute name="{concat($xfe:dataAttribute.prefix, 'xfe-xf_idRef')}"/>
          </choice>
        </optional>
        <!--<attribute name="{concat($dataXmlAttribute.prefix, 'xf_refType')}"/>-->
      </xsl:if>
      <!--On estime que tous les élément html peuvent avoir un attribut @style-->
      <xsl:if test="not(.//attribute[@name='style'])">
        <optional>
          <attribute name="style">
            <data type="string"/>
          </attribute>
        </optional>
      </xsl:if>
      <xsl:variable name="class" as="xs:string*">
        <xsl:choose>
          <!--la transfo cals2html fait toujours un div class="table" sur table-->
          <xsl:when test="count($confMappingElementFromSrng) = 1 and $confMappingElementFromSrng/@xml-format = 'cals:table' and @name = ('table', 'TABLE', 'Table')">
            <xsl:text>table</xsl:text>
          </xsl:when>
          <xsl:when test="matches($confMappingElementFromSrng/@html-xmlNameAsClass, '(0|false)')">
            <!--la conf indique de ne pas mettre de class-->
          </xsl:when>
          <xsl:otherwise>
            <!--par défaut le nom de l'élément xml-->
            <xsl:value-of select="@name"/>
          </xsl:otherwise>
        </xsl:choose>
        <!--class ajoutée dans la conf.-->
        <xsl:if test="$confMappingElementFromSrng/@html-addClass != ''">
          <xsl:text> </xsl:text>
          <xsl:value-of select="$confMappingElementFromSrng/@html-addClass"/>
        </xsl:if>
      </xsl:variable>
      <xsl:variable name="classAttribute" as="element()">
        <attribute name="class">
          <value>
            <xsl:value-of select="normalize-space(string-join($class, ''))"/>
          </value>
        </attribute>
      </xsl:variable>
      <xsl:if test="normalize-space(string-join($class, '')) != ''">
        <xsl:choose>
          <xsl:when test="$name = 'body' and $xfe:srng.grammar.rootIsSingle">
            <!--FE-77 : class on body must be optional for technical reasons in XFE (but ok for css)-->
            <optional>
              <xsl:sequence select="$classAttribute"/>
            </optional>
          </xsl:when>
          <xsl:otherwise>
            <xsl:sequence select="$classAttribute"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:if>
      <xsl:variable name="htmlAttributes" select="$confMappingElementFromSrng/@*[starts-with(local-name(.), 'html-attribute-')][not(local-name(.) = 'html-attribute-style')]" as="attribute()*"/>
      <!--style est géré partout, cf. ci-dessus-->
      <xsl:for-each select="$htmlAttributes">
        <attribute name="{substring-after(local-name(.), 'html-attribute-')}">
          <data type="string"/>
        </attribute>
      </xsl:for-each>
      <xsl:apply-templates mode="xfe:snrgXML2rngHtml_main">
        <xsl:with-param name="desactivate-children" select="tokenize($confMappingElementFromSrng/@desactivate-children, $els:regAnySpace)" tunnel="yes"/>
        <xsl:with-param name="xmlFormat" select="$confMappingElementFromSrng/@xml-format" as="xs:string?" tunnel="yes"/>
        <xsl:with-param name="html-ignoreXmlAttributes" select="$confMappingElementFromSrng/@html-ignoreXmlAttributes" as="xs:string?" tunnel="yes"/>
        <xsl:with-param name="htmlSchema-disableAttribute" select="$confMappingElementFromSrng/@htmlSchema-disableAttribute" as="xs:string?" tunnel="yes"/>
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="ref" mode="xfe:snrgXML2rngHtml_main">
    <xsl:param name="desactivate-children" as="xs:string*" tunnel="yes"/>
    <xsl:choose>
      <xsl:when test="rng:getDefine(.)/element/@name = $desactivate-children">
        <xsl:copy copy-namespaces="no">
          <xsl:apply-templates select="@*" mode="#current"/>
          <xsl:attribute name="xfe:activate" select="'false'"/>
          <xsl:apply-templates mode="#current"/>
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:next-match/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="attribute" mode="xfe:snrgXML2rngHtml_main">
    <xsl:param name="html-ignoreXmlAttributes" as="xs:string?" tunnel="yes"/>
    <xsl:param name="htmlSchema-disableAttribute" as="xs:string?" tunnel="yes"/>
    <!--s'il est spécifié d'ignorer l'attribut, on le fait (le nettoyage rng:clean supprimera ce qui est au dessus)-->
    <xsl:choose>
      <xsl:when test="@name = tokenize($html-ignoreXmlAttributes, '\s+')">
        <!--<xsl:message>[INFO][html-ignoreXmlAttributes] Suppression <xsl:value-of select="@name"/> sur <xsl:value-of select="ancestor::element[1]/@name"/></xsl:message>-->        
      </xsl:when>
      <xsl:when test="@name = tokenize($htmlSchema-disableAttribute, '\s+')">
        <xsl:copy copy-namespaces="no">
          <xsl:apply-templates select="@*" mode="#current"/>
          <xsl:attribute name="xfe:disable" select="'true'"/>
          <xsl:apply-templates select="node()" mode="#current"/>
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:next-match/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--<xsl:template match="element/@name[lower-case(.) = 'id']" mode="xfe:snrgXML2rngHtml_main" priority="1">
    <xsl:attribute name="name" select="."/>
  </xsl:template>-->
  
  <xsl:template match="attribute/@name[lower-case(.) = 'id']" mode="xfe:snrgXML2rngHtml_main" priority="1">
    <xsl:attribute name="name" select="lower-case(.)"/>
  </xsl:template>
  
  <xsl:template match="attribute/@name" mode="xfe:snrgXML2rngHtml_main">
    <xsl:param name="xmlFormat" as="xs:string?" tunnel="yes"/>
    <xsl:choose>
      <xsl:when test="starts-with($xmlFormat, 'html:')">
        <xsl:next-match/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:attribute name="name" select="concat($xfe:dataXmlAttribute.prefix, .)"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="@ns" mode="xfe:snrgXML2rngHtml_main"/>
  
  <!--xs datatypeLibrary remis à la racine--> 
  <xsl:template match="@datatypeLibrary[. = 'http://www.w3.org/2001/XMLSchema-datatypes']" mode="xfe:snrgXML2rngHtml_main"/>

  <!--=======================================-->
  <!--STEP 1 : duplicateDefineByXpathContext -->
  <!--=======================================-->
  
  <xsl:template match="define[choice]" mode="xfe:snrgXML2rngHtml_duplicateDefineByXpathContext">
    <xsl:apply-templates mode="#current"/>
    <!--let the default define, that could be usefull-->
    <xsl:copy-of select="." copy-namespaces="no"/>
  </xsl:template>
  
  <xsl:template match="define/choice" mode="xfe:snrgXML2rngHtml_duplicateDefineByXpathContext">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
    
  <xsl:template match="define/choice/element" mode="xfe:snrgXML2rngHtml_duplicateDefineByXpathContext">
    <xsl:message>[INFO] Duplicating define <xsl:value-of select="ancestor::define/@name"/> - <xsl:value-of select="@name"/></xsl:message>
    <define name="{xfe:snrgXML2rngHtml_duplicateDefineByXpathContext_renameDefine(.)}">
      <xsl:next-match/>
    </define>
  </xsl:template>
  
  <xsl:template match="ref[rng:getDefine(.)/choice]" mode="xfe:snrgXML2rngHtml_duplicateDefineByXpathContext">
    <xsl:variable name="ref.define" select="rng:getDefine(.)" as="element(define)"/>
    <xsl:variable name="ref.define.element.xml.name" select="els:getPseudoAttributeValue($ref.define/choice/element[1]/processing-instruction('xfe-srng'), 'name')" as="xs:string"/>
    <xsl:variable name="context.element" select="ancestor::element" as="element(element)"/>
    <xsl:variable name="context.element.xml.name" select="els:getPseudoAttributeValue($context.element/processing-instruction('xfe-srng'), 'name')" as="xs:string"/>
    <xsl:variable name="ref.define.elements" select="rng:getDefine(.)/choice/element" as="element(element)*"/>
    <xsl:variable name="xpath.level1" select="concat('//', $ref.define.element.xml.name)" as="xs:string"/>
    <xsl:variable name="xpath.level2" select="concat('//', string-join(($context.element.xml.name, $ref.define.element.xml.name), '/'))" as="xs:string"/>
    <xsl:variable name="ref.define.elements.with.xpath.level1" select="$ref.define.elements[els:getPseudoAttributeValue(processing-instruction('xfe-conf')[1], 'xpath') = $xpath.level1]" as="element(element)*"/>
    <xsl:variable name="ref.define.elements.with.xpath.level2" select="$ref.define.elements[els:getPseudoAttributeValue(processing-instruction('xfe-conf')[1], 'xpath') = $xpath.level2]" as="element(element)*"/>
    <xsl:variable name="ref.define.element" as="element(element)*">
      <xsl:choose>
        <xsl:when test="count($ref.define.elements.with.xpath.level2) = 1">
          <xsl:sequence select="$ref.define.elements.with.xpath.level2"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:sequence select="$ref.define.elements.with.xpath.level1"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="count($ref.define.element) = 1">
        <ref name="{xfe:snrgXML2rngHtml_duplicateDefineByXpathContext_renameDefine($ref.define.element)}"/>
        <!--count-xpath2="{count($ref.define.elements.with.xpath.level2)}"
        count-xpath1="{count($ref.define.elements.with.xpath.level1)}"-->
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="no">[INFO] <xsl:value-of select="count($ref.define.element)"/> ref.define.element found for <xsl:value-of select="els:displayNode(.)"/>, xpath=<xsl:value-of select="$xpath.level2"/>, go to the default define</xsl:message>
        <xsl:next-match/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:function name="xfe:snrgXML2rngHtml_duplicateDefineByXpathContext_renameDefine" as="xs:string">
    <xsl:param name="element" as="element(element)"/>
    <xsl:variable name="define" select="$element/ancestor::define" as="element(define)"/>
    <xsl:sequence select="concat('duplicate_', $define/@name, '_', $element/@name, 
      if ($element/following-sibling::element[@name = $element/@name]) then (concat('_', count($element/preceding-sibling::element[@name = $element/@name]) + 1)) else ())"/>
  </xsl:function>
  
  <!--================================-->
  <!--STEP 2 : REMOVEVOID -->
  <!--================================-->
  
  <xsl:template match="element[@name = 'void']" mode="xfe:snrgXML2rngHtml_removeVoid">
    <!--<xsl:message>[INFO] genid=<xsl:value-of select="generate-id(.)"/></xsl:message>-->
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="element[@name = 'void']//attribute" mode="xfe:snrgXML2rngHtml_removeVoid"/>
  <xsl:template match="element[@name = 'void']//optional[attribute]" mode="xfe:snrgXML2rngHtml_removeVoid"/>

  <!--Suppression des références circulaires sur les void-->
  <xsl:template match="element[@name = 'void']//*[local-name(.) = ('zeroOrMore', 'oneOreMore')][ref[@name = ancestor::define/@name]]" mode="xfe:snrgXML2rngHtml_removeVoid"/>
  <xsl:template match="element[@name = 'void']//ref[@name = ancestor::define/@name]" mode="xfe:snrgXML2rngHtml_removeVoid"/>
  
  <!--=================================-->
  <!--STEP 3 : CONVERT ID/IDREF TO SCH -->
  <!--=================================-->
  <!--
    Même avec une class différente, jing se plaint "conflicting ID-types for attribute ..." lorsque 2 éléments du même nom ont 2 attributs identiques, l'un de type ID  ou IDREF, l'autre différent
    On va donc supprimer ces contraintes en remplaçant les ID/IDREF par des NCNAME, on conserve cependant l'information dans du schematron "embed"
  -->
  
  <xsl:template match="/" mode="xfe:snrgXML2rngHtml_IdIdref">
    <xsl:variable name="root" select="." as="document-node()"/>
    <xsl:apply-templates mode="#current">
      <xsl:with-param name="idTypeByElementName" as="element()*" tunnel="yes">
        <xsl:for-each select="distinct-values(.//element[.//attribute[@name = 'id']]/@name)">
          <xsl:variable name="name" select="." as="xs:string"/>
          <element name="{$name}">
            <xsl:for-each select="$root//*[@name = $name]//attribute[@name = 'id']">
              <xsl:copy-of select="."/>
            </xsl:for-each>
          </element>
        </xsl:for-each>
      </xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>
  
  <!--On ne travaille que sur les attribut autre que @id -->
  
  <xsl:template match="attribute[not(@name = ('id'))]/data/@type[. = ('ID', 'IDREF')]" mode="xfe:snrgXML2rngHtml_IdIdref">
    <xsl:attribute name="{name()}" select="'NCName'"/>
  </xsl:template>
  
  <xsl:template match="attribute[not(@name = ('id'))][data/@type[. = ('ID', 'IDREF')]]" mode="xfe:snrgXML2rngHtml_IdIdref">
    <xsl:variable name="type" select="data/@type[. = ('ID', 'IDREF')]" as="xs:string"/>
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:processing-instruction name="was"><xsl:value-of select="$type"/></xsl:processing-instruction>
      <!--<sch:pattern name="Test constraints on the Root element" >
        <sch:rule context="Root">
          <sch:assert test="test-condition"></sch:assert>
        </sch:rule>
      </sch:pattern>-->
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--puis sur les @id qui ont été générés par le step précédents-->
  <xsl:template match="attribute[@name = 'id']/data/@type[. = ('ID')]" mode="xfe:snrgXML2rngHtml_IdIdref">
    <xsl:param name="idTypeByElementName" as="element()*" tunnel="yes"/>
    <xsl:variable name="elementName" select="ancestor::element[1]/@name" as="xs:string"/>
    <xsl:choose>
      <!--Pour un nom (html) d'élément donné, tous les @id sont de type id : on conserve ce typage-->
      <xsl:when test="every $id in $idTypeByElementName[@name = $elementName]/attribute[@name = 'id'] satisfies $id/data/@type = 'ID'">
        <!--<xsl:message>Que des vrais ID pour <xsl:value-of select="$elementName"/> (count =<xsl:value-of select="count($idTypeByElementName[@name = $elementName]/attribute[@name = 'id'])"/>)</xsl:message>-->
        <xsl:next-match/>
      </xsl:when>
      <!--Sinon on passe en NCName les attributs id qui sont de type ID (ceux matchés ici)-->
      <xsl:otherwise>
        <xsl:attribute name="{name()}" select="'NCName'"/>
      </xsl:otherwise>
    </xsl:choose>
    <!--<xsl:if test="$elementName = 'div'">
      <xsl:message>idTypeByElementName=<xsl:copy-of select="$idTypeByElementName"/></xsl:message>
      <DEBUG>
        <xsl:copy-of select="$idTypeByElementName"/>
      </DEBUG>
    </xsl:if>-->
  </xsl:template>
  
  <!--=================================-->
  <!--STEP 4 : adaptCalsFormat -->
  <!--=================================-->
  <!--Dans la conf tous les éléments de tableaux héritent automatiquement du bon mapping-->
  <!--Finalisation de ce qui est difficile à faire génériquement -->
  
  <!--TABLEAU CALS vers HTML : @class autorisés (sans le nom de l'élément xml)-->
  
  <xsl:template match="element[snrgXML2rngHtml:xmlFormatIsCals(.)][@name = ('table', 'tr', 'td')]" mode="xfe:snrgXML2rngHtml_adaptCalsFormat">
    <xsl:copy>
      <xsl:apply-templates select="@* | processing-instruction()" mode="#current"/>
      <optional>
        <attribute name="class">
          <data type="string"/>
        </attribute>
      </optional>
      <xsl:if test="@name = 'td'">
        <optional>
          <attribute name="colspan">
            <data type="integer"/>
          </attribute>
        </optional>
        <optional>
          <attribute name="rowspan">
            <data type="integer"/>
          </attribute>
        </optional>
      </xsl:if>
      <xsl:apply-templates select="node() except processing-instruction()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- attribut @style ajouté ci-dessus sur tous les éléments (FIXME : est ce bien ? à mettre juste sur certains éléments ?)
    <xsl:template match="element[snrgXML2rngHtml:xmlFormatIsCals(.)][@name = 'col']" mode="xfe:snrgXML2rngHtml_adaptCalsFormat">
    <xsl:copy>
      <xsl:apply-templates select="@* | processing-instruction()" mode="#current"/>
      <optional>
        <attribute name="style">
          <data type="string"/>
        </attribute>
      </optional>
      <xsl:apply-templates select="node() except processing-instruction()" mode="#current"/>
    </xsl:copy>
  </xsl:template>-->
  
  <!--cals2html ne mets pas ces attributs-->
  <xsl:template match="element[snrgXML2rngHtml:xmlFormatIsCals(.)][@name = ('table', 'tr', 'td')]//attribute[starts-with(@name, $xfe:dataXmlAttribute.prefix)] " mode="xfe:snrgXML2rngHtml_adaptCalsFormat"/>
  
  <!--pour le table/@tabStyle qui passe en class-->
  <xsl:template match="element[snrgXML2rngHtml:xmlFormatIsCals(.)]//attribute[@name = 'class']" mode="xfe:snrgXML2rngHtml_adaptCalsFormat">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <data type="string">
        <param name="pattern"><xsl:value-of select="."/>(\s*.*)?</param>
      </data>
    </xsl:copy>
  </xsl:template>
  
  <!--TABLEAU CALS vers HTML : ajout de colgroup-->
  <!--fixme : vérifier au préalable que ce match va matcher quelque-chose-->
  <xsl:template match="element[snrgXML2rngHtml:xmlFormatIsCals(.)][@name = 'table']//*[local-name() = ('oneOrMore', 'zeroOrMore')][count(*) = 1][ref[rng:getDefine(.)/element/@name = 'col']]" mode="xfe:snrgXML2rngHtml_adaptCalsFormat">
    <element name="colgroup">
      <xsl:next-match/>
    </element>
  </xsl:template>
  
  <!--INUTILE : on ne va plutôt adapter cals2html pour ne pas mettre N fois le titre dans le html-->
  <!--TABLEAU CALS vers HTML : déplacement de table/titre dans tgroup/titre=table/caption-->
  <!--<xsl:template match="
    element[els:getPseudoAttributeValue(processing-instruction('xfe-conf')[1], 'xml-format') = 'cals']
    //optional[count(*) = 1][ref/rng:getDefine(.)/element/processing-instruction('xfe-srng')[1]/els:getPseudoAttributeValue(., 'name') = 'titre']"
    mode="xfe:snrgXML2rngHtml_adaptCalsFormat">
    <xsl:message>[INFO] suppression de la référence à titre dans table</xsl:message>
  </xsl:template>
  
  <xsl:template match="element[els:getPseudoAttributeValue(processing-instruction('xfe-srng')[1], 'name') = 'tgroup']" mode="xfe:snrgXML2rngHtml_adaptCalsFormat">
    <xsl:copy>
      <xsl:apply-templates select="@* | processing-instruction()" mode="#current"/>
      <xsl:variable name="refElement" select="//element[els:getPseudoAttributeValue(processing-instruction('xfe-conf')[1], 'xml-format') = 'cals']
        //optional[count(*) = 1]/ref[rng:getDefine(.)/element/processing-instruction('xfe-srng')[1]/els:getPseudoAttributeValue(., 'name') = 'titre']
        /rng:getDefine(.)/element" as="element()"/>
      <optional>
        <xsl:choose>
          <xsl:when test="count($refElement) != 1">
            <xsl:message>[ERROR] déplacement des titres de tableaux cals, <xsl:value-of select="count($refElement)"/> ref trouvé</xsl:message>
          </xsl:when>
          <xsl:otherwise>
            <element name="caption">
              <xsl:sequence select="$refElement/node()"/>
            </element>
          </xsl:otherwise>
        </xsl:choose>
      </optional>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>-->
  
  <!--=================================-->
  <!--STEP 5 : Move Inline Notes-->
  <!--=================================-->
  
  <xsl:template match="element[@name = 'body']" mode="xfe:snrgXML2rngHtml_moveInlineNotes">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" mode="#current"/>
      <xsl:variable name="xfe:inlineNotes" select="//element[xfe:isInlineNote(.)]" as="element(element)*"/>
      <xsl:choose>
        <xsl:when test="count($xfe:inlineNotes) = 1">
          <element name="div">
            <attribute name="class">
              <value>xfe_notes</value>
            </attribute>
            <zeroOrMore>
              <element name="div">
                <attribute name="class">
                  <value>xfe_note</value>
                </attribute>
                <attribute name="id">
                  <data type="NCName"/> <!--ID créérait un conflit avec les autres div ID-->
                </attribute>
                <xsl:apply-templates select="$xfe:inlineNotes" mode="xfe:snrgXML2rngHtml_getInlineNoteDataModel"/>
              </element>
            </zeroOrMore>
          </element>
        </xsl:when>
        <xsl:when test="count($xfe:inlineNotes) gt 1">
          <xsl:message>[ERROR] Il ne peut pas y avoir plusieurs elements de type xfe:inlineNote dans la conf (ou alors il faut revoir le code pour gérer différents contenus)</xsl:message>
        </xsl:when>
      </xsl:choose>
    </xsl:copy>
  </xsl:template>
  
  <!--Suppression du contenu de la note en line qui est déplacée en fin de modèles-->
  <xsl:template match="element[xfe:isInlineNote(.)]//ref" mode="xfe:snrgXML2rngHtml_moveInlineNotes"/>
  <xsl:template match="element[xfe:isInlineNote(.)]//text[not(ancestor::attribute)]" mode="xfe:snrgXML2rngHtml_moveInlineNotes"/>
  
  <!--Un mode pour récupérer le dataModel du contenu de la note et le déplacer en fin de RNG-->
  <!--Suppression de l'élément qui sert d'appel de note (lui n'est pas déplacé)-->
  <xsl:template match="element[xfe:isInlineNote(.)]" mode="xfe:snrgXML2rngHtml_getInlineNoteDataModel">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!--Suppression des attributs qui restent sur l'appel de note-->
  <xsl:template match="element[xfe:isInlineNote(.)]//attribute" mode="xfe:snrgXML2rngHtml_getInlineNoteDataModel"/>
  
  <xsl:function name="xfe:isInlineNote" as="xs:boolean">
    <xsl:param name="element" as="element(element)"/>
    <xsl:sequence select="$element/els:getPseudoAttributeValue(processing-instruction('xfe-conf')[1], 'type') = 'xfe:inlineNote'"/>
  </xsl:function>
  
  <!--=================================-->
  <!--STEP 6 : tinyMceAnnotation-->
  <!--=================================-->
  
  <!--Les annotation n'ont lieux que sur les élément inline : mixed et texte brut-->
  
  <!--
    MIXED 2 possibilités : 
    - mixed qui est converti (srng) en interleave {pattern} text
    
          <interleave>
            <text/>
            <zeroOrMore>
              <choice>
                <ref name="e"/>
                <ref name="ind"/>

     - une autre structure de type 
     
            <oneOrMore>
              <choice>
                <text/>
                <ref name="ITAL"/>
    TEXTE brut :
      <data type="string"/>
   -->
  
  <xsl:template match="interleave[*[1]/self::text][count(*) = 2]/zeroOrMore[count(*) = 1]/choice" mode="xfe:snrgXML2rngHtml_tinyMceAnnotation">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:if test="count(* except ref) != 0">
        <xsl:message terminate="yes">[ERROR] Not only ref found within this mixed element structure : <xsl:value-of select="els:get-xpath(.)"/></xsl:message>
      </xsl:if>
      <xsl:copy-of select="ref"/>
      <!--<xsl:apply-templates select="ref" mode="#current"/>-->
      <ref name="comment_text"/>
      <xsl:sequence select="xfe:snrgXML2rngHtml_makeAnnotation('ins', (ref))"/>
      <xsl:sequence select="xfe:snrgXML2rngHtml_makeAnnotation('del', (ref))"/>
    </xsl:copy>
  </xsl:template>
  <!--unfallback matching-->
  <xsl:template match="interleave[*[1]/self::text][count(*) = 2][zeroOrMore]/text" mode="xfe:snrgXML2rngHtml_tinyMceAnnotation">
    <xsl:copy-of select="."/>
  </xsl:template>
  
  <xsl:template match="oneOrMore[count(*) = 1]/choice[text]" mode="xfe:snrgXML2rngHtml_tinyMceAnnotation">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:if test="count(* except (ref | text)) != 0">
        <xsl:message terminate="yes">[ERROR] Not only ref and text found within this mixed element structure : <xsl:value-of select="els:get-xpath(.)"/></xsl:message>
      </xsl:if>
      <xsl:copy-of select="text | ref"/>
      <!--<xsl:apply-templates select="text | ref" mode="#current"/>-->
      <ref name="comment_text"/>
      <xsl:sequence select="xfe:snrgXML2rngHtml_makeAnnotation('ins', (ref))"/>
      <xsl:sequence select="xfe:snrgXML2rngHtml_makeAnnotation('del', (ref))"/>
    </xsl:copy>
  </xsl:template>
  <!--unfallback matching : unusefull because there is no apply-template (to text) on the template above-->
  <!--<xsl:template match="oneOrMore[count(*) = 1]/choice/text" mode="xfe:snrgXML2rngHtml_tinyMceAnnotation">
    <xsl:copy-of select="."/>
  </xsl:template>-->
  
  <!--ÉLÉMENTS EN TEXTE BRUT-->
  
  <!--<xsl:template match="element/data[not(parent::attribute)]" mode="xfe:snrgXML2rngHtml_tinyMceAnnotation" priority="1">
    <zeroOrMore>
      <choice>
        <xsl:comment><xsl:value-of select="els:displayNode(.)"/> : on ne peut pas avoir une répétition (zeroOrMore ci-dessus) de data en RNG</xsl:comment>
        <text/>
        <ref name="ins_text"/>
        <ref name="del_text"/>
        <ref name="comment_text"/>
      </choice>
    </zeroOrMore>
  </xsl:template>-->
  
  <xsl:template mode="xfe:snrgXML2rngHtml_tinyMceAnnotation" priority="1"
    match="element[count(* except (attribute | optional[attribute] | optional[choice[attribute]])) = 1]/*[name() = ('data', 'text')]
    | element[count(* except (attribute | optional[attribute] |optional[choice[attribute]])) = 1]/group[count(* except (attribute | optional[attribute] | optional[choice[attribute]])) = 1]/*[name() = ('data', 'text')]">
    <choice>
      <xsl:copy-of select="."/>
      <zeroOrMore>
        <choice>
          <xsl:if test="self::data">
            <!--FIXME : attention ici on perd le data type, on ne peut pas vraiment faire autrement ... schematron qui vérifie le datatype (sauf string) entre chaque ins/del/comment ? -->
            <xsl:comment><xsl:value-of select="els:displayNode(.)"/> : on ne peut pas avoir une répétition (zeroOrMore ci-dessus) de data en RNG</xsl:comment>
          </xsl:if>
          <text/>
          <ref name="ins_text"/>
          <ref name="del_text"/>
          <ref name="comment_text"/>
        </choice>
      </zeroOrMore>
    </choice>
  </xsl:template>
  
  <!--Fallback-->
  <xsl:template match="*[name() = ('text', 'data')][not(parent::attribute)]" mode="xfe:snrgXML2rngHtml_tinyMceAnnotation" priority="-1">
    <xsl:next-match/>
    <xsl:message terminate="yes">[ERROR] element <xsl:value-of select="concat('rng:', name())"/> UNMATCHED in mode "xfe:snrgXML2rngHtml_tinyMceAnnotation" : define "<xsl:value-of select="ancestor::define/@name"/>" at <xsl:value-of select="els:get-xpath(.)"/></xsl:message>
  </xsl:template>
  
  <xsl:template match="/grammar/define[last()]" mode="xfe:snrgXML2rngHtml_tinyMceAnnotation">
    <xsl:next-match/>
    <xsl:comment>=============================================</xsl:comment>
    <xsl:comment> TINY MCE : annotations</xsl:comment>
    <xsl:comment>=============================================</xsl:comment>
    <define name="comment_text">
      <element name="span">
        <attribute name="class">
          <value>tinyMce_annotation</value>
        </attribute>
        <attribute name="{$xfe:dataAttribute.prefix}annotation"/>
        <!--<optional> Non car selon qu'il y a d'autres span avec @id, il faudrait s'assure que ce soit le même type (dalloz revue est en type cdata sur les id ...)
          <attribute name="id">
            <data type="ID"/>
          </attribute>
        </optional>-->
        <zeroOrMore>
          <attribute>
            <anyName>
              <except>
                <name ns="">id</name>
                <name ns="">class</name>
                <name ns=""><xsl:value-of select="$xfe:dataAttribute.prefix"/>annotation</name>
              </except>
            </anyName>
          </attribute>
        </zeroOrMore>
        <empty/><!--le contenu du commentaire est mis dans l'attribut annotation-->
      </element>
    </define>
    <define name="ins_text">
      <xsl:sequence select="xfe:snrgXML2rngHtml_makeAnnotation('ins', ())"/>
    </define>
    <define name="del_text">
      <xsl:sequence select="xfe:snrgXML2rngHtml_makeAnnotation('del', ())"/>
    </define>
  </xsl:template>
  
  <!--Default Copy-->
  <xsl:template match="node() | @*" mode="xfe:snrgXML2rngHtml_tinyMceAnnotation" priority="-2">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--=================================-->
  <!--STEP 6 : rng:clean -->
  <!--=================================-->
  <!--cf. rng-common.xsl-->
  
  <!--================================-->
  <!--COMMON-->
  <!--================================-->
  
  <xsl:function name="snrgXML2rngHtml:xmlFormatIsCals" as="xs:boolean">
    <xsl:param name="element" as="element(rng:element)"/>
    <xsl:sequence select="starts-with(els:getPseudoAttributeValue($element/processing-instruction('xfe-conf')[1], 'xml-format'), 'cals:')"/>
  </xsl:function>
  
  <xsl:function name="snrgXML2rngHtml:hasXmlName" as="xs:boolean">
    <xsl:param name="element" as="element(rng:element)"/>
    <xsl:param name="xmlName" as="xs:string"/>
    <xsl:sequence select="els:getPseudoAttributeValue($element/processing-instruction('xfe-srng')[1], 'name') = $xmlName"/>
  </xsl:function>
  
  <xsl:function name="xfe:snrgXML2rngHtml_makeAnnotation" as="element()">
    <xsl:param name="name" as="xs:string"/> <!--ins or del-->
    <xsl:param name="refs" as="element(ref)*"/>
    <element name="{$name}">
      <zeroOrMore>
        <attribute>
          <anyName/>
        </attribute>
      </zeroOrMore>
      <xsl:choose>
        <xsl:when test="count($refs) = 0">
          <text/>
        </xsl:when>
        <xsl:otherwise>
          <zeroOrMore>
            <choice>
              <text/>
              <xsl:sequence select="$refs"/>
            </choice>
          </zeroOrMore>
        </xsl:otherwise>
      </xsl:choose>
    </element>
  </xsl:function>
  
  <!--copie par défaut-->
  <xsl:template match="node() | @*" 
    mode="
    xfe:snrgXML2rngHtml_main 
    xfe:snrgXML2rngHtml_duplicateDefineByXpathContext
    xfe:snrgXML2rngHtml_removeVoid 
    xfe:snrgXML2rngHtml_IdIdref 
    xfe:snrgXML2rngHtml_adaptCalsFormat 
    xfe:snrgXML2rngHtml_moveInlineNotes 
    xfe:snrgXML2rngHtml_getInlineNoteDataModel
    ">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
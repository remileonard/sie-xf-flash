<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:saxon="http://saxon.sf.net/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:rng="http://relaxng.org/ns/structure/1.0"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xslLib="http://www.lefebvre-sarrut.eu/ns/els/xslLib"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns:html2xml="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor/html2xsl"
  xmlns="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
  xpath-default-namespace="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <xsl:import href="xfe:/xsl/xfe-common.xsl"/>
  <xsl:import href="xfe:/xsl/xfe-HTML2XML_clean.xsl"/>
  <xsl:import href="xslLib:/xslLib/html2cals.xsl"/>
  <xsl:include href="xfe:/xsl/efl/infoCommentaire.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Retro-conv HTML vers XML du contentNode</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!--FIXME : changer xmlns par défaut-->
  
  <xsl:output method="xml" indent="no"/>
  
  <xsl:param name="xslLib:cals.ns.uri" select="$xfe:ns.uri" as="xs:string"/>
  <xsl:param name="xfe:wrapWithFictionalEE" select="true()" as="xs:boolean"/>
  <xsl:param name="xfe:addXmlModelPi" select="true()" as="xs:boolean"/>
  <xsl:param name="xfe:cleanHtmlBefore" select="false()" as="xs:boolean"/>
  
  <xsl:variable name="xfe:ns.uri" select="rng:getRootNamespaceUri($xfe:srng.grammar)" as="xs:string"/>
  
  <!--================================-->
  <!--INIT-->
  <!--================================-->
  
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfe:html2xml"/>
  </xsl:template>
  
  <!--================================-->
  <!--MAIN-->
  <!--================================-->
  
  <xsl:template match="/" mode="xfe:html2xml">
    <xsl:message>[INFO] xfe-HTML2XML.xsl on <xsl:value-of select="els:getFileName(base-uri(.))"/> [params : xfe:wrapWithFictionalEE=<xsl:value-of select="$xfe:wrapWithFictionalEE"/>, xfe:addXmlModelPi=<xsl:value-of select="$xfe:addXmlModelPi"/>, xfe:cleanHtmlBefore=<xsl:value-of select="$xfe:cleanHtmlBefore"/>]</xsl:message>
    <!--STEP 0 : clean before if necessary (need for local tests)-->
    <xsl:variable name="step0" as="document-node()">
      <xsl:choose>
        <xsl:when test="$xfe:cleanHtmlBefore">
          <xsl:apply-templates select="." mode="xfe:html2xml_clean"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:sequence select="."/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <!--STEP 1-->
    <xsl:variable name="step1" as="document-node()">
      <xsl:document>
        <xsl:choose>
          <xsl:when test="$xfe:srng.grammar.rootIsSingle">
            <xsl:apply-templates select="$step0/html/body" mode="xfe:html2xml_main"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="$step0/html/body/div[1]" mode="xfe:html2xml_main"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="step1.log.uri" select="resolve-uri('html2xml.step1.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$step1.log.uri"/></xsl:message>
      <xsl:result-document href="{$step1.log.uri}">
        <xsl:sequence select="$step1"/>
      </xsl:result-document>
    </xsl:if>
    <!--STEP 2-->
    <xsl:variable name="step2" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step1" mode="xslLib:xhtml2cals">
          <xsl:with-param name="ns.uri" select="$xfe:ns.uri"/>
        </xsl:apply-templates>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="step2.log.uri" select="resolve-uri('html2xml.step2.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$step2.log.uri"/></xsl:message>
      <xsl:result-document href="{$step2.log.uri}">
        <xsl:sequence select="$step2"/>
      </xsl:result-document>
    </xsl:if>
    <!--STEP 3-->
    <xsl:variable name="step3" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step2" mode="xfe:tables-fixup">
        </xsl:apply-templates>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="step3.log.uri" select="resolve-uri('html2xml.step3.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$step3.log.uri"/></xsl:message>
      <xsl:result-document href="{$step3.log.uri}">
        <xsl:sequence select="$step3"/>
      </xsl:result-document>
    </xsl:if>
    <!--STEP 4-->
    <xsl:variable name="step4" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step3" mode="xmlFix"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="step4.log.uri" select="resolve-uri('html2xml.step4.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$step4.log.uri"/></xsl:message>
      <xsl:result-document href="{$step4.log.uri}">
        <xsl:sequence select="$step4"/>
      </xsl:result-document>
    </xsl:if>
    <!--STEP5-->
    <xsl:variable name="step5" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step4" mode="html2xml:clean"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="step5.log.uri" select="resolve-uri('html2xml.step5.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$step5.log.uri"/></xsl:message>
      <xsl:result-document href="{$step5.log.uri}">
        <xsl:sequence select="$step5"/>
      </xsl:result-document>
    </xsl:if>
    <!--FINALY-->
    <xsl:choose>
      <xsl:when test="$xfe:wrapWithFictionalEE">
        <xsl:variable name="editorialEntity" as="document-node()">
          <xsl:document>
            <editorialEntity 
              xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
              xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst">
              <metadata/>
              <body>
                <contentNode>
                  <metadata/>
                  <content>
                    <xsl:sequence select="$step5"/>
                  </content>
                </contentNode>
              </body>
              <relations/>
            </editorialEntity>
          </xsl:document>
        </xsl:variable>
        <xsl:if test="$xfe:addXmlModelPi">
          <xsl:processing-instruction name="xml-model">
            <xsl:text>href="xf-models:/xf-models/editorialEntity/editorialEntity.nvdl" type="application/xml" schematypens="http://purl.oclc.org/dsdl/nvdl/ns/structure/1.0"</xsl:text>
          </xsl:processing-instruction>
        </xsl:if>
        <xsl:variable name="editorialEntity.withRelations" as="document-node()">
          <xsl:document>
            <xsl:apply-templates select="$editorialEntity" mode="xfe:makeRelation"/>
          </xsl:document>
        </xsl:variable>
        <xsl:apply-templates select="$editorialEntity.withRelations" mode="html2xml:namespace-declaration"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="$xfe:addXmlModelPi">
          <xsl:processing-instruction name="xml-model">
            <xsl:text>href="</xsl:text>
            <xsl:value-of select="$xfe:srng.uri"/>
            <xsl:text>" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"</xsl:text>
          </xsl:processing-instruction>
        </xsl:if>
        <xsl:apply-templates select="$step5" mode="html2xml:namespace-declaration"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--================================-->
  <!--STEP 1 : HTML2XML-->
  <!--================================-->
  
  <!--Tableau : on les copie pour le moment, ils seront transformé après dans un autre step--> 
  <xsl:template match="table | colgroup | col | thead | tbody | tfoot | tr | td | th" mode="xfe:html2xml_main">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="*" mode="xfe:html2xml_main">
    <xsl:element name="{xfe:getXmlELementName(.)}">
      <xsl:namespace name="html" select="'http://www.w3.org/1999/xhtml'"/>
      <xsl:call-template name="html2xml:makeXmlAttributes"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="ins | del" mode="xfe:html2xml_main">
    <xsl:processing-instruction name="xfe_{local-name(.)}_start">
      <xsl:for-each select="@*">
        <xsl:value-of select="name()"/>
        <xsl:text>="</xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>"</xsl:text>
        <xsl:if test="not(position() = last())">
          <xsl:text> </xsl:text>
        </xsl:if>
      </xsl:for-each>
    </xsl:processing-instruction>
    <xsl:apply-templates mode="#current"/>
    <xsl:processing-instruction name="xfe_{local-name(.)}_end"/>
  </xsl:template>
  
  <xsl:template match="span[els:hasClass(., 'tinyMce_annotation')]" mode="xfe:html2xml_main">
    <xsl:processing-instruction name="xfe_comment">
      <xsl:for-each select="@*">
        <xsl:value-of select="name()"/>
        <xsl:text>="</xsl:text>
        <xsl:value-of select="."/>
        <xsl:text>"</xsl:text>
        <xsl:if test="not(position() = last())">
          <xsl:text> </xsl:text>
        </xsl:if>
      </xsl:for-each>
    </xsl:processing-instruction>
  </xsl:template>
  
  <xsl:template name="html2xml:makeXmlAttributes">
    <xsl:for-each select="@*">
      <xsl:choose>
        <xsl:when test="name(.) = 'id'">
          <!--les @id sont les seuls attributs qu'on conserve entre xml et html-->
          <xsl:copy-of select="."/>
        </xsl:when>
        <xsl:when test="name(.) = 'data-xfe-label'"/>
        <xsl:when test="name(.) = 'href' and html2xml:is-internalLinkHref(.)">
          <xsl:attribute name="xf:href" select="."/>
        </xsl:when>
        <xsl:when test="name(.) = 'href' and html2xml:is-externalLinkHref(.)">
          <xsl:attribute name="xf:targetResId" select="html2xml:normalize-externalLinkHref(.)"/>
        </xsl:when>
        <!--NO => think about http links-->
        <!--<xsl:when test="name(.) = 'href' and not(html2xml:is-externalLinkHref(.))">
          <!-\- empty xf:idRef (lost relation)-\->
          <xsl:attribute name="xf:idRref" select="''"/>
        </xsl:when>-->
        <xsl:when test="starts-with(local-name(.), $xfe:dataXmlAttribute.prefix)">
          <xsl:attribute name="{substring-after(local-name(), $xfe:dataXmlAttribute.prefix)}" select="."/>
        </xsl:when>
        <xsl:when test="starts-with(local-name(.), $xfe:dataXfeAttribute.prefix)">
          <xsl:attribute name="{concat('xf:', substring-after(local-name(), $xfe:dataXfeAttribute.prefix))}" select="."/>
        </xsl:when>
        <xsl:when test="not(contains(name(), ':'))">
          <xsl:attribute name="{concat('html:', local-name(.))}" select="."/>
        </xsl:when>
        <!--<xsl:otherwise/>-->
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>
  
  <!--====================================-->
  <!-- STEP 2 : xhtml2cals -->
  <!--====================================-->
  
  <!--cf. xhtml2cals.xsl-->
  
  <!--====================================-->
  <!-- STEP 3 : tables-fixup -->
  <!--====================================-->
  
  <xsl:template match="*:table" mode="xfe:tables-fixup">
    <xsl:choose>
      <xsl:when test="parent::*:table">
        <xsl:apply-templates select="*" mode="#current"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:copy>
          <xsl:if test=".//*:table">
            <xsl:copy-of select="(.//*:table)[1]/(@frame|@colsep|@rowsep)"/>
          </xsl:if>
          <xsl:apply-templates select="node() | @*" mode="#current"/>
        </xsl:copy>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="xfe:tables-fixup">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--====================================-->
  <!-- STEP 4 : xmlFix -->
  <!--====================================-->
  
  <!--cf. html2xml_modules/uiCommentaire.xsl-->
  
  <!--====================================-->
  <!-- STEP 5 : clean -->
  <!--====================================-->
  
  <xsl:template match="@xfe:* | @html:*" mode="html2xml:clean"/>
  
  <xsl:template match="node() | @*" mode="html2xml:clean">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--====================================-->
  <!-- STEP 5 : xfe:makeRelation -->
  <!--====================================-->
  
  <xsl:template match="*[@xf:targetResId]" mode="xfe:makeRelation">
    <xsl:copy copy-namespaces="no">
      <xsl:apply-templates select="@* except (@xf:targetResId | @xf:idRef | @xf:xf_idRef)" mode="#current"/>
      <xsl:attribute name="xf:idRef" select="html2xml:generate-relation-id(.)"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="xf:relations" mode="xfe:makeRelation">
    <xsl:copy>
      <xsl:apply-templates select="//*[@xf:targetResId]" mode="xfe:makeRelationElement"/>
    </xsl:copy>
  </xsl:template>

  <!--Dans la xsl spécifique du fond, il faudra avoir un template comme suit :-->
  <!--<xsl:template match="*[@xf:targetResId]" mode="xfe:makeRelationElement" priority="1">
    <xsl:next-match>
      <xsl:with-param name="relation.code" select="'...'"/>
      <xsl:with-param name="metadata" as="element()">
        <metadata xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst">...</metadata>
      </xsl:with-param>
      <xsl:with-param name="ref.refType" select="'...'"/>
    </xsl:next-match>
  </xsl:template>-->
  
  <xsl:template match="*[@xf:targetResId]" mode="xfe:makeRelationElement">
    <xsl:param name="relation.code" as="xs:string?"/>
    <xsl:param name="relation.metadata" as="element(xf:metadata)?"/>
    <xsl:param name="relation.ref.refType" as="xs:string?"/>
    <xsl:choose>
      <xsl:when test="count($relation.code) != 1 or count($relation.ref.refType) != 1">
        <xsl:message terminate="yes">[FATAL] Unable to create relation for <xsl:value-of select="els:get-xpath(.)"/>
          Template xfe:makeRelationElement must be called with its 2 non empty parameters (<xsl:value-of select="count($relation.code)"/> $relation.code, <xsl:value-of select="count($relation.ref.refType)"/> $relation.ref.refType).
          Please add an overriding template in your specific XSLT, wich add theses parameters before calling the generic template</xsl:message>
      </xsl:when>
      <xsl:otherwise>
        <relation xf:id="{html2xml:generate-relation-id(.)}" code="{$relation.code}" calculated="true"
          xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst">
          <xsl:choose>
            <xsl:when test="count($relation.metadata) = 1">
              <xsl:copy-of select="$relation.metadata"/>
            </xsl:when>
            <xsl:otherwise>
              <metadata xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"/>
            </xsl:otherwise>
          </xsl:choose>
          <ref xf:targetResId="{@xf:targetResId}" xf:refType="{concat('efl:', local-name())}"> <!--fixme xf:refType est faux-->
            <xsl:choose>
              <xsl:when test="@xf:idRef">
                <xsl:attribute name="idRef" select="@xf:idRef"/>
              </xsl:when>
              <xsl:when test="@xf:xf_idRef">
                <xsl:attribute name="xf:idRef" select="@xf:xf_idRef"/>
              </xsl:when>
            </xsl:choose>
          </ref>
        </relation>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="xfe:makeRelation">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--================================-->
  <!--STEP last : namespace-declaration-->
  <!--================================-->
  
  <!--useFull for xspec essentialy-->
  
  <xsl:template match="/*" mode="html2xml:namespace-declaration">
    <xsl:copy><!--copy-namespaces="yes"-->
      <xsl:namespace name="xf" select="'http://www.lefebvre-sarrut.eu/ns/xmlfirst'"/>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="html2xml:namespace-declaration">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--================================-->
  <!--COMMON-->
  <!--================================-->  
  
  <xsl:function name="html2xml:generate-relation-id" as="xs:string">
    <xsl:param name="e" as="element()"/>
    <xsl:value-of select="concat('relation-', local-name($e), '-', count($e/preceding::*[@xf:targetResId]) + 1)"/>
  </xsl:function>
  
  <!--FIXME : ici il faudrait lire la conf pour vérifier qu'on est bien sur un externalLink-->
  <xsl:function name="html2xml:is-externalLinkHref" as="xs:boolean">
    <xsl:param name="href" as="attribute(href)"/>
    <xsl:choose>
      <xsl:when test="normalize-space($href) = ''">
        <xsl:sequence select="false()"/>
      </xsl:when>
      <xsl:when test="not(contains($href, '#'))">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <xsl:when test="matches($href, '/#/editorialEntity/.+')">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="html2xml:is-internalLinkHref" as="xs:boolean">
    <xsl:param name="href" as="attribute(href)"/>
    <xsl:sequence select="starts-with($href, '#')"/>
  </xsl:function>
  
  <!--ex : <a class="refDoc" data-xfe-label="refDoc" href="http://localhost:3000/#/editorialEntity/4e3d261b-8749-4348-ad6d-c60ff402d3df">blablabla</a>-->
  <xsl:function name="html2xml:normalize-externalLinkHref" as="xs:string">
    <xsl:param name="href" as="attribute(href)"/>
    <xsl:choose>
      <xsl:when test="contains($href, '#/editorialEntity/')">
        <xsl:value-of select="substring-after($href, '#/editorialEntity/')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$href"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
</xsl:stylesheet>
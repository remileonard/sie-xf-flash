<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:saxon="http://saxon.sf.net/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:rng="http://relaxng.org/ns/structure/1.0"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
  xmlns:xslLib="http://www.lefebvre-sarrut.eu/ns/els/xslLib"
  xmlns:cals="-//OASIS//DTD XML Exchange Table Model 19990315//EN"
  xmlns:html="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <xsl:import href="xfe:/xsl/xfe-common.xsl"/>
  <xsl:import href="xslLib:/xslLib/cals2html.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Conv XML vers HTML d'un contentNode pour XFE</xd:p>
    </xd:desc>
  </xd:doc>
  
  <xsl:param name="xfe:contentNodeId" select="''" as="xs:string"/>
  
  <xsl:param name="xfe:addHTML5DOCTYPE" select="true()" as="xs:boolean"/>
  <xsl:param name="xfe:addXmlModelPi" select="true()" as="xs:boolean"/>
  <xsl:param name="xfe:fillHeadTitle" select="false()" as="xs:boolean"/><!--FE-105 : pas de contenu dans le titre-->
  <!--Le doctype xhtml ajoute des méta spécifiques qui ne sont pas reproduite dans xspec (qui ignore vraissemblablement xsl:output)-->
  <xsl:output method="xhtml" indent="no"/>
  
  <!--Le vrai DOCTYPE html5 serait avec 
  method="html"
  version="5.0"
  Mais cela pose des problème de balise non fermée (http://markmail.org/message/jaajiozvbbhqeipz)
  => cf. ajout manuel du DOCTYPE HTML5
  -->
  
  <!--================================-->
  <!--INIT-->
  <!--================================-->
  
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfe:xml2html"/>
  </xsl:template>
  
  <!--================================-->
  <!--MAIN-->
  <!--================================-->
  
  <xsl:template match="/" mode="xfe:xml2html" priority="1">
    <xsl:variable name="contentNodes" select=".//xf:contentNode" as="element()*"/>
    <xsl:choose>
      <xsl:when test="not(xf:editorialEntity)">
        <xsl:message terminate="no">[ERROR][XFE] Document must be an editorialEntity, but we continue for test</xsl:message>
        <xsl:next-match/>
      </xsl:when>
      <xsl:when test="count($contentNodes) = 0">
        <xsl:message terminate="yes">[FATAL][XFE] No contentNode found in this EE</xsl:message>
      </xsl:when>
      <xsl:when test="count($contentNodes) = 1">
        <xsl:apply-templates select="$contentNodes[1]/xf:content/*" mode="#current"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="contentNode" select="$contentNodes[@xf:id = $xfe:contentNodeId]" as="element()?"/>
        <xsl:choose>
          <xsl:when test="count($contentNode) = 0">
            <xsl:message terminate="yes">[FATAL][XFE] No contentNode with id="<xsl:value-of select="$xfe:contentNodeId"/>" found in this EE</xsl:message>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="$contentNode/xf:content/*" mode="#current"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--On garde la possibilité d'avoir un contentNode en entrée (/) pour que les sample continuent de fonctionner, même si ce n'est pas la cible dans l'ECM (on aura toujours une EE en entrée)-->
  <!--Cela permets de travailler ici sur des sample valides selon le modèle srng de la conf-->
  <xsl:template match="/ | xf:content/*" mode="xfe:xml2html">
    <xsl:message>[INFO] xfe:conf.uri=<xsl:value-of select="$xfe:conf.uri"/></xsl:message>
    <xsl:message>[INFO] xfe:debug=<xsl:value-of select="$xfe:debug"/><xsl:if test="not($xfe:debug)"> (les log de la transformation ne seront pas écrits sur le filesytem)</xsl:if></xsl:message>
    <xsl:variable name="root" as="document-node()">
      <xsl:document>
        <xsl:sequence select="."/>
      </xsl:document>
    </xsl:variable>
    <xsl:call-template name="xfe:log.normalizeConf"/>
    <xsl:variable name="xfe:srng.name" select="els:getFileName($xfe:conf.normalized/*:snrgXmlmodel/@href, false())" as="xs:string"/>
    <!--STEP 0-->
    <xsl:variable name="xfe:xml2html_resolveRelation" as="document-node()">
      <xsl:document>
        <xsl:choose>
          <xsl:when test="parent::xf:content">
            <xsl:apply-templates select="." mode="xfe:xml2html_resolveRelation"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="xfe:xml2html_resolveRelation.log.uri" select="resolve-uri('XML2HTML.step0_xml2html_resolveRelation.log.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$xfe:xml2html_resolveRelation.log.uri"/></xsl:message>
      <xsl:result-document href="{$xfe:xml2html_resolveRelation.log.uri}">
        <xsl:sequence select="$xfe:xml2html_resolveRelation"/>
      </xsl:result-document>
    </xsl:if>
    <!--STEP 1-->
    <xsl:variable name="xfe:xml2html_main" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$xfe:xml2html_resolveRelation" mode="xfe:xml2html_main"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="xfe:xml2html_main.log.uri" select="resolve-uri('XML2HTML.step1_xml2html_main.log.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$xfe:xml2html_main.log.uri"/></xsl:message>
      <xsl:result-document href="{$xfe:xml2html_main.log.uri}">
        <xsl:sequence select="$xfe:xml2html_main"/>
      </xsl:result-document>
    </xsl:if>
    <!--STEP 2-->
    <xsl:variable name="xfe:xml2html_cals2html" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$xfe:xml2html_main" mode="xslLib:cals2html"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="xfe:xml2html_cals2html.log.uri" select="resolve-uri('XML2HTML.step2_cals2html.log.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$xfe:xml2html_cals2html.log.uri"/></xsl:message>
      <xsl:result-document href="{$xfe:xml2html_cals2html.log.uri}">
        <xsl:sequence select="$xfe:xml2html_cals2html"/>
      </xsl:result-document>
    </xsl:if>
    <!--STEP 3-->
    <xsl:variable name="xfe:xml2html_moveInlineNotes" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$xfe:xml2html_cals2html" mode="xfe:xml2html_step3_moveInlineNotes"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="xfe:xml2html_moveInlineNotes.log.uri" select="resolve-uri('XML2HTML.step3_moveInlineNotes.log.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$xfe:xml2html_moveInlineNotes.log.uri"/></xsl:message>
      <xsl:result-document href="{$xfe:xml2html_moveInlineNotes.log.uri}">
        <xsl:sequence select="$xfe:xml2html_moveInlineNotes"/>
      </xsl:result-document>
    </xsl:if>
    <!--STEP 4-->
    <xsl:variable name="xfe:xml2html_addCalsStyle" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$xfe:xml2html_moveInlineNotes" mode="xslLib:xml2html_addCalsStyle"/>
      </xsl:document>
    </xsl:variable>
    <xsl:if test="$xfe:debug">
      <xsl:variable name="xfe:xml2html_addCalsStyle.log.uri" select="resolve-uri('XML2HTML.step4_addCalsStyle.log.xml', $log.uri)" as="xs:anyURI"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$xfe:xml2html_addCalsStyle.log.uri"/></xsl:message>
      <xsl:result-document href="{$xfe:xml2html_addCalsStyle.log.uri}">
        <xsl:sequence select="$xfe:xml2html_addCalsStyle"/>
      </xsl:result-document>
    </xsl:if>
    <!--FINALY-->
    <xsl:if test="$xfe:addXmlModelPi">
      <xsl:processing-instruction name="xml-model">
        <xsl:text>href="</xsl:text>
        <!--FIXME : voir comment calculer une URI relative ici (cf. https://groups.google.com/d/msg/els-xdev-xmltecho/hl-zJ2WSLZA/tUNsjF_YBgAJ)-->
        <!--=> Utiliser cp à la place ! ou pdu ?-->
        <xsl:value-of select="concat(els:getFolderPath($xfe:srng.uri), '/', $xfe:srng.name, '.html.rng')"/>
        <xsl:text>" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"</xsl:text>
      </xsl:processing-instruction>
    </xsl:if>
    <!--Puisqu'on veut éviter des problème de balise non fermées à la sérialisation en local, 
            on ajoute ce doctype pour html5 avec une vielle méthode-->
    <xsl:if test="$xfe:addHTML5DOCTYPE">
      <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html></xsl:text>
    </xsl:if>
    <html>
      <head>
        <!--<meta charset="UTF-8"/> FIXME : pour html5-->
        <title>
          <xsl:if test="$xfe:fillHeadTitle">
            <xsl:value-of select="els:evaluate-xpath($xfe:conf.normalized/*:title/@xpath, $root/*)"/>
          </xsl:if>
        </title>
        <link type="text/css" rel="Stylesheet" href="{concat($xfe:srng.name, '.css')}" />
        <!--FIXME : impossible de distinguer les tableaux cals ou html natif, de toute façon pour l'édition mieux vaut ne pas mettre de css ici 
        mais tout mettre en attribut (@style si besoin), servira notamment à la rétro-conv HTML2XML-->
        <!--<link type="text/css" rel="Stylesheet" href="../../cals2html.css"/>-->
        <!--<xsl:result-document href="{resolve-uri('cals2html.css', base-uri(.))}" format="els:text">
          <xsl:copy-of select="unparsed-text('xslLib:/cals2html.css', 'utf-8')"/>
        </xsl:result-document>-->
      </head>
      <xsl:sequence select="$xfe:xml2html_addCalsStyle"/>
    </html>
  </xsl:template>

  <!--================================================================-->
  <!-- STEP 0 : resolve relation-->
  <!--================================================================-->
  
  <!--Replace the xf:idRef target by the one targeted in the relation-->
  <xsl:template match="xf:content//*[@xf:idRef[count(key('getRelationByXfId', .)) = 1]]/@xf:idRef" mode="xfe:xml2html_resolveRelation">
    <xsl:variable name="relation" select="key('getRelationByXfId', .)" as="element(xf:relation)"/>
    <xsl:choose>
      <xsl:when test="$relation/xf:ref/@xf:targetResId">
        <xsl:attribute name="html:href" select="$relation/xf:ref/@xf:targetResId"/>
        <xsl:if test="$relation/xf:ref/@idRef">
          <xsl:attribute name="{concat('html:', $xfe:dataAttribute.prefix, 'xfe-idRef')}" select="$relation/xf:ref/@idRef"/>
        </xsl:if>
        <xsl:if test="$relation/xf:ref/@xf:idRef">
          <xsl:attribute name="{concat('html:', $xfe:dataAttribute.prefix, 'xfe-xf_idRef')}" select="$relation/xf:ref/@xf:idRef"/>
        </xsl:if>
      </xsl:when>
      <xsl:otherwise>
        <!--temp : ancien schéma-->
        <xsl:attribute name="html:href" select="$relation/xf:ref/@xf:idRef"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--Make an empty href is the relation is not resolved-->
  <xsl:template match="xf:content//*[@xf:idRef[count(key('getRelationByXfId', .)) != 1]]/@xf:idRef" mode="xfe:xml2html_resolveRelation">
    <xsl:message>[WARNING] <xsl:value-of select="count(key('getRelationByXfId', .))"/> relation(s) found for xf:idRef="<xsl:value-of select="."/>". The link will not be resolved</xsl:message>
    <xsl:attribute name="html:href" select="''"/>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="xfe:xml2html_resolveRelation">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--================================================================-->
  <!-- STEP 1 : mode step1_xml2html avec gestion tableaux-->
  <!--================================================================-->
  
  <!--On ajoute le body ici car on va avoir besoin d'une racine pour ajouter le bloc xfe_notes sous le body-->
  <!--Si la racine n'est pas unique, on ajoute le body, la racine du xml deviendra un div (cf. ci-après), sinon c'est la racine xml qui fera office de body-->
  <xsl:template match="/*[not($xfe:srng.grammar.rootIsSingle)]" mode="xfe:xml2html_main" priority="1">
    <body>
      <xsl:next-match/>
    </body>
  </xsl:template>
  
  <!--+ passage en mode addCalsPrefix ou keepAsHtml--> 
  
  <xsl:template match="*[xfe:getConfMappingElementFromXML(.)/@xml-format = 'cals:table']" mode="xfe:xml2html_main">
    <xsl:apply-templates select="." mode="xfe:xml2html_addCalsPrefix"/>
  </xsl:template>
  
  <xsl:template match="*[xfe:getConfMappingElementFromXML(.)/@xml-format = 'html:table']" mode="xfe:xml2html_main">
    <xsl:apply-templates select="." mode="xfe:xml2html_keepAsHtml"/>
  </xsl:template>
  
  <!--=================================-->
  <!--Mode addCalsPrefix-->
  <!--=================================-->
  
  <xsl:template 
    match="*:table | *:tgroup | *:colspec | *:spanspec | *:thead | *:tbody | *:tfoot | *:row 
    | *:TABLE | *:TGROUP | *:COLSPEC | *:SPANSPEC | *:THEAD | *:TBODY | *:TFOOT | *:ROW 
    | *:Table | *:Tgroup | *:Colspec | *:Spanspec | *:Thead | *:Tbody | *:Tfoot | *:Row" 
         mode="xfe:xml2html_addCalsPrefix">
    <!--table/titre-->
    <xsl:element name="cals:{lower-case(local-name())}">
      <xsl:for-each select="@*">
        <xsl:attribute name="{lower-case(name(.))}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>
      <!--<xsl:copy-of select="@*"/>-->
      <xsl:apply-templates mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <!--titre de tableau cals : on repasse dans le mode main-->
  <xsl:template match="*:table/*:titre | *:TABLE/*:TITRE | *:Table/*:Titre " mode="xfe:xml2html_addCalsPrefix">
    <xsl:apply-templates select="." mode="xfe:xml2html_main"/>
  </xsl:template>
  
  <!--Pour les autres éléments qui ne seraient pas du cals on repasse dans le mode step1-->
  <xsl:template match="*" mode="xfe:xml2html_addCalsPrefix">
    <xsl:apply-templates select="." mode="xfe:xml2html_main"/>
  </xsl:template>
  
  <xsl:template match="*:entry | *:ENTRY | *:Entry" mode="xfe:xml2html_addCalsPrefix">
    <xsl:element name="cals:{lower-case(local-name())}">
      <xsl:for-each select="@*">
        <xsl:attribute name="{lower-case(name(.))}">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </xsl:for-each>
      <!--On repasse par le mode normal-->
      <xsl:apply-templates mode="xfe:xml2html_main"/>
    </xsl:element>
  </xsl:template>
  
  <!--=================================-->
  <!--Mode keepAsHtml-->
  <!--=================================-->
  
  <xsl:template match="*:table | *:table//* | *:TABLE | *:TABLE//*" mode="xfe:xml2html_keepAsHtml">
    <!--table/titre-->
    <xsl:element name="{lower-case(local-name())}">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="*:td | *:TD | *:th | *:TH | *:caption | *:CAPTION" mode="xfe:xml2html_keepAsHtml" priority="1">
    <xsl:element name="{lower-case(local-name())}">
      <xsl:copy-of select="@*"/>
      <!--On repasse par le mode normal-->
      <xsl:apply-templates mode="xfe:xml2html_main"/>
    </xsl:element>
  </xsl:template>
  
  <!--=================================-->
  <!-- Mode xfe:xml2html_main default -->
  <!--=================================-->
  
  <!--<xsl:template match="e">
    <xsl:variable name="e.dataModel" select="rng:getSRNGdataModelFromXmlElement(., $srng.grammar)" as="element(rng:element)"/>
    <xsl:variable name="e.parent.dataModel" select="rng:getSRNGdataModelFromXmlElement(./parent::*, $srng.grammar)" as="element(rng:element)"/>
    <parentDataModel>
      <xsl:copy-of select="$e.parent.dataModel"/>
    </parentDataModel>
    <DataModel>
      <xsl:copy-of select="$e.dataModel"/>
    </DataModel>
    <xsl:variable name="ref" select="$e.parent.dataModel//rng:ref[rng:getDefine(.)/rng:element is $e.dataModel]" as="element(rng:ref)"/>
    <inline><xsl:value-of select="exists($ref/ancestor::rng:interleave/rng:text)"/></inline>
    <isInline><xsl:value-of select="rng:isInline($e.dataModel, $e.parent.dataModel)"/></isInline>
    <!-\-<xsl:message><xsl:value-of select="local-name()"/> : <xsl:value-of select="xf:getHtmlElementName(.)"/></xsl:message>-\->
  </xsl:template>-->
  
  <!--TODO : c'est probablement trop gourmand de parcourir tout le schéma à chaque élément xml pour trouver son dataModel, 
  Idée => le faire de proche en proche en passant le dataModel de chaque élément au suivant (param), comme ça on sait où on en était juste avant.-->
  <xsl:template match="*" mode="xfe:xml2html_main">
    <xsl:variable name="self" select="self::*" as="element()"/>
    <xsl:variable name="confMappingElement" select="xfe:getConfMappingElementFromXML(.)" as="element()?"/>
    <xsl:variable name="isXMLrootElement" select="not(ancestor::*)" as="xs:boolean"/>
    <xsl:if test="$confMappingElement/@error">
      <xsl:message terminate="no"><xsl:value-of select="$confMappingElement/@error"/> (<xsl:value-of select="els:get-xpath(.)"/>)</xsl:message>
    </xsl:if>
    <xsl:variable name="isVoid" select="normalize-space($confMappingElement/@html-name) = 'void'" as="xs:boolean"/>
    <xsl:variable name="htmlElementName" as="xs:string?">
      <xsl:choose>
        <xsl:when test="$isVoid">
          <!--ne pas sortir d'élément ici-->
        </xsl:when>
        <!--l'élément est surchargé dans la conf-->
        <xsl:when test="normalize-space($confMappingElement/@html-name) != ''">
          <xsl:value-of select="$confMappingElement/@html-name"/>
        </xsl:when>
        <!--La racine sera toujours un div ou un body -->
        <xsl:when test="$isXMLrootElement">
          <xsl:choose>
            <xsl:when test="$xfe:srng.grammar.rootIsSingle">
              <xsl:text>body</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>div</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <!--default inline or block ? we need to check the element data-model in srng schema-->
          <xsl:variable name="e.dataModel" select="rng:getSRNGdataModelFromXmlElement(., $xfe:srng.grammar)" as="element(rng:element)"/>
          <xsl:variable name="e.parent.dataModel" select="rng:getSRNGdataModelFromXmlElement(parent::*, $xfe:srng.grammar)" as="element(rng:element)"/>
          <!--<xsl:value-of select="els:displayNode($e.dataModel/parent::rng:define)"/>-->
          <xsl:choose>
            <xsl:when test="rng:isInline($e.dataModel, $e.parent.dataModel)">
              <xsl:text>span</xsl:text>
            </xsl:when>
            <xsl:otherwise>
              <xsl:text>div</xsl:text>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$isVoid">
        <xsl:apply-templates mode="#current"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:element name="{$htmlElementName}">
          <xsl:variable name="xmlNameAsClass" select="not(matches($confMappingElement/@html-xmlNameAsClass, '(false|0)'))" as="xs:boolean"/>
          <xsl:variable name="class" as="xs:string*">
            <xsl:if test="$xmlNameAsClass">
              <xsl:value-of select="local-name()"/>
            </xsl:if>
            <xsl:if test="$confMappingElement/@html-addClass != ''">
              <xsl:value-of select="$confMappingElement/@html-addClass"/>
            </xsl:if>
          </xsl:variable>
          <!--<xsl:attribute name="{$dataAttribute.prefix}xfe-label" ... Error with saxon9.7, cf. http://markmail.org/thread/xoajcp6uemrmkp6q -->
          <xsl:attribute name="{concat($xfe:dataAttribute.prefix, 'xfe-label')}" select="($confMappingElement/@label, local-name(.))[1]"/>
          <xsl:if test="string-join($class, ' ') != ''">
            <xsl:attribute name="class" select="string-join($class, ' ')"/>
          </xsl:if>
          <xsl:if test="starts-with($confMappingElement/@type, 'xfe:list(')">
            <xsl:choose>
              <xsl:when test="matches($confMappingElement/@type, 'xfe:list\(.*?\)')">
                <xsl:variable name="listTypeValue.xpath" select="replace($confMappingElement/@type, '^xfe:list\((.*?)\)$', '$1')" as="xs:string"/>
                <xsl:attribute name="{concat($xfe:dataAttribute.prefix, 'xfe-listType')}">
                  <!--<xsl:message>evaluate(<xsl:value-of select="$listTypeValue.xpath"/>, .)=<xsl:value-of select="els:evaluate-xpath($listTypeValue.xpath, .)"/></xsl:message>-->
                  <xsl:value-of select="els:evaluate-xpath($listTypeValue.xpath, .)"/>
                </xsl:attribute>
              </xsl:when>
              <xsl:otherwise>
                <xsl:message terminate="yes">[ERROR] @type xfe:list doit être de la forme xfe:list(expressionXpath)</xsl:message>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
          <xsl:variable name="htmlAttributes" select="$confMappingElement/@*[starts-with(local-name(.), 'html-attribute-')]" as="attribute()*"/>
          <xsl:if test="count($htmlAttributes) != 0">
            <xsl:for-each select="$htmlAttributes">
              <xsl:attribute name="{substring-after(local-name(.), 'html-attribute-')}" select="els:evaluate-xpath(., $self)"/>
            </xsl:for-each>
          </xsl:if>
          <xsl:choose>
            <!--FIXME : ce choose pourrait être mis plus haut mais il faudra gérer les impact sur le html.rng et sur la css, pour l'instant on laisse ici-->
            <!--Common case :-->
            <xsl:when test="not($isXMLrootElement and $xfe:srng.grammar.rootIsSingle)">
              <xsl:for-each select="@* except (@xf:*|@html:*|(@id|@ID|@Id)[1])">
                <xsl:if test="not(name() = tokenize($confMappingElement/@html-ignoreXmlAttributes, '\s+'))">
                  <xsl:attribute name="{concat($xfe:dataXmlAttribute.prefix, local-name(.))}" select="." />
                </xsl:if>
              </xsl:for-each>
              <!--First adding an empty href for external links-->
              <xsl:if test="starts-with($confMappingElement/@type, 'xfe:externalLinkType') and $htmlElementName = 'a'">
                <xsl:attribute name="href" select="''"/>
              </xsl:if>
              <!--This href will be override by the resolved relation (in case it has one)-->
              <xsl:for-each select="@html:*">
                <xsl:attribute name="{local-name(.)}" select="."/>
              </xsl:for-each>
              <!--<xsl:for-each select="@xf:refType">
                <xsl:attribute name="{concat($dataXmlAttribute.prefix, 'xf_', local-name(.))}" select="."/>
              </xsl:for-each>-->
            </xsl:when>
            <!--From here we are on the single root element-->
            <xsl:otherwise>
              <xsl:if test="@*">
                <xsl:message>[ERROR] the root element of a single root grammar should not have attributes because the editor will not deal with them</xsl:message>
              </xsl:if>
              <xsl:if test="count($confMappingElement)">
                <xsl:message>[ERROR] Conf mapping element on the root element of a single root grammar will be ignored</xsl:message>
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose>
          <xsl:if test="@id | @ID | @Id">
            <xsl:attribute name="id" select="(@id | @ID | @Id)[1]"/>
          </xsl:if>
          <xsl:if test="normalize-space($confMappingElement/@type) = 'xfe:inlineNote'">
            <xsl:attribute name="xfe_type" select="$confMappingElement/@type"/>
          </xsl:if>
          <xsl:call-template name="xfe:revisionPiTohtml"/>
          <!--<xsl:apply-templates mode="#current"/>-->
        </xsl:element>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template name="xfe:revisionPiTohtml">
    <xsl:for-each-group select="node()" group-starting-with="processing-instruction('xfe_del_start')">
      <xsl:choose>
        <xsl:when test="current-group()[1]/self::processing-instruction('xfe_del_start')">
          <xsl:variable name="piStart" select="current-group()[1]" as="processing-instruction()"/>
          <xsl:for-each-group select="current-group()" group-ending-with="processing-instruction('xfe_del_end')">
            <xsl:choose>
              <xsl:when test="current-group()[last()]/self::processing-instruction('xfe_del_end')">
                <del>
                  <xsl:copy-of select="els:pseudoAttributes2xml($piStart, $els:dquot)"/>
                  <xsl:apply-templates select="current-group()" mode="xfe:xml2html_main"/>
                </del>
              </xsl:when>
              <xsl:otherwise>
                <xsl:call-template name="xfe:insPiTohtml"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each-group>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="xfe:insPiTohtml"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each-group>
  </xsl:template>
  
  <xsl:template name="xfe:insPiTohtml">
    <xsl:for-each-group select="current-group()" group-starting-with="processing-instruction('xfe_ins_start')">
      <xsl:choose>
        <xsl:when test="current-group()[1]/self::processing-instruction('xfe_ins_start')">
          <xsl:variable name="piStart" select="current-group()[1]" as="processing-instruction()"/>
          <xsl:for-each-group select="current-group()" group-ending-with="processing-instruction('xfe_ins_end')">
            <xsl:choose>
              <xsl:when test="current-group()[last()]/self::processing-instruction('xfe_ins_end')">
                <ins>
                  <xsl:copy-of select="els:pseudoAttributes2xml($piStart, $els:dquot)"/>
                  <xsl:apply-templates select="current-group()" mode="xfe:xml2html_main"/>
                </ins>
              </xsl:when>
              <xsl:otherwise>
                <xsl:apply-templates select="current-group()" mode="xfe:xml2html_main"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each-group>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="current-group()" mode="xfe:xml2html_main"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each-group>
  </xsl:template>
  
  <xsl:template match="processing-instruction('xfe_comment')" mode="xfe:xml2html_main">
    <span class="tinyMce_annotation">
      <xsl:copy-of select="els:pseudoAttributes2xml(., $els:dquot)"/>
    </span>
  </xsl:template>
  
  <!--================================================================-->
  <!-- STEP 2 : xslLib:cals2html -->
  <!--================================================================-->
  <!--cf. xslLib:/cals2html.xsl -->
  
  <!--================================================================-->
  <!-- STEP 3 : moveInlineNotes -->
  <!--================================================================-->
  <!--suppression du contenu de la note en ligne qui est mise dans un div "xfe_notes" plus bas-->
  
  <!--On vide la note en line de son contenu-->
  <xsl:template match="*[@xfe_type = 'xfe:inlineNote']" mode="xfe:xml2html_step3_moveInlineNotes">
    <xsl:copy>
      <xsl:apply-templates select="@* except @xfe_type" mode="#current"/>
      <xsl:attribute name="href" select="concat('#' , xfe:generateInlineNoteId(.))"/>
    </xsl:copy>
  </xsl:template>
  
  <!--Ajout d'un conteneur pour les notes après la racine-->
  <xsl:template match="/html:body[$xfe:conf.normalized//*[@type = 'xfe:inlineNote']]" mode="xfe:xml2html_step3_moveInlineNotes">
    <xsl:message>[INFO] Adding block "xfe_notes" inside html body</xsl:message>
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
      <div class="xfe_notes">
        <xsl:apply-templates select="//*[@xfe_type = 'xfe:inlineNote']" mode="step3_copyInlineNoteContent"/>
      </div>
    </xsl:copy>
  </xsl:template>
  
  <!--Déplacement des notes dans ce conteneur-->
  <xsl:template match="*[@xfe_type = 'xfe:inlineNote']" mode="step3_copyInlineNoteContent">
    <div class="xfe_note" id="{xfe:generateInlineNoteId(.)}">
      <xsl:apply-templates select="node()" mode="#current"/>
    </div>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="xfe:xml2html_step3_moveInlineNotes step3_copyInlineNoteContent">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:function name="xfe:generateInlineNoteId" as="xs:string">
    <xsl:param name="noteRef" as="element()"/>
    <xsl:sequence select="concat('xfe-note_', count($noteRef/preceding::*[@xfe_type = 'xfe:inlineNote']) + 1)"/>
  </xsl:function>
  
  <!--================================================================-->
  <!-- STEP 4 : xml2html_addCalsStyle -->
  <!--================================================================-->
  <!-- cf xslLib:/xslLib/cals2html.xsl -->
  
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:rng="http://relaxng.org/ns/structure/1.0"
  xmlns:rng2srng="http://relaxng.org/ns/rng2srng"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
  xmlns="http://relaxng.org/ns/structure/1.0"
  xpath-default-namespace="http://relaxng.org/ns/structure/1.0"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <xsl:import href="xslLib:/xslLib/rng2srng.xsl"/>
  <xsl:import href="xslLib:/xslLib/rng-common.xsl"/>

  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>"Optimisation" du html.rng pour les besoin de l'éditeur (tinyMCE). 
        L'idée est de n'avoir qu'un seul élément par define (comme SRNG): </xd:p>
      <xd:p> le html.rng est lui même issue d'une conversion "xmlSrng2htmlRng", c'est donc quasi du RNG à 3 exceptions près :</xd:p>
      <xd:ul>
        <xd:li>le début du fichier HTML (html, head, body) écrit en RNG</xd:li>
        <xd:li>les void : où on supprime l'élément dans le define</xd:li>
        <xd:li>les choose : lorsque la conf désigne plusieurs définition différentes pour la même définition XML (ex : les titres)</xd:li>
      </xd:ul>
      <xd:p>On applique donc ici le step 7.20 de la simplification RNG</xd:p>
      <xd:pre>
        "In this rule, the grammar is transformed so that every element element is the child of a define element, and the child of every define element is an element element."
      </xd:pre>
    </xd:desc>
  </xd:doc>
  
  <!-- =================================================================== -->
  <!--                           MAIN                                      -->
  <!-- =================================================================== -->
  
  <xsl:template match="/">
    <!--Application du mode rng2srng:step7.20-->
    <xsl:variable name="rng2srng_step7.20" as="document-node()">
      <xsl:document>
        <xsl:apply-templates mode="rng2srng:step7.20"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="mergeIdenticalDefine" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$rng2srng_step7.20" mode="rng:mergeIdenticalDefine"/>
      </xsl:document>
    </xsl:variable>
    <!--Application reorder-->
    <xsl:apply-templates select="$mergeIdenticalDefine" mode="rng:reorder">
      <xsl:with-param name="rng:reorder_addRootNamespaces" as="node()*">
        <xsl:namespace name="xfe">http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor</xsl:namespace>
        <xsl:namespace name="rng">http://relaxng.org/ns/structure/1.0</xsl:namespace>
        <xsl:namespace name="rng2srng">http://relaxng.org/ns/rng2srng</xsl:namespace>
      </xsl:with-param>
    </xsl:apply-templates>
  </xsl:template>
  
</xsl:stylesheet>
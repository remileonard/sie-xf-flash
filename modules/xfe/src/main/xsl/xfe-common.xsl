<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:rng="http://relaxng.org/ns/structure/1.0"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
  xmlns:html="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <xsl:import href="xslLib:/xslLib/rng-common.xsl"/>
  
  <xsl:key name="getRelationByXfId" match="xf:relation" use="@xf:id"/>
  
  <!--FE-68 : This debug parameter can be used to desactivte logging file to file system while evaluating a variable : 
  XTDE1480: Cannot execute xsl:result-document while evaluating variable-->
  <xsl:param name="xfe:debug" select="true()" as="xs:boolean"/>
  
  <!--Par défaut les log sont écris à côté du xml-->
  <xsl:param name="LOG.URI" select="resolve-uri('log', base-uri(.))" as="xs:string"/>
  <xsl:variable name="log.uri" select="if(ends-with($LOG.URI, '/')) then ($LOG.URI) else(concat($LOG.URI, '/'))" as="xs:string"/>
  
  <xsl:param name="xfe:conf.uri" select="resolve-uri('xfe-conf.xml', base-uri(/))" as="xs:string"/>
  
  <xsl:variable name="xfe:conf" select="document($xfe:conf.uri)/*" as="element()"/>
  
  <xsl:variable name="xfe:conf.normalized" as="element()">
    <xsl:variable name="conf.normalized.step1" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$xfe:conf" mode="normalizeConf.step1"/>
      </xsl:document>
    </xsl:variable>
    <xsl:apply-templates select="$conf.normalized.step1" mode="normalizeConf.step2"/>
  </xsl:variable>
  
  <xsl:variable name="xfe:srng.href" select="$xfe:conf/*:snrgXmlmodel/@href" as="xs:anyURI"/>
  <xsl:variable name="xfe:srng.uri" select="resolve-uri($xfe:srng.href, $xfe:conf.uri)" as="xs:anyURI"/>
  <xsl:variable name="xfe:srng.grammar" select="document($xfe:srng.uri)/rng:grammar" as="element(rng:grammar)"/>
  <xsl:variable name="xfe:srng.grammar.rootIsSingle" select="rng:rootIsSingle($xfe:srng.grammar)" as="xs:boolean"/>
  <xsl:variable name="xfe:dataAttribute.prefix" select="'data-'" as="xs:string"/>
  <xsl:variable name="xfe:dataXmlAttribute.prefix" select="concat($xfe:dataAttribute.prefix, 'xml-')" as="xs:string"/>
  <xsl:variable name="xfe:dataXfeAttribute.prefix" select="concat($xfe:dataAttribute.prefix, 'xfe-')" as="xs:string"/>
  
  <!--=================================================-->
  <!--MODE normalizeConf-->
  <!--=================================================-->


  <!--===== STEP 1 =====-->
  <!--Gestion des @type-->
  
  <xsl:template match="*:element[@xml-format = 'cals:table']" mode="normalizeConf.step1">
    <xsl:variable name="element.name" select="tokenize(@xpath, '/')[last()]" as="xs:string"/>
    <xsl:variable name="nameCase" select="xfe:getTableCasePattern($element.name)" as="xs:string"/>
    <!--on estime que la casse sera la même sur tous les éléments du langage cals-->
    <!--<xsl:copy-of select="@xpath-filter"/> FIXME : ne s'applique pas au même nœud-->
    <xsl:next-match/>
    <element xpath="{@xpath}/{xfe:apply-case('tgroup', $nameCase)}"  html-name="table" xml-format="cals:tgroup" html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('colspec', $nameCase)}" html-name="col"   xml-format="cals:colspec" html-xmlNameAsClass="false"/>
    <!--<element xpath="{@xpath}/{xfe:conditional-upper-case('spanspec', $nameCase)}" html-name="?"/>-->
    <element xpath="{@xpath}/{xfe:apply-case('thead', $nameCase)}"   html-name="thead" xml-format="cals:thead" html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('tbody', $nameCase)}"   html-name="tbody" xml-format="cals:tbody" html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('tfoot', $nameCase)}"   html-name="tfoot" xml-format="cals:tfoot" html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('row', $nameCase)}"     html-name="tr"    xml-format="cals:row" html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('entry', $nameCase)}"   html-name="td"    xml-format="cals:entry" html-xmlNameAsClass="false"/>
    <!--fixme : pas de th car cals2Html n'en fait pas-->
  </xsl:template>
  
  <xsl:template match="*:element[@xml-format = 'html:table']" mode="normalizeConf.step1">
    <xsl:variable name="element.name" select="tokenize(@xpath, '/')[last()]" as="xs:string"/>
    <xsl:variable name="nameCase" select="xfe:getTableCasePattern($element.name)" as="xs:string"/>
    <!--on estime que la casse sera la même sur tous les éléments du langage cals-->
    <!--<xsl:copy-of select="@xpath-filter"/> FIXME : ne s'applique pas au même nœud-->
    <xsl:copy>
      <xsl:attribute name="html-name" select="'table'"/>
      <xsl:attribute name="html-xmlNameAsClass" select="'false'"/>
      <xsl:copy-of select="@*"/>
    </xsl:copy>
    <element xpath="{@xpath}/{xfe:apply-case('caption', $nameCase)}" html-name="caption" xml-format="html:caption" html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('col', $nameCase)}"     html-name="col"     xml-format="html:col"     html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('colgroup', $nameCase)}"     html-name="colgroup"     xml-format="html:colgroup"     html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('thead', $nameCase)}"   html-name="thead"   xml-format="html:thead"   html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('tbody', $nameCase)}"   html-name="tbody"   xml-format="html:tbody"   html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('tfoot', $nameCase)}"   html-name="tfoot"   xml-format="html:tfoot"   html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('tr', $nameCase)}"      html-name="tr"      xml-format="html:tr"      html-xmlNameAsClass="false"/>
    <!--FIXME : attention le xpath table/td devient faux... et pourtant ça marche ???-->
    <element xpath="{@xpath}/{xfe:apply-case('td', $nameCase)}"      html-name="td"      xml-format="html:td"      html-xmlNameAsClass="false"/>
    <element xpath="{@xpath}/{xfe:apply-case('th', $nameCase)}"      html-name="th"      xml-format="html:th"      html-xmlNameAsClass="false"/>
  </xsl:template>
  
  <xsl:template match="*:element[starts-with(@type, 'xfe:title-')]" mode="normalizeConf.step1">
    <xsl:variable name="level" select="substring-after(@type, 'xfe:title-')" as="xs:string"/>
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <!--on force certains attributs-->
      <!--cf. schematron conf-->
      <xsl:attribute name="html-addClass" select="normalize-space(concat('xfe_title-', $level, ' ', @html-addClass))"/>
      <!--<xsl:attribute name="html-xmlNameAsClass" select="'false'"/> non car on pourrait mélanger avoir plusieurs type de titre, non bijection du xfe:title-X avec le nom de l'élément?-->
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="*:element[starts-with(@type, 'xfe:externalLinkType')]" mode="normalizeConf.step1">
    <xsl:copy>
      <!--<xsl:copy-of select="@*"/>-->
      <!--on force certains attributs-->
      <xsl:attribute name="html-name" select="'a'"/>
      <!--FIXME : pour éviter trop d'impacts, on conserve html-name si spécifié-->
      <xsl:copy-of select="@*"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="*:element[@type = 'xfe:inlineNote']" mode="normalizeConf.step1">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <!--on force certains attributs-->
      <!--cf. schematron conf-->
      <xsl:attribute name="html-name" select="'a'"/>
      <xsl:attribute name="html-attribute-href">
        <xsl:text>''</xsl:text>
        <!--valeur sera générée dans XML2HTML (pour avoir des generate-id() homogènes), mais le modèle a besoin qu'on définisse la surcharge ici-->
      </xsl:attribute>
      <xsl:attribute name="html-addClass" select="normalize-space(concat('xfe_noteRef', ' ', @html-addClass))"/>
      <!--<xsl:attribute name="html-xmlNameAsClass" select="'false'"/> non car on pourrait mélanger les note inline et les autres à la reconstruction du XML-->
    </xsl:copy>
  </xsl:template>
  
  <!--FIXME ! ATTENTION le fait de remplacer les attribut id et idRef par des nouveaux attributs change leur type (et donc ceux des autres éléments html du même nom adjacents)
  => il faudrait plutôt avoir une instruction pour changer le nom d'un attribut :
  <attribute xpath="toto/@foo" html-name="bar"/>
  Sinon c'est surtout @IDNOT sur les note qui doit être transformé en @id, à généraliser (attribut qui sont des id mais n'ont pas le bon nom) hors de notes ?  -->
  
  <xsl:template match="*:element[starts-with(@type, 'xfe:noteRef(')]" mode="normalizeConf.step1">
    <xsl:choose>
      <xsl:when test="matches(@type, 'xfe:noteRef\(@\w+\)')">
        <xsl:variable name="idRefAttributeName" select="replace(@type, '^xfe:noteRef\(@(\w+)\)$', '$1')" as="xs:string"/>
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <!--on force certains attributs-->
          <!--cf. schematron conf-->
          <xsl:attribute name="html-name" select="'a'"/>
          <xsl:attribute name="html-attribute-href">
            <!--xpath évalué à ne pas mettre en select-->
            <xsl:text>concat('#', @</xsl:text>
            <xsl:value-of select="$idRefAttributeName"/>
            <xsl:text>)</xsl:text>
          </xsl:attribute>
          <xsl:attribute name="html-addClass" select="normalize-space(concat('xfe_noteRef', ' ', @html-addClass))"/>
          <xsl:attribute name="html-ignoreXmlAttributes" select="normalize-space(concat(@html-ignoreXmlAttributes, ' ', $idRefAttributeName))"/>
          <!--<xsl:attribute name="html-xmlNameAsClass" select="'false'"/> non car on pourrait mélanger les note inline et les autres à la reconstruction du XML-->
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">[ERROR] @type xfe:noteRef doit inclure l'attribut servant d'idRef sous la forme xfe:noteRef(@idRefAttributeName)</xsl:message>
      </xsl:otherwise>  
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="*:element[starts-with(@type, 'xfe:note(')]" mode="normalizeConf.step1">
    <xsl:choose>
      <xsl:when test="matches(@type, 'xfe:note\(@\w+\)')">
        <xsl:variable name="idAttributeName" select="replace(@type, '^xfe:note\(@(\w+)\)$', '$1')" as="xs:string"/>
        <xsl:copy>
          <xsl:copy-of select="@*"/>
          <!--on force certains attributs-->
          <!--cf. schematron conf-->
          <xsl:attribute name="html-attribute-id">
            <!--xpath évalué à ne pas mettre en select-->
            <xsl:text>@</xsl:text>
            <xsl:value-of select="$idAttributeName"/>
          </xsl:attribute>
          <xsl:attribute name="html-addClass" select="normalize-space(concat('xfe_note', ' ', @html-addClass))"/>
          <xsl:attribute name="html-ignoreXmlAttributes" select="normalize-space(concat(@html-ignoreXmlAttributes, ' ', $idAttributeName))"/>
          <!--<xsl:attribute name="html-xmlNameAsClass" select="'false'"/> non car on pourrait mélanger les note inline et les autres à la reconstruction du XML-->
        </xsl:copy>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">[ERROR] @type xfe:note doit inclure l'attribut servant d'id sous la forma xfe:note(@idAttributeName) - ce qui n'est pas le cas dans <xsl:value-of select="@type"/></xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--===== STEP 2 =====-->
  
  <!--Ajoute attribut @snrg-defines (pratique pour le mapping entre la conf et le schema + au passage ajout de @label systématiquement-->
  <xsl:template match="*:element" mode="normalizeConf.step2">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:attribute name="label" select="(@label, tokenize(@xpath, '/')[last()])[1]"/>
      <xsl:attribute name="snrg-defines">
        <xsl:variable name="xpath.token" select="tokenize(@xpath, '/')" as="xs:string*"/>
        <xsl:choose>
          <!--Si le xpath est absolu-->
          <xsl:when test="starts-with(@xpath, '/') and not(starts-with(@xpath, '//'))">
            <xsl:value-of select="rng:getSRNGdataModelFromXpath(@xpath, $xfe:srng.grammar)/parent::rng:define/@name"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:variable name="xpath.elementName" select="$xpath.token[last()]" as="xs:string"/>
            <xsl:variable name="rngDefines" select="$xfe:srng.grammar//rng:define[rng:element[1][@name = $xpath.elementName]]" as="element(rng:define)*"/>
            <xsl:choose>
              <xsl:when test="count($rngDefines) = 0">
                <xsl:message terminate="yes">0 define pour <xsl:copy-of select="$xpath.token"/> (dans conf /element/@xpath='<xsl:value-of select="@xpath"/>')</xsl:message>
              </xsl:when>
              <!--S'il n'y a qu'un seul élément avec ce nom définit dans le modèle, on prend directement sa définition-->
              <xsl:when test="count($rngDefines) = 1">
                <xsl:value-of select="$rngDefines/@name"/>
              </xsl:when>
              <xsl:when test="count($xpath.token) = 1">
                <xsl:value-of select="$rngDefines/@name" separator=" "/>
              </xsl:when>
              <xsl:otherwise>
                <!--Solution temporaire sur le parent uniquement :-->
                <xsl:variable name="xpath.parentName" select="$xpath.token[last() - 1]" as="xs:string"/>
                <xsl:variable name="rngDefines.parent" select="$xfe:srng.grammar//rng:define[rng:element[1][@name = $xpath.parentName]]" as="element(rng:define)*"/>
                <xsl:variable name="rngDefines.filter" as="element(rng:define)*">
                  <xsl:for-each select="$rngDefines">
                    <xsl:variable name="rngDefine" select="." as="element(rng:define)"/>
                    <xsl:if test="$rngDefines.parent[rng:defineHasRefName(., $rngDefine/@name)]">
                      <xsl:sequence select="."/>
                    </xsl:if>
                  </xsl:for-each>
                </xsl:variable>
                <xsl:value-of select="$rngDefines.filter/@name" separator=" "/>
                <!--<xsl:call-template name="filterConfAmbiguousRngDefinesOnRelativeXpath">
                  <xsl:with-param name="defines" select="$rngDefines"/>
                  <xsl:with-param name="xpath.token" select="$xpath.token"/>
                  <xsl:with-param name="xpath.token.position" select="index-of($xpath.token, last())"/>
                </xsl:call-template>-->
              </xsl:otherwise>
            </xsl:choose>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- FIXME A FINIR !
    <xsl:template name="filterConfAmbiguousRngDefinesOnRelativeXpath">
    <xsl:param name="defines" required="yes" as="element(rng:define)*"/>
    <xsl:param name="xpath.tokens" required="yes" as="xs:string*"/>
    <xsl:param name="xpath.token.position" required="yes" as="xs:integer"/>
    <xsl:choose>
      <xsl:when test="count($defines) = 1">
        <xsl:sequence select="$defines"/>
      </xsl:when>
      <xsl:when test="$xpath.token.position = 0">
        <xsl:sequence select="$defines"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="current.xpath.name" select="$xpath.tokens[$xpath.token.position]" as="xs:string"/>
        <xsl:variable name="rngDefines" select="$srng.grammar//rng:define[rng:element[1][@name = $current.xpath.name]]" as="element(rng:define)*"/>
        <xsl:choose>
          <!-\-Il n'y a qu'un seul élément avec ce nom définit dans le modèle-\->
          <xsl:when test="count($rngDefines) = 1">
            
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="filterConfAmbiguousRngDefinesOnRelativeXpath">
              <xsl:with-param name="defines" select="$rngDefines"/>
              <xsl:with-param name="xpath.token" select="remove($xpath.tokens, last())"/>
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>-->
  
  <!--adapte les @match pour le rendre "absolu" (//) => de type "template rule"-->
  <xsl:template match="@xpath" mode="normalizeConf.step2">
    <xsl:attribute name="{name(.)}">
      <xsl:choose>
        <xsl:when test="starts-with(., '/')">
          <xsl:value-of select="."/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="concat('//', .)"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="normalizeConf.step1 normalizeConf.step2">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--=================================================-->
  <!--COMMON-->
  <!--=================================================-->
  
  <!--Un template a appeler pour logger la conf normalisée-->
  <xsl:template name="xfe:log.normalizeConf">
    <xsl:variable name="xfe:normalizeConf.log.uri" select="resolve-uri('conf.normalized.xml', $log.uri)" as="xs:anyURI"/>
    <xsl:if test="$xfe:debug">
      <xsl:message>[INFO] writing <xsl:value-of select="$xfe:normalizeConf.log.uri"/></xsl:message>
      <xsl:result-document href="{$xfe:normalizeConf.log.uri}" format="els:xml">
        <xsl:sequence select="$xfe:conf.normalized"/>
      </xsl:result-document>
    </xsl:if>
  </xsl:template>
  
  <!--In conf.normalized.xml we added on each element an @srng-define attribute that lists all defines in the schema-->
  <xsl:function name="xf:getConfMappingElementsFromSrng" as="element()*">
    <xsl:param name="rngElement" as="element(rng:element)"/>
    <xsl:variable name="define" select="$rngElement/parent::rng:define" as="element(rng:define)"/>
    <xsl:sequence select="$xfe:conf.normalized/*:mapping/*:element[xfe:confElementHasRngDefine(., $define/@name)]"/>
  </xsl:function>
  
  
  <xd:doc>
    <xd:desc>
      <xd:p>Get all conf elements corresponding to the $e xml element instance : whether xpath based or sharing the same define in the schema</xd:p>
      <xd:p>Remember we add the schema define name to every conf elements</xd:p>
      <xd:p>Beware of the plural (elementS) in the name of the function, see the other function "xfe:getConfMappingElementFromXML" (which take the last element)  </xd:p>
    </xd:desc>
    <xd:param name="e">xml element in xml instance</xd:param>
    <xd:return>Zero or more conf elements corresponding</xd:return>
  </xd:doc>
  <xsl:function name="xfe:getConfMappingElementsFromXML" as="element()*">
    <xsl:param name="e" as="element()"/>
    <!--Ici il faut faire attention car la conf définit du xpath ce qui n'est pas forcément basé sur le srng : 
    ex : si titre prévu en h1 sur infoCommentaire/titre mais que tableau/titre utilise la même définition srng, 
    alors il faut induire qu'un titre de tableau sera forcément un h1
    (sauf s'il est lui même redéfini dans la conf plus bas)-->
    <xsl:variable name="conf.elements" select="$xfe:conf.normalized/*:mapping/*:element" as="element()*"/>
    <xsl:variable name="conf.matching.elements" as="element()*"
      select="$conf.elements[els:evaluate-xpath(
      if(@xpath-filter) then(concat(@xpath, '[', @xpath-filter,']')) else(@xpath), 
      $e/root()/*
      )[generate-id(.) = generate-id($e)]]
      "/>
    <!-- Une autre variante identique
      <xsl:variable name="conf.matching.elements" as="element()*">
      <xsl:for-each select="$conf.elements[tokenize(@xpath, '/')[last()] = local-name($e)]">
        <xsl:variable name="conf.element" select="." as="element()"/>
        <xsl:variable name="xpath-with-filter" select="if(@xpath-filter) then(concat(@xpath, '[', @xpath-filter,']')) else(@xpath)" as="xs:string"/>
        <xsl:variable name="xml.matching.elements" select="els:evaluate-xpath($xpath-with-filter, $e/root()/*)" as="element()*"/>
        <xsl:if test="generate-id($e) = $xml.matching.elements/generate-id(.)">
          <xsl:sequence select="$conf.element"/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>-->
    <xsl:variable name="conf.matching.elements.theMorePreciseXpath" as="element()*" 
      select="$conf.matching.elements[ count(tokenize(@xpath, '/')) = max( for $e in $conf.matching.elements return count(tokenize($e/@xpath, '/')) )]"/>
    <xsl:choose>
      <!--There is one (or more) xpath corresponding conf element--> 
      <xsl:when test="count($conf.matching.elements.theMorePreciseXpath) != 0">
        <xsl:sequence select="$conf.matching.elements.theMorePreciseXpath"/>
      </xsl:when>
      <!--If not : look if there is a define corresponding element in the conf-->
      <xsl:otherwise>
        <!--option 1 : on vérifie qu'il n'y a pas de règles dans la conf qui s'applique au même élément par sa définition (pas par son xpath)-->
        <xsl:variable name="e.dataModel" select="rng:getSRNGdataModelFromXmlElement($e, $xfe:srng.grammar)" as="element(rng:element)"/>
        <xsl:variable name="e.define" select="$e.dataModel/parent::rng:define" as="element(rng:define)"/>
        <xsl:variable name="conf.matchingDefine.elements" select="$conf.elements[xfe:confElementHasRngDefine(., $e.define/@name)]" as="element()*"/>
        <!--<xsl:message>e.dataModel= <xsl:value-of select="els:displayNode($e.dataModel)"/></xsl:message>
        <xsl:message>e.define =<xsl:value-of select="$e.define/@name"/></xsl:message>
        <xsl:message>count $conf.matchingDefine.elements = <xsl:value-of select="count($conf.matchingDefine.elements)"/></xsl:message>-->
        <xsl:if test="count($conf.matchingDefine.elements) != 0">
          <xsl:message>[INFO] surcharge de conf induite sur <xsl:value-of select="els:displayNode($e)"/></xsl:message>
          <xsl:if test="count($conf.matchingDefine.elements) gt 1">
            <xsl:message>[WARNING] <xsl:value-of select="count($conf.matchingDefine.elements)"/> elements de surcharge induite sur <xsl:value-of select="els:displayNode($e)"/>, on a pris le dernier ?</xsl:message>
          </xsl:if>
          <xsl:sequence select="$conf.matchingDefine.elements[last()]"/>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="xfe:confElementHasRngDefine" as="xs:boolean">
    <xsl:param name="e" as="element()"/> <!--element dans la conf (sans namespace)-->
    <xsl:param name="defineName" as="xs:string"/>
    <xsl:sequence select="tokenize($e/@snrg-defines, ' ') = $defineName"/>
  </xsl:function>
  
  <xsl:function name="xfe:getConfMappingElementFromXML" as="element()*">
    <xsl:param name="e" as="element()"/>
    <!--comme en css, on prend la dernière règle qui matche-->
    <xsl:sequence select="xfe:getConfMappingElementsFromXML($e)[last()]"/>
  </xsl:function>
  
  <xsl:function name="xfe:getXmlELementName" as="xs:string">
    <xsl:param name="e" as="element()"/> <!--élement html-->
    <xsl:variable name="result" as="xs:string">
      <xsl:choose>
        <xsl:when test="$xfe:srng.grammar.rootIsSingle and $e/self::html:body">
          <xsl:value-of select="rng:getRootElementsName($xfe:srng.grammar)[1]"/>
        </xsl:when>
        <!--On est sur un XFE titre-->
        <xsl:when test="some $class in tokenize($e/@class, $els:regAnySpace) satisfies (starts-with($class, 'xfe_title-'))">
          <xsl:variable name="xfe_title.class" select="(tokenize($e/@class, $els:regAnySpace)[starts-with(., 'xfe_title-')])[1]" as="xs:string"/>
          <xsl:variable name="conf.mappingElements" select="$xfe:conf.normalized/*:mapping/*:element[@html-addClass = $xfe_title.class]" as="element()*"/>
          <xsl:if test="count($conf.mappingElements) gt 1">
            <xsl:message>[WARNING][xfe:getXmlELementName] <xsl:value-of select="count($conf.mappingElements)"/> éléments trouvé pour la class de titre "<xsl:value-of select="$xfe_title.class"/>", on pris le 1er</xsl:message>
          </xsl:if>
          <xsl:value-of select="tokenize($conf.mappingElements[1]/@xpath, '/')[last()]"/>
        </xsl:when>
        <!--Sinon on se base sur la class ou (si asbente) le xpath de la conf trouvée-->
        <xsl:otherwise>
          <xsl:variable name="conf.mappingElements" select="$xfe:conf.normalized/*:mapping/*:element[@html-name = local-name($e)]" as="element()*"/>
          <!--cf. schematron conf : dans la conf, il ne peut pas y avoir plusieurs éléments donnant le même élément html avec xmlNameAsClass=false-->
          <xsl:variable name="conf.mappingElementNoClass" select="$conf.mappingElements[matches(@html-xmlNameAsClass, '(false|0)') and not(starts-with(@html-addClass, 'xfe_title-'))]" as="element()?"/>
          <xsl:choose>
            <xsl:when test="count($conf.mappingElementNoClass) = 1">
              <xsl:value-of select="tokenize($conf.mappingElementNoClass/@xpath, '/')[last()]"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="tokenize($e/@class, $els:regAnySpace)[1]"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="normalize-space($result) != ''">
        <xsl:value-of select="$result"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">[FATAL][xfe:getXmlELementName] Impossible de déterminer le nom de l'élément XML pour <xsl:value-of select="els:get-xpath($e)"/> : <xsl:value-of select="els:displayNode($e)"/></xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="xfe:getTableCasePattern" as="xs:string">
    <xsl:param name="elementName" as="xs:string"/>
    <xsl:choose>
      <xsl:when test="upper-case($elementName) = $elementName">
        <xsl:value-of select="'uppercase'"/>
      </xsl:when>
      <xsl:when test="lower-case($elementName) = $elementName">
        <xsl:value-of select="'lowercase'"/>
      </xsl:when>
      <xsl:when test="upper-case(substring($elementName, 1, 1)) = substring($elementName, 1, 1) and lower-case(substring($elementName, 2)) = substring($elementName, 2)">
        <xsl:value-of select="'titlecase'"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'error'"/>
        <xsl:message terminate="yes">[FATAL][xfe:getTableCasePattern] Impossible de déduire le modèle de nommage des tableaux (uppercase, lowercase ot titlecase)</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="xfe:apply-case" as="xs:string">
    <xsl:param name="string" as="xs:string"/>
    <xsl:param name="nameCase" as="xs:string"/>
    <xsl:choose>
      <xsl:when test="$nameCase = 'uppercase'">
        <xsl:sequence select="upper-case($string)"/>
      </xsl:when>
      <xsl:when test="$nameCase = 'lowercase'">
        <xsl:sequence select="lower-case($string)"/>
      </xsl:when>
      <xsl:when test="$nameCase = 'titlecase'">
        <xsl:sequence select="concat(upper-case(substring($string, 1, 1)), lower-case(substring($string, 2)))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
</xsl:stylesheet>
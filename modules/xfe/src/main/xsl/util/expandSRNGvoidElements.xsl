<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
	xmlns="http://relaxng.org/ns/structure/1.0"
	xmlns:saxon="http://saxon.sf.net/"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:rng="http://relaxng.org/ns/structure/1.0"
	xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
	xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
	xmlns:step1="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor/snrgXML2rngHtml/step1"
	xmlns:step2="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor/snrgXML2rngHtml/step2"
	xmlns:step3="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor/snrgXML2rngHtml/step3"
	xmlns:step4="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor/snrgXML2rngHtml/step4"
	xmlns:conf="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor/conf"
	xpath-default-namespace="http://relaxng.org/ns/structure/1.0"
	exclude-result-prefixes="#all"
	version="2.0">
	
	<xsl:import href="../xfe-common.xsl"/>
	
	<xd:doc scope="stylesheet">
		<xd:desc>
			<xd:p>XSLT permettant d'analyser le SNRG par rapport à la conf xfe : elle fait abstraction des éléments void (dans conf) 
				de manière à voir notamment les éléments qui deviennent adjaçant. Cela permets notamment d'aider à la rétro-conv HTML2XML</xd:p>
			<xd:p>Exemple de requête à faire dans le fichier de sortie : </xd:p>
			<xd:pre>//xfe:children/*[@define = preceding-sibling::*/@define]</xd:pre>
		</xd:desc>
	</xd:doc>
	
	<xsl:param name="voidToIgnore" select="('')" as="xs:string*"/>
	
	<!--infoCommentaire-->
	<!--<xsl:param name="voidToIgnore" select="('blocAnalyses', 'blocObservations', 'blocAbstracts', 'niveau', 'microContenu', 'blocRattach', 'blocNotes', 'blocAuteur')" as="xs:string*"/>-->
	
	
	<!--================================-->
	<!--MAIN-->
	<!--================================-->
	
	<xsl:template match="/">
		<xsl:call-template name="xfe:log.normalizeConf"/>
		<xsl:variable name="step1" as="document-node()">
			<xsl:document>
				<xsl:apply-templates mode="step1"/>
			</xsl:document>
		</xsl:variable>
		<xsl:variable name="step2" as="document-node()">
			<xsl:document>
				<xsl:apply-templates select="$step1" mode="step2"/>
			</xsl:document>
		</xsl:variable>
		<xsl:apply-templates select="$step2" mode="rng:clean"/>
	</xsl:template>
	
	<!--================================-->
	<!--STEP1-->
	<!--================================-->
	
	<xsl:template match="element" mode="step1">
		<xsl:variable name="self" select="self::*" as="element(element)"/>
		<xsl:variable name="confMappingElementsFromSrng" select="xf:getConfMappingElementsFromSrng(.)" as="element()*"/>
		<xsl:message use-when="false()">match element name=<xsl:value-of select="@name"/>, count conf = <xsl:value-of select="count($confMappingElementsFromSrng)"/></xsl:message>
		<xsl:copy>
			<xsl:apply-templates select="@*" mode="#current"/>
			<xsl:for-each select="$confMappingElementsFromSrng">
				<conf:element>
					<xsl:copy-of select="@*"/>
				</conf:element>
			</xsl:for-each>
			<xsl:apply-templates mode="#current"/>
		</xsl:copy>
	</xsl:template>
	
	<!--================================-->
	<!--STEP2-->
	<!--================================-->
	
	<xsl:template match="element[.//ref[rng:getDefine(.)/element[xfe:isVoid(.)]/conf:element[1]]]" mode="step2">
		<xsl:variable name="children" as="element(element)*">
			<xsl:apply-templates select=".//ref" mode="getChildrenBewareVoidElements"/>
		</xsl:variable>
		<xfe:children>
			<xsl:copy-of select="$children"/>
		</xfe:children>
		<xsl:next-match/>
	</xsl:template>
	
	<xsl:template match="ref" mode="getChildrenBewareVoidElements">
		<xsl:apply-templates select="rng:getDefine(.)/element" mode="getChildrenBewareVoidElements"/>
	</xsl:template>
	
	<xsl:template match="element" mode="getChildrenBewareVoidElements">
		<xsl:param name="voids" as="xs:string*" tunnel="yes"/>
		<xsl:copy copy-namespaces="no">
			<xsl:copy-of select="@*"/>
			<xsl:attribute name="define" select="parent::define/@name"/>
			<xsl:attribute name="voids" select="$voids" separator=">"/>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="element[xfe:isVoid(.)]" mode="getChildrenBewareVoidElements">
		<xsl:param name="voids" as="xs:string*" tunnel="yes"/>
		<xsl:apply-templates select=".//ref[not(@name = current()/@name)]" mode="getChildrenBewareVoidElements">
			<xsl:with-param name="voids" select="@name" tunnel="yes"/>
		</xsl:apply-templates>
	</xsl:template>
	
	<xsl:template match="attribute" mode="step2"/>
	
	<xsl:template match="ref[not(@name = ancestor::define/@name)][rng:getDefine(.)/element[xfe:isVoid(.)]]" mode="step2">
		<xsl:comment>VOID <xsl:value-of select="els:displayNode(.)"/></xsl:comment>
		<xsl:apply-templates select="rng:getDefine(.)/element/node()" mode="#current"/>
	</xsl:template>
	
	<!--================================-->
	<!--COMMON-->
	<!--================================-->

	<xsl:function name="xfe:isVoid" as="xs:boolean">
		<xsl:param name="rngElement" as="element(rng:element)"/>
		<xsl:sequence select="some $htmlName in $rngElement[not(@name = $voidToIgnore)]/conf:element/@html-name satisfies $htmlName ='void'"/>
	</xsl:function>
	
	<!--copie par défaut-->
	<xsl:template match="node() | @*" mode="step1 step2">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" mode="#current"/>
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>
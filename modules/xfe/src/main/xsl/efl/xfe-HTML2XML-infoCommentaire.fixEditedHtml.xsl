<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:saxon="http://saxon.sf.net/"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
	xmlns:rng="http://relaxng.org/ns/structure/1.0"
	xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
	xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
	xmlns="http://www.w3.org/1999/xhtml"
	xpath-default-namespace="http://www.w3.org/1999/xhtml"
	exclude-result-prefixes="#all"
	version="2.0">
  
  <xsl:import href="xslLib:/xslLib/rng-common.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>XSLT spécifique à EFL/infoCommentaire permettant corriger les erreurs HTML après édition dans TinyMCE</xd:p>
    </xd:desc>
  </xd:doc>
	
  <!--===================================================-->
  <!--INIT-->
  <!--===================================================-->
  
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfe:fixEditedHtml"/>
  </xsl:template>
  
  <!--===================================================-->
  <!--MAIN-->
  <!--===================================================-->
  
  <xsl:template match="body" mode="xfe:fixEditedHtml">
    <xsl:copy copy-namespaces="no">
      <xsl:apply-templates select="@*" mode="#current"/>
      <!--Le body n'accepte pas de contenu textuel, donc mettre select="*" (on perdra les commentaire et PI mais on estime qu'il n'y en aura pas)-->
      <xsl:for-each-group select="*" group-adjacent="els:hasClass(., 'accroche')">
        <xsl:choose>
          <xsl:when test="current-group()[1]/self::*[els:hasClass(., 'accroche')]">
            <xsl:element name="{local-name(current-group()[1])}">
              <!--on va conserver l'id du 1er élément du groupe-->
              <xsl:apply-templates select="current-group()[1]/@*" mode="#current"/>
              <xsl:apply-templates select="current-group()/node()" mode="#current"/>
            </xsl:element>
          </xsl:when>
          <xsl:otherwise>
            <xsl:for-each-group select="current-group()" group-adjacent="els:hasClass(., 'resume')">
              <xsl:choose>
                <xsl:when test="current-group()[1]/self::*[els:hasClass(., 'resume')]">
                  <xsl:element name="{local-name(current-group()[1])}">
                    <!--on va conserver l'id du 1er élément du groupe-->
                    <xsl:apply-templates select="current-group()[1]/@*" mode="#current"/>
                    <xsl:apply-templates select="current-group()/node()" mode="#current"/>
                  </xsl:element>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:for-each-group select="current-group()" group-adjacent="els:hasClass(., 'introduction')">
                    <xsl:choose>
                      <xsl:when test="current-group()[1]/self::*[els:hasClass(., 'introduction')]">
                        <xsl:element name="{local-name(current-group()[1])}">
                          <!--on va conserver l'id du 1er élément du groupe-->
                          <xsl:apply-templates select="current-group()[1]/@*" mode="#current"/>
                          <xsl:apply-templates select="current-group()/node()" mode="#current"/>
                        </xsl:element>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:apply-templates select="current-group()" mode="#current"/>
                      </xsl:otherwise>
                    </xsl:choose>
                  </xsl:for-each-group>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:for-each-group>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each-group> 
    </xsl:copy>
  </xsl:template>
  
  <!--===================================================-->
  <!--COMMON-->
  <!--===================================================-->
 
  <xsl:template match="node() | @*" mode="xfe:fixEditedHtml">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
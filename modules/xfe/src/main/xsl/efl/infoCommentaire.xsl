<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:saxon="http://saxon.sf.net/"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
	xmlns:rng="http://relaxng.org/ns/structure/1.0"
	xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
	xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
	xmlns:html="http://www.w3.org/1999/xhtml"
	xmlns:xmlFix="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor/html2xml/xmlFix"
	xmlns="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
	xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
	exclude-result-prefixes="#all"
	version="2.0">
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>XSLT spécifique à EFL/infoCommentaire permettant de rendre le xml valide lors de la conversion html2xml : 
        - Fixe certains attributs manuellement
        - ajoute les éléments "void" qui avaient été supprimés lors de la conversion XML->HTML
        </xd:p>
    </xd:desc>
  </xd:doc>
	
	<!--<xsl:import href="../xfe-common.xsl"/>-->
	
  <!--===================================================-->
  <!--SPECIFIC OVERRIDING TEMPLATES-->
  <!--===================================================-->
  
  <xsl:template match="*:refDoc[@xf:targetResId]" 
    mode="xfe:makeRelationElement" priority="1">
    <xsl:next-match>
      <xsl:with-param name="relation.code" select="concat('EFL_TR_', local-name(.))"/>
      <xsl:with-param name="relation.metadata" as="element()">
        <metadata xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst">
          <!--FIXME : -->
          <xsl:if test="@type">
            <meta code="relationEFL_TYPE">
              <value>
                <xsl:value-of select="@type"/>
              </value>
            </meta>
          </xsl:if>
          <xsl:if test="@formatRedige">
            <meta code="relationEFL_formatRedige">
              <value>
                <xsl:value-of select="@formatRedige"/>
              </value>
            </meta>
          </xsl:if>
        </metadata>
      </xsl:with-param>
      <xsl:with-param name="relation.ref.refType" select="concat('efl:', local-name(.))"/>
    </xsl:next-match>
  </xsl:template>
  
  <xsl:template match="*[local-name() = ('renvoiAutrui', 'rMemN', 'rPratN', 'rEflN')][@xf:targetResId]" 
    mode="xfe:makeRelationElement" priority="1">
    <xsl:next-match>
      <xsl:with-param name="relation.code" select="concat('EFL_TR_', local-name(.))"/>
      <xsl:with-param name="relation.ref.refType" select="concat('efl:', local-name(.))"/>
    </xsl:next-match>
  </xsl:template>
  
	<!--===================================================-->
	<!--MAIN-->
	<!--===================================================-->
	
	<xsl:template match="/" mode="xmlFix">
		<xsl:variable name="step1" as="document-node()">
			<xsl:document>
				<xsl:apply-templates select="." mode="xmlFix:step1"/>
			</xsl:document>
		</xsl:variable>
		<xsl:variable name="step2" as="document-node()">
			<xsl:document>
				<xsl:apply-templates select="$step1" mode="xmlFix:step2"/>
			</xsl:document>
		</xsl:variable>
		<xsl:variable name="step3" as="document-node()">
			<xsl:document>
				<xsl:apply-templates select="$step2" mode="xmlFix:step3"/>
			</xsl:document>
		</xsl:variable>
		<xsl:variable name="step4" as="document-node()">
			<xsl:document>
				<xsl:apply-templates select="$step3" mode="xmlFix:step4"/>
			</xsl:document>
		</xsl:variable>
	  <xsl:variable name="step5" as="document-node()">
	    <xsl:document>
	      <xsl:apply-templates select="$step4" mode="xmlFix:step5"/>
	    </xsl:document>
	  </xsl:variable>
	  <xsl:apply-templates select="$step5" mode="xmlFix:attributes"/>
	</xsl:template>
  
	<!--===================================================-->
	<!--xmlFix:step1 "blocRattach"-->
	<!--===================================================-->
	<!--add blocRattach (dans infoCommentaire, niveau, pNum, pNonNum, ...-->
	
	<!--[not(self::blocRattach)]-->
	<xsl:template match="*[rattach]" mode="xmlFix:step1">
		<xsl:copy>
			<xsl:apply-templates select="@*" mode="#current"/>
			<xsl:for-each-group select="*" group-adjacent="self::rattach or false()">
				<xsl:choose>
					<xsl:when test="current-grouping-key()">
						<blocRattach>
							<xsl:apply-templates select="current-group()" mode="#current"/>
						</blocRattach>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="current-group()" mode="#current"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each-group>
		</xsl:copy>
	</xsl:template>
	
	<!--===================================================-->
	<!--xmlFix:step2 : blocs non ambigües sous infoCommentaire -->
	<!--===================================================-->
	<!--add blocAnalyses, blocObservations, blocAbstracts, blocRattach, blocNotes, blocAuteur-->
	
	<xsl:template match="infoCommentaire" mode="xmlFix:step2">
		<xsl:copy>
			<xsl:apply-templates select="@*" mode="#current"/>
			<xsl:for-each-group select="*" group-adjacent="
					 self::blocRefDoc[following-sibling::*[1]/self::analyse]
				or self::analyse">
				<xsl:choose>
					<xsl:when test="current-grouping-key()">
						<blocAnalyses>
							<xsl:apply-templates select="current-group()" mode="#current"/>
						</blocAnalyses>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each-group select="current-group()" group-adjacent="
							self::introduction[following-sibling::*[1]/self::*[local-name() = ('observation', 'refObservation')]] 
							(: 
							d'après le schema il ne peut ne pas ne pas y avoir de (ref)observation juste après, 
							par contre cette introduction pourrait être celle du microContenu => c'est un choix arbitraire
							:)
							or self::observation
							or self::refObservation">
							<xsl:choose>
								<xsl:when test="current-grouping-key()">
									<blocObservations>
										<xsl:apply-templates select="current-group()" mode="#current"/>
									</blocObservations>
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each-group select="current-group()" group-adjacent="self::abstract or false()">
										<xsl:choose>
											<xsl:when test="current-grouping-key()">
												<blocAbstracts>
													<xsl:apply-templates select="current-group()" mode="#current"/>
												</blocAbstracts>
											</xsl:when>
											<xsl:otherwise>
												<xsl:for-each-group select="current-group()" group-adjacent="self::note[not(parent::tblNote)] or false()">
													<xsl:choose>
														<xsl:when test="current-grouping-key()">
															<blocNotes>
																<xsl:apply-templates select="current-group()" mode="#current"/>
															</blocNotes>
														</xsl:when>
														<xsl:otherwise>
															<xsl:for-each-group select="current-group()" group-adjacent="self::horsTexte or false()">
																<xsl:choose>
																	<xsl:when test="current-grouping-key()">
																		<blocHorsTextes>
																			<xsl:apply-templates select="current-group()" mode="#current"/>
																		</blocHorsTextes>
																	</xsl:when>
																	<xsl:otherwise>
																		<xsl:for-each-group select="current-group()" group-adjacent="self::libelleIntroAuteur or self::auteur or false()">
																			<xsl:choose>
																				<xsl:when test="current-grouping-key()">
																					<blocAuteur>
																						<xsl:apply-templates select="current-group()" mode="#current"/>
																					</blocAuteur>
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:apply-templates select="current-group()" mode="#current"/>
																				</xsl:otherwise>
																			</xsl:choose>
																		</xsl:for-each-group>
																	</xsl:otherwise>
																</xsl:choose>
															</xsl:for-each-group>
														</xsl:otherwise>
													</xsl:choose>
												</xsl:for-each-group>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:for-each-group>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each-group>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each-group>
		</xsl:copy>
	</xsl:template>
	
	<!--===================================================-->
	<!--xmlFix:step3 : niveau-->
	<!--===================================================-->
	<!-- add : niveau -->
	<!-- parent : commentaire, horsTexte, observation + niveau (en référence circulaire)-->
	<!-- On n'a pas encore mis commentaire à ce stade (justement on veut le faire après pour éliminer les ambiguité lié aux sous-élement des niveaux)
				- commentaire fini par niveau 
				- sous infoCommentaire, après commentaire on peut avoir : blocAnalyses, blocHorsTextes, blocNotes
	-->
	<xsl:template match="infoCommentaire" mode="xmlFix:step3">
		<xsl:copy>
			<xsl:apply-templates select="@*" mode="#current"/>
			<xsl:for-each-group select="*" 
				group-starting-with="*[xmlFix:isHeading(.)]">
				<xsl:choose>
					<xsl:when test="xmlFix:isHeading(current-group()[1])">
						<xsl:for-each-group select="current-group()" 
							group-starting-with="*[local-name(.) = ('blocAnalyses', 'blocHorsTextes', 'blocNotes')]">
							<xsl:choose>
								<xsl:when test="local-name(current-group()[1]) = ('blocAnalyses', 'blocHorsTextes', 'blocNotes')">
									<xsl:apply-templates select="current-group()" mode="#current"/>
								</xsl:when>
								<xsl:otherwise>
									<niveau>
										<xsl:apply-templates select="current-group()" mode="#current"/>
									</niveau>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each-group> 
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="current-group()" mode="#current"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each-group> 
		</xsl:copy>
	</xsl:template>
	
	<!-- sous observation et sous horsTexte, niveau est TOUJOURS à la fin-->
	<xsl:template match="observation | horsTexte" mode="xmlFix:step3">
		<xsl:copy>
			<xsl:apply-templates select="@*" mode="#current"/>
			<xsl:for-each-group select="*" 
				group-starting-with="*[xmlFix:isHeading(.)]">
				<xsl:choose>
					<xsl:when test="xmlFix:isHeading(current-group()[1])">
						<niveau>
							<xsl:apply-templates select="current-group()" mode="#current"/>
						</niveau> 
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="current-group()" mode="#current"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each-group> 
		</xsl:copy>
	</xsl:template>
	
	<xsl:function name="xmlFix:isHeading" as="xs:boolean">
		<xsl:param name="e" as="element()"/>
		<xsl:sequence select="some $class in tokenize($e/@html:class, $els:regAnySpace) satisfies (starts-with($class, 'xfe_title-'))"/>
	</xsl:function>
	
	<!--===================================================-->
	<!--xmlFix:step4 : microContenu-->
	<!--===================================================-->
	<!-- add microContenu-->
	<!--Il y avait une ambiguité sur introduction qui peut également apparaître dans blocObservations (qui est void aussi au même niveau)
	Mais puisqu'on a déjà ajouté les blocObservations dans un step précédent, le problème ne se pose plus-->
	<xsl:template match="infoCommentaire" mode="xmlFix:step4">
		<xsl:copy>
			<xsl:apply-templates select="@*" mode="#current"/>
			<xsl:for-each-group select="*" 
				group-adjacent="self::resume or self::accroche or self::introduction">
				<!--[following-sibling::commentaire or following-sibling::analyseObservation] : plus nécessaire !-->
				<xsl:choose>
					<xsl:when test="current-grouping-key()">
						<microContenu>
							<xsl:apply-templates select="current-group()" mode="#current"/>
						</microContenu>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates select="current-group()" mode="#current"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each-group>
		</xsl:copy>
	</xsl:template>
	
	<!--===================================================-->
	<!--xmlFix:step4-->
	<!--===================================================-->
	
	<!--add analyseObservation or commentaire or nothing-->
	<!-- model après expansion avec les void : 
		<element name="infoCommentaire" ns="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire">
			<group>
				<!-\- ======= GROUP 1 ======= -\->
				<ref name="titre"/>
				<optional>
					<ref name="sousTitre"/>
				</optional>
				<optional>
					<ref name="titreObjet"/>
				</optional>
				<optional>
					<ref name="titreUne"/>
				</optional>
				<optional>
					<ref name="titreSommaire"/>
				</optional>
				<optional>
					<ref name="blocAuteur"/>
				</optional>
				<optional>
					<ref name="blocRattach"/>
				</optional>
				<optional>
					<ref name="blocRefDoc"/>
				</optional>
				<optional>
					<ref name="microContenu"/>
				</optional>
				<optional>
					<!-\- ======= GROUP 2 ======= -\->
					<choice> 
						<group>
							<!-\-VOID element():ref_@name="commentaire"-\->
							<!-\- ======= GROUP 2 > BRANCH 1 ======= -\->
							<group>
								<zeroOrMore>
									<choice>
										<ref name="al"/>
										<ref name="couyard"/>
										<ref name="lst"/>
										<ref name="table"/>
										<ref name="graphique"/>
										<ref name="multimedia"/>
										<ref name="formule"/>
										<ref name="exemple"/>
										<ref name="complement"/>
										<ref name="entreeEnVigueur"/>
										<ref name="sourceReproduite"/>
										<ref name="miseEnValeur"/>
										<ref name="ndlr"/>
									</choice>
								</zeroOrMore>
								<zeroOrMore>
									<choice>
										<ref name="pNonNum"/>
										<ref name="pNum"/>
									</choice>
								</zeroOrMore>
								<zeroOrMore>
									<ref name="niveau"/>
								</zeroOrMore>
							</group>
							<optional>
								<ref name="blocAnalyses"/>
							</optional>
						</group>
						<!-\- ======= GROUP 2 > BRANCH 2 ======= -\->
						<oneOrMore>
							<!-\-VOID element():ref_@name="analyseObservation"-\->
							<group>
								<optional>
									<ref name="blocAbstracts"/>
								</optional>
								<choice>
									<ref name="renvoiAutrui"/>
									<group>
										<optional>
											<ref name="blocAnalyses"/>
										</optional>
										<optional>
											<ref name="blocObservations"/>
										</optional>
										<zeroOrMore>
											<ref name="sourceReproduite"/>
										</zeroOrMore>
									</group>
								</choice>
							</group>
						</oneOrMore>
						<!-\- ======= GROUP 2 > BRANCH 3 ======= -\->
						<ref name="blocAnalyses"/>
					</choice>
				</optional>
				<!-\- ======= GROUP 3 ======= -\->
				<optional>
					<ref name="blocHorsTextes"/>
				</optional>
				<optional>
					<ref name="blocNotes"/>
				</optional>
			</group>
		</element>
		
		Analyse : 
		Éléments communs entre les 3 branches : 
		- branche 1 et 2 : sourceReproduite
			=> désambiguisable :
				- cas 1 : on a un des éléments spécifiques de la branche 1 : al | couyard | lst | table | graphique | multimedia | formule | exemple | complement | entreeEnVigueur | sourceReproduite | miseEnValeur | ndlr | pNonNum | pNum | niveau
							=>  branche 1
				- cas 2 : on a un des éléments spécifiques de la branche 2 : blocAbstracts | renvoiAutrui | blocObservations
							=> branche 2
				- cas 3 : N blocAnalyses 
							=> branche 2 (branche 1 et 3 n'autorisent qu'un seul blocAnalyses, la 2 dans un oneOrMore en autorise plusieurs)
							=> cf. cas 8
				- cas 4 : uniquement N sourceReproduite  : on ne peut pas savoir
							=> branche 1 (arbitrairement)
				- cas 5 : 1 blocAnalyses puis N sourceReproduite  
							=> branche 2
				- cas 6 : N sourceReproduite puis 1 blocAnalyses 
							=> branche 1 ("arbitrairement" car on aurait pu aussi choisir branche 2 qui a un oneOrMore donc l'ordre importe pas)
		- branche 2 et 3 : blocAnalyses 
			=> désambiguisable : 
				- cas 7 : (uniquement) 1 seul blocAnalyses : on ne peut pas savoir 
					=> branche 3 (arbitrairement)
				- cas 8 : N blocAnalyses
					=> branche 2
					
					
			TODO : prendre toutes les données de la chaîne XML et faire xml2html puis html2xml pour voir si ça marche
	-->

	<xsl:template match="infoCommentaire[not(commentaire | analyseObservation)]" mode="xmlFix:step5">
		<xsl:copy>
			<xsl:apply-templates select="@*" mode="#current"/>
			<xsl:for-each-group select="*" 
				group-adjacent="xmlFix:isInInfoCommentaireGroup1(.)" >
				<xsl:choose>
					<xsl:when test="current-grouping-key()">
						<xsl:apply-templates select="current-group()" mode="#current"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:for-each-group select="current-group()" 
							group-adjacent="xmlFix:isInInfoCommentaireGroup3(.)" >
							<xsl:choose>
								<xsl:when test="current-grouping-key()">
									<xsl:apply-templates select="current-group()" mode="#current"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:message use-when="false()">cg : <xsl:value-of select="current-group()/local-name(.)"/></xsl:message>
									<!--GROUP 2-->
									<xsl:variable name="switchNum" as="xs:integer">
										<xsl:choose>
											<!--cas 1-->
											<xsl:when test="some $e in current-group() satisfies 
												local-name($e) = ('al', 'couyard', 'lst', 'table', 'graphique', 'multimedia', 'formule', 'exemple', 'complement', 'entreeEnVigueur', 'sourceReproduite', 'miseEnValeur', 'ndlr', 'pNum', 'pNonNum', 'niveau')">
												<xsl:sequence select="1"/>
											</xsl:when>
											<!--cas 2-->
											<xsl:when test="some $e in current-group() satisfies 
												local-name($e) = ('blocAbstracts', 'renvoiAutrui', 'blocObservations')">
												<xsl:sequence select="2"/>
											</xsl:when>
											<!--cas 3 = cas 8-->
											<xsl:when test="count(current-group()) gt 1 and
												(every $e in current-group() satisfies local-name($e) = ('blocAnalyses'))">
												<xsl:sequence select="2"/>
											</xsl:when>
											<!--cas 4-->
											<xsl:when test="every $e in current-group() satisfies local-name($e) = ('sourceReproduite')">
												<xsl:sequence select="1"/>
											</xsl:when>
											<!--cas 5 : 1 blocAnalyses puis N sourceReproduite-->
											<xsl:when test="every $e in current-group() satisfies local-name($e) = ('sourceReproduite', 'blocAnalyses')
												and (every $blocAnalyses in current-group()[self::blocAnalyses] satisfies $blocAnalyses/following-sibling::sourceReproduite)">
												<xsl:sequence select="2"/>
											</xsl:when>
											<!--cas 6 : N sourceReproduite puis 1 blocAnalyses-->
											<xsl:when test="every $e in current-group() satisfies local-name($e) = ('sourceReproduite', 'blocAnalyses')
												and (every $sourceReproduite in current-group()[self::sourceReproduite] satisfies $sourceReproduite/following-sibling::blocAnalyses)">
												<xsl:sequence select="1"/>
											</xsl:when>
											<!--cas 7-->
											<xsl:when test="count(current-group()) = 1 and current-group()/self::blocAnalyses">
												<xsl:sequence select="3"/>
											</xsl:when>
											<!--cas 8 => cas 3-->
											<xsl:otherwise>
												<xsl:sequence select="-1"/>
											</xsl:otherwise>
										</xsl:choose>
									</xsl:variable>
									<xsl:choose>
										<xsl:when test="$switchNum = -1">
											<xsl:result-document href="log/uiCommentaire.beforeERROR.xml" format="els:xml">
												<xsl:sequence select="root(.)"/>
											</xsl:result-document>
											<xsl:message terminate="yes">[ERROR] Impossible de déterminer si on est dans un commentaire ou un analyseObservation, cf. log/uiCommentaire.beforeERROR.xml</xsl:message>
										</xsl:when>
										<xsl:when test="$switchNum = 1">
											<commentaire>
												<xsl:apply-templates select="current-group()" mode="#current"/>
											</commentaire>
										</xsl:when>
										<xsl:when test="$switchNum = 2">
											<analyseObservation>
												<xsl:apply-templates select="current-group()" mode="#current"/>
											</analyseObservation>
										</xsl:when>
										<xsl:otherwise>
											<xsl:apply-templates select="current-group()" mode="#current"/>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each-group>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each-group>
		</xsl:copy>
	</xsl:template>
	
	<xsl:function name="xmlFix:isInInfoCommentaireGroup1" as="xs:boolean">
		<xsl:param name="e" as="element()"/>
		<xsl:sequence select="local-name($e) = 
			('titre', 'sousTitre', 'titreObjet', 'titreUne', 'titreSommaire', 'blocAuteur', 'blocRattach', 'blocRefDoc', 'microContenu')"/>
	</xsl:function>
	
	<xsl:function name="xmlFix:isInInfoCommentaireGroup3" as="xs:boolean">
		<xsl:param name="e" as="element()"/>
		<xsl:sequence select="local-name($e) = ('blocHorsTextes', 'blocNotes')"/>
	</xsl:function>
  
  <!--===================================================-->
  <!--LAST STEP : fix attributes-->
  <!--===================================================-->
  
  <xsl:template match="commentaire | niveau | analyseObservation | blocAnalyses | blocObservations | blocAbstracts" mode="xmlFix:attributes">
    <xsl:copy>
      <xsl:attribute name="id" select="concat('id-a_supprimer-', local-name(), '-', count(preceding::*[local-name() = local-name(current())]) + 1)"/>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="lst/@xf:listType" mode="xmlFix:attributes">
    <xsl:attribute name="format">
      <xsl:choose>
        <!-- === UL === -->
        <xsl:when test=". = '-'">
          <xsl:text>tiret</xsl:text>
        </xsl:when>
        <xsl:when test=". = '•'">
          <xsl:text>puce</xsl:text>
        </xsl:when>
        <!-- === OL === -->
        <!--decimal => numerique-->
        <xsl:when test="matches(., '^decimal\s+$') or matches(., '^decimal\s+°$')"> <!--FIXME-->
          <xsl:text>numerique</xsl:text>
        </xsl:when>
        <xsl:when test="matches(., '^decimal\s+\.$')">
          <xsl:text>numerique-point</xsl:text>
        </xsl:when>
        <xsl:when test="matches(., '^decimal\s+\)$')">
          <xsl:text>numerique-parenthese</xsl:text>
        </xsl:when>
        <xsl:when test="matches(., '^decimal\s+-$')">
          <xsl:text>numerique-tiret</xsl:text>
        </xsl:when>
        <!--lower-alpha => letter-->
        <!--<xsl:when test="matches(., '^lower-alpha\s+$')">
          <xsl:text>numerique</xsl:text>
        </xsl:when>-->
        <xsl:when test="matches(., '^lower-alpha\s+\.$')">
          <xsl:text>letter-point</xsl:text>
        </xsl:when>
        <xsl:when test="matches(., '^lower-alpha\s+\)$')">
          <xsl:text>letter-parenthese</xsl:text>
        </xsl:when>
        <!--<xsl:when test="matches(., '^lower-alpha\s+-$')">
          <xsl:text>letter-tiret</xsl:text>
        </xsl:when>-->
        <xsl:otherwise>
          <xsl:message>[ERROR] format de liste "<xsl:value-of select="."/>" non reconnu</xsl:message>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:template>
  
  <xsl:template match="graphique/@html:src" mode="xmlFix:attributes">
    <xsl:attribute name="href" select="."/>
  </xsl:template>
  
  <xsl:template match="portrait/@html:src" mode="xmlFix:attributes">
    <xsl:attribute name="src" select="."/>
  </xsl:template>
  
  <!--===============================-->
  <!-- Internal Links-->
  <!--===============================-->
  
  <xsl:template match="renvoi/@xf:href" mode="xmlFix:attributes">
    <xsl:attribute name="ref" select="substring-after(., '#')"/>
  </xsl:template>
  
  <xsl:template match="rNote/@xf:href" mode="xmlFix:attributes">
    <xsl:attribute name="ref" select="substring-after(., '#')"/>
  </xsl:template>
  
  <xsl:template match="rHorsTexte/@xf:href" mode="xmlFix:attributes">
    <xsl:attribute name="ref" select="substring-after(., '#')"/>
  </xsl:template>
  
  <!--===============================-->
  <!-- External Links : resolved -->
  <!--===============================-->
  
  <xsl:template match="renvoiAutrui/@xf:targetResId" mode="xmlFix:attributes">
    <xsl:next-match/>
    <xsl:attribute name="xf:refType" select="'efl:renvoiAutrui'"/>
    <xsl:attribute name="uiId" select="'obsolete'"/>
  </xsl:template>
  
  <xsl:template match="refDoc/@xf:targetResId" mode="xmlFix:attributes">
    <xsl:next-match/>
    <xsl:attribute name="ref" select="'fixme:getTargetMeta2generateRef'"/>
    <xsl:attribute name="xf:refType" select="'efl:refDoc'"/>
  </xsl:template>
  
  <!--===============================-->
  <!-- External Links : unresolved -->
  <!--===============================-->
  
  <xsl:template match="renvoiAutrui[not(@xf:targetResId)]" mode="xmlFix:attributes">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:attribute name="uiId" select="'OBSOLETE'"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--<xsl:template match="refDoc[not(@xf:targetResId)]" mode="xmlFix:attributes">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>-->
  
  <!--===============================-->
  <!-- default copy -->
  <!--===============================-->
  
  <xsl:template match="node() | @*" mode="xmlFix:attributes">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
	
	<!--===================================================-->
	<!--COMMON-->
	<!--===================================================-->
	
	<xsl:template match="node() | @*" mode="xmlFix:step1 xmlFix:step2 xmlFix:step3 xmlFix:step4 xmlFix:step5">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" mode="#current"/>
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
  xpath-default-namespace="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <!-- (pourra être utilisée si besoin) -->
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Clean TinyMCE HTML before converting it to XML</xd:p>
    </xd:desc>
  </xd:doc>
  
  <xsl:output method="xml" indent="no"/>
  
  <xsl:param name="xfe:tinyMce.class.prefix" select="'ms-'" as="xs:string"/>
  <xsl:param name="xfe:tinyMce.data-attributes.suffix" select="'-ms'" as="xs:string"/>
  
  <xsl:variable name="xfe:tinyMce.data-attributes.regex" select="concat('^data-.*?', $xfe:tinyMce.data-attributes.suffix, '$')" as="xs:string"/>
  
  <!--================================-->
  <!--INIT-->
  <!--================================-->
  
  <!--FIXME : xspec : il faut matcher l'élément racine sinon xspec n'applique pas le bon mode-->
  <xsl:template match="/*">
    <xsl:apply-templates select="." mode="xfe:html2xml_clean"/>
  </xsl:template>
  
  <!--================================-->
  <!--MAIN-->
  <!--================================-->
  
  <xsl:template match="/*" mode="xfe:html2xml_clean">
    <xsl:message>[INFO] Applying "xfe:html2xml_clean" on <xsl:value-of select="els:getFileName(base-uri(.))"/></xsl:message>
    <xsl:copy>
      <xsl:apply-templates select="@* | node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!-- Deleting special tinyMce classes
    Ex : <div class="titre ms-selectedByNav"/> => <div class="titre"/> -->
  <xsl:template match="@class[contains(., $xfe:tinyMce.class.prefix)]" mode="xfe:html2xml_clean">
    <xsl:attribute name="class" select="tokenize(., '\s+')[not(starts-with(., $xfe:tinyMce.class.prefix))]"/>
  </xsl:template>
  
  <!--Deleting special tinyMce data attributes
    Ex : data-idbranche-ms="0" -->
  <xsl:template match="@*[matches(local-name(.), $xfe:tinyMce.data-attributes.regex)]" mode="xfe:html2xml_clean"/>
  
  <!--================================-->
  <!--COMMON-->
  <!--================================-->
  
  <!--Default copy-->
  <xsl:template match="node() | @*" mode="xfe:html2xml_clean">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
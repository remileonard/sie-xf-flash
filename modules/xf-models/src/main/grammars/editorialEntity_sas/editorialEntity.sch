<?xml version="1.0" encoding="UTF-8"?>
<!--<schema xmlns="http://www.ascc.net/xml/schematron">-->
<schema xmlns="http://purl.oclc.org/dsdl/schematron"
        xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
        xmlns:xs="http://www.w3.org/2001/XMLSchema"
        xmlns:saxon="http://saxon.sf.net/"
        xmlns:html="http://www.w3.org/1999/xhtml"
        xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
        xml:lang="en"
        queryBinding="xslt2"
        id="validation_editorialEntity">
  
  <!--fixme i18n sur les messages : cf. https://github.com/IDPF/epubcheck/issues/474 : utilisation de <diagnostic> avec pré-compilation ?-->
  
  <title>Schematron for Editorial Entity Structure</title>
  
  <ns prefix="xsl" uri="http://www.w3.org/1999/XSL/Transform"/>
  <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
  <ns prefix="saxon" uri="http://saxon.sf.net/"/>
  <ns prefix="html" uri="http://www.w3.org/1999/xhtml"/>
  <ns prefix="xf" uri="http://www.lefebvre-sarrut.eu/ns/xmlfirst"/>
  
  <xsl:key name="getElementById" match="*[@id]" use="@id"/>
  <xsl:key name="getElementByXfId" match="*[@xf:id]" use="@xf:id"/>
  <xsl:key name="getElementByXfIdRef" match="*[@xf:idRef]" use="@xf:idRef"/>
  
  <!--<xsl:variable name="referentials.uri" select="resolve-uri('../../../test/referentials/', static-base-uri())"/>
  <xsl:variable name="referentials.editorialEntityCode.xml" select="document(resolve-uri('editorialEntityCode.xml', $referentials.uri))"/>-->
  
  <!--=============================================================================-->
  <!--ID/IDREF-->
  <!--=============================================================================-->
  
  <pattern id="unique-xfId-constraint">
    <!--usefull in SAS format only (otherwise type = ID is enough)-->
      <rule context="*[@xf:id]">
         <assert test="count(key('getElementByXfId', current()/@xf:id)) = 1">
        @xf:id="<value-of select="@xf:id"/>" must be unique within document
      </assert>
      </rule>
  </pattern>
  
  <pattern id="unique-id-constraint">
      <rule context="*[@id]">
         <assert test="count(key('getElementById', current()/@id)) = 1">
        @id="<value-of select="@id"/>" must be unique within document
      </assert>
      </rule>
  </pattern>
  
	  <pattern id="sas_xfhistoricalId-constraint">
      <rule context="*[@xf:id[starts-with(., '{historicalId:')]][@xf:historicalId]">
         <let name="xfId" value="concat('{historicalId:', @xf:historicalId, '}')"/>
         <report test="@xf:id ne $xfId">
        if @xf:id must be equal to "<value-of select="$xfId"/>"
      </report>
      </rule>
  </pattern>
  
  <!--=============================================================================-->
  <!--LINKS-->
  <!--=============================================================================-->

  <pattern id="links_generic">
      <let name="rootName" value="local-name(/*)"/>
      <rule context="*[@xf:idRef] | xf:*[@idRef[not(starts-with(., '{xpath:'))]]">
         <let name="xf:targetResType" value="(@xf:targetResType, $rootName)[1]"/>
         <let name="xf:targetResId"
              value="(if (@xf:targetResId = /*/@xf:id) then ('.') else(@xf:targetResId), '.')[1]"/>
         <report test="($xf:targetResType != $rootName) and not(@xf:targetResId)">
        xf:targetResId missing, the target is not the same type of the current document ("<value-of select="$xf:targetResType"/>")
      </report>
         <report test="@xf:idRef          and ($xf:targetResType = $rootName)          and ($xf:targetResId = '.' and @xf:idRef)          and (count(key('getElementByXfId', @xf:idRef)) = 0)         ">
        @xf:idRef="<value-of select="@xf:idRef"/>" no matching @xf:id found within the current document 
      </report>
         <report test="@idRef          and ($xf:targetResType = $rootName)          and ($xf:targetResId = '.' and @idRef)          and (count(key('getElementById', @idRef)) = 0)         ">
        @idRef="<value-of select="@idRef"/>" no matching @id found within the current document 
      </report>
         <report test="@xf:idRef = @xf:targetResId">
        @xf:idRef is useless if it refers the target itself, please remove @xf:idRef (or change its value) 
      </report>
         <report test="@xf:idRef = $xf:targetResId">
        @xf:idRef refers the same xf:id as xf:targetResId
      </report>
      </rule>
      <rule context="*[@xf:refType        and not(self::*/@xf:idRef)        and not(self::xf:*[@idRef])]">
         <report test="not(@xf:targetResId)">
        Missing one of : @xf:targetResId or (xf:)idRef attribute
      </report>
      </rule>
  </pattern>
  
  <pattern id="link_specific_common">
      <rule context="xf:meta/xf:value[not(@as)]/xf:ref">
         <xsl:variable name="meta.ref.refTypes"
                       select="('xf:referenceItemRef', 'xf:relationRef')"
                       as="xs:string*"/>
         <assert test="@xf:refType = $meta.ref.refTypes">
        [SAS] meta/ref must have an xf:refType attribute with value = <value-of select="$meta.ref.refTypes"/>
         </assert>
      </rule>
  </pattern>
  
  <pattern id="link_specific_referenceNode">
      <rule context="xf:referenceNode/xf:ref">
         <assert test="@xf:refType = 'xf:referenceNode'">
        the attribute xf:refType of a referenceNode/ref must have the value "xf:referenceNode"
      </assert>
         <assert test="@xf:targetResId[. != root()/*/@xf:id]">
        referenceNode/ref must have xf:targetResId attribute
        (whose value is different from the current document xf:id)
      </assert>
         <assert test="(@xf:targetResType, 'editorialEntity')[1] = 'editorialEntity'">
        The target of a referenceNode/ref must be an editorialEntity
      </assert>
         <report test="@xf:idRef | @idRef">
        referenceNode/ref must not have an (xf:)idRef attribute
      </report>
      </rule>
  </pattern>
  
  <pattern id="link_specific_relation">
      <rule context="xf:relation">
         <xsl:variable name="link"
                       select="key('getElementByXfIdRef', @xf:id)"
                       as="element()*"/>
         <report test="count($link) = 0">
        Orphan relation : no link within the document
      </report>
      </rule>
  </pattern>
  
  <!--=============================================================================-->
  <!--COMMON-->
  <!--=============================================================================-->
  
  <pattern id="Common">
    <!--FIXME : cf FE-36-->
    <!--<rule context="xf:editorialEntity[@code]">
      <assert test="@code = $referentials.editorialEntityCode.xml//referenceItem/@id">
        EE code is not in the referential
      </assert>
    </rule>-->
      <rule context="xf:editorialEntity">
         <xsl:variable name="contentSubNodes"
                       select=".//xf:content/*[ancestor::xf:editorialEntity[1]/generate-id(.) = current()/generate-id(.)]"
                       as="element()*"/>
         <assert test="every $namespaceUri in $contentSubNodes/namespace-uri(.) satisfies $namespaceUri = $contentSubNodes[1]/namespace-uri(.)">
        Every contentNode content must be in the same namespace (same schema) within this editorialEntity 
      </assert>
      </rule>
      <rule context="xf:editorialEntity/xf:metadata">
         <let name="systemTitle" value="xf:meta[@code = 'systemTitle']"/>
         <let name="systemAbstract" value="xf:meta[@code = 'systemAbstract']"/>
         <assert test="count($systemTitle) = 1">
        editorialEntity must have exactly one meta "systemTitle" (found <value-of select="count($systemTitle)"/>)
      </assert>
         <assert test="count($systemAbstract) = 1">
        editorialEntity must have exactly one meta "systemAbstract" (found <value-of select="count($systemAbstract)"/>)
      </assert>
      </rule>
      <rule context="xf:content">
         <assert test="count(*) = 1">
        'content' must have exactly one child
      </assert>
      </rule>
      <rule context="xf:structuralNode/xf:title">
         <assert test="count(*) = 1 and html:div">
        Under 'title' content must be wrapped into a html div element
      </assert>
      </rule>
  </pattern>
  
  <!--=============================================================================-->
  <!--METADATA-->
  <!--=============================================================================-->
  
  <pattern id="meta.common">
      <rule context="xf:meta">
         <let name="code" value="@code"/>
         <assert test="count(preceding-sibling::xf:meta[@code = $code]) = 0">
        Meta with code "<value-of select="$code"/>" already setted
      </assert>
      </rule>
  </pattern>
  
  <pattern id="meta.common2">
      <rule context="xf:meta[@as][count(xf:value) gt 1]">
         <assert test="ends-with(@as, '+')">
        Meta with multiple values must have "as type" ending with "+" 
      </assert>
      </rule>
      <rule context="xf:meta[@as]/xf:value">
         <report test="not(@as)">
        Value must have @as attribute if meta has a @as attribute 
      </report>
      </rule>
  </pattern>
  
  <pattern id="meta.value.common">
      <rule context="xf:meta[@as]">
         <let name="metaAs" value="@as"/>
         <let name="metaType"
              value="if(ends-with(@as, '+')) then (substring-before(@as, '+')) else (@as)"/>
         <let name="authorized.metaTypes"
              value="('xs:integer', 'xs:float', 'xs:string', 'element(html:div)', 'element(html:span)', 'xs:dateTime', 'xs:boolean', 'xf:relationRef', 'xf:referenceItemRef', 'xf:verbalization')"/>
         <assert test="$metaType = $authorized.metaTypes">
        @as attribute "<value-of select="@as"/>" not recognized, it must be one of the following type : <value-of select="string-join($authorized.metaTypes,' ')"/> (+?)
      </assert>
      </rule>
      <rule context="xf:meta/xf:value[@as]">
         <let name="metaAs" value="parent::xf:meta/@as"/>
         <let name="metaType"
              value="if(ends-with($metaAs, '+')) then (substring-before($metaAs, '+')) else ($metaAs)"/>
         <assert test="@as = $metaType">
        This value must be of type "<value-of select="$metaType"/>" according to the parent meta type "<value-of select="$metaAs"/>"
      </assert>
      </rule>
  </pattern>
  
  <pattern id="meta.value.common.2">
      <rule context="xf:meta/xf:value[starts-with(@as, 'xs:')]">
         <let name="castableAsXsDatatype" value="concat('. castable as ', @as)"/>
         <assert test="saxon:evaluate($castableAsXsDatatype)">
        A meta whose type is "<value-of select="@as"/>" must have a value which is castable as "<value-of select="@as"/>"
      </assert>
         <report test="*">
        A meta whose type is "<value-of select="@as"/>" must only have text node content
      </report>
      </rule>
      <rule context="xf:meta/xf:value[starts-with(@as, 'element(')]">
         <let name="elementType" value="replace(@as, '^element\((.*?)\)\+?$', '$1')"/>
         <let name="xpathTest" value="concat('*/self::', $elementType)"/>
         <assert test="saxon:evaluate($xpathTest)">
        A meta whose type is "<value-of select="@as"/>" must be an element "<value-of select="$elementType"/>"
      </assert>
      </rule>
      <rule context="xf:meta[@code = 'systemAbstract']/xf:value/html:div//html:a[@xf:idRef]">
         <report test="true()">
        XMLfirst links are not allowed within systemAbstract meta
      </report>
      </rule>
  </pattern>
  
  <!--Datatype must be enforced by specific rules so it can be properly loaded in the ECM database-->
  <pattern id="meta.value_specific.1">
      <rule context="xf:meta/xf:value[@as = 'xs:dateTime']">
         <assert test="matches(., '^\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\dZ$')">
        meta of type <value-of select="@as"/> must be formatted with Z as timeZone, as expected by SolR (e.g. 2000-01-20T00:00:00Z)
      </assert>
      </rule>
      <rule context="xf:meta/xf:value[@as = 'xs:boolean']">
         <assert test="matches(., '^(true|false)$')">
        meta of type <value-of select="@as"/> must be formatted true/false (not 0 or 1)
      </assert>
      </rule>
  </pattern>
  
  <pattern id="meta.value_specific.2">
    <!-- mricaud : this was a test to illustrate one can define complexe meta with specific rules
      <rule context="xf:meta/xf:value[starts-with(@as, 'els:period')]">
      <let name="startDate" value="xf:field[@name = 'startDate'] cast as xs:date"/>
      <let name="endDate" value="xf:field[@name = 'endDate'] cast as xs:date"/>
      <assert test="count(xf:field) = 2 and count(xf:field[@as='xs:date']) = 2 and xf:field/@name = 'startDate' and xf:field/@name = 'endDate'">
        A meta whose type is "<value-of select="@as"/>" must have 2 xs:date fields named "startDate" and "enDate" 
      </assert>
      <assert test="$endDate gt $startDate ">
        endDate must be after startDate in the time
      </assert>
    </rule>-->
      <rule context="xf:meta/xf:value[@as = 'element(html:span)']">
         <assert test="count(*) = 1 and html:span">
        An html:span element is compuslsary for meta whose type is "<value-of select="@as"/>"
      </assert>
      </rule>
      <rule context="xf:meta/xf:value[@as = 'element(html:div)']">
         <assert test="count(*) = 1 and html:div">
        An html:div element is compuslsary for meta whose type is "<value-of select="@as"/>"
      </assert>
      </rule>
  </pattern>
  
  <pattern id="meta.value_specific.referenceItem">
      <rule context="xf:meta/xf:value[@as = 'xf:referenceItemRef']">
         <assert test="following-sibling::*[1]/self::xf:verbalization">
        Element verbalization is compulsory after value of meta whose type is "<value-of select="@as"/>"
      </assert>
         <assert test="xf:ref">
        meta of type "<value-of select="@as"/>" must have an element ref as value 
      </assert>
      </rule>
      <rule context="xf:meta/xf:value[@as = 'xf:referenceItemRef']/xf:ref">
         <assert test="@xf:targetResType = 'referential'">
        meta of type <value-of select="../@as"/> must have a ref with @xf:targetResType="referential" 
        (it is not inherited from the current document, which is an editorialEntity here)
      </assert>
         <assert test="@xf:refType = 'xf:referenceItemRef'">
        meta of type <value-of select="../@as"/> must have a ref with @xf:refType="xf:referenceItemRef"
      </assert>
         <assert test="@idRef">
        meta of type <value-of select="../@as"/> must have a ref with @idRef
      </assert>
         <report test="starts-with(@xf:targetResId, '{{')">
        meta of type <value-of select="../@as"/> must not have a value with AVT {...}
      </report>
         <report test="starts-with(@idRef, '{{')">
        meta of type <value-of select="../@as"/> must not have a value with AVT {...}
      </report>
      </rule>
  </pattern>
  
  <pattern id="meta.value_specific.relationRef">
      <rule context="xf:meta/xf:value[@as = 'xf:relationRef']/xf:ref">
         <assert test="(@xf:targetResType, 'editorialEntity')[1] = 'editorialEntity'">
        meta of type xf:relationRef must have an xf:targetResType="editorialEntity" if present 
        (if not it is inherited from the current document, which is an editorialEntity here)
      </assert>
         <assert test="@xf:refType = 'xf:relationRef'">
        meta of type xf:relationRef must have an xf:refType="xf:relationRef"
      </assert>
         <report test="starts-with(@xf:targetResId, '{{')">
        meta of type xf:relationRef must not have a value with {...}
      </report>
         <report test="starts-with(@xf:idRef, '{{')">
        meta of type xf:relationRef must not have a value with {...}
      </report>
      </rule>
  </pattern>
  
  <pattern id="meta.field">
      <rule context="xf:meta/xf:value/xf:field[starts-with(@as, 'xs:')]">
         <let name="castableAsXsDatatype" value="concat('. castable as ', @as)"/>
         <assert test="saxon:evaluate($castableAsXsDatatype)">
        A field whose type is "<value-of select="@as"/>" must have a value which is castable as "<value-of select="@as"/>"
      </assert>
      </rule>
  </pattern>
  
</schema>

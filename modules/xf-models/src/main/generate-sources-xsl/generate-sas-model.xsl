<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
	xmlns:nvdl="http://purl.oclc.org/dsdl/nvdl/ns/structure/1.0"
	xmlns:rng="http://relaxng.org/ns/structure/1.0"
	xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0"
	xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
	exclude-result-prefixes="#all"
	version="2.0">
	
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Generation a SAS version on the XF model (editorialEntity.nvdl and linked schemas) by transforming the ECM version of the xf model.</xd:p>
      <xd:p>This XSLT is launch with maven (xml-maven-plugin), see the pom.xml file</xd:p>
    </xd:desc>
  </xd:doc>
  
  <xsl:output indent="yes"/>
  
	<!--<xsl:param name="sas.prefix" select="'sas_'" as="xs:string"/>-->
	<xsl:param name="relPathToOriginalModel" select="'../editorialEntity/'" as="xs:string"/>
  
  <xsl:variable name="baseUri.string" select="string(base-uri())" as="xs:string"/>
  <xsl:variable name="fileName" select="els:getFileName($baseUri.string)" as="xs:string"/>
	
	<!--================================================-->
	<!-- MAIN -->
	<!--================================================-->
	
	<xsl:template match="/">
	  <xsl:message>[INFO] Applying <xsl:value-of select="tokenize(static-base-uri(), '/')[last()]"/> on <xsl:value-of select="$fileName"/></xsl:message>
	  <!--<xsl:message select="system-property('xsl:version')">Version</xsl:message>
	  <xsl:message select="system-property('xsl:vendor')">Vendor</xsl:message>-->
		<xsl:choose>
			<xsl:when test="$fileName = 'editorialEntity.nvdl'">
				<xsl:apply-templates mode="editorialEntity.nvdl"/>
			</xsl:when>
			<xsl:when test="$fileName = 'editorialEntity.rng'">
				<xsl:apply-templates mode="editorialEntity.rng"/>
			</xsl:when>
			<xsl:when test="$fileName = 'xf.attributes.rng'">
				<xsl:apply-templates mode="xf.attributes.rng"/>
			</xsl:when>
			<xsl:when test="$fileName = 'xf-id.attributes.rng'">
				<xsl:apply-templates mode="xf-id.attributes.rng"/>
			</xsl:when>
			<xsl:when test="$fileName = 'xf-idRef.attributes.rng'">
				<xsl:apply-templates mode="xf-idRef.attributes.rng"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:apply-templates/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<!--================================================-->
	<!-- MODE = editorialEntity.rng -->
	<!--================================================-->
	
	<xsl:template match="rng:grammar" mode="editorialEntity.rng">
	  <xsl:variable name="docOverride" select="document('editorialEntity.sas.override.rng')" as="document-node()"/>
		<xsl:copy copy-namespaces="yes">
			<xsl:apply-templates select="@*|node()" mode="#current"/>
			<xsl:comment>================= OVERRIDE =================</xsl:comment>
		  <xsl:comment>From <xsl:value-of select="base-uri($docOverride)"/></xsl:comment>
			<xsl:copy-of select="$docOverride//rng:div[@xml:id='sas-override']"/>
		</xsl:copy>
	</xsl:template>
	
  <xsl:template match="rng:define[@name = 'referenceNode']//rng:ref[@name='referenceNode.verbalization']" mode="editorialEntity.rng">
    <optional xmlns="http://relaxng.org/ns/structure/1.0">
	    <a:documentation xml:lang="en">Verbalization is optional in SAS format</a:documentation>
	    <xsl:next-match/>
	  </optional>
	</xsl:template>
  
  <!--================================================-->
	<!-- MODE = editorialEntity.nvdl -->
	<!--================================================-->
	<!--Les fichiers rnc sont les seuls qui ne sont pas copié (car non html), donc on va re-pointer sur les originaux-->
	<xsl:template match="nvdl:validate/@schema[. = 'html5.div.rnc']" mode="editorialEntity.nvdl">
		<xsl:attribute name="{local-name()}" select="concat($relPathToOriginalModel, .)"/>
	</xsl:template>
	
	<xsl:template match="nvdl:validate/@schema[. = 'html5.span.rnc']" mode="editorialEntity.nvdl">
		<xsl:attribute name="{local-name()}" select="concat($relPathToOriginalModel, .)"/>
	</xsl:template>
	
	<!--================================================-->
	<!-- MODE = xf.attributes.rng -->
	<!--================================================-->
	
	<!--<xsl:template match="rng:externalRef/@href[. = ('xf-id.attributes.rng', 'xf-idRef.attributes.rng')]" mode="xf.attributes.rng">
		<xsl:attribute name="{local-name()}" select="concat($sas.prefix, .)"/>
	</xsl:template>-->
	
	<!--================================================-->
	<!-- MODE = xf-idRef.attributes.rng -->
	<!--================================================-->
	
	<xsl:template match="rng:attribute[@name = 'xf:id']/rng:data[@type = 'ID']" mode="xf-id.attributes.rng">
		<xsl:copy copy-namespaces="no">
			<xsl:attribute name="type" select="'string'"/>
			<param name="pattern" xmlns="http://relaxng.org/ns/structure/1.0">
				<xsl:text>(\{(historicalId|query):.*\}|[\i-[:]][\c-[:]]*)</xsl:text>
				<xsl:comment>[\i-[:]][\c-[:]]* = NCNAME (cf. xsd de xsd)</xsl:comment>
			</param>
		</xsl:copy>
	</xsl:template>
	
	<!--================================================-->
	<!-- MODE = xf-idRef.attributes.rng -->
	<!--================================================-->
	
  <xsl:template match="rng:attribute[@name = ('xf:idRef', 'xf:targetResId')]/rng:data[@type = 'NCName']" mode="xf-idRef.attributes.rng">
		<xsl:copy copy-namespaces="no">
			<xsl:attribute name="type" select="'string'"/>
			<param name="pattern" xmlns="http://relaxng.org/ns/structure/1.0">
				<xsl:text>(\{(historicalId|query):.*\}|[\i-[:]][\c-[:]]*)</xsl:text>
				<xsl:comment>[\i-[:]][\c-[:]]* = NCNAME (cf. xsd de xsd)</xsl:comment>
			</param>
		</xsl:copy>
	</xsl:template>
  
  <xsl:template match="rng:attribute[@name = 'idRef']/rng:data[@type = 'NCName']" mode="xf-idRef.attributes.rng">
    <xsl:copy copy-namespaces="no">
      <xsl:attribute name="type" select="'string'"/>
      <param name="pattern" xmlns="http://relaxng.org/ns/structure/1.0">
        <xsl:text>(\{(xpath):.*\}|[\i-[:]][\c-[:]]*)</xsl:text>
        <xsl:comment>[\i-[:]][\c-[:]]* = NCNAME (cf. xsd de xsd)</xsl:comment>
      </param>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="rng:choice[rng:attribute[@name = 'idRef']]/a:documentation[@xml:lang='en'][last()]" mode="xf-idRef.attributes.rng">
    <xsl:copy copy-namespaces="no">
      <xsl:apply-templates select="@* | node()" mode="#current"/>
      <xsl:text>&#10;</xsl:text>
      <xsl:text>[SAS] In SAS format, those attribute may be expressed as AVT (Attribute Value Template) with starting string : 
        - for @xf:id "query:codeMeta1=val1&amp;codeeMeta2=val2" (one may also use : parent.codeTR.codeMeta=val and enfant.codeTR.codeMeta=val)
        - for @id  : "xpath:..." any xpath to a node which has an @id</xsl:text>
    </xsl:copy>
  </xsl:template>
	
	<!--================================================-->
	<!-- COMMON -->
	<!--================================================-->
	
	<xsl:template match="node() | @*" mode="#all">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" mode="#current"/>
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>
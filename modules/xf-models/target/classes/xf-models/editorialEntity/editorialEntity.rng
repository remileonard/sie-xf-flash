<?xml version="1.0" encoding="utf-8"?>
<grammar 
  xmlns="http://relaxng.org/ns/structure/1.0"
  xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0"
  datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes"
  ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst">
  
  
  <!--================================================-->
  <!--                START-MAIN                      -->
  <!--================================================-->
  
  <start>
    <ref name="editorialEntity"/>
  </start>
  
  <div>
    <a:documentation xml:lang="en">Root element for an Editorial Entity</a:documentation>
    <define name="editorialEntity">
      <element name="editorialEntity">
        <ref name="xf-id.attribute"/>
        <ref name="systemMeta-common.attributes"/>
        <ref name="systemMeta-code.attribute">
          <a:documentation xml:lang="en">Code attribute for an EditorialEntity is defined in the "Template for Editorial Entity" (TEE)</a:documentation>
        </ref>
        <ref name="editorialEntity.status.attribute"/>
        <ref name="editorialEntity.archived.attribute"/>
        <ref name="editorialEntity.version.attribute"/>
        <ref name="editorialEntity.ownerGroup.attribute"/>
        <ref name="metadata"/>
        <ref name="body"/>
        <ref name="relations"/>
      </element>
    </define>
    
    <define name="editorialEntity.status.attribute">
      <attribute name="status">
        <a:documentation xml:lang="en">System meta "status" used in ECM workflow (e.g. status="draft", status="validate", etc.)</a:documentation>
        <data type="NCName"/>
      </attribute>
    </define>
    
    <define name="editorialEntity.archived.attribute">
      <attribute name="archived">
        <a:documentation xml:lang="en">System meta "archived" used in ECM to say an editorialEntity is used or is not used anymore</a:documentation>
        <data type="boolean"/>
      </attribute>
    </define>
    
    <define name="editorialEntity.version.attribute">
      <attribute name="version">
        <a:documentation xml:lang="en">System meta "version" used in the ECM to keep a trace of each version of the editorialEntity</a:documentation>
        <data type="string"/>
      </attribute>
    </define>
    
    <define name="editorialEntity.ownerGroup.attribute">
      <attribute name="ownerGroup">
        <a:documentation xml:lang="en">System meta "ownerGroup" used in the ECM to say which group of user is responsible of the editorialEntity</a:documentation>
        <a:documentation xml:lang="en">Typically : each kind of editorialEntity (defined by @code) has one ownerGroup. e.g : "Rédaction Affaire" owned EE with code="AFF"</a:documentation>
        <data type="NCName"/>
      </attribute>
    </define>
  
  </div>
  
  <!--================================================-->
  <!--                    BODY                        -->
  <!--================================================-->
  
  <div>
    
    <define name="body">
      <element name="body">
        <a:documentation xml:lang="en">Body block of the editorialEntity</a:documentation>
        <ref name="body.subNodes"/>
      </element>
    </define>
    
    <define name="body.subNodes">
      <zeroOrMore>
        <choice>
          <ref name="structuralNode"/>
          <ref name="referenceNode"/>
          <ref name="queryNode"/>
          <ref name="contentNode"/>
        </choice>
      </zeroOrMore>
    </define>
    
  </div>
  <!--================================================-->
  <!--              STRUCTURAL NODE                    -->
  <!--================================================-->
  
  <div>
    
    <define name="structuralNode">
      <element name="structuralNode">
        <a:documentation xml:lang="en">StructuralNode : a section, a chapter, etc. It has a title which has to be in html</a:documentation>
        <a:documentation xml:lang="en">StructuralNode @code attribute : has to be defined in the TEE (07/10/2016 : today it's list of values, later it will be bound to a referential)</a:documentation>
        <a:documentation xml:lang="en">StructuralNode @num attribute : is used for numeration of the title (it can be a digit, a letter, etc.) </a:documentation>
        <ref name="xf-id.attribute"/>
        <ref name="systemMeta-common.attributes"/>
        <ref name="xf-common.attribute"/>
        <ref name="systemMeta-num.attribute"/>
        <ref name="systemMeta-active.attribute"/>
        <optional>
          <attribute name="type">
            <data type="NCName"/>
          </attribute>
        </optional>
        <ref name="metadata"/>
        <!--FE-41 : on autorise les structuralNode vides-->
        <optional>
          <ref name="structuralNode.title"/>
        </optional>
        <optional>
          <ref name="structuralNode.subNodes"/>
        </optional>
      </element>
    </define>
    
    <define name="structuralNode.subNodes">
      <oneOrMore>
        <choice>
          <ref name="structuralNode"/>
          <ref name="referenceNode"/>
          <ref name="queryNode"/>
          <ref name="contentNode"/>
        </choice>
      </oneOrMore>
    </define>
    
    <define name="structuralNode.title">
      <element name="title">
        <a:documentation xml:lang="en">Title of a structural node (html inside)</a:documentation>
        <ref name="blockContent"/>
      </element>
    </define>
    
  </div>
  
  <!--================================================-->
  <!--              REFERENCE NODE                    -->
  <!--================================================-->
  
  <define name="referenceNode">
    <element name="referenceNode">
      <a:documentation xml:lang="en">ReferenceNode permit to bind another editorialEntity to the current one (it works like xi:include)</a:documentation>
      <a:documentation xml:lang="en">At publication time referenceNode will be (in most cases) resolved : the content of the referenced editorialEntity will be inserted in the current one, so we have finally a full document.</a:documentation>
      <ref name="xf-id.attribute"/>
      <ref name="systemMeta-common.attributes"/>
      <ref name="systemMeta-active.attribute"/>
      <ref name="systemMeta-code.attribute">
        <a:documentation xml:lang="en">Code attribute for a referenceNode is a "Template for Relation" (TR) code</a:documentation>
      </ref>
      <ref name="systemMeta-num.attribute"/>
      <ref name="metadata"/>
      <ref name="ref.element"/>
      <ref name="referenceNode.verbalization"/>
    </element>
  </define>
  
  <define name="referenceNode.verbalization">
    <element name="verbalization">
      <a:documentation xml:lang="en">Verbalization of the referenceNode in HTML.</a:documentation>
      <a:documentation xml:lang="en">The verbalization will be added by the ECM by copying the systemTitle meta of the referenced EE</a:documentation>
      <ref name="blockContent"/>
    </element>
  </define>
  
  <!--================================================-->
  <!--              QUERY NODE                        -->
  <!--================================================-->
  
  <define name="queryNode">
    <element name="queryNode">
      <a:documentation xml:lang="en">QueryNode is referenceNode, except that we don't know the target document, it is the result of a dynamic query.</a:documentation>
      <a:documentation xml:lang="en">The query (expressed in a - undefined - generic format) can grap several documents.</a:documentation>
      <ref name="xf-id.attribute"/>
      <ref name="systemMeta-common.attributes"/>
      <ref name="systemMeta-active.attribute"/>
      <ref name="metadata"/>
      <element name="queryString">
        <data type="string"/>
      </element>
    </element>
  </define>
  
  <!--================================================-->
  <!--              CONTENT NODE                      -->
  <!--================================================-->
  
  <div>

    <define name="contentNode">
      <element name="contentNode">
        <a:documentation xml:lang="en">ContentNode is used to insert XML fragments from any grammar (typically publisher's specific DTDs)</a:documentation>
        <ref name="xf-id.attribute"/>
        <ref name="systemMeta-common.attributes"/>
        <ref name="systemMeta-active.attribute"/>
        <ref name="metadata"/>
        <ref name="content"/>
      </element>
    </define>
    
    <define name="content">
      <element name="content">
        <a:documentation xml:lang="en">Content is a wrapper to insert the XML fragment </a:documentation>
        <a:documentation xml:lang="en">Only one XML element is allowed as child</a:documentation>
        <a:documentation xml:lang="en">This Relax NG allow anything as child, but the NVDL script will check each grammars depending on the namespace</a:documentation>
        <ref name="anyElementExceptXmlFirst"/>
      </element>
    </define>

  </div>
  <!--================================================-->
  <!--                RELATIONS                        -->
  <!--================================================-->
  
  <div>
    
    <define name="relations">
      <element name="relations">
        <a:documentation xml:lang="en">Block element wrapper for each relation</a:documentation>
        <zeroOrMore>
          <ref name="relation"/>
        </zeroOrMore>
      </element>
    </define>
    
    <define name="relation">
      <element name="relation">
        <a:documentation xml:lang="en">A relation is referenced on "link" elements from the contentNode. Because it has metadata, it would have been too intrusive to describe a full relation directly in the XML content.</a:documentation>
        <a:documentation xml:lang="en">A relation starts from the current editorialEntity and has for target another editorialEntity or an element (width xf:id) inside a contentNode of another editorialEntity </a:documentation>
        <a:documentation xml:lang="en">NB : a referenceNode is a relation itself, so it's not represented in the relation block</a:documentation>
        <ref name="xf-id.attribute"/>
        <ref name="metadata"/>
        <ref name="systemMeta-code.attribute">
          <a:documentation xml:lang="en">Code attribute on a relation must be a "Template of Relation" (TR) code</a:documentation>
        </ref>
        <attribute name="calculated">
          <data type="boolean"/>
        </attribute>
        <ref name="ref.element"/>
      </element>
    </define>
  
  </div>
  
  <!--================================================-->
  <!--                METADATA                        -->
  <!--================================================-->
  
  <div>
    
    <define name="metadata">
      <element name="metadata">
        <a:documentation xml:lang="en">Block wrapper elements for all metadata of a node</a:documentation>
        <a:documentation xml:lang="en">Other metadata can be found as specific attributes of each node : they are called "system metadata"</a:documentation>
        <zeroOrMore>
          <ref name="meta"/>
        </zeroOrMore>
      </element>
    </define>
    
    <define name="meta">
      <element name="meta">
        <a:documentation xml:lang="en">One metadata describe with a generic manner : one or more value element with a text inside (if the metadata is atomic) or element (e.g. : html metadata, referenced metadata with a ref element)</a:documentation>
        <a:documentation xml:lang="en">Each metadata is fully described in the "Template for Editorial Entity" (TEE), which is not described as an XML file but a SQL database in ECM</a:documentation>
        <a:documentation xml:lang="en">Most attributes are copied from this TEE, only @code is actually important.</a:documentation>
        <a:documentation xml:lang="en">meta/@code is use to identify the meta. It works like an id/idRef relation to declaration of that meta in the TEE</a:documentation>
        <ref name="xf-id.attribute"/>
        <ref name="systemMeta-code.attribute">
          <a:documentation xml:lang="en">Code attribute for a meta is a meta code which is defined in the "Template for Editorial Entity" (TEE)</a:documentation>
        </ref>
        <ref name="meta.label.attribute"/>
        <ref name="meta.group.attribute"/>
        <ref name="as.attribute"/>
        <!-- Sera géré par l'ECM
          <optional>
          <!-\-Permet de savoir si la méta est éditable ou non-\->
          <attribute name="generated">
            <data type="boolean"/>
          </attribute>
        </optional>-->
        <oneOrMore>
          <ref name="meta.value"/>
          <optional>
            <ref name="meta.value.verbalization"/>
          </optional>
        </oneOrMore>
      </element>
    </define>
    
    <define name="meta.label.attribute">
      <a:documentation xml:lang="en">meta/@label is used to verbalize (any language) the metadata. It comes from the description in the TEE</a:documentation>
      <attribute name="label">
        <data type="string"/>
      </attribute>
    </define>
    
    <define name="meta.group.attribute">
      <attribute name="group">
        <!--fixme : schematron => referentiel + TEE check -->
        <a:documentation xml:lang="en">meta/@group define the user's group allowed to manage this meta in the TEE</a:documentation>
        <data type="NCName"/>
      </attribute>
    </define>
    
    <define name="meta.value">
      <element name="value">
        <a:documentation xml:lang="en">block wrapper element to insert the value of the meta. The meta can have multiple values (multi-valueted meta). Each value is used like this :</a:documentation>
        <a:documentation xml:lang="en">- atomic value : text inside directly</a:documentation>
        <a:documentation xml:lang="en">- compound value : element field with @name</a:documentation>
        <a:documentation xml:lang="en">- ref value : element ref with @xf:idRef (to a referential element or relation to another editorialEntity)</a:documentation>
        <!--<ref name="xf-id.attribute"/>-->
        <ref name="as.attribute"/>
        <choice>
          <!--quand la valeur est directement dans la méta (type atomic "xs:boolean" par exemple)-->
          <!--<text/> => no because text pattern allow empty string-->
          <data type="string">
            <param name="pattern">.+</param>
          </data>
          <!--quand la valeur doit être exprimée avec plusieurs champs (ex : période = dateDebut + dateFin)-->
          <oneOrMore>
            <ref name="meta.value.field"/>
          </oneOrMore>
          <!--quand la valeur doit être exprimée avec du contenu mixte-->
            <!--span-->
            <ref name="inlineContent"/>
            <!-- ou div notamment pour la méta systemAbstract qui peut contenir plusieurs p-->
            <ref name="blockContent"/>
          <!--quand la valeur est une référence : lien-->
          <ref name="ref.element"/>
        </choice>
      </element>
    </define>
    
    <define name="meta.value.field">
      <element name="field">
        <a:documentation xml:lang="en">Field is used for compound metadata (e.g. startDate, endDate if the meta is a period)</a:documentation>
        <optional>
          <ref name="xf-id.attribute"/>
        </optional>
        <attribute name="name">
          <data type="NCName"/>
        </attribute>
        <ref name="as.attribute"/>
        <data type="string"/>
      </element>
    </define>
    
    <define name="meta.value.verbalization">
      <element name="verbalization">
        <a:documentation xml:lang="en">Verbalization of the meta in natural language (from the TEE)</a:documentation>
        <a:documentation xml:lang="en">The verbalization is applied to the first preceding meta element</a:documentation>
        <ref name="inlineContent"/>
      </element>
    </define>
    
  </div>
  
  <!--================================================-->
  <!--                BLOCK CONTENT                    -->
  <!--================================================-->
  
  <div>
    <define name="blockContent">
      <ref name="html.div"/>
    </define>
    
    <define name="html.div">
      <element name="div" ns="http://www.w3.org/1999/xhtml">
        <ref name="anyHtmlElement"/>
      </element>
    </define>
  </div>
  
  <!--================================================-->
  <!--                MIXED CONTENT                    -->
  <!--================================================-->
  
  <div>
    <!--Le inline content est défini en html (ne pas ré-inventer la roue)-->
    <define name="inlineContent">
      <ref name="html.span"/>
    </define>
    
    <define name="html.span">
      <element name="span" ns="http://www.w3.org/1999/xhtml">
        <ref name="anyHtmlElement"/>
      </element>
    </define>
    
    <define name="html.a">
      <element name="a" ns="http://www.w3.org/1999/xhtml">
        <ref name="anyHtmlElement"/>
        <!--<empty/> fixme : le xml doit-il être valide RNG en mode "attach"-->
      </element>
    </define>
  </div>
  
  <!--================================================-->
  <!--                    COMMON                      -->
  <!--================================================-->
  
  <div>

    
    <define name="systemMeta-common.attributes">
      <ref name="systemMeta-creationUser.attribute"/>
      <ref name="systemMeta-creationDateTime.attribute"/>
      <ref name="systemMeta-lastModificationDateTime.attribute"/>
      <ref name="systemMeta-lastModificationUser.attribute"/>
    </define>
    
    <define name="systemMeta-creationUser.attribute">
      <attribute name="creationUser">
        <a:documentation xml:lang="en">User that has created the editorialEntity</a:documentation>
        <data type="NCName"/>
      </attribute>
    </define>
    
    <define name="systemMeta-creationDateTime.attribute">
      <attribute name="creationDateTime">
        <a:documentation xml:lang="en">Date and time at which the editorialEntity has been first created</a:documentation>
        <data type="dateTime"/>
      </attribute>
    </define>
    
    <define name="systemMeta-lastModificationDateTime.attribute">
      <attribute name="lastModificationDateTime">
        <a:documentation xml:lang="en">Date and time at which the editorialEntity has been last modified</a:documentation>
        <data type="dateTime"/>
      </attribute>
    </define>
    
    <define name="systemMeta-lastModificationUser.attribute">
      <attribute name="lastModificationUser">
        <a:documentation xml:lang="en">User that has last modified the editorialEntity</a:documentation>
        <data type="NCName"/>
      </attribute>
    </define>

    <define name="systemMeta-code.attribute">
      <attribute name="code">
        <a:documentation xml:lang="en">Code attribute is used to identify a kind of element (meta, node, ...) from the TEE configuration. 
          Its value is a code which works like an ID/IDREF relation to the "Template for Editorial Entity" (TEE) or Template for relation (TR) depending on the current element.</a:documentation>
        <data type="NCName"/>
      </attribute>
    </define>

    <define name="systemMeta-active.attribute">
      <optional>
        <attribute name="active">
          <a:documentation xml:lang="en">Permit to make a node inactive in the ECM, without really deleting it.</a:documentation>
          <data type="boolean"/>
        </attribute>
      </optional>
    </define>

    <define name="systemMeta-num.attribute">
      <optional>
        <attribute name="num">
          <a:documentation xml:lang="en">Numerotation of an node (e.g. a chapter in a book when applied to a structuralNode, a position of a subpart when applied to a referenceNode)</a:documentation>
          <data type="string"/>
        </attribute>
      </optional>
    </define>
    
    <define name="ref.element">
      <element name="ref">
        <a:documentation xml:lang="en">A common element to make an reference to another node (when not in HTML). Each ref (or a element if html) has 3 attribute :</a:documentation>
        <a:documentation xml:lang="en">- xf:idRef : id of the target</a:documentation>
        <a:documentation xml:lang="en">- xf:base (optional) : name or "path" of the document where is the target. 
          It can also be a filtered path (as REST uri), so there's no need to search the whole database (e.g. xf:base="/xmlFirst/EE/DALLOZ" or xf:base="/xmlFirst/REFERENTIALS")</a:documentation>
        <a:documentation xml:lang="en">- xf:refType (optional) : normalize value (so it can be queried) to describe the reference.</a:documentation>
        <ref name="xf-idRef.attributes"/>
        <ref name="ref.subNodes"/>
      </element>
    </define>
    
    <define name="ref.subNodes">
      <empty/>
    </define>

    <define name="as.attribute">
      <attribute name="as">
        <a:documentation xml:lang="en">Works like xsl @as attribute, is used to make typed values (schematron controlled)</a:documentation>
        <data type="string">
          <param name="pattern">((xs|els|xf):[a-zA-Z]+|element\(html:(span|div)\))\+?</param>
        </data>
      </attribute>
    </define>
    
    <define name="xf-common.attribute">
      <externalRef href="xf-common.attributes.rng"/> <!-- mis dans un fichier à part car utilisé aussi sur les contentNode lors de la validation NVDL-->
    </define>
    
    <define name="xf-id.attribute">
      <!-- mis dans un fichier à part car utilisé aussi sur les contentNode lors de la validation NVDL-->
      <externalRef href="xf-id.attributes.rng"/>
    </define>
    
    <define name="xf-idRef.attributes">
      <!-- mis dans un fichier à part car utilisé aussi sur les contentNode lors de la validation NVDL-->
      <externalRef href="xf-idRef.attributes.rng"/>
    </define>
    
    <define name="anyElementExceptXmlFirst">
      <zeroOrMore>
        <choice>
          <element>
            <anyName>
              <except>
                <nsName ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"/>
              </except>
            </anyName>
            <ref name="anyElementExceptXmlFirst"/>
          </element>
          <attribute>
            <anyName>
              <!--Ici il est nécessaire d'exclure les attributs id/idRef du namespace xmlFirst sinon conflit rng
              (cf. http://lists.xml.org/archives/xml-dev/200710/msg00067.html)-->
              <!--Finalement l'unicité des id étant vérifier en schematron, ce n'est plus nécessaire d'exclure ces attributs 
              (et ça permet de rendre les fichier valides RNG sans passer par le NVDL)-->
              <!--<except>
                <name ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst">id</name>
                <name ns="http://www.lefebvre-sarrut.eu/ns/xmlfirst">idRef</name>
              </except>-->
            </anyName>
          </attribute>
          <text/>
        </choice>
      </zeroOrMore>
    </define>
    
    <define name="anyHtmlElement">
      <zeroOrMore>
        <choice>
          <element ns="http://www.w3.org/1999/xhtml">
            <nsName/>
            <ref name="anyHtmlElement"/>
          </element>
          <attribute>
            <anyName/>
          </attribute>
          <text/>
        </choice>
      </zeroOrMore>
    </define>
  
  </div>
  
</grammar>
# XMLFirst (FLASH) Models 

This repo contains 2 main Flash models :

## Editorial Entity
An editorial entity represents any kind of document unit. It has 3 main parts:

- **Metadata** : with all metadata expressed in a generic grammar

- **Body** : with 4 kind of subnodes

- - structuralNode : a chapter, a section, etc. It always has a title

- - contentNode : XML specific content (depends on the publisher's model)

- - referenceNode : inclusion of another editorialEntity by its id

- - QueryNode : inclusion of another editorialEntity by a query

- **Relations** : List of all relations with other editorialEntity

The schema consist of an editorialEntity.nvdl file which refers to : 

- editorialEntity.rng : The main structure

- editorialEntity.sch : a schematron

- div.rnc an span.rnc : html5 validation (meta, structuralNode title)

- rng specific files : publisher's specific models (loaded from another repo)

It is used within the ECM of Flash. There is a SAS version which is generated from the original schema (generate-source maven plugin). It is a bit different : 

- referenceNode may contain another editorialEntity : so it is possible to load sereval editorialEntity at the same time

- some meta attributes become optionnals (they will be added during import phase in the ECM)

## Referentials
A generic way of representing referentials inside Flash.
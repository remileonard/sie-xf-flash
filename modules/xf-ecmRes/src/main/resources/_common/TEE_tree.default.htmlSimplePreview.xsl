<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns="http://www.w3.org/1999/xhtml"
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  exclude-result-prefixes="#all"
  version="2.0">

  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Generate a HTML simple preview for EE type tree.</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!--indent="no" is important for xspec diff--> 
  <xsl:output method="xhtml" indent="no"/>
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfRes:htmlSimplePreview"/>
  </xsl:template>
  
  <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
  
  <xsl:template match="/" mode="xfRes:htmlSimplePreview">
    <html>
      <head>
        <title><xsl:value-of select="els:getFileName(base-uri(.))"/></title>
      </head>
      <xsl:apply-templates select="editorialEntity/body" mode="#current"/>
    </html>
  </xsl:template>
  
  <xsl:template match="body" mode="xfRes:htmlSimplePreview">
    <body>
      <ul>
        <xsl:apply-templates mode="#current"/>
      </ul>
    </body>
  </xsl:template>
  
  <xsl:template match="structuralNode" mode="xfRes:htmlSimplePreview">
    <xsl:variable name="structuralNode.level" select="count(ancestor::structuralNode) + 1" as="xs:integer"/>
    <xsl:variable name="htmlElement.level" select="if($structuralNode.level le 6) then($structuralNode.level) else(6)" as="xs:integer"/>
    <xsl:variable name="title.div" as="node()*">
      <xsl:apply-templates select="title/html:div" mode="#current"/>
    </xsl:variable>
    <li class="structuralNode level-{$structuralNode.level}">
      <span><!--(this span make easier to indent the structure)-->
        <xsl:choose>
          <xsl:when test="normalize-space(string($title.div)) != ''">
            <xsl:copy-of select="$title.div/node()"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>[NO TITLE]</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
      </span>
      <xsl:if test="* except (metadata | title)">
        <ul>
          <xsl:apply-templates select="* except (metadata | title)" mode="#current"/>
        </ul>
      </xsl:if>
    </li>
  </xsl:template>
  
  <xsl:template match="queryNode" mode="xfRes:htmlSimplePreview">
    <li class="queryNode">
      <xsl:text>[ERROR][queryNode] TODO</xsl:text>
    </li>
  </xsl:template>
  
  <xsl:template match="referenceNode" mode="xfRes:htmlSimplePreview">
    <li class="referenceNode">
      <xsl:apply-templates select="(verbalization, ref)[1]" mode="#current"/>
    </li>
  </xsl:template>
  
  <!--FIXME : supprimer @xf:idRef qui a été changé en @xf:targetResId-->
  <xsl:template match="referenceNode/verbalization" mode="xfRes:htmlSimplePreview">
    <xsl:variable name="ref" select="preceding-sibling::ref" as="element(ref)*"/>
    <a href="#/editorialEntity/{($ref/@xf:targetResId, $ref/@xf:idRef)[1]}" target="_blank">
      <xsl:apply-templates mode="#current"/>
    </a>
  </xsl:template>
  
  <!--If no verbalization, this template is applied-->
  <xsl:template match="referenceNode/ref" mode="xfRes:htmlSimplePreview">
    <a href="#/editorialEntity/{(@xf:targetResId, @xf:idRef)[1]}" target="_blank">
      <xsl:value-of select="(@xf:targetResId, @xf:idRef, '[ERROR] No verbalization or id found')[1]"/>
    </a>
  </xsl:template>
  
  <xsl:template match="contentNode" mode="xfRes:htmlSimplePreview">
    <xsl:message>[WARNING] ContentNode will not be displayed</xsl:message>
    <div class="contentNode">
      <p>[ERROR][ContentNode] TODO</p>
    </div>
  </xsl:template>
  
  <!--+==================================================+
      | COMMON                                           |
      +==================================================+-->
  
  <!--Default xf element : continue -->
  <xsl:template match="xf:*" mode="xfRes:htmlSimplePreview">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!--except metadata-->
  <xsl:template match="metadata" mode="xfRes:htmlSimplePreview"/>
  
  <!--Default html element : copy-->
  <xsl:template match="html:*" mode="xfRes:htmlSimplePreview">
    <xsl:copy copy-namespaces="no">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  exclude-result-prefixes="#all" 
  version="2.0">
  
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Common functions and templates for XMLfirst</xd:p>
    </xd:desc>
  </xd:doc>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Group metadata with same code so they become multivalued meta </xd:p>
    </xd:desc>
    <xd:param name="meta">Set of meta</xd:param>
    <xd:return>Set of meta grouped</xd:return>
  </xd:doc>
  <xsl:function name="xfRes:groupMultivaluedMeta" as="element(xf:metadata)">
    <xsl:param name="metadata" as="element(xf:metadata)"/>
    <metadata>
      <xsl:copy-of select="$metadata/@*"/>
      <xsl:for-each-group select="$metadata/*" group-by="self::meta/@code">
        <meta code="{current-grouping-key()}">
          <xsl:for-each select="current-group()/@*">
            <xsl:copy-of select="."/>
          </xsl:for-each>
          <xsl:sequence select="current-group()/node()"/>
        </meta>
      </xsl:for-each-group>
    </metadata>
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>Update a meta verbalization element by looking for its current value in the appropriate referential database.</xd:p>
      <xd:p>FIXME : A implémenter quand le format des référentiels sera défini + ces derniers accessibles depuis une base XML. Pour le moment -> simple copie de la méta en entrée.</xd:p>
    </xd:desc>
    <xd:param name="meta">[element(meta)] The meta element whose verbalization needs to be updated. It must contains a ref/@xf:idRef pointing to a referential database entry.</xd:param>
    <xd:return>[element(meta)] The updated meta element.</xd:return>
  </xd:doc>
  <xsl:function name="xfRes:updateMetaVerbalization" as="element(meta)">
    <xsl:param name="meta" as="element(meta)"/>
    <xsl:if test="not($meta/value/ref[@xf:refType = 'xf:referenceItemRef'])">
      <xsl:message>[ERROR][xfRes:updateMetaVerbalization] The meta must contain a reference to a referential database entry (ref/@xf:refType = 'xf:referenceItemRef').</xsl:message>
    </xsl:if>
    <!-- TO DO -->
    <xsl:sequence select="$meta"/>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Simple template to update all metadata verbalizations.</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="meta[value/ref/@xf:refType = 'xf:referenceItemRef']" mode="xfRes:updateMetaVerbalization">
    <xsl:sequence select="xfRes:updateMetaVerbalization(.)"/>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Default copy for mode xfRes:updateMetaVerbalization.</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="node() | @*" mode="xfRes:updateMetaVerbalization">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Returns the short/long label of an identified referential database entry.</xd:p>
      <xd:p>FIXME : A revoir lorsque les référentiels seront mis en place (la fonction ne doit pas être utilisée pour le moment).</xd:p>
    </xd:desc>
    <xd:param name="refXML">[element(xf:referenceTable)] XML serialization of the referential database (FLASH format).</xd:param>
    <xd:param name="entryId">[xs:string] The database entry ID.</xd:param>
    <xd:param name="labelType">[xs:string] Possible values: "short" or "long".</xd:param>
    <xd:return>[xs:string?] The short/long label, if it was found.</xd:return>
  </xd:doc>
  <xsl:function name="xfRes:getRefEntryLabel" as="xs:string?">
    <xsl:param name="refXML" as="element(xf:referenceTable)"/>
    <xsl:param name="entryId" as="xs:string"/>
    <xsl:param name="labelType" as="xs:string"/>
    <xsl:variable name="authorisedLabelTypes" select="('short','long')" as="xs:string+"/>
    <xsl:if test="not($labelType = $authorisedLabelTypes)">
      <xsl:message>[ERROR][xfRes:getRefEntryLabel] Unknown label type: <xsl:value-of select="$labelType"/>.</xsl:message>
    </xsl:if>
    <xsl:value-of select="$refXML//referenceItem[@id = $entryId]/label[@type = $labelType]"/>
  </xsl:function>
  
</xsl:stylesheet>
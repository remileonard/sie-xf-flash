<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  exclude-result-prefixes="#all"
  xmlns="http://www.w3.org/1999/xhtml"
  version="2.0">
  
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  <xsl:import href="xf-ecmRes:/_common/extractMetaAsHtml.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Insert Metadata bloc under the html body element (of the html simple preview of an EE).</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!--Getting the XML name (because it will be lost in steps transformation)-->
  <xsl:param name="xfRes:baseUri.name" select="els:getFileName(base-uri())" as="xs:string"/>
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <!-- A way to launch the transformation without initial mode -->
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfRes:insertMetaInsideBody"/>
  </xsl:template>
  
  <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
  
  <xsl:template match="/" mode="xfRes:insertMetaInsideBody">
    <xsl:variable name="xfRes:extractMeta" as="element(html:div)">
      <xsl:apply-templates select="." mode="xfRes:extractMeta"/>
    </xsl:variable>
    <xsl:variable name="xfRes:htmlPreview">
      <!-- /!\ Apply mode "xfRes:htmlPreview" which depends on the xslt which imports this one-->
      <xsl:apply-templates select="." mode="xfRes:htmlSimplePreview"/>
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$xfRes:htmlPreview//html:body">
        <!-- Inserting the meta bloc inside the body of the simple html preview-->
        <xsl:apply-templates select="$xfRes:htmlPreview" mode="xfRes:insertMetaInsideBody.local">
          <xsl:with-param name="xfRes:baseUri" select="$xfRes:baseUri.name"/>
          <xsl:with-param name="xfRes:extractMeta" select="$xfRes:extractMeta" tunnel="yes"/>
        </xsl:apply-templates>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message terminate="yes">[FATAL][<xsl:value-of select="els:getFileName(static-base-uri())"/>] Applying xfRes:htmlSimplePreview on the EE did not generate any HTML body element. Impossible to insert metadata.</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!--+==================================================+
      | MODE insertMetaInsideBody                        |
      +==================================================+-->
  
  <xsl:template match="html:body" mode="xfRes:insertMetaInsideBody.local">
    <xsl:param name="xfRes:extractMeta" as="element(html:div)" tunnel="yes"/>
    <xsl:message>[INFO] Inserting metadata inside body of the html preview</xsl:message>
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:sequence select="$xfRes:extractMeta"/>
      <div class="ecm_content">
        <xsl:apply-templates mode="xfRes:insertMetaInsideBody.local"/>
      </div>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="* | @*  | processing-instruction() | comment()" mode="xfRes:insertMetaInsideBody.local">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
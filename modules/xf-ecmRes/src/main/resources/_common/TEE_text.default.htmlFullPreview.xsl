<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  version="2.0">
  
  <xsl:import href="TEE_text.default.htmlSimplePreview.xsl"/>
  <xsl:import href="insertMetaInsideBody.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Generate the default full html preview for EE of type text</xd:p>
      <xd:p>xsl:imports do the job</xd:p>
    </xd:desc>
  </xd:doc>
  
</xsl:stylesheet>
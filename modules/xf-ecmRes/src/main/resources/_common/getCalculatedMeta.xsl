<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  exclude-result-prefixes="#all" 
  version="2.0">
  
  <xsl:import href="xf-ecmRes:/_common/xf-common.xsl"/>
  
  <!--Import all xsl to calculate metadata from any EE type-->
  <!--EFL-->
  <xsl:import href="xf-ecmRes:/efl/infoCommentaire.getCalculatedMetadata.xsl"/>
  <xsl:import href="xf-ecmRes:/efl/treeRevueCxml.getCalculatedMetadata.xsl"/>
  <!--DALLOZ-->
  <xsl:import href="xf-ecmRes:/dalloz/DZ_TEE_revues.getCalculatedMetadata.xsl"/>
  <xsl:import href="xf-ecmRes:/dalloz/DZ_TEE_ouvrages.getCalculatedMetadata.xsl"/>
  <xsl:import href="xf-ecmRes:/dalloz/DZ_TEE_encyclo.getCalculatedMetadata.xsl"/>
  <xsl:import href="xf-ecmRes:/dalloz/DZ_TEE_colLoc.getCalculatedMetadata.xsl"/>
  <xsl:import href="xf-ecmRes:/dalloz/DZ_TEE_formule.getCalculatedMetadata.xsl"/>
  <!--EL-->
  <xsl:import href="xf-ecmRes:/el/fichePratique.getCalculatedMetadata.xsl"/>
  <xsl:import href="xf-ecmRes:/el/el_FICHE_QR.getCalculatedMetadata.xsl"/>
  <xsl:import href="xf-ecmRes:/el/el_fichePro.getCalculatedMetadata.xsl"/>
  <xsl:import href="xf-ecmRes:/el/el_ficheMT.getCalculatedMetadata.xsl"/>
  <xsl:import href="xf-ecmRes:/el/el_etudes.getCalculatedMetadata.xsl"/>
  <!--ELS-->
  <xsl:import href="xf-ecmRes:/els/ELS_TEE_jrpJuri.getCalculatedMetatada.xsl"/>
  
  <xsl:output indent="yes"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Get calculated metadata from any EE file</xd:p>
      <xd:p>If param $metaCode is not setted, then get all calculated metadat</xd:p>
      <xd:p>If param $metaCode is setted, then get the calculated metada whose code is asked for.</xd:p>
    </xd:desc>
  </xd:doc>

  <!--$metaCode parameter : list of meta code as coma separated values, empty string means "all"-->  
  <xsl:param name="metaCode" as="xs:string" select="''"/>
  
  <!--version tokenizée-->
  <xsl:variable name="metaCode.token" select="tokenize(replace($metaCode, '\s', ''), ',')" as="xs:string*"/>
  
  <!--========================================================-->
  <!--INIT-->
  <!--========================================================-->
  
  <!--A way to launch the transformation without initial mode-->
  
  <xsl:template match="/" priority="-1">
    <xsl:apply-templates select="descendant::xf:editorialEntity" mode="xfRes:getCalculatedMetadata"/> 
  </xsl:template>
  
  <!--========================================================-->
  <!--MAIN-->
  <!--========================================================-->
  
  <!--No "/" because this xslt is also called from EE at SAS format--> 
  <xsl:template match="editorialEntity" mode="xfRes:getCalculatedMetadata">
    <xsl:message>code:<xsl:value-of select="@code"/></xsl:message>
    <xsl:variable name="calculatedMetadata" as="document-node()">
      <xsl:document>
        <xsl:choose>
          <!--=== EFL ===--> 
          <!--info EFL-->
          <xsl:when test="@code = 'EFL_TEE_uiCommentaire'">
            <xsl:apply-templates select="." mode="xfRes:EFL_TEE_infoCommentaire.getCalculatedMetatada"/>
          </xsl:when>
          <!--revues EFL-->
          <xsl:when test="@code = (
              'EFL_TEE_baf', 'EFL_TEE_bdp', 'EFL_TEE_bpat', 'EFL_TEE_bpim', 'EFL_TEE_brda', 'EFL_TEE_frc', 'EFL_TEE_frchs', 'EFL_TEE_rjf', 'EFL_TEE_acp', 'EFL_TEE_fr', 'EFL_TEE_frs', 'EFL_TEE_acdaf', 'EFL_TEE_brdaguide', 'EFL_TEE_bpci'
            , 'EFL_TEE_ACTAFF', 'EFL_TEE_ACTASSO', 'EFL_TEE_ACTCPT', 'EFL_TEE_ACTFISC', 'EFL_TEE_ACTIM', 'EFL_TEE_ACTPAT', 'EFL_TEE_ACTSOC'
            , 'EFL_TEE_newsletter', 'EFl_TEE_quotidienne')">
            <xsl:apply-templates select="." mode="xfRes:EFL_TEE_treeRevueCxml.getCalculatedMetatada"/>
          </xsl:when>
          <!-- NewsLetter -->
          <xsl:when test="@code = 'EFL_TEE_newsletter'">
            <xsl:apply-templates select="." mode="xfRes:EFL_TEE_treeRevueCxml.getCalculatedMetatada"/>
          </xsl:when>
          <!-- Matiere -->
          <xsl:when test="@code = 'EFL_TEE_filActu'">
            <xsl:apply-templates select="." mode="xfRes:EFL_TEE_treeRevueCxml.getCalculatedMetatada4Matiere"/>
          </xsl:when>
          <!-- Quotidienne -->
          <xsl:when test="@code = 'EFL_TEE_quotidienne'">
            <xsl:apply-templates select="." mode="xfRes:EFL_TEE_treeRevueCxml.getCalculatedMetatada4Quotidienne"/>
          </xsl:when>          
          <!-- OM -->
          <xsl:when test="@code = ('EFL_TEE_filActusOmAvocat', 'EFL_TEE_filActusOmCEC', 'EFL_TEE_filActusOmCee', 'EFL_TEE_filActusOmDaf', 'EFL_TEE_filActusOmJur', 'EFL_TEE_filActusOmPaie', 'EFL_TEE_filActusOmRh')">
            <xsl:apply-templates select="." mode="xfRes:EFL_TEE_treeRevueCxml.getCalculatedMetatada4OM"/>
          </xsl:when>
          <!--=== DALLOZ ===-->
          <!-- revues -->
          <xsl:when test="matches(@code, 'DZ_TEE_revues\p{Lu}{2,}') or @code = ('DZ_TEE_revuesDossier', 'DZ_TEE_revuesArticle')">
            <xsl:apply-templates select="." mode="xfRes:DZ_TEE_revues.getCalculatedMetatada"/>           
          </xsl:when>
          <!-- ouvrages -->
          <xsl:when test="@code = ('DZ_TEE_ouvrages', 'DZ_TEE_ouvragesParagraphe')">
            <xsl:apply-templates select="." mode="xfRes:DZ_TEE_ouvrages.getCalculatedMetatada"/>
          </xsl:when>
          <!-- encyclopedies -->
          <xsl:when test="@code = ('DZ_TEE_encycloRubrique', 'DZ_TEE_encycloParagraphe')">
            <xsl:apply-templates select="." mode="xfRes:DZ_TEE_encyclo.getCalculatedMetatada"/>
          </xsl:when>
          <!-- formules -->
          <xsl:when test="@code = ('DZ_TEE_formule', 'DZ_TEE_formuleTexte')">
            <xsl:apply-templates select="." mode="xfRes:DZ_TEE_formule.getCalculatedMetatada"/>
          </xsl:when>
          <!-- Collectivités Locales -->
          <xsl:when test="@code = 'DZ_TEE_colLoc'">
            <xsl:apply-templates select="." mode="xfRes:DZ_TEE_colLoc.getCalculatedMetatada"/>
          </xsl:when>
          <!--=== EL ===-->  
          <xsl:when test="@code = 'EL_TEE_el_FICHE_FP' or @code = 'EL_TEE_fichePratique'">
            <xsl:apply-templates select="." mode="xfRes:EL_TEE_FichePratique.getCalculatedMetatada"/>
          </xsl:when>
          <xsl:when test="@code = 'EL_TEE_FICHE_QR' or @code = 'EL_TEE_ficheQR'">
            <xsl:apply-templates select="." mode="xfRes:EL_TEE_FICHE_QR.getCalculatedMetatada"/>
          </xsl:when>
          <xsl:when test="@code = 'EL_TEE_fichePro'">
            <xsl:apply-templates select="." mode="xfRes:EL_TEE_fichePro.getCalculatedMetatada"/>
          </xsl:when>
          <xsl:when test="@code = 'EL_TEE_ficheMT'">
            <xsl:apply-templates select="." mode="xfRes:EL_TEE_ficheMT.getCalculatedMetatada"/>
          </xsl:when>
          <xsl:when test="@code = 'EL_TEE_etudes'">
            <xsl:apply-templates select="." mode="xfRes:EL_TEE_etudes.getCalculatedMetatada"/>
          </xsl:when>
          <xsl:when test="@code = 'EL_TEE_pnum'">
            <xsl:apply-templates select="." mode="xfRes:EL_TEE_etudes.getCalculatedMetatada"/>
          </xsl:when>
          
          <!--=== ELS ===-->
          <xsl:when test="@code = 'ELS_TEE_jurisprudence'">
            <xsl:apply-templates select="." mode="xfRes:ELS_TEE_jrpJuri.getCalculatedMetatada"/>            
          </xsl:when>
          <xsl:otherwise>
            <xsl:message terminate="yes">[FATAL] call mode="xfRes:getCalculatedMetadata" with editorialEntity whose @code="<xsl:value-of select="@code"/>" is not recognized</xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="calculatedMetadata.filtered" as="element(xf:metadata)">
      <xsl:apply-templates select="$calculatedMetadata" mode="xfRes:getCalculatedMetadata.filtering"/>
    </xsl:variable>
    <xsl:sequence select="xfRes:groupMultivaluedMeta($calculatedMetadata.filtered)"/>
  </xsl:template>
  
  <xsl:template match="/xf:metadata" mode="xfRes:getCalculatedMetadata.filtering">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:choose>
        <xsl:when test="$metaCode = ''">
          <xsl:apply-templates mode="#current"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates select="meta[@code = $metaCode.token] | processing-instruction() | comment()" mode="#current"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="xfRes:getCalculatedMetadata.filtering">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
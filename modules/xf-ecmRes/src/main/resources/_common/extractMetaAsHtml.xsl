<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  exclude-result-prefixes="#all"
  xmlns="http://www.w3.org/1999/xhtml"
  version="2.0">
  
  <!-- IMPORTS -->
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p xml:lang="en">Metadata extraction from any FLASH EE (generic XSL).</xd:p>
      <xd:p xml:lang="fr">Extraction des métadonnées d'une EE FLASH (XSL générique).</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!-- PARAMS/VARIABLES -->
  <xsl:param name="xfRes:baseUri.name" select="els:getFileName(base-uri())" as="xs:string"/>
  
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <xd:doc>
    <xd:desc>A way to launch the transformation without initial mode.</xd:desc>
  </xd:doc>
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfRes:extractMeta"/>
  </xsl:template>
  

  <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
  
  <xd:doc>
    <xd:desc>Launcher for metadata HTML extraction.</xd:desc>
  </xd:doc>
  <xsl:template match="/" mode="xfRes:extractMeta">
    <xsl:message>[INFO] Extracting metadata as HTML for EE : <xsl:value-of select="$xfRes:baseUri.name"/></xsl:message>
    <xsl:apply-templates select="xf:editorialEntity/xf:metadata" mode="#current"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MODE : extractMeta / LOOP THROUGH METADATAS      |
      | Metadata extraction from the EE.                 |
      +==================================================+-->

  <xd:doc>
    <xd:desc>Metadata block under xf:editorialEntity.</xd:desc>
  </xd:doc>
  <xsl:template match="xf:editorialEntity/xf:metadata" mode="xfRes:extractMeta">
    <div class="ecm_metadata">
      <table>
        <xsl:apply-templates mode="#current"/>
      </table>
    </div>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>One metadata, eligible for extraction.</xd:desc>
  </xd:doc>
  <xsl:template match="xf:meta[xfRes:isMetaEligibleForExtraction(.)]" mode="xfRes:extractMeta">
    <tr class="{local-name()}">
      <td class="label" data-code="{@code}">
        <xsl:value-of select="@label"/>
      </td>
      <td class="values">
        <xsl:apply-templates mode="#current"/>
      </td>
    </tr>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>Other metadatas are simply ignored.</xd:desc>
  </xd:doc>
  <xsl:template match="xf:meta" mode="xfRes:extractMeta"/>
  
  
  <!-- Values extraction -->

  <xd:doc>
    <xd:desc>Metadata value = HTML element(s) -> copy of HTML children nodes.</xd:desc>
  </xd:doc>
  <xsl:template match="xf:value[matches(@as,'element\(html:(div|span)\)')]" mode="xfRes:extractMeta">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>Metadata verbalization -> copy of HTML children nodes.</xd:desc>
  </xd:doc>
  <xsl:template match="xf:verbalization" mode="xfRes:extractMeta">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>Metadata value = simple content (xs:string, xs:integer, etc.).</xd:desc>
  </xd:doc>
  <xsl:template match="xf:value[normalize-space()][not(child::*)]" mode="xfRes:extractMeta">
    <span class="simpleValue {xfRes:getSimpleType(@as)}">
      <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>Metadata value = xf:field elements.</xd:desc>
  </xd:doc>
  <xsl:template match="xf:value[xf:field]" mode="xfRes:extractMeta">
    <div class="fields">
      <xsl:apply-templates mode="#current"/>
    </div>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>xf:field value.</xd:desc>
  </xd:doc>
  <xsl:template match="xf:field" mode="xfRes:extractMeta">
    <span class="field" data-field="{@name}">
      <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>
  
  
  <!-- Values normalization -->
  
  <xd:doc>
    <xd:desc>Verbalizes an xs:date value (YYYYMMD) according to the pattern: DD MonthInFrench YYYY.</xd:desc>
  </xd:doc>
  <xsl:template match="xf:value[@as = 'xs:dateTime']/text() |
                       xf:field[@as = 'xs:dateTime']/text()"
                mode="xfRes:extractMeta">
    <xsl:value-of select="els:displayDate(replace(els:getIsoDateFromString(.),'\D+',''))"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MODE : extractMeta / DEFAULT                     |
      +==================================================+-->

  <xd:doc>
    <xd:desc>Raw copy of metadatas' html:* children nodes.</xd:desc>
  </xd:doc>
  <xsl:template match="html:*" mode="xfRes:extractMeta">
    <xsl:copy-of select="." copy-namespaces="no"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | FUNCTIONS                                        |
      +==================================================+-->
  
  <xd:doc>
    <xd:desc>
      <xd:p>Returns a simple metadata type (default: string).</xd:p>
    </xd:desc>
    <xd:param name="as">[xs:string?] The value of one metadata @as attribute (or an empty sequence if the attribute does not exist).</xd:param>
    <xd:return>[xs:string] The metadata type (default: string).</xd:return>
  </xd:doc>
  <xsl:function name="xfRes:getSimpleType" as="xs:string">
    <xsl:param name="as" as="xs:string?"/>
    <xsl:value-of select="(substring-after($as,':'),'string')[1]"/>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Checks whether a meta is eligible for extraction, according to various conditions.</xd:p>
    </xd:desc>
    <xd:param name="meta">[xf:meta] The medatadata to check.</xd:param>
    <xd:return>[xs:boolean] The result of the test.</xd:return>
  </xd:doc>
  <xsl:function name="xfRes:isMetaEligibleForExtraction" as="xs:boolean">
    <xsl:param name="meta" as="element(xf:meta)"/>
    <!-- Metadata codes which are not to be extracted -->
    <xsl:variable name="forbiddenMetaCodes"
                  select="('systemTitle','systemAbstract')"
                  as="xs:string+"/>
    <!-- Check $meta/@code -> according to a code list -->
    <xsl:variable name="checkMetaCode" select="not($meta/@code = $forbiddenMetaCodes)" as="xs:boolean"/>
    <!-- Check $meta content -> there must be something to display -->
    <xsl:variable name="checkMetaContent" select="normalize-space($meta) != ''" as="xs:boolean"/>
    <!-- Test result: code and content must be OK -->
    <xsl:sequence select="not(($checkMetaCode,$checkMetaContent) = false())"/>
  </xsl:function>
 
</xsl:stylesheet>
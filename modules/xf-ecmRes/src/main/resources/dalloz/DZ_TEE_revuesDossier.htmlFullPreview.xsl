<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
  version="2.0">

  <xsl:import href="xf-ecmRes:/dalloz/DZ_TEE_revuesDossier.htmlSimplePreview.xsl"/>
  <xsl:import href="xf-ecmRes:/_common/insertMetaInsideBody.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Génère la prévisualisation complète d'un EE de type DOSSIER</xd:p>
      <xd:p>Les xsl:imports ci-dessus suffisent</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!--FE-68 : désactivation des log xfe-->
  <xsl:param name="xfe:debug" select="false()" as="xs:boolean"/>
  
</xsl:stylesheet>
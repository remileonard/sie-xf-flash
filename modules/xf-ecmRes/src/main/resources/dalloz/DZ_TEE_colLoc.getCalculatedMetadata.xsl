<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" 
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  exclude-result-prefixes="#all" 
  version="2.0">
  
  <xsl:import href="xf-ecmRes:/dalloz/_common/xf-common-dalloz.xsl"/>
  <xsl:include href="xf-ecmRes:/dalloz/_common/title2html.xsl"/>
  
  <xsl:variable name="systemMeta" select="('systemTitle', 'systemAbstract')" as="xs:string+"/>
  
  <xsl:template match="xf:editorialEntity" mode="xfRes:DZ_TEE_colLoc.getCalculatedMetatada">
    <metadata>
      <xsl:sequence select="for $codeMeta in $systemMeta return xfRes:colLoc_dispatchGenMeta(xf:metadata, $codeMeta)"/>
    </metadata>
  </xsl:template>
  
  <xsl:function name="xfRes:colLoc_dispatchGenMeta">
    <xsl:param name="element" as="element()"/>
    <xsl:param name="codeMeta" as="xs:string"/>
    <xsl:choose>
      <!-- System -->
      <xsl:when test="$codeMeta = 'systemTitle'">
        <xsl:choose>
          <xsl:when test="$element/xf:meta[@code = 'DZ_META_encycloTitreInt']">
            <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:colLoc_createTitle($element/xf:meta[@code = 'DZ_META_encycloTitreInt']))"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message>[ERROR] System meta "title" undefined</xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:when test="$codeMeta = 'systemAbstract'">
        <xsl:choose>
          <xsl:when test="$element/xf:meta[@code = 'systemAbstract']">
            <xsl:copy-of select="$element/xf:meta[@code = 'systemAbstract']"/>
          </xsl:when>
          <xsl:when test="$element/xf:meta[@code = 'DZ_META_encycloTitreInt']">
            <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:colLoc_createAbstract($element/xf:meta[@code = 'DZ_META_encycloTitreInt']))"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message>[ERROR] System meta "abstract" undefined</xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>[ERROR] Metadata <xsl:value-of select="$codeMeta"/> undefined</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="xfRes:colLoc_createTitle" as="element(html:div)">
    <xsl:param name="element" as="element()"/>
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:value-of select="$element/xf:value"/>
    </div>
  </xsl:function>
  
  <xsl:function name="xfRes:colLoc_createAbstract" as="element(html:div)">
    <xsl:param name="element" as="element()"/>
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:value-of select="$element/xf:value"/>
    </div>
  </xsl:function>
  
</xsl:stylesheet>

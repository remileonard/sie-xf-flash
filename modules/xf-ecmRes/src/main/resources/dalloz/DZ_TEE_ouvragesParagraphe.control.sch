<?xml version="1.0" encoding="utf-8"?>
<!--<schema xmlns="http://www.ascc.net/xml/schematron">-->
<schema xmlns="http://purl.oclc.org/dsdl/schematron" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:saxon="http://saxon.sf.net/"
  xmlns:ouvrages="http://www.lefebvre-sarrut.eu/ns/dalloz/ouvrages" queryBinding="xslt2"
  id="validation_editorialEntity">

  <title>Schematron for Dalloz Paragraphe TEE</title>

  <ns prefix="xsl" uri="http://www.w3.org/1999/XSL/Transform"/>
  <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
  <ns prefix="saxon" uri="http://saxon.sf.net/"/>
  <ns prefix="xf" uri="http://www.lefebvre-sarrut.eu/ns/xmlfirst"/>
  
  <pattern id="CheckMETA_ouvragesParaNume">
    <rule context="/xf:editorialEntity/xf:metadata/xf:meta[@code='DZ_META_ouvragesParaNume']/xf:value">
      <report test="not(text()) or text() = ''"   role="warning"> Le numero du paragraphe est vide !</report>
      <report test="text() = 'XX'" role="error"> Le numero du paragraphe est invalide !</report>
    </rule>
  </pattern>
  
</schema>

<?xml version="1.0" encoding="utf-8"?>
<!--<schema xmlns="http://www.ascc.net/xml/schematron">-->
<schema xmlns="http://purl.oclc.org/dsdl/schematron" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:saxon="http://saxon.sf.net/"
  xmlns:encyclo="http://www.lefebvre-sarrut.eu/ns/dalloz/encyclopedies" queryBinding="xslt2"
  id="validation_editorialEntity">

  <title>Schematron for Dalloz Collectivites Locales main file TEE</title>

  <ns prefix="xsl" uri="http://www.w3.org/1999/XSL/Transform"/>
  <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
  <ns prefix="saxon" uri="http://saxon.sf.net/"/>
  <ns prefix="xf" uri="http://www.lefebvre-sarrut.eu/ns/xmlfirst"/>
  <ns prefix="encyclo" uri="http://www.lefebvre-sarrut.eu/ns/dalloz/encyclopedies"/>
  
  <!-- test initial bidon -->
  <pattern id="hasUniqueRoot">
    <rule context="/">
      <assert test="count(xf:editorialEntity) = 1" role="error"> [TEST] il doit y avoir une racine editorialEntity unique.</assert>
    </rule>
  </pattern>

</schema>

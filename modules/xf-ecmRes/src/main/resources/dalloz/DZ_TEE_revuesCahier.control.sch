<?xml version="1.0" encoding="utf-8"?>
<!--<schema xmlns="http://www.ascc.net/xml/schematron">-->
<schema 
    xmlns="http://purl.oclc.org/dsdl/schematron"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:saxon="http://saxon.sf.net/"
    xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
    queryBinding="xslt2"
    
    id="validation_editorialEntity">
    
    <title>Schematron for Dalloz revues TEE</title>
    
    <ns prefix="xsl" uri="http://www.w3.org/1999/XSL/Transform"/>
    <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
    <ns prefix="saxon" uri="http://saxon.sf.net/"/>
    <ns prefix="xf" uri="http://www.lefebvre-sarrut.eu/ns/xmlfirst"/>
    
    <pattern id="test">
        <rule context="xf:editorialEntity">
        		<let name="annee" value="xf:metadata/xf:meta[@code='DZ_META_revuesCahierAnnee']/xf:value"/>
            <assert test="@xf:id" role="error">
                [TEST] L'ID du cahier ne doit pas être vide !
            </assert>           
            <assert test="string-length($annee) = 4  and $annee castable as xs:integer" role="error">
                [TEST] La méta année doit être valide !
            </assert>
            <assert test="xf:metadata/xf:meta[@code='DZ_META_revuesCahierNum'] 
                and xf:metadata/xf:meta[@code='DZ_META_revuesCahierNum']/xf:value 
                and normalize-space(string-join(xf:metadata/xf:meta[@code='DZ_META_revuesCahierNum']/xf:value, '')) != ''" role="error">
                [TEST] La méta année doit être valide !
            </assert>
        </rule>
    </pattern> 
    
    <pattern id="CheckMETA_annee">
        <rule context="xf:editorialEntity/xf:metadata/xf:meta[@code='DZ_META_revuesCahierAnnee']/xf:value">
            <report test="number(text()) &gt; 2017"   role="warning"> L'année dépasse 2017 !</report>
            <report test="number(text()) &lt; 2010" role="error"> L'année est avant 2010 !</report>
        </rule>
    </pattern>
</schema>
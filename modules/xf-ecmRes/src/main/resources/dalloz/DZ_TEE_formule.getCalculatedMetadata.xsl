<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" 
  xmlns:content="http://www.lefebvre-sarrut.eu/ns/dalloz/formule" 
  xmlns:html="http://www.w3.org/1999/xhtml"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  exclude-result-prefixes="#all" 
  version="2.0">
  
  <xsl:import href="xf-ecmRes:/dalloz/_common/xf-common-dalloz.xsl"/>
  <xsl:include href="xf-ecmRes:/dalloz/_common/title2html.xsl"/>
  
  <xsl:variable name="systemMeta" select="('systemTitle', 'systemAbstract')" as="xs:string+"/>
  
  <xsl:template match="xf:editorialEntity" mode="xfRes:DZ_TEE_formule.getCalculatedMetatada">
    <xsl:variable name="element" as="element()">
      <xsl:choose>
        <xsl:when test="@code = 'DZ_TEE_formule'">
          <xsl:sequence select="xf:metadata"/>
        </xsl:when>
        <xsl:when test="@code = 'DZ_TEE_formuleTexte'">
          <xsl:sequence select="xf:metadata"/>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>
    <metadata>
      <xsl:sequence select="for $codeMeta in $systemMeta return xfRes:formule_dispatchGenMeta($element, $codeMeta)"/>
    </metadata>
  </xsl:template>
  
  <xsl:function name="xfRes:formule_dispatchGenMeta">
    <xsl:param name="element" as="element()"/>
    <xsl:param name="codeMeta" as="xs:string"/>
    <xsl:choose>
      <!-- System -->
      <xsl:when test="$codeMeta = 'systemTitle'">
        <xsl:choose>
          <xsl:when test="local-name($element) = 'metadata'">
            <xsl:choose>
              <xsl:when test="$element/xf:meta[@code = 'DZ_META_formuleTitre']">
                <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:formule_createTitle($element/xf:meta[@code = 'DZ_META_formuleTitre']))"/>
              </xsl:when>
              <xsl:when test="$element/xf:meta[@code = 'DZ_META_formuleTitreInt']">
                <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:formule_createTitle($element/xf:meta[@code = 'DZ_META_formuleTitreInt']))"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:message>[ERROR] System meta "title" undefined</xsl:message>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:formule_createTitleTexte($element))"/>
          </xsl:otherwise>
        </xsl:choose>  
      </xsl:when>
      <xsl:when test="$codeMeta = 'systemAbstract'">
        <xsl:choose>
          <xsl:when test="local-name($element) = 'metadata'">
            <xsl:choose>
              <xsl:when test="$element/xf:meta[@code = 'systemAbstract']">
                <xsl:copy-of select="$element/xf:meta[@code = 'systemAbstract']"/>
              </xsl:when>
              <xsl:when test="$element/xf:meta[@code = 'DZ_META_formuleTitre']">
                <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:formule_createAbstract($element/xf:meta[@code = 'DZ_META_formuleTitre']))"/>
              </xsl:when>
              <xsl:when test="$element/xf:meta[@code = 'DZ_META_formuleTitreInt']">
                <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:formule_createAbstract($element/xf:meta[@code = 'DZ_META_formuleTitreInt']))"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:message>[ERROR] System meta "abstract" undefined</xsl:message>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:formule_createAbstractTexte($element))"/>
          </xsl:otherwise>
        </xsl:choose>  
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>[ERROR] Metadata <xsl:value-of select="$codeMeta"/> undefined</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="xfRes:formule_createTitle" as="element(html:div)">
    <xsl:param name="titre" as="element()"/>
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:value-of select="$titre/xf:value"/>
    </div>
  </xsl:function>
  
  <xsl:function name="xfRes:formule_createAbstract" as="element(html:div)">
    <xsl:param name="titre" as="element()"/>
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:value-of select="$titre/xf:value"/>
    </div>
  </xsl:function>
  
  <xsl:function name="xfRes:formule_createTitleTexte" as="element(html:div)">
    <xsl:param name="titre" as="element()"/>
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:value-of select="$titre/xf:value"/>
    </div>
  </xsl:function>
  
  <xsl:function name="xfRes:formule_createAbstractTexte" as="element(html:div)">
    <xsl:param name="titre" as="element()"/>
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:value-of select="$titre/xf:value"/>
    </div>
  </xsl:function>
  
</xsl:stylesheet>

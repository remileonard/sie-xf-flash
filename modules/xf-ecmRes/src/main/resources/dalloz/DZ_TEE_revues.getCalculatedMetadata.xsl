<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
    xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" 
    xmlns:content="http://www.lefebvre-sarrut.eu/ns/dalloz/revues" 
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
    exclude-result-prefixes="#all" 
    version="2.0">
    
    <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
    <xsl:import href="xf-ecmRes:/dalloz/_common/xf-common-dalloz.xsl"/>

    <xsl:variable name="systemMeta" select="('systemTitle', 'systemAbstract')" as="xs:string+"/>
    <xsl:variable name="calculatedMetaCahier" select="($systemMeta, 'DZ_META_revuesCahierId')" as="xs:string+"/>
    <xsl:variable name="calculatedMetaDossier" select="($systemMeta, 'DZ_META_revuesArticleId', 'DZ_META_revuesAuteurId', 'DZ_META_revuesAuteurPrenomNom')" as="xs:string+"/>
    <xsl:variable name="calculatedMetaArticle" select="($systemMeta, 'DZ_META_revuesArticleId', 'DZ_META_revuesAuteurId', 'DZ_META_revuesAuteurPrenomNom')" as="xs:string+"/>

    <xsl:template match="xf:editorialEntity" mode="xfRes:DZ_TEE_revues.getCalculatedMetatada">
        <xsl:variable name="metaList" as="xs:string*">
            <xsl:choose>
                <xsl:when test="matches(@code, 'DZ_TEE_revues\p{Lu}{2,}')">
                    <xsl:sequence select="$calculatedMetaCahier"/>
                </xsl:when>
                <xsl:when test="@code = 'DZ_TEE_revuesDossier'">
                    <xsl:sequence select="$calculatedMetaDossier"/>
                </xsl:when>
                <xsl:when test="@code = 'DZ_TEE_revuesArticle'">
                    <xsl:sequence select="$calculatedMetaArticle"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:sequence select="''"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="element" as="element()?">
            <xsl:choose>
                <xsl:when test="matches(@code, 'DZ_TEE_revues\p{Lu}{2,}')">
                    <xsl:sequence select="xf:metadata"/>
                </xsl:when>
                <xsl:when test="@code = 'DZ_TEE_revuesDossier'">
                    <xsl:sequence select="xf:body/xf:contentNode/xf:content/content:DOSSIER"/>
                </xsl:when>
                <xsl:when test="@code = 'DZ_TEE_revuesArticle'">
                    <xsl:sequence select="xf:body/xf:contentNode/xf:content/content:ARTICLE"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <metadata>
            <xsl:sequence select="for $codeMeta in $metaList return xfRes:dispatchGenMeta($element, $codeMeta)"/>
        </metadata>
    </xsl:template>

    <xsl:function name="xfRes:dispatchGenMeta">
        <xsl:param name="element" as="element()?"/>
        <xsl:param name="codeMeta" as="xs:string"/>
        <xsl:choose>
            <!-- System -->
            <xsl:when test="$codeMeta = 'systemTitle'">
                <xsl:choose>
                    <xsl:when test="local-name($element) = 'metadata'">
                        <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:createTitleCahier($element))"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:createTitle($element))"/>
                    </xsl:otherwise>
                </xsl:choose>               
            </xsl:when>
            <xsl:when test="$codeMeta = 'systemAbstract'">
                <xsl:choose>
                    <xsl:when test="local-name($element) = 'metadata'">
                        <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:createAbstractCahier($element))"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:sequence select="xfRes:genHtmlMeta($codeMeta, xfRes:createAbstract($element))"/>
                    </xsl:otherwise>
                </xsl:choose>               
            </xsl:when>
            <!-- CAHIER -->
            <xsl:when test="$codeMeta = 'DZ_META_revuesCahierId'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, xfRes:getCahierId($element))"/>
            </xsl:when>
            <!-- DOSSIER -->
            <xsl:when test="$codeMeta = 'DZ_META_revuesDossierEtat'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/@ETAT)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesPartTitre'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/content:PARTIE/content:TITRE-PART)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesArticleTitreFull'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, els:normalized-string($element/content:INTITULE))"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesArticleTitre'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/content:INTITULE/content:TITRE-ART)"/>
            </xsl:when>
            <!-- ARTICLE -->
            <!-- begin: DOSSIER / ARTICLE : folios -->
            <xsl:when test="$codeMeta = 'DZ_META_revuesFolioDeb'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/content:METAS/content:FOLIO/@DEB)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesFolioFin'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/content:METAS/content:FOLIO/@FIN)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesFolioPosition'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/content:METAS/content:FOLIO/@POSITION)"/>
            </xsl:when>
            <!-- end: DOSSIER / ARTICLE : folios -->
            <xsl:when test="$codeMeta = 'DZ_META_revuesArticleId'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/@ID)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesArticleType'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/@TYPE)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesArticleType2'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/@TYPE2)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesArticleNum'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/content:INTITULE/content:NUM-ART)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesArticleSurtitre'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/content:INTITULE/content:SURTITRE)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesArticleSoustitre'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/content:INTITULE/content:STITRE)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesAuteurId'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element//content:AUTEUR/@ID)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesAuteurPrenomNom'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, xfRes:getAuteurPrenomNom($element//content:AUTEUR))"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesAuteurNom'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element//content:AUTEUR/content:NOM)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesAuteurPrenom'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element//content:AUTEUR/content:PRENOM)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesAuteurQualite'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element//content:SIGNATURE/content:QUALITE)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesMotcleNiv1'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element//content:INDEX/content:NOME/content:MOT)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesMotcle'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element//content:INDEX/content:NOME//content:MOCL/content:MOT)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesDecision'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, for $lienjur in $element//content:RENVOIS/content:RENV-JURIS/content:LIENJUR return els:normalized-string($lienjur))"/>                
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesJuri'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element//content:RENVOIS/content:RENV-JURIS/content:LIENJUR/content:JURIDIC/content:JURI)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesNaff'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element//content:RENVOIS/content:RENV-JURIS/content:LIENJUR/content:NAFF)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesChambre'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element//content:RENVOIS/content:RENV-JURIS/content:LIENJUR/content:JURIDIC/content:CHAM)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesTexteleg'">
                <xsl:variable name="legisTexte" select="$element//content:RENVOIS/content:RENV-LEGIS/content:LIENJUR/content:LIENLEG/content:TEXTELEG"/>
                <xsl:variable name="decisionTexte" select="$element//content:PARADECI/content:FONDLEG/content:TEXTESLEG/content:TEXTELEG"/>
                <xsl:variable name="textes" select="if ($legisTexte) then $legisTexte else $decisionTexte"/>
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, for $texte in $textes return els:normalized-string($texte))"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesArticleEtat'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/@ETAT)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesArticleMedia'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/@MEDIA)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesDecisionDate'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, xfRes:formatDate($element//content:RENVOIS/content:RENV-JURIS/content:LIENJUR/content:DATE))"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesCode'">
                <xsl:variable name="legisCode" select="$element//content:RENVOIS/content:RENV-LEGIS/content:LIENLEG/content:CODE/content:ARTICLES/content:NOART"/>
                <xsl:variable name="decisionCode" select="$element//content:PARADECI/content:FONDLEG/content:CODES/content:CODE/content:ARTICLES/content:NOART"/>
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, xfRes:getCode(if ($legisCode) then $legisCode else $decisionCode))"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesSouspart'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/content:PARTIE/content:SPARTIE)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesRubrTitre'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/content:RUBRIQUE/content:TITRE-RUBR)"/>
            </xsl:when>
            <xsl:when test="$codeMeta = 'DZ_META_revuesSousRubr'">
                <xsl:sequence select="xfRes:genAtomicMeta($codeMeta, $element/content:RUBRIQUE/content:SRUBR)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:message>[ERROR] Metadata <xsl:value-of select="$codeMeta"/> undefined</xsl:message>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>CAHIER : create title of cahier</xd:p>
        </xd:desc>
        <xd:param name="metas">Element of metas</xd:param>
        <xd:return>Title of cahier in html:div</xd:return>
    </xd:doc>
    <xsl:function name="xfRes:createTitleCahier" as="element(html:div)">
        <xsl:param name="metas" as="element()"/>
        <xsl:variable name="revuesNom" select="tokenize($metas/ancestor::xf:editorialEntity/@historicalId, '_')[2]"/>
        <div xmlns="http://www.w3.org/1999/xhtml" class="systemTitle">
            <p>
                <xsl:value-of select="concat($revuesNom, ' ')"/>
                <xsl:value-of select="$metas/xf:meta[@code = 'DZ_META_revuesCahierAnnee']/xf:value"/>
                <xsl:text> n</xsl:text>
                <sup>o</sup>
                <xsl:text>&#xA0;</xsl:text>
                <xsl:value-of select="$metas/xf:meta[@code = 'DZ_META_revuesCahierNum']/xf:value"/>
            </p>
        </div>
    </xsl:function>

    <!-- DOSSIER | ARTICLE : INTITULE/TITRE-ART -->
    <xsl:function name="xfRes:createTitle" as="element(html:div)">
        <xsl:param name="entity" as="element()"/>
        <div xmlns="http://www.w3.org/1999/xhtml" class="systemTitle">
            <xsl:choose>
                <xsl:when test="$entity/content:INTITULE/content:TITRE-ART">
                    <xsl:apply-templates select="$entity/content:INTITULE/content:TITRE-ART" mode="revues2html"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:message>[ERROR] System meta "title" undefined</xsl:message>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:function>

    <xd:doc>
        <xd:desc>
            <xd:p>CAHIER : create abstract of cahier</xd:p>
        </xd:desc>
        <xd:param name="metas">Element of metas</xd:param>
        <xd:return>Abstract of cahier in html:div</xd:return>
    </xd:doc>
    <xsl:function name="xfRes:createAbstractCahier" as="element(html:div)">
        <xsl:param name="metas" as="element()"/>
        <div xmlns="http://www.w3.org/1999/xhtml"  class="systemAbstract">
            <p>
                <xsl:value-of select="xfRes:getCahierId($metas)"/>
            </p>
        </div>
    </xsl:function>

    <xd:doc>
        <xd:desc>
            <xd:p>ARTICLE : create abstract of article</xd:p>
        </xd:desc>
        <xd:param name="metas">Element of metas</xd:param>
        <xd:return>Abstract of article in html:div</xd:return>
    </xd:doc>
    <xsl:function name="xfRes:createAbstract" as="element(html:div)">
        <xsl:param name="entity" as="element()"/>
        <div xmlns="http://www.w3.org/1999/xhtml" class="systemAbstract">
            <p>
                <xsl:text>ID logique&#xA0;: </xsl:text> 
                <xsl:value-of select="xfRes:genAtomicMeta('DZ_META_revuesArticleId', $entity/@ID)"/>
            </p>
            <xsl:if test="$entity//content:COMPO">
                <p>
                    <xsl:text>Compo&#xA0;: </xsl:text> 
                    <xsl:value-of select="$entity//content:COMPO"/>
                </p>
            </xsl:if>
            <xsl:if test="$entity//content:INDEX">
                <p>
                    <xsl:text>Index&#xA0;: </xsl:text> 
                    <xsl:for-each select="$entity//content:INDEX/content:NOME">
                        <xsl:value-of select="string-join(.//content:MOT, ' / ')"/>
                        <xsl:if test="following-sibling::content:NOME">
                            <xsl:text>&#xA0;; </xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </p>
            </xsl:if>
            <xsl:if test="$entity//content:SIGNATURE">
                <p>
                    <xsl:text>Signatures&#xA0;: </xsl:text>
                    <xsl:for-each select="$entity//content:SIGNATURE/content:AUTEUR">
                        <xsl:value-of select="string-join((.//content:PRENOM, .//content:NOM), ' ')"/>
                        <xsl:if test="./parent::content:SIGNATURE/following-sibling::content:SIGNATURE">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </p>
            </xsl:if>
        </div>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>CAHIER : get ID of cahier</xd:p>
        </xd:desc>
        <xd:param name="metas">Element of metas</xd:param>
        <xd:return>ID of cahier</xd:return>
    </xd:doc>
    <xsl:function name="xfRes:getCahierId" as="xs:string">
        <xsl:param name="metas" as="element()"/>
        <xsl:variable name="revuesNom" select="tokenize($metas/ancestor::xf:editorialEntity/@historicalId, '_')[2]"/>
        <xsl:variable name="normaliseCahierNum" select="replace(upper-case(els:normalize-no-diacritic(string-join($metas/xf:meta[@code = 'DZ_META_revuesCahierNum']/xf:value, '_'))), '\p{Zs}', '_')" as="xs:string"/>
        <xsl:sequence select="concat($revuesNom, '/', $metas/xf:meta[@code = 'DZ_META_revuesCahierAnnee']/xf:value, '/', $normaliseCahierNum)"/>
    </xsl:function>

    <xsl:function name="xfRes:getAuteurPrenomNom" as="xs:string*">
        <xsl:param name="element" as="node()*"/>
        <xsl:if test="$element">
            <xsl:for-each select="$element">
                <xsl:value-of select="string-join((content:PRENOM, content:NOM), ' ')"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:function>

    <xsl:function name="xfRes:formatDate" as="xs:string*">
        <xsl:param name="element" as="node()*"/>
        <xsl:if test="$element">
            <xsl:for-each select="$element">
                <xsl:variable name="date" select="replace(., '-', '/')"/>
                <xsl:variable name="dateSequence" select="tokenize($date, '/')" as="xs:string*"/>
                <xsl:if test="count($dateSequence) = 3">
                    <xsl:value-of select="concat($dateSequence[3], '-', $dateSequence[2], '-', $dateSequence[1], 'T00:00:00Z')"/>
                </xsl:if>
            </xsl:for-each>           
        </xsl:if>
    </xsl:function>

    <xsl:function name="xfRes:getCode" as="xs:string*">
        <xsl:param name="element" as="node()*"/>
        <xsl:if test="$element">
            <xsl:for-each select="$element">
                <xsl:value-of select="concat(ancestor::content:CODE/content:TITRCODE, '&#xA0;', .)"/>
            </xsl:for-each>
        </xsl:if>
    </xsl:function>
    
    <xd:doc>
        <xd:desc>
            <xd:p>CAHIER : get info of revue ID</xd:p>
        </xd:desc>
        <xd:param name="id">Revue ID</xd:param>
        <xd:param name="position">Postion of revue ID</xd:param>
        <xd:return>Info of revue</xd:return>
    </xd:doc>
    <xsl:function name="xfRes:getRevuesInfo" as="xs:string">
        <xsl:param name="id" as="xs:string"/>
        <xsl:param name="position" as="xs:integer"/>
        <xsl:value-of select="tokenize($id, '/')[$position]"/>
    </xsl:function>
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" xmlns:content="http://www.lefebvre-sarrut.eu/ns/dalloz/revues" xmlns:html="http://www.w3.org/1999/xhtml" exclude-result-prefixes="#all" xmlns="http://www.w3.org/1999/xhtml" version="2.0">

    <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->

    <!-- IMPORTS -->
    <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
    <xsl:import href="_common/xf-common-dalloz.xsl"/>

    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p>Génère une vue HTML résumée de l'EE.</xd:p>
            <xd:p>Ce résumé sera utilisé lors de l'affichage des EE accrochées dans un arbre.</xd:p>
            <xd:p>Eléments du résumé d'insertion :</xd:p>
            <xd:ul>
                <xd:li>ID logique</xd:li>
                <xd:li>Statut</xd:li>
                <xd:li>Type</xd:li>
            </xd:ul>
        </xd:desc>
    </xd:doc>

    <!-- OUTPUT -->
    <xsl:output encoding="UTF-8" method="xhtml" omit-xml-declaration="no" indent="no"/>

    <!-- LANCEMENT SANS MODE -->
    <!-- Construction du document HTML -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Résumé insertion : <xsl:value-of select="els:getFileName(base-uri(.))"/></title>
                <style type="text/css">
                    .text{
                        color:#0000FF;
                        font-weight:bold;
                    }
                    
                    .valeur{
                        color:#000;
                        font-weight:normal;
                    }</style>
            </head>
            <body>
                <div class="resumeInsertion">
                    <xsl:apply-templates mode="xfRes:referenceNodeAbstract"/>
                </div>
            </body>
        </html>
    </xsl:template>

    <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->

    <xsl:template match="xf:editorialEntity[@code = ('DZ_TEE_revuesDossier', 'DZ_TEE_revuesArticle')]" mode="xfRes:referenceNodeAbstract">
        <xsl:variable name="element" select="xf:body/xf:contentNode/xf:content/content:*" as="element()?"/>
        <div class="text">
            <span>
                <xsl:text>ID logique&#xA0;: </xsl:text>
            </span>
            <span class="valeur">
                <xsl:value-of select="$element/@ID"/>
            </span>
        </div>
        <div class="text">
            <span>
                <xsl:text>Statut&#xA0;: </xsl:text>
            </span>
            <span class="valeur">
                <xsl:value-of select="@status"/>
            </span>
        </div>
        <xsl:if test="$element/@TYPE2">
            <div class="text">
                <span>
                    <xsl:text>Type&#xA0;: </xsl:text>
                </span>
                <span class="valeur">
                    <xsl:value-of select="$element/@TYPE2"/>
                </span>
            </div>
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>

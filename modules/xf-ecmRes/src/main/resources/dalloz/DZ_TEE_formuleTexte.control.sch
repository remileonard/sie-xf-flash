<?xml version="1.0" encoding="utf-8"?>
<!--<schema xmlns="http://www.ascc.net/xml/schematron">-->
<schema xmlns="http://purl.oclc.org/dsdl/schematron" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:saxon="http://saxon.sf.net/"
  xmlns:formule="http://www.lefebvre-sarrut.eu/ns/dalloz/formule" queryBinding="xslt2"
  id="validation_editorialEntity">

  <title>Schematron for Dalloz Document or Letter TEE</title>

  <ns prefix="xsl" uri="http://www.w3.org/1999/XSL/Transform"/>
  <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
  <ns prefix="saxon" uri="http://saxon.sf.net/"/>
  <ns prefix="xf" uri="http://www.lefebvre-sarrut.eu/ns/xmlfirst"/>
  
  <pattern id="CheckMETA_formuleTexteID">
    <rule context="/xf:editorialEntity/xf:metadata/xf:meta[@code='DZ_META_formuleTexteID']/xf:value">
      <report test="not(text()) or text() = ''"   role="warning"> L'ID de la formule est vide !</report>
      <report test="text() = 'XX'" role="error"> L'ID de la formule est invalide !</report>
    </rule>
  </pattern>
  
</schema>

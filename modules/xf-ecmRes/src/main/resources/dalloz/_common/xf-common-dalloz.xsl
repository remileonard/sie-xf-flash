<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
    xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
    xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
    xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
    exclude-result-prefixes="#all" 
    version="2.0">

    <xd:doc>
        <xd:desc>
            <xd:p>Generate an xf:meta (multivalued if multiple values)</xd:p>
        </xd:desc>
        <xd:param name="code">Meta code</xd:param>
        <xd:param name="values">Values</xd:param>
    </xd:doc>
    <xsl:function name="xfRes:genAtomicMeta">
        <xsl:param name="code" as="xs:string"/>
        <xsl:param name="values" as="xs:string*"/>
        <xsl:if test="$values != ''">
            <meta code="{$code}">
                <xsl:for-each select="$values">
                    <value>
                        <xsl:sequence select="."/>
                    </value>
                </xsl:for-each>
            </meta>
        </xsl:if>
    </xsl:function>
    
    <xsl:function name="xfRes:genHtmlMeta" as="element(meta)">
        <xsl:param name="code" as="xs:string"/>
        <xsl:param name="values" as="element(*)*"/> <!--must be html-->
        <xsl:param name="as" as="xs:string"/>
        <meta code="{$code}">
            <xsl:for-each select="$values">
                <xsl:if test="namespace-uri(.) != 'http://www.w3.org/1999/xhtml'">
                    <xsl:message>[ERROR][xfRes:genHtmlMeta] Calling xfRes:genHtmlMeta() with a value which is not in html namespace</xsl:message>
                </xsl:if>
                <value>
                    <xsl:sequence select="."/>
                </value>  
            </xsl:for-each>
        </meta>
    </xsl:function>
    
    <!--Default type is element(html:div)-->
    <xsl:function name="xfRes:genHtmlMeta" as="element(meta)">
        <xsl:param name="code" as="xs:string"/>
        <xsl:param name="values" as="element(*)"/>
        <xsl:sequence select="xfRes:genHtmlMeta($code, $values, 'element(html:div)')"/>
    </xsl:function>
</xsl:stylesheet>
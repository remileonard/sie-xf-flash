<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:dalloz="http://www.lefebvre-sarrut.eu/ns/dalloz"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <!--xpath-default-namespace="http://modeles.dalloz.fr/modeles/reference"-->
  <!--
    "*:" car cette xslt est utilisée dans le sas donc ns = celui de ouvrages
    mais aussi dans certaines conversion de xf-resources donc ns = celui assigné par xmlFirst
  -->
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Converti les élément de titres ou paragraphe simple en html.</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!--==================================================-->
  <!--MODE "xfRes:title2html"-->
  <!--==================================================-->
  
  <!--FIXME : attributs ?-->
  
  <xsl:template match="*:AL" mode="xfRes:title2html">
    <p xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </p>
  </xsl:template>
  
  <xsl:template match="*:FOOTNOTE" mode="xfRes:title2html">
    <i xmlns="http://www.w3.org/1999/xhtml"><xsl:text> </xsl:text><xsl:apply-templates mode="#current"/></i>
  </xsl:template>
  
  <xsl:template match="*:EXP" mode="xfRes:title2html">
    <sup xmlns="http://www.w3.org/1999/xhtml"><xsl:apply-templates mode="#current"/></sup>
  </xsl:template>
  
  <xsl:template match="*:BR" mode="xfRes:title2html">
    <br/>
  </xsl:template>
  
  <xsl:template match="*:GRAS" mode="xfRes:title2html">
    <span xmlns="http://www.w3.org/1999/xhtml" style="font-weight:bold;"><xsl:apply-templates mode="#current"/></span>
  </xsl:template>
  
  <xsl:template match="*:ITAL" mode="xfRes:title2html">
    <span xmlns="http://www.w3.org/1999/xhtml" style="font-style:italic;"><xsl:apply-templates mode="#current"/></span>
  </xsl:template>
  
  <xsl:template match="*:PTCAP" mode="xfRes:title2html">
    <span xmlns="http://www.w3.org/1999/xhtml" style="font-variant: small-caps;"><xsl:apply-templates mode="#current"/></span>
  </xsl:template>
  
  <xsl:template match="*:ROMA" mode="xfRes:title2html">
    <span xmlns="http://www.w3.org/1999/xhtml" style="font-style:normal;"><xsl:apply-templates mode="#current"/></span>
  </xsl:template>
  
  <xsl:template match="*:REF" mode="xfRes:title2html"/>
  
  <!-- 
  <xsl:template match="*:motRep" mode="xfRes:titre2html">
    <strong class="motRep">
      <xsl:apply-templates mode="#current"/>
    </strong>
  </xsl:template>
  
  <xsl:template match="*:e" mode="xfRes:titre2html">
    <sup>
      <xsl:apply-templates mode="#current"/>
    </sup>
  </xsl:template>
  
  <xsl:template match="*:ind" mode="xfRes:titre2html">
    <sub>
      <xsl:apply-templates mode="#current"/>
    </sub>
  </xsl:template>
  
  <xsl:template match="*:ital" mode="xfRes:titre2html">
    <em>
      <xsl:apply-templates mode="#current"/>
    </em>
  </xsl:template>
  
  <xsl:template match="*:url " mode="xfRes:titre2html">
    <a href="{@href}">
      <xsl:apply-templates mode="#current"/>
    </a>
  </xsl:template>
  
  <xsl:template match="*:graphique" mode="xfRes:titre2html">
    FIXME : xf:id ?-->
    <!-- <img href="{@href}">
      <xsl:apply-templates mode="#current"/>
    </img>
  </xsl:template>
  
  <xsl:template match="*:refDocEnrichie" mode="xfRes:titre2html">
    FIXME : xf:id ?-->
    <!-- <span class="refDocEnrichie">
      <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>-->
  
 
  <xsl:template match="*" mode="xfRes:title2html" priority="-1">
    <xsl:message><xsl:value-of select="local-name()"/> non converti en mode xfRes:titre2html</xsl:message>
      <xsl:apply-templates select="node()" mode="#current"/>
  </xsl:template>
  
</xsl:stylesheet>
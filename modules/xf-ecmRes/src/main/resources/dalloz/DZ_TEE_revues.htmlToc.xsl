<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:saxon="http://saxon.sf.net/"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
    xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
    xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
    xmlns:content="http://www.lefebvre-sarrut.eu/ns/dalloz/revues"
    xmlns:html="http://www.w3.org/1999/xhtml"
    exclude-result-prefixes="#all"
    xmlns="http://www.w3.org/1999/xhtml"
    version="2.0">
    
    
    <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
    
    <!-- IMPORTS -->
    <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
    <xsl:import href="_common/title2html.xsl"/>
    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p>Génère une vue HTML "sommaire" de l'EE.</xd:p>
            <xd:p>Cette vue sera affichée dans le panneau de gauche de la fiche de l'EE (en alternance avec les métas).</xd:p>
            <xd:p>Elle permet de visualiser l'arborescence du contenu de l'EE.</xd:p>
            <xd:p>Eléments du sommaire :</xd:p>
            <xd:ul>
                <xd:li>titre de l'EE (a minima)</xd:li>
                <xd:li>signatures</xd:li>
                <xd:li>résumé</xd:li>
            </xd:ul>
        </xd:desc>
    </xd:doc>
    
    <!-- OUTPUT -->
    <xsl:output encoding="UTF-8" method="xhtml" omit-xml-declaration="no" indent="no"/>
    
    <!-- LANCEMENT SANS MODE -->
    <xsl:template match="/">
        <xsl:apply-templates select="." mode="xfRes:htmlToc"/>
    </xsl:template>
    
    
    <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
    
    <!-- Construction du fichier HTML / partie head -->
    <xsl:template match="/" mode="xfRes:htmlToc">
        <xsl:message>[INFO] Calcul du sommaire de l'EE : <xsl:value-of select="base-uri()"/></xsl:message>
        <html>
            <head>
                <title>Sommaire : <xsl:value-of select="els:getFileName(base-uri())"/></title>
                <style type="text/css">
                    body{font-family:sans-serif;font-size:0.8em;}
                    a{text-decoration:none;color:#000000;}
                    div.title{font-size:1.2em;margin-bottom:10px;font-style:italic;}
                    div.signature{font-size:0.9em;margin:0.2em 0 0 10px;}
                    div.resume{font-size:1em;margin:0.2em 0 0 10px;}
                </style>
            </head>
            <xsl:apply-templates mode="#current"/>
        </html>
    </xsl:template>
    
    <!-- Construction du fichier HTML / partie body -->
    <xsl:template match="xf:editorialEntity" mode="xfRes:htmlToc">
        <xsl:variable name="element" select="xf:body/xf:contentNode/xf:content/content:*" as="element()?"/>
        <body>
            <div id="htmlToc">
                <xsl:apply-templates select="$element/content:INTITULE/content:TITRE-ART" mode="#current"/>
                <xsl:apply-templates select="$element//content:SIGNATURES" mode="#current"/>
                <xsl:apply-templates select="($element//content:RESUME | $element//content:SOMMAIRE)[1]" mode="#current"/>
            </div>
        </body>
    </xsl:template>    

    <xsl:template match="content:TITRE-ART" mode="xfRes:htmlToc">
        <div class="title">
            <a href="#"> <!-- Pas d'@id sur revues -->
                <xsl:apply-templates mode="xfRes:title2html"/>
            </a>
        </div>
    </xsl:template>
    
    <xsl:template match="content:SIGNATURES" mode="xfRes:htmlToc">
        <div class="signature">
            <xsl:for-each select="content:SIGNATURE/content:AUTEUR">
                <xsl:value-of select="string-join((.//content:PRENOM, .//content:NOM), ' ')"/>
                <xsl:if test="./parent::content:SIGNATURE/following-sibling::content:SIGNATURE">
                    <xsl:text>, </xsl:text>
                </xsl:if>
            </xsl:for-each>
        </div>
    </xsl:template>
    
    <xsl:template match="content:RESUME | content:SOMMAIRE" mode="xfRes:htmlToc">
        <div class="resume">
            <xsl:apply-templates mode="xfRes:title2html"/>
        </div>
    </xsl:template>
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
                xmlns:infoCommentaire="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
                xmlns:html="http://www.w3.org/1999/xhtml"
                xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
                exclude-result-prefixes="#all"
                xmlns="http://www.w3.org/1999/xhtml"
                version="2.0">


  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <!-- IMPORTS -->
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>

  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Génère une vue HTML résumée de l'EE.</xd:p>
      <xd:p>Ce résumé sera utilisé lors de l'affichage des EE accrochées dans un arbre.</xd:p>
      <xd:p>Pour les données EFL, le résumé d'insertion contient :</xd:p>
      <xd:ul>
        <xd:li>une copie de la métadonnée systemAbstract, qui elle-même contient :
          <xd:ul>
            <xd:li>le résumé de l'EE,</xd:li>
            <xd:li>la/les refDoc principale(s) verbalisée(s) ;</xd:li>
          </xd:ul>
         </xd:li>
        <xd:li>les renvois Memento de l'EE (blocs rattach).</xd:li>
      </xd:ul>
    </xd:desc>
  </xd:doc>
  
  <!-- OUTPUT -->
  <xsl:output encoding="UTF-8" method="xhtml" omit-xml-declaration="no" indent="no"/>
  
  <!-- LANCEMENT SANS MODE -->
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfRes:referenceNodeAbstract"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
  
  <xsl:template match="/" mode="xfRes:referenceNodeAbstract">
    <xsl:message>[INFO] Calcul du résumé d'insertion de l'EE : <xsl:value-of select="base-uri()"/></xsl:message>
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!-- Pour alerte -->
  <xsl:template match="xf:editorialEntity[not(xf:metadata/xf:meta[@code = 'systemAbstract'])]" mode="xfRes:referenceNodeAbstract" priority="2">
    <xsl:message>[ERROR] La métadonnée "systemAbstract" n'est pas présente dans l'EE.</xsl:message>
    <!-- FALLBACK ? -->
    <xsl:next-match/>
  </xsl:template>

  <!-- Pour alerte -->
  <xsl:template match="xf:editorialEntity[not(xf:metadata/xf:meta[@code = 'systemAbstract'][normalize-space()])]" mode="xfRes:referenceNodeAbstract" priority="2">
    <xsl:message>[ERROR] La métadonnée "systemAbstract" est vide.</xsl:message>
    <!-- FALLBACK ? -->
    <xsl:next-match/>
  </xsl:template>
  
  <!-- Construction du document HTML -->
  <xsl:template match="xf:editorialEntity" mode="xfRes:referenceNodeAbstract" priority="1">
    <html>
      <head>
        <title>Résumé insertion : <xsl:value-of select="els:getFileName(base-uri(.))"/></title>
      </head>
      <body>
        <div class="resumeInsertion">
          <xsl:apply-templates mode="#current"/>
        </div>
      </body>
    </html>
  </xsl:template>
  
  
  <!--+==================================================+
      | TRAITEMENT DE xf:meta[@code = 'systemAbstract']  |
      +==================================================+-->
  
  <!-- Copie du contenu de systemAbstract -->
  <xsl:template match="xf:meta[@code = 'systemAbstract']/xf:value[matches(@as,'element\(html:div\)')]" mode="xfRes:referenceNodeAbstract" priority="1">
    <div class="systemAbstract">
      <xsl:copy-of select="*" copy-namespaces="no"/>
    </div>
  </xsl:template>

  <!-- Erreur (?) -->
  <xsl:template match="xf:meta[@code = 'systemAbstract']/xf:value" mode="xfRes:referenceNodeAbstract">
    <xsl:message>[ERROR] La métadonnée "systemAbstract" ne contient pas de noeud HTML.</xsl:message>
    <!-- FALLBACK ? -->
  </xsl:template>
  
  
  <!--+==================================================+
      | RECUPERATION DES RENVOIS MEMENTO                 |
      | CF. PRODPUBLI RENVOIS2_MANAGEBLOCRATTACH.XSL     |
      +==================================================+-->
  
  <!-- Chaque bloc <renvoisXXX> sous <rattach> -> un élément <div> -->
  <xsl:template match="blocRattach/rattach/*[local-name() = ('renvoisMemento', 'renvoisFeuillet', 'renvoisPratique')]" mode="xfRes:referenceNodeAbstract">
    <div class="{local-name()}">
      <xsl:apply-templates mode="#current"/>
    </div>
  </xsl:template>
  
  <!-- Renvoi -> span -->
  <xsl:template match="rMemN | rEflN | rPratN" mode="xfRes:referenceNodeAbstract">
    <span class="{local-name()}">
      <xsl:variable name="countN" select="count(../*[local-name() = ('rMemN', 'rEflN', 'rPratN')])" as="xs:integer"/>
      <xsl:variable name="type" select="@type[normalize-space() != '']" as="attribute()?"/>
      <xsl:variable name="position" select="count(preceding-sibling::*[local-name() = ('rMemN', 'rEflN', 'rPratN')]) + 1" as="xs:integer"/>
      <xsl:variable name="numFin" select="if (@numFin and @type != 's.') then concat(' ', @numFin) else ''" as="xs:string"/>
      <xsl:variable name="preci" select="if (@preci) then concat(' ', @preci) else ''" as="xs:string"/>
      <xsl:choose>
        <xsl:when test="local-name() = 'rEflN'">
          <xsl:variable name="ser" select="(@ser, ../@ser)[1]"/>
          <xsl:variable name="div" select="(@div, ../@div)[1]"/>
          <xsl:choose>
            <xsl:when test="$position = 1">
              <xsl:value-of select="concat($ser, '-', $div, '-', @num, if ($type) then concat('&#160;', $type) else '', $numFin, $preci)"/>
            </xsl:when>
            <xsl:when test="$position = $countN">
              <xsl:value-of select="concat(' et ', @num, if ($type) then concat('&#160;', $type) else '', $numFin, $preci)"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="concat(', ', @num, if ($type) then concat('&#160;', $type) else '', $numFin, $preci)"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="nom" select="(@nom, ../@nom)[1]"/>
          <xsl:variable name="mille" select="(@mille, ../@mille)[1]"/>
          <xsl:choose>
            <xsl:when test="$position = 1">
              <xsl:value-of select="concat(upper-case($nom),' ',$mille)"/>
              <xsl:text> n</xsl:text>
              <sup>
                <xsl:value-of select="if ($countN > 1 or ($countN = 1 and @type)) then 'os' else 'o'"/>
              </sup>
              <xsl:text>&#160;</xsl:text>
              <xsl:value-of select="concat(@num, if ($type) then concat('&#160;', $type) else '', $numFin, $preci)"/>
            </xsl:when>
            <xsl:when test="$position = $countN">
              <xsl:value-of select="concat(' et ', @num, if ($type) then concat('&#160;', $type) else '', $numFin, $preci)"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="concat(', ', @num, if ($type) then concat('&#160;', $type) else '', $numFin, $preci)"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </span>
  </xsl:template>
  
  
  <!--+==================================================+
      | KO -> NE PAS UTILISER                            |
      | RECUPERATION ET REGROUPEMENT DES RENVOIS MEMENTO |
      +==================================================+-->
  
  <!-- Récupération de tous les renvois Memento de l'EE
       Regroupement par type de Memento (renvoisMemento/@nom) + tri par numéro de renvoi -->
  <!--<xsl:template match="infoCommentaire[.//rattach/renvoisMemento/rMemN]">
    <div class="renvoisMemento">
      <!-\- Regroupement par type de Memento + tri numérique -\->
      <xsl:for-each-group select=".//rattach/renvoisMemento/rMemN" group-by="parent::renvoisMemento/@nom">
        <xsl:sort select="current-grouping-key()" order="ascending" data-type="text"/>
        <xsl:for-each select="current-group()">
          <xsl:sort select="@num" order="ascending" data-type="number"/>
          <xsl:variable name="mille" select="parent::renvoisMemento/@mille" as="xs:string?"/>
          <span class="rMemN">
            <xsl:value-of select="current-grouping-key()"/>
            <xsl:text> </xsl:text>
            <xsl:if test="$mille != ''">
              <xsl:value-of select="$mille"/>
              <xsl:text> </xsl:text>
            </xsl:if>
            <xsl:text>n° </xsl:text>
            <xsl:value-of select="@num"/>
            <xsl:if test="@type = ('à', 's.')">
              <xsl:text> </xsl:text>
              <xsl:value-of select="@type"/>
              <xsl:if test="@type = 'à'">
                <xsl:text> </xsl:text>
                <xsl:value-of select="@numFin"/>
              </xsl:if>
            </xsl:if>
          </span>
          <xsl:if test="position() != last()">
            <xsl:text> / </xsl:text>
          </xsl:if>
        </xsl:for-each>
        <xsl:if test="position() != last()">
          <xsl:text> / </xsl:text>
        </xsl:if>
      </xsl:for-each-group>
    </div>
  </xsl:template>-->
  
  
  <!--+==================================================+
      | DEFAULT                                          |
      +==================================================+-->
  
  <xsl:template match="*" mode="xfRes:referenceNodeAbstract"/>
  <xsl:template match="*[descendant-or-self::xf:meta or descendant-or-self::rattach]" mode="xfRes:referenceNodeAbstract">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>

</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns:infoCom="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Generate a really simple HTML with span and div.</xd:p>
      <xd:p>Used as default simple preview for EE type text</xd:p>
    </xd:desc>
  </xd:doc>
  
  <xsl:output method="xhtml" indent="no" encoding="utf-8"/>
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfRes:htmlSimplePreview"/>
  </xsl:template>
  
  <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
  
  <xsl:template match="/" mode="xfRes:htmlSimplePreview">
    <html>
      <head>
        <title><xsl:value-of select="tokenize(base-uri(.), '/')[last()]"/></title>
      </head>
      <body class="infoCommentaire">
        <xsl:apply-templates mode="#current"/>
      </body>
    </html>
  </xsl:template>
  
  <xsl:template match="xf:metadata" mode="xfRes:htmlSimplePreview"/>
  
  <xsl:template match="xf:*" mode="xfRes:htmlSimplePreview">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="infoCom:infoCommentaire" mode="xfRes:htmlSimplePreview">
    <div class="ecm_content">
      <xsl:apply-templates mode="#current"/>
    </div>
  </xsl:template>
  
  <!--div or span by default trying to guess if it's an inline or block element-->
  <xsl:template match="*" mode="xfRes:htmlSimplePreview">
    <xsl:choose>
      <xsl:when test="preceding-sibling::text()[normalize-space(.)] or following-sibling::text()[normalize-space(.)]">
        <span class="{local-name()}">
          <xsl:apply-templates mode="#current"/>
        </span>
      </xsl:when>
      <xsl:otherwise>
        <div class="{local-name()}">
          <xsl:apply-templates mode="#current"/>
        </div>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
</xsl:stylesheet>
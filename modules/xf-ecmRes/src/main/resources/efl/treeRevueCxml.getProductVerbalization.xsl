<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" 
  xmlns:xfRef="http://modeles.efl.fr/modeles/reference"
  exclude-result-prefixes="#all" 
  version="2.0">
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Lit le référentiel produit (referentiels/referentielCodeTee.xml) sortir le code libelle</xd:p>
      <xd:p>Utilisation : calcul des méta systemTitle et SAS pour le mapping</xd:p>
      <xd:p>FIXME : (09/02/2017) Référentiel mis dans le projet xf-ecmRes pour l'instant; à sortir du projet si possible.</xd:p>
    </xd:desc>
  </xd:doc>

  <xsl:param name="xfRes:referentialCodeTee" select="document('_common/referentielCodeTee.xml')" as="document-node()"/>

  <xd:doc>
    <xd:desc>
      <xd:b>Retourne le verbalisation du code TEE donné en paramêtre.</xd:b>
      <xd:param>Code TEE : (xs:string)</xd:param>
      <xd:param>labelType : "short" ou "long (xs:string)</xd:param>
    </xd:desc>
  </xd:doc>
  <xsl:function name="xfRes:getVerbalizationCodeTee" as="xs:string?">
    <xsl:param name="codeTee" as="xs:string"/>
    <xsl:param name="labelType" as="xs:string"/>
    <xsl:variable name="authorisedLabelTypes" select="('short','long')" as="xs:string+"/>
    <!-- FIXME : Supprimer ces variables quand utilisation des référentiels FLASH -> utiliser $labelType directement -->
    <xsl:variable name="labelNames" select="('libelleAbrege','libelleComplet')" as="xs:string+"/>
    <xsl:variable name="labelName" select="$labelNames[index-of($authorisedLabelTypes,$labelType)]"/>
    <xsl:if test="not($labelType = $authorisedLabelTypes)">
      <xsl:message>ERROR : le paramètre labelType "<xsl:value-of select="$labelType"/>" n'est pas autorisé.</xsl:message>
    </xsl:if>
    <!-- FIXME : A reprendre quand utilisation des référentiels FLASH -->
    <xsl:value-of select="$xfRes:referentialCodeTee//xfRef:*[@flashId = $codeTee]/xfRef:*[local-name() = $labelName]"/>
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:b>Retourne le code TEE en fonction du produitCx.</xd:b>
      <xd:param>Code TEE : (xs:string)</xd:param>
      <xd:param>labelType : "short" ou "long (xs:string)</xd:param>
    </xd:desc>
  </xd:doc>
<!--  <xsl:function name="xfRes:getCodeTee" as="xs:string?">
    <xsl:param name="produitCx" as="xs:string"/>
    <!-\- TO DO : A reprendre quand utilisation des référentiels FLASH -\->
    <xsl:value-of select="$xfRes:referentialCodeTee//xfRef:*[@cxmlId = $produitCx]/@flashId"/>
  </xsl:function>-->
</xsl:stylesheet>

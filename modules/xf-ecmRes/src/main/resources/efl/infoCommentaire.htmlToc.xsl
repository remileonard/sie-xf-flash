<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:saxon="http://saxon.sf.net/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns:infoCommentaire="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
  exclude-result-prefixes="#all"
  xmlns="http://www.w3.org/1999/xhtml"
  version="2.0">
  
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <!-- IMPORTS -->
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  <xsl:import href="xf-ecmRes:/efl/_common/cxml2html.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Génère une vue HTML "sommaire" de l'EE.</xd:p>
      <xd:p>Cette vue sera affichée dans le panneau de gauche de la fiche de l'EE (en alternance avec les métas).</xd:p>
      <xd:p>Elle permet de visualiser l'arborescence du contenu de l'EE.</xd:p>
      <xd:p>Eléments du sommaire :</xd:p>
      <xd:ul>
        <xd:li>titre de l'EE (a minima),</xd:li>
        <xd:li>titres des niveaux (niveau/titre)</xd:li>
      </xd:ul>
    </xd:desc>
  </xd:doc>

  <!-- OUTPUT -->
  <xsl:output encoding="UTF-8" method="xhtml" omit-xml-declaration="no" indent="no"/>
  
  <!-- LANCEMENT SANS MODE -->
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfRes:htmlToc"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
  
  <xsl:template match="/" mode="xfRes:htmlToc">
    <xsl:message>[INFO] Calcul du sommaire de l'EE : <xsl:value-of select="base-uri()"/></xsl:message>
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!-- Construction du fichier HTML / partie head -->
  <xsl:template match="/xf:editorialEntity" mode="xfRes:htmlToc" priority="1">
    <html>
      <head>
        <title>Sommaire : <xsl:value-of select="els:getFileName(base-uri())"/></title>
        <style type="text/css">
          body{font-family:sans-serif;font-size:0.8em;}
          a{text-decoration:none;color:#000000;}
          div.title{font-size:1.2em;margin-bottom:20px;font-style:italic;}
          h1{font-size:1.3em;margin:0.5em 0 0 0;}
          h2{font-size:1.1em;margin:0.3em 0 0 10px;}
          h3{font-size:1.1em;margin:0.2em 0 0 20px;font-weight:normal;}
          h4{font-size:1em;margin:0.1em 0 0 30px;font-weight:normal;}
          h5{font-size:0.9em;margin:0.2em 0 0 35px;font-weight:normal;font-style:italic;}
          h6{font-size:0.9em;margin:0.2em 0 0 40px;font-weight:normal;}
          div.h7,div.h8,div.h9,div.h10,div.h11{font-size:0.9em;margin:0.2em 0 0 45px;}
          div.h0{font-size:1em;margin:0.2em 0 0 0;font-style:italic;color:red;}
          div.h12{font-size:1em;margin:0.2em 0 0 0;font-style:italic;}
        </style>
      </head>
      <xsl:apply-templates mode="#current"/>
    </html>
  </xsl:template>
  
  <!-- Pour alerte -->
  <xsl:template match="infoCommentaire[count(.//niveau) = 0]" mode="xfRes:htmlToc" priority="2">
    <xsl:message>[INFO] Il n'y a pas de niveau dans cette EE.</xsl:message>
    <xsl:next-match/>
  </xsl:template>
  
  <!-- Pour alerte -->
  <xsl:template match="infoCommentaire[.//niveau/titre[@type]]" mode="xfRes:htmlToc" priority="2">
    <xsl:message>[WARNING] Il semble que les éléments "niveau" ne soient pas imbriqués dans cette EE (présence d'éléments "titre" avec attribut @type).</xsl:message>
    <xsl:next-match/>
  </xsl:template>
  
  <!-- Construction du fichier HTML / partie body -->
  <xsl:template match="infoCommentaire" mode="xfRes:htmlToc" priority="1">
    <body>
      <div id="htmlToc">
        <xsl:apply-templates mode="#current"/>
      </div>
    </body>
  </xsl:template>
  
  
  <!--+==================================================+
      | GESTION DES ENTREES DU SOMMAIRE                  |
      +==================================================+-->
  
  <!-- Titre de l'infoCommentaire toujours affiché, pour éviter les sommaires vides -->
  <xsl:template match="infoCommentaire/titre" mode="xfRes:htmlToc">
    <div class="title">
      <a href="#"> <!-- Pas d'@id sur infoCommentaire -->
        <xsl:apply-templates mode="xfRes:cxml2html"/>
      </a>
    </div>
  </xsl:template>

  <!-- Niveaux de titre imbriqués -> utilisation de la hiérarchie des niveau -->
  <xsl:template match="niveau/titre" mode="xfRes:htmlToc">
    <xsl:variable name="lvl" select="xfRes:getHierarchicalTitleLevel(.)"/>
    <xsl:call-template name="xfRes:makeHeaderMarkup">
      <xsl:with-param name="lvl" select="$lvl"/>
    </xsl:call-template>
  </xsl:template>

  <!-- Niveaux de titre typés -> utilisation de la valeur de l'attribut @type -->
  <xsl:template match="niveau/titre[@type != '']" mode="xfRes:htmlToc" priority="1">
    <xsl:variable name="lvl" select="xfRes:getTypedTitleLevel(.)"/>
    <xsl:call-template name="xfRes:makeHeaderMarkup">
      <xsl:with-param name="lvl" select="$lvl"/>
    </xsl:call-template>
  </xsl:template>
  
  
  <!--+==================================================+
      | ELEMENTS INLINE                                  |
      +==================================================+-->

  <!-- Surcharge de xfRes:cxml2html -->
  
  <!-- Ignorés -->
  <xsl:template match="rHorsTexte | rNote | renvoiAutrui" mode="xfRes:cxml2html">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>


  <!--+==================================================+
      | DEFAULT                                          |
      +==================================================+-->

  <xsl:template match="*" mode="xfRes:htmlToc"/>
  <xsl:template match="*[descendant::infoCommentaire or descendant-or-self::niveau]" mode="xfRes:htmlToc">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | TEMPLATES NOMMES                                 |
      +==================================================+-->
  
  <xsl:template name="xfRes:makeHeaderMarkup">
    <xsl:param name="lvl" required="yes" as="xs:integer"/>
    <xsl:variable name="h.lvl" select="(1,2,3,4,5,6)" as="xs:integer+"/>
    <!-- Elément HTML h autorisé jusqu'à h6 seulement ! -->
    <xsl:element name="{if ($lvl = $h.lvl) then (concat('h',$lvl)) else ('div')}">
      <xsl:if test="not($lvl = $h.lvl)">
        <xsl:attribute name="class" select="concat('h',$lvl)"/>
      </xsl:if>
      <!-- Lien vers niveau parent (-> utilisation de @id / c'est ce que fait la transformation XFE) -->
      <a href="#{parent::niveau/@id}">
        <xsl:apply-templates mode="xfRes:cxml2html"/>
      </a>
    </xsl:element>
  </xsl:template>
  
  
  <!--+==================================================+
      | FONCTIONS                                        |
      +==================================================+-->
  
  <!-- Obtention du niveau de titre via la hiérarchie des ancêtres <niveau> -->
  <xsl:function name="xfRes:getHierarchicalTitleLevel" as="xs:integer">
    <xsl:param name="elt" as="element(titre)"/>
    <xsl:value-of select="count($elt/ancestor::niveau)"/>
  </xsl:function>
  
  <!-- Obtention du niveau de titre via l'attribut @type -->
  <xsl:function name="xfRes:getTypedTitleLevel" as="xs:integer">
    <xsl:param name="elt" as="element(titre)"/>
    <xsl:variable name="lvl" select="substring-after($elt/@type,'titre')"/>
    <xsl:choose>
      <xsl:when test="$lvl castable as xs:integer">
        <xsl:value-of select="$lvl cast as xs:integer"/>
      </xsl:when>
      <xsl:when test="$lvl = 'Profond'">
        <xsl:message>[WARNING] Titre profond détecté.</xsl:message>
        <xsl:value-of select="12"/> <!-- Cf. transformation XFE -->
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>[ERROR] Niveau inconnu détecté : <xsl:value-of select="$lvl"/>.</xsl:message>
        <xsl:value-of select="0"/> <!-- FALLBACK ? -->
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
</xsl:stylesheet>
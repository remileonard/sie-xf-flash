<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <xsl:include href="xf-ecmRes:/efl/_common/cxml2html.xsl"/>
  
  <xsl:template match="xf:editorialEntity" mode="xfRes:EFL_TEE_infoCommentaire.getCalculatedMetatada">
    <metadata>
      <meta code="systemTitle" as="element(html:div)">
        <value as="element(html:div)">
          <xsl:variable name="titres" select="xf:body/xf:contentNode/xf:content/infoCommentaire/*[local-name() = ('titre', 'titreObjet', 'titreUne', 'titreSommaire')]" as="element()*"/>
          <div xmlns="http://www.w3.org/1999/xhtml">
            <xsl:choose>
              <xsl:when test="count($titres) = 0">
                <xsl:message>[INFO][XF-ECM-RES] System meta "title" undefined for xf:id=<xsl:value-of select="@xf:id"/></xsl:message>
              </xsl:when>
              <xsl:otherwise>
                <xsl:apply-templates select="$titres[1]/node()" mode="xfRes:cxml2html_systemTitle"/>
              </xsl:otherwise>
            </xsl:choose>
          </div>
        </value>
      </meta>
      <meta code="systemAbstract" as="element(html:div)">
        <value as="element(html:div)">
          <xsl:variable name="abstracts" select="xf:body/xf:contentNode/xf:content/infoCommentaire/microContenu/*[local-name() = ('resume', 'accroche', 'introduction')]" as="element()*"/>
          <div xmlns="http://www.w3.org/1999/xhtml">
            <xsl:choose>
              <xsl:when test="count($abstracts) = 0">
                <xsl:message>[INFO][XF-ECM-RES] System meta "abstract" undefined for xf:id=<xsl:value-of select="@xf:id"/></xsl:message>
              </xsl:when>
              <xsl:otherwise>
                <xsl:apply-templates select="$abstracts[1]/node()" mode="xfRes:cxml2html_systemAbstract"/>
                <!-- Récupération des refDoc principales (//blocRefDoc//refDoc) -->
                <!-- Ajout des refDoc principales -->
                <xsl:apply-templates select="xf:body/xf:contentNode/xf:content/infoCommentaire//blocRefDoc//refDoc" mode="xfRes:cxml2html_refDocSystemAbstract"/>
              </xsl:otherwise>
            </xsl:choose>
          </div>
        </value>
      </meta>
    </metadata>
  </xsl:template>
  
  <!--Mode spécifique pour l'ajout des refDoc-->
  
  <xsl:template match="refDoc" mode="xfRes:cxml2html_refDocSystemAbstract">
    <span xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="xfRes:cxml2html"/>
    </span>
  </xsl:template>
  
  <!--==================================================================-->
  <!--MODE xfRes:cxml2html_systemTitle et xfRes:cxml2html_systemAbstract-->
  <!--==================================================================-->
  <!--(surcharge de xfRes:cxml2html)-->
  
  <xsl:template match="*" mode="xfRes:cxml2html_systemTitle xfRes:cxml2html_systemAbstract">
    <xsl:apply-templates select="." mode="xfRes:cxml2html"/>
  </xsl:template>
  
  <xsl:template match="refDoc" mode="xfRes:cxml2html_systemAbstract">
    <xsl:apply-templates mode="xfRes:cxml2html"/>
  </xsl:template>
  
  <xsl:template match="refDocEnrichie" mode="xfRes:cxml2html_systemAbstract">
    <xsl:apply-templates mode="xfRes:cxml2html"/>
  </xsl:template>
  
  <xsl:template match="refDocReprise" mode="xfRes:cxml2html_systemAbstract">
    <xsl:apply-templates mode="xfRes:cxml2html"/>
  </xsl:template>
  
  <xsl:template match="renvoiAutrui" mode="xfRes:cxml2html_systemAbstract">
    <xsl:apply-templates mode="xfRes:cxml2html"/>
  </xsl:template>
  
  <xsl:template match="renvoiAutruiTransition" mode="xfRes:cxml2html_systemAbstract">
    <xsl:apply-templates mode="xfRes:cxml2html"/>
  </xsl:template>
  
</xsl:stylesheet>
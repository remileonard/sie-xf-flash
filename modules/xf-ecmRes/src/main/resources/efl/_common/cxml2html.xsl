<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <!--xpath-default-namespace="http://modeles.efl.fr/modeles/reference"-->
  <!--
    "*:" car cette xslt est utilisée dans le sas donc ns = celui de chaîne XML
    mais aussi dans certaines conversion de xf-resources donc ns = celui assigné par xmlFirst
  -->
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Converti les élément Chaîne XML en html.</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!--==================================================-->
  <!--MODE "xfRes:cxml2html"-->
  <!--==================================================-->
  
  <!--FIXME : attributs ?-->
  
  <xsl:template match="*:al" mode="xfRes:cxml2html">
    <p>
      <xsl:apply-templates mode="#current"/>
    </p>
  </xsl:template>
  
  <xsl:template match="*:motRep" mode="xfRes:cxml2html">
    <strong class="motRep">
      <xsl:apply-templates mode="#current"/>
    </strong>
  </xsl:template>
  
  <xsl:template match="*:e" mode="xfRes:cxml2html">
    <sup>
      <xsl:apply-templates mode="#current"/>
    </sup>
  </xsl:template>
  
  <xsl:template match="*:ind" mode="xfRes:cxml2html">
    <sub>
      <xsl:apply-templates mode="#current"/>
    </sub>
  </xsl:template>
  
  <xsl:template match="*:ital" mode="xfRes:cxml2html">
    <em>
      <xsl:apply-templates mode="#current"/>
    </em>
  </xsl:template>
  
  <xsl:template match="*:url " mode="xfRes:cxml2html">
    <a href="{@href}">
      <xsl:apply-templates mode="#current"/>
    </a>
  </xsl:template>
  
  <xsl:template match="*:graphique" mode="xfRes:cxml2html">
    <!--FIXME : xf:id ?-->
    <img href="{@href}">
      <xsl:apply-templates mode="#current"/>
    </img>
  </xsl:template>
  
  <xsl:template match="*:refDocEnrichie" mode="xfRes:cxml2html">
    <!--FIXME : xf:id ?-->
    <span class="refDocEnrichie">
      <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>
    
  <!-- Cas géré : ces éléments existent au niveau de la méta systemAbstract -->
  <xsl:template match="*:refDoc | *:refDocReprise | *:renvoiAutrui | *:renvoiAutruiTransition | *:sousTitre" mode="xfRes:cxml2html">
    <span class="{local-name()}">
      <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>
  
  <!-- liste par défaut -->
  <xsl:template match="*:lst" mode="xfRes:cxml2html">
    <ul>
      <xsl:apply-templates mode="#current"/>
    </ul>
  </xsl:template>
  
  <!-- item de liste par défaut -->
  <xsl:template match="*:lst/*:item" mode="xfRes:cxml2html">
    <li>
      <xsl:apply-templates mode="#current"/>
    </li>
  </xsl:template>
  
  <xsl:template match="*" mode="xfRes:cxml2html" priority="-1">
    <xsl:message><xsl:value-of select="local-name()"/> non converti en mode xfRes:cxml2html</xsl:message>
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
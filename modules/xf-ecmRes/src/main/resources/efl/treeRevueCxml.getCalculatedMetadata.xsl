<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:infoCommentaire="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire" 
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" 
  xmlns:h="http://www.w3.org/1999/xhtml" 
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire" 
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  exclude-result-prefixes="#all" 
  version="2.0">

  <!-- Verbalization du code TEE -->
  <xsl:import href="xf-ecmRes:/efl/treeRevueCxml.getProductVerbalization.xsl"/>

  <xsl:template match="xf:editorialEntity" mode="xfRes:EFL_TEE_treeRevueCxml.getCalculatedMetatada">
    <metadata>
      <meta code="systemTitle" as="element(html:div)">
        <value as="element(html:div)">
          <!-- Flas75 point 2 et 4 : suppression de la meta EFL_META_produitCx -->
          <xsl:variable name="shortVerbalizationCodeTee" select="xfRes:getVerbalizationCodeTee(@code, 'short')"/>
          <xsl:variable name="numeroOrdre" select="xf:metadata/xf:meta[@code = 'EFL_META_numeroOrdre']/xf:value" as="xs:string?"/>
          <xsl:variable name="anneePublication" select="xf:metadata/xf:meta[@code = 'EFL_META_anneePublication']/xf:value" as="xs:integer?"/>
          <!-- L'année est représentées sur les deux derniers digits -->
          <xsl:variable name="anneeTronquee" as="xs:integer?">
            <xsl:choose>
              <xsl:when test="$anneePublication > 1000">
                <xsl:value-of select="$anneePublication mod 1000"/>
              </xsl:when>
              <xsl:when test="$anneePublication > 100">
                <xsl:value-of select="$anneePublication mod 100"/>
              </xsl:when>
              <xsl:when test="$anneePublication > 10">
                <xsl:value-of select="$anneePublication"/>
              </xsl:when>
              <xsl:otherwise/>
            </xsl:choose>
          </xsl:variable>
          <div xmlns="http://www.w3.org/1999/xhtml">
            <xsl:variable name="systemTitle.calculated" select="concat($shortVerbalizationCodeTee, ' ', $numeroOrdre, '/', $anneeTronquee)" as="xs:string"/>
            <!--FLAS-47 : cas spécial où il y a une méta EFL_META_title (non vide) : alors c'est elle qu'on mets dans le systemTitle-->
            <xsl:variable name="EFL_META_title.value" select="xf:metadata/xf:meta[@code = 'EFL_META_title']/xf:value" as="element(xf:value)?"/>
            <xsl:choose>
              <xsl:when test="string-join($EFL_META_title.value/h:div//text(), '') != '' ">
                <xsl:sequence select="$EFL_META_title.value/h:div/node()"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:sequence select="$systemTitle.calculated"/>
              </xsl:otherwise>
            </xsl:choose>
          </div>
        </value>
      </meta>
      <xsl:call-template name="xfRes:efl_getDefaultSystemAbstractMeta"/>
    </metadata>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>
      <xd:p>
        Calculated metadata for TEE (OM).
      </xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="xf:editorialEntity" mode="xfRes:EFL_TEE_treeRevueCxml.getCalculatedMetatada4OM">
    <metadata>
      <meta code="systemTitle" as="element(html:div)">
        <value as="element(html:div)">
          <!-- Flas75 point 2 et 4 : suppression de la meta EFL_META_produitCx -->
          <xsl:variable name="shortVerbalizationCodeTee" select="xfRes:getVerbalizationCodeTee(@code, 'short')"/>
          <xsl:variable name="verbalizationMetier" select="string-join(xf:metadata/xf:meta[@code = 'EFL_META_metier']/xf:verbalization, ' - ')" as="xs:string?"/>
          <div xmlns="http://www.w3.org/1999/xhtml">
            <xsl:variable name="systemTitle.calculated" select="concat('Actualités OM – INNEO ', $verbalizationMetier)" as="xs:string"/>
            <xsl:sequence select="normalize-space($systemTitle.calculated)"/>
          </div>
        </value>
      </meta>      
      <xsl:call-template name="xfRes:efl_getDefaultSystemAbstractMeta"/>
    </metadata>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>
      <xd:p>
        Calculated metadata for TEE (Matiere).
      </xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="xf:editorialEntity" mode="xfRes:EFL_TEE_treeRevueCxml.getCalculatedMetatada4Matiere">
    <metadata>
      <meta code="systemTitle" as="element(html:div)">
        <value as="element(html:div)">
          <!-- Flas75 point 2 et 4 : suppression de la meta EFL_META_produitCx -->
          <xsl:variable name="shortVerbalizationCodeTee" select="xfRes:getVerbalizationCodeTee(@code, 'short')"/>
          <xsl:variable name="verbalizationMatiere" select="string-join(xf:metadata/xf:meta[@code = 'EFL_META_matiere']/xf:verbalization, ' - ')" as="xs:string?"/>
          <div xmlns="http://www.w3.org/1999/xhtml">
            <xsl:variable name="systemTitle.calculated" select="concat('Fil Info ', $verbalizationMatiere)" as="xs:string"/>
            <xsl:sequence select="normalize-space($systemTitle.calculated)"/>
          </div>
        </value>
      </meta>      
      <xsl:call-template name="xfRes:efl_getDefaultSystemAbstractMeta"/>
    </metadata>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>
      <xd:p>
        Calculated metadata for TEE (Quotidienne).
      </xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template match="xf:editorialEntity" mode="xfRes:EFL_TEE_treeRevueCxml.getCalculatedMetatada4Quotidienne">
    <metadata>
      <meta code="systemTitle" as="element(html:div)">
        <value as="element(html:div)">
          <!-- Flas75 point 2 et 4 : suppression de la meta EFL_META_produitCx -->
          <xsl:variable name="shortVerbalizationCodeTee" select="xfRes:getVerbalizationCodeTee(@code, 'short')"/>
          <xsl:variable name="dateParution" select="xf:metadata/xf:meta[@code = 'EFL_META_dateParution']/xf:value" as="xs:string?"/>
          <div xmlns="http://www.w3.org/1999/xhtml">
            <xsl:variable name="systemTitle.calculated" select="concat('La Quotidienne du ', substring-before(xfRes:getDayFromIsoDate($dateParution), 'T'), ' ', xfRes:getMonthFromIsoDate($dateParution), ' ', xfRes:getYearFromIsoDate($dateParution))" as="xs:string"/>
            <xsl:variable name="EFL_META_title.value" select="xf:metadata/xf:meta[@code = 'EFL_META_title']/xf:value" as="element(xf:value)?"/>
            <xsl:choose>
              <xsl:when test="string-join($EFL_META_title.value/h:div//text(), '') != '' ">
                <xsl:sequence select="$EFL_META_title.value/h:div/node()"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:sequence select="$systemTitle.calculated"/>
              </xsl:otherwise>
            </xsl:choose>            
          </div>
        </value>
      </meta>      
      <xsl:call-template name="xfRes:efl_getDefaultSystemAbstractMeta"/>
    </metadata>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Get the default systemAbstract Meta (EFL)</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:template name="xfRes:efl_getDefaultSystemAbstractMeta">
    <meta code="systemAbstract" as="element(html:div)">
      <value as="element(html:div)">
        <xsl:variable name="abstracts" select="xf:body/xf:contentNode/xf:content/infoCommentaire/microContenu/*[local-name() = ('resume', 'accroche', 'introduction')]" as="element()*"/>
        <div xmlns="http://www.w3.org/1999/xhtml">
          <!--systemAbstract vide (nous ne savons pas quoi y mettre pour le moment)-->
        </div>
      </value>
    </meta>
  </xsl:template>
  
  <!-- ******************************************************** -->
  <!-- Urgence 24 03 2017 : factoriser les fonction si dessous  -->
  <!-- ******************************************************** -->
    
  <!--AAAA-MM-JJ => AAAA-->
  <xsl:function name="xfRes:getYearFromIsoDate" as="xs:string">
    <xsl:param name="isoDate" as="xs:string"/>
    <xsl:value-of select="tokenize($isoDate, '-')[1]"/>
  </xsl:function>
  
  <!--AAAA-MM-JJ => MM-->
  <xsl:function name="xfRes:getMonthFromIsoDate" as="xs:string">
    <xsl:param name="isoDate" as="xs:string"/>
    <xsl:variable name="numMonth" select="tokenize($isoDate, '-')[2]"/>
    <xsl:choose>
      <xsl:when test="$numMonth = '01'">janvier</xsl:when>
      <xsl:when test="$numMonth = '02'">février</xsl:when>
      <xsl:when test="$numMonth = '03'">mars</xsl:when>
      <xsl:when test="$numMonth = '04'">avril</xsl:when>
      <xsl:when test="$numMonth = '05'">mai</xsl:when>
      <xsl:when test="$numMonth = '06'">juin</xsl:when>
      <xsl:when test="$numMonth = '07'">juillet</xsl:when>
      <xsl:when test="$numMonth = '08'">aout</xsl:when>
      <xsl:when test="$numMonth = '09'">septembre</xsl:when>
      <xsl:when test="$numMonth = '10'">octobre</xsl:when>
      <xsl:when test="$numMonth = '11'">novembre</xsl:when>
      <xsl:when test="$numMonth = '12'">décembre</xsl:when>
      <xsl:otherwise>undefined</xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <!--AAAA-MM-JJ => JJ-->
  <xsl:function name="xfRes:getDayFromIsoDate" as="xs:string">
    <xsl:param name="isoDate" as="xs:string"/>
    <xsl:value-of select="tokenize($isoDate, '-')[3]"/>
  </xsl:function>
</xsl:stylesheet>

<?xml version="1.0" encoding="utf-8"?>
<!--<schema xmlns="http://www.ascc.net/xml/schematron">-->
<schema xmlns="http://purl.oclc.org/dsdl/schematron"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:saxon="http://saxon.sf.net/" xmlns:c="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
  queryBinding="xslt2" id="validation_editorialEntity">

  <title>Schematron for EFL infoCommentaire TEE</title>

  <ns prefix="xsl" uri="http://www.w3.org/1999/XSL/Transform"/>
  <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
  <ns prefix="saxon" uri="http://saxon.sf.net/"/>
  <ns prefix="xf" uri="http://www.lefebvre-sarrut.eu/ns/xmlfirst"/>

  <pattern id="test">
    <rule context="xf:body/xf:structuralNode">
      <assert
        test="xf:metadata/xf:meta[@code='EFL_META_marqueurStructure']/xf:verbalization/normalize-space()=('corpus','sommaire')"
        role="error"> [TEST] Les structuralNode du 1e niveau doivent toujours être des sommaire ou
        corpus ! </assert>
    </rule>
  </pattern
  >
  <pattern id="test2">
    <rule
      context="xf:editorialEntity[xf:metadata/xf:meta[@code='EFL_META_produitCx']/xf:verbalization/normalize-space()='BRDA']/xf:body/xf:structuralNode[xf:metadata/xf:meta[@code='EFL_META_marqueurStructure']/xf:verbalization/normalize-space()=('sommaire')]">
      <assert
        test="xf:structuralNode[1]/xf:metadata/xf:meta[@code='EFL_META_marqueurStructure']/xf:verbalization/normalize-space()=('ALaUne') and xf:structuralNode[2]/xf:metadata/xf:meta[@code='EFL_META_marqueurStructure']/xf:verbalization/normalize-space()=('lireAussi')"
        role="error"> [TEST] Dans un sommaire, le premier niveau est ALaUne et le deuxieme est
        lireAussi. </assert>
    </rule>

  </pattern>

</schema>

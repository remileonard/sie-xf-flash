<?xml version="1.0" encoding="utf-8"?>
<!--<schema xmlns="http://www.ascc.net/xml/schematron">-->
<schema 
  xmlns="http://purl.oclc.org/dsdl/schematron"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:saxon="http://saxon.sf.net/"
  xmlns:c="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
  queryBinding="xslt2"
  
  id="validation_editorialEntity">
  
  <title>Schematron for EFL infoCommentaire TEE</title>
  
  <ns prefix="xsl" uri="http://www.w3.org/1999/XSL/Transform"/>
  <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
  <ns prefix="saxon" uri="http://saxon.sf.net/"/>
  <ns prefix="cxml" uri="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"/>
  <ns prefix="xf" uri="http://www.lefebvre-sarrut.eu/ns/xmlfirst"/>
  
  <pattern id="test">
    <rule context="xf:editorialEntity/xf:metadata/xf:meta[@code='systemTitle']">
      <report test="contains(string(.),'TESTERREUR')" role="warning">
        [TEST] le titre contient l’expression « TESTERREUR » ?
      </report>
      <assert test="normalize-space(.) != ''" role="error">
        le titre ne doit pas être vide 
      </assert>
    </rule>
    
    <rule context="cxml:infoCommentaire">
      <assert test="cxml:microContenu" role="warning">
        [TEST] infoCommentaire devrait-avoir un microContenu ?
      </assert>
    </rule>
    <rule context="cxml:blocAuteur/cxml:auteur[position() = 3]" role="error">
      <report test="true()">
        [TEST] infoCommentaire ne peut pas avoir plus de 2 auteurs
      </report>
    </rule>
  </pattern>
    
</schema>
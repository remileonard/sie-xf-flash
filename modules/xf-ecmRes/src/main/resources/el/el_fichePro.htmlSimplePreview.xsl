<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns:xfe="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xmlEditor"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/el/el_FICHE_FP"
  exclude-result-prefixes="#all"
  xmlns="http://www.w3.org/1999/xhtml"
  version="2.0">
  
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <!-- IMPORTS -->
  <xsl:import href="xfe:/xfe/xsl/xfe-XML2HTML.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Génère une vue HTML de prévisualisation de l'EE.</xd:p>
      <xd:p>Il s'agit de la vue utilisée dans le panneau de droite de la fiche de l'EE et lors de la prévisualisation d'une EE dans les résultats de recherche ou les arbres.</xd:p>
      <xd:p>Importe la transformation utilisée pour l'éditeur XML (xfe-XML2HTML.xsl) avec fichier de configuration customisé pour remonter quelques informations supplémentaires.</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!-- PARAMS -->
  <!-- Paramètre obligatoire pour xfe:xml2html -->
  <xsl:param name="xfe:conf.uri" select="resolve-uri('_common/el_FICHE_FPRO.htmlPreview.xfe-conf.xml',static-base-uri())" as="xs:string"/>
  <xsl:param name="xfe:addXmlModelPi" select="false()" as="xs:boolean"/>
  <xsl:param name="xfe:addHTML5DOCTYPE" select="false()" as="xs:boolean"/>
  
  <!-- LANCEMENT SANS MODE -->
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfRes:htmlSimplePreview"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
  
  <xsl:template match="/" mode="xfRes:htmlSimplePreview">
    <xsl:message>[INFO] Prévisualisation HTML de l'EE : <xsl:value-of select="base-uri()"/></xsl:message>
    <!-- Application de la transfoHTML pour l'éditeur XML -->
    <xsl:apply-templates select="." mode="xfe:xml2html"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | SURCHARGE DU MODE xfe:xml2html_main              |
      +==================================================+-->

  <!-- Les éléments xf:* sont ignorés : la transformation travaille sur des nœuds el_FICHE_FP -->
  <xsl:template match="xf:*" mode="xfe:xml2html_main"/>
  <xsl:template match="xf:*[@code='EL_TEE_fichePro']" mode="xfe:xml2html_main">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <xsl:template match="xf:*/*[@code='EL_TEE_fichePro']" mode="xfe:xml2html_main">
    <xsl:variable name="el_FICHE_FPAsDocumentNode" as="document-node()">
      <xsl:document>
        <xsl:copy-of select="."/>
      </xsl:document>
    </xsl:variable>
    <xsl:apply-templates select="$el_FICHE_FPAsDocumentNode" mode="#current"/>
  </xsl:template>
  
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
                xmlns:infoCommentaire="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
                xmlns:html="http://www.w3.org/1999/xhtml"
                xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
                exclude-result-prefixes="#all"
                xmlns="http://www.w3.org/1999/xhtml"
                version="2.0">


  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <!-- IMPORTS -->
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>

  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Génère une vue HTML résumée de l'EE.</xd:p>
      <xd:p>Ce résumé sera utilisé lors de l'affichage des EE accrochées dans un arbre.</xd:p>
      <xd:p>Pour les données EFL, le résumé d'insertion contient :</xd:p>
      <xd:ul>
        <xd:li>une copie de la métadonnée systemAbstract, qui elle-même contient :
          <xd:ul>
            <xd:li>le résumé de l'EE,</xd:li>
            <xd:li>la/les refDoc principale(s) verbalisée(s) ;</xd:li>
          </xd:ul>
         </xd:li>
        <xd:li>les renvois Memento de l'EE (blocs rattach).</xd:li>
      </xd:ul>
    </xd:desc>
  </xd:doc>
  
  <!-- OUTPUT -->
  <xsl:output encoding="UTF-8" method="xhtml" omit-xml-declaration="no" indent="no"/>
  
  <!-- LANCEMENT SANS MODE -->
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfRes:referenceNodeAbstract"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
  
  <xsl:template match="/" mode="xfRes:referenceNodeAbstract">
    <xsl:message>[INFO] Calcul du résumé d'insertion de l'EE : <xsl:value-of select="base-uri()"/></xsl:message>
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!-- Pour alerte -->
  <xsl:template match="xf:editorialEntity[not(xf:metadata/xf:meta[@code = 'systemAbstract'])]" mode="xfRes:referenceNodeAbstract" priority="2">
    <xsl:message>[ERROR] La métadonnée "systemAbstract" n'est pas présente dans l'EE.</xsl:message>
    <!-- FALLBACK ? -->
    <xsl:next-match/>
  </xsl:template>

  <!-- Pour alerte -->
  <xsl:template match="xf:editorialEntity[not(normalize-space(xf:metadata/xf:meta[@code = 'systemAbstract']))]" mode="xfRes:referenceNodeAbstract" priority="2">
    <xsl:message>[ERROR] La métadonnée "systemAbstract" est vide.</xsl:message>
    <!-- FALLBACK ? -->
    <xsl:next-match/>
  </xsl:template>
  
  <!-- Construction du document HTML -->
  <xsl:template match="xf:editorialEntity" mode="xfRes:referenceNodeAbstract" priority="1">
    <html>
      <head>
        <title>Résumé insertion : <xsl:value-of select="els:getFileName(base-uri(.))"/></title>
      </head>
      <body>
        <div class="resumeInsertion">
          <xsl:apply-templates mode="#current"/>
        </div>
      </body>
    </html>
  </xsl:template>
  
  
  <!--+==================================================+
      | TRAITEMENT DE xf:meta[@code = 'systemAbstract']  |
      +==================================================+-->
  
  <!-- Copie du contenu de systemAbstract -->
  <xsl:template match="xf:meta[@code = 'systemAbstract']/xf:value[matches(@as,'element\(html:div\)')]" mode="xfRes:referenceNodeAbstract" priority="1">
    <div class="systemAbstract">
      <xsl:copy-of select="*" copy-namespaces="no"/>
    </div>
  </xsl:template>

  <!-- Erreur (?) -->
  <xsl:template match="xf:meta[@code = 'systemAbstract']/xf:value" mode="xfRes:referenceNodeAbstract">
    <xsl:message>[ERROR] La métadonnée "systemAbstract" ne contient pas de noeud HTML.</xsl:message>
    <!-- FALLBACK ? -->
  </xsl:template>
  
  
  
  <!--+==================================================+
      | DEFAULT                                          |
      +==================================================+-->
  
  <xsl:template match="*" mode="xfRes:referenceNodeAbstract"/>
  <xsl:template match="*[descendant-or-self::xf:meta ]" mode="xfRes:referenceNodeAbstract">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>

</xsl:stylesheet>
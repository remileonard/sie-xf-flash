<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:saxon="http://saxon.sf.net/"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns:el_FICHE_FP="http://www.lefebvre-sarrut.eu/ns/el/el_FICHE_FP"
  xmlns:html="http://www.w3.org/1999/xhtml"
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/el/el_FICHE_FP"
  exclude-result-prefixes="#all"
  xmlns="http://www.w3.org/1999/xhtml"
  version="2.0">
  
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <!-- IMPORTS -->
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Génère une vue HTML "sommaire" de l'EE.</xd:p>
      <xd:p>Cette vue sera affichée dans le panneau de gauche de la fiche de l'EE (en alternance avec les métas).</xd:p>
      <xd:p>Elle permet de visualiser l'arborescence du contenu de l'EE.</xd:p>
      <xd:p>Eléments du sommaire :</xd:p>
      <xd:ul>
        <xd:li>titre de l'EE (a minima),</xd:li>
        <xd:li>titres des niveaux (niveau/titre)</xd:li>
      </xd:ul>
    </xd:desc>
  </xd:doc>

  <!-- OUTPUT -->
  <xsl:output encoding="UTF-8" method="xhtml" omit-xml-declaration="no"/>
  
  <!-- LANCEMENT SANS MODE -->
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfRes:htmlToc"/>
  </xsl:template>
  
  
  <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
  
  <xsl:template match="/" mode="xfRes:htmlToc">
    <xsl:message>[INFO] Calcul du sommaire de l'EE : <xsl:value-of select="base-uri()"/></xsl:message>
    <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  <!-- Construction du fichier HTML -->
  <xsl:template match="xf:*[@code='EL_TEE_ficheQR']" mode="xfRes:htmlToc" priority="1">
    <html>
      <head>
        <title>Sommaire : <xsl:value-of select="els:getFileName(base-uri())"/></title>
        <style type="text/css">
          body{font-family:sans-serif;font-size:0.8em;}
          a{text-decoration:none;color:#000000;}
          div.title{font-size:1.2em;margin-bottom:20px;font-style:italic;}
          h1{font-size:1.3em;margin:0.5em 0 0 0;}
          h2{font-size:1.1em;margin:0.3em 0 0 10px;}
          h3{font-size:1.1em;margin:0.2em 0 0 20px;font-weight:normal;}
          h4{font-size:1em;margin:0.1em 0 0 30px;font-weight:normal;}
          h5{font-size:0.9em;margin:0.2em 0 0 35px;font-weight:normal;font-style:italic;}
          h6{font-size:0.9em;margin:0.2em 0 0 40px;font-weight:normal;}
          div.h7, div.h8, div.h9, div.h10, div.h11 {font-size:0.9em;margin:0.2em 0 0 45px;}
          div.h0 {font-size:1em;margin:0.2em 0 0 0;font-style:italic;color:red;}
          div.h12 {font-size:1em;margin:0.2em 0 0 0;font-style:italic;}
        </style>
      </head>
      <body>
        <div id="htmlToc">
          <!-- TODO
          <xsl:apply-templates mode="#current"/>
          -->
        </div>
      </body>
    </html>
  </xsl:template>
  

  
</xsl:stylesheet>
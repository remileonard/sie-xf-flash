<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" 
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire" 
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  exclude-result-prefixes="#all" version="2.0">

  <xsl:include href="xf-ecmRes:/el/_common/el2html.xsl"/>
  <xsl:template match="/">
    <xsl:apply-templates select="." mode="xfRes:EL_TEE_etudes.getCalculatedMetatada"/>
  </xsl:template>
  <xsl:template match="xf:editorialEntity[@code='EL_TEE_etudes']" mode="xfRes:EL_TEE_etudes.getCalculatedMetatada" priority="10">
    <metadata>
      <xsl:call-template name="xfRes:createMetadataCalculatedByCopy">
        <xsl:with-param name="prefix" select="''"/>
        <xsl:with-param name="name" select="'systemTitle'"/>
        <xsl:with-param name="toCopy" select="./*:metadata/*:meta[@code='EL_META_titreFiches']/xf:value/*"/>
      </xsl:call-template>
      <xsl:call-template name="xfRes:createMetadataCalculatedFromElement">
        <xsl:with-param name="prefix" select="''"/>
        <xsl:with-param name="name" select="'systemAbstract'"/>
        <xsl:with-param name="element" select="()"/>
      </xsl:call-template>
    </metadata>
  </xsl:template>
  <xsl:template match="xf:editorialEntity[@code='EL_TEE_pnum'][./*:body/*:contentNode/*:content/*:Etudered/*:Pnumti]" mode="xfRes:EL_TEE_etudes.getCalculatedMetatada" priority="10">
    <metadata>
      <xsl:call-template name="xfRes:createMetadataCalculatedFromElement">
          <xsl:with-param name="prefix" select="''"/>
          <xsl:with-param name="name" select="'systemTitle'"/>
          <xsl:with-param name="element" select="./*:body/*:contentNode/*:content/*:Etudered/*:Pnumti"/>
      </xsl:call-template>
      <xsl:call-template name="xfRes:createMetadataCalculatedFromElement">
        <xsl:with-param name="prefix" select="''"/>
        <xsl:with-param name="name" select="'systemAbstract'"/>
        <xsl:with-param name="element" select="./*:body/*:contentNode/*:content/*:Etudered/*:Al[following-sibling::*:Quest]"/>
      </xsl:call-template>
    </metadata>
  </xsl:template>
  <xsl:template match="xf:editorialEntity" mode="xfRes:EL_TEE_etudes.getCalculatedMetatada">
    <metadata>
      <xsl:copy-of select="ancestor::xf:structuralNode/xf:metadata/xf:meta[@code='systemTitle']"/>
      <xsl:call-template name="xfRes:createMetadataCalculatedFromElement">
        <xsl:with-param name="prefix" select="''"/>
        <xsl:with-param name="name" select="'systemAbstract'"/>
        <xsl:with-param name="element" select="()"/>
      </xsl:call-template>
    </metadata>
  </xsl:template>

  <xsl:template name="xfRes:createMetadataCalculatedFromElement">
    <xsl:param name="prefix"/>
    <xsl:param name="name"/>
    <xsl:param name="element"/>
    
    <xsl:variable name="code" select="if ($prefix != '') then concat($prefix,'.',$name) else $name"/>
    <xf:meta code="{$code}">
      <xf:value>
        <xsl:choose>
          <xsl:when test="count($element) = 0">
            <xsl:message>[INFO][XF-ECM-RES] System meta <xsl:value-of select="$code"/> undefined</xsl:message>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="$element" mode="xfRes:el2html"/>
          </xsl:otherwise>
        </xsl:choose>
      </xf:value>
    </xf:meta>
    
  </xsl:template>
  
  <xsl:template name="xfRes:createMetadataCalculatedByCopy">
    <xsl:param name="prefix"/>
    <xsl:param name="name"/>
    <xsl:param name="toCopy"/>
    
    <xsl:variable name="code" select="if ($prefix != '') then concat($prefix,'.',$name) else $name"/>
    <xf:meta code="{$code}">
      <xf:value>
        <xsl:choose>
          <xsl:when test="count($toCopy) = 0">
            <xsl:message>[INFO][XF-ECM-RES] System meta <xsl:value-of select="$code"/> undefined</xsl:message>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="$toCopy"/>
          </xsl:otherwise>
        </xsl:choose>
      </xf:value>
    </xf:meta>
    
  </xsl:template>
  
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns="http://www.w3.org/1999/xhtml"
  exclude-result-prefixes="#all"
  version="2.0">
  
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Converti les élément EL en html.</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!--==================================================-->
  <!--MODE "xfRes:el2html"-->
  <!--==================================================-->
  
  
  <xsl:template match="*" mode="xfRes:el2html">
    <div class="{local-name(.)}">
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </div>
  </xsl:template>
  
  <xsl:template match="text()" mode="xfRes:el2html">
    <span class="texte-{local-name(..)} texte">
      <xsl:copy-of select="."/>
    </span>
  </xsl:template>
  
  <xsl:template match="@*" mode="xfRes:el2html">
    
  </xsl:template>
  
  
  
</xsl:stylesheet>
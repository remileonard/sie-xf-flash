<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:h="http://www.w3.org/1999/xhtml"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns:juri="http://www.els.fr/ns/juri"
  xmlns="http://www.w3.org/1999/xhtml"
  xpath-default-namespace="http://www.els.fr/ns/juri" 
  exclude-result-prefixes="#all" 
  version="2.0"
  >
  
  <!--indent="no" is important for xspec diff--> 
	<xsl:output method="xhtml" indent="no"/>

  <!--==========================================-->
	<!-- INIT -->
  <!--==========================================-->
  
	<xsl:template match="/">
		<xsl:apply-templates select="." mode="xfRes:htmlSimplePreview"/>		
	</xsl:template>
	
  <!--==========================================-->
  <!-- MAIN -->
  <!--==========================================-->
  
	<xsl:template match="/" mode="xfRes:htmlSimplePreview">
		<xsl:message>[INFO] Prévisualisation HTML de l'EE : <xsl:value-of select="base-uri()"/></xsl:message>
		<xsl:apply-templates mode="#current"/>
	</xsl:template>
  
  <xsl:template match="/xf:editorialEntity" mode="xfRes:htmlSimplePreview">
    <html>
      <head>
        <title>
          <xsl:value-of select="normalize-space(xf:metadata/xf:meta[@code='systemTitle']/xf:value/h:div)"/>
        </title>
      </head>
      <xsl:apply-templates mode="#current"/>
    </html>
  </xsl:template>
  
  <xsl:template match="/xf:editorialEntity/xf:metadata" mode="xfRes:htmlSimplePreview"/>
		
  <xsl:template match="/xf:editorialEntity/xf:body" mode="xfRes:htmlSimplePreview">
		<body>
			<div id="wrapper">
				<div class="text file-container" style="display:block;">
					<div class="doc">
						<xsl:apply-templates mode="#current"/>
					</div>
				</div>
			</div>
		</body>
	</xsl:template>

  <!--==========================================-->
  <!-- juri2html -->
  <!--==========================================-->
  
  <xsl:template match="MetaJuri | MetaDoc | MetaFlux" mode="xfRes:htmlSimplePreview">
    <!-- Méta non affichées -->
  </xsl:template>
  
	<xsl:template match="P" mode="xfRes:htmlSimplePreview">
		<p>
			<xsl:apply-templates mode="#current"/>
		</p>
	</xsl:template>

  <xsl:template match="Personne" mode="xfRes:htmlSimplePreview">
    <span class="nompartie show-text">
		  <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>

  <xsl:template match="Personne/Texte" mode="xfRes:htmlSimplePreview">
    <i class="nompartie-texte">
      <xsl:apply-templates mode="#current"/>
    </i>
  </xsl:template>

  <xsl:template match="Personne/TexteAnonymise" mode="xfRes:htmlSimplePreview">
    <b class="nompartie-texte-anonymise">
		  <xsl:apply-templates mode="#current"/>
    </b>
  </xsl:template>

  <xsl:template match="Adresse" mode="xfRes:htmlSimplePreview">
    <span class="adresse">
		  <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>

</xsl:stylesheet>
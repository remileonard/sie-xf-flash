<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
    xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" 
    xmlns:juri="http://www.els.fr/ns/juri" 
    xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
    xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
    exclude-result-prefixes="#all" 
    version="2.0">

    <xsl:variable name="systemMeta" select="('systemTitle', 'systemAbstract')" as="xs:string+"/>

    <xsl:template match="xf:editorialEntity" mode="xfRes:ELS_TEE_jrpJuri.getCalculatedMetatada">  
        <metadata>
            <xsl:if test="'systemTitle' = $systemMeta">
                <xsl:apply-templates select="xf:body/xf:contentNode/xf:content/juri:Juri" mode="xfRes:getTitle"/>
            </xsl:if>
            <xsl:if test="'systemAbstract' = $systemMeta">
                <xsl:apply-templates select="xf:body/xf:contentNode/xf:content/juri:Juri" mode="xfRes:getAbstract"/>
            </xsl:if>           
        </metadata>
    </xsl:template>

    <xsl:template match="juri:Juri" mode="xfRes:getTitle">
        <meta code="systemTitle" as="element(html:div)">
            <value as="element(html:div)">
                <div xmlns="http://www.w3.org/1999/xhtml">
                    <xsl:choose>
                        <xsl:when test="juri:MetaJuri/juri:GroupeFondTextes/juri:FondTextes/juri:Titre">
                            <xsl:value-of select="juri:MetaJuri/juri:GroupeFondTextes/juri:FondTextes/juri:Titre"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="juri:MetaJuri/juri:DecisionTraitee" mode="#current"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </div>
            </value>
        </meta>
    </xsl:template>

    <xsl:template match="juri:DecisionTraitee" mode="xfRes:getTitle">
        <xsl:value-of select="juri:Juridiction"/>
        <xsl:if test="juri:Lieu">
            <xsl:text>, </xsl:text>
            <xsl:value-of select="juri:Lieu"/>
        </xsl:if>
        <xsl:text>, </xsl:text>
        <xsl:choose>
            <xsl:when test="els:isIsoDate(juri:Date)">
                <xsl:sequence select="els:date-number-slash(juri:Date)"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="juri:Date"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="juri:Juri" mode="xfRes:getAbstract">
        <meta code="systemAbstract" as="element(html:div)">
            <value as="element(html:div)">
                <div xmlns="http://www.w3.org/1999/xhtml">
                    <xsl:apply-templates select="juri:MetaJuri/juri:DecisionTraitee" mode="#current"/>
                    <xsl:apply-templates select="juri:MetaFlux/juri:Source/juri:Origine" mode="#current"/>
                    <xsl:apply-templates select="juri:MetaFlux/juri:Document/juri:Dates/juri:Creation" mode="#current"/>
                </div>
            </value>
        </meta>
    </xsl:template>

    <xsl:template match="juri:DecisionTraitee" mode="xfRes:getAbstract">
        <p xmlns="http://www.w3.org/1999/xhtml">
            <xsl:if test="juri:Nature">
                <xsl:value-of select="juri:Nature"/>
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:if test="juri:NumerosJuri">
                <xsl:value-of select="juri:NumerosJuri/juri:Numero[1]"/>
                <xsl:text>, </xsl:text>
            </xsl:if>
            <xsl:choose>
                <xsl:when test="els:isIsoDate(juri:Date)">
                    <xsl:sequence select="els:date-number-slash(juri:Date)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="juri:Date"/>
                </xsl:otherwise>
            </xsl:choose>
        </p>
    </xsl:template>

    <xsl:template match="juri:Origine" mode="xfRes:getAbstract">
        <p xmlns="http://www.w3.org/1999/xhtml">
            <xsl:value-of select="."/>
        </p>
    </xsl:template>

    <xsl:template match="juri:Creation" mode="xfRes:getAbstract">
        <xsl:variable name="date" select="substring(., 1, 10)" as="xs:string"/>
        <p xmlns="http://www.w3.org/1999/xhtml">
            <xsl:text>Créé le </xsl:text>
            <xsl:choose>
                <xsl:when test="els:isIsoDate($date)">
                    <xsl:sequence select="els:date-number-slash($date)"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </p>
    </xsl:template>
</xsl:stylesheet>

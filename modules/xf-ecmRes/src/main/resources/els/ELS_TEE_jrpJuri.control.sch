<?xml version="1.0" encoding="utf-8"?>
<!--<schema xmlns="http://www.ascc.net/xml/schematron">-->
<schema 
  xmlns="http://purl.oclc.org/dsdl/schematron"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:saxon="http://saxon.sf.net/"
  xmlns:juri="http://www.els.fr/ns/juri"
  queryBinding="xslt2"
  
  id="validation_editorialEntity">
  
  <title>Schematron for ELS Juri TEE</title>
  
  <ns prefix="xsl" uri="http://www.w3.org/1999/XSL/Transform"/>
  <ns prefix="xs" uri="http://www.w3.org/2001/XMLSchema"/>
  <ns prefix="saxon" uri="http://saxon.sf.net/"/>
	<ns prefix="juri" uri="http://www.els.fr/ns/juri"/>
  
  <pattern id="test">
  	<rule context="juri:Juri">
  		<assert test="juri:MetaJuri" role="warning">
  			[TEST] Juri devrait-avoir un MetaJuri ?
      </assert>
    </rule>
  </pattern>
  
</schema>
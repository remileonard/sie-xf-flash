<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
                xmlns:html="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="#all"
                xmlns="http://www.w3.org/1999/xhtml"
                version="2.0">
  
  
  <!-- [JIRA FE-63] Fichiers associés aux TEE
       Pour debug / test.
       Fonctionnement :
         - prend une EE de type arbre non-résolue en entrée,
         - parcourt les différents referenceNode de l'EE,
         - et génère un fichier HTML aggrégeant les fichiers de debug de chaque EE fille (prévisualisations DEBUGView).
       Présuppose :
         - les traitements TEE ont été passés sur chaque EE fille (prévisualisation complète + résumé d'insertion + sommaire),
         - le fichier de debug regroupant ces différentes vues a été créé pour chaque EE fille,
         - tous les fichiers (XML sources, HTML out) sont dans le même répertoire,
         - xf:referenceNode/xf:ref/@xf:idRef = nom du fichir XML de l'EE. -->
  
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <!-- IMPORTS -->
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  
  <!-- PARAMETERS -->
  <xsl:param name="debugView_suffix" select="'_DEBUG_ECMView'" as="xs:string"/>
  
  <!-- VARIABLES -->
  <xsl:variable name="docName" select="els:getFileName(base-uri(),false())" as="xs:string"/>
    
  <!-- KEYS -->
  <xsl:key name="uniqueRef" match="xf:referenceNode/xf:ref" use="@xf:idRef"/>
  
  <!-- OUTPUT -->
  <xsl:output encoding="UTF-8" method="xhtml" omit-xml-declaration="no"/>
  
  
  <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
  
  <!-- Aggrégation des fichiers de debug proposant une synthèse des différentes vues d'une EE.
       Utilisation de div + iframe -> à gauche pour la navigation, à droite pour la preview.
       Parcours des referenceNode + création d'une entrée pour chaque referenceNode unique.
       Pas de vérification que les fichiers de debug ont bien été créés au préalable -> c'est juste pour du test ! -->
  <xsl:template match="/">
    <xsl:message>[INFO] Aggrégation des sorties DEBUG produites pour chaque EE (IN : <xsl:value-of select="base-uri()"/>)</xsl:message>
    <html>
      <head>
        <title>DEBUG VIEW AGGREGATE / <xsl:value-of select="$docName"/></title>
        <style type="text/css">
          body,html{width:100%;height:100%;padding:0;margin:0;}
          #nav{width:20%;float:left;height:100%;border:0;font-family:sans-serif;font-size:0.7em;overflow:auto;}
          #view{width:80%;float:right;height:100%;border:0;}
        </style>
      </head>
      <body>
        <div id="nav">
          <xsl:apply-templates/>
        </div>
        <iframe name="view" id="view"/>
      </body>
    </html>
  </xsl:template>  


  <!--+==================================================+
      | PARSING DES REFERENCE NODE                       |
      +==================================================+-->
  
  <!-- Tri pour referenceNode unique -->
  <xsl:template match="xf:referenceNode/xf:ref[key('uniqueRef',@xf:idRef)[1] is .]">
    <xsl:variable name="docName" select="@xf:idRef"/>
    <p>
      <a href="{concat($docName,$debugView_suffix,'.html')}" target="view">
        <xsl:value-of select="$docName"/>
      </a>
    </p>
  </xsl:template>


  <!--+==================================================+
      | DEFAULT                                          |
      +==================================================+-->
  
  <xsl:template match="*"/>
  <xsl:template match="*[descendant-or-self::xf:referenceNode]">
    <xsl:apply-templates/>
  </xsl:template>

</xsl:stylesheet>
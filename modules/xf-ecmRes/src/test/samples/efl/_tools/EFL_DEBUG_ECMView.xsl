<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
                xmlns:html="http://www.w3.org/1999/xhtml"
                exclude-result-prefixes="#all"
                xmlns="http://www.w3.org/1999/xhtml"
                version="2.0">
  
  
  <!-- [JIRA FE-63] Fichiers associés aux TEE
       Pour debug / test.
       Génère un fichier HTML appelant les différentes vues d'une EE dans des iframes.
       A appliquer en prenant en entrée le fichier XML de l'EE, ayant servi à générer les vues HTML.
       Présuppose que les sorties des différents traitements sont à côté du fichier XML.
       SI $rewriteSommaire, crée une copie du contenu du fichier HTML contenant le sommaire, dans laquelle les liens sont redirigés vers le fichier de prévisualisation complète. -->
  
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <!-- IMPORTS -->
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  
  <!-- PARAMS -->
  <xsl:param name="rewriteSommaire" select="true()" as="xs:boolean"/>
  <xsl:param name="referenceNodeAbstract_suffix" select="'_referenceNodeAbstract'" as="xs:string"/>
  <xsl:param name="htmlToc_suffix" select="'_htmlToc'" as="xs:string"/>
  <xsl:param name="htmlFullPreview_suffix" select="'_htmlFullPreview'" as="xs:string"/>
  <!-- Répertoire de stockage des fichiers créés (par défaut, à côté du XML source) -->
  <xsl:param name="OUT-directory" select="concat(els:getFolderPath(base-uri()),'/')" as="xs:string"/>
  
  <!-- VARIABLES -->
  <xsl:variable name="docName" select="els:getFileName(base-uri(),false())" as="xs:string"/>

  <!-- OUTPUT -->
  <xsl:output encoding="UTF-8" method="xhtml" omit-xml-declaration="no"/>
  
  
  <!--+==================================================+
      | MAIN                                             |
      +==================================================+-->
  
  <!-- Création du fichier de debug -> 3 iframes les unes à côté des autres appelant les différents vues HTML.
       (ou si $rewriteSommaire, 2 iframes + 1 div contenant le sommaire avec ses liens réécrits.)
       La prévisualisation HTML restreinte n'est pas utilisée. -->
  <xsl:template match="/">
    <xsl:message>[INFO] Assemblage des sorties produites pour DEBUG (IN : <xsl:value-of select="base-uri()"/>)</xsl:message>
    <html>
      <head>
        <title>DEBUG VIEW / <xsl:value-of select="$docName"/></title>
        <style type="text/css">
          body,html{width:100%;height:100%;padding:0;margin:0;}
          #resume{width:20%;float:left;height:100%;border:0;}
          #sommaire{width:20%;float:left;height:100%;border:0;}
          #preview{width:60%;float:right;height:100%;border:0;}
        </style>
      </head>
      <body>
        <iframe src="{$docName}{$referenceNodeAbstract_suffix}.html" name="resume" id="resume"/>
        <xsl:choose>
          <!-- Si $rewriteSommaire : copie et réécriture du sommaire dans un div pour faire pointer les liens vers la prévisualisation -->
          <xsl:when test="$rewriteSommaire">
            <xsl:variable name="in" select="resolve-uri(concat($docName,$htmlToc_suffix,'.html'),$OUT-directory)" as="xs:anyURI"/>
            <div id="sommaire" style="margin-top:10px;overflow:auto;">
              <xsl:apply-templates mode="rewriteSommaire" select="document($in)"/>
            </div>
          </xsl:when>
          <xsl:otherwise>
            <!-- Sinon : lien direct dans une iframe vers le fichier HTML de sommaire -->
            <iframe src="{$docName}{$htmlToc_suffix}.html" name="sommaire" id="sommaire"/>    
          </xsl:otherwise>
        </xsl:choose>
        <iframe src="{$docName}{$htmlFullPreview_suffix}.html" name="preview" id="preview"/>
      </body>
    </html>
  </xsl:template>  


  <!--+==================================================+
      | MODE rewriteSommaire                             |
      +==================================================+-->
  
  <!-- Sélection du contenu -->
  <xsl:template match="/" mode="rewriteSommaire">
    <xsl:copy-of select="/html:html/html:head/html:style" copy-namespaces="no"/>
    <xsl:apply-templates select="/html:html/html:body/*" mode="rewriteSommaire"/>
  </xsl:template>
  
  <!-- Récupération de tous les liens <a> + concaténation de l'ID cible avec le nom du fichier HTML de prévisualisation complète.
       L'attribut @target correspond au nom de l'iframe appelant le fichier HTML de prévisualisation, pour ouverture des liens dans celle-ci. --> 
  <xsl:template match="html:a" mode="rewriteSommaire">
     <xsl:copy>
       <xsl:copy-of select="@*"/>
       <xsl:attribute name="href" select="concat($docName,$htmlFullPreview_suffix,'.html',@href)"/>
       <xsl:attribute name="target" select="'preview'"/>
       <xsl:apply-templates mode="rewriteSommaire"/>
     </xsl:copy>
   </xsl:template>
  
  <!-- Default -->
  <xsl:template match="* | @* | processing-instruction() | comment()" mode="rewriteSommaire">
    <xsl:copy copy-namespaces="no">
      <xsl:apply-templates select="node() | @*" mode="rewriteSommaire"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
@ECHO OFF
SETLOCAL ENABLEDELAYEDEXPANSION
TITLE ECMView launcher

SET CWD=%~dp0
SET CWD=%CWD:\=/%

SET XPL=%CWD%EFL_DEBUG_ECMView_Launcher.xpl
SET XML=%~dpnx1
SET XML=file:/%XML:\=/%

REM A adapter
REM Saxon EE requis
SET SAXON_JAR=C:\Users\EXT-acourt\_SOFT\saxon\saxon95ee\saxon9ee.jar
SET CALABASH_JAR=C:\Users\EXT-acourt\_SOFT\xmlcalabash\xmlcalabash-1.1.12-95\xmlcalabash-1.1.12-95.jar
SET CALABASH_LIB=C:\Users\EXT-acourt\_SOFT\xmlcalabash\xmlcalabash-1.1.12-95\lib\

SET _CLASSPATH=
SET _CLASSPATH=%_CLASSPATH%;%CWD%XMLResolver.properties
SET _CLASSPATH=%_CLASSPATH%;%SAXON_JAR%
SET _CLASSPATH=%_CLASSPATH%;%CALABASH_JAR%
FOR %%f IN (%CALABASH_LIB%*.jar) DO SET _CLASSPATH=!_CLASSPATH!;%%f

SET JAVA_OPT=-Xms512m -Xmx1024m -Dfile.encoding=UTF-8 -Dcom.xmlcalabash.phonehome="false" -classpath "%_CLASSPATH%"
SET CALABASH_OPT=-p EE-directory=file:/T:/XML%%20First/acourt/BDD_FLASH/xmlFirst/ee/efl/revues/

SET RUN_CALABASH=java %JAVA_OPT% com.xmlcalabash.drivers.Main %CALABASH_OPT%

REM Launch
%RUN_CALABASH% -i %XML% %XPL%
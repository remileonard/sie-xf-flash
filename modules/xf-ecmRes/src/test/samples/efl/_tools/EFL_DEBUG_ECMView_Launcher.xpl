<?xml version="1.0" encoding="UTF-8"?>
<p:declare-step xmlns:p="http://www.w3.org/ns/xproc"
                xmlns:c="http://www.w3.org/ns/xproc-step"
                xmlns:cx="http://xmlcalabash.com/ns/extensions"
                xmlns:cxf="http://xmlcalabash.com/ns/extensions/fileutils"
                xmlns:err="http://www.w3.org/ns/xproc-error"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:html="http://www.w3.org/1999/xhtml"
                version="1.0"
                name="current">
  
  
  <!-- [JIRA FE-63] Fichiers associés aux TEE
       Pour debug / test.
       Fonctionnement :
         - prend une EE de type arbre non-résolue en entrée,
         - parcourt les différents referenceNode de l'EE,
         - applique toutes les XSL de visualisation du TEE sur chaque EE fille,
         - génère pour chaque EE fille un fichier HTML de debug permettant de visualiser les différentes sorties dans une même page,
         - et génère un fichier HTML aggrégeant les fichiers de debug de chaque EE fille.
       Présuppose :
         - xf:referenceNode/xf:ref/@xf:idRef = nom du fichier XML de l'EE. -->
  
  
  <!--+==================================================+
      | INIT.                                            |
      +==================================================+-->
  
  <!-- INPUT / OUTPUT -->
  <!-- Document d'entrée : EE revue (arbre) -->
  <p:input port="source" primary="true"/>
  <!-- Document de sortie : nada, on force toujours l'écriture en <p:store> -->
  <p:output port="result" primary="true" sequence="true"><p:empty/></p:output>
  <!-- Paramètres -->
  <p:input port="parameters" kind="parameter" primary="true" sequence="true"/>
  
  <!-- IMPORTS -->
  <p:import href="http://xmlcalabash.com/extension/steps/library-1.0.xpl"/>
  
  <!-- PARAMS/VARIABLES -->
  <p:parameters name="parameters2XML">
    <p:input port="parameters"><p:pipe port="parameters" step="current"/></p:input>
  </p:parameters>
  <p:group>
    <!-- Répertoire du fichier XML d'entrée -->
    <p:variable name="IN-directory" select="concat(string-join(tokenize(base-uri(),'/')[position() != last()],'/'),'/')"><p:pipe port="source" step="current"/></p:variable>
    <!-- Nom du fichier XML d'entrée -->
    <p:variable name="IN-name" select="string-join(tokenize(tokenize(base-uri(),'/')[position() = last()],'\.')[position() != last()],'.')"><p:pipe port="source" step="current"/></p:variable>
    <!-- Répertoire de stockage des EE filles (par défaut, on considère qu'elles sont à côté du XML source) -->
    <p:variable name="EE-directory" select="(//c:param[@name = 'EE-directory']/@value,$IN-directory)[1]"><p:pipe port="result" step="parameters2XML"/></p:variable>
    <!-- Répertoire de stockage des fichiers créés (par défaut, à côté du XML source) -->
    <p:variable name="OUT-directory" select="(//c:param[@name = 'OUT-directory']/@value,$IN-directory)[1]"><p:pipe port="result" step="parameters2XML"/></p:variable>
    <!-- Répertoire contenant les programmes / ressources -->
    <p:variable name="SRC-dir" select="(//c:param[@name = 'SRC-dir']/@value,resolve-uri('../../../../main/resources/efl/',static-base-uri()))[1]"><p:pipe port="result" step="parameters2XML"/></p:variable>
    <!-- CSS de prévisualisation (sera copiée à côté des fichiers créés) -->
    <p:variable name="CSS-file" select="(//c:param[@name = 'CSS-file']/@value,resolve-uri('infoCommentaire.css',$SRC-dir))[1]"><p:pipe port="result" step="parameters2XML"/></p:variable>
    <!-- Nom du fichier de CSS -->
    <p:variable name="CSS-name" select="tokenize($CSS-file,'/')[position() = last()]"><p:pipe port="source" step="current"/></p:variable>
    <!-- Préfixes des fichiers de sortie -->
    <p:variable name="referenceNodeAbstract_suffix" select="(//c:param[@name = 'referenceNodeAbstract_suffix']/@value,'_referenceNodeAbstract')[1]"><p:pipe port="result" step="parameters2XML"/></p:variable>
    <p:variable name="htmlToc_suffix" select="(//c:param[@name = 'htmlToc_suffix']/@value,'_htmlToc')[1]"><p:pipe port="result" step="parameters2XML"/></p:variable>
    <p:variable name="htmlFullPreview_suffix" select="(//c:param[@name = 'htmlFullPreview_suffix']/@value,'_htmlFullPreview')[1]"><p:pipe port="result" step="parameters2XML"/></p:variable>
    <p:variable name="htmlSimplePreview_suffix" select="(//c:param[@name = 'htmlSimplePreview_suffix']/@value,'_htmlSimplePreview')[1]"><p:pipe port="result" step="parameters2XML"/></p:variable>
    
    
    <!--+==================================================+
        | MAIN                                             |
        +==================================================+-->
    
    <p:identity><p:input port="source"><p:pipe port="source" step="current"/></p:input></p:identity>
    <cx:message>
      <p:with-option name="message" select="concat('Fichier source : ',base-uri())"/>
    </cx:message>
    
    <!-- 1. Parcours des EE filles
         2. Lancement des XSL de visualisation du TEE sur chaque EE
         3. Création d'un fichier HTML de debug pour visualiser le résultat -->
    <p:group name="parseEE">
      <p:filter select="//xf:referenceNode/xf:ref"/>
      <p:wrap-sequence wrapper="refs"/>
      <p:for-each>
        <!-- Sélection unique de chaque EE -->
        <p:iteration-source select="for $val in (distinct-values(//xf:ref/@xf:idRef)) return (//xf:ref[@xf:idRef = $val][1])"/>
        <p:variable name="fileName" select="/xf:ref/@xf:idRef"/>
        <p:variable name="fileUri" select="resolve-uri(concat($fileName,'.xml'),$EE-directory)"/>
        <cx:message>
          <p:with-option name="message" select="concat('Traitement du fichier (EE fille) : ',$fileUri)"/>
        </cx:message>
        <!-- Traitements TEE -->
        <p:group name="step1_TEE">
          <p:output port="result" primary="true"/>
          <p:load name="loadEE">
            <p:with-option name="href" select="$fileUri"/>
          </p:load>
          <!-- Prévisualisation HTML complète -->
          <p:load name="loadXSL_htmlFullPreview">
            <p:with-option name="href" select="resolve-uri('infoCommentaire.htmlFullPreview.xsl',$SRC-dir)"/>
          </p:load>
          <p:xslt>
            <p:input port="source"><p:pipe port="result" step="loadEE"/></p:input>
            <p:input port="parameters"/>
            <p:input port="stylesheet"><p:pipe port="result" step="loadXSL_htmlFullPreview"/></p:input>
          </p:xslt>
          <p:store omit-xml-declaration="false" method="xhtml" version="1.0">
            <p:with-option name="href" select="resolve-uri(concat($fileName,$htmlFullPreview_suffix,'.html'),$OUT-directory)"/>
          </p:store>
          <!-- Prévisualisation HTML restreinte -->
          <p:load name="loadXSL_htmlSimplePreview">
            <p:with-option name="href" select="resolve-uri('infoCommentaire.htmlSimplePreview.xsl',$SRC-dir)"/>
          </p:load>
          <p:xslt>
            <p:input port="source"><p:pipe port="result" step="loadEE"/></p:input>
            <p:input port="parameters"/>
            <p:input port="stylesheet"><p:pipe port="result" step="loadXSL_htmlSimplePreview"/></p:input>
          </p:xslt>
          <p:store omit-xml-declaration="false" method="xhtml" version="1.0">
            <p:with-option name="href" select="resolve-uri(concat($fileName,$htmlSimplePreview_suffix,'.html'),$OUT-directory)"/>
          </p:store>
          <!-- Extraction du résumé d'insertion -->
          <p:load name="loadXSL_referenceNodeAbstract">
            <p:with-option name="href" select="resolve-uri('infoCommentaire.referenceNodeAbstract.xsl',$SRC-dir)"/>
          </p:load>
          <p:xslt>
            <p:input port="source"><p:pipe port="result" step="loadEE"/></p:input>
            <p:input port="parameters"/>
            <p:input port="stylesheet"><p:pipe port="result" step="loadXSL_referenceNodeAbstract"/></p:input>
          </p:xslt>
          <p:store omit-xml-declaration="false" method="xhtml" version="1.0">
            <p:with-option name="href" select="resolve-uri(concat($fileName,$referenceNodeAbstract_suffix,'.html'),$OUT-directory)"/>
          </p:store>
          <!-- Extraction du sommaire -->
          <p:load name="loadXSL_htmlToc">
            <p:with-option name="href" select="resolve-uri('infoCommentaire.htmlToc.xsl',$SRC-dir)"/>
          </p:load>
          <p:xslt>
            <p:input port="source"><p:pipe port="result" step="loadEE"/></p:input>
            <p:input port="parameters"/>
            <p:input port="stylesheet"><p:pipe port="result" step="loadXSL_htmlToc"/></p:input>
          </p:xslt>
          <p:store omit-xml-declaration="false" method="xhtml" version="1.0">
            <p:with-option name="href" select="resolve-uri(concat($fileName,$htmlToc_suffix,'.html'),$OUT-directory)"/>
          </p:store>
          <p:identity><p:input port="source"><p:pipe port="result" step="loadEE"/></p:input></p:identity>
        </p:group>
        <!-- Fichier de DEBUG (step à exécuter après les traitements précédents -> le fichier de sommaire doit être créé) -->
        <p:group name="step2_DEBUG">
          <p:load name="loadXSL_EFL_DEBUG_ECMView">
            <p:with-option name="href" select="resolve-uri('EFL_DEBUG_ECMView.xsl',static-base-uri())"/>
          </p:load>
          <p:xslt>
            <p:input port="source"><p:pipe port="result" step="step1_TEE"/></p:input>
            <p:input port="parameters"/>
            <p:input port="stylesheet"><p:pipe port="result" step="loadXSL_EFL_DEBUG_ECMView"/></p:input>
            <p:with-param name="OUT-directory" select="$OUT-directory"/>
          </p:xslt>
          <p:store omit-xml-declaration="false" method="xhtml" version="1.0">
            <p:with-option name="href" select="resolve-uri(concat($fileName,'_DEBUG_ECMView.html'),$OUT-directory)"/>
          </p:store>
        </p:group>
      </p:for-each>
    </p:group>
    
    <!-- Génération du fichier de DEBUG aggrégeant ceux des EE filles -->
    <p:group name="debugAggregate">
      <p:load name="loadXSL_EFL_DEBUG_ECMView_Aggregate">
        <p:with-option name="href" select="resolve-uri('EFL_DEBUG_ECMView_Aggregate.xsl',static-base-uri())"/>
      </p:load>
      <p:xslt>
        <p:input port="source"><p:pipe port="source" step="current"/></p:input>
        <p:input port="parameters"/>
        <p:input port="stylesheet"><p:pipe port="result" step="loadXSL_EFL_DEBUG_ECMView_Aggregate"/></p:input>
      </p:xslt>
      <p:store omit-xml-declaration="false" method="xhtml" version="1.0">
        <p:with-option name="href" select="resolve-uri(concat($IN-name,'_DEBUG_ECMView_Aggregate.html'),$OUT-directory)"/>
      </p:store>
    </p:group>
    
    <!-- Copie de la CSS à l'emplacement OUT -->
    <p:group name="copyCSS">
      <cxf:copy>
        <p:with-option name="href" select="$CSS-file"/>
        <p:with-option name="target" select="concat($OUT-directory,$CSS-name)"/>
      </cxf:copy>
    </p:group>
    
  </p:group>
</p:declare-step>
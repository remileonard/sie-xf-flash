SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `ecm` ;
CREATE SCHEMA IF NOT EXISTS `ecm` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin ;
USE `ecm` ;

-- -----------------------------------------------------
-- Table `ecm`.`DATABASECHANGELOG`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`databasechangelog` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`databasechangelog` (
  `ID` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `AUTHOR` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `FILENAME` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `DATEEXECUTED` DATETIME NOT NULL ,
  `ORDEREXECUTED` INT(11) NOT NULL ,
  `EXECTYPE` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `MD5SUM` VARCHAR(35) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DESCRIPTION` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `COMMENTS` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `TAG` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `LIQUIBASE` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `CONTEXTS` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `LABELS` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`DATABASECHANGELOGLOCK`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`databasechangeloglock` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`databasechangeloglock` (
  `ID` INT(11) NOT NULL ,
  `LOCKED` BIT(1) NOT NULL ,
  `LOCKGRANTED` DATETIME NULL DEFAULT NULL ,
  `LOCKEDBY` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`action`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`action` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`action` (
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`admin_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`admin_detail` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`admin_detail` (
  `sid` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `admin_house` BIT(1) NOT NULL ,
  `admin_si` BIT(1) NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`sid`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`assignment_group_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`assignment_group_status` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`assignment_group_status` (
  `ee_id` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `assignment_group_id` BIGINT(20) NULL DEFAULT NULL ,
  `created_date` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`ee_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`configuration_version`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`configuration_version` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`configuration_version` (
  `id` BIGINT(20) NOT NULL ,
  `version` BIGINT(20) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`content_node_editorial_entity_template_meta_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`content_node_editorial_entity_template_meta_data` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`content_node_editorial_entity_template_meta_data` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `information` BIT(1) NULL DEFAULT NULL ,
  `instantiation` BIT(1) NULL DEFAULT NULL ,
  `mandatory` BIT(1) NULL DEFAULT NULL ,
  `multiple_occurrences` BIT(1) NULL DEFAULT NULL ,
  `xpath` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `editoriale_entity_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  `relationship_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`content_type_role_permission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`content_type_role_permission` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`content_type_role_permission` (
  `id` BIGINT(20) NOT NULL ,
  `archive_element` BIT(1) NULL DEFAULT NULL ,
  `create_element` BIT(1) NULL DEFAULT NULL ,
  `edit_content` BIT(1) NULL DEFAULT NULL ,
  `edit_meta` BIT(1) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `content_type_id` BIGINT(20) NULL DEFAULT NULL ,
  `role_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`databasechangelog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`databasechangelog` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`databasechangelog` (
  `ID` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `AUTHOR` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `FILENAME` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `DATEEXECUTED` DATETIME NOT NULL ,
  `ORDEREXECUTED` INT(11) NOT NULL ,
  `EXECTYPE` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `MD5SUM` VARCHAR(35) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `DESCRIPTION` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `COMMENTS` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `TAG` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `LIQUIBASE` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `CONTEXTS` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `LABELS` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`databasechangeloglock`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`databasechangeloglock` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`databasechangeloglock` (
  `ID` INT(11) NOT NULL ,
  `LOCKED` BIT(1) NOT NULL ,
  `LOCKGRANTED` DATETIME NULL DEFAULT NULL ,
  `LOCKEDBY` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`ID`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`editor_configuration`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`editor_configuration` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`editor_configuration` (
  `id` BIGINT(20) NOT NULL ,
  `editor_configuration_type` BIGINT(20) NOT NULL ,
  `path` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`editorial_content_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`editorial_content_type` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`editorial_content_type` (
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`editorial_context`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`editorial_context` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`editorial_context` (
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `entity_status` INT(11) NOT NULL ,
  `last_modified_date` DATETIME NULL DEFAULT NULL ,
  `menu_wording` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `wording` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `house_id` BIGINT(20) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`editorial_context_search_interface_template`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`editorial_context_search_interface_template` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`editorial_context_search_interface_template` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `editorial_context_id` BIGINT(20) NULL DEFAULT NULL ,
  `search_interface_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`editorial_entity_lock`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`editorial_entity_lock` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`editorial_entity_lock` (
  `ee_id` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `created_date` DATETIME NOT NULL ,
  `user_id` BIGINT(20) NOT NULL ,
  `edition_status` BIGINT(2) NULL DEFAULT NULL ,
  PRIMARY KEY (`ee_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`editorial_entity_template`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`editorial_entity_template` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`editorial_entity_template` (
  `content_type` VARCHAR(31) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `entity_status` INT(11) NULL DEFAULT NULL ,
  `instance_name` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `last_modified_date` DATETIME NULL DEFAULT NULL ,
  `short_wording` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `wording` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `edition_schema` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `insertion_summarize` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `previewhtml` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `previewxml` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `proofinghtml` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `calculated_meta_data` VARCHAR(255) CHARACTER SET 'utf8' NULL DEFAULT NULL ,
  `schematron_edition` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `schematron_structure` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `storage_schema` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `summary` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `translation_sheet` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `authorized_node_deactivation` BIT(1) NOT NULL DEFAULT b'0' ,
  `section_type_setting` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `structural_type_setting` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `editorial_content_type_id` BIGINT(20) NULL DEFAULT NULL ,
  `house_id` BIGINT(20) NULL DEFAULT NULL ,
  `creation_workflow_id` BIGINT(20) NULL DEFAULT NULL ,
  `modification_workflow_id` BIGINT(20) NULL DEFAULT NULL ,
  `code` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `used_flash_editor` BIT(1) NOT NULL DEFAULT b'0' ,
  `css_sheet` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `editor_configuration_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`editorial_entity_template_actions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`editorial_entity_template_actions` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`editorial_entity_template_actions` (
  `editorial_entity_template_id` BIGINT(20) NOT NULL ,
  `actions_id` BIGINT(20) NOT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`editorial_entity_template_meta_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`editorial_entity_template_meta_data` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`editorial_entity_template_meta_data` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `information` BIT(1) NULL DEFAULT NULL ,
  `instantiation` BIT(1) NULL DEFAULT NULL ,
  `mandatory` BIT(1) NULL DEFAULT NULL ,
  `multiple_occurrences` BIT(1) NULL DEFAULT NULL ,
  `xpath` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `editoriale_entity_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  `relationship_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`editorial_entity_template_publication_channels`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`editorial_entity_template_publication_channels` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`editorial_entity_template_publication_channels` (
  `editorial_entity_template_id` BIGINT(20) NOT NULL ,
  `publication_channels_id` BIGINT(20) NOT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`export_engine`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`export_engine` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`export_engine` (
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`filter_instance`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`filter_instance` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`filter_instance` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `date_operator` INT(11) NULL DEFAULT NULL ,
  `filter_value` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `meta_data_operator` INT(11) NULL DEFAULT NULL ,
  `type` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `direct_filter_id` BIGINT(20) NULL DEFAULT NULL ,
  `extended_filter_id` BIGINT(20) NULL DEFAULT NULL ,
  `search_full_text_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 25823
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`filter_values`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`filter_values` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`filter_values` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `wording` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `filter_instance_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 217
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`flat_view_setting`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`flat_view_setting` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`flat_view_setting` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `editoriale_entity_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`hanging_node_editorial_entity_template_meta_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`hanging_node_editorial_entity_template_meta_data` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`hanging_node_editorial_entity_template_meta_data` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `information` BIT(1) NULL DEFAULT NULL ,
  `instantiation` BIT(1) NULL DEFAULT NULL ,
  `mandatory` BIT(1) NULL DEFAULT NULL ,
  `multiple_occurrences` BIT(1) NULL DEFAULT NULL ,
  `xpath` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `editoriale_entity_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  `relationship_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`house`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`house` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`house` (
  `id` BIGINT(20) NOT NULL ,
  `detail` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`list_meta_data_values`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`list_meta_data_values` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`list_meta_data_values` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `wording` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`meta_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`meta_data` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`meta_data` (
  `metadata_type` VARCHAR(31) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `id` BIGINT(20) NOT NULL ,
  `code` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `description` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `entity_status` INT(11) NOT NULL ,
  `last_modified_date` DATETIME NULL DEFAULT NULL ,
  `meta_system` BIT(1) NULL DEFAULT NULL ,
  `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `comma_allowed` BIT(1) NULL DEFAULT b'0' ,
  `negative_allowed` BIT(1) NULL DEFAULT b'0' ,
  `text_length` INT(11) NULL DEFAULT NULL ,
  `false_wording` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `right_wording` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `selection_format` INT(11) NULL DEFAULT NULL ,
  `xpath` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `extension_allowed` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_group_id` BIGINT(20) NOT NULL ,
  `refid` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`meta_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`meta_group` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`meta_group` (
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`meta_group_role_permission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`meta_group_role_permission` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`meta_group_role_permission` (
  `id` BIGINT(20) NOT NULL ,
  `authorized` BIT(1) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_group_id` BIGINT(20) NULL DEFAULT NULL ,
  `role_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`notification_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`notification_group` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`notification_group` (
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`notification_role_permission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`notification_role_permission` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`notification_role_permission` (
  `id` BIGINT(20) NOT NULL ,
  `authorized` BIT(1) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `notification_group_id` BIGINT(20) NULL DEFAULT NULL ,
  `role_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`notifications`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`notifications` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`notifications` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `category` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `notification_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
  `object_notification` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `reading` BIT(1) NULL DEFAULT NULL ,
  `recipient` BIGINT(20) NULL DEFAULT NULL ,
  `sender` BIGINT(20) NULL DEFAULT NULL ,
  `type_notification` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `wording` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `notification_group_id` BIGINT(20) NULL DEFAULT NULL ,
  `ee_id` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `message` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`proxyref_referential`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`proxyref_referential` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`proxyref_referential` (
  `id` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `labelen` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `labelfr` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `type` INT(11) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `ref_name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`proxyref_referentialvalue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`proxyref_referentialvalue` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`proxyref_referentialvalue` (
  `id` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `archived` BIT(1) NULL DEFAULT NULL ,
  `labelen` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `labelfr` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `left_value` INT(11) NULL DEFAULT NULL ,
  `right_value` INT(11) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `ref_id` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`proxyref_referentialvaluemetadata`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`proxyref_referentialvaluemetadata` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`proxyref_referentialvaluemetadata` (
  `id` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `ref_value_id` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `meta_key` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `meta_value` VARCHAR(300) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `java_type` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `java_pattern` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `ref_value_id_FK` (`ref_value_id` ASC) ,
  CONSTRAINT `ref_value_id_FK`
    FOREIGN KEY (`ref_value_id` )
    REFERENCES `ecm`.`proxyref_referentialvalue` (`id` ))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`publication_channel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`publication_channel` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`publication_channel` (
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `proof_mode` BIT(1) NULL DEFAULT b'0' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`publish_and_proofing_historical`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`publish_and_proofing_historical` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`publish_and_proofing_historical` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `channel_id` BIGINT(20) NULL DEFAULT NULL ,
  `creation_date` DATETIME NULL DEFAULT NULL ,
  `entity_id` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `result_proofing` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `publish_status` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `publish_type` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `user_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`relationship_template`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`relationship_template` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`relationship_template` (
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `entity_status` INT(11) NOT NULL ,
  `last_modified_date` DATETIME NULL DEFAULT NULL ,
  `sub_elements_entity_target` BIT(1) NOT NULL ,
  `wording` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `search_interface_template_target_id` BIGINT(20) NULL DEFAULT NULL ,
  `relationship_type_id` BIGINT(20) NULL DEFAULT NULL ,
  `code` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `tag` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `auto_update_with_notifications` BIT(1) NULL DEFAULT b'0' ,
  `auto_update_without_notifications` BIT(1) NULL DEFAULT b'0' ,
  `manual_update_with_notification` BIT(1) NULL DEFAULT b'0' ,
  `manual_update_with_notification_entire_product` BIT(1) NULL DEFAULT b'0' ,
  `no_updates_required` BIT(1) NULL DEFAULT b'0' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`relationship_template_type`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`relationship_template_type` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`relationship_template_type` (
  `id` BIGINT(20) NOT NULL ,
  `wording` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`relationships_template_editorial_entity_template_source`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`relationships_template_editorial_entity_template_source` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`relationships_template_editorial_entity_template_source` (
  `id` BIGINT(20) NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `editorial_entity_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `relationship_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`relationships_template_editorial_entity_template_target`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`relationships_template_editorial_entity_template_target` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`relationships_template_editorial_entity_template_target` (
  `id` BIGINT(20) NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `editorial_entity_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `relationship_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`relationships_template_meta_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`relationships_template_meta_data` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`relationships_template_meta_data` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  `relationship_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `mandatory` BIT(1) NULL DEFAULT b'0' ,
  `multiple_occurrences` BIT(1) NULL DEFAULT b'0' ,
  `copy_from_target` BIT(1) NULL DEFAULT b'0' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`request_node_editorial_entity_template_meta_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`request_node_editorial_entity_template_meta_data` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`request_node_editorial_entity_template_meta_data` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `information` BIT(1) NULL DEFAULT NULL ,
  `instantiation` BIT(1) NULL DEFAULT NULL ,
  `mandatory` BIT(1) NULL DEFAULT NULL ,
  `multiple_occurrences` BIT(1) NULL DEFAULT NULL ,
  `xpath` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `editoriale_entity_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  `relationship_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`role` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`role` (
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `entity_status` INT(11) NULL DEFAULT NULL ,
  `final_state_limited` BIT(1) NULL DEFAULT NULL ,
  `last_modified_date` DATETIME NULL DEFAULT NULL ,
  `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `house_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`saved_filter_values`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`saved_filter_values` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`saved_filter_values` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `wording` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `saved_filter_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`saved_filters`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`saved_filters` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`saved_filters` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `date_operator` INT(11) NULL DEFAULT NULL ,
  `filter_value` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `meta_data_operator` INT(11) NULL DEFAULT NULL ,
  `type` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `direct_filter_id` BIGINT(20) NULL DEFAULT NULL ,
  `extended_filter_id` BIGINT(20) NULL DEFAULT NULL ,
  `saved_search_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 33
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`saved_search`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`saved_search` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`saved_search` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `share_with_groups` BIT(1) NOT NULL ,
  `add_to_menu` BIT(1) NOT NULL ,
  `add_to_notification` BIT(1) NOT NULL ,
  `libelle_saved_search` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `search_interface_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `user_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 16
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`saved_search_full_text`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`saved_search_full_text` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`saved_search_full_text` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `content_editorial_entity` BIT(1) NULL DEFAULT NULL ,
  `all_editorial_entity` BIT(1) NULL DEFAULT NULL ,
  `metadata_editorial_entity` BIT(1) NULL DEFAULT NULL ,
  `search_full_text` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `summary_editorial_entity` BIT(1) NULL DEFAULT NULL ,
  `title_editorial_entity` BIT(1) NULL DEFAULT NULL ,
  `saved_search_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 13
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`schema_version`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`schema_version` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`schema_version` (
  `version_rank` INT(11) NOT NULL ,
  `installed_rank` INT(11) NOT NULL ,
  `version` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `description` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `type` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `script` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `checksum` INT(11) NULL DEFAULT NULL ,
  `installed_by` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `installed_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `execution_time` INT(11) NOT NULL ,
  `success` TINYINT(1) NOT NULL ,
  PRIMARY KEY (`version`) ,
  INDEX `schema_version_vr_idx` (`version_rank` ASC) ,
  INDEX `schema_version_ir_idx` (`installed_rank` ASC) ,
  INDEX `schema_version_s_idx` (`success` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`search_full_text`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`search_full_text` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`search_full_text` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `content_editorial_entity` BIT(1) NULL DEFAULT NULL ,
  `all_editorial_entity` BIT(1) NULL DEFAULT NULL ,
  `metadata_editorial_entity` BIT(1) NULL DEFAULT NULL ,
  `search_full_text` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `summary_editorial_entity` BIT(1) NULL DEFAULT NULL ,
  `title_editorial_entity` BIT(1) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `search_interface_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `user_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4601
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`search_interface_template`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`search_interface_template` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`search_interface_template` (
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `display_classification_search_summary` BIT(1) NOT NULL ,
  `display_result_search_summary` BIT(1) NOT NULL ,
  `entity_status` INT(11) NULL DEFAULT NULL ,
  `last_modified_date` DATETIME NULL DEFAULT NULL ,
  `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `open_classification_summary` BIT(1) NOT NULL ,
  `open_result_summary` BIT(1) NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `house_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`search_interface_template_classification`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`search_interface_template_classification` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`search_interface_template_classification` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `wording` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  `search_interface_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`search_interface_template_direct_filter`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`search_interface_template_direct_filter` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`search_interface_template_direct_filter` (
  `id` BIGINT(20) NOT NULL ,
  `assign_default_value` BIT(1) NULL DEFAULT NULL ,
  `default_value` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `facet` BIT(1) NULL DEFAULT NULL ,
  `free_text` BIT(1) NULL DEFAULT NULL ,
  `multi_valued` BIT(1) NULL DEFAULT NULL ,
  `visibility` BIT(1) NULL DEFAULT NULL ,
  `wording` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  `search_interface_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`search_interface_template_editorial_entity_template`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`search_interface_template_editorial_entity_template` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`search_interface_template_editorial_entity_template` (
  `id` BIGINT(20) NOT NULL ,
  `editorial_entity_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `search_interface_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`search_interface_template_extended_filter`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`search_interface_template_extended_filter` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`search_interface_template_extended_filter` (
  `id` BIGINT(20) NOT NULL ,
  `assign_default_value` BIT(1) NULL DEFAULT NULL ,
  `default_value` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `facet` BIT(1) NULL DEFAULT NULL ,
  `free_text` BIT(1) NULL DEFAULT NULL ,
  `multi_valued` BIT(1) NULL DEFAULT NULL ,
  `visibility` BIT(1) NULL DEFAULT NULL ,
  `wording` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `filter_type` INT(11) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  `search_interface_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `relationship_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`search_interface_template_result`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`search_interface_template_result` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`search_interface_template_result` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `wording` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  `search_interface_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`step`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`step` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`step` (
  `id` BIGINT(20) NOT NULL ,
  `comment` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `deadline_installation_allowed` BIT(1) NULL DEFAULT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `final_status` BIT(1) NULL DEFAULT NULL ,
  `initial_status` BIT(1) NULL DEFAULT NULL ,
  `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `user_assignment_allowed` BIT(1) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `action_id` BIGINT(20) NULL DEFAULT NULL ,
  `workflow_id` BIGINT(20) NULL DEFAULT NULL ,
  `workflow_status_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`step_reserved_user_group_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`step_reserved_user_group_list` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`step_reserved_user_group_list` (
  `step_id` BIGINT(20) NOT NULL ,
  `reserved_user_group_list_id` BIGINT(20) NOT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`structural_node_editorial_entity_template_meta_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`structural_node_editorial_entity_template_meta_data` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`structural_node_editorial_entity_template_meta_data` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `information` BIT(1) NULL DEFAULT NULL ,
  `instantiation` BIT(1) NULL DEFAULT NULL ,
  `mandatory` BIT(1) NULL DEFAULT NULL ,
  `multiple_occurrences` BIT(1) NULL DEFAULT NULL ,
  `xpath` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `editoriale_entity_template_id` BIGINT(20) NULL DEFAULT NULL ,
  `meta_data_id` BIGINT(20) NULL DEFAULT NULL ,
  `relationship_template_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`user` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`user` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `activated` BIT(1) NOT NULL ,
  `created_by` BIGINT(20) NOT NULL ,
  `created_date` DATETIME NOT NULL ,
  `email` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `first_name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `lang_key` VARCHAR(5) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `last_modified_by` BIGINT(20) NULL DEFAULT NULL ,
  `last_modified_date` DATETIME NULL DEFAULT NULL ,
  `last_name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `login` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `sid` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `house_id` BIGINT(20) NULL DEFAULT NULL ,
  `role_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `UK_user_login` (`login` ASC) ,
  UNIQUE INDEX `UK_user_sid` (`sid` ASC) ,
  UNIQUE INDEX `UK_user_email` (`email` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 141
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`user_group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`user_group` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`user_group` (
  `id` BIGINT(20) NOT NULL ,
  `description` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `entity_status` INT(11) NULL DEFAULT NULL ,
  `last_modified_date` DATETIME NULL DEFAULT NULL ,
  `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `house_id` BIGINT(20) NULL DEFAULT NULL ,
  `code` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`user_group_editorial_context`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`user_group_editorial_context` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`user_group_editorial_context` (
  `id` BIGINT(20) NOT NULL ,
  `display_order` INT(11) NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `editorial_context_id` BIGINT(20) NULL DEFAULT NULL ,
  `user_group_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`user_group_editorial_entity_templates`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`user_group_editorial_entity_templates` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`user_group_editorial_entity_templates` (
  `user_group_id` BIGINT(20) NOT NULL ,
  `editorial_entity_templates_id` BIGINT(20) NOT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`user_user_group_list`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`user_user_group_list` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`user_user_group_list` (
  `user_id` BIGINT(20) NOT NULL ,
  `user_group_list_id` BIGINT(20) NOT NULL ,
  INDEX `FK_user_group_list_id` (`user_group_list_id` ASC) ,
  INDEX `FK_user_id` (`user_id` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`workflow`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`workflow` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`workflow` (
  `id` BIGINT(20) NOT NULL ,
  `comment_allowed` BIT(1) NULL DEFAULT NULL ,
  `description` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `entity_status` INT(11) NULL DEFAULT NULL ,
  `last_modified_date` DATETIME NULL DEFAULT NULL ,
  `name` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  `house_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`workflow_history`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`workflow_history` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`workflow_history` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `ee_id` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `workflow_id` BIGINT(20) NOT NULL ,
  `step_id` BIGINT(20) NOT NULL ,
  `assignment_group_id` BIGINT(20) NULL DEFAULT NULL ,
  `user_assignment_id` BIGINT(20) NULL DEFAULT NULL ,
  `comment_wfw` VARCHAR(1000) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `assignment_date` DATETIME NULL DEFAULT NULL ,
  `dead_line` DATE NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 420
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`workflow_metadatas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`workflow_metadatas` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`workflow_metadatas` (
  `workflow_id` BIGINT(20) NOT NULL ,
  `metadatas_id` BIGINT(20) NOT NULL )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;


-- -----------------------------------------------------
-- Table `ecm`.`workflow_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ecm`.`workflow_status` ;

CREATE  TABLE IF NOT EXISTS `ecm`.`workflow_status` (
  `id` BIGINT(20) NOT NULL ,
  `name` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL ,
  `status` VARCHAR(255) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NULL DEFAULT NULL ,
  `version_id` BIGINT(20) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_bin;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

<?xml version="1.0" encoding="UTF-8"?>
<ui xmlns="http://modeles.efl.fr/modeles/reference" createDt="2016-04-14T06:35:39.999+02:00" createUser="epinsard" id="UI-2c761d74-be25-4149-ba30-8cd7dcbcbb5b" lastUpdateDt="2016-06-01T11:15:22.999+02:00" lastUpdateUser="fsissi" status="BAP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://modeles.efl.fr/modeles/reference uiCommentaire-bdd.xsd">
  <metadonnees>
    <metaCommunes>
      <metaType>
        <typeObjet>ui</typeObjet>
        <typeContenu>redige</typeContenu>
        <categorieUIRedigee idref="commentaireJurisprudenceNonSigné">Commentaire de jurisprudence non signé</categorieUIRedigee>
        <produitCx idref="rjf">RJF</produitCx>
      </metaType>
      <metaComportement>
        <publicationAuto>true</publicationAuto>
        <dateValidite>2016-04-15</dateValidite>
      </metaComportement>
      <metaOrigine>
        <origine idref="EFL">EFL</origine>
      </metaOrigine>
      <metaQualif>
        <themes>
          <theme idref="fis08">Fiscal / Contentieux</theme>
        </themes>
        
      <titreCalcule>Le recours en annulation contre les actes de « droit souple » est recevable</titreCalcule></metaQualif>
    </metaCommunes>
    <metaSpecifiques>
      <metaQualifCible>
        <matieres>
          <matiere idref="Fiscal">Fis</matiere>
        </matieres>
        <metiers>
          <metier idref="AVOCAT">Avocat</metier>
        </metiers>
        <niveauExpertise>expert</niveauExpertise>
      </metaQualifCible>
      <metaSource><refDoc formatRedige="true" ref="TY:jp|JUR:CE|DT:20160321|N:368082" type="premiere">CE 21 mars 2016 n° 368082, 368083, 368084 ass., Sté Fairvesta international GMBH</refDoc></metaSource>
      <version>developpee</version>
      <modifierDate>true</modifierDate>
      <alerter>true</alerter>
      <actu>
        <statut>propose</statut>
      </actu>
    </metaSpecifiques>
  </metadonnees>
  <infoCommentaire>
    <titre>Le recours en annulation contre les actes de « droit souple » est recevable</titre>
    <titreObjet>Droit souple</titreObjet>
    <titreSommaire>Voies de recours contre les actes de droit souple</titreSommaire>
    <blocRefDoc>
      <refDoc formatRedige="true" ref="TY:jp|JUR:CE|DT:20160321|N:368082" type="premiere">CE 21 mars 2016 n° 368082, 368083, 368084 ass., Sté Fairvesta international GMBH</refDoc>
    </blocRefDoc>
    <microContenu>
      <resume id="UI-f9b84fca-6e17-4f49-b5c0-5c27d487b90f">
        <al>Le Conseil d’Etat ouvre les recours pour excès de pouvoir contre les actes de « droit souple », tels que les communiqués ou les prises de position des autorités de régulation, sous certaines conditions.</al>
      </resume>
    </microContenu>
    <analyseObservation id="UI-ccaa0187-7d4b-467c-9123-a78f0721c5db">
      <blocAbstracts id="UI-414c02f0-72ea-4d24-90fb-28a3b3058068">
        <abstract id="UI-5a65f353-2cee-4d40-a0bd-4eab107368af">
          <descripteur>actes de droit souple des autorités de régulation</descripteur>
          <descripteur>actes susceptibles de faire l’objet d’un recours pour excès de pouvoir</descripteur>
          <descripteur>conditions</descripteur>
          <descripteur>communiqués émis par l’AMF</descripteur>
        </abstract>
      </blocAbstracts>
      <blocAnalyses id="UI-f3d9c05c-61db-464e-b2b3-d355e7c42f49">
        <analyse id="UI-e94766c7-4f1c-4bfd-a410-3b4c06a48c02" num="I. 1°">
          <miseEnValeur type="mere">
            <al>Les avis, recommandations, mises en garde et prises de position adoptés par les autorités de régulation dans l’exercice des missions dont elles sont investies peuvent être déférés au juge de l’excès de pouvoir lorsqu’ils revêtent le caractère de dispositions générales et impératives ou lorsqu’ils énoncent des prescriptions individuelles dont ces autorités pourraient ultérieurement censurer la méconnaissance.</al>
          </miseEnValeur>
        </analyse>
        <analyse id="UI-a77d7708-8dc9-43a5-8b91-0b20de2d7d9b" num="2°">
          <miseEnValeur type="mere">
            <al>Ces actes peuvent également faire l’objet d’un recours pour excès de pouvoir, introduit par un requérant justifiant d’un intérêt direct et certain à leur annulation, lorsqu’ils sont de nature à produire des effets notables, notamment de nature économique, ou ont pour objet d’influer de manière significative sur les comportements des personnes auxquelles ils s’adressent.</al>
          </miseEnValeur>
          <miseEnValeur type="mere">
            <al>Dans ce cas, il appartient au juge, saisi de moyens en ce sens, d’examiner les vices susceptibles d’affecter la légalité de ces actes en tenant compte de leur nature et de leurs caractéristiques, ainsi que du pouvoir d’appréciation dont dispose l’autorité de régulation. Il lui appartient également, si des conclusions lui sont présentées à cette fin, de faire usage des pouvoirs d’injonction qu’il tient du titre I<e>er</e> du livre IX du Code de justice administrative.</al>
          </miseEnValeur>
        </analyse>
        <analyse id="UI-55179f75-a271-43e5-a230-0800c5c6f708" num="II. 1°">
          <al>En vertu de l’article L 621-1 du Code monétaire et financier, il appartient à l’Autorité des marchés financiers (AMF) de publier des communiqués invitant les épargnants ou investisseurs à faire preuve de vigilance vis-à-vis de certains types de placements ou de pratiques financières risqués. Le législateur a ainsi entendu confier à l’AMF une mission de protection de l’épargne et d’information des investisseurs qui s’étend non seulement aux instruments financiers, définis par l’article L 211-1 du Code monétaire et financier, et aux actifs mentionnés au II de l’article L 421-1 du même code admis aux négociations sur un marché réglementé, mais également à tous les autres placements offerts au public. Par suite, il est loisible à l’AMF d’appeler l’attention des investisseurs sur les caractéristiques et les modalités de commercialisation de placements immobiliers, alors même qu’ils ne relèvent pas de la réglementation applicable aux titres financiers, dès lors qu’il s’agit de placements offerts au public.</al>
        </analyse>
        <analyse id="UI-2baf636f-285c-4916-81a5-fb14fb6320d7" num="2°">
          <al>Les communiqués attaqués, émis par l’Autorité des marchés financiers (AMF) dans le cadre de sa mission de protection de l’épargne investie dans les placements offerts au public, sont destinés aux investisseurs et ont pour objet de les mettre en garde contre les conditions dans lesquelles sont commercialisés plusieurs produits de placement, précisément identifiés, offerts au public par la société requérante et de leur adresser des recommandations de vigilance. Ils ont été publiés sur le site internet de l’AMF, ont connu une large diffusion et sont depuis lors restés accessibles sur ce site.</al>
          <al>Dans les circonstances de l’espèce, ces communiqués, qui font référence à une société en usant du nom du groupe auquel elle appartient, doivent être regardés comme faisant grief à la société commercialisant les produits et aux autres sociétés, filiales du même groupe, qui portent le nom de ce groupe.</al>
          <al>La société requérante fait valoir des éléments sérieux attestant que la publication de ces communiqués a eu pour conséquence une diminution brutale des souscriptions des produits de placement qu’elle commercialisait en France. Ainsi, les communiqués contestés doivent être regardés comme étant de nature à produire des effets économiques notables et comme ayant pour objet de conduire des investisseurs à modifier de manière significative leur comportement vis-à-vis des produits qu’ils désignent. Ils sont donc susceptibles de recours, de même que le refus opposé à la demande de la société tendant à leur rectification.</al>
        </analyse>
        <analyse id="UI-193935a7-b9db-4900-ac98-c4d6e225d206" num="3°">
          <al>Le juge de l’excès de pouvoir exerce un contrôle restreint sur la publication, par l’Autorité des marchés financiers (AMF), d’un communiqué mettant les investisseurs en garde contre les conditions dans lesquelles sont commercialisés certains produits de placement.</al>
        </analyse>
      </blocAnalyses>
      <blocObservations id="UI-d7e7fd5d-3448-42dc-a113-20c1a08f5a5b">
        <observation id="UI-015b18d2-dfdb-472b-9bea-ba9e60c14aaa">
          <al>Les <motRep>conclusions</motRep> du rapporteur public <motRep>Suzanne von Coester</motRep> sont reproduites sur abonnes.efl.fr (<renvoiAutrui ref="UI-ee4be8f2-4774-4bdd-8223-29fcb1bd6b7e" uiId="UI-ee4be8f2-4774-4bdd-8223-29fcb1bd6b7e"/>).</al>
          <al>On se reportera à nos observations sous CE 21 mars 2016 n° 390023 ass., Sté NC Numéricable, ci-dessus n° <renvoiAutrui ref="UI-7b268723-1044-42d4-88d5-ee1dc6723440" uiId="UI-7b268723-1044-42d4-88d5-ee1dc6723440"/>.</al>
        </observation>
      </blocObservations>
      <sourceReproduite extrait="true" id="UI-67863592-541c-4145-83ee-f198de6c501a">
        <blocRefDoc>
          <refDocEnrichie>
            <refDoc formatRedige="true" ref="TY:jp|JUR:CE|DT:20160321|N:368082" type="reproduite">CE 21 mars 2016 n° 368082, 368083 et 368084 ass., Sté Fairvesta international GmbH</refDoc>
            <CompositionCour>MM. Sauvé, Prés. - M<e>mes</e> Olsina, Rapp. - von Coester, R. public - SCP Foussard, Froger et SCP Vincent, Ohl, Av.</CompositionCour>
          </refDocEnrichie>
        </blocRefDoc>
      </sourceReproduite>
    </analyseObservation>
  </infoCommentaire>
</ui>
<?xml version="1.0" encoding="UTF-8"?>
<ui xmlns="http://modeles.efl.fr/modeles/reference" createDt="2016-04-12T10:11:26.999+02:00" createUser="epinsard" id="UI-fb9c8825-651e-4ea7-9f1d-be2070a179d5" lastUpdateDt="2016-06-01T11:00:01.999+02:00" lastUpdateUser="fsissi" status="BAP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://modeles.efl.fr/modeles/reference uiCommentaire-bdd.xsd">
  <metadonnees>
    <metaCommunes>
      <metaType>
        <typeObjet>ui</typeObjet>
        <typeContenu>redige</typeContenu>
        <categorieUIRedigee idref="commentaireJurisprudenceNonSigné">Commentaire de jurisprudence non signé</categorieUIRedigee>
        <produitCx idref="rjf">RJF</produitCx>
      </metaType>
      <metaComportement>
        <publicationAuto>true</publicationAuto>
        <dateValidite>2016-04-13</dateValidite>
      </metaComportement>
      <metaOrigine>
        <origine idref="EFL">EFL</origine>
      </metaOrigine>
      <metaQualif>
        <themes>
          <theme idref="BIC">Fiscal / BIC</theme>
          <theme idref="fis08">Fiscal / Contentieux</theme>
        </themes>
        
      <titreCalcule>L’inscription dans un compte de provisions d’une dette certaine est sans incidence fiscale</titreCalcule></metaQualif>
    </metaCommunes>
    <metaSpecifiques>
      <metaQualifCible>
        <matieres>
          <matiere idref="Fiscal">Fis</matiere>
        </matieres>
        <metiers>
          <metier idref="AVOCAT">Avocat</metier>
        </metiers>
        <niveauExpertise>expert</niveauExpertise>
      </metaQualifCible>
      <metaSource><refDoc formatRedige="true" ref="TY:jp|JUR:CE|DT:20160317|N:381427" type="premiere">CE 17 mars 2016 n° 381427, 3<e>e</e> et 8<e>e</e> s.-s., Sté Sogetra</refDoc></metaSource>
      <version>developpee</version>
      <modifierDate>true</modifierDate>
      <alerter>true</alerter>
      <actu>
        <statut>propose</statut>
      </actu>
    </metaSpecifiques>
  </metadonnees>
  <infoCommentaire>
    <titre>L’inscription dans un compte de provisions d’une dette certaine est sans incidence fiscale</titre>
    <titreObjet>Provisions</titreObjet>
    <titreSommaire>Constitution à tort d’une provision</titreSommaire>
    <blocRefDoc>
      <refDoc formatRedige="true" ref="TY:jp|JUR:CE|DT:20160317|N:381427" type="premiere">CE 17 mars 2016 n° 381427, 3<e>e</e> et 8<e>e</e> s.-s., Sté Sogetra</refDoc>
    </blocRefDoc>
    <microContenu>
      <resume id="UI-f9b84fca-6e17-4f49-b5c0-5c27d487b90f">
        <al>La requalification en dette certaine figurant au passif du bilan de l’exercice d'une somme inscrite à tort en provision ne peut donner lieu à réintégration au résultat imposable.</al>
      </resume>
    </microContenu>
    <analyseObservation id="UI-ccaa0187-7d4b-467c-9123-a78f0721c5db">
      <blocAbstracts id="UI-414c02f0-72ea-4d24-90fb-28a3b3058068">
        <abstract id="UI-5a65f353-2cee-4d40-a0bd-4eab107368af">
          <descripteur>conditions de fond</descripteur>
          <descripteur>caractère probable</descripteur>
          <descripteur>condition non remplie s’il s’agit d’une dette certaine dans son principe et son montant</descripteur>
          <descripteur>erreur comptable sans incidence sur le montant de l’actif net à la clôture et sur le résultat imposable</descripteur>
        </abstract>
      </blocAbstracts>
      <blocAnalyses id="UI-f3d9c05c-61db-464e-b2b3-d355e7c42f49">
        <analyse id="UI-a6eccc70-5488-4005-a3e4-a3c8c6785255" num="1°">
          <miseEnValeur type="mere">
            <al>Une société a inscrit dans un compte de provisions les sommes dues au titre de sa contribution au Fonds national pour l'emploi (FNE) alors qu’en raison de leur caractère liquide et exigible, elles devaient être regardées comme des dettes certaines dans leur principe et leur montant.</al>
          </miseEnValeur>
        </analyse>
        <analyse id="UI-a9140c99-bb24-47f3-bf29-b263564579bc" num="2°">
          <miseEnValeur type="mere">
            <al>Toutefois, une dette certaine dans son principe et dans son montant concourt à la réduction, à concurrence de ce montant, de l’actif net à la clôture de l’exercice et, par là-même, du résultat imposable.</al>
          </miseEnValeur>
        </analyse>
        <analyse id="UI-ccaab4a6-8eb5-4dd8-a31c-71ae24caf81b" num="3°">
          <miseEnValeur type="mere">
            <al>Dès lors, une cour administrative d’appel commet une erreur de droit en jugeant fondée la réintégration des sommes en litige, inscrites dans un compte de provisions, dans le résultat imposable de la société au seul motif qu’elles devaient être analysées, à raison de leur caractère liquide et exigible, comme des dettes certaines.</al>
          </miseEnValeur>
        </analyse>
        <analyse id="UI-6422e9b2-2ef2-492b-a62e-7dd44d352ee3" num="4°">
          <al>Il résulte des stipulations des conventions conclues entre l’administration du travail et la société sur les contributions dues par l’entreprise au FNE, que ces contributions s’analysent comme une dette certaine dans son principe comme dans son montant, sans qu’y fasse obstacle le non-respect de l’échéancier de paiement conventionnellement défini à raison de l’absence d’appels de fonds, cette seule circonstance invoquée par le ministre ne suffisant pas à faire présumer l’abandon de sa créance par le titulaire de celle-ci.</al>
        </analyse>
        <analyse id="UI-3502eec1-38fc-4fdf-9967-b69a4690f369" num="5°">
          <al>Dans ces conditions, la société doit être regardée comme justifiant de manière suffisante du maintien de cette dette, enregistrée à tort à titre de provision et non de charge à payer, dans ses écritures au passif du bilan de l’exercice.</al>
        </analyse>
      </blocAnalyses>
      <blocObservations id="UI-d7e7fd5d-3448-42dc-a113-20c1a08f5a5b">
        <observation id="UI-22752c59-385a-4e84-bdac-a709dc806de4">
          <al>Les <motRep>conclusions</motRep> du rapporteur public <motRep>Vincent Daumas</motRep> sont reproduites sur abonnes.efl.fr (<renvoiAutrui ref="UI-11e1cdcd-7b04-44db-94f6-8e53567c8600" uiId="UI-11e1cdcd-7b04-44db-94f6-8e53567c8600"/>).</al>
          <al>La décision n° 351852 du 1<e>er</e> octobre 2013 du Conseil d’Etat, cassant une première fois l’arrêt de la cour administrative d’appel de Nantes a été publiée à la <renvoiAutruiTransition annee="2013" numeroDocument="12/13" numeroInfo="1121" produitCx="RJF">RJF 12/13 n° 1121</renvoiAutruiTransition>, concl. V. Daumas <renvoiAutruiTransition annee="2013" numeroDocument="12/13" numeroInfo="127" produitCx="BDCF">BDCF 12/13 n° 127</renvoiAutruiTransition>. Par cette décision, le Conseil d’Etat censurait le motif retenu par la cour pour justifier la réintégration par l’administration d’une provision, constituée par l’entreprise pour faire face au versement futur à l’Etat de la contribution due au FNE. Ce motif était tiré d’une assimilation infondée en droit de cette contribution aux allocations de départ à la retraite ou en préretraite (CGI art. 39, 1-5°).</al>
          <al>Par son arrêt du 25 mars 2014 n° 13VE03133, non publié à la RJF, la cour de Versailles a justifié la réintégration de la provision sur un autre fondement, celui de l’erreur commise en comptabilisant en provisions les contributions dues au FNE qui constituaient une dette certaine dans son principe et son montant.</al>
          <al>Par la présente décision, le Conseil d’Etat censure une nouvelle fois le nouveau motif de rejet retenu par le second arrêt de la cour, pris après renvoi, puis après cassation règle l’affaire au fond.</al>
          <al>La jurisprudence estime, en effet, qu’un contribuable qui, à tort, a constitué une provision pour faire face à une dette dont il démontre qu’elle était d’ores et déjà certaine et exigible, ou correspondait à une perte définitive, commet une erreur comptable rectifiable : <refDocReprise date="14/04/1970" nature="CE" ref="78614">CE 14 avril 1970 n° 78614 plén.</refDocReprise> : Dupont p. 304, Lebon p. 248 ; <refDocReprise date="04/11/1970" nature="CE" ref="75564">CE 4 novembre 1970 n° 75564</refDocReprise> : Dupont 1971 p. 35, Lebon p. 636 (BOI-BIC-CHG-10-20-10 n° 30, 12 septembre 2012, BOI-BIC-BASE-40-10 n° 90, 13 mai 2013) ;<refDocReprise date="13/12/1972" nature="CE" ref="78584-78585"> CE 13 décembre 1972 n° 78584-78585</refDocReprise> : Dupont 1973 p. 93 et 94, Lebon p. 798.</al>
          <al>La jurisprudence estime, de façon analogue, que l’erreur consistant à inscrire dans un compte de frais à payer des appointements dus à son directeur par une société, alors que la société s’était abstenue pendant plusieurs années de les payer, est sans incidence sur le résultat de la société puisque ces sommes « n’en constituaient pas moins des dettes que la société devait inscrire à son passif » : <refDocReprise date="24/11/1971" nature="CE" ref="75549">CE 24 novembre 1971 n° 75549 plén.</refDocReprise> : Dupont 1972 p. 39. Voir dans le même sens, CE 21 janvier 1983 n° 23545 : <renvoiAutruiTransition annee="1983" numeroDocument="3/83" numeroInfo="444" produitCx="RJF">RJF 3/83 n° 444</renvoiAutruiTransition>.</al>
          <al>On rapprochera également la solution de CE 25 mars 2013 n° 355035, Sté Merlett France : <renvoiAutruiTransition annee="2013" numeroDocument="6/13" numeroInfo="589" produitCx="RJF">RJF 6/13 n° 589</renvoiAutruiTransition>, concl. N. Escaut <renvoiAutruiTransition annee="2013" numeroDocument="6/13" numeroInfo="67" produitCx="BDCF">BDCF 6/13 n° 67</renvoiAutruiTransition> selon laquelle une erreur d’enregistrement en comptabilité par une société d’une aide consentie par sa mère qui était également son fournisseur, n’avait pas eu d’incidence sur son bénéfice net dès lors que la correction de l’enregistrement de la dette de la société à l’égard de sa mère avait eu pour seul effet de la faire glisser d’un compte de passif à un autre compte de passif enregistrant la même créance de la société mère. A cet égard, le débat sur le caractère volontaire ou non de l’erreur comptable était sans incidence en l’absence de variation de l’actif net.</al>
          <al>La solution infirme la doctrine administrative selon laquelle une provision correspondant à une créance dont la perte apparaîtrait comme certaine à la clôture de l’exercice, serait irrégulière de sorte que son montant devrait être réintégré dans le résultat (D. adm. 4 E-1132 n° 2, 26 novembre 1996 ; BOI-BIC-PROV-20-10-30 n° 80, 12 septembre 2012 ; Doc. Lefebvre <renvoisFeuillet div="XII" ser="BIC">BIC-XII-<rEflN num="1795">1795</rEflN></renvoisFeuillet>).</al>
          <al>Dans l’hypothèse de l’erreur comptable en sens inverse (par exemple provision comptabilisée à tort en charges à payer), il est rappelé que pour les exercices clos avant le 12 juillet 1987, le défaut d’inscription des provisions sur le tableau faisait obstacle à leur déductibilité. A cet égard le Conseil d’Etat avait jugé que les charges inscrites à tort en frais à payer alors qu’elles pouvaient uniquement donner lieu à la constitution de provisions n’étaient pas déductibles faute d’avoir été comptabilisées en tant que telles et de figurer sur ledit tableau (CE 4 mai 1979 n° 98253 : <renvoiAutruiTransition annee="1979" numeroDocument="6/79" numeroInfo="342" produitCx="RJF">RJF 6/79 n° 342</renvoiAutruiTransition>).</al>
          <al>Il résulte des conclusions du rapporteur public que, dans cette espèce, le Conseil d’Etat semble avoir fait un certain effort pour poser la question à trancher mieux que ne l’avaient fait les parties.</al>
        </observation>
      </blocObservations>
      <sourceReproduite id="UI-67863592-541c-4145-83ee-f198de6c501a">
        <blocRefDoc>
          <refDocEnrichie>
            <refDoc formatRedige="true" ref="TY:jp|JUR:CE|DT:20160317|N:381427" type="reproduite">CE 17 mars 2016 n° 381427, 3<e>e</e> et 8<e>e</e> s.-s., Sté Sogetra</refDoc>
            <CompositionCour>M. Honorat, Prés. - M<e>me</e> Egerszegi, Rapp. - M. Daumas, R. public - M<e>e</e> Le Prado, Av.</CompositionCour>
          </refDocEnrichie>
        </blocRefDoc>
        <al>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'une vérification de la comptabilité de la société Sogetra, l'administration fiscale a réintégré dans les résultats de l'exercice clos en 1999 les provisions que cette société avait constituées au titre de sommes dues au Fonds national pour l'emploi (FNE) en vertu de conventions signées les 9 mars 1992 et 4 juin 1997 et d'un avenant conclu le 5 mars 1999 avec la direction départementale du travail et de l'emploi de la Guadeloupe ; que, par une décision n° 351852 du 1<e>er</e> octobre 2013, le Conseil d'Etat statuant au contentieux a annulé l'arrêt du 14 juin 2011 par lequel la cour administrative d'appel de Versailles avait rejeté l'appel formé par la société Sogetra contre le jugement du 8 juillet 2010 du tribunal administratif de Cergy-Pontoise rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés et des contributions assises sur cet impôt résultant de la réintégration des provisions mentionnées ci-dessus, auxquelles elle a été assujettie au titre de l'exercice clos en 1999 ; que la société Sogetra se pourvoit en cassation contre l'arrêt du 25 mars 2014 par lequel la cour administrative d'appel de Versailles, statuant sur renvoi, a rejeté son appel ;</al>
        <al>2. Considérant qu'aux termes du 2 de l'article 38 du CGI applicable à l'impôt sur les sociétés en vertu de l'article 209 du même Code, dans sa rédaction applicable à l'année d'imposition en litige : « 2. Le bénéfice net est constitué par la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de la période dont les résultats doivent servir de base à l'impôt (…). L'actif net s'entend de l'excédent des valeurs d'actif sur le total formé au passif par les créances des tiers, les amortissements et les provisions justifiés » ; qu'aux termes du 1 de l'article 39 du même Code : « Le bénéfice net est établi sous déduction de toutes charges, celles-ci comprenant, sous réserve des dispositions du 5, notamment : (…) 5° Les provisions constituées en vue de faire face à des pertes ou charges nettement précisées et que des événements en cours rendent probables, à condition qu'elles aient été effectivement constatées dans les écritures de l'exercice (…) » ; qu'il résulte de ces dispositions qu'une entreprise peut valablement porter en provision et déduire des bénéfices imposables d'un exercice le montant de charges qui ne seront supportées qu'ultérieurement par elle, à la condition que ces charges soient nettement précisées quant à leur nature et susceptibles d'être évaluées avec une approximation suffisante, qu'elles apparaissent comme probables eu égard aux circonstances constatées à la date de clôture de l'exercice et qu'elles se rattachent aux opérations de toute nature déjà effectuées à cette date par l'entreprise ;</al>
        <al>3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la cour a jugé fondée la réintégration des sommes en litige, inscrites dans un compte de provisions, dans le résultat imposable de la société au titre de l'exercice clos en 1999 au motif qu'elles devaient être analysées, à raison de leur caractère liquide et exigible, comme des dettes certaines ; que, toutefois, une dette certaine dans son principe et dans son montant concourt à la réduction, à concurrence de ce montant, de l'actif net à la clôture de l'exercice et, par là même, du résultat imposable ; que, dès lors, en jugeant ainsi qu'elle l'a fait, alors que la requalification de provision en dette certaine figurant au passif du bilan de l'exercice était sans incidence sur le résultat imposable de la société, la cour a commis une erreur de droit ; que, par suite, la société Sogetra est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'elle attaque ;</al>
        <al>4. Considérant qu'aux termes du second alinéa de l'article L 821-2 du Code de justice administrative : « Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire » ; qu'il y a lieu, par suite, de régler l'affaire au fond ;</al>
        <al>5. Considérant qu'il résulte de l'instruction que la convention conclue par la société Sogetra et la direction départementale du travail et de l'emploi de la Guadeloupe le 9 mars 1992 ainsi que celle conclue le 4 juin 1997 et amendée le 5 mars 1999, prévoyaient respectivement, à l'article 6 et à l'article 7, d'une part, les modalités de calcul des contributions dues par l'entreprise au FNE en fonction, premièrement, du salaire journalier de référence, défini à l'article 2 de ces textes, versé aux bénéficiaires du Fonds avant leur licenciement, deuxièmement, du nombre de jours, également défini aux articles 6 et 7, de perception par ces bénéficiaires de l'allocation spéciale versée par le Fonds et, troisièmement, d'un taux conventionnel, d'autre part, un échéancier de paiement de ces contributions ; que les stipulations conventionnelles précisaient que ce paiement était subordonné à des appels de fonds de la direction départementale du travail et de l'emploi ;</al>
        <al>6. Considérant, en premier lieu, qu'il résulte de ces stipulations que les sommes dues par la société Sogetra au FNE s'analysent comme une dette certaine dans son principe comme dans son montant, sans qu'y fasse obstacle le non-respect de l'échéancier de paiement conventionnellement défini à raison de l'absence d'appels de fonds tant au titre de la convention de 1992 que de celle de 1997 amendée en 1999, cette seule circonstance invoquée par le ministre ne suffisant pas à faire présumer l'abandon de sa créance par le titulaire de celle-ci ; que, dans ces conditions, la société Sogetra doit être regardée comme justifiant de manière suffisante du maintien de cette dette, enregistrée à tort à titre de provision et non de charge à payer, dans ses écritures au passif du bilan de l'exercice clos en 1999 ;</al>
        <al>7. Considérant, en second lieu, qu'au cours de la procédure contentieuse, la société a, d'une part, réduit le montant de sa dette à l'égard du FNE, initialement provisionnée pour un montant total de 1 556 776 F, à hauteur de 57 833 F au titre de la convention signée en 1992 et 97 807 F au titre de celle signée en 1997, ramenant ainsi le montant de sa dette à 1 401 136 F ; que la différence résultant de cette correction et s'élevant au total à 155 640 F doit, par suite, être réintégrée dans le résultat imposable de l'exercice clos en 1999 ; que, d'autre part, la société a indiqué que la provision calculée au titre de la convention signée en 1992 incluait, à hauteur de 173 776 F, la prise en charge de la participation au FNE due par les salariés bénéficiaires ; que cette prise en charge, qui ne correspond pas à une dépense normale de l'entreprise, doit, par suite, être également réintégrée dans le résultat imposable de l'exercice ;</al>
        <al>8. Considérant qu'il résulte de ce qui précède que la société Sogetra est seulement fondée à soutenir que c'est à tort que, par jugement du 8 juillet 2010, le tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à la réduction de sa base imposable à l'impôt sur les sociétés au titre de l'exercice clos en 1999 à hauteur de 1 227 360 F (187 109,80 €) ;</al>
        <al>Décide : 1° Annulation de l'arrêt de la cour administrative d'appel ; 2° Réduction de la base imposable à l'IS au titre de l'exercice clos en 1999 d'un montant de 1 227 360 francs (187 109,80 euros) ; 3° Décharge des cotisations supplémentaires d'IS et des contributions assises sur cet impôt assignées au titre de l'exercice clos en 1999 correspondant à la réduction de base définie au 2° ; 4° Réformation du jugement du tribunal administratif en ce qu'il a de contraire.</al>
      </sourceReproduite>
    </analyseObservation>
  </infoCommentaire>
</ui>
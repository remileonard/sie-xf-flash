<?xml version="1.0" encoding="UTF-8"?>
<ui xmlns="http://modeles.efl.fr/modeles/reference" createDt="2016-05-13T11:57:08.999+02:00" createUser="sdublineau" id="UI-c6ad0f4c-23ff-4c0c-804f-e80f17c59be7" lastUpdateDt="2016-06-03T11:22:59.999+02:00" lastUpdateUser="sdublineau" status="BAP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://modeles.efl.fr/modeles/reference uiCommentaire-bdd.xsd">
  <metadonnees>
    <metaCommunes>
      <metaType>
        <typeObjet>ui</typeObjet>
        <typeContenu>redige</typeContenu>
        <categorieUIRedigee idref="étudeSignée">Étude signée</categorieUIRedigee>
        <produitCx idref="rjf">RJF</produitCx>
      </metaType>
      <metaComportement>
        <publicationAuto>true</publicationAuto>
        <dateValidite>2016-05-04</dateValidite>
      </metaComportement>
      <metaOrigine>
        <origine idref="EFL">EFL</origine>
      </metaOrigine>
      <metaQualif>
        <themes>
          <theme idref="A6">Fiscal / IS</theme>
        </themes>
        
      <titreCalcule>Modalités de remboursement de la créance de « carry back »</titreCalcule></metaQualif>
    </metaCommunes>
    <metaSpecifiques>
      <metaQualifCible>
        <matieres>
          <matiere idref="Fiscal">Fis</matiere>
        </matieres>
        <metiers>
          <metier idref="AVOCAT">Avocat</metier>
        </metiers>
        <niveauExpertise>expert</niveauExpertise>
      </metaQualifCible>
      <metaSource><refDoc formatRedige="true" ref="TY:jp|JUR:CE|DT:20160309|N:385265" type="premiere">CE 9 mars 2016 no 385265, 9<e>e</e> et 10e s.-s., Sté Fimipar</refDoc></metaSource>
      <version>developpee</version>
      <modifierDate>true</modifierDate>
      <alerter>true</alerter>
    </metaSpecifiques>
  </metadonnees>
  <infoCommentaire>
    <titre>Modalités de remboursement de la créance de « carry back »</titre>
    <blocAuteur>
      <libelleIntroAuteur><motRep>CONCLUSIONS</motRep> du</libelleIntroAuteur>
      <auteur>
        <qualité/>
        <nom>Bokdam-Tognetti</nom>
        <prenom>Emilie</prenom>
        <signature/>
        <biographie>
          <al/>
        </biographie>
        <fonction>rapporteur public</fonction>
        <site/>
        <mail/>
        <societe>
          <al/>
        </societe>
        <explication>
          <al/>
        </explication>
      </auteur>
    </blocAuteur>
    <blocRefDoc>
      <refDocEnrichie><refDoc formatRedige="true" ref="TY:jp|JUR:CE|DT:20160309|N:385265" type="premiere">CE 9 mars 2016 no 385265, 9<e>e</e> et 10e s.-s., Sté Fimipar</refDoc> et <refDoc formatRedige="true" ref="TY:jp|JUR:CE|DT:20160309|N:385244" type="premiere">CE 9 mars 2016 no 385244, 9<e>e</e> et 10e s.-s.,</refDoc><renvoiAutrui uiId="UI-a3c392e8-9566-4bab-844a-ceb6ff6fdf63"/></refDocEnrichie>
    </blocRefDoc>
    <commentaire id="UI-6e1123df-ea68-4ae1-b754-9540b227d148">
      <niveau id="UI-12996052-41c0-4def-8612-6777f2b3d1c4">
        <pNum id="UI-95a83c2d-6069-4dfa-8334-c2a488f408f3">
          <al>Si le nom de Carrie est, pour les cinéphiles, évocateur de film d’horreur, celui de carry back est parfois, pour les fiscalistes, eu égard à ses ambiguïtés <rNote ref="Note-194ab0292"/>, synonyme de cauchemar. C’est néanmoins sans appréhension que nous vous invitons à vous pencher sur ces dossiers, qui posent la question de savoir dans quelles conditions et sous quels délais un contribuable qui a opté pour le report en arrière de ses déficits peut demander le remboursement à l’administration de sa créance sur le Trésor. </al>
        </pNum>
      </niveau>
      <niveau id="UI-49e9cf7d-c445-48b4-9cb5-353aa29af144">
        <titre type="titre1">Notions préliminaires sur le carry back</titre>
        <pNum id="UI-61648baf-07a8-4cd7-9c94-57241bc7d2bf">
          <al>Le report en arrière des déficits, ou « carry back », est autorisé et encadré par <motRep>l’article 220 quinquies</motRep> du CGI. Dans sa rédaction antérieure aux durcissements opérés par la <refDocReprise date="19/09/2011" nature="Loi" ref="2011-1117">loi 2011-1117 du 19 septembre 2011</refDocReprise>, cet article <motRep>permet</motRep>, par dérogation aux règles de droit commun de report en avant des déficits prévues à l’article 209, à une entreprise soumise à l’impôt sur les sociétés <motRep>d’opter pour le report en arrière du déficit</motRep> constaté au titre d’un exercice, c'est-à-dire pour son imputation sur un exercice passé qui a déjà été imposé. </al>
        </pNum>
        <pNum id="UI-a007faa4-b1ca-43a1-9c48-6c24e70b9f0d">
          <al><motRep>L'excédent d'impôt sur les sociétés en résultant</motRep>, qui ne procède pas d’une taxation excessive au sens de <refDocReprise nature="LPF" ref="L 80">l’article L 80 du LPF</refDocReprise> (CE 12 janvier 2005 n° 257652, SA Europinvest : <renvoiAutruiTransition annee="2005" numeroDocument="4/05" numeroInfo="310" produitCx="RJF">RJF 4/05 n° 310</renvoiAutruiTransition>, concl. L. Vallée <renvoiAutruiTransition annee="2005" numeroDocument="4/05" numeroInfo="41" produitCx="BDCF">BDCF 4/05 n° 41</renvoiAutruiTransition>), fait alors naître au profit de l'entreprise une <motRep>créance d'égal montant</motRep>, qui n’est pas imposable et qui est <motRep>remboursée au terme des cinq années</motRep> suivant celle de la clôture de l'exercice au titre duquel l'option a été exercée <rNote ref="Note-2e2c86a82"/>, <motRep>mais peut être utilisée pour le paiement de l'impôt sur les sociétés</motRep> dû au titre des exercices clos au cours de ces cinq années, la créance n'étant alors remboursée qu'à hauteur de la fraction qui n'a pas été utilisée pour ce paiement. <refDocReprise nature="CGI" rc="" ref="46 quater ">L’article 46 quater 0-W de l’annexe III au CGI</refDocReprise> dispose que l'entreprise qui exerce l'option pour le report en arrière doit joindre à la déclaration de résultat de l'exercice au titre duquel cette option a été exercée une déclaration conforme à un modèle fixé par l'administration. Le II de cet article, supprimé pour les créances ou réductions d'impôt constatées à compter du 1<e>er</e> novembre 2004, prévoit quant à lui qu’en cas d'utilisation de la créance pour le paiement de l'impôt sur les sociétés ou de cession, l'entreprise doit produire l'état de suivi qui lui avait été remis, à sa demande, par l'administration. Indiquons qu’en pratique, la demande de remboursement du montant de la créance non imputée sur l’impôt sur les sociétés à l’issue de la période de cinq ans est formulée sur le relevé de solde d’impôt sur les sociétés déposé auprès du service dont dépend l’entreprise. Enfin, <refDocReprise nature="LPF" ref="L 171 A">l’article L 171 A du LPF</refDocReprise> dote l’administration de pouvoirs de contrôle, en prévoyant que, pour l'application de <refDocReprise nature="CGI" ref="220 quinquies">l'article 220 quinquies du CGI</refDocReprise>, l'administration est fondée à vérifier l'existence et la quotité de la créance et à en rectifier le montant, même si l'option pour le report en arrière du déficit correspondant a été exercée au titre d'un exercice prescrit.</al>
        </pNum>
      </niveau>
      <niveau id="UI-b45dee5f-b2d6-4a8e-bb21-657acbfbfa55">
        <titre type="titre1">Conditions de remboursement de la créance de carry back</titre>
        <pNum id="UI-8506d12a-e711-4cdc-b085-af7078f2cf90">
          <al>Lorsqu’une entreprise a opté pour le report en arrière des déficits et qu’elle n’a, à l’expiration du délai de cinq ans mentionné à <refDocReprise nature="CGI" ref="220 quinquies">l’article 220 quinquies du Code</refDocReprise>, pas ou pas intégralement utilisé sa créance pour le paiement de ses cotisations d’impôt sur les sociétés, <motRep>le remboursement de la créance</motRep> qu’elle conserve sur le Trésor <motRep>est-il subordonné à la présentation d’une demande de remboursement, dans les délais et conditions de réclamation</motRep> fixés à <refDocReprise nature="LPF" ref="R 196-1">l’article R 196-1 du LPF</refDocReprise>, ou ce remboursement doit-il intervenir spontanément de la part de l’administration ?</al>
        </pNum>
        <pNum id="UI-bd6e88de-f15e-466e-9831-9fb7a65e91cd">
          <al>Les <motRep>juridictions du fond</motRep> se sont <motRep>divisées</motRep> sur cette question. </al>
          <al>Ainsi, tandis que le tribunal administratif de Paris avait jugé, par un jugement Sté Jet Multimedia Hosting (TA Paris 21 octobre 2009 n° 05-19135 : <renvoiAutruiTransition annee="2010" numeroDocument="5/10" numeroInfo="468" produitCx="RJF">RJF 5/10 n° 468</renvoiAutruiTransition>, concl. Mme K. Weidenfeld <renvoiAutruiTransition annee="2010" numeroDocument="5/10" numeroInfo="54" produitCx="BDCF">BDCF 5/10 n° 54</renvoiAutruiTransition>), que le remboursement d'une créance de carry-back n'est pas conditionné par la formulation d'une demande tendant à ce qu'il soit procédé à ce remboursement, et qu’il y avait lieu de faire application de la prescription quadriennale prévue à <refDocReprise date="31/12/1968" nature="Loi" ref="68-1250">l'article 1<e>er</e> de la loi 68-1250 du 31 décembre 1968</refDocReprise> relative à la prescription des créances sur l’Etat, les départements, les communes et les établissements publics, le tribunal administratif de Montreuil et la cour administrative d’appel de Versailles, dans les présentes affaires, par des décisions également publiées, ont estimé à l’inverse que la demande de remboursement d'une créance de carry-back était constitutive d'une réclamation contentieuse au sens de <refDocReprise nature="LPF" ref="L 190">l'article L 190 du LPF</refDocReprise>, distincte de la déclaration d'option pour le report en arrière des déficits, laquelle ne vaut pas demande de restitution, et qu’elle relevait par suite de <refDocReprise nature="LPF" ref="R 196-1">l'article R 196-1 du LPF</refDocReprise>.</al>
        </pNum>
        <pNum id="UI-b14beacf-b31a-4ad9-936d-540897d8d124">
          <al>Si vous n’avez, quant à vous, pas encore pris parti sur cette question, la solution nous semble toutefois déjà largement engagée par votre jurisprudence sur la nature de la déclaration d’option et, contrairement à ce qu’ont soutenu certains commentateurs de l’arrêt de la cour de Versailles, dans un sens opposé à l’approche de transposition retenue par celle-ci.</al>
        </pNum>
        <pNum id="UI-b850d3b5-ff39-41f2-be23-e86e5c262037">
          <al>En effet, par <motRep>votre décision min. c/ SA Sectronic</motRep> (CE 30 juin 1997 n° 178742 : <renvoiAutruiTransition annee="1997" numeroDocument="8-9/97" numeroInfo="776" produitCx="RJF">RJF 8-9/97 n° 776</renvoiAutruiTransition>, avec conclusions contraires sur ce point du président J. Arrighi de Casanova <renvoiAutruiTransition annee="1997" numeroDocument="8-9/97" page="511" produitCx="RJF">p. 511</renvoiAutruiTransition>), vous avez jugé que la <motRep>déclaration d’option pour le report en arrière</motRep> qui doit être jointe à la déclaration de résultat de l’exercice <motRep>valait réclamation</motRep> au sens de <refDocReprise nature="LPF" ref="L 190">l’article L 190 du LPF</refDocReprise>.</al>
        </pNum>
        <pNum id="UI-368d8dea-46d3-480c-ac00-1a81138b54ad">
          <al>Si cette solution avait été initialement dégagée pour justifier l’application aux contestations de refus de report en arrière des règles du plein contentieux fiscal, vous en avez ensuite tiré les <motRep>conséquences sur les règles de présentation de telles demandes</motRep>. Ainsi, vous avez d’abord jugé, par votre décision SA Vérimédia (CE 19 décembre 2007 n° 285588 et 294358 : <renvoiAutruiTransition annee="2008" numeroDocument="3/08" numeroInfo="347" produitCx="RJF">RJF 3/08 n° 347</renvoiAutruiTransition>, avec chronique Mme J. Burguburu <renvoiAutruiTransition annee="2008" numeroDocument="3/08" page="211" produitCx="RJF">p. 211</renvoiAutruiTransition>, concl. C. Landais <renvoiAutruiTransition annee="2008" numeroDocument="3/08" numeroInfo="42" produitCx="BDCF">BDCF 3/08 n° 42</renvoiAutruiTransition>), que <motRep>cette réclamation doit s’exercer dans les formes, conditions et délais prévus par le LPF</motRep>, et avez apprécié la recevabilité d’une demande de report en arrière de bénéfices rectifiés à la suite d’une procédure de reprise par l’administration au regard des règles de délai posées par <refDocReprise nature="LPF" ref="R 196-1">l’article R 196-1 du LPF</refDocReprise> et de la notion d’événement motivant la réclamation au sens du c de cet article. Puis dans votre décision min. c/ Sté Maysam France (CE 23 décembre 2011 n° 338773 : <renvoiAutruiTransition annee="2012" numeroDocument="3/12" numeroInfo="232" produitCx="RJF">RJF 3/12 n° 232</renvoiAutruiTransition>, concl. E. Geffray <renvoiAutruiTransition annee="2012" numeroDocument="3/12" numeroInfo="31" produitCx="BDCF">BDCF 3/12 n° 31</renvoiAutruiTransition>), vous avez jugé que si, en vertu des dispositions de <refDocReprise nature="CGI" ref="46 quater-0 W">l'article 46 quater-0 W de l'annexe III au CGI</refDocReprise>, cette déclaration doit être en principe souscrite en même temps que la déclaration des résultats de cet exercice dans le <motRep>délai</motRep> légal de déclaration, ces dispositions ne peuvent avoir eu pour effet d'interdire à une entreprise, dans le cas où elle aurait souscrit sa déclaration de résultats après l'expiration de ce délai, de <motRep>régulariser</motRep> sa déclaration d'option pour le report en arrière d'un déficit <motRep>jusqu'à l'expiration du délai de réclamation</motRep> prévu par les dispositions de <refDocReprise nature="LPF" ref="R 196-1">l'article R 196-1 du LPF</refDocReprise>.</al>
        </pNum>
        <pNum id="UI-d0e59c17-cdbf-4c77-8a6a-454cebdfde84">
          <al><motRep>Si la déclaration d’option vaut ainsi réclamation</motRep> au sens de <refDocReprise nature="LPF" ref="L 190">l’article L 190 du LPF</refDocReprise>, c’est parce qu’elle tend au bénéfice d’un droit résultant d’une disposition législative, en l’occurrence de <refDocReprise nature="CGI" ref="220 quinquies">l’article 220 quinquies du CGI</refDocReprise>. <motRep>Le remboursement</motRep> de la créance sur le Trésor née de l’option au terme des cinq années suivant celle de la clôture de l'exercice au titre duquel cette option a été exercée, est en effet non seulement de droit – <refDocReprise nature="CGI" ref="220 quinquies">l’article 220 quinquies</refDocReprise> utilise ainsi l’indicatif et ne subordonne ce remboursement à aucune formalité particulière, notamment aucune demande complémentaire à la déclaration d’option – mais <motRep>constitue le droit commun du report en arrière</motRep>, l’utilisation anticipée de la créance pour le paiement de l’impôt sur les sociétés dans ce même délai de cinq années n’apparaissant que comme une faculté, dérogeant au principe du remboursement au bout de cinq ans. Dès la déclaration d’option, qui fait naître la créance sur le Trésor, le montant de cette créance est connu, les règles relatives à son calcul étant posées par <refDocReprise nature="CGI" ref="220 quinquies">l’article 220 quinquies</refDocReprise> lui-même. L’administration dispose toutefois des pouvoirs nécessaires à son contrôle, grâce à <refDocReprise nature="LPF" ref="L 171 A">l’article L 171 A du LPF</refDocReprise>, ainsi qu’à son suivi au fur et à mesure de son utilisation éventuelle pour le paiement de l’impôt, grâce notamment à l’article 46 quater 0-W de l’annexe III.</al>
        </pNum>
        <pNum id="UI-daec77a7-a838-44fd-9f06-2953db55f609">
          <al>Dès lors que le droit au bénéfice du report en arrière et le droit au remboursement de la créance ne forment qu’un seul et même droit et que la déclaration d’option vaut ainsi réclamation contentieuse tendant à la constatation de l’existence d’une créance sur le Trésor et à son remboursement au terme du délai de cinq ans, pour la fraction qui n’aura pas été utilisée pour le paiement de l’impôt sur les sociétés, <motRep>point n’est besoin pour le contribuable de présenter une nouvelle réclamation contentieuse à l’expiration de ce délai</motRep> pour obtenir ce remboursement, qui doit intervenir spontanément de la part de l’administration. En cas de cession à un établissement de crédit de la créance de carry-back, le contribuable et l’établissement devraient, selon nous, en tenir informée l’administration, afin qu’au terme des cinq années le remboursement puisse intervenir au profit de l’exact bénéficiaire.</al>
        </pNum>
        <pNum id="UI-5dcd3c6d-a51c-48d8-819e-dd207c25109f">
          <al><motRep>Dans l’hypothèse où l’administration ne s’acquitte pas de son obligation de remboursement spontané</motRep> à l’expiration du délai de cinq années prévu à <refDocReprise nature="CGI" ref="220 quinquies">l’article 220 quinquies</refDocReprise>, il appartient au <motRep>contribuable</motRep> de lui présenter une <motRep>demande</motRep> tendant à ce <motRep>remboursement dans le délai de prescription quadriennale</motRep> prévu par l’article 1<e>er</e> de la loi du 31 décembre 1968, et, en cas de rejet de cette demande, de porter le litige devant le juge de plein contentieux. </al>
        </pNum>
        <pNum id="UI-42c9b67a-a921-4105-870c-97eadd9f118d">
          <al>C’est en effet, compte tenu de ce que nous venons de vous dire sur l’existence et l’acquisition de la créance par le contribuable dès la première réclamation, <motRep>ce délai de prescription</motRep> qui <motRep>nous semble applicable</motRep>, à l’exclusion de celui de <refDocReprise nature="LPF" ref="R 196-1">l’article R 196-1 du LPF</refDocReprise>. Nous n’ignorons pas votre jurisprudence min. c./ Sté Champagne Jeanmaire et min. c/ Sté Champagne Beaumet (CE 14 février 2001 n° 202966 et n° 202967 : <renvoiAutruiTransition annee="2001" numeroDocument="5/01" numeroInfo="671" produitCx="RJF">RJF 5/01 n° 671</renvoiAutruiTransition>, concl. J. Courtial <renvoiAutruiTransition annee="2001" numeroDocument="5/01" numeroInfo="70" produitCx="BDCF">BDCF 5/01 n° 70</renvoiAutruiTransition>), dans laquelle vous avez jugé que la prescription quadriennale instituée par la loi du 31 décembre 1968 n'est applicable que sous réserve des dispositions définissant un régime légal de prescription spécial à une catégorie déterminée de créances susceptibles d'être invoquées à l'encontre de l'une de ces personnes morales de droit public, que les dispositions de <refDocReprise nature="LPF" ref="R 196-1">l'article R 196-1 du LPF</refDocReprise>, alors même qu’elles se présentent comme instituant des délais de réclamation, ont pour effet d'instituer un régime légal de prescription propre aux créances d'origine fiscale dont les contribuables entendent se prévaloir envers l'Etat, et que ces créances sont, de ce fait, exclues du champ d'application de la loi du 31 décembre 1968. Mais cette jurisprudence ne nous semble pas applicable au cas d’une créance fiscale dont l’administration a déjà admis, à l’occasion d’une première réclamation réalisée dans les formes et selon les conditions des <refDocReprise nature="LPF" ref="L 190">articles L 190 et R 196-1 du LPF</refDocReprise>, l’existence, et dont il ne s’agit plus que d’obtenir le paiement.</al>
        </pNum>
        <pNum id="UI-b86c8dec-f023-4eea-9c0a-5fc5138cb9a9">
          <al>Enfin, la solution que nous vous invitons aujourd’hui à adopter ne nous paraît pas miroiter avec celle que vous avez retenue en matière de crédit d’impôt recherche, pour lequel vous avez jugé que la demande de remboursement présentée sur le fondement de <refDocReprise nature="CGI" ref="199 ter B">l’article 199 ter B du CGI</refDocReprise> constitue une réclamation au sens de <refDocReprise nature="LPF" ref="L 190">l’article L 190 du LPF</refDocReprise> (CE 8 novembre 2010 n° 308672, Sté ICBT Madinox : <renvoiAutruiTransition annee="2011" numeroDocument="2/11" numeroInfo="229" produitCx="RJF">RJF 2/11 n° 229</renvoiAutruiTransition>). En effet, si pour le crédit d’impôt recherche, il y a bien une première déclaration avant la demande de remboursement, cette déclaration n’a toutefois jamais été qualifiée de réclamation contentieuse par votre jurisprudence. </al>
        </pNum>
      </niveau>
      <niveau id="UI-4d9160e2-6309-408c-9db3-797de6950a05">
        <titre type="titre1">Les présentes espèces</titre>
        <pNum id="UI-58d3f3a8-583d-452e-b772-c6d393b808ef">
          <al>Venons-en maintenant aux faits des deux espèces. La société BFO, le 27 avril 2001, et la société Fimipar, le 18 juin 2002, ont opté pour le <motRep>report en arrière du déficit</motRep> constaté au titre des exercices clos respectivement en 1999 et en 2001, faisant ainsi naître au profit de chacune une <motRep>créance sur le Trésor</motRep>. N’ayant pas utilisé cette créance pour le paiement de l’impôt sur les sociétés au terme du délai de cinq ans mentionné à <refDocReprise nature="CGI" ref="220 quinquies">l’article 220 quinquies</refDocReprise>, elles en ont <motRep>sollicité le remboursement</motRep> le 23 novembre 2009 et le 23 novembre 2011, par des demandes que l’administration fiscale a <motRep>rejetées comme tardives au regard du délai de réclamation</motRep> fixé par <refDocReprise nature="LPF" ref="R 196-1">l’article R 196-1 du LPF</refDocReprise>. Le tribunal administratif de Montreuil, par deux jugements dont l’un a été publié (TA Montreuil 23 mai 2013 n° 1201493 : <renvoiAutruiTransition annee="2014" numeroDocument="1/14" numeroInfo="12" produitCx="RJF">RJF 1/14 n° 12</renvoiAutruiTransition>, concl. V. Restino <renvoiAutruiTransition annee="2014" numeroDocument="1/14" numeroInfo="3" produitCx="BDCF">BDCF 1/14 n° 3</renvoiAutruiTransition>), a rejeté les demandes des contribuables tendant au remboursement de ces créances. Saisie en appel par les sociétés, la cour administrative d’appel de Versailles, par les deux arrêts du 8 juillet 2014, également publiés et commentés (n° 11VE03849 et n° 13VE02399 : <renvoiAutruiTransition annee="2014" numeroDocument="11/14" numeroInfo="987" produitCx="RJF">RJF 11/14 n° 987</renvoiAutruiTransition>), attaqués devant vous aujourd’hui, a confirmé ces jugements. </al>
        </pNum>
        <pNum id="UI-64d75d54-6099-419c-bb33-6f855729cc18">
          <al>A l’appui de leurs <motRep>pourvois</motRep>, les sociétés soutiennent notamment que la cour a commis une <motRep>erreur de droit</motRep> en jugeant que le <motRep>remboursement de leur créance</motRep> de carry-back était <motRep>subordonné à la présentation</motRep>, dans les délais prévus à <refDocReprise nature="LPF" ref="R 196-1">l’article R 196-1 du LPF</refDocReprise>, <motRep>d’une réclamation contentieuse</motRep> distincte de la déclaration d’option qu’elles avaient effectuée. Compte tenu de ce que nous vous avons expliqué, ce moyen nous paraît fondé. Vous ferez donc droit aux pourvois, sans qu’il soit besoin d’examiner les autres moyens qu’ils soulèvent.</al>
          <couyard type="astérisme"/>
        </pNum>
        <pNum id="UI-707873a5-254c-4e5a-9a7a-595561d66bc7">
          <al>Par ces motifs, nous concluons :</al>
          <lst format="tiret">
            <item>
              <al>à l’annulation des arrêts du 8 juillet 2014 de la cour administrative d’appel de Versailles ;</al>
            </item>
            <item>
              <al>au renvoi des affaires à cette cour ;</al>
            </item>
            <item>
              <al>et à ce que l’Etat verse à la société Fimipar et à la société BFO la somme de 3 500 € chacune au titre de <refDocReprise nature="C. just. adm." ref="L 761-1">l’article L 761-1 du Code de justice administrative</refDocReprise>.</al>
            </item>
          </lst>
        </pNum>
      </niveau>
    </commentaire>
    <blocNotes>
      <note id="Note-194ab0292">
        <al>Voir par exemple, M-C. Bergeres, « Les ambiguïtés du régime juridique de la créance de carry-back », Dr. fisc. 15/04 n° 16.</al>
      </note>
      <note id="Note-2e2c86a82">
        <al>C’est-à-dire l’exercice d’origine du déficit et non celui durant lequel la société déclare opter pour le report (CE 4 août 2006 n° 285201, min. c/ Sté Kaufman &amp; Broad Participations : <renvoiAutruiTransition annee="2006" numeroDocument="11/06" numeroInfo="1330" produitCx="RJF">RJF 11/06 n° 1330</renvoiAutruiTransition>, concl. F. Séners <renvoiAutruiTransition annee="2006" numeroDocument="11/06" numeroInfo="127" produitCx="BDCF">BDCF 11/06 n° 127</renvoiAutruiTransition>).</al>
      </note>
    </blocNotes>
  </infoCommentaire>
</ui>
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE FPRO SYSTEM "file:///D:/FrameMaker/EDD/GB/GB_FPRO.dtd" [

<!-- Begin Document Specific Declarations -->

<?Fm Validation Off?>


<!-- End Document Specific Declarations -->

]>
<?xml-stylesheet href="Q5203_FR_Rayonnementsoptiquesartificiels.fm.css" type="text/css"?>

<?Fm Condition Commentaire Rouge SINGLE_UNDERLINE show AsIs?>
<?Fm Condition FM8_SYSTEM_HIDEELEMENT Gris%20anthracite NO_OVERRIDE hide AsIs?>
<?Fm Condition FM8_TRACK_CHANGES_ADDED Vert%20sapin SINGLE_UNDERLINE show AsIs?>
<?Fm Condition FM8_TRACK_CHANGES_DELETED Rouge STRIKETHROUGH show AsIs?>

<?Fm BoolCondExpr "Commentaire" State 0?>
<?Fm TagBoolCondExpr Expression%20par%20défaut?>


<?Fm TrackChange Off PreviewState PREVIEW_OFF_TRACK_CHANGE ?>
<FPRO GB = "GB95" MR = "On" Position = "Corps" Maj = "01-11" Eddversion = "GB_FPRO_EDD_V1" Typedoc = "FRISQ"><Titrefiche>Rayonnements optiques artificiels</Titrefiche>
<Al>Il existe deux sortes de rayonnements optiques : naturels et artificiels. Les rayonnements optiques artificiels sont utilisés dans de nombreux secteurs et présentent des risques importants pour la santé et la sécurité des travailleurs. C’est pourquoi l’employeur doit mettre en œuvre toutes les mesures de prévention nécessaires pour éviter tout dépassement de valeurs limites d’exposition à ces rayonnements.</Al><TABLEAU><COURANT><LIGNE><CELLULE></CELLULE><CELLULE></CELLULE><CELLULE></CELLULE></LIGNE></COURANT></TABLEAU>
<INT>Un rayonnement optique, c’est quoi au juste ?</INT>
<Al>Selon le code du travail, les rayonnements optiques représentent tous les rayonnements électromagnétiques d’une longueur d’onde comprise entre 100 nanomètres et 1 millimètre <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-1" Type = "R" fmcssattr = "12"/></Refcode>. Les rayonnements optiques se divisent en 3 catégories :</Al>
<Alpuce>les ultraviolets : rayonnements optiques d’une longueur d’onde comprise entre 100 nanomètres et 400 nanomètres. Le domaine de l’ultraviolet se subdivise en rayonnements UVA (315-400 nanomètres), UVB (280-315 nanomètres) et UVC (100-280 nanomètres) ;</Alpuce>
<Alpuce>les visibles : rayonnements optiques d’une longueur d’onde comprise entre 380 nanomètres et 780 nanomètres ;</Alpuce>
<Alpuce>les infrarouges : rayonnements optiques d’une longueur d’onde comprise entre 780 nanomètres et 1 millimètre. Le domaine de l’infrarouge se subdivise en rayonnements IRA (780-1 400 nanomètres), IRB (1 400-3 000 nanomètres) et IRC (3 000 nanomètres-1 millimètre).</Alpuce>
<Dtif>Les différentes sources de rayonnements optiques</Dtif>
<Al>Il existe plusieurs sources de rayonnements optiques. Les plus connues sont par exemple :</Al>
<Alpuce>les sources à incandescence : tungstène des ampoules ;</Alpuce>
<Alpuce>les sources fluorescentes ou phosphorescentes : lampes tubulaires tapissées de poudre fluorescente ;</Alpuce>
<Alpuce>les sources lasers : Argon, Krypton, CO<IN>2</IN> par exemple.</Alpuce>
<Dtif>Les différentes utilisations des sources optiques</Dtif>
<Al>Les domaines d’utilisation sont variés. C’est surtout en matière d’éclairage que l’on retrouve les rayonnements optiques mais ils sont aussi utilisés dans le secteur industriel (par exemple, séchage des encres, polymérisation des colles) ou encore médical (par exemple, photothérapie, désinfection bactérienne).</Al>
<INT>Qui est concerné ?</INT>
<Al>Beaucoup de branches d’activités sont concernées par les risques liés aux rayonnements optiques : industrie du bois, mines et carrières, BTP, plastique, recherche, électronique, etc.</Al>
<Al>Les deux types de population les plus exposés sont les personnes développant, fabriquant, testant ou réglant les sources et appareils optiques ou le personnel utilisateur final.</Al>
<INT Typemaj = "0">Comment identifier les dangers liés aux rayonnements optiques ?</INT>
<Al Typemaj = "0">L’identification du danger peut être difficile, notamment pour un nouvel embauché. Exemple : le nouvel embauché dans une station d’épuration peut considérer que la présence des lampes à rayonnements UV n’est pas un danger, ou un danger mineur, car l’EPI à porter est une paire de lunettes de protection qui ressemble à des lunettes de soleil. </Al>
<INT>Comment évaluer les risques d’exposition aux rayonnements optiques ?</INT>
<Dtif>Connaître les risques d’exposition aux rayonnements optiques artificiels</Dtif>
<Al Typemaj = "0">Les risques liés à l’exposition aux rayonnements optiques touchent essentiellement les yeux et la peau. Les rayonnements peuvent donner lieu à des brûlures allant jusqu’au 3<E>e</E> degré, un vieillissement de la peau, des cancers de la peau, des cataractes, brûlures de la cornée, etc.</Al>
<Al Typemaj = "0">L’accident du travail peut résulter par exemple d’une brûlure suite à une exposition accidentelle à un laser. Mais les risques résident surtout dans la longueur et la fréquence de l’exposition.</Al>
<Dtif Typemaj = "0">Prendre en considération les éléments indispensables pour l’évaluation</Dtif>
<Al Typemaj = "0">L’employeur évalue les risques résultant de l’exposition aux rayonnements optiques artificiels, notamment afin de vérifier le respect des valeurs limites d’exposition. Si une évaluation à partir des données documentaires techniques disponibles ne permet pas de conclure à l’absence de risque, il calcule et, le cas échéant, mesure les niveaux de rayonnements optiques artificiels auxquels les travailleurs sont exposés <Refcode Type = "ART" Code = "PXCT2" Typemaj = "0"><Xacode Num = "4452-7" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<Al Typemaj = "0">Cette évaluation est effectuée après consultation du CHSCT, avec le concours, le cas échéant du médecin du travail <Refcode Type = "ART" Code = "PXCT2" Typemaj = "0"><Xacode Num = "4452-9" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<Al Typemaj = "0">L’employeur doit prendre en considération <Refcode Type = "ART" Code = "PXCT2" Typemaj = "0"><Xacode Num = "4452-8" Type = "R" fmcssattr = "12"/></Refcode> :</Al>
<Alpuce Typemaj = "0">le niveau, le domaine des longueurs d’ondes et la durée de l’exposition à des sources artificielles de rayonnement optique ;</Alpuce>
<Alpuce Typemaj = "0">les valeurs limites d’exposition ;</Alpuce>
<Alpuce Typemaj = "0">toute incidence sur la santé et la sécurité des travailleurs appartenant à des groupes à risques particulièrement sensibles ;</Alpuce>
<Alpuce Typemaj = "0">toute incidence éventuelle sur la santé et la sécurité des travailleurs résultant d’interactions, sur le lieu de travail, entre des rayonnements optiques et des substances chimiques photosensibilisantes ;</Alpuce>
<Alpuce Typemaj = "0">tout effet indirect tel qu’un aveuglement temporaire, une explosion ou un incendie ;</Alpuce>
<Alpuce Typemaj = "0">l’existence d’équipements de remplacement conçus pour réduire les niveaux d’exposition à des rayonnements optiques artificiels ;</Alpuce>
<Alpuce Typemaj = "0">des informations appropriées obtenues de la surveillance de la santé, y compris les informations publiées, dans la mesure du possible ;</Alpuce>
<Alpuce Typemaj = "0">l’exposition à plusieurs sources de rayonnements optiques artificiels ;</Alpuce>
<Alpuce Typemaj = "0">le classement d’un laser conformément à la norme pertinente de la CEI et, en ce qui concerne les sources artificielles susceptibles de provoquer des lésions similaires à celles provoquées par des lasers de classe 3B ou 4, tout classement analogue ;</Alpuce>
<Alpuce Typemaj = "0">l’information fournie par les fabricants de sources de rayonnement optique et d’équipements de travail associés conformément aux directives communautaires applicables.</Alpuce>
<Al Typemaj = "0">Les résultats de l’évaluation des risques sont consignés dans le document unique d’évaluation des risques professionnels. Ils sont communiqués au CHSCT et au médecin du travail.</Al>
<INT>Quelles sont les valeurs limites d’exposition ?</INT>
<Al>L’exposition des travailleurs ne peut dépasser les valeurs limites d’exposition aux rayonnements optiques. Ces limites d’exposition sont complexes. Elles figurent en annexe I (rayonnements optiques hors laser) et annexe II (rayonnements optiques laser) du décret n<E>o</E> 2010-750 du 2 juillet 2010 (<I>JO</I>, 4 juill., texte n<E>o</E> 11) <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-5" Type = "R" fmcssattr = "12"/> et <Xacodes Num = "4452-6" Type = "R"/></Refcode>.</Al>
<Al>Lorsqu’il y a modification des installations ou de l’organisation de travail susceptible de faire augmenter l’exposition aux rayonnements optiques ou encore lorsque le médecin du travail a détecté une maladie ou une anomalie pouvant être liée à ce type d’exposition, l’employeur a l’obligation de renouveler l’évaluation <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-9" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<Al>L’inspecteur ou le contrôleur du travail peut demander à l’employeur de faire procéder à un contrôle technique des valeurs limites d’exposition aux rayonnements optiques artificiels par un organisme accrédité. Il fixe le délai dans lequel l’organisme accrédité doit être saisi. L’employeur justifie qu’il a saisi l’organisme accrédité pendant le délai qui lui a été fixé et transmet à l’inspecteur ou au contrôleur du travail les résultats dès leur réception <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4722-20" Type = "R" fmcssattr = "12"/> et s.</Refcode>.</Al>
<INT Typemaj = "0">Quelles mesures de prévention mettre en place ?</INT>
<Dtif Typemaj = "0">Mesures de prévention à mettre en place en cas de dépassement 
des valeurs limites d’exposition ?</Dtif>
<Al>Lorsque le seuil des valeurs limites d’exposition est susceptible d’être dépassé, l’employeur détermine toutes les mesures de prévention à prendre, la formation du personnel et le suivi médical de ce dernier <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-11" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<Al>Si en dépit des mesures de prévention prises par l’employeur le seuil des valeurs d’exposition est dépassé, l’employeur détermine toutes les causes du dépassement et met en œuvre toutes les mesures de prévention et de précaution pour réduire l’exposition durablement <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-18" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<Titre3 Typemaj = "0">Notice de postes exposés aux dépassements</Titre3>
<Al>Pour tous les postes ou situations de travail susceptibles d’être exposés à un dépassement des valeurs limites d’exposition d’après les résultats de l’évaluation des risques, l’employeur doit établir une notice de poste. Cette notice <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-20" Type = "R" fmcssattr = "12"/></Refcode> :</Al>
<Alpuce>informe les salariés sur les risques auxquels ils sont confrontés et les dispositions prises pour les éviter ;</Alpuce>
<Alpuce>rappelle les règles de sécurité applicables ainsi que les consignes relatives à l’emploi des équipements de protection collective ou individuelle.</Alpuce>
<Titre3 Typemaj = "0">Liste et fiches d’exposition des travailleurs exposés au dépassement</Titre3>
<Al>Pour chacun des travailleurs susceptibles d’être exposés à un dépassement du seuil limite d’exposition, l’employeur a l’obligation <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-22" Type = "R" fmcssattr = "12"/> et <Xacodes Num = "4452-23" Type = "R"/></Refcode> :</Al>
<Alpuce>de les inclure dans une liste qui précise la nature de l’exposition, sa durée et son niveau. L’employeur doit la tenir à jour ;</Alpuce>
<Alpuce>d’établir une fiche d’exposition pour chacun des travailleurs concernés. Elle précise la nature du travail accompli, les caractéristiques des sources émettrices auxquelles le travailleur est confronté, la nature des rayonnements, les résultats des mesurages des niveaux de rayonnements le cas échéant et enfin la période d’exposition du travailleur.</Alpuce>
<Al>En cas d’exposition anormale, l’employeur porte sur la fiche d’exposition sa durée et sa nature <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-24" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<Dtif Typemaj = "0">Mesures de prévention collective</Dtif>
<Al>L’employeur doit prévoir des mesures de protection collective pour supprimer le risque ou, le cas échéant, le réduire.</Al>
<Al>La réduction des risques d’exposition aux rayonnements optiques artificiels se fonde notamment sur <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-13" Type = "R" fmcssattr = "12"/></Refcode> :</Al>
<Alpuce>la mise en œuvre d’autres procédés de travail n’exposant pas aux rayonnements optiques artificiels ou entraînant une exposition moindre ;</Alpuce>
<Alpuce>le choix d’équipements de travail appropriés émettant, compte tenu du travail à effectuer, le moins de rayonnements optiques artificiels possible ;</Alpuce>
<Alpuce>la limitation de la durée et de l’intensité des expositions ;</Alpuce>
<Alpuce>la conception, l’agencement des lieux et postes de travail et leur modification ;</Alpuce>
<Alpuce>des moyens techniques pour réduire l’exposition aux rayonnements optiques artificiels en agissant sur leur émission, leur propagation, leur réflexion, tels qu’écrans, capotages ;</Alpuce>
<Alpuce>des programmes appropriés de maintenance des équipements de travail et du lieu de travail ;</Alpuce>
<Alpuce>l’information et la formation adéquates des travailleurs.</Alpuce>
<Al>L’atténuation des rayonnements optiques peut être obtenue grâce à l’interposition de plusieurs éléments de protection comme des écrans de protection ou des hublots filtrants par exemple.</Al>
<Al>En outre, pour minimiser les effets des rayonnements optiques il faut laisser un dégagement suffisant autour de la zone de travail, prévoir un éclairage des locaux importants, proscrire l’utilisation de surfaces réfléchissantes, notamment.</Al>
<Al>D’autre part, les risques dus aux rayonnements sont réduits quand on évite les expositions de longue durée et les expositions trop répétées. Il faut donc organiser la rotation des postes de travail sur les postes concernés.</Al>
<Dtif Typemaj = "0">Mesures de protection individuelle</Dtif>
<Al>Les équipements de protection individuelle doivent permettre de réduire les expositions à un niveau qui ne dépasse pas les valeurs limites d’exposition <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-17" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<Al>Lorsque les niveaux d’exposition sont dépassés, l’employeur a l’obligation de veiller à ce que les salariés portent correctement leurs équipements <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-16" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<Al>L’employeur, pour la mise en place de ces équipements, doit consulter le CHSCT ainsi que le médecin du travail.</Al>
<Al>Les équipements de protection individuelle sont choisis en concertation avec les travailleurs.</Al>
<Al>Voici des exemples d’équipements de protection individuelle :</Al>
<Alpuce>lunettes de protection contre les ultraviolets, des rayons laser, des rayonnements X ;</Alpuce>
<Alpuce>masques ;</Alpuce>
<Alpuce>écrans faciaux ;</Alpuce>
<Alpuce>gants de protection…</Alpuce>
<Dtif>Information et formation des travailleurs</Dtif>
<Al>L’employeur veille à ce que les travailleurs exposés à des rayonnements optiques artificiels reçoivent une information sur les risques éventuels liés à ce type de rayonnements <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-3" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<Al>La formation des travailleurs est une obligation pour l’employeur <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-19" Type = "R" fmcssattr = "12"/></Refcode>. Les mesures de formation portent notamment sur :</Al>
<Alpuce>les sources de rayonnements optiques artificiels se trouvant sur le lieu de travail ;</Alpuce>
<Alpuce>les risques pour la santé et la sécurité pouvant résulter d’une exposition excessive aux rayonnements optiques artificiels ainsi que les valeurs limites d’exposition applicables ;</Alpuce>
<Alpuce>les résultats de l’évaluation des risques ainsi que les mesures prises en vue de supprimer ou de réduire les risques résultant des rayonnements optiques artificiels ;</Alpuce>
<Alpuce>les précautions à prendre par les travailleurs pour assurer leur protection et celle des autres travailleurs présents sur le lieu de travail ;</Alpuce>
<Alpuce>l’utilisation des équipements de travail et équipements de protection individuelle ;</Alpuce>
<Alpuce>la conduite à tenir en cas d’accident ;</Alpuce>
<Alpuce>la manière de repérer les effets nocifs d’une exposition sur la santé et de les signaler ;</Alpuce>
<Alpuce>les conditions dans lesquelles les travailleurs sont soumis à une surveillance médicale.</Alpuce>
<Dtif>Signalisation</Dtif>
<Al>Les lieux de travail dans lesquels les travailleurs sont susceptibles d’être exposés à des dépassements de valeurs limites d’exposition doivent faire l’objet d’une signalisation. L’employeur, s’il le peut, circonscrit les lieux et limite leurs accès <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-14" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<INT>Surveillance médicale renforcée</INT>
<Al>Le médecin du travail établit et met à jour pour chaque travailleur susceptible d’être exposé aux rayonnements dépassant les valeurs limites d’exposition, un dossier contenant <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-31" Type = "R" fmcssattr = "12"/></Refcode> :</Al>
<Alpuce>une copie de la fiche d’exposition faite par l’employeur ;</Alpuce>
<Alpuce>les dates et résultats des examens médicaux pratiqués.</Alpuce>
<Al>Dès lors qu’un travailleur est exposé à un dépassement du seuil des valeurs limites d’exposition aux rayonnements ou que la surveillance médicale met en évidence la survenance d’une maladie ou d’une anomalie susceptible d’être la conséquence d’une exposition aux rayonnements, le médecin du travail informe le travailleur des résultats le concernant et lui indique les suites médicales nécessaires. Concernant les travailleurs qui ont subi une exposition comparable, le médecin détermine la pertinence et la nature des examens éventuels nécessaires <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-29" Type = "R" fmcssattr = "12"/></Refcode>. </Al>
<Al>Dans ce cas, une nouvelle évaluation des risques doit être réalisée par l’employeur <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-30" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<Al>Une copie de la fiche d’exposition est remise au médecin du travail. Elle est tenue à disposition, sur sa demande, de l’inspection du travail <Refcode Type = "ART" Code = "PXCT2"><Xacode Num = "4452-25" Type = "R" fmcssattr = "12"/></Refcode>.</Al>
<OBS ID = "Q5FPRO203-1" Type = "Zoneobs"><Table><Tgroup>
<Tbody>
<Row>
<Entry></Entry>
<Entry></Entry>
<Entry></Entry>
</Row>
<Row>
<Entry></Entry>
<Entry><Titreobservation Typemaj = "0"> </Titreobservation>
<Al Typemaj = "0">Sur la réglementation applicable aux rayonnements non ionisants, <Renvoi Type = "FREG" Typemaj = "0" Produit = "GP95" Inline = "Oui"><Saisie></Saisie><Fiche Idref = "Q5FPRO525" Partie = "réglementaire" Numfiche = "24" Nbr = "SING" fmcssattr = "7" Titre = "Rayonnements non ionisants" Theme = "Risques physiques"></Fiche></Renvoi></Al>
<Al Typemaj = "0">Sur la démarche de prévention, <Renvoi Type = "FP" Typemaj = "0" Produit = "GP95" Inline = "Oui"><Saisie></Saisie><Fiche Idref = "Q5F278" Partie = "pratique" Numfiche = "4" Nbr = "SING" fmcssattr = "7" Titre = "Démarche de prévention par étapes" Theme = "Prévention"></Fiche></Renvoi></Al>
<Al Typemaj = "0">Sur la préparation de l’évaluation des risques, <Renvoi Type = "FP" Typemaj = "0" Produit = "GP95" Inline = "Oui"><Saisie></Saisie><Fiche Idref = "Q5F022" Partie = "pratique" Numfiche = "11" Nbr = "SING" fmcssattr = "7" Titre = "Étape 1 : préparer la démarche" Theme = "Prévention"></Fiche></Renvoi></Al>
<Al Typemaj = "0">Sur l’évaluation des risques, <Renvoi Type = "FP" Typemaj = "0" Produit = "GP95" Inline = "Oui"><Saisie></Saisie><Fiche Idref = "Q5F216" Partie = "pratique" Numfiche = "12" Nbr = "SING" fmcssattr = "7" Titre = "Étape 2 : analyser et préparer l'évaluation" Theme = "Prévention"></Fiche></Renvoi></Al>
<Al Typemaj = "0">Sur la hiérarchisation des risques, <Renvoi Type = "FP" Typemaj = "0" Produit = "GP95" Inline = "Oui"><Saisie></Saisie><Fiche Idref = "Q5F217" Partie = "pratique" Numfiche = "13" Nbr = "SING" fmcssattr = "7" Titre = "Étape 3 : hiérarchiser et préparer l'action" Theme = "Prévention"></Fiche></Renvoi></Al>
<Al Typemaj = "0">Sur la mise en place d’actions de prévention, <Renvoi Type = "FP" Typemaj = "0" Produit = "GP95" Inline = "Oui"><Saisie></Saisie><Fiche Idref = "Q5F218" Partie = "pratique" Numfiche = "14" Nbr = "SING" fmcssattr = "7" Titre = "Étape 4 : mettre en place les actions de prévention" Theme = "Prévention"></Fiche></Renvoi></Al>
<Al Typemaj = "0">Sur le document unique, <Renvoi Type = "FP" Typemaj = "0" Produit = "GP95" Inline = "Oui"><Saisie></Saisie><Fiche Idref = "Q5F019" Partie = "pratique" Numfiche = "30" Nbr = "SING" fmcssattr = "7" Titre = "Établir le document unique d'évaluation des risques professionnels" Theme = "Prévention"></Fiche></Renvoi></Al>
<Al Typemaj = "0">Sur la surveillance médicale renforcée, <Renvoi Type = "FP" Typemaj = "0" Produit = "GP95" Inline = "Oui"><Saisie></Saisie><Fiche Idref = "Q5F158" Partie = "pratique" Numfiche = "39" Nbr = "SING" fmcssattr = "7" Titre = "Surveillance médicale des salariés" Theme = "Prévention"></Fiche></Renvoi></Al>
<Al Typemaj = "0">Pour un exemple de document unique, <Renvoi Type = "M" Typemaj = "0" Produit = "GP95" Inline = "Oui"><Saisie></Saisie><Fiche Idref = "Q5M172" Partie = "Modèle" Numfiche = "2" Nbr = "SING" fmcssattr = "7" Titre = "Document unique" Theme = "Prévention"></Fiche></Renvoi></Al>
<Al Typemaj = "0">Pour plus de précisions sur les rayonnements optiques, <Renvoiexterne><Nomdp DP = "DP12">voir Dictionnaire Permanent Sécurité et conditions de travail, </Nomdp><Tietudefiche Idref = "Y2083" Type = "Sans">étude Rayonnements optiques artificiels.</Tietudefiche></Renvoiexterne></Al>
<Al Typemaj = "0">Pour consulter la base de textes, <Renvoi Type = "Outils" Typemaj = "0" Produit = "GP95" Inline = "Oui"><Saisie></Saisie><Fiche Idref = "animation45" Partie = "outil" Nbr = "SING" fmcssattr = "7" Titre = "Base de textes en santé, sécurité au travail"></Fiche></Renvoi></Al></Entry>
<Entry></Entry>
</Row>
<Row>
<Entry></Entry>
<Entry></Entry>
<Entry></Entry>
</Row>
<Row>
<Entry></Entry>
<Entry></Entry>
<Entry></Entry>
</Row>
</Tbody>
</Tgroup></Table></OBS></FPRO>

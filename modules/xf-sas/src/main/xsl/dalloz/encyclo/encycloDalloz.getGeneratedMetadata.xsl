<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema" 
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
	xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
	xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
	xmlns:dalloz="http://www.lefebvre-sarrut.eu/ns/dalloz" 
	xmlns:h="http://www.w3.org/1999/xhtml" 
	xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
	xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" 
	exclude-result-prefixes="#all" 
	version="2.0">
  
  <xsl:param name="codePrefixMeta" select="'DZ_META_'" as="xs:string"/>
  <xsl:param name="codeModele" select="'rubrique'" as="xs:string"/>
  <xsl:param name="codeStructure" select="'encyclo'" as="xs:string"/>

	<!--==================================================-->
	<!-- MODE METADATA-->
	<!--==================================================-->

	<xd:doc scope="stylesheet">
		<xd:desc>
			<xd:p>XSLT pour gérer les métadonnées générées (1 seule fois dans le SAS et ensuite éditable dans l'ECM)</xd:p>
			<xd:p>A ne pas confondre avec les métadonnées calculées à partir du contenu (gérées dans sie-xf-ecm-resources), ici les éléments xml qui servent au métadonnées générées ne sont pas conservés. (ici on peut aussi déclarer des métadonnées "générées" qui ne sont pas liées au contenu xml, par exemple une date d'import)</xd:p>
		</xd:desc>
	</xd:doc>

	<!--==================-->
	<!-- -->
  <!--==================-->
	<xsl:template match="@id" mode="metadata"/>
  
  <xsl:template match="METADONNEES|METAS" mode="metadata">
    <xsl:apply-templates mode="metadata"/>
  </xsl:template>
  
  <xsl:template match="ISBN" mode="metadata"/>
  
  <xsl:template match="NIVEAU6/METAS/DATES" mode="metadata"/>
  
  <xsl:template match="DATES" mode="rubrique metadata">
    <xsl:apply-templates select="*" mode="#current"/>
  </xsl:template> 

  <!-- Les dates sont toutes au format AAAA-MM on les passe en AAAA-MM-01T00:00:00Z-->
  <xsl:template match="DATE-CREA" mode="metadata rubrique">
    <meta code="{concat($codePrefixMeta, $codeModele, 'DateCrea')}">
      <value>
        <xsl:value-of select="concat(., '-01T00:00:00Z')"/>
      </value>
    </meta>
  </xsl:template>  
  
  <xsl:template match="DATE-MAJ" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'DateMaj')}">
      <value>
        <xsl:value-of select="concat(., '-01T00:00:00Z')"/>
      </value>
    </meta>  
  </xsl:template>  
  
  <xsl:template match="DATE-MAJ" mode="rubrique">
    <meta code="{concat($codePrefixMeta, $codeModele, 'DateMaj')}">
      <value>
        <xsl:value-of select="concat(., '-01T00:00:00Z')"/>
      </value>
    </meta>  
    <meta code="{concat($codePrefixMeta, $codeModele, 'AnneeMaj')}">
      <value>
        <xsl:value-of select="substring(., 1, 4)"/>
      </value>
    </meta>  
  </xsl:template>
  
  <xsl:template match="DATE-PUBLI" mode="metadata rubrique">
    <meta code="{concat($codePrefixMeta, $codeModele, 'DatePubli')}">
      <value>
        <xsl:choose>
          <xsl:when test="matches(., '^\d\d\d\d-\d\d$')">
            <xsl:value-of select="concat(., '-01T00:00:00Z')"/>
          </xsl:when>
          <xsl:when test="matches(., '^(janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre)\s+(\d\d\d\d)$')">
            <xsl:analyze-string select="." regex="^(janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre)\s+(\d\d\d\d)$">
              <xsl:matching-substring>
                <xsl:value-of select="concat(regex-group(2), '-', dalloz:month-num(regex-group(1)), '-01T00:00:00Z')"/>
              </xsl:matching-substring>
            </xsl:analyze-string>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message terminate="yes">[ERROR] format de date incorrect pour la date de publication</xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </value>
    </meta>
  </xsl:template> 
  
  <xsl:template match="@DATE_ACTU" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeStructure, 'ParaDateActu')}">
      <value>
        <xsl:choose>
          <xsl:when test="matches(., '^\d\d\d\d-\d\d$')">
            <xsl:value-of select="concat(., '-01T00:00:00Z')"/>
          </xsl:when>
          <xsl:when test="matches(., '^(janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre)\s+(\d\d\d\d)$')">
            <xsl:analyze-string select="." regex="^(janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre)\s+(\d\d\d\d)$">
              <xsl:matching-substring>
                <xsl:value-of select="concat(regex-group(2), '-', dalloz:month-num(regex-group(1)), '-01T00:00:00Z')"/>
              </xsl:matching-substring>
            </xsl:analyze-string>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message terminate="yes">[ERROR] format de date incorrect pour la date de publication</xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </value>
    </meta>
  </xsl:template> 
  
  <xsl:function name="dalloz:month-num" as="xs:string">
    <xsl:param name="month-name" as="xs:string"/>
    <xsl:variable name="months" as="xs:string*" 
      select="'janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre'"/>
    <xsl:sequence select="format-number(index-of($months, $month-name), '00')"/>
  </xsl:function> 
  
  <xsl:template match="DATE-APPLI" mode="metadata rubrique"/>
  
  <xsl:template match="TITRE" mode="metadata">
    <xsl:choose>
      <xsl:when test="parent::METADONNEES/parent::OUVRAGE-DALLOZ"/>
      <xsl:otherwise>
        <xsl:apply-templates select="*" mode="metadata"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>  
  
  <xsl:template match="TIT-NUMERO" mode="metadata">
    <xsl:apply-templates select="*" mode="metadata"/>
  </xsl:template>  
  
  <xsl:template match="TIT-NIV" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeStructure, 'TitreNiv')}">
      <value><xsl:value-of select="."/></value>
    </meta>
  </xsl:template> 
  
  <xsl:template match="TIT-NUM" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeStructure, 'NumNiveau')}">
      <value><xsl:value-of select="."/></value>
    </meta>
  </xsl:template> 
  
  <xsl:template match="TIT-INTITULE" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeStructure, 'TitreInt')}">
      <value><xsl:value-of select="TIT-INTT"/></value>
    </meta>
  </xsl:template>
  
  <xsl:template match="TIT-INTITULE" mode="rubrique">
    <meta code="{concat($codePrefixMeta, $codeModele, 'Titre')}">
      <value><xsl:value-of select="TIT-INTT"/></value>
    </meta>
  </xsl:template>
  
  <xsl:template match="NUME" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeStructure, 'ParaNume')}">
      <value><xsl:value-of select="."/></value>
    </meta>
  </xsl:template>  
  
  <xsl:template match="NIVEAU6/NIVO-PRELIM/AUTEUR" mode="metadata"/>
  
  <xsl:template match="AUTEUR" mode="rubrique metadata">
    <xsl:apply-templates select="*" mode="metadata"/>
  </xsl:template> 
  
  <xsl:strip-space elements="AUTEUR-NOM-COMPLET"/>
  <xsl:template match="AUTEUR-NOM-COMPLET" mode="metadata">
    <xsl:if test="AUT-NOM|AUT-PRENOM">
      <xsl:apply-templates select="*" mode="metadata"/>
    </xsl:if>
    <meta code="{concat($codePrefixMeta, $codeModele, 'AuteurPrenomNom')}">
      <xsl:variable name="AuteurPrenomNom"><xsl:value-of select="*|text()" separator="&#160;"/></xsl:variable>
      <value><xsl:value-of select="normalize-space($AuteurPrenomNom)"/></value>
    </meta>
  </xsl:template>
  
  <xsl:template match="AUT-NOM" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'AuteurNom')}">
      <value><xsl:value-of select="."/></value>
    </meta>
  </xsl:template>
  
  <xsl:template match="AUT-PRENOM" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'AuteurPrenom')}">
      <value><xsl:value-of select="."/></value>
    </meta>
  </xsl:template>
  
  <xsl:template match="AUTEUR-QUALITES" mode="metadata"/>
  <xsl:template match="AUT-QUALITE" mode="metadata"/>
  <xsl:template match="AUTEUR-COMPL" mode="metadata"/>
  
  <xsl:template match="NIVEAU6/METAS/THEMES" mode="rubrique">
    <meta code="{concat($codePrefixMeta, $codeModele, 'ThemeRepert')}">
      <xsl:apply-templates select="THM-NIV1/THM-LIB" mode="metadata"/>
    </meta>
  </xsl:template>
  
  <xsl:template match="THEMATISATION" mode="rubrique"/>
  
  <xsl:template match="THEMES|THEMATISATION" mode="metadata"/>
  
  <xsl:template match="THM-LIB" mode="metadata">
    <value><xsl:value-of select="."/></value>
  </xsl:template>
  
  <xsl:template match="THM-LIB" mode="metadata1">
    <xsl:value-of select="."/>
  </xsl:template>
  
  <xsl:template match="INDX" mode="metadata" />

	<xsl:template match="*" mode="metadata" priority="-1">
		<xsl:message>[ERROR] élément non matché en mode "metadata" : <xsl:value-of select="local-name(.)"/></xsl:message>
	</xsl:template>
</xsl:stylesheet>

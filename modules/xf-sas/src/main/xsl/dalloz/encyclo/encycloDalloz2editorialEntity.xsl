﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:dalloz="http://www.lefebvre-sarrut.eu/ns/dalloz" 
  xmlns:h="http://www.w3.org/1999/xhtml" 
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"  
  exclude-result-prefixes="#all" 
  version="2.0">
  
  <xsl:import href="../../xf-sas-common.xsl"/>
  <xsl:import href="encycloDalloz2editorialEntity.makeRelations.xsl"/>
  <xsl:import href="encycloDalloz.getGeneratedMetadata.xsl"/>
  
  <xsl:output method="xml" indent="no"/>
  
  <xsl:param name="rel.to.schema" select="'xf-models:/editorialEntity'" as="xs:string"/>
  <xsl:param name="xfSAS:company" select="'dalloz'" as="xs:string"/>
  <xsl:param name="outDir" select="base-uri()" as="xs:string"/>

  <xsl:param name="codeModele" select="'rubrique'" as="xs:string"/>
  <xsl:param name="codeStructure" select="'encyclo'" as="xs:string"/>
  <xsl:param name="codeFeuille" select="'paragraphe'" as="xs:string"/>
  
  
  <xsl:variable name="codePrefixTee" select="'DZ_TEE_'" as="xs:string"/>
  <xsl:variable name="codePrefixTr" select="'DZ_TR_'" as="xs:string"/>
  <xsl:variable name="codePrefixMeta" select="'DZ_META_'" as="xs:string"/>
  <xsl:variable name="contentNode.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/dalloz/encyclopedies'" as="xs:string"/>
  
  <xsl:template match="/">
    <xsl:call-template name="xfSAS:makeXmlModelPi"/>
    
    <xsl:variable name="step0-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates/>
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step0-xml2ee.log.xml">
      <xsl:sequence select="$step0-xml2ee"/>
    </xsl:result-document>
    
    <xsl:variable name="step1-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step0-xml2ee" mode="fix-paras"/>
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step1-xml2ee.log.xml">
      <xsl:sequence select="$step1-xml2ee"/>
    </xsl:result-document>
    
    <xsl:variable name="step2-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step1-xml2ee" mode="structuralNode"/>
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step2-xml2ee.log.xml">
      <xsl:sequence select="$step2-xml2ee"/>
    </xsl:result-document>
    
    <!-- Normalisation des méta -->
    <xsl:variable name="step3-fixMeta" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step2-xml2ee" mode="xfSAS:fixMeta"/>        
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step3-fixMeta.log.xml">
      <xsl:sequence select="$step3-fixMeta"/>
    </xsl:result-document>
    
    <!-- Gestion des relations --> 
    <xsl:variable name="step4-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step3-fixMeta" mode="makeRelations"/>
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step4-xml2ee.log.xml">
      <xsl:sequence select="$step4-xml2ee"/>
    </xsl:result-document>
    
    <xsl:variable name="step5-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step4-xml2ee" mode="fixIds"/>
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step5-xml2ee.log.xml">
      <xsl:sequence select="$step5-xml2ee"/>
    </xsl:result-document>
    <xsl:sequence select="$step5-xml2ee"/>
  </xsl:template>
  
  <xsl:variable name="idRoot" select="/OUVRAGE-DALLOZ/CORPUS/NIVEAU6/@ID" as="xs:string?"/>
  
  <xsl:template match="@ID">
    <xsl:attribute name="id" select="."/>
  </xsl:template>
  
  <xsl:template match="@REFID">
    <xsl:attribute name="idRef" select="."/>
  </xsl:template>
  
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="TABLEAU[parent::NIV-TEXTE]" mode="fix-paras">
    <PARA xmlns="" id="{dalloz:Id(., $idRoot)}">
      <PARA-TEXT>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()" mode="#current"/>
        </xsl:copy>
      </PARA-TEXT>
    </PARA>
  </xsl:template>
  
  <xsl:template match="NIVO-CORPS/NIV-TEXTE/PARA[1][not(@id)]" mode="fix-paras" priority="1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="PARA[not(@id)]|PARA-LEGIS[not(@id)]|PARA-JURIS[not(@id)]|PARA-QUEST[not(@id)]|PARA-BIBLIO[not(@id)]" mode="fix-paras">
    <xsl:copy>
      <xsl:attribute name="id" select="dalloz:Id(., $idRoot)"/>
      <xsl:apply-templates select="@*|node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="REF" mode="fix-paras">
    <xsl:copy>
      <xsl:attribute name="TYPE" select="upper-case(@TYPE)"/>
      <xsl:apply-templates select="@* except @TYPE" mode="#current"/>
      <xsl:apply-templates select="node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="@*|node()" mode="fix-paras">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--===============================-->
  <!--STRUCTURAL NODES -->
  <!--===============================-->
  <!-- Document Root -->
  <xsl:template match="OUVRAGE-DALLOZ" mode="structuralNode">
    <xsl:apply-templates select="CORPUS/NIVEAU6" mode="structuralNode"/>
  </xsl:template>
    
  <xsl:template match="NIVEAU6" mode="structuralNode">
    <editorialEntity 
      xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
      xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst">
      <xsl:attribute name="xf:id" select="dalloz:HistoricalId(.)"/>
      <xsl:choose>
        <xsl:when test="$codeModele = 'ouvrages'">
          <xsl:attribute name="code" select="concat($codePrefixTee, $codeModele)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="code" select="concat($codePrefixTee, $codeStructure, dalloz:ucfirst($codeModele))"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:call-template name="xfSAS:addAttributeOwnerGroup"/>
      <metadata>
        <xsl:copy-of select="dalloz:MakeMetaOrigine(.)"/>
        <xsl:if test="@id">
          <meta code="{concat($codePrefixMeta, $codeModele, 'ID')}">
            <value><xsl:value-of select="@id"/></value>
          </meta> 
        </xsl:if>
          <xsl:if test="TITRE">
            <xsl:apply-templates select="TITRE" mode="rubrique"/>
          </xsl:if>
          <xsl:if test="NIVO-PRELIM/AUTEUR">
            <xsl:apply-templates select="NIVO-PRELIM/AUTEUR" mode="rubrique"/>
          </xsl:if>
          <xsl:if test="METAS/DATES">
            <xsl:apply-templates select="METAS/DATES" mode="rubrique"/>
          </xsl:if>
        <xsl:if test="METAS/THEMES">
          <xsl:apply-templates select="METAS/THEMES" mode="rubrique"/>
        </xsl:if>
          <xsl:if test="METAS/THEMATISATION">
            <xsl:apply-templates select="METAS/THEMATISATION" mode="rubrique"/>
          </xsl:if>
        <meta code="{concat($codePrefixMeta, $codeModele, 'Type')}">
          <value><xsl:value-of select="'RUB'"/></value>
        </meta>
      </metadata>
      <body>
        <xsl:apply-templates select="* except METAS" mode="structuralNode"/>
      </body>
    </editorialEntity>
  </xsl:template>
  
  <!-- Document Leaf -->
  <xsl:template match="PARA|PARA-LEGIS|PARA-JURIS|PARA-QUEST|PARA-BIBLIO" mode="structuralNode">
        <referenceNode code="DZ_TR_accrochage_Encyclo_EncycloPara">
          <ref xf:targetResId="{dalloz:HistoricalId(.)}" xf:refType="xf:referenceNode">
        <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
          xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
          code="{concat($codePrefixTee, $codeStructure, 'Paragraphe')}">
          <xsl:call-template name="xfSAS:addAttributeOwnerGroup"/> 
          <xsl:attribute name="xf:id" select="dalloz:HistoricalId(.)"/>
          <metadata>
            <xsl:copy-of select="dalloz:MakeMetaOrigine(.)"/>
            <xsl:if test="@id">
              <meta code="{concat($codePrefixMeta, $codeStructure, 'ParaID')}">
                <value><xsl:value-of select="@id"/></value>
              </meta>
            </xsl:if>
            <xsl:apply-templates select="@*" mode="metadata"/>
            <xsl:apply-templates select="NUME" mode="metadata"/>
            <xsl:apply-templates select="METAS" mode="metadata"/>
          </metadata>
          <body>
            <xsl:apply-templates select="." mode="contentNode"/>
          </body>
        </editorialEntity>
      </ref>
    </referenceNode>
  </xsl:template>
  
  <!-- skip the element and process the children -->
  <xsl:template match="NIVO-CORPS | NIV-TEXTE[PARA|PARA-LEGIS|PARA-JURIS|PARA-QUEST|PARA-BIBLIO]" mode="structuralNode">
    <xsl:apply-templates select="* except METAS" mode="structuralNode"/>
  </xsl:template>
  
  <!-- skip the element and  the children -->
  <xsl:template match="NIV-TEXTE | NIVO-PRELIM | NIVO-COMPL" mode="structuralNode"/>
  <!-- Mini TDM trouvées dans le Delmas (cette structure n'apparait pas dans les autres exemples -->
  <xsl:template match="NIVO-CORPS/NIV-TEXTE/PARA[1][not(@id)]" mode="structuralNode"/>
 
  
  <!-- TBD -->
  <xsl:template match="TITRE | PGTITRE | INDEX" mode="structuralNode"/>
  
  <!-- Document Branches -->
  <xsl:template match="*" mode="structuralNode">
    <!-- check for Oops! -->
    <xsl:if test="not(self::OUVRAGE-DALLOZ
      |self::PRELIMINAIRES|self::CORPUS|self::COMPLEMENTS
      |self::ANNEXES|self::ANNEXE
      |self::NIVEAU1|self::NIVEAU2|self::NIVEAU3|self::NIVEAU4|self::NIVEAU5|self::NIVEAU6
      |self::INTRO|self::NIV1|self::NIV2|self::NIV3|self::NIV4|self::NIV5|self::NIV6
      |self::NIV7|self::NIV8|self::NIV9|self::NIV10|self::NIV11|self::NIV12|self::CONCL)">
      <xsl:message>Structural Mode Called on <xsl:value-of select="local-name(.)"></xsl:value-of></xsl:message>
    </xsl:if>
    <structuralNode>
      <xsl:attribute name="xf:id" select="dalloz:HistoricalId(.)"/>
      <metadata>
        <xsl:copy-of select="dalloz:MakeMetaOrigine(.)"/>
        <xsl:apply-templates select="@*" mode="metadata"/>
        <xsl:apply-templates select="METAS" mode="metadata"/>
        <xsl:apply-templates select="TITRE" mode="metadata"/>
        <xsl:apply-templates select="NIVO-PRELIM/AUTEUR" mode="metadata"/>
      </metadata>
      <xsl:choose>
        <xsl:when test="self::NIVEAU6">
          <xsl:copy-of select="dalloz:MakeStructuralNodeTitleEx(.)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:copy-of select="dalloz:MakeStructuralNodeTitle(.)"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="* except METAS" mode="structuralNode"/>
    </structuralNode>
  </xsl:template>
  
  <xsl:template match="text()" mode="structuralNode"/>
  
  <xsl:template match="PARA|PARA-LEGIS|PARA-JURIS|PARA-QUEST|PARA-BIBLIO" mode="contentNode">
    <contentNode>
      <content>
        <xsl:apply-templates select="." mode="convertNamespace">
          <xsl:with-param name="namespace.uri" select="$contentNode.namespace.uri" tunnel="yes"/>
        </xsl:apply-templates>
      </content>
    </contentNode>
  </xsl:template>
  
  <xsl:template match="*" mode="contentNode">
    <xsl:message>Content Mode Called on <xsl:value-of select="local-name(.)"></xsl:value-of></xsl:message>
  </xsl:template>
  
  <xsl:template match="FOOTNOTE" mode="xfRes:title2html"/>
  
  <!--==================================================-->
  <!--MODE convertNamespace -->
  <!--==================================================-->
  
  <xsl:template match="METAS" mode="convertNamespace"/>
  
  <xsl:template match="TABLE-CALS" mode="convertNamespace">
    <xsl:param name="namespace.uri" required="yes" tunnel="yes"/>
    <xsl:element name="table" namespace="{$namespace.uri}">
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="*" mode="convertNamespace" priority="-1">
    <xsl:param name="namespace.uri" required="yes" tunnel="yes"/>
    <xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="convertNamespace" priority="-2">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="@id" mode="fixIds">
    <xsl:attribute name="id" select="replace(., '/', '_slash_')"/>
  </xsl:template>
  
  <xsl:template match="@idRef" mode="fixIds">
    <xsl:attribute name="idRef" select="replace(., '/', '_slash_')"/>
  </xsl:template>
  
  <xsl:template match="@xf:idRef" mode="fixIds">
    <xsl:attribute name="xf:idRef" select="replace(., '/', '_slash_')"/>
  </xsl:template>
  
  <xsl:template match="@*|node()" mode="fixIds">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  
  <!--==================================================-->
  <!--COMMON"-->
  <!--==================================================-->
  
  <xsl:function name="dalloz:ucfirst" as="xs:string">
    <xsl:param name="string"/>
    <xsl:copy-of select="concat(upper-case(substring($string, 1, 1)), lower-case(substring($string, 2)))"/>
  </xsl:function>
  
  <xsl:function name="dalloz:makeMetaCode" as="xs:string">
    <xsl:param name="codePrefix"/>
    <xsl:param name="codeModele"/>
    <xsl:param name="codeMeta"/>
    <xsl:copy-of select="concat($codePrefix, $codeModele, dalloz:ucfirst($codeMeta))"/>
  </xsl:function>
  
  <xsl:function name="dalloz:Id" as="xs:string">
    <xsl:param name="context" as="element()"/>
    <xsl:param name="idRoot" as="xs:string?"/>
    <xsl:choose>
      <xsl:when test="not(empty($context/@id))">
        <xsl:value-of select="$context/@id"/>
      </xsl:when>
      <xsl:when test="$context/ancestor::*/@id">
        <xsl:variable name="ancestor-with-id" select="($context/ancestor::*[@id])[1]"/>
        <xsl:variable name="xpath" select="els:get-xpath($context)"/>
        <xsl:variable name="ancestor-xpath" select="els:get-xpath($ancestor-with-id)"/>
        <xsl:value-of select="concat($ancestor-with-id/@id, translate(translate(substring-after($xpath, $ancestor-xpath), '[', '-'), ']', ''))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>[ERROR] Structure du document fausse!</xsl:message>
        <xsl:message terminate="yes">[ERROR] Element <xsl:value-of select="local-name($context)"/> sans ID ni ancetre portant un ID.</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="dalloz:HistoricalId" as="xs:string">
    <xsl:param name="context" as="element()"/>
    <xsl:value-of select="xfSAS:makeHistoricIdAttribute($context, dalloz:Id($context, $idRoot))"/>
  </xsl:function>
  
  <xsl:function name="dalloz:MakeStructuralNodeTitle" as="element(xf:title)">
    <xsl:param name="context" as="element()"/>
    <title>
      <div xmlns="http://www.w3.org/1999/xhtml">
        <xsl:choose>
          <xsl:when test="$context/TITRE">
            <xsl:for-each select="$context/TITRE//(TIT-NIV|TIT-NUM), $context/TITRE/TIT-INTITULE/(TIT-INTT|TIT-COMPL)">
              <xsl:if test="position() &gt; 1">
                <xsl:choose>
                  <xsl:when test="self::TIT-INTT or self::TIT-COMPL[not(preceding-sibling::TIT-INTT)]">
                    <xsl:value-of select="'&#160;-&#160;'"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="'&#160;'"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
              <!-- <span class="{local-name(.)}"><xsl:apply-templates select="." mode="xfe:xml2html_main"/></span> -->
              <span class="{local-name(.)}"><xsl:apply-templates mode="xfRes:title2html"/></span>
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            <span><xsl:value-of select="upper-case(local-name($context))"/></span>
          </xsl:otherwise>
        </xsl:choose>
      </div>
    </title>
  </xsl:function>
  
  <xsl:function name="dalloz:MakeStructuralNodeTitleEx" as="element(xf:title)">
    <xsl:param name="context" as="element()"/>
    <title>
      <div xmlns="http://www.w3.org/1999/xhtml">
        <xsl:choose>
          <xsl:when test="$context/TITRE">
            <xsl:for-each select="$context/TITRE//(TIT-NIV|TIT-NUM), $context/TITRE/TIT-INTITULE/TIT-INTT">
              <xsl:if test="position() &gt; 1">
                <xsl:choose>
                  <xsl:when test="self::TIT-INTT">
                    <xsl:value-of select="'&#160;-&#160;'"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="'&#160;'"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
              <!-- <span class="{local-name(.)}"><xsl:apply-templates select="." mode="xfe:xml2html_main"/></span> -->
              <span class="{local-name(.)}"><xsl:apply-templates mode="xfRes:title2html"/></span>
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            <span><xsl:value-of select="upper-case(local-name($context))"/></span>
          </xsl:otherwise>
        </xsl:choose>
      </div>
    </title>
  </xsl:function>
  
  <xsl:function name="dalloz:MakeMetaOrigine" as="element(xf:meta)">
    <xsl:param name="context" as="element()"/>
    <meta code="{dalloz:makeMetaCode($codePrefixMeta, $codeStructure, 'origin')}">
      <value><xsl:value-of select="local-name($context)"/></value>
    </meta>
  </xsl:function>
  
  <xsl:template name="xfSAS:addAttributeOwnerGroup"> 
    <xsl:attribute name="ownerGroup" select="'DZ_GRP_encyclopedie'"/>
  </xsl:template>
  
</xsl:stylesheet>

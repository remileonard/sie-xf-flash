<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:functx="http://www.functx.com" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" 
  xmlns:dalloz="http://www.lefebvre-sarrut.eu/ns/dalloz" 
  xmlns:encyclo="http://www.lefebvre-sarrut.eu/ns/dalloz/encyclopedies" 
  xmlns:h="http://www.w3.org/1999/xhtml" 
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  exclude-result-prefixes="#all" version="2.0">

  <xsl:import href="../../xf-sas-common.xsl"/>
	
	<xsl:param name="codePrefixMeta" select="'DZ_META_'" as="xs:string"/>
  
  <xsl:key name="idkey" match="*[@id]" use="@id"/>

  <xsl:template match="editorialEntity/body" mode="makeRelations">
		<xsl:next-match/>
		<relations>
		  <xsl:apply-templates select="contentNode/content//encyclo:REF[@idRef]" mode="makeRelationElement"/>
		</relations>
	</xsl:template>

	<!--=====================================-->
  <!-- Encyclopedies / Codes : REF-->
	<!--=====================================-->

  <xsl:template match="encyclo:REF[@idRef]" mode="makeRelationElement">
		<xsl:call-template name="makeRelationElement">
		  <xsl:with-param name="element" select="." as="element(encyclo:REF)"/>
		  <xsl:with-param name="type" select="@TYPE" as="xs:string?"/>
			<xsl:with-param name="refid" select="@idRef" as="xs:string"/>
		</xsl:call-template>
	</xsl:template>
  
	<xsl:template name="makeRelationElement">
		<xsl:param name="element" as="element()"/>
		<xsl:param name="type" as="xs:string?"/>
	  <xsl:param name="refid" as="xs:string"/>
	  
		<xsl:choose>
		  <xsl:when test="upper-case($type) = ('INTRA','INTER', 'EXTRA')">
		    <xsl:variable name="referedPara" select="key('idkey', $refid)"/>
		    <xsl:choose>
		      <xsl:when test="$referedPara and (generate-id($element/ancestor::content[1]) = generate-id($referedPara/ancestor::content[1]))"/>
		      <xsl:otherwise>
		        <relation xf:id="{generate-id($element)}" code="DZ_TR_renvoi_GrpEncyclo_GrpEncyclo" calculated="'true'">
		          <metadata/>
		          <xsl:choose>
		            <xsl:when test="$referedPara and ($element/ancestor::editorialEntity[1]/@xf:id = $referedPara/ancestor::editorialEntity[1]/@xf:id)">
		              <ref 
		                xf:targetResType="editorialEntity"
		                xf:idRef="{$refid}" 
		                xf:refType="referenceItemRef"/>
		            </xsl:when>
		            <xsl:otherwise>
		              <xsl:choose>
		                <xsl:when test="$referedPara">
		                  <ref xf:targetResType="editorialEntity"
		                    xf:targetResId="{$referedPara/ancestor::editorialEntity[1]/@xf:id}"
		                    xf:refType="referenceItemRef"/>
		                </xsl:when>
		                <xsl:otherwise>
		                  <ref xf:targetResType="editorialEntity"
		                    xf:targetResId="{xfSAS:makeHistoricIdAttribute($element, $refid)}" 
		                    xf:refType="referenceItemRef"/>
		                </xsl:otherwise>
		              </xsl:choose>
		            </xsl:otherwise>
		          </xsl:choose>
		        </relation>
		      </xsl:otherwise>
		    </xsl:choose>
		  </xsl:when>
		  <xsl:when test="$type = ('JUR', 'REV', 'BROC', 'LEG')">
		    <relation xf:id="{generate-id($element)}" code="DZ_TR_renvoi_GrpEncyclo_GrpEncyclo" calculated="'true'">
		      <metadata/>
		    <ref xf:targetResType="editorialEntity"
		      xf:targetResId="{xfSAS:makeHistoricIdAttribute($element, $refid)}" 
		      xf:refType="referenceItemRef"/>
		    </relation>
		  </xsl:when>
		  <xsl:when test="$type = 'ENCY'">
		    <xsl:message>Unexpected reference @type ENCY found. It should be INTRA INTER or EXTRA.</xsl:message>
		    <xsl:message>Please correct the source document.</xsl:message>
		    <xsl:message terminate="yes">Exiting program</xsl:message>
		  </xsl:when>
		  <!-- certains ne figurent peut être ici que pour des raisons théoriques
	      <xsl:when test="$type = ('LEX', 'FORM', 'FICH')"/>
	    -->
		  <xsl:otherwise>
		    <xsl:message>Unexpected reference @type found : <xsl:value-of select="$type"/> !</xsl:message>
		    <xsl:message>Please adapt the sas conversuion code for this new value.</xsl:message>
		    <xsl:message terminate="yes">Exiting program</xsl:message>
		  </xsl:otherwise>
		</xsl:choose>
	</xsl:template>
  
  <!--=====================================-->
  <!--makeRelations-->
  <!--=====================================-->

  <xsl:template match="contentNode/content//encyclo:REF[@idRef]" mode="makeRelations">
    <xsl:copy>
      <xsl:choose>
        <xsl:when test="upper-case(@TYPE) = ('INTRA','INTER', 'EXTRA')">
          <!-- lien sur une para de la meme rubrique -->
          <xsl:variable name="referedPara" select="key('idkey', @idRef)"/>
          <xsl:choose>
            <xsl:when test="generate-id(ancestor::content[1]) = generate-id($referedPara/ancestor::content[1])">
              <xsl:attribute name="idRef" select="replace(@idRef, '/', '_slash_')"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="xf:idRef" select="generate-id(.)"/>
              <xsl:attribute name="xf:refType" select="'xf:relation'"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <!-- lien d'un autre type que INTRA, INTER ou EXTRA -->
        <xsl:otherwise>
          <xsl:attribute name="xf:idRef" select="generate-id(.)"/>
          <xsl:attribute name="xf:refType" select="'xf:relation'"/>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="@* except @idRef" mode="#current"/>
      <xsl:apply-templates select="node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template name="makeRelation">
    <!-- pas traite pour le moment et ne devrait pas survenir-->
    <xsl:param name="element" as="element()"/>
    <xsl:copy-of select="."/>
	</xsl:template>
  
	<!--=====================================-->
	<!--COPY-->
	<!--=====================================-->

	<xsl:template match=" node() | @*" mode="makeRelations">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" mode="#current"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>

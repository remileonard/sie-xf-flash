<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" xmlns:revues="http://www.lefebvre-sarrut.eu/ns/dalloz/revues" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" exclude-result-prefixes="#all" version="2.0">

    <xsl:include href="xslLib:/xslLib/els-common.xsl"/>

    <xsl:output method="xml" indent="no" encoding="UTF-8" doctype-public="DALLOZ//DTD REVUES//FR" doctype-system="file:/C:/DTD Dalloz/Revues/REVUES.DTD"/>

    <xsl:variable name="referenceDallozRevues" select="doc('../../../../test/samples/dalloz/revues/referentials/referenceTable-dalloz_revues.xml')" as="document-node()"/>
	<xsl:variable name="codePrefixTee" select="'DZ_TEE_revues'" as="xs:string"/>
    <xsl:variable name="isoDateFormat" select="'^(\p{Nd}{4}-\p{Nd}{2}-\p{Nd}{2})(T00:00:00Z)?'"/>

    <xsl:template match="/">
        <xsl:apply-templates/>
    </xsl:template>

    <!-- 1ere EE : CAHIER -->
    <xsl:template match="xf:editorialEntity[@code = $referenceDallozRevues//referenceItem/concat($codePrefixTee, @id)]">
        <xsl:variable name="id" select="tokenize(@xf:id, '_')[2]" as="xs:string"/>
        <!-- Reconstruir la racine REVUE -->
        <REVUE ID="{$id}">
            <TITRE-REVUE>
                <xsl:value-of select="$referenceDallozRevues//referenceItem[@id = $id]/label[@type = 'long']"/>
            </TITRE-REVUE>
            <CAHIER ID="{xf:getMetaValue(., 'DZ_META_revuesCahierId')}">
                <ANNEE>
                    <xsl:value-of select="xf:getMetaValue(., 'DZ_META_revuesCahierAnnee')"/>
                </ANNEE>
                <DATE-CAHIER>
                    <xsl:variable name="date" select="xf:getMetaValue(., 'DZ_META_revuesCahierDate')" as="xs:string*"/>
                    <xsl:analyze-string select="$date" regex="{$isoDateFormat}">
                        <xsl:matching-substring>
                            <xsl:variable name="dateSequence" select="tokenize(regex-group(1), '-')" as="xs:string+"/>
                            <xsl:value-of select="concat($dateSequence[3], '-', $dateSequence[2], '-', $dateSequence[1])"/>
                        </xsl:matching-substring>
                        <xsl:non-matching-substring>
                            <xsl:value-of select="$date"/>
                        </xsl:non-matching-substring>
                    </xsl:analyze-string>                    
                </DATE-CAHIER>
                <NUM-CAHIER>
                    <NUM>
                        <xsl:value-of select="xf:getMetaValue(., 'DZ_META_revuesCahierNum')"/>
                    </NUM>
                </NUM-CAHIER>
                <xsl:apply-templates select="xf:body/xf:referenceNode/xf:ref/xf:editorialEntity/xf:body/xf:contentNode/xf:content/*"/>
            </CAHIER>
        </REVUE>
    </xsl:template>

    <xsl:template match="xf:content/*">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates select="*"/>
            <xsl:apply-templates select="../../../xf:contentNode/following-sibling::xf:referenceNode/xf:ref/xf:editorialEntity/xf:body/xf:contentNode/xf:content/*"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="revues:NAVIGATION">
        <xsl:element name="{local-name()}">
            <xsl:for-each select="*">
                <xsl:attribute name="{local-name(.)}" select="@REFID"/>
            </xsl:for-each>
        </xsl:element>
    </xsl:template>

    <xsl:function name="xf:getMetaValue">
        <xsl:param name="node" as="element()"/>
        <xsl:param name="code" as="xs:string"/>
        <xsl:sequence select="$node/xf:metadata/xf:meta[@code = $code]/xf:value"/>
    </xsl:function>

    <!--===============================-->
    <!-- Enlever namespace
         element: supprimer le namespace de nom d'élément
         attribut: supprimer l'attribut ayant namespace -->
    <!--===============================-->
    <xsl:template match="*">
        <xsl:element name="{local-name()}">
            <xsl:copy-of select="@*[name() = local-name()]"/>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
</xsl:stylesheet>

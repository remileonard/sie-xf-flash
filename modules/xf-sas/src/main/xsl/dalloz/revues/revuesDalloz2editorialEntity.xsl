<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" xmlns:dalloz="http://www.lefebvre-sarrut.eu/ns/dalloz" xmlns:h="http://www.w3.org/1999/xhtml" xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" exclude-result-prefixes="#all" version="2.0">

  <xsl:import href="../../xf-sas-common.xsl"/>
  <xsl:include href="revuesDalloz2editorialEntity.makeRelations.xsl"/>
  <xsl:include href="revuesDalloz.getGeneratedMetadata.xsl"/>

  <xsl:output method="xml" indent="no"/>

  <xsl:param name="rel.to.schema" select="'xf-models:/editorialEntity'" as="xs:string"/>
  <xsl:param name="xfSAS:company" select="'dalloz'" as="xs:string"/>
  <xsl:param name="outDir" select="base-uri()" as="xs:string"/>

  <xsl:variable name="contentNode.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/dalloz/revues'" as="xs:string"/>
  <xsl:variable name="codePrefixTee" select="'DZ_TEE_revues'" as="xs:string"/>
  <xsl:variable name="codePrefixTr" select="'DZ_TR_accrochage'" as="xs:string"/>

  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>

  <!--===============================-->
  <!--REVUE-->
  <!--===============================-->

  <xsl:template match="/REVUE">
    <!--N'est pas une EE : c'est le site web qui agrège tous les cahier par année et appelle ça une "revue"-->
    <xsl:choose>
      <xsl:when test="count(CAHIER) = 1">
        <xsl:call-template name="xfSAS:makeXmlModelPi"/>
        <xsl:variable name="step1-xml2ee" as="document-node()">
          <xsl:document>
            <xsl:apply-templates/>
          </xsl:document>
        </xsl:variable>
        <xsl:result-document href="log/step1-xml2ee.log.xml">
          <xsl:sequence select="$step1-xml2ee"/>
        </xsl:result-document>
        <!--Normalisation des méta-->
        <xsl:variable name="step2-fixMeta" as="document-node()">
          <xsl:document>
            <xsl:apply-templates select="$step1-xml2ee" mode="xfSAS:fixMeta"/>
          </xsl:document>
        </xsl:variable>
        <xsl:result-document href="log/step2-fixMeta.log.xml">
          <xsl:sequence select="$step2-fixMeta"/>
        </xsl:result-document>
        <!--Add verbalization for referenceNode-->
        <xsl:variable name="step3-addVerbalization" as="document-node()">
          <xsl:document>
            <xsl:apply-templates select="$step2-fixMeta" mode="xfSAS:addVerbalization"/>
          </xsl:document>
        </xsl:variable>
        <!-- Gestion des relations -->
        <xsl:apply-templates select="$step3-addVerbalization" mode="makeRelations"/>
      </xsl:when>
      <xsl:when test="count(CAHIER) > 1">
        <xsl:for-each-group select="CAHIER" group-by="@ID">
          <xsl:variable name="revue" as="element()">
            <xsl:element name="{current-group()[1]/parent::node()/local-name()}" namespace="">
              <xsl:attribute name="ID" select="current-group()[1]/parent::node()/@ID"/>
              <xsl:copy-of select="current-group()[1]/parent::node()/TITRE-REVUE"/>
              <xsl:element name="{local-name()}" namespace="">
                <xsl:copy-of select="current-group()[1]/@*"/>
                <xsl:copy-of select="current-group()[1]/node()"/>
                <xsl:copy-of select="current-group()[position() > 1]/DOSSIER | current-group()[position() > 1]/ARTICLE"/>
              </xsl:element>
            </xsl:element>
          </xsl:variable>
          <xsl:variable name="step1-xml2ee" as="document-node()">
            <xsl:document>
              <xsl:apply-templates select="$revue/CAHIER"/>
            </xsl:document>
          </xsl:variable>
          <xsl:result-document href="log/step1-xml2ee.log{position()}.xml">
            <xsl:sequence select="$step1-xml2ee"/>
          </xsl:result-document>
          <xsl:variable name="step2-fixMeta" as="document-node()">
            <xsl:document>
              <xsl:apply-templates select="$step1-xml2ee" mode="xfSAS:fixMeta"/>
            </xsl:document>
          </xsl:variable>
          <!--Add verbalization for referenceNode-->
          <xsl:variable name="step3-addVerbalization" as="document-node()">
            <xsl:document>
              <xsl:apply-templates select="$step2-fixMeta" mode="xfSAS:addVerbalization"/>
            </xsl:document>
          </xsl:variable>
          <!--FIXME : répertoire pour les fichiers de sortie -->
          <xsl:variable name="filename" select="concat($outDir, '/', string-join(tokenize(tokenize(base-uri(), '/')[last()], '\.')[position() != last()], '.'))"/>
          <!-- Gestion des relations -->
          <xsl:result-document href="{$filename}_{position()}.xml">
            <xsl:call-template name="xfSAS:makeXmlModelPi"/>
            <xsl:apply-templates select="$step3-addVerbalization" mode="makeRelations"/>
          </xsl:result-document>
        </xsl:for-each-group>
      </xsl:when>
    </xsl:choose>
  </xsl:template>

  <xsl:template match="TITRE-REVUE">
    <!--on le récupérera dans chaque EE CAHIER-->
  </xsl:template>

  <!--===============================-->
  <!--CAHIER-->
  <!--===============================-->

  <xsl:template match="CAHIER">
    <xsl:variable name="code" select="concat($codePrefixTee, ../self::REVUE/@ID)" as="xs:string"/>
    <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{xfSAS:makeHistoricIdAttribute(., dalloz:normalizeId(@ID))}" code="{$code}">
      <xsl:attribute name="ownerGroup" select="xfSAS:addAttributeOwnerGroup($code)"/>
      <xsl:namespace name="xf" select="'http://www.lefebvre-sarrut.eu/ns/xmlfirst'"/>
      <xsl:apply-templates select="." mode="metadata"/>
      <body>
        <xsl:apply-templates select="DOSSIER | ARTICLE" mode="structuralNode"/>
      </body>
    </editorialEntity>
  </xsl:template>

  <!--===============================-->
  <!--DOSSIER-->
  <!--===============================-->

  <xsl:template match="DOSSIER" mode="structuralNode">
    <referenceNode code="{$codePrefixTr}_Revue{ancestor::REVUE/@ID}_GrpDossierArticle">
      <xsl:attribute name="num">
        <xsl:number level="any" count="DOSSIER" from="/"/>
      </xsl:attribute>
      <ref xf:targetResId="{xfSAS:makeHistoricIdAttribute(., dalloz:normalizeId(@ID))}" xf:refType="xf:referenceNode">
        <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{xfSAS:makeHistoricIdAttribute(., dalloz:normalizeId(@ID))}" code="{$codePrefixTee}Dossier">
          <xsl:attribute name="ownerGroup" select="xfSAS:addAttributeOwnerGroup(concat($codePrefixTee, ancestor::REVUE/@ID))"/>
          <!--<xsl:if test="@ETAT">
            <xsl:attribute name="status" select="xfSAS:addAttributeStatus(@ETAT)"/>
          </xsl:if>-->
          <xsl:apply-templates select="." mode="metadata"/>
          <body>
            <!--Dossier est une EE, il contient 1 referenceNode (contenant des sous-EE pour les ARTICLES) et 1 contentNode (pour le contenu introductif) -->
            <xsl:apply-templates select="." mode="contentNode"/>
            <xsl:apply-templates select="ARTICLE" mode="structuralNode"/>
          </body>
        </editorialEntity>
      </ref>
    </referenceNode>
  </xsl:template>

  <xsl:template match="DOSSIER" mode="contentNode">
    <xsl:variable name="self" select="." as="element()"/>
    <xsl:for-each-group select="*" group-adjacent="exists(self::ARTICLE)">
      <xsl:choose>
        <xsl:when test="current-grouping-key()"/>
        <xsl:otherwise>
          <contentNode>
            <content>
              <xsl:element name="{$self/name()}" namespace="{$contentNode.namespace.uri}">
                <xsl:copy-of select="$self/@*"/>
                <xsl:apply-templates select="current-group()" mode="convertNamespace">
                  <xsl:with-param name="namespace.uri" select="$contentNode.namespace.uri" tunnel="yes"/>
                </xsl:apply-templates>
              </xsl:element>
            </content>
          </contentNode>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each-group>
  </xsl:template>

  <!--mode contentNode-->
  <xsl:template match="node() | @*" mode="contentNode">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <!--par défaut : on ne fait rien-->
  <xsl:template match="*" mode="structuralNode.title" priority="-1"/>

  <xsl:template match="INTITULE" mode="structuralNode.title">
    <title>
      <xsl:apply-templates mode="#current"/>
    </title>
  </xsl:template>

  <xsl:template match="INTITULE/TITRE-ART" mode="structuralNode.title">
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </div>
  </xsl:template>

  <xsl:template match="CAHIER/@ID" mode="structuralNode.title">
    <title>
      <div xmlns="http://www.w3.org/1999/xhtml">
        <xsl:value-of select="."/>
      </div>
    </title>
  </xsl:template>

  <!--===============================-->
  <!--ARTICLE-->
  <!--===============================-->

  <xsl:template match="ARTICLE" mode="structuralNode">
    <xsl:variable name="code" as="xs:string">
      <xsl:choose>
        <xsl:when test="parent::CAHIER">
          <xsl:value-of select="concat('_Revue', ancestor::REVUE/@ID, '_GrpDossierArticle')"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="'_Dossier_Article'"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <referenceNode code="{$codePrefixTr}{$code}">
      <xsl:attribute name="num">
        <xsl:number level="any" count="ARTICLE" from="/"/>
      </xsl:attribute>
      <ref xf:targetResId="{xfSAS:makeHistoricIdAttribute(., dalloz:normalizeId(@ID))}" xf:refType="xf:referenceNode">
        <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{xfSAS:makeHistoricIdAttribute(., dalloz:normalizeId(@ID))}" code="{$codePrefixTee}Article">
          <xsl:attribute name="ownerGroup" select="xfSAS:addAttributeOwnerGroup(concat($codePrefixTee, ancestor::REVUE/@ID))"/>
          <!--<xsl:if test="@ETAT">
            <xsl:attribute name="status" select="xfSAS:addAttributeStatus(@ETAT)"/>
          </xsl:if>-->
          <xsl:apply-templates select="." mode="metadata"/>
          <!--FIXME : on conserve les id ?-->
          <body>
            <!--FIXME : faut-il créér un structuralNode ici avec l'intitulé de l'article 
                  (qui est répété ci-dessous pour des question de validation) ?
                  Faut-il répéter les METAS dans contenu alors qu'elles sont déjà converties ?
                  Si on ne le fait pas => ça imposerai de créer plusieurs contentNode, car par d'élément "englobeur"
                  donc créer un namespace par type d'élément sous ARTICLE, c'est lourd à gérer et ça impose de changer les traitement existants.
               -->
            <xsl:apply-templates select="." mode="contentNode"/>
          </body>
        </editorialEntity>
      </ref>
    </referenceNode>
  </xsl:template>

  <!--==================================================-->
  <!--CONTENT NODE-->
  <!--==================================================-->

  <xsl:template match="ARTICLE" mode="contentNode">
    <contentNode>
      <content>
        <xsl:variable name="contentNode" as="element()">
          <!--reste dans le mode contentNode sur l'élément courant-->
          <xsl:next-match/>
        </xsl:variable>
        <xsl:apply-templates select="$contentNode" mode="convertNamespace">
          <xsl:with-param name="namespace.uri" select="$contentNode.namespace.uri" tunnel="yes"/>
        </xsl:apply-templates>
      </content>
    </contentNode>
  </xsl:template>

  <!-- Chaque élément peut avoir seulement un @REFID, @PREC et @SUIV sont convertis en élément pour la pose de lien -->
  <xsl:template match="NAVIGATION" mode="contentNode">
    <NAVIGATION>
      <xsl:if test="@PREC">
        <PREC REFID="{@PREC}"/>
      </xsl:if>
      <xsl:if test="@SUIV">
        <SUIV REFID="{@SUIV}"/>
      </xsl:if>
    </NAVIGATION>
  </xsl:template>

  <!--FLASDZ-4 : Suppression de l'attribut CORPS/@LIB pour les besoin de XFE (CORPS sera applati lors de l'édition HTML)-->
  <!--<xsl:template match="(ARTICLE|DOSSIER)/CORPS/@LIB" mode="contentNode"/>-->

  <!--FLASDZ-4 : Suppression de la méta "RENVOIS/RENV-JURIS" et "RENVOIS/RENV-LEGIS" car elle est calculable à partir de ARTICLE/DECISION-->
  <!--<xsl:template match="(ARTICLE|DOSSIER)/METAS/RENVOIS/RENV-JURIS" mode="contentNode"/>
  <xsl:template match="(ARTICLE|DOSSIER)/METAS/RENVOIS/RENV-LEGIS" mode="contentNode"/>-->

  <!--==================================================-->
  <!--MODE convertNamespace -->
  <!--==================================================-->

  <xsl:template match="*" mode="convertNamespace" priority="-1">
    <xsl:param name="namespace.uri" required="yes" tunnel="yes"/>
    <xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="node() | @*" mode="convertNamespace" priority="-2">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>

  <!--==================================================-->
  <!--COMMON-->
  <!--==================================================-->

  <xsl:template match="node() | @*">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:function name="dalloz:normalizeId" as="xs:string">
    <xsl:param name="id" as="xs:string?"/>
    <xsl:choose>
      <xsl:when test="empty($id)">
        <xsl:message terminate="no">[ERROR][dalloz:normalizeId] paramètre id vide </xsl:message>
      </xsl:when>
      <xsl:otherwise>
        <!--FIXME : est ce nécessaire ?-->
        <xsl:value-of select="translate($id, '/', '_')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <!--==================================================-->
  <!--Add verbalization for referenceNode-->
  <!--==================================================-->

  <xsl:template match="xf:referenceNode/xf:ref" mode="xfSAS:addVerbalization">
    <xsl:next-match/>
    <verbalization>
      <xsl:sequence select="xfRes:createTitle(xf:editorialEntity/xf:body/xf:contentNode/xf:content/*)"/>
    </verbalization>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p>
        <xd:b>Récupération du code status</xd:b>
      </xd:p>
      <xd:p>Pour revues Dalloz : voir Jira FLASDZ-47</xd:p>
    </xd:desc>
    <xd:param>etat : DOSSIER/@ETAT ou ARTICLE/@ETAT dans le XML revues</xd:param>
    <xd:return>Attribut status avec l'id numérique défini dans MariaDB</xd:return>
  </xd:doc>
  <xsl:function name="xfSAS:addAttributeStatus">
    <xsl:param name="etat" as="xs:string"/>
    <xsl:attribute name="status">
      <xsl:choose>
        <xsl:when test="$etat = 'ERREUR'">
          <xsl:value-of select="'74'"/>
        </xsl:when>
        <xsl:when test="$etat = 'PREPA'">
          <xsl:value-of select="'75'"/>
        </xsl:when>
        <xsl:when test="$etat = 'COMPO'">
          <xsl:value-of select="'76'"/>
        </xsl:when>
        <xsl:when test="$etat = 'PRET'">
          <xsl:value-of select="'77'"/>
        </xsl:when>
        <xsl:when test="$etat = 'PARU'">
          <xsl:value-of select="'78'"/>
        </xsl:when>
      </xsl:choose>
    </xsl:attribute>
  </xsl:function>

  <xsl:template match="node() | @*" mode="xfSAS:addVerbalization">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>

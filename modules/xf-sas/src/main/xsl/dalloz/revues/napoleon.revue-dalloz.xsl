<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
	xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	xpath-default-namespace=""
	exclude-result-prefixes="#all"
	version="2.0">

	<!--
	XSL dans XMLfirst qui appele des service web pour certaine calculs :
	- service web de génération d'ic : http://xmlFirst/genId => renvoi texte id unique
	- service web d'accès à d'autres ressources :
		- http://xmlFirst/ee/idEE.xml[#idFragment] => xml complet de l'EE
		- http://xmlFirst/referentiel/idreferentiel.xml[#idFragment] => xml complet de l'EE (ou fragment)
		- http://xmlFirst/gps/arret.xml[#idFragment] => arret xml (ou fragment)
		ou en mode recherche :
		- http://xmlFirst/gps/getArret?date=xxx&num=yyy&ville=zzz&jurudiction=jjj
	-->
		
	<xsl:include href="xslLib:/els-common.xsl"/>
	
	<xsl:output method="xml" indent="yes"/>
	
	<xsl:variable name="sw-genid" select="'http://xmlFirst/genId'" as="xs:string"/>
	<xsl:variable name="sw-ee.uri" select="'http://xmlFirst/ee/'" as="xs:string"/>
	<xsl:variable name="sw-referentiel.uri" select="'http://xmlFirst/ee/'" as="xs:string"/>
	
	<!--==================================================-->
	<!--MAIN-->
	<!--==================================================-->
	
	<xsl:template match="/*">
		<xsl:copy copy-namespaces="yes">
			<xsl:copy-of select="@*"/>
			<xsl:namespace name="xf" select="'http://www.lefebvre-sarrut.eu/ns/xmlfirst'"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>

	<!--FIXME Issam me dit que ce sera plutôt fait dans un script (java) à part => à voir ?-->
		<!--<xsl:template match="editorialEntity/body/editorialEntity">
		<referenceNode>TODO</referenceNode>
	</xsl:template>-->
	<!--Génération xf:id-->
	<xsl:template match="*[local-name() = ('editorialEntity', 'structuralNode', 'contentNode')]/@id">
		<xsl:copy/>
		<xsl:variable name="xfId" select="(concat($sw-genid, '/makeNewId?historicId=', .)) cast as xs:string" as="xs:string"/>
		<xsl:attribute name="xf:id" select="$xfId"/>
		<!--<xsl:message>Indexation du mapping id/xf:id : <xsl:value-of select="doc(concat($sw-genid, '/indexIdd?xfId=',$xfId, '&amp;id=', .))"/></xsl:message>
		fait directement par makeNewId -->
	</xsl:template>
	
	<!--Passage en mode revueDalloz-->
	<xsl:template match="contentNode/content/*[namespace-uri(.) = 'http://www.lefebvre-sarrut.eu/ns/dalloz/revues']">
		<xsl:apply-templates select="." mode="revueDalloz"/>
	</xsl:template>
	
	<!--========================-->
	<!--MODE REVUEDALLOZ-->
	<!--========================-->
	
	<!--ajout attribut xf:* pour les liens-->
	<xsl:template match="REF" mode="revueDalloz">
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:attribute name="xf:base" select="'?'"/>
			<!--appele service web de mapping d'id-->
			<xsl:attribute name="xf:idRef" select="doc(concat($sw-genid, '/getXfId?idOriginXml=', @REFID))"/>
			<xsl:attribute name="xf:refType" select="'dalloz:REF'"/>
		</xsl:copy>
	</xsl:template>
	 <!--TODO : créer les relation-->
	
	<!--<xsl:if test="getHttpCode($ws) != '202'">
		<xsl:message terminate="yes"></xsl:message>
	</xsl:if>-->
	
	<!--==================================================-->
	<!--COMMON-->
	<!--==================================================-->
	
	<xsl:template match="node() | @*" mode="revueDalloz">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*"/>
		</xsl:copy>
	</xsl:template>
	
	<xsl:template match="node() | @*">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*"/>
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>
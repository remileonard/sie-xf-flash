<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"> 
    <xsl:output method="xml" indent="no"/>
    
    <xsl:template match="/">
        <xsl:processing-instruction name="xml-model">type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0" href="xf:models:/referenceTable/referenceTable.rng"</xsl:processing-instruction>
        <xsl:processing-instruction name="xml-model">type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron" href="xf:models:/referenceTable/referenceTable.rng"</xsl:processing-instruction>
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="REVUES">
        <referenceTable id="editorialEntityTypes">
            <metadata>
                <title>Référentiel des noms et id des Revues dalloz</title>
                <description/>
                <from/>
                <date>
                    <xsl:value-of select="format-dateTime(current-dateTime(), '[Y0001]-[M01]-[D01]')"/>
                </date>
            </metadata>
            <referenceItems>
                <xsl:apply-templates/>
            </referenceItems>
        </referenceTable>
    </xsl:template>
    
    <xsl:template match="REVUE">
        <referenceItem id="{@ID}">
            <label type="long">
                <xsl:value-of select="LIBELLE"/>
            </label>
            <label type="short">
                <xsl:value-of select="ABREV"/>
            </label>
        </referenceItem>
    </xsl:template>
    
    <xsl:template match="node() | @*"/>
</xsl:stylesheet>

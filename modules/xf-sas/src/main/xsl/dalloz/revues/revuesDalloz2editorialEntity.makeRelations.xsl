<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:functx="http://www.functx.com" xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" xmlns:dalloz="http://www.lefebvre-sarrut.eu/ns/dalloz" xmlns:dallozRevues="http://www.lefebvre-sarrut.eu/ns/dalloz/revues" xmlns:h="http://www.w3.org/1999/xhtml" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst" exclude-result-prefixes="#all" version="2.0">

	<xsl:variable name="codeTR.dalloz" select="'DZ_TR_renvoi_GrpDossierArticle_Grp'" as="xs:string"/>
	<xsl:variable name="codeTR.els.jurisprudences" select="'DZ_TR_renvoi_GrpDossierArticle_Jurisprudence'" as="xs:string"/>

	<xsl:template match="editorialEntity/body" mode="makeRelations">
		<xsl:next-match/>
		<relations>
			<xsl:apply-templates select="contentNode/content//dallozRevues:REF[@REFID]" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//dallozRevues:LIENDOCT" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//dallozRevues:LIENJUR[@REFID]" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//dallozRevues:LIENLEG[@REFID]" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//dallozRevues:DECISION/dallozRevues:ALIAS" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//dallozRevues:METAS/dallozRevues:RENVOIS/dallozRevues:ORIGINE/dallozRevues:ALIAS" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//dallozRevues:METAS/dallozRevues:NAVIGATION/dallozRevues:PREC" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//dallozRevues:METAS/dallozRevues:NAVIGATION/dallozRevues:SUIV" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//dallozRevues:DECISION/dallozRevues:PARADECI[dallozRevues:JURIDIC]/dallozRevues:NAFF" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//dallozRevues:CODE/dallozRevues:ARTICLES/dallozRevues:NOART" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//dallozRevues:TEXTELEG/dallozRevues:ARTICLES/dallozRevues:NOART" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//dallozRevues:INDEX/dallozRevues:NOME[dallozRevues:MOT='OASIS']/dallozRevues:MOCL" mode="makeRelationElement"/>
		</relations>
	</xsl:template>

	<!--=====================================-->
	<!--Revues / Codes : REF-->
	<!--=====================================-->

	<xsl:template match="dallozRevues:REF[@REFID]" mode="makeRelationElement">
		<xsl:call-template name="makeRelationElement">
			<xsl:with-param name="element" select="." as="element(dallozRevues:REF)"/>
			<xsl:with-param name="type" select="@TYPE" as="xs:string?"/>
			<xsl:with-param name="refid" select="@REFID" as="xs:string"/>
		</xsl:call-template>
	</xsl:template>

	<!--=====================================-->
	<!--Revues : doctrine-->
	<!--=====================================-->

	<xsl:template match="dallozRevues:LIENDOCT" mode="makeRelationElement">
		<xsl:call-template name="makeRelationElement">
			<xsl:with-param name="element" select="." as="element(dallozRevues:LIENDOCT)"/>
			<xsl:with-param name="type" select="@TYPE" as="xs:string?"/>
			<xsl:with-param name="refid" select="@REFID" as="xs:string"/>
		</xsl:call-template>
	</xsl:template>

	<!--=====================================-->
	<!--Revues : jurisprudence-->
	<!--=====================================-->

	<xsl:template match="dallozRevues:LIENJUR[@REFID]" mode="makeRelationElement">
		<xsl:call-template name="makeRelationElement">
			<xsl:with-param name="element" select="." as="element(dallozRevues:LIENJUR)"/>
			<xsl:with-param name="type" select="if (@TYPE) then @TYPE else 'Article'" as="xs:string?"/>
			<xsl:with-param name="refid" select="@REFID" as="xs:string"/>
		</xsl:call-template>
	</xsl:template>

	<!--=====================================-->
	<!--Revues : ALIAS-->
	<!--=====================================-->

	<xsl:template match="dallozRevues:DECISION/dallozRevues:ALIAS" mode="makeRelationElement">
		<xsl:call-template name="makeRelationElement">
			<xsl:with-param name="element" select="." as="element(dallozRevues:ALIAS)"/>
			<xsl:with-param name="type" select="'REVUE'" as="xs:string"/>
			<xsl:with-param name="refid" select="@REFID" as="xs:string"/>
		</xsl:call-template>
	</xsl:template>

	<!-- Quand METAS/RENVOIS/ORIGINE/ALIAS/@REFID est différent de DECISION/ALIAS/@REFID, on génère le lien -->
	<xsl:template match="dallozRevues:METAS/dallozRevues:RENVOIS/dallozRevues:ORIGINE/dallozRevues:ALIAS" mode="makeRelationElement">
		<xsl:if test="@REFID and not(@REFID = (ancestor::dallozRevues:ARTICLE//dallozRevues:DECISION/dallozRevues:ALIAS/@REFID))">
			<xsl:call-template name="makeRelationElement">
				<xsl:with-param name="element" select="." as="element(dallozRevues:ALIAS)"/>
				<xsl:with-param name="type" select="'REVUE'" as="xs:string"/>
				<xsl:with-param name="refid" select="@REFID" as="xs:string"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!--=====================================-->
	<!--Revues : PREC, SUIV-->
	<!--=====================================-->

	<xsl:template match="dallozRevues:METAS/dallozRevues:NAVIGATION/dallozRevues:PREC" mode="makeRelationElement">
		<xsl:call-template name="makeRelationElement">
			<xsl:with-param name="element" select="." as="element(dallozRevues:PREC)"/>
			<xsl:with-param name="type" select="'REVUE'" as="xs:string"/>
			<xsl:with-param name="refid" select="@REFID" as="xs:string"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="dallozRevues:METAS/dallozRevues:NAVIGATION/dallozRevues:SUIV" mode="makeRelationElement">
		<xsl:call-template name="makeRelationElement">
			<xsl:with-param name="element" select="." as="element(dallozRevues:SUIV)"/>
			<xsl:with-param name="type" select="'REVUE'" as="xs:string"/>
			<xsl:with-param name="refid" select="@REFID" as="xs:string"/>
		</xsl:call-template>
	</xsl:template>

	<!--=====================================-->
	<!--Jurisprudence-->
	<!--=====================================-->

	<xsl:template match="dallozRevues:DECISION/dallozRevues:PARADECI[dallozRevues:JURIDIC]/dallozRevues:NAFF" mode="makeRelationElement">
		<relation xf:id="{generate-id(.)}" code="{$codeTR.els.jurisprudences}" calculated="true">
			<metadata>
				<meta code="DZ_META_relationType">
					<value>
						<xsl:value-of select="'Jurisp'"/>
					</value>
				</meta>
			</metadata>
			<xsl:variable name="dateIso" as="xs:string">
				<xsl:call-template name="isoDate">
					<xsl:with-param name="dateStr" select="preceding-sibling::dallozRevues:DATE"/>
				</xsl:call-template>
			</xsl:variable>
			<!-- FIXME : vérifier les paramètres de query ;
				 formation prend la valeur de dallozRevues:CHAM avec le nom abrégé ou complet ?
			-->
			<ref xf:refType="dalloz:{local-name(.)}" xf:targetResId="{				
				xfSAS:makeQueryIdAttribute(., (
				(concat('ELS_META_jrpJuridiction=', preceding-sibling::dallozRevues:JURIDIC/dallozRevues:JURI)),
				(concat('ELS_META_jrpFormation=', preceding-sibling::dallozRevues:JURIDIC/dallozRevues:CHAM)),
				(concat('ELS_META_jrpDate=', $dateIso)),
				(concat('ELS_META_jrpNumjuri=', .))
				))
				}"/>
		</relation>
	</xsl:template>

	<!--=====================================-->
	<!--Codes-->
	<!--=====================================-->

	<xsl:template match="dallozRevues:CODE/dallozRevues:ARTICLES/dallozRevues:NOART" mode="makeRelationElement">
		<relation xf:id="{generate-id(.)}" code="{$codeTR.dalloz}Code" calculated="true">
			<metadata>
				<meta code="DZ_META_relationType">
					<value>
						<xsl:value-of select="'CODE'"/>
					</value>
				</meta>
			</metadata>
			<!-- FIXME : vérifier les paramètres de query -->
			<ref xf:refType="dalloz:{local-name(.)}" xf:targetResId="{				
				xfSAS:makeQueryIdAttribute(., (
				(concat('DZ_META_codesCode=', ancestor::dallozRevues:CODE/dallozRevues:TITRCODE)),
				(concat('DZ_META_codesNumero=', .))
				))
				}"/>
		</relation>
	</xsl:template>

	<xsl:template match="dallozRevues:LIENLEG[@REFID]" mode="makeRelationElement">
		<xsl:if test="@REFID">
			<xsl:call-template name="makeRelationElement">
				<xsl:with-param name="element" select="." as="element(dallozRevues:LIENLEG)"/>
				<xsl:with-param name="type" select="if (@TYPE) then @TYPE else 'CODE'" as="xs:string?"/>
				<xsl:with-param name="refid" select="@REFID" as="xs:string"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<!--=====================================-->
	<!--Texte non codifié-->
	<!--=====================================-->

	<xsl:template match="dallozRevues:TEXTELEG/dallozRevues:ARTICLES/dallozRevues:NOART" mode="makeRelationElement">
		<relation xf:id="{generate-id(.)}" code="{$codeTR.dalloz}Code" calculated="true">
			<metadata>
				<meta code="DZ_META_relationType">
					<value>
						<xsl:value-of select="'TEXTLEG'"/>
					</value>
				</meta>
			</metadata>
			<xsl:variable name="dateIso">
				<xsl:call-template name="isoDate">
					<xsl:with-param name="dateStr" select="ancestor::dallozRevues:TEXTELEG/dallozRevues:TITRTEXTE/dallozRevues:DATE"/>
				</xsl:call-template>
			</xsl:variable>
			<xsl:variable name="regExp" select="'n(°|o)\p{Zs}'" as="xs:string"/>
			<xsl:variable name="nume" select="ancestor::dallozRevues:TEXTELEG/dallozRevues:TITRTEXTE/dallozRevues:NUME"/>
			<!-- FIXME : vérifier les paramètres de query -->
			<ref xf:refType="dalloz:{local-name(.)}" xf:targetResId="{				
				xfSAS:makeQueryIdAttribute(., (
				(concat('DZ_META_codesNatx=', ancestor::dallozRevues:TEXTELEG/dallozRevues:TITRTEXTE/dallozRevues:NATX)),
				(concat('DZ_META_codesTxNume=', replace($nume, $regExp, ''))),
				(concat('DZ_META_codesTxDate=', $dateIso)),
				(concat('DZ_META_codesTxNoart=', .))
				))
				}"/>
		</relation>
	</xsl:template>

	<!--=====================================-->
	<!--OASIS-->
	<!--=====================================-->

	<xsl:template match="dallozRevues:INDEX/dallozRevues:NOME[dallozRevues:MOT='OASIS']/dallozRevues:MOCL" mode="makeRelationElement">
		<xsl:call-template name="makeRelationElement">
			<xsl:with-param name="element" select="." as="element(dallozRevues:MOCL)"/>
			<xsl:with-param name="type" select="'OASIS'" as="xs:string"/>
			<xsl:with-param name="refid" select="dallozRevues:DESCRIPT/dallozRevues:MOT" as="xs:string"/>
		</xsl:call-template>
	</xsl:template>

	<!--=====================================-->
	<!--makeRelations-->
	<!--=====================================-->

	<xsl:template name="makeRelationElement">
		<xsl:param name="element" as="element()"/>
		<xsl:param name="type" as="xs:string?"/>
		<xsl:param name="refid" as="xs:string?"/>

		<xsl:variable name="code" as="xs:string?">
			<xsl:choose>
				<xsl:when test="$type = ('REVUE', 'Article')">
					<xsl:sequence select="concat($codeTR.dalloz, 'DossierArticle')"/>
				</xsl:when>
				<xsl:when test="$type = 'CODE'">
					<xsl:sequence select="concat($codeTR.dalloz, 'Code')"/>
				</xsl:when>
				<xsl:when test="$type = 'OASIS'">
					<xsl:sequence select="concat($codeTR.dalloz, 'Oasis')"/>
				</xsl:when>
				<xsl:when test="$type = 'Jurisp'">
					<xsl:sequence select="$codeTR.els.jurisprudences"/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:message terminate="no">[ERROR] type de relation inconnu : <xsl:value-of select="$type"/></xsl:message>
					<xsl:message terminate="no"><xsl:sequence select="$element"/></xsl:message>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<relation xf:id="{generate-id($element)}" code="{$code}" calculated="true">
			<metadata>
				<xsl:if test="$type">
					<meta code="DZ_META_relationType">
						<value>
							<xsl:value-of select="$type"/>
						</value>
					</meta>
				</xsl:if>
			</metadata>
			<ref xf:targetResId="{replace(xfSAS:makeHistoricIdAttribute($element, $refid), '/', '_')}" xf:refType="dalloz:{local-name($element)}"/>
		</relation>
	</xsl:template>

	<xsl:template match="contentNode/content//dallozRevues:REF[@REFID]
		| contentNode/content//dallozRevues:METAS/dallozRevues:NAVIGATION/dallozRevues:PREC
		| contentNode/content//dallozRevues:METAS/dallozRevues:NAVIGATION/dallozRevues:SUIV
		| contentNode/content//dallozRevues:LIENDOCT
		| contentNode/content//dallozRevues:LIENJUR[@REFID]
		| contentNode/content//dallozRevues:LIENLEG[@REFID]
		| contentNode/content//dallozRevues:DECISION/dallozRevues:ALIAS
		| contentNode/content//dallozRevues:DECISION/dallozRevues:PARADECI[dallozRevues:JURIDIC]/dallozRevues:NAFF
		| contentNode/content//dallozRevues:CODE/dallozRevues:ARTICLES/dallozRevues:NOART 
		| contentNode/content//dallozRevues:TEXTELEG/dallozRevues:ARTICLES/dallozRevues:NOART
		| contentNode/content//dallozRevues:INDEX/dallozRevues:NOME[dallozRevues:MOT='OASIS']/dallozRevues:MOCL" mode="makeRelations">
		<xsl:call-template name="makeRelation">
			<xsl:with-param name="element" select="." as="element()"/>
		</xsl:call-template>
	</xsl:template>

	<!-- Quand METAS/RENVOIS/ORIGINE/ALIAS/@REFID est différent de DECISION/ALIAS/@REFID, on génère le lien -->
	<xsl:template match="contentNode/content//dallozRevues:METAS/dallozRevues:RENVOIS/dallozRevues:ORIGINE/dallozRevues:ALIAS" mode="makeRelations">
		<xsl:if test="not(@REFID = (ancestor::dallozRevues:ARTICLE//dallozRevues:DECISION/dallozRevues:ALIAS/@REFID))">
			<xsl:call-template name="makeRelation">
				<xsl:with-param name="element" select="." as="element()"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="makeRelation">
		<xsl:param name="element" as="element()"/>
		<xsl:copy>
			<xsl:attribute name="xf:idRef" select="generate-id(.)"/>
			<xsl:attribute name="xf:refType" select="'xf:relation'"/>
			<xsl:apply-templates select="$element/@*" mode="#current"/>
			<xsl:apply-templates select="$element/node()" mode="#current"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template name="isoDate">
		<xsl:param name="dateStr"/>
		<xsl:choose>
			<xsl:when test="matches($dateStr, '^\d\d-\d\d-\d\d\d\d$')">
				<xsl:variable name="tokenizedDate" select="tokenize($dateStr, '-')"/>
				<xsl:sequence select="concat($tokenizedDate[3], '-', $tokenizedDate[2], '-', $tokenizedDate[1])"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="inputDate">
					<xsl:choose>
						<xsl:when test="tokenize($dateStr, ' ')[1] = ''">
							<xsl:sequence select="concat('1er', $dateStr)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:sequence select="$dateStr"/>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				<xsl:variable name="date" select="els:date-string-to-number-slash($inputDate)"/>
				<xsl:sequence select="if ($date != '[ErreurDate]') then concat(els:makeIsoDate($date, '/'), 'T00:00:00Z') else $dateStr"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<!--=====================================-->
	<!--COPY-->
	<!--=====================================-->

	<xsl:template match="node() | @*" mode="makeRelations">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" mode="#current"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>

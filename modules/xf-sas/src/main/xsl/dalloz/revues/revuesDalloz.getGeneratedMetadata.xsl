<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" xmlns:dalloz="http://www.lefebvre-sarrut.eu/ns/dalloz" xmlns:h="http://www.w3.org/1999/xhtml" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" exclude-result-prefixes="#all" version="2.0">

	<!--==================================================-->
	<!-- MODE METADATA-->
	<!--==================================================-->

	<xd:doc scope="stylesheet">
		<xd:desc>
			<xd:p>XSLT pour gérer les métadonnées générées (1 seule fois dans le SAS et ensuite éditable dans l'ECM)</xd:p>
			<xd:p>A ne pas confondre avec les métadonnées calculées à partir du contenu (gérées dans sie-xf-ecm-resources), ici les éléments xml qui servent au métadonnées générées ne sont pas conservés. (ici on peut aussi déclarer des métadonnées "générées" qui ne sont pas liées au contenu xml, par exemple une date d'import)</xd:p>
		</xd:desc>
	</xd:doc>

	<!--==================-->
	<!-- META CAHIER-->
	<!--==================-->

	<xsl:template match="CAHIER" mode="metadata">
		<metadata>
			<meta code="DZ_META_revuesNom">
				<!-- FIXME : mettre en commentaire verbalization -->
				<value>
					<!--<ref xf:idRef="{../self::REVUE/@ID}" xf:base="{$xfSAS:REST.referenceTable.uri}referenceTable-dalloz_revues.xml" xf:refType="referenceItemRef"/>-->
					<xsl:value-of select="../self::REVUE/TITRE-REVUE"/>
				</value>
				<!--<verbalization>
					<span xmlns="http://www.w3.org/1999/xhtml">
						<xsl:value-of select="../self::REVUE/TITRE-REVUE"/>
					</span>
				</verbalization>-->
			</meta>
			<xsl:apply-templates select="@* | *" mode="metadata"/>
		</metadata>
	</xsl:template>

	<xsl:template match="CAHIER/ANNEE" mode="metadata">
		<meta code="DZ_META_revuesCahierAnnee">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="CAHIER/DATE-CAHIER" mode="metadata">
		<meta code="DZ_META_revuesCahierDate">
			<value>
				<xsl:value-of select="concat(els:makeIsoDate(., '-'), 'T00:00:00Z')"/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="CAHIER/NUM-CAHIER" mode="metadata">
		<xsl:apply-templates mode="#current"/>
	</xsl:template>

	<xsl:template match="CAHIER/NUM-CAHIER/NUM" mode="metadata">
		<meta code="DZ_META_revuesCahierNum">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="REVUE/@ID" mode="metadata">
		<meta code="DZ_META_revuesOrigine">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<!--==================-->
	<!-- META DOSSIER-->
	<!--==================-->

	<xsl:template match="DOSSIER" mode="metadata">
		<metadata>
			<!-- FIXME : métas cahier pour EE dossier et article -->
			<xsl:apply-templates select="ancestor::REVUE/@ID" mode="#current"/>
			<xsl:apply-templates select="parent::CAHIER/ANNEE" mode="#current"/>
			<xsl:apply-templates select="parent::CAHIER/NUM-CAHIER/NUM" mode="#current"/>
			<xsl:apply-templates mode="#current"/>
		</metadata>
	</xsl:template>

	<xsl:template match="DOSSIER/@ETAT" mode="metadata">
		<meta code="DZ_META_revuesDossierEtat">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="METAS" mode="metadata">
		<xsl:apply-templates mode="#current"/>
	</xsl:template>

	<xsl:template match="FOLIO" mode="metadata">
		<xsl:apply-templates select="@*" mode="#current"/>
	</xsl:template>

	<xsl:template match="METAS/FOLIO/@DEB" mode="metadata">
		<meta code="DZ_META_revuesFolioDeb">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="METAS/FOLIO/@FIN" mode="metadata">
		<meta code="DZ_META_revuesFolioFin">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="METAS/FOLIO/@POSITION" mode="metadata">
		<meta code="DZ_META_revuesFolioPosition">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="PARTIE" mode="metadata">
		<xsl:apply-templates mode="#current"/>
	</xsl:template>

	<xsl:template match="PARTIE/TITRE-PART" mode="metadata">
		<meta code="DZ_META_revuesPartTitre">
			<value>
				<xsl:value-of select="normalize-space(.)"/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="PARTIE/SPARTIE" mode="metadata">
		<meta code="DZ_META_revuesSouspart">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<!--==================-->
	<!-- META ARTICLE-->
	<!--==================-->

	<xsl:template match="ARTICLE" mode="metadata">
		<metadata>
			<!-- FIXME : métas cahier pour EE dossier et article -->
			<xsl:apply-templates select="ancestor::REVUE/@ID" mode="#current"/>
			<xsl:apply-templates select="ancestor::CAHIER/ANNEE" mode="#current"/>
			<xsl:apply-templates select="ancestor::CAHIER/NUM-CAHIER/NUM" mode="#current"/>
			<xsl:apply-templates select="@*" mode="#current"/>
			<xsl:apply-templates mode="#current"/>
		</metadata>
	</xsl:template>

	<xsl:template match="ARTICLE/@TYPE" mode="metadata">
		<meta code="DZ_META_revuesArticleType">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="ARTICLE/@TYPE2" mode="metadata">
		<meta code="DZ_META_revuesArticleType2">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="ARTICLE/@ETAT" mode="metadata">
		<meta code="DZ_META_revuesArticleEtat">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<!--<xsl:template match="ARTICLE/INTITULE" mode="metadata">
		<xsl:apply-templates mode="metadata"/>
	</xsl:template>
	
	<xsl:template match="INTITULE/NUM-ART" mode="metadata">
		<meta code="DZ_META_revuesArticleNum">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>-->

	<xsl:template match="RUBRIQUE" mode="metadata">
		<xsl:apply-templates mode="#current"/>
	</xsl:template>

	<xsl:template match="TITRE-RUBR" mode="metadata">
		<meta code="DZ_META_revuesRubrTitre">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="SRUBR" mode="metadata">
		<meta code="DZ_META_revuesSousRubr">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="SIGNATURES | SIGNATURE | AUTEUR" mode="metadata">
		<xsl:apply-templates mode="#current"/>
	</xsl:template>

	<xsl:template match="AUTEUR/NOM" mode="metadata">
		<meta code="DZ_META_revuesAuteurNom">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="AUTEUR/PRENOM" mode="metadata">
		<meta code="DZ_META_revuesAuteurPrenom">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="QUALITE" mode="metadata">
		<meta code="DZ_META_revuesAuteurQualite">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="INDEX" mode="metadata">
		<xsl:apply-templates mode="#current"/>
	</xsl:template>

	<xsl:template match="NOME" mode="metadata">
		<xsl:apply-templates select="MOT" mode="#current"/>
		<xsl:apply-templates select=".//MOT[not(following-sibling::*/MOT)]" mode="#current"/>
	</xsl:template>

	<xsl:template match="NOME/MOT" mode="metadata">
		<meta code="DZ_META_revuesMotcleNiv1">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="MOT[not(following-sibling::*/MOT)]" mode="metadata">
		<meta code="DZ_META_revuesMotcle">
			<value>
				<xsl:value-of select="concat(string-join(ancestor::NOME//MOT except parent::*/MOT, ' / '), ' / ', .)"/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="RENVOIS | RENV-JURIS | JURIDIC | RENV-LEGIS | LIENLEG" mode="metadata">
		<xsl:apply-templates mode="#current"/>
	</xsl:template>

	<xsl:template match="LIENJUR | PARADECI[JURIDIC]" mode="metadata">
		<meta code="DZ_META_revuesDecision">
			<value>
				<xsl:value-of select="string-join((JURIDIC/JURI, JURIDIC/CHAM, DATE), ' ')"/>
				<xsl:if test="NAFF">
					<xsl:text> n° </xsl:text>
					<xsl:value-of select="NAFF"/>
				</xsl:if>
			</value>
		</meta>
		<xsl:if test="LIENJUR">
			<xsl:apply-templates mode="#current"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="RENVOIS//JURI | PARADECI/JURIDIC/JURI" mode="metadata">
		<meta code="DZ_META_revuesJuri">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="RENVOIS//NAFF | PARADECI/NAFF" mode="metadata">
		<meta code="DZ_META_revuesNaff">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="RENVOIS/RENV-JURIS/LIENJUR/DATE | PARADECI/DATE" mode="metadata">
		<meta code="DZ_META_revuesDecisionDate">
			<value>
				<xsl:value-of select="xfRes:formatDate(.)"/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="RENVOIS//CHAM | PARADECI/JURIDIC/CHAM" mode="metadata">
		<meta code="DZ_META_revuesChambre">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="RENVOIS//TEXTELEG | PARADECI//TEXTELEG" mode="metadata">
		<meta code="DZ_META_revuesTexteleg">
			<value>
				<xsl:apply-templates select="TITRTEXTE/*" mode="#current"/>
				<xsl:apply-templates select="ARTICLES" mode="#current"/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="TITRTEXTE/NATX" mode="metadata">
		<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="TITRTEXTE/NUME" mode="metadata">
		<xsl:text> </xsl:text>
		<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="TITRTEXTE/DATE" mode="metadata">
		<xsl:text> du </xsl:text>
		<xsl:value-of select="."/>
	</xsl:template>

	<xsl:template match="ARTICLES" mode="metadata">
		<xsl:text> art. </xsl:text>
		<xsl:value-of select="string-join(NOART, ', ')"/>
	</xsl:template>

	<xsl:template match="LIENLEG/CODE | PARADECI/FONDLEG/CODES/CODE" mode="metadata">
		<meta code="DZ_META_revuesCode">
			<value>
				<xsl:value-of select="concat(TITRCODE, ' - ')"/>
				<xsl:apply-templates select="ARTICLES" mode="#current"/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="DECISION" mode="metadata">
		<xsl:if test="not(parent::ARTICLE//RENVOIS//LIENJUR)">
			<xsl:apply-templates select="PARADECI[JURIDIC]" mode="#current"/>
			<xsl:apply-templates select="PARADECI/JURIDIC/JURI" mode="#current"/>
			<xsl:apply-templates select="PARADECI/NAFF" mode="#current"/>
			<xsl:apply-templates select="PARADECI/DATE" mode="#current"/>
			<xsl:apply-templates select="PARADECI/JURIDIC/CHAM" mode="#current"/>
		</xsl:if>
		<xsl:if test="not(parent::ARTICLE//RENVOIS//TEXTELEG)">
			<xsl:apply-templates select="PARADECI//TEXTELEG" mode="#current"/>
		</xsl:if>
		<xsl:if test="not(parent::ARTICLE//RENVOIS//LIENLEG/CODE)">
			<xsl:apply-templates select="PARADECI/FONDLEG/CODES/CODE" mode="#current"/>
		</xsl:if>
	</xsl:template>

	<xsl:template match="*" mode="metadata" priority="-1"/>
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:functx="http://www.functx.com" xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" xmlns:dalloz="http://www.lefebvre-sarrut.eu/ns/dalloz" xmlns:h="http://www.w3.org/1999/xhtml" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" exclude-result-prefixes="#all" version="2.0">

	<xsl:import href="../xf-sas-common.xsl"/>
	<xsl:include href="xslLib:/els-common.xsl"/>

	<xsl:output method="xml" indent="no"/>

	<xsl:param name="rel.to.schema" select="'xf-models:/editorialEntity'" as="xs:string"/>
	<xsl:param name="xfSAS:company" select="'dalloz'" as="xs:string"/>
	<xsl:param name="outDir" select="base-uri()" as="xs:string"/>

	<xsl:variable name="contentNode.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/dalloz/codes'" as="xs:string"/>

	<xsl:variable name="REST.referenceTable.uri" select="'http://xmlFirst/referenceTables/'"/>

	<xsl:variable name="dalloz.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/dalloz/codes'"/>

	<xsl:template match="/">
		<xsl:apply-templates/>
		<!--<xsl:call-template name="xfSAS:makeXmlModelPi"/>
		<xsl:variable name="step1-xml2ee" as="document-node()">
			<xsl:document>
				<xsl:apply-templates/>
			</xsl:document>
		</xsl:variable>
		<xsl:result-document href="log/step1-xml2ee.log.xml">
			<xsl:sequence select="$step1-xml2ee"/>
		</xsl:result-document>
		<xsl:apply-templates select="$step1-xml2ee" mode="makeRelations"/>-->
	</xsl:template>


	<!--===============================-->
	<!--CODALLOZ-->
	<!--===============================-->
	<xsl:template match="CODALLOZ">
		<!-- chaque code est une EE -->
		<editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{xfSAS:makeHistoricIdAttribute(., dalloz:normalizeId(@NOM))}" type="DALLOZ_CODE_CODE">
			<xsl:namespace name="xf" select="'http://www.lefebvre-sarrut.eu/ns/xmlfirst'"/>
			<metadata>
				<meta code="CODES_NOM">
					<value>
						<ref xf:idRef="{@NOM}" xf:base="{$xfSAS:REST.referenceTable.uri}referenceTable-dalloz_codes.xml" xf:refType="relationRef"/>
					</value>
					<verbalization>
						<span xmlns="http://www.w3.org/1999/xhtml">
							<xsl:value-of select="CODE/PRECODE/TITR | CODEAPPE/PREAPPE/TITR"/>
						</span>
					</verbalization>
				</meta>
			</metadata>

			<body>
				<xsl:apply-templates mode="structuralNode"/>
			</body>
		</editorialEntity>
	</xsl:template>

	<!--===============================-->
	<!--EE-->
	<!-- codes officiels : ARTI (sauf SOUSCODE//ARTI, LOID//ARTI, GRATA//ARTI, APPE//ARTI), APPEMEG//JURI, APPEMEG//SOUSCODE, APPEMEG//LOID -->
	<!-- codes éditeurs : CODEAPPE//SOUSCODE, LOID, JURIS -->
	<!--===============================-->
	<xsl:template match="*[descendant::ARTI[not(ancestor::APPE) and not(ancestor::LOID) and not(ancestor::GRATA) and not(ancestor::SOUSCODE)]]" mode="structuralNode">
		<structuralNode>
			<xsl:apply-templates mode="#current"/>
		</structuralNode>
	</xsl:template>

	<xsl:template match="ARTI[not(ancestor::APPE) and not(ancestor::LOID) and not(ancestor::GRATA) and not(ancestor::SOUSCODE)]" mode="structuralNode">
		<structuralNode>
			<xsl:apply-templates select="NUME/text()" mode="structuralNode.title"/>
			<referenceNode>
				<ref xf:idRef="{xfSAS:makeHistoricIdAttribute(., dalloz:normalizeId(@ID))}" xf:base="{$xfSAS:REST.referenceTable.uri}referenceTable-dalloz_codes.xml" xf:refType="xf:referenceNode">
					<editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{xfSAS:makeHistoricIdAttribute(., dalloz:normalizeId(@ID))}" type="DALLOZ_CODES_ARTICLE">
						<!--FIXME : on conserve les id ?-->
						<metadata>
							<xsl:apply-templates select="@*" mode="metadata"/>
							<xsl:apply-templates select="NUME" mode="metadata"/>
						</metadata>

						<body>
							<xsl:apply-templates select="." mode="contentNode"/>
						</body>
					</editorialEntity>
				</ref>
			</referenceNode>
		</structuralNode>
	</xsl:template>

	<xsl:template match="*" mode="structuralNode" priority="-1">
		<structuralNode>
			<xsl:apply-templates select="if ((.//text()[normalize-space() != ''])[1]) then (.//text()[normalize-space() != ''])[1] else text()[1]" mode="structuralNode.title"/>
			<contentNode>
				<content>
					<xsl:apply-templates select="." mode="convertNamespace">
						<xsl:with-param name="namespace.uri" select="$contentNode.namespace.uri" tunnel="yes"/>
					</xsl:apply-templates>
				</content>
			</contentNode>
		</structuralNode>
	</xsl:template>
	
	<xsl:template match="text()" mode="structuralNode.title">
		<title>
			<div xmlns="http://www.w3.org/1999/xhtml">
				<xsl:value-of select="."/>
			</div>
		</title>
	</xsl:template>

	<!--==================================================-->
	<!--MODE contentNode-->
	<!--==================================================-->
	<xsl:template match="node() | @*" mode="contentNode">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" mode="#current"/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="ARTI/@ID" mode="metadata"/>

	<xsl:template match="ARTI/@DATE_MAJ" mode="metadata">
		<meta code="CODES_ARTI_MAJ">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<xsl:template match="ARTI/NUME" mode="metadata">
		<meta code="CODES_ARTI_NUME">
			<value>
				<xsl:value-of select="."/>
			</value>
		</meta>
	</xsl:template>

	<!--==================================================-->
	<!--CONTENT NODE-->
	<!--==================================================-->

	<xsl:template match="ARTI" mode="contentNode">
		<contentNode>
			<content>
				<xsl:apply-templates select="." mode="convertNamespace">
					<xsl:with-param name="namespace.uri" select="$contentNode.namespace.uri" tunnel="yes"/>
				</xsl:apply-templates>
			</content>
		</contentNode>
	</xsl:template>

	<!--==================================================-->
	<!--MODE convertNamespace -->
	<!--==================================================-->

	<xsl:template match="*" mode="convertNamespace" priority="-1">
		<xsl:param name="namespace.uri" required="yes" tunnel="yes"/>
		<xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
			<xsl:apply-templates select="node() | @*" mode="#current"/>
		</xsl:element>
	</xsl:template>

	<xsl:template match="node() | @*" mode="convertNamespace" priority="-2">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*"/>
		</xsl:copy>
	</xsl:template>

	<!--==================================================-->
	<!--COMMON"-->
	<!--==================================================-->

	<xsl:template match="node() | @*">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*"/>
		</xsl:copy>
	</xsl:template>

	<xsl:function name="dalloz:normalizeId" as="xs:string">
		<xsl:param name="id" as="xs:string?"/>
		<xsl:choose>
			<xsl:when test="empty($id)">
				<xsl:message terminate="no">[ERROR][dalloz:normalizeId] paramètre id vide </xsl:message>
			</xsl:when>
			<xsl:otherwise>
				<!--FIXME : est ce nécessaire ?-->
				<xsl:value-of select="translate($id, '/', '_')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
</xsl:stylesheet>

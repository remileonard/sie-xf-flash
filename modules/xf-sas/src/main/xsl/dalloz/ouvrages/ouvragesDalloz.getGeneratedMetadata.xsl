<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema" 
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
	xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
	xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
	xmlns:dalloz="http://www.lefebvre-sarrut.eu/ns/dalloz" 
	xmlns:h="http://www.w3.org/1999/xhtml" 
	xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
	xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" 
	exclude-result-prefixes="#all" 
	version="2.0">
  
  
  <xsl:param name="codePrefixMeta" select="'DZ_META_'" as="xs:string"/>
  <xsl:param name="codeModele" select="'ouvrages'" as="xs:string"/>

	<!--==================================================-->
	<!-- MODE METADATA-->
	<!--==================================================-->

	<xd:doc scope="stylesheet">
		<xd:desc>
			<xd:p>XSLT pour gérer les métadonnées générées (1 seule fois dans le SAS et ensuite éditable dans l'ECM)</xd:p>
			<xd:p>A ne pas confondre avec les métadonnées calculées à partir du contenu (gérées dans sie-xf-ecm-resources), ici les éléments xml qui servent au métadonnées générées ne sont pas conservés. (ici on peut aussi déclarer des métadonnées "générées" qui ne sont pas liées au contenu xml, par exemple une date d'import)</xd:p>
		</xd:desc>
	</xd:doc>

	<!--==================-->
	<!-- -->
  <!--==================-->
	<xsl:template match="@id" mode="metadata"/>
  
  <xsl:template match="METADONNEES|METAS" mode="metadata">
    <xsl:apply-templates mode="metadata"/>
  </xsl:template>
  
  <xsl:template match="ISBN" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'Isbn')}">
      <value>
        <xsl:value-of select="."/>
      </value>
    </meta>
  </xsl:template>
  
  <xsl:template match="DATES" mode="metadata">
    <xsl:apply-templates select="*" mode="metadata"/>
  </xsl:template>  
  
  <xsl:template match="DATE-CREA" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'Date2')}">
      <value>
        <xsl:value-of select="concat(., '-01T00:00:00Z')"/>
      </value>
    </meta>
  </xsl:template>  
  
  <xsl:template match="DATE-MAJ" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'Date3')}">
      <value>
        <xsl:value-of select="."/>
      </value>
    </meta>  
  </xsl:template>  
  
  <xsl:template match="DATE-PUBLI" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'Annee')}">
      <value>
        <xsl:value-of select="."/>
      </value>
    </meta>
  </xsl:template>  
  
  <xsl:template match="DATE-APPLI" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'Date4')}">
      <value>
        <xsl:value-of select="concat(., '-01T00:00:00Z')"/>
      </value>
    </meta>
  </xsl:template>
  
  <xsl:template match="TITRE" mode="metadata">
    <xsl:apply-templates select="*" mode="metadata"/>
  </xsl:template>  
  
  <xsl:template match="TIT-NUMERO" mode="metadata">
    <xsl:apply-templates select="*" mode="metadata"/>
  </xsl:template>  
  
  <xsl:template match="TIT-NIV" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'TitreNiv')}">
      <value><xsl:value-of select="."/></value>
    </meta>
  </xsl:template> 
  
  <xsl:template match="TIT-NUM" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'NumNiv')}">
      <value><xsl:value-of select="."/></value>
    </meta>
  </xsl:template>  
  
  <xsl:template match="METADONNEES/TITRE/TIT-INTITULE" mode="metadata"/>
  
  <xsl:template match="TIT-INTITULE" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'TitreInt')}">
      <value><xsl:value-of select="TIT-INTT"/></value>
    </meta>
  </xsl:template>
  
  <xsl:template match="NUME" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'ParaNume')}">
      <value><xsl:value-of select="."/></value>
    </meta>
  </xsl:template> 
  
  <xsl:template match="AUTEUR" mode="metadata">
    <xsl:apply-templates select="*" mode="metadata"/>
  </xsl:template> 
  
  <xsl:strip-space elements="AUTEUR-NOM-COMPLET"/>
  <xsl:template match="AUTEUR-NOM-COMPLET" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'AuteurPrenomNom')}">
      <xsl:variable name="AuteurPrenomNom"><xsl:value-of select="*|text()" separator="&#160;"/></xsl:variable>
      <value><xsl:value-of select="normalize-space($AuteurPrenomNom)"/></value>
    </meta>
  </xsl:template>
  
  <xsl:template match="AUTEUR-QUALITES" mode="metadata">
    <meta code="{concat($codePrefixMeta, $codeModele, 'AuteurQualite')}">
      <xsl:apply-templates select="*" mode="metadata"/>
    </meta>
  </xsl:template>
  
  <xsl:template match="AUT-QUALITE" mode="metadata">
    <xsl:choose>
      <xsl:when test="local-name(..) = 'AUTEUR-QUALITES'">
        <value><xsl:value-of select="."/></value>
      </xsl:when>
      <xsl:otherwise>
        <meta code="{concat($codePrefixMeta, $codeModele, 'AuteurQualite')}">
          <value><xsl:value-of select="."/></value>
        </meta>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="THEMATISATION" mode="metadata">
    <xsl:for-each-group select="THM-NIV1" group-starting-with="*[THM-NIV2]">
      <meta code="{concat($codePrefixMeta, $codeModele, 'MotcleNiv1')}">
        <xsl:for-each select="current-group()">
          <xsl:apply-templates select="THM-LIB" mode="metadata"/>
        </xsl:for-each>
      </meta>
      <xsl:if test="./THM-NIV2">
        <meta code="{concat($codePrefixMeta, $codeModele, 'MotcleNiv2')}">
          <xsl:for-each select="./THM-NIV2">
            <xsl:apply-templates select="THM-LIB" mode="metadata"/>
          </xsl:for-each>
        </meta>
      </xsl:if>
    </xsl:for-each-group>
  </xsl:template> 
  
  <xsl:template match="THM-LIB" mode="metadata">
    <value><xsl:value-of select="."/></value>
  </xsl:template>
  
  <xsl:template match="INDX" mode="metadata" />
  
  <xsl:function name="dalloz:metaCollectionType" as="element(xf:meta)*">
    <xsl:param name="root" as="element(OUVRAGE-DALLOZ)"/>
    <xsl:if test="$root/@id">
      <xsl:variable name="tokens" select="tokenize($root/@id, '/')"/>
      <xsl:if test="$tokens[1]">
        <xsl:variable name="TypeCollec">
          <xsl:choose>
            <xsl:when test="$tokens[1]='ACTION'"><xsl:value-of select="'Dalloz Action'"/></xsl:when>
            <xsl:when test="$tokens[1]='DELMAS'"><xsl:value-of select="'Delmas'"/></xsl:when>
            <xsl:when test="$tokens[1]='JURIS-CORPUS'"><xsl:value-of select="'Juris Corpus'"/></xsl:when>
            <xsl:when test="$tokens[1]='REFERENCE'"><xsl:value-of select="'Dalloz Référence'"/></xsl:when>
            <xsl:when test="$tokens[1]='JURIS-CORPUS'"><xsl:value-of select="'Juris Corpus'"/></xsl:when>
            <!-- en attente de repose Jira, je mets Praxis Dalloz mais ce devrait etre Dalloz Professionnel-->
            <xsl:when test="$tokens[1]='PCA'"><xsl:value-of select="'Praxis Dalloz'"/></xsl:when>
            <!-- et il manque les codes pour Praxis Dalloz et Guides Dalloz -->
            <xsl:otherwise>
              <xsl:message>[WARNING] Code de collection inconnu <xsl:value-of select="$tokens[1]"/></xsl:message>
              <xsl:value-of select="$tokens[1]"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <meta code="{concat($codePrefixMeta, 'ouvrages', 'Collection')}">
          <value><xsl:value-of select="$TypeCollec"/></value>
        </meta>
      </xsl:if>
      <xsl:if test="$tokens[2]">
        <meta code="{concat($codePrefixMeta, 'ouvrages', 'Type')}">
          <value><xsl:value-of select="$tokens[2]"/></value>
        </meta>
      </xsl:if>
    </xsl:if>
  </xsl:function>
  
  <xsl:function name="dalloz:metaTitle" as="element(xf:meta)">
    <xsl:param name="root" as="element(OUVRAGE-DALLOZ)"/>
    <meta code="{concat($codePrefixMeta, $codeModele, 'TitreInt')}">
      <xsl:choose>
        <xsl:when test="$root/METADONNEES/TITRE/TIT-INTITULE/TIT-INTT/text()">
          <value><xsl:value-of select="$root/METADONNEES/TITRE/TIT-INTITULE/TIT-INTT"/></value>
        </xsl:when>
        <xsl:otherwise>
          <xsl:variable name="tokens" select="tokenize($root/@id, '/')"/>
          <xsl:choose>
            <xsl:when test="$tokens[2]">
              <xsl:variable name="apos">'</xsl:variable>
              <xsl:choose>
                <!-- Dalloz action: le code apparait sous la forme ACTION/XXXX -->
                <xsl:when test="$tokens[2]='ACTION'"><value><xsl:value-of select="'Dalloz Action'"/></value></xsl:when>
                <xsl:when test="$tokens[2]='CASSATION-PENALE'"><value><xsl:value-of select="'La cassation en matière pénale'"/></value></xsl:when>
                <xsl:when test="$tokens[2]='COPROPRIETE'"><value><xsl:value-of select="'La copropriété'"/></value></xsl:when>
                <xsl:when test="$tokens[2]='DROIT-AUTEU'"><value><xsl:value-of select="concat('Droit d', $apos, 'auteur')"/></value></xsl:when>
                <xsl:when test="$tokens[2]='CONSTRUCTION'"><value><xsl:value-of select="'Droit de la construction'"/></value></xsl:when>
                <xsl:when test="$tokens[2]='EXECUTION-PEINES'"><value><xsl:value-of select="concat('Droit de l', $apos, 'exécution des peines')"/></value></xsl:when>
                <xsl:when test="$tokens[2]='EXPERTISE'"><value><xsl:value-of select="concat('Droit de l', $apos, 'expertise')"/></value></xsl:when>
                <xsl:when test="$tokens[2]='FAMILLE'"><value><xsl:value-of select="'Droit de la famille'"/></value></xsl:when>
                <xsl:when test="$tokens[2]='BAUX-COMMERCIAUX'"><value><xsl:value-of select="'Droit et pratique des baux commerciaux'"/></value></xsl:when>
                <xsl:when test="$tokens[2]='INSTRUCTION-PREPARATOIRE'"><value><xsl:value-of select="concat('Droit et pratique de l', $apos, 'instruction préparatoire')"/></value></xsl:when>
                <xsl:when test="$tokens[2]='PROCEDURE-CIVILE'"><value><xsl:value-of select="'Droit et pratique de la procédure civile'"/></value></xsl:when>
                <xsl:when test="$tokens[2]='PATRIMONIAL'"><value><xsl:value-of select="'Droit patrimonial de la famille'"/></value></xsl:when>
                <xsl:when test="$tokens[2]='RESPONSABILITE'"><value><xsl:value-of select="'Droit de la responsabilité et des contrats'"/></value></xsl:when>
                <xsl:when test="$tokens[2]='FONDS-COMMERCE'"><value><xsl:value-of select="'Fonds de commerce'"/></value></xsl:when>
                <!-- Dalloz référence: le code apparait sous la forme REFERENCE/XXXX -->
                <xsl:when test="$tokens[2]='CABINET-AVOCATS'"><value><xsl:value-of select="concat('Cabinet d', $apos, 'avocats')"/></value></xsl:when>
                <xsl:when test="$tokens[2]='AFFAIRES'"><value><xsl:value-of select="concat('Contrats d', $apos, 'affaires')"/></value></xsl:when>
                <xsl:when test="$tokens[2]='DIVORCE'"><value><xsl:value-of select="'Droit et pratique du divorce'"/></value></xsl:when>
                <!-- cas particulier -->
                <!-- Juris corpus: le code apparait sous la forme JURIS-CORPUS/XXXX -->
                <xsl:when test="$tokens[2]='DROIT-ASSOCIATIONS-FONDATIONS'"><value><xsl:value-of select="'Droit des associations et fondations'"/></value></xsl:when>
                <!-- Dalloz Professionnel: le code apparait sous la forme PCA/XXXX -->
                <xsl:when test="$tokens[2]='CONTENTIEUX-ADMINISTRATIF'"><value><xsl:value-of select="'Pratique du contentieux administratif'"/></value></xsl:when>
                <!-- Delmas: le code apparait sous la forme DELMAS/XXXX -->
                <xsl:when test="$tokens[2]='PROCEDURES-COLLECTIVES'"><value><xsl:value-of select="'Procédures collectives'"/></value></xsl:when>
                <xsl:otherwise>
                  <xsl:message>[WARNING] Code d'ouvrage inconnu <xsl:value-of select="$tokens[2]"/></xsl:message>
                  <xsl:message>Veuillez adapter le code du SAS d'import</xsl:message>
                  <value><xsl:value-of select="$tokens[2]"/></value>
                </xsl:otherwise>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
              <xsl:message>[ERROR] Ouvrage sans code!</xsl:message>
              <value><xsl:value-of select="'Ouvrage sans titre'"/></value>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:otherwise>
      </xsl:choose>
    </meta>
  </xsl:function>

	<xsl:template match="*" mode="metadata" priority="-1">
		<xsl:message>[ERROR] élément non matché en mode "metadata" : <xsl:value-of select="local-name(.)"/></xsl:message>
	</xsl:template>
</xsl:stylesheet>

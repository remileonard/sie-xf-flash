﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:dalloz="http://www.lefebvre-sarrut.eu/ns/dalloz" 
  xmlns:h="http://www.w3.org/1999/xhtml" 
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"  
  exclude-result-prefixes="#all" 
  version="2.0">
  
  <xsl:import href="../../xf-sas-common.xsl"/>
  <xsl:import href="ouvragesDalloz2editorialEntity.makeRelations.xsl"/>
  <xsl:import href="ouvragesDalloz.getGeneratedMetadata.xsl"/>
  
  <xsl:output method="xml" indent="no"/>
  
  <xsl:param name="rel.to.schema" select="'xf-models:/editorialEntity'" as="xs:string"/>
  <xsl:param name="xfSAS:company" select="'dalloz'" as="xs:string"/>
  <xsl:param name="outDir" select="base-uri()" as="xs:string"/>

  <xsl:variable name="codeModele" select="'ouvrages'" as="xs:string"/>
  <xsl:variable name="codePrefixTee" select="'DZ_TEE_'" as="xs:string"/>
  <xsl:variable name="codePrefixTr" select="'DZ_TR_'" as="xs:string"/>
  <xsl:variable name="codePrefixMeta" select="'DZ_META_'" as="xs:string"/>
  <xsl:variable name="contentNode.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/dalloz/ouvrages'" as="xs:string"/>
  
  <xsl:template match="/">
    <xsl:call-template name="xfSAS:makeXmlModelPi"/>
    
    <xsl:variable name="step0-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates/>
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step0-xml2ee.log.xml">
      <xsl:sequence select="$step0-xml2ee"/>
    </xsl:result-document>
    
    <xsl:variable name="step1-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step0-xml2ee" mode="fix-paras"/>
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step1-xml2ee.log.xml">
      <xsl:sequence select="$step1-xml2ee"/>
    </xsl:result-document>
    
    <xsl:variable name="step2-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step1-xml2ee" mode="structuralNode"/>
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step2-xml2ee.log.xml">
      <xsl:sequence select="$step2-xml2ee"/>
    </xsl:result-document>
    
    <!-- Normalisation des méta -->
    <xsl:variable name="step3-fixMeta" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step2-xml2ee" mode="xfSAS:fixMeta"/>        
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step3-fixMeta.log.xml">
      <xsl:sequence select="$step3-fixMeta"/>
    </xsl:result-document>
    
    <!-- Gestion des relations --> 
    <xsl:variable name="step4-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step3-fixMeta" mode="makeRelations"/>
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step4-xml2ee.log.xml">
      <xsl:sequence select="$step4-xml2ee"/>
    </xsl:result-document>
    
    <xsl:variable name="step5-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step4-xml2ee" mode="fixIds"/>
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step5-xml2ee.log.xml">
      <xsl:sequence select="$step5-xml2ee"/>
    </xsl:result-document>
    <xsl:sequence select="$step5-xml2ee"/>
  </xsl:template>
  
  <xsl:variable name="idRoot" select="/OUVRAGE-DALLOZ/@ID" as="xs:string?"/>
  
  <xsl:template match="@ID">
    <xsl:attribute name="id" select="."/>
  </xsl:template>
  
  <xsl:template match="@REFID">
    <xsl:attribute name="idRef" select="."/>
  </xsl:template>
  
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="TABLEAU[parent::NIV-TEXTE]" mode="fix-paras">
    <PARA xmlns="" id="{dalloz:Id(., $idRoot)}">
      <PARA-TEXT>
        <xsl:copy>
          <xsl:apply-templates select="@*|node()" mode="#current"/>
        </xsl:copy>
      </PARA-TEXT>
    </PARA>
  </xsl:template>
  
  <xsl:template match="NIVO-CORPS/NIV-TEXTE/PARA[1][not(@id)]" mode="fix-paras" priority="1">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="PARA[not(@id)]|PARA-LEGIS[not(@id)]|PARA-JURIS[not(@id)]|PARA-QUEST[not(@id)]|PARA-BIBLIO[not(@id)]" mode="fix-paras">
    <xsl:copy>
      <xsl:attribute name="id" select="dalloz:Id(., $idRoot)"/>
      <xsl:apply-templates select="@*|node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="REF" mode="fix-paras">
    <xsl:copy>
      <xsl:attribute name="TYPE" select="upper-case(@TYPE)"/>
      <xsl:apply-templates select="@* except @TYPE" mode="#current"/>
      <xsl:apply-templates select="node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="@*|node()" mode="fix-paras">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--===============================-->
  <!--STRUCTURAL NODES -->
  <!--===============================-->
  
  <xsl:template match="*" mode="structuralNode">
    <xsl:message>Structural Mode Called on <xsl:value-of select="local-name(.)"></xsl:value-of></xsl:message>
  </xsl:template>
  
  <xsl:template match="text()" mode="structuralNode"/>

  <xsl:template match="OUVRAGE-DALLOZ" mode="structuralNode">
    <editorialEntity code="{concat($codePrefixTee, $codeModele)}"
      xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
      xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst">
      <xsl:attribute name="xf:id" select="dalloz:HistoricalId(.)"/>
      <xsl:call-template name="xfSAS:addAttributeOwnerGroup"/>
      <metadata>
        <meta code="{concat($codePrefixMeta, 'ouvrages', 'Origine')}">
          <value><xsl:value-of select="local-name(.)"/></value>
        </meta>
        <xsl:copy-of select="dalloz:metaCollectionType(.)"/>
        <xsl:copy-of select="dalloz:metaTitle(.)"/>
        <xsl:apply-templates select="@*" mode="metadata"/>
        <xsl:apply-templates select="METADONNEES" mode="metadata"/>
      </metadata>
      <body>
        <xsl:apply-templates select="* except METADONNEES" mode="structuralNode"/>
      </body>
    </editorialEntity>
  </xsl:template>
  
  <xsl:template match="CORPUS" mode="structuralNode">
    <structuralNode>
      <xsl:attribute name="xf:id" select="dalloz:HistoricalId(.)"/>
      <metadata>
        <meta code="{concat($codePrefixMeta, 'ouvrages', 'Origine')}">
          <value><xsl:value-of select="local-name(.)"/></value>
        </meta>
        <xsl:apply-templates select="@*" mode="metadata"/>
      </metadata>
      <title>
        <div xmlns="http://www.w3.org/1999/xhtml">
            <span>CORPUS</span>
        </div>
      </title>
      <xsl:apply-templates select="* except METAS" mode="structuralNode"/>
    </structuralNode>
  </xsl:template>
  
  <xsl:template match="PRELIMINAIRES" mode="structuralNode">
    <structuralNode>
      <xsl:attribute name="xf:id" select="dalloz:HistoricalId(.)"/>
      <metadata>
        <meta code="{concat($codePrefixMeta, 'ouvrages', 'Origine')}">
          <value><xsl:value-of select="local-name(.)"/></value>
        </meta>
        <xsl:apply-templates select="@*" mode="metadata"/>
      </metadata>
      <title>
        <div xmlns="http://www.w3.org/1999/xhtml">
          <span>PRELIMINAIRES</span>
        </div>
      </title>
      <xsl:apply-templates select="* except METAS" mode="structuralNode"/>
    </structuralNode>
  </xsl:template>
  
  <xsl:template match="COMPLEMENTS" mode="structuralNode">
    <structuralNode>
      <xsl:attribute name="xf:id" select="dalloz:HistoricalId(.)"/>
      <metadata>
        <meta code="{concat($codePrefixMeta, 'ouvrages', 'Origine')}">
          <value><xsl:value-of select="local-name(.)"/></value>
        </meta>
        <xsl:apply-templates select="@*" mode="metadata"/>
      </metadata>
      <title>
        <div xmlns="http://www.w3.org/1999/xhtml">
          <span>COMPLEMENTS</span>
        </div>
      </title>
      <xsl:apply-templates select="* except METAS" mode="structuralNode"/>
    </structuralNode>
  </xsl:template>
  
  <xsl:template match="ANNEXES" mode="structuralNode">
    <structuralNode>
      <xsl:attribute name="xf:id" select="dalloz:HistoricalId(.)"/>
      <metadata>
        <meta code="{concat($codePrefixMeta, 'ouvrages', 'Origine')}">
          <value><xsl:value-of select="local-name(.)"/></value>
        </meta>
        <xsl:apply-templates select="@*" mode="metadata"/>
      </metadata>
      <title>
        <div xmlns="http://www.w3.org/1999/xhtml">
          <span>ANNEXES</span>
        </div>
      </title>
      <xsl:apply-templates select="ANNEXE" mode="structuralNode"/>
    </structuralNode>
  </xsl:template>  
  
  
  <xsl:template match="NIVEAU1|NIVEAU2|NIVEAU3|NIVEAU4|NIVEAU5|NIVEAU6|ANNEXE" mode="structuralNode">
    <structuralNode>
      <xsl:attribute name="xf:id" select="dalloz:HistoricalId(.)"/>
      <metadata>
        <meta code="{concat($codePrefixMeta, 'ouvrages', 'Origine')}">
          <value><xsl:value-of select="local-name(.)"/></value>
        </meta>
        <xsl:apply-templates select="@*" mode="metadata"/>
        <xsl:apply-templates select="METAS" mode="metadata"/>
        <xsl:apply-templates select="TITRE" mode="metadata"/>
        <xsl:apply-templates select="NIVO-PRELIM/AUTEUR" mode="metadata"/>
      </metadata>
      <xsl:if test="TITRE">
        <title>
          <div xmlns="http://www.w3.org/1999/xhtml">
            <xsl:for-each select="TITRE//(TIT-NIV|TIT-NUM), TITRE/TIT-INTITULE/(TIT-INTT|TIT-COMPL)">
              <xsl:if test="position() &gt; 1">
                <xsl:choose>
                  <xsl:when test="self::TIT-INTT or self::TIT-COMPL[not(preceding-sibling::TIT-INTT)]">
                    <xsl:value-of select="'&#160;-&#160;'"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="'&#160;'"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
              <span class="{local-name(.)}"><xsl:apply-templates mode="xfRes:title2html"/></span>
            </xsl:for-each>
          </div>
        </title>
      </xsl:if>
      <xsl:apply-templates select="* except METAS" mode="structuralNode"/>
    </structuralNode>
  </xsl:template>
  
  <xsl:template match="NIVO-CORPS" mode="structuralNode">
      <xsl:apply-templates select="* except METAS" mode="structuralNode"/>
  </xsl:template>
  
  <xsl:template match="NIVO-PRELIM | NIVO-COMPL" mode="structuralNode">
    <!-- TBD -->
  </xsl:template>
  
  <xsl:template match="TITRE" mode="structuralNode">
    <!-- TBD -->
  </xsl:template>
  
  <xsl:template match="INTRO|NIV1|NIV2|NIV3|NIV4|NIV5|NIV6|NIV7|NIV8|NIV9|NIV10|NIV11|NIV12|CONCL" mode="structuralNode">
    <structuralNode>
      <xsl:attribute name="xf:id" select="dalloz:HistoricalId(.)"/>
      <metadata>
        <meta code="{concat($codePrefixMeta, 'ouvrages', 'Origine')}">
          <value><xsl:value-of select="local-name(.)"/></value>
        </meta>
        <xsl:apply-templates select="@*" mode="metadata"/>
        <xsl:apply-templates select="METAS" mode="metadata"/>
      </metadata>
      <xsl:if test="TITRE">
        <title>
          <div xmlns="http://www.w3.org/1999/xhtml">
            <xsl:for-each select="TITRE//(TIT-NIV|TIT-NUM), TITRE/TIT-INTITULE/(TIT-INTT|TIT-COMPL)">
              <xsl:if test="position() &gt; 1">
                <xsl:choose>
                  <xsl:when test="self::TIT-INTT or self::TIT-COMPL[not(preceding-sibling::TIT-INTT)]">
                    <xsl:value-of select="'&#160;-&#160;'"/>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="'&#160;'"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:if>
              <span class="{local-name(.)}"><xsl:apply-templates mode="xfRes:title2html"/></span>
            </xsl:for-each>
          </div>
        </title>
      </xsl:if>
      <xsl:apply-templates select="* except METAS" mode="structuralNode"/>
    </structuralNode>
  </xsl:template>
  
  <xsl:template match="NIV-TEXTE" mode="structuralNode"/>
  
  <xsl:template match="NIV-TEXTE[PARA|PARA-LEGIS|PARA-JURIS|PARA-QUEST|PARA-BIBLIO]" mode="structuralNode">
    <xsl:apply-templates select="* except METAS" mode="structuralNode"/>
  </xsl:template>
 
  <xsl:template match="PGTITRE" mode="structuralNode">
    <!-- TBD -->
  </xsl:template>
  
  <xsl:template match="INDEX" mode="structuralNode">
    <!-- TBD -->
  </xsl:template>
  
  <xsl:template match="PARA|PARA-LEGIS|PARA-JURIS|PARA-QUEST|PARA-BIBLIO" mode="structuralNode">
    <referenceNode code="{concat($codePrefixTr, 'accrochage_Ouvrage_OuvragePara')}">
      <ref xf:targetResId="{dalloz:HistoricalId(.)}" xf:refType="xf:referenceNode">
        <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
          xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
          code="{concat($codePrefixTee, $codeModele, 'Paragraphe')}">
          <xsl:call-template name="xfSAS:addAttributeOwnerGroup"/>
          <xsl:attribute name="xf:id" select="dalloz:HistoricalId(.)"/>
          <metadata>
            <meta code="{concat($codePrefixMeta, 'ouvrages', 'Origine')}">
              <value><xsl:value-of select="local-name(.)"/></value>
            </meta>
            <xsl:apply-templates select="@*" mode="metadata"/>
            <xsl:apply-templates select="NUME" mode="metadata"/>
            <xsl:apply-templates select="METAS" mode="metadata"/>
          </metadata>
          <body>
            <xsl:apply-templates select="." mode="contentNode"/>
          </body>
        </editorialEntity>
      </ref>
    </referenceNode>
  </xsl:template>
  
  <xsl:template match="PARA|PARA-LEGIS|PARA-JURIS|PARA-QUEST|PARA-BIBLIO" mode="contentNode">
    <contentNode>
      <content>
        <xsl:apply-templates select="." mode="convertNamespace">
          <xsl:with-param name="namespace.uri" select="$contentNode.namespace.uri" tunnel="yes"/>
        </xsl:apply-templates>
      </content>
    </contentNode>
  </xsl:template>
  
  <xsl:template match="*" mode="contentNode">
    <xsl:message>Content Mode Called on <xsl:value-of select="local-name(.)"></xsl:value-of></xsl:message>
  </xsl:template>
  
  <xsl:template match="FOOTNOTE" mode="xfRes:title2html"/>
  
  <!--==================================================-->
  <!--MODE convertNamespace -->
  <!--==================================================-->
  
  <xsl:template match="METAS" mode="convertNamespace"/>
  
  <xsl:template match="TABLE-CALS" mode="convertNamespace">
    <xsl:param name="namespace.uri" required="yes" tunnel="yes"/>
    <xsl:element name="table" namespace="{$namespace.uri}">
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="*" mode="convertNamespace" priority="-1">
    <xsl:param name="namespace.uri" required="yes" tunnel="yes"/>
    <xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="convertNamespace" priority="-2">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="@id" mode="fixIds">
    <xsl:attribute name="id" select="replace(., '/', '_slash_')"/>
  </xsl:template>
  
  <xsl:template match="@idRef" mode="fixIds">
    <xsl:attribute name="idRef" select="replace(., '/', '_slash_')"/>
  </xsl:template>
  
  <xsl:template match="@xf:idRef" mode="fixIds">
    <xsl:attribute name="xf:idRef" select="replace(., '/', '_slash_')"/>
  </xsl:template>
  
  <xsl:template match="@*|node()" mode="fixIds">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--==================================================-->
  <!--COMMON"-->
  <!--==================================================-->
  
  <xsl:function name="dalloz:Id" as="xs:string">
    <xsl:param name="context" as="element()"/>
    <xsl:param name="idRoot" as="xs:string?"/>
    <xsl:choose>
      <xsl:when test="not(empty($context/@id))">
        <xsl:value-of select="$context/@id"/>
      </xsl:when>
      <xsl:when test="$context/ancestor::*/@id">
        <xsl:variable name="ancestor-with-id" select="($context/ancestor::*[@id])[1]"/>
        <xsl:variable name="xpath" select="els:get-xpath($context)"/>
        <xsl:variable name="ancestor-xpath" select="els:get-xpath($ancestor-with-id)"/>
        <xsl:value-of select="concat($ancestor-with-id/@id, translate(translate(substring-after($xpath, $ancestor-xpath), '[', '-'), ']', ''))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>[ERROR] Structure du document fausse!</xsl:message>
        <xsl:message terminate="yes">[ERROR] Element <xsl:value-of select="local-name($context)"/> sans ID ni ancetre portant un ID.</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="dalloz:HistoricalId" as="xs:string">
    <xsl:param name="context" as="element()"/>
    <xsl:value-of select="xfSAS:makeHistoricIdAttribute($context, dalloz:Id($context, $idRoot))"/>
  </xsl:function>
  
  <xsl:function name="dalloz:ucfirst">
    <xsl:param name="in"/>
    <xsl:copy-of select="concat(upper-case(substring($in, 1, 1)), lower-case(substring($in, 2)))"/>
  </xsl:function>
  
  <xsl:template name="xfSAS:addAttributeOwnerGroup">   
    <!--<xsl:attribute name="ownerGroup" select="'Groupe-Ouvrage-Dalloz'"/> 42 -->
    <xsl:attribute name="ownerGroup" select="'DZ_GRP_ouvrage'"/>
  </xsl:template>

</xsl:stylesheet>

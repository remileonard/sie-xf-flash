<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
                xmlns:html="http://www.w3.org/1999/xhtml"
                xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:infoCommentaire="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
                xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
                xmlns:local="local"
                xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                exclude-result-prefixes="#all"
                version="2.0">



  <xsl:key name="getElementByXfId" match="*[@xf:id]" use="@xf:id"/>

  <xsl:param name="xfSAS:company" select="'efl'" as="xs:string"/>
  <xsl:param name="xfSAS:rel.to.schema" select="'xf-models:/xf-models/editorialEntity/'"/>
  <xsl:param name="outputDir" select="concat(tokenize(base-uri(), '/')[last()], '_serialized')" as="xs:string"/>
  <xsl:param name="EEID_origin" as="xs:string?"/>
  <xsl:param name="EEID_calculated" as="xs:string?"/>
  <xsl:param name="baseId" as="xs:string?" select="''"/>
  <xsl:param name="XProcPipeline" select="false()" as="xs:boolean"/>
  


  <!--==========================================-->
  <!-- MAIN -->
  <!--==========================================-->

  <!-- Si la XSL est lancée via XProc, l'ID de l'EE à extraire a été préalablement sélectionné et la création du fichier XML est gérée côté XProc -->
  <xsl:template match="/*[$XProcPipeline = true() and $EEID_origin != '']">
    <xsl:variable name="this.EE" select="descendant-or-self::editorialEntity[@xf:id = $EEID_origin]" as="element(editorialEntity)"/>
    <!--<xsl:if test="$EEID_calculated != local:getId($this.EE,$this.EE/@xf:id)">
      <xsl:message terminate="yes">Les ID ne sont pas les mêmes ! <xsl:value-of select="$EEID_calculated"/> / <xsl:value-of select="local:getId($this.EE,$this.EE/@xf:id)"/></xsl:message>
    </xsl:if>-->
    <xsl:apply-templates select="$this.EE" mode="EEInternal"/>
  </xsl:template>
  
  <!-- Si la XSL n'est pas lancée via XProc, split de chaque EE via <xsl:result-document> -->
  <xsl:template match="editorialEntity[$XProcPipeline = false()]">
    <xsl:result-document href="{$outputDir}/{local:getId(.,@xf:id)}.xml">
      <xsl:apply-templates select="." mode="EEInternal"/>
    </xsl:result-document>
  </xsl:template>
  
  <!-- Fallback pour les EE dont @xf:id != $EEID_origin lorsque la XSL est lancée via XProc -->
  <xsl:template match="editorialEntity"/>
  
  <!-- Traitement d'une EE lorsque la XSL n'est pas lancée via XProc / traitement de l'EE dont @xf:id = $EEID_origin lorsque la XSL est lancée via XProc -->
  <xsl:template match="editorialEntity" mode="EEInternal">
      <xsl:call-template name="xf:makeXmlModelPi"/>
      <xsl:copy>
        <xsl:attribute name="status" select="'draft'"/>
        <xsl:attribute name="archived" select="'false'"/>
        <xsl:attribute name="version" select="'0.2'"/>
        <xsl:attribute name="ownerGroup" select="'efl:0'"/>
        <xsl:call-template name="addCommonAttributes"/>
        <xsl:apply-templates select="@* | node()"/>
      </xsl:copy>
  </xsl:template>
  
  
  <xsl:template match="structuralNode[not(metadata)] | referenceNode[not(metadata)] | contentNode[not(metadata)] | queryNode[not(metadata)]">
    <xsl:copy>
      <xsl:attribute name="xf:id" select="xf:generate-id(.)"/>
      <xsl:call-template name="addCommonAttributes"/>
      <xsl:apply-templates select="@*"/>
      <metadata/>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="structuralNode | referenceNode | contentNode | queryNode">
    <xsl:copy>
      <xsl:attribute name="xf:id" select="xf:generate-id(.)"/>
      <xsl:call-template name="addCommonAttributes"/>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
  
  
  <!-- Verbalization pour referenceNode -->
  <xsl:template match="referenceNode[not(verbalization)]/*[last()]">
    <xsl:next-match/>
    <verbalization>
      <div xmlns="http://www.w3.org/1999/xhtml">
        <xsl:sequence select="parent::referenceNode/ref/editorialEntity/metadata/meta[@code = 'systemTitle']/value/html:div/node()"/>
      </div>
    </verbalization>
  </xsl:template>


  <!-- Métadonnées -->
  <xsl:template match="meta">
    <xsl:copy>
      <!--<xsl:attribute name="xf:id" select="xf:generate-id(.)"/>-->
      <xsl:attribute name="label" select="'Label à récupérer du TEE'"/>
      <xsl:attribute name="group" select="'groupX'"/>
      <xsl:attribute name="as" select="'xs:string+'"/>
      <xsl:apply-templates select="@*"/>
      <xsl:if test="@as = ''">
        <xsl:comment>@as est censé être récupéré du TEE (mricaud : mis xs:string+ ici par défaut)</xsl:comment>
      </xsl:if>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="meta/value">
    <xsl:copy>
      <xsl:attribute name="as" select="'xs:string'"/>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>


  <!-- Traitement global des @xf:id -->
  <xsl:template match="@xf:id">
    <xsl:attribute name="{name()}" select="local:getId(parent::*,.)"/>
  </xsl:template>
  
  <!-- Traitement global des @xf:idRef + @xf:targetResId
       -> Si l'attribut fait référence à une EE résolue du document, on utilise l'ID qui lui a été attribué par cette XSL
       -> Si c'est un ID de référentiel qui est pointé, il est restitué tel quel
       -> S'il est fait référence à une EE non-résolue (renvoiAutrui par exemple), on attribue un nouvel ID unique bidon -->
  <xsl:template match="@xf:idRef | @xf:targetResId">
    <xsl:attribute name="{name()}" select="local:getId((key('getElementByXfId',.),parent::*)[1],.)"/>
  </xsl:template>
  

  <!-- Traitement spécifique des idRef sur les relations -->
  <xsl:template match="content//*[@xf:idRef][@xf:refType = 'xf:relation']">
    <xsl:copy>
      <xsl:apply-templates select="@* except @xf:idRef"/>
      <xsl:attribute name="xf:idRef" select="xf:generate-id(key('getElementByXfId',@xf:idRef))"/>
      <!--<xsl:attribute name="xf:base" select="'.'"/>-->
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>

  <!-- Régénération des id sur les relations -->
  <xsl:template match="relation">
    <xsl:copy>
      <xsl:apply-templates select="@* except @xf:id"/>
      <xsl:attribute name="xf:id" select="xf:generate-id(.)"/>
      <xsl:comment>xf:id was <xsl:value-of select="@xf:id"/></xsl:comment>
      <xsl:if test="not(metadata)">
        <metadata/>
      </xsl:if>
      <xsl:apply-templates/>
    </xsl:copy>
  </xsl:template>


  <!-- mricaud :
       Pour stocker dans l'ECM ces attributs ne me paraissent pas utiles. 
       C'est en sortie de l'ECM qu'on les voudra (3e schema à prévoir)-->
  <!--<xsl:template match="meta[@code = 'typeObjet']">
		<xsl:copy>
			<xsl:apply-templates select="@*"/>
			<xsl:attribute name="as" select="'xs:string'"/>
			<xsl:attribute name="label" select="'xs:string'"/>
			<xsl:attribute name="group" select="''"/>
			<xsl:attribute name="xf:id" select="xf:generate-id()"/>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>-->



  <!--==========================================-->
  <!-- COMMON -->
  <!--==========================================-->
  
  <xsl:template name="addCommonAttributes">
    <xsl:attribute name="creationDateTime" select="format-dateTime(current-dateTime(),'[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')"/>
    <xsl:attribute name="creationUser" select="'cxml2ee'"/>
    <xsl:attribute name="lastModificationDateTime" select="format-dateTime(current-dateTime(),'[Y0001]-[M01]-[D01]T[H01]:[m01]:[s01]')"/>
    <xsl:attribute name="lastModificationUser" select="'cxml2ee'"/>
  </xsl:template>

  <xsl:template name="xf:makeXmlModelPi">
    <!-- Validation NVDL -->
    <xsl:processing-instruction name="xml-model">
			<xsl:text>type="application/xml" schematypens="http://purl.oclc.org/dsdl/nvdl/ns/structure/1.0" href="</xsl:text>
			<xsl:value-of select="$xfSAS:rel.to.schema"/>
			<xsl:text>editorialEntity.nvdl"</xsl:text>
		</xsl:processing-instruction>
    <xsl:text>&#10;</xsl:text>
    <!--<xsl:comment>
			<xsl:text>&lt;?xml-model type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0" href="</xsl:text>
			<xsl:value-of select="$xfSAS:rel.to.schema"/>
			<xsl:text>editorialEntity.rng"?&gt;</xsl:text>
		</xsl:comment>
    <xsl:text>&#10;</xsl:text>-->
    <!--<xsl:processing-instruction name="xml-model">
			<xsl:text>type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron" href="</xsl:text>
			<xsl:value-of select="$xfSAS:rel.to.schema"/>
			<xsl:text>editorialEntity.sch"</xsl:text>
		</xsl:processing-instruction>
    <xsl:text>&#10;</xsl:text>-->
  </xsl:template>

  <xsl:function name="local:getId" as="xs:string?">
    <xsl:param name="e" as="element()"/>
    <xsl:param name="id" as="attribute()"/>
    <xsl:choose>
      <xsl:when test="matches($id, '\{historicalId:.*?\}')">
        <!--<xsl:value-of select="substring-before(substring-after($e, '{historicalId:'), '}')"/>-->
        <xsl:value-of select="xf:generate-id($e)"/>
      </xsl:when>
      <xsl:when test="matches($id, '\{query:.*?\}')">
        <!--<xsl:value-of select="substring-before(substring-after($e, '{historicalId:'), '}')"/>-->
        <xsl:value-of select="xf:generate-id($e)"/>
      </xsl:when>
      <xsl:otherwise>
        <!--<xsl:message>[INFO] "<xsl:value-of select="$id"/>" ne matche pas {historicalId:xxx}, ni {query:xxx}</xsl:message>-->
        <xsl:value-of select="$id"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="xf:generate-id">
    <xsl:param name="e" as="element()"/>
    <xsl:value-of select="concat($baseId,generate-id($e))"/>
  </xsl:function>
  
  <xsl:template match="processing-instruction()[. &lt;&lt; /*] | comment()[. &lt;&lt; /*]"/>
  
  <xsl:template match="node() | @*">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:EL="http://www.lefebvre-sarrut.eu/ns/el" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl" 
  xmlns:etudeRed="http://www.lefebvre-sarrut.eu/ns/el/etudered" 
  xmlns:itg="http://www.infotrustgroup.com/" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" 
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xpath-default-namespace="" 
  exclude-result-prefixes="#all" 
  version="2.0">

  <xsl:import href="../xf-sas-common.xsl"/>
  <xsl:include href="normalisationEL.xsl"/>
  <xsl:include href="./convertNameSpace.xsl"/>
  <!--<xsl:include href="ELRelation.xsl"/>-->

  <xsl:param name="xfSAS:company" select="'el'" as="xs:string"/>

  <xsl:variable name="contentNode.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/el/etudered'" as="xs:string"/>
  <xsl:variable name="ownerGroup">EL_GRP_Etudes</xsl:variable>
  <xsl:variable name="user">88</xsl:variable>

  <xsl:output method="xml" indent="yes"/>

  <xsl:key name="getElementById" match="*[@id]" use="@id"/>

  <!--==================================================-->
  <!-- MAIN -->
  <!--==================================================-->

  <!--<xsl:template match="/">
		<xsl:call-template name="xfSAS:makeXmlModelPi"/>
		<xsl:variable name="step1-xml2ee" as="document-node()">
			<xsl:document>
				<xsl:apply-templates/>
			</xsl:document>
		</xsl:variable>
		<xsl:result-document href="log/step1-xml2ee.log.xml">
			<xsl:sequence select="$step1-xml2ee"/>
		</xsl:result-document>
		<xsl:apply-templates select="$step1-xml2ee" mode="makeRelations"/>
	</xsl:template>-->

  <xsl:template match="/">
    <xsl:call-template name="xfSAS:makeXmlModelPi"/>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template name="decoupeStructure">
    <xsl:param name="niveau" as="xs:integer"/>
    <xsl:param name="groupe" as="element()*"/>

    <!--   <xsl:for-each-group select="Tietude/following-sibling::*" group-starting-with="*[etudeRed:isStructuralNode(.,$niveau)]">-->
    <xsl:for-each-group select="$groupe" group-starting-with="*[etudeRed:isCutNode(.,$niveau)]">
      <xsl:variable name="levelElement" select="current-group()[1]"/>
      <xsl:choose>
        <!-- when the group is a group with children -->
        <xsl:when test="etudeRed:getStructuralNodeLevel($levelElement) &gt; 0">
          <structuralNode type="{$xfSAS:company}_{local-name($levelElement)}">
            <xsl:if test="current-group()[1]/@Id">
              <xsl:attribute name="xf:id" select="xfSAS:makeHistoricIdAttribute(., $levelElement/@Id)"/>
            </xsl:if>
            <metadata>
              <meta code="systemTitle">
                <value>
                  <xsl:value-of select="."/>
                </value>
              </meta>
              <xsl:sequence select="EL:createMetadataFromString('', 'level', string(etudeRed:getStructuralNodeLevel($levelElement)))"/>
              <xsl:apply-templates select="@*" mode="createMetadata">
                <xsl:with-param name="prefix" select="'ee.el.etudered.'"/>
              </xsl:apply-templates>
            </metadata>
            <title>
              <xsl:value-of select="."/>
            </title>
            <xsl:call-template name="decoupeStructure">
              <xsl:with-param name="niveau" select="$niveau + 1"/>
              <xsl:with-param name="groupe" select="current-group()[position() &gt; 1]"/>
            </xsl:call-template>
          </structuralNode>
        </xsl:when>
        <xsl:otherwise>
          <xsl:for-each-group select="current-group()" group-starting-with="Pnumti">
            <xsl:choose>
              <xsl:when test="current-group()[1]/self::Pnumti">
                <!--<xsl:when test="not(etudeRed:isStructuralNode(current-group()[1]))">-->
                <referenceNode code="EL_TR_accrochage_EL_Etudes_Pnum">
                  <ref xf:targetResId="{xfSAS:makeHistoricIdAttribute(., current-group()[1]/self::Pnumti/@Id)}" xf:targetResType="editorialEntity" xf:refType="xf:referenceNode">
                    <editorialEntity xf:id="{xfSAS:makeHistoricIdAttribute(., current-group()[1]/self::Pnumti/@Id)}" code="EL_TEE_pnum" creationDateTime="{current-dateTime()}" lastModificationDateTime="{current-dateTime()}" lastModificationUser="{$user}" creationUser="{$user}" ownerGroup="{$ownerGroup}">
                      <metadata>
                        <xsl:apply-templates select="@*" mode="createMetadata">
                          <xsl:with-param name="prefix" select="'ee.el.etudered.'"/>
                        </xsl:apply-templates>
                        <meta xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" code="systemTEE">
                          <xf:value>EL_TEE_pnum</xf:value>
                        </meta>
                        <meta xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" code="systemStatut">
                          <xf:value>PREPA</xf:value>
                        </meta>
                      </metadata>
                      <body>
                        <contentNode>
                          <content>
                            <Etudered xmlns="http://www.lefebvre-sarrut.eu/ns/el/etudered">
                              <xsl:apply-templates select="current-group()" mode="convertNamespace">
                                <xsl:with-param name="namespace.uri" select="$contentNode.namespace.uri" tunnel="yes"/>
                              </xsl:apply-templates>
                            </Etudered>
                          </content>
                        </contentNode>
                      </body>
                      <relations>
                        <!--<xsl:apply-templates select="current-group()" mode="makeRelation"/>-->
                      </relations>
                    </editorialEntity>
                  </ref>
                </referenceNode>
              </xsl:when>
              <xsl:when test="current-group()[1][etudeRed:getStructuralNodeLevel(.) = -100]">
                <xsl:variable name="code" select="upper-case(local-name(current-group()[1]))"/>
                <referenceNode code="EL_TR_accrochage_EL_Etudes_Pnum">
                  <ref xf:targetResId="{xfSAS:makeHistoricIdAttribute(., current-group()[1]/@Id)}" xf:targetResType="editorialEntity" xf:refType="xf:referenceNode">
                    <editorialEntity xf:id="{xfSAS:makeHistoricIdAttribute(., current-group()[1]/@Id)}" code="EL_TEE_pnum" creationDateTime="{current-dateTime()}" lastModificationDateTime="{current-dateTime()}" lastModificationUser="{$user}" creationUser="{$user}" ownerGroup="{$ownerGroup}">
                      <metadata>
                        <xsl:apply-templates select="@*" mode="createMetadata">
                          <xsl:with-param name="prefix" select="'ee.el.etudered.'"/>
                        </xsl:apply-templates>
                      </metadata>
                      <body>
                        <contentNode>
                          <content>
                            <Etudered xmlns="http://www.lefebvre-sarrut.eu/ns/el/etudered">
                              <xsl:apply-templates select="current-group()" mode="convertNamespace">
                                <xsl:with-param name="namespace.uri" select="$contentNode.namespace.uri" tunnel="yes"/>
                              </xsl:apply-templates>
                            </Etudered>
                          </content>
                        </contentNode>
                      </body>
                      <relations>
                        <!--<xsl:apply-templates select="current-group()" mode="makeRelation"/>-->
                      </relations>
                    </editorialEntity>
                  </ref>
                </referenceNode>
              </xsl:when>
            </xsl:choose>
          </xsl:for-each-group>
        </xsl:otherwise>
      </xsl:choose>

    </xsl:for-each-group>
  </xsl:template>

  <xsl:template match="/Etudered">
    <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" code="EL_TEE_etudes" creationDateTime="{current-dateTime()}" lastModificationDateTime="{current-dateTime()}" lastModificationUser="{$user}" creationUser="{$user}" ownerGroup="{$ownerGroup}" xf:id="{xfSAS:makeHistoricIdAttribute(., @Id)}">
      <xsl:namespace name="xf">http://www.lefebvre-sarrut.eu/ns/xmlfirst</xsl:namespace>
      <metadata>
        <xsl:apply-templates select="@*" mode="createMetadata">
          <xsl:with-param name="prefix" select="'ee.el.'"/>
        </xsl:apply-templates>
        <xsl:sequence select="EL:createMetadataFromString('', 'systemTitle', string(Tietude))"/>
        <meta xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" code="systemTEE">
          <xf:value>EL_TEE_etudes</xf:value>
        </meta>
        <meta xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" code="systemStatut">
          <xf:value>PREPA</xf:value>
        </meta>
      </metadata>
      <!--    <title>
        <div xmlns="http://www.w3.org/1999/xhtml">
          <xsl:apply-templates select="Tietude" mode="etudeRed2html"/>
        </div>
      </title>
 -->
      <body>
        <xsl:variable name="niveauDepart" select="etudeRed:getStructuralNodeLevel((Partie | Chapitre | Titre | Section | Ssection | Ti1 | Ti2 | Ti3)[1])"/>
        <xsl:call-template name="decoupeStructure">
          <xsl:with-param name="niveau" select="$niveauDepart"/>
          <xsl:with-param name="groupe" select="*"/>
        </xsl:call-template>
      </body>
      <relations>
        <!--<xsl:apply-templates select="." mode="makeRelation"/>-->
      </relations>
    </editorialEntity>
  </xsl:template>

  <!--==================================================-->
  <!-- STRUCTURALNODE -->
  <!--==================================================-->

  <!--  <xsl:template match="*[etudeRed:isStructuralNode(.)]">
    <title>
      <div xmlns="http://www.w3.org/1999/xhtml">
        <xsl:apply-templates mode="etudeRed2html"/>
      </div>
    </title>
  </xsl:template>
-->
  <!--<xsl:template match="*[etudeRed:isStructuralNode(.)]" mode="metadata">
		<!-\-?-\->
	</xsl:template>-->

  <!--==================================================-->
  <!-- CONTENTNODE -->
  <!--==================================================-->

  <!--==================================================-->
  <!-- METADATA -->
  <!--==================================================-->

  <!-- ========= EE REVUE =========-->

  <xsl:template match="Etudered" mode="metadata">
    <xsl:apply-templates select="@* except @Id" mode="#current"/>
  </xsl:template>

  <xsl:template match="@Datemaj" mode="metadata">
    <meta code="{$xfSAS:company}_{local-name(.)}" as="xs:date">
      <value as="xs:date">
        <xsl:value-of select="els:date-string-to-number-slash(.)"/>
      </value>
      <verbalization>
        <span xmlns="http://www.w3.org/1999/xhtml">
          <xsl:value-of select="."/>
        </span>
      </verbalization>
    </meta>
  </xsl:template>

  <xsl:template match="@Ref | @Derid | @Dertap | @Grp" mode="metadata">
    <meta code="{$xfSAS:company}_{local-name(.)}" as="xs:integer">
      <value as="xs:integer">
        <xsl:value-of select="."/>
      </value>
    </meta>
  </xsl:template>

  <xsl:template match="@Maj" mode="metadata">
    <meta code="{$xfSAS:company}_{local-name(.)}" as="xs:string">
      <value as="xs:string">
        <xsl:value-of select="."/>
      </value>
    </meta>
  </xsl:template>

  <!-- ========= EE PNUMTI =========-->

  <xsl:template match="Pnumti" mode="metadata">
    <xsl:apply-templates select="@* except @Id" mode="#current"/>
  </xsl:template>

  <xsl:template match="Pnumti/@Typemaj | Pnumti/@Indtap" mode="metadata">
    <meta code="{$xfSAS:company}_{local-name(.)}" as="xs:string">
      <value as="xs:string">
        <xsl:value-of select="."/>
      </value>
    </meta>
  </xsl:template>

  <xsl:template match="@*" mode="metadata" priority="-1">
    <xsl:message>[INFO] méta <xsl:value-of select="local-name(.)"/> non convertie : <xsl:value-of select="els:get-xpath(.)"/></xsl:message>
  </xsl:template>

  <!--	<!-\-==================================================-\->
	<!-\-STRUCTURAL NODE-\->
	<!-\-==================================================-\->
	
	<xsl:template match="document">
		<body>
			<xsl:apply-templates select="arbreDocument"/>
		</body>
	</xsl:template>
	
	<xsl:template match="arbreDocument">
		<xsl:apply-templates/>
	</xsl:template>
	
	<xsl:template match="docNiv">
		<xsl:variable name="marqueursStructuralNode" select="('sommaire', 'newsLetter', 'couverture', 'rjEssentiel', 'corpus', 'rub', 'rjDoctrine', 'rjDecisions', 'rjConclusions', 'abreviations')" as="xs:string*"/>
		<xsl:variable name="marqueursNotStructural" select="replace(@marqueurs, string-join($marqueursStructuralNode, '|'), '')" as="xs:string*"/>
		<structuralNode xf:id="{xfSAS:makeHistoricIdAttribute(., @id)}">
			<xsl:if test="cx:hasMarqueur(., $marqueursStructuralNode)">
				<xsl:choose>
					<xsl:when test="count(tokenize(@marqueurs, $els:regAnySpace)[. = $marqueursStructuralNode]) = 1">
						<xsl:attribute name="xf:type" select="tokenize(@marqueurs, $els:regAnySpace)[. = $marqueursStructuralNode]"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:message>[ERROR] plusieurs marqueurs structurels : <xsl:value-of select="tokenize(@marqueurs, $els:regAnySpace)[. = $marqueursStructuralNode]"/></xsl:message>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:if>
			<xsl:if test="normalize-space($marqueursNotStructural)">
				<metadata>
					<xsl:for-each select="$marqueursNotStructural">
						<meta code="marqueur" label="Marqueur" as="xf:referenceItemRef">
							<value as="xf:referenceItemRef">
								<ref
									xf:idRef="{.}"
									xf:base="{$xfSAS:REST.referenceTable.uri}marqueurs.xml"
									xf:refType="xf:referenceItemRef"/>
							</value>
						</meta>
					</xsl:for-each>
				</metadata>
			</xsl:if>
			<xsl:apply-templates/>
		</structuralNode>
	</xsl:template>
	
	<xsl:template match="docNiv/titre">
		<title>
			<div xmlns="http://www.w3.org/1999/xhtml">
				<xsl:apply-templates mode="etudeRed2html"/>
			</div>
		</title>
	</xsl:template>

	<!-\-==================================================-\->
	<!-\-UI-\->
	<!-\-==================================================-\->
	
	<xsl:template match="ui[not(ancestor::docNiv[cx:hasMarqueur(., 'corpus')])]">
		<xsl:choose>
			<xsl:when test="key('getElementById', @id)[self::ui][ancestor::docNiv[cx:hasMarqueur(., 'corpus')]]">
				<referenceNode>
					<ref xf:idRef="{xfSAS:makeHistoricIdAttribute(., @id)}" xf:base="{$xfSAS:REST.ee.efl.revues.uri}" xf:refType="xf:referenceNode"/>
				</referenceNode>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message>UI "<xsl:value-of select="@id"/>" est référencée quelque part (sommaire, couv ou autre) mais elle n'est pas dans le corpus</xsl:message>
				<xsl:next-match/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="ui">
		<referenceNode>
			<metadata>
				<meta code="numeroInfo" label="Numéro d'info">
					<value><xsl:value-of select="count(preceding::ui[ancestor::docNiv[cx:hasMarqueur(., 'corpus')]])"/></value>
				</meta>
			</metadata>
			<ref xf:idRef="{xfSAS:makeHistoricIdAttribute(., @id)}" xf:base="{$xfSAS:REST.ee.efl.revues.uri}" xf:refType="xf:referenceNode">
				<editorialEntity type="EFL_INFO" xf:id="{xfSAS:makeHistoricIdAttribute(., @id)}">
					<xsl:apply-templates/>
				</editorialEntity>
			</ref>
		</referenceNode>
	</xsl:template>
	
	<xsl:template match="*[cx:isInfo(.)]">
		<body>
			<contentNode>
				<content>
					<xsl:apply-templates select="." mode="convertNamespace"/>
				</content>
			</contentNode>
		</body>
	</xsl:template>
	

-->

  <!--==================================================-->
  <!--MODE "etudeRed2html"-->
  <!--==================================================-->

  <xsl:template match="Lettrine" mode="etudeRed2html">
    <span class="Lettrine" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>

  <xsl:template match="Titret" mode="etudeRed2html">
    <span class="Titret" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>

  <xsl:template match="Coupe" mode="etudeRed2html">
    <br xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="@*" mode="#current"/>
    </br>
  </xsl:template>

  <xsl:template match="Coupe/@Type" mode="etudeRed2html">
    <xsl:attribute name="class" select="."/>
  </xsl:template>

  <xsl:template match="E" mode="etudeRed2html">
    <sup xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </sup>
  </xsl:template>

  <xsl:template match="I" mode="etudeRed2html">
    <em xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </em>
  </xsl:template>

  <xsl:template match="Tietude" mode="etudeRed2html">
    <div class="{local-name()}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </div>
  </xsl:template>

  <!--<xsl:template match="ind" mode="etudeRed2html">
		<sub>
			<xsl:apply-templates mode="#current"/>
		</sub>
	</xsl:template>-->

  <xsl:template match="*" mode="etudeRed2html" priority="-1">
    <xsl:message>[INFO] <xsl:value-of select="local-name()"/> non converti en mode etudeRed2html</xsl:message>
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>




  <!--==================================================-->
  <!--COMMON-->
  <!--==================================================-->

  <!-- ID -->
  <xsl:template match="@id" mode="#default">
    <xsl:copy-of select="."/>
    <xsl:attribute name="xf:id" select="xfSAS:makeHistoricIdAttribute(parent::*, .)"/>
  </xsl:template>

  <xsl:template match="node() | @*">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>

  <xsl:function name="etudeRed:isCutNode" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:param name="niveau"/>

    <xsl:sequence select="etudeRed:getStructuralNodeLevel($e) = $niveau"/>
  </xsl:function>

  <xsl:function name="etudeRed:isStructuralNode" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:param name="niveau"/>

    <xsl:sequence select="etudeRed:getStructuralNodeLevel($e) = $niveau"/>
  </xsl:function>

  <xsl:function name="etudeRed:getStructuralNodeLevel" as="xs:integer">
    <xsl:param name="e" as="element()"/>

    <xsl:choose>
      <xsl:when test="local-name($e) = 'Tietude'">
        <xsl:sequence select="2"/>
      </xsl:when>
      <xsl:when test="local-name($e) = 'Partie'">
        <xsl:sequence select="2"/>
      </xsl:when>
      <xsl:when test="local-name($e) = 'Titre'">
        <xsl:sequence select="3"/>
      </xsl:when>
      <xsl:when test="local-name($e) = 'Chapitre'">
        <xsl:sequence select="4"/>
      </xsl:when>
      <xsl:when test="local-name($e) = 'Section'">
        <xsl:sequence select="5"/>
      </xsl:when>
      <xsl:when test="local-name($e) = 'Ssection'">
        <xsl:sequence select="6"/>
      </xsl:when>
      <xsl:when test="local-name($e) = 'Ti1'">
        <xsl:sequence select="7"/>
      </xsl:when>
      <xsl:when test="local-name($e) = 'Ti2'">
        <xsl:sequence select="8"/>
      </xsl:when>
      <xsl:when test="local-name($e) = 'Ti3'">
        <xsl:sequence select="9"/>
      </xsl:when>
      <xsl:when test="local-name($e) = 'Pnumti'">
        <xsl:sequence select="-50"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="-100"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:template match="@*" mode="createMetadata">
    <xsl:param name="prefix"/>
    <xsl:sequence select="EL:createMetadataFromString($prefix, local-name(.), string(.))"/>
  </xsl:template>


</xsl:stylesheet>

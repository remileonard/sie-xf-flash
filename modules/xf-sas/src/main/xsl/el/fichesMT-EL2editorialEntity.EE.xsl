<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl" 
  xmlns:formulaire_dp="http://www.lefebvre-sarrut.eu/ns/el/formulaire_dp"
  xmlns:EL="http://www.lefebvre-sarrut.eu/ns/el" 
  xmlns:itg="http://www.infotrustgroup.com/" 
  xmlns:ns1="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" 
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xpath-default-namespace="" exclude-result-prefixes="#all" version="2.0">
  <xsl:import href="../xf-sas-common.xsl"/>
  
  <xsl:include href="convertNameSpace.xsl"/>
  <xsl:include href="normalisationEL.xsl"/>
  <xsl:include href="ELRelation.xsl"/>
  
  <xsl:param name="inputFilename"  select="base-uri(.)"></xsl:param>
  
  <xsl:variable name="prefix.contentNode.namespace.fichesMT.uri" select="'http://www.lefebvre-sarrut.eu/ns/el/fichesMT_'" as="xs:string"/>
  <xsl:variable name="TEECode" select="'EL_TEE_ficheMT'"></xsl:variable>
  
  <xsl:variable name="filename" select="tokenize($inputFilename,'/')[position() = last()]"/>
  <xsl:variable name="infosFileName" select="tokenize(replace($filename,'(..)([^_]*)_([^_]*)_([^_])','$1|$2|$3|$4'),'\|')"/>
  <xsl:variable name="idProduit" select="$infosFileName[1]"/>
  <xsl:variable name="idFiche" select="$infosFileName[2]"/>
  <xsl:variable name="typeFiche" select="$infosFileName[3]"/>
  <xsl:variable name="titreFiche" select="$infosFileName[4]"/>
  <xsl:variable name="ownerGroup" select="'EL_GRP_gb'"/>
  <xsl:output method="xml" indent="yes"/>
  
  <xsl:key name="getElementById" match="*[@id]" use="@id"/>
  
  
  
  <xsl:template match="/DPFORM2" mode="xfSAS:fichesMT-EL2editorialEntity.EE">
    <xsl:variable name="modelName" select="$typeFiche"/>
    <editorialEntity 
      xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
      xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
      xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
      code="{$TEECode}" 
      xf:id="{xfSAS:makeHistoricIdAttribute(., concat($idProduit,'M',$idFiche))}">
      <xsl:namespace name="xf">http://www.lefebvre-sarrut.eu/ns/xmlfirst</xsl:namespace>
      <metadata>
        <xsl:sequence select="EL:createMetadataFromString('','systemTEE',$TEECode)"/>

        <xsl:sequence select="EL:createMetadataFromString('','systemStatut','Libre')"/>
        <xsl:sequence select="EL:createMetadataFromString('','systemArchive','False')"/>
        <xsl:sequence select="EL:createMetadataFromString('','systemVersion','0')"/>
        <xsl:sequence select="EL:createMetadataFromString('','systemGroupeProprietaire','EL_GRP_gb')"/>
        <xsl:sequence select="EL:createMetadataFromString('','EL_META_idEL',concat($idProduit,'M',$idFiche))"/>
        <xsl:call-template name="EL:createMetadataFromElement" >
          <xsl:with-param name="prefix" select="''"></xsl:with-param>
          <xsl:with-param name="name" select="'EL_META_Papl'"></xsl:with-param>
          <xsl:with-param name="element" select="Titrepapl | Alpapl"></xsl:with-param>
        </xsl:call-template>
        <xsl:sequence select="EL:createMetadataFromString('','EL_TR_accrochageFicheGB',./@GB)"/>
        <xsl:sequence select="EL:createMetadataFromString('','EL_META_accrochageFicheGB',./@GB)"/>
        <xsl:sequence select="EL:createMetadataFromString('','EL_META_mr',./@MR)"/>
        <xsl:sequence select="EL:createMetadataFromString('','EL_META_themeGB',./@Theme)"/>
        <xsl:sequence select="EL:createMetadataFromString('','EL_META_etudeDpform2Titre',Titrefiche)"/>
        <xsl:if test="./@Position">
          <xsl:sequence select="EL:createMetadataFromString('','EL_META_positionFiche',./@Position)"/>  
        </xsl:if>
        <xsl:if test="./@Eddversion">
          <xsl:sequence select="EL:createMetadataFromString('','EL_META_versionEDD',./@Eddversion)"/>
        </xsl:if>
        <xsl:sequence select="EL:createMetadataFromString('','EL_META_etudeDpform2Type',./@Type)"/>
      </metadata>
      <body>
        <contentNode>
          <content>
            <xsl:apply-templates select="." mode="convertNamespace">
              <xsl:with-param name="namespace.uri" select="concat($prefix.contentNode.namespace.fichesMT.uri,$modelName)" tunnel="yes"/>
            </xsl:apply-templates>
          </content>
        </contentNode>
      </body>
    </editorialEntity>
  </xsl:template>
  
  <xsl:template match="comment() | processing-instruction() | @*" mode="createMetadata"/>
  <xsl:template match="text()" mode="createMetadata">
    <xsl:value-of select="normalize-space(.)"/>
  </xsl:template>
  <!-- 
  Elements à traiter dans les meta :
  <!ELEMENT Tifp       (#PCDATA | E | IN | Coupe | Revision | Veuro |
                              Vinfouegal | Vmultiplie | Vsupouegal |
                              Vpourmille)* >
  <!ELEMENT Al         (#PCDATA | E | IN | Veuro | Vinfouegal | Vmultiplie
                              | Vsupouegal | Vpourmille | I | B | Pcap |
                              Renvoi | Renvoiexterne | Reftexte | Refjrp |
                              Refcode | Refna | Revision | Xacodes | GPAppelNT
                              | Blocnote | XSIT)* >
  <!ELEMENT PAPL       (Al | Renvoi | Renvoiexterne)* >
  -->
  <xsl:template match="*" mode="createMetadata">
    <xsl:sequence select="."/>
  </xsl:template>
  
 
  <!--<xsl:template match="*[etudeRed:isStructuralNode(.)]" mode="metadata">
		<!-\-?-\->
	</xsl:template>-->
  
  <!--==================================================-->
  <!-- CONTENTNODE -->
  <!--==================================================-->
  
  <!--==================================================-->
  <!-- METADATA -->
  <!--==================================================-->
  
  
  <!--==================================================-->
  <!--COMMON-->
  <!--==================================================-->
  
  <!-- ID -->
  <xsl:template match="@id|@ID" mode="xfSAS:fichesMT-EL2editorialEntity.EE" priority="10">
    <xsl:copy-of select="."/>
    <xsl:attribute name="xf:id" select="xfSAS:makeHistoricIdAttribute(parent::*, string(.))"/>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="xfSAS:fichesMT-EL2editorialEntity.EE">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="xfSAS:fichesMT-EL2editorialEntity.EE"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
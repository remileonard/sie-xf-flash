<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:math="http://www.w3.org/2005/xpath-functions/math" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" exclude-result-prefixes="xs math xd" version="2.0">
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p><xd:b>Created on:</xd:b> Aug 22, 2016</xd:p>
      <xd:p><xd:b>Author:</xd:b> MASTER</xd:p>
      <xd:p/>
    </xd:desc>
  </xd:doc>
  <!--==================================================-->
  <!--MODE convertNamespace -->
  <!--==================================================-->

  <xsl:template match="*" mode="convertNamespace">
    <xsl:param name="namespace.uri" required="yes" as="xs:string" tunnel="yes"/>
    <xsl:param name="xf:idRef" as="xs:string?"/>
    <xsl:param name="xf:refType" as="xs:string?"/>
    <xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
      <xsl:if test="$xf:refType">
        <xsl:attribute name="xf:refType" select="$xf:refType"/>
      </xsl:if>
      <xsl:if test="$xf:idRef">
        <xsl:attribute name="xf:idRef" select="$xf:idRef"/>
      </xsl:if>
      <xsl:apply-templates select="node() | @*" mode="#current">
      </xsl:apply-templates>
    </xsl:element>
  </xsl:template>

  <xsl:template match="@id | @ID | @Id | @iD" mode="convertNamespace">
    <xsl:copy-of select="."/>
    <xsl:attribute name="xf:id" select="xfSAS:makeHistoricIdAttribute(parent::*, .)"/>
  </xsl:template>

  <xsl:template match="node() | @*" mode="convertNamespace" priority="-2">
    <xsl:copy>
      <xsl:copy-of select="node() | @*"/>
    </xsl:copy>
  </xsl:template>


</xsl:stylesheet>

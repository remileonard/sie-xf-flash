<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl" xmlns:formulaire_dp="http://www.lefebvre-sarrut.eu/ns/el/formulaire_dp"
  xmlns:EL="http://www.lefebvre-sarrut.eu/ns/el" xmlns:itg="http://www.infotrustgroup.com/" xmlns:ns1="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xpath-default-namespace="" exclude-result-prefixes="#all" version="2.0">
  
  <xsl:param name="inputFilename"  select="base-uri(.)"></xsl:param>
  
  <xsl:variable name="filename" select="tokenize($inputFilename,'/')[position() = last()]"/>
  <xsl:variable name="infosFileName" select="tokenize(replace($filename,'(..)([^_]*)_([^_]*)_([^_])','$1|$2|$3|$4'),'\|')"/>
  <xsl:variable name="idProduit" select="$infosFileName[1]"/>
  <xsl:variable name="idFiche" select="$infosFileName[2]"/>
  <xsl:variable name="typeFiche" select="$infosFileName[3]"/>
  <xsl:variable name="titreFiche" select="$infosFileName[4]"/>
  <xsl:variable name="ownerGroup" select="'EL_GRP_gb'"/>
  <xsl:output method="xml" indent="yes"/>
  
  <xsl:key name="getElementById" match="*[@id]" use="@id"/>
  
  
  
  <xsl:template match="/FPRO" mode="xfSAS:fichesPRO-EL2editorialEntity.EE">
    <xsl:variable name="modelName" select="$typeFiche"/>
    <editorialEntity 
      xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
      xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
      xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
      code="EL_TEE_fichePro" 
      xf:id="{xfSAS:makeHistoricIdAttribute(., concat($idProduit,'FPRO',$idFiche))}"
      ownerGroup="{$ownerGroup}" creationUser="88">
      <xsl:namespace name="xf">http://www.lefebvre-sarrut.eu/ns/xmlfirst</xsl:namespace>
      <metadata>
        <xsl:sequence select="EL:createMetadataFromString('','systemTEE','fichePro')"/>
        <xsl:sequence select="EL:createMetadataFromString('','systemGroupeProprietaire','EL_GRP_gb')"/>
        <xsl:sequence select="EL:createMetadataFromString('','EL_META_idEL',concat($idProduit,'FPRO',$idFiche))"/>
        <xsl:sequence select="EL:createMetadataFromString('','EL_META_mr',(./@MR,'')[1])"/>
        <xsl:sequence select="EL:createMetadataFromString('','EL_META_positionFiche',(./@Position,'')[1])"/>
        <xsl:sequence select="EL:createMetadataFromString('','EL_META_versionEDD',(./@Eddversion,'')[1])"/>
      </metadata>
      <body>
        <contentNode>
          <content>
            <xsl:apply-templates select="." mode="convertNamespace">
              <xsl:with-param name="namespace.uri" select="concat($prefix.contentNode.namespace.fichePRO.uri,$modelName)" tunnel="yes"/>
            </xsl:apply-templates>
          </content>
        </contentNode>
      </body>
    </editorialEntity>
  </xsl:template>
  
  <xsl:template match="comment() | processing-instruction() | @*" mode="createMetadata"/>
  <xsl:template match="text()" mode="createMetadata">
    <xsl:value-of select="normalize-space(.)"/>
  </xsl:template>
  <!-- 
  Elements à traiter dans les meta :
  <!ELEMENT Tifp       (#PCDATA | E | IN | Coupe | Revision | Veuro |
                              Vinfouegal | Vmultiplie | Vsupouegal |
                              Vpourmille)* >
  <!ELEMENT Al         (#PCDATA | E | IN | Veuro | Vinfouegal | Vmultiplie
                              | Vsupouegal | Vpourmille | I | B | Pcap |
                              Renvoi | Renvoiexterne | Reftexte | Refjrp |
                              Refcode | Refna | Revision | Xacodes | GPAppelNT
                              | Blocnote | XSIT)* >
  <!ELEMENT PAPL       (Al | Renvoi | Renvoiexterne)* >
  -->
  <xsl:template match="*" mode="createMetadata">
    <xsl:sequence select="."/>
  </xsl:template>
  
 
  <!--<xsl:template match="*[etudeRed:isStructuralNode(.)]" mode="metadata">
		<!-\-?-\->
	</xsl:template>-->
  
  <!--==================================================-->
  <!-- CONTENTNODE -->
  <!--==================================================-->
  
  <!--==================================================-->
  <!-- METADATA -->
  <!--==================================================-->
  
  
  <!--==================================================-->
  <!--COMMON-->
  <!--==================================================-->
  
  <!-- ID -->
  <xsl:template match="@id" mode="xfSAS:fichesPRO-EL2editorialEntity.EE" priority="10">
    <xsl:copy-of select="."/>
    <xsl:attribute name="xf:id" select="xfSAS:makeHistoricIdAttribute(parent::*, string(.))"/>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="xfSAS:fichesPRO-EL2editorialEntity.EE">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="xfSAS:fichesPRO-EL2editorialEntity.EE"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
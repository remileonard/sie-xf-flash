<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl"
  xmlns:cx="http://www.lefebvre-sarrut.eu/ns/efl/chaineXml" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" xmlns:functx="http://www.functx.com" xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:Etudered="http://www.lefebvre-sarrut.eu/ns/el/etudered" xmlns:EL="http://www.lefebvre-sarrut.eu/ns/el" xmlns:h="http://www.w3.org/1999/xhtml" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst" exclude-result-prefixes="#all" version="2.0">
  <xsl:import href="../xf-sas-common.xsl"/>
  <xsl:param name="xfSAS:company" select="'el'" as="xs:string"/>

  <xsl:variable name="contentNode.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/el/etudered'" as="xs:string"/>

  <xsl:output method="xml" indent="yes"/>

  <xsl:include href="normalisationEL.xsl"/>

  <xsl:template match="/">
    <xsl:apply-templates select="." mode="makeRelations"/>
  </xsl:template>

  <xsl:template match="editorialEntity/body" mode="makeRelations">
    <xsl:next-match/>
    <relations>
      <xsl:apply-templates select="contentNode/content//Etudered:Xacode" mode="makeRelationElement"/>
      <xsl:apply-templates select="contentNode/content//Etudered:Xacodes" mode="makeRelationElement"/>
      <xsl:apply-templates select="contentNode/content//Etudered:Nomet" mode="makeRelationElement"/>
    </relations>
  </xsl:template>

  <!--=====================================-->
  <!--Refcode/Xacode-->
  <!--=====================================-->

  <xsl:template match="Etudered:Refcode/Etudered:Xacode" mode="makeRelationElement">
    <relation xf:id="{generate-id(.)}" type="generated">
      <!--<metadata>
				<xsl:if test="@TYPE">
					<meta code="relationDalloz_Type">
						<value><xsl:value-of select="@TYPE"/></value>
					</meta>
				</xsl:if>
			</metadata>-->
      <xsl:variable name="Refcode" select="parent::Etudered:Refcode" as="element()"/>
      <xsl:variable name="Refcode.Type" select="$Refcode/@Type" as="xs:string"/>
      <ref xf:base="{$xfSAS:REST.ee.textes.uri}" xf:refType="{$xfSAS:company}:{local-name(.)}"
        xf:idRef="{xfSAS:makeQueryIdAttribute(., (
					 concat('typeEE=', concat('source.texte.', EL:normalizeType(., $Refcode.Type))),
					 concat('parent.code=', $Refcode/@Code),
					 concat('parent.num=', @Num)
					 ))}"
      />
    </relation>
  </xsl:template>

  <xsl:template match="contentNode/content//Etudered:Refcode/Etudered:Xacode" mode="makeRelations">
    <xsl:copy>
      <xsl:attribute name="xf:idRef" select="generate-id(.)"/>
      <xsl:attribute name="xf:refType" select="'xf:relation'"/>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Etudered:Xacode[not(parent::Etudered:Refcode)]" mode="makeRelations">
    <xsl:message>[WARNING] Xacode sans parent Refcode sur <xsl:value-of select="els:get-xpath(.)"/></xsl:message>
  </xsl:template>

  <!--=====================================-->
  <!--Xacodes-->
  <!--=====================================-->
  <!--EXEMPLE : 
		1) dans le flux : 
				aux articles <Xacodes Num="722-5" Type="L" Kelcode="XACRN"/> et suivants
		2) dans Refcode
				<Refcode Type="ART" Code="PXCRN" Typemaj="0"><Xacode Num="752-37" Type="R"/> et <Xacodes Num="752-38" Type="R"/></Refcode>
	-->

  <xsl:template match="Etudered:Refcode/Etudered:Xacodes" mode="makeRelationElement">
    <relation xf:id="{generate-id(.)}" type="generated">
      <xsl:variable name="Refcode" select="parent::Etudered:Refcode" as="element()"/>
      <xsl:variable name="Refcode.Type" select="($Refcode/@Type,'NaN')[1]" as="xs:string"/>
      <ref xf:base="{$xfSAS:REST.ee.textes.uri}" xf:refType="{$xfSAS:company}:{local-name(.)}"
        xf:idRef="{xfSAS:makeQueryIdAttribute(., (
				concat('typeEE=', concat('source.texte.', EL:normalizeType(., $Refcode.Type))),
				concat('parent.code=', (@Kelcode, $Refcode/@Code)[1]),
				concat('parent.num=', @Num)
				))}"
      />
    </relation>
  </xsl:template>

  <xsl:template match="contentNode/content//*[not(self::Etudered:Refcode)]/Etudered:Xacodes" mode="makeRelationElement">
    <relation xf:id="{generate-id(.)}" type="generated">
      <ref xf:base="{$xfSAS:REST.ee.textes.uri}" xf:refType="{$xfSAS:company}:{local-name(.)}"
        xf:idRef="{xfSAS:makeQueryIdAttribute(., (
				concat('typeEE=', concat('source.texte.', EL:normalizeType(., @Type))),
				concat('parent.code=', (@Kelcode, @Code)[1]),
				concat('parent.num=', @Num)
				))}"
      />
    </relation>
  </xsl:template>

  <xsl:template match="contentNode/content//Etudered:Xacodes" mode="makeRelations">
    <xsl:copy>
      <xsl:attribute name="xf:idRef" select="generate-id(.)"/>
      <xsl:attribute name="xf:refType" select="'xf:relation'"/>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <!--=====================================-->
  <!--Reftexte/Fixe-->
  <!--=====================================-->
  <!--Exemple  : 
	- <Reftexte Typemaj="0">
			<Fixe Typeloi="TAUTRE" Idref="LOI83164" Idbr="T12240">L. n<E>o</E> 2001-1128, 30 nov. 2001, art. 13 : JO, 1<E>er</E> déc.</Fixe>
	  </Reftexte>
	- l’assurance Atexa <Reftexte Typemaj="0"><Fixe Idbr="663794">L. n<E>o</E> 2013-1203, 23 déc. 2013</Fixe>, art. 82, III et IV : JO, 24 déc. </Reftexte><Reftexte Typemaj="0"><Fixe Idbr="694213">D. n<E>o</E> 2014-741, 30 juin 2014</Fixe> : JO, 1<E>er</E> juill.</Reftexte>. Blablabla
	-->
  <!--FIXME : Attention l'Idref pointe vers un texte Dalloz => il faut donc aller dans ce texte et y récupérer ses métas identificatoires-->

  <!--=====================================-->
  <!--Renvoiexterne/Nomet-->
  <!--=====================================-->

  <xsl:template match="Etudered:Renvoiexterne/Etudered:Nomet" mode="makeRelationElement">
    <relation xf:id="{generate-id(.)}" type="generated">
      <ref xf:base="{$xfSAS:REST.ee.el.uri}" xf:refType="{$xfSAS:company}:{local-name(.)}" xf:idRef="{xfSAS:makeHistoricIdAttribute(., @Idref)}"/>
    </relation>
  </xsl:template>

  <xsl:template match="Etudered:Renvoiexterne/Etudered:Nomet" mode="makeRelations">
    <xsl:copy>
      <xsl:attribute name="xf:idRef" select="generate-id(.)"/>
      <xsl:attribute name="xf:refType" select="'xf:relation'"/>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="Etudered:Nomet[not(parent::Etudered:Renvoiexterne)]" mode="makeRelations">
    <xsl:message>[WARNING] Nomet sans parent Renvoiexterne</xsl:message>
  </xsl:template>

  <!--=====================================-->
  <!--COMMON-->
  <!--=====================================-->

  <xsl:template match="node() | @*" mode="makeRelations">
    <xsl:param name="namespace.uri" required="yes" as="xs:string" tunnel="yes"/>
    <xsl:param name="xf:idRef" as="xs:string?"/>
    <xsl:param name="xf:refType" as="xs:string?"/>
    <xsl:copy>
      <xsl:if test="$xf:refType">
        <xsl:attribute name="xf:refType" select="$xf:refType"/>
      </xsl:if>
      <xsl:if test="$xf:idRef">
        <xsl:attribute name="xf:idRef" select="$xf:idRef"/>
      </xsl:if>
      <xsl:apply-templates select="node() | @*" mode="#current">
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>

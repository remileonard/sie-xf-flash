<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:math="http://www.w3.org/2005/xpath-functions/math"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:EL="http://www.lefebvre-sarrut.eu/ns/el" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" exclude-result-prefixes="xs math xd" version="2.0">
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p><xd:b>Created on:</xd:b> Aug 22, 2016</xd:p>
      <xd:p><xd:b>Author:</xd:b> Marc</xd:p>
      <xd:p/>
    </xd:desc>
  </xd:doc>

  <xsl:template match="xf:editorialEntity/xf:body" mode="makeRelations">
    
    <xsl:next-match/>
    <!--
    <xf:relations>
      <xsl:apply-templates select="." mode="makeRelationElement"/>
    </xf:relations>
    -->
  </xsl:template>

  <xsl:template match="*:Refcode[not(*:Xacodes | *:Xacode)] | *:Xacodes | *:Xacode | *:Refjrp | *:Reftexte/Fixe | *:Renvoiexterne | *:formulaire | *:Renvoi[@Produit='FormDP']/*:Formule"
    mode="makeRelations" priority="20">
    <xsl:next-match>
      <xsl:with-param name="xf:idRef" select="generate-id()" as="xs:string"/>
      <xsl:with-param name="xf:refType" select="'TODO'" as="xs:string"/>
    </xsl:next-match>
  </xsl:template>



  <xsl:template match="*:Refcode" mode="makeRelationElement" priority="20">
    <xsl:apply-templates select="*:Xacodes | *:Xacode" mode="#current">
      <xsl:with-param name="Code" select="./@Code"/>
      <xsl:with-param name="Type" select="./@Type"/>
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match="*:Xacodes | *:Xacode" mode="makeRelationElement" priority="20">
    <xsl:param name="Code" select="'PXCT2'" as="xs:string"/>
    <xsl:param name="Type" select="'ART'" as="xs:string"/>
    <xsl:variable name="id" select="generate-id(.)"/>
    <xf:relation xmlns:ns1="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{$id}" type="generated">
      <xf:metadata>
        <xsl:sequence select="EL:createMetadataFromString('er.el.etude.dpform2.refcode','type',$Type)"/>
        <xsl:sequence select="EL:createMetadataFromString('er.el.etude.dpform2.refcode','code',$Code)"/>
        <xsl:apply-templates select="../self::*:Refcode/@*[not(local-name()=('Type','Code'))]" mode="createMetadata">
          <xsl:with-param name="prefix" select="'er.el.etude.dpform2.'"/>
        </xsl:apply-templates>
        <xsl:apply-templates select="@*" mode="createMetadata">
          <xsl:with-param name="prefix" select="'er.el.etude.dpform2.refcode.'"/>
        </xsl:apply-templates>
      </xf:metadata>
      <xf:ref xf:targetResId="" xf:targetResType="" xf:refType="el:Xacode"
        xf:idRef="query:{string-join((concat('typeEE=', concat('source.texte.', EL:normalizeType(., $Type))),
        concat('parent.code=', $Code),
        concat('parent.num=',@Type,'-', @Num)),':')}"/>
    </xf:relation>
  </xsl:template>


  <xsl:template match="*:Refjrp" mode="makeRelationElement" priority="20">
    <xf:relation xmlns:ns1="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{generate-id(.)}" type="generated">
      <xf:metadata>
        <xsl:apply-templates select="@*" mode="createMetadata">
          <xsl:with-param name="prefix" select="'er.el.etude.dpform2.'"/>
        </xsl:apply-templates>
        <xsl:apply-templates select="*:Fixe/@*" mode="createMetadata">
          <xsl:with-param name="prefix" select="'er.el.etude.dpform2.refjrp.'"/>
        </xsl:apply-templates>
      </xf:metadata>
      <xf:ref xf:targetResId="" xf:targetResType="" xf:refType=""
        xf:idRef="query:{string-join((concat('typeEE=', concat('source.jrp.', EL:normalizeType(., (*:Fixe/text())[1]/replace(.,'^(\w+)[^\w]*.*','$1')))),
        concat('parent.code=', ''),
        concat('parent.num=',normalize-space(string-join(*:Fixe/*:E/following-sibling::text(),' ')))),':')}"
      />
    </xf:relation>
  </xsl:template>


  <xsl:template match="*:Renvoiexterne[*:Nomdp]" mode="makeRelationElement" priority="20">
    <xf:relation xmlns:ns1="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{generate-id(.)}" type="generated">
      <xf:metadata>
        <xsl:apply-templates select="@*" mode="createMetadata">
          <xsl:with-param name="prefix" select="'er.el.etude.dpform2.'"/>
        </xsl:apply-templates>
        <xsl:apply-templates select="*:Nomdp/@*" mode="createMetadata">
          <xsl:with-param name="prefix" select="'er.el.etude.dpform2.renvoiexterne.'"/>
        </xsl:apply-templates>
      </xf:metadata>
      <xf:ref xf:targetResId="" xf:targetResType="" xf:refType=""
        xf:idRef="query:{string-join((concat('typeEE=', 'source.texte.', EL:normalizeType(., *:Nomdp/@DP)),
        concat('parent.code=', ''),
        concat('parent.num=',*:Tietudefiche/@Idref)),':')}"/>
    </xf:relation>
  </xsl:template>

  <xsl:template match="*:Renvoiexterne[*:Nomet]" mode="makeRelationElement" priority="20">
    <xf:relation xmlns:ns1="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{generate-id(.)}" type="generated">
      <xf:metadata>
        <xsl:apply-templates select="@*" mode="createMetadata">
          <xsl:with-param name="prefix" select="'er.el.etude.dpform2.'"/>
        </xsl:apply-templates>
        <xsl:apply-templates select="*:Nomet/@*" mode="createMetadata">
          <xsl:with-param name="prefix" select="'er.el.etude.dpform2.renvoiexterne.'"/>
        </xsl:apply-templates>
      </xf:metadata>
      <xf:ref xf:targetResId="xfSAS:makeHistoricIdAttribute(., *:Nomet/@Idref)" xf:targetResType="" xf:refType=""
        xf:idRef="xfSAS:makeHistoricIdAttribute(., *:Nomet/@Idref)"/>
     </xf:relation>
  </xsl:template>


  <xsl:template match="Reftexte/Fixe" mode="makeRelationElement" priority="20">
    <xf:relation xmlns:ns1="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{generate-id(.)}" type="generated">
      <xf:metadata>
        <xsl:apply-templates select="../@*" mode="createMetadata">
          <xsl:with-param name="prefix" select="'er.el.etude.dpform2.'"/>
        </xsl:apply-templates>
        <xsl:apply-templates select="@*" mode="createMetadata">
          <xsl:with-param name="prefix" select="'er.el.etude.dpform2.reftexte.'"/>
        </xsl:apply-templates>
      </xf:metadata>
      <xf:ref xf:targetResId="" xf:targetResType="" xf:refType=""
        xf:idRef="query:{string-join((
        concat('typeEE=', 'source.texte.', EL:normalizeType(., substring-before((text())[1],'.'))),
        concat('parent.code=', ''),
        concat('parent.num=',@Ibdr)
        ),':')}"/>
    </xf:relation>
  </xsl:template>


  <xsl:template match="formulaire | Renvoi[@Produit='FormDP']/Formule" mode="makeRelationElement" priority="20">
    <xsl:variable name="idTarget" select="replace(id-modele | @Idref,'([A-Z]\d)[A-Z](\d*)','$1$2')"/>
    <xsl:comment><xsl:value-of select="$idTarget"/></xsl:comment>
    <xf:relation xmlns:ns1="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{generate-id(.)}" type="generated">
      <xf:metadata>
        <xsl:apply-templates select="*" mode="createMetadata">
          <xsl:with-param name="prefix" select="'ee.el.procedure.formulaire.'"/>
        </xsl:apply-templates>
      </xf:metadata>
      <!--   
   <xf:ref xf:idRef="{xfSAS:makeHistoricIdAttribute(., $idTarget)}" xf:refType="{$xfSAS:company}_FORMULAIRE"/>
-->  <xf:ref xf:targetResId="" xf:targetResType="" xf:refType=""
        xf:idRef="query:{string-join((
        concat('typeEE=',$xfSAS:company,'_FORMULAIRE'),
        concat('id=','ee.el.etude.dpform2','.',$idTarget)
        ),':')}"/>
    </xf:relation>
  </xsl:template>



  <xsl:template name="titreRelation">
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="etudeRed2html"/>
    </div>
  </xsl:template>


  <xsl:template match="@* | text() | processing-instruction() | comment()" mode="makeRelations" priority="2">
    <xsl:copy> </xsl:copy>
  </xsl:template>
  <xsl:template match="node()" mode="makeRelations">
    <xsl:param name="xf:idRef" as="xs:string?"/>
    <xsl:param name="xf:refType" as="xs:string?"/>
    <xsl:copy>
      <xsl:if test="$xf:refType">
        <xsl:attribute name="xf:refType" select="$xf:refType"/>
      </xsl:if>
      <xsl:if test="$xf:idRef">
        <xsl:attribute name="xf:idRef" select="$xf:idRef"/>
      </xsl:if>
      <xsl:apply-templates select="node() | @*" mode="#current"> </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>


  <xsl:template match="node() | @* | text()" mode="makeRelationElement">
    <xsl:apply-templates select="node() | @*" mode="#current"/>
  </xsl:template>


</xsl:stylesheet>

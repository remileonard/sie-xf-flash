<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:math="http://www.w3.org/2005/xpath-functions/math"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:EL="http://www.lefebvre-sarrut.eu/ns/el" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" 
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" exclude-result-prefixes="#all" version="2.0">
  
  <xsl:import href="../xf-sas-common.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p><xd:b>Created on:</xd:b> Aug 22, 2016</xd:p>
      <xd:p><xd:b>Author:</xd:b> Marc</xd:p>
      <xd:p/>
    </xd:desc>
  </xd:doc>

  <xsl:variable name="etudeMetaMapping" as="element()">
    <mapping>
      <meta name="Id">EL_META_IdEL</meta>
      <meta name="Derid">EL_META_etuderedDerid</meta>
      <meta name="Ref">EL_META_etuderedRef</meta>
      <meta name="Dertap">EL_META_etuderedDertap</meta>
      <meta name="Maj">EL_META_etuderedMaj</meta>
      <meta name="Datemaj">EL_META_etuderedDateMaj</meta>
      <meta name="Typemaj">EL_META_typeMaj</meta>
      <meta name="Grp">EL_META_etuderedGrp</meta>
      <meta name="File">EL_META_etuderedFile</meta>
      <meta name="Rep">EL_META_etuderedRep</meta>
      <meta name="title">EL_META_etuderedTitle</meta>
      <meta name="Indtap">EL_META_pnumIndTap</meta>
    </mapping>
  </xsl:variable>

  <xsl:function name="EL:normalizeType" as="xs:string?">
    <xsl:param name="e" as="element()"/>
    <xsl:param name="type" as="xs:string?"/>
    <xsl:choose>
      <xsl:when test="$type = 'ART'">
        <xsl:text>article</xsl:text>
      </xsl:when>
      <xsl:when test="$type = ('L','D','R','A')">
        <xsl:text>article</xsl:text>
      </xsl:when>
      <xsl:when test="$type = 'Circ'">
        <xsl:text>Circ</xsl:text>
      </xsl:when>
      <xsl:when test="$type = 'Arr'">
        <xsl:text>Arr</xsl:text>
      </xsl:when>
      <xsl:when test="$type = 'Cass. soc.' or starts-with($type,'Cass')">
        <xsl:text>Cass</xsl:text>
      </xsl:when>
      <xsl:when test="$type = 'Rép'">
        <xsl:text>Rép</xsl:text>
      </xsl:when>
      <xsl:when test="starts-with($type,'CA')">
        <xsl:text>CA</xsl:text>
      </xsl:when>
      <xsl:when test="starts-with($type,'ANI')">
        <xsl:text>ANI</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$type"/>
        <xsl:message>[WARNING] type='<xsl:value-of select="$type"/>' non reconnu sur <xsl:value-of select="els:get-xpath($e)"/></xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>


  <xsl:function name="EL:createMetadataFromString">
    <xsl:param name="prefix" as="xs:string"/>
    <xsl:param name="name" as="xs:string"/>
    <xsl:param name="value" as="xs:string"/>
    <xsl:variable name="codeEL">
      <xsl:value-of select="$etudeMetaMapping/*[@name=$name]"/>
    </xsl:variable>
    <xsl:variable name="code" select="if ($prefix != '') then concat($prefix,'.',$name) else $name"/>
    <xsl:variable name="codeFinal" select="if ($codeEL != '') then $codeEL else $code"/>

    <xf:meta code="{$codeFinal}">
      <xf:value>
        <xsl:value-of select="$value"/>
      </xf:value>
    </xf:meta>
  </xsl:function>
  
  <xsl:template name="EL:createMetadataFromElement">
    <xsl:param name="prefix" as="xs:string"/>
    <xsl:param name="name" as="xs:string"/>
    <xsl:param name="element" as="element()*"/>
    <xsl:variable name="codeEL">
      <xsl:value-of select="$etudeMetaMapping/*[@name=$name]"/>
    </xsl:variable>
    <xsl:variable name="code" select="if ($prefix != '') then concat($prefix,'.',$name) else $name"/>
    <xsl:variable name="codeFinal" select="if ($codeEL != '') then $codeEL else $code"/>
    <xf:meta code="{$codeFinal}">
      <xf:value>
        <xsl:apply-templates select="$element" mode="xfRes:el2html"/>
      </xf:value>
    </xf:meta>
  </xsl:template>


</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl" xmlns:formulaire_dp="http://www.lefebvre-sarrut.eu/ns/el/formulaire_dp"
  xmlns:EL="http://www.lefebvre-sarrut.eu/ns/el" xmlns:itg="http://www.infotrustgroup.com/" xmlns:ns1="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xpath-default-namespace="" exclude-result-prefixes="#all" version="2.0">

  <xsl:import href="../xf-sas-common.xsl"/>
  
  <xsl:include href="convertNameSpace.xsl"/>
  <xsl:include href="normalisationEL.xsl"/>
  
  <xsl:include href="fichesQR-EL2editorialEntity.EE.xsl"/>
  <xsl:include href="ELRelation.xsl"/>

  <xsl:param name="xfSAS:company" select="'el'" as="xs:string"/>

  <xsl:variable name="prefix.contentNode.namespace.ficheQR.uri" select="'http://www.lefebvre-sarrut.eu/ns/el/fichesQR_'" as="xs:string"/>


  <!--==================================================-->
  <!-- MAIN -->
  <!--==================================================-->

  <!--<xsl:template match="/">
		<xsl:call-template name="xfSAS:makeXmlModelPi"/>
		<xsl:variable name="step1-xml2ee" as="document-node()">
			<xsl:document>
				<xsl:apply-templates/>
			</xsl:document>
		</xsl:variable>
		<xsl:result-document href="log/step1-xml2ee.log.xml">
			<xsl:sequence select="$step1-xml2ee"/>
		</xsl:result-document>
		<xsl:apply-templates select="$step1-xml2ee" mode="makeRelations"/>
	</xsl:template>-->
  
  <xsl:template match="/">
    <xsl:apply-templates select="node()"></xsl:apply-templates>
  </xsl:template>

  <xsl:template match="/QR">
    <xsl:call-template name="xfSAS:makeXmlModelPi"/>
    <xsl:variable name="step1-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="." mode="xfSAS:fichesQR-EL2editorialEntity.EE"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step1-xml2ee.uri" select="'log/step1-xml2ee.log.xml'" as="xs:string"/>
    <!--<xsl:result-document href="{$step1-xml2ee.uri}">
      <xsl:sequence select="$step1-xml2ee"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$step1-xml2ee.uri"/></xsl:message>
    </xsl:result-document>-->
    <!--Deduplication des EE dans referenceNode-->
    <xsl:variable name="step2-deduplicateRefNodeEntity" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step1-xml2ee" mode="xfSAS:deduplicateRefNodeEntity"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step2-deduplicateRefNodeEntity.uri" select="'log/step2-deduplicateRefNodeEntity.log.xml'" as="xs:string"/>
    <!--<xsl:result-document href="{$step2-deduplicateRefNodeEntity.uri}">
      <xsl:sequence select="$step2-deduplicateRefNodeEntity"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$step2-deduplicateRefNodeEntity.uri"/></xsl:message>
    </xsl:result-document>-->
    <!--Normalisation des méta-->
    <xsl:variable name="step3-fixMeta" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step2-deduplicateRefNodeEntity" mode="xfSAS:fixMeta"/>        
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step3-fixMeta.uri" select="'log/step3-fixMeta.log.xml'" as="xs:string"/>
    <!--<xsl:result-document href="{$step3-fixMeta.uri}">
      <xsl:sequence select="$step3-fixMeta"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$step3-fixMeta.uri"/></xsl:message>
    </xsl:result-document>-->
    <!-- Gestion des relations -->
    <xsl:variable name="step4-makeRelations" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step3-fixMeta" mode="makeRelations"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step4-makeRelations.uri" select="'log/step4-makeRelations.log.xml'" as="xs:string"/>
    <!--<xsl:result-document href="{$step4-makeRelations.uri}">
      <xsl:sequence select="$step4-makeRelations"/>
      <xsl:message>[INFO] writing <xsl:value-of select="$step4-makeRelations.uri"/></xsl:message>
    </xsl:result-document>-->
    
    <xsl:sequence select="$step4-makeRelations"/>
    
  </xsl:template>
</xsl:stylesheet>

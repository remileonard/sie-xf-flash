<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
	xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl" xmlns:etudeRed="http://www.lefebvre-sarrut.eu/ns/el/etudes"
	xmlns:itg="http://www.infotrustgroup.com/" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:EL="http://www.lefebvre-sarrut.eu/ns/el"
	xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xpath-default-namespace="" exclude-result-prefixes="#all" version="2.0">

	<xsl:import href="../xf-sas-common.xsl"/>

	<xsl:include href="normalisationEL.xsl"/>
	<xsl:include href="convertNameSpace.xsl"/>
	<xsl:include href="ELRelation.xsl"/>


	<xsl:param name="xfSAS:company" select="'el'" as="xs:string"/>

	<xsl:variable name="contentNode.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/el/procedures'" as="xs:string"/>

	<xsl:output method="xml" indent="yes"/>

	<xsl:key name="getElementById" match="*[@id]" use="@id"/>

	<!--==================================================-->
	<!-- MAIN -->
	<!--==================================================-->

	<xsl:template match="/">
		<xsl:call-template name="xfSAS:makeXmlModelPi"/>
		<xsl:apply-templates/>
	</xsl:template>

	<xsl:template match="/procedures">
		<editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
			xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" code="{$xfSAS:company}_PROCEDURES" xf:id="{xfSAS:makeHistoricIdAttribute(., 'EL-procedures-')}">
			<xsl:namespace name="xf">http://www.lefebvre-sarrut.eu/ns/xmlfirst</xsl:namespace>
			<metadata>
				<xsl:sequence select="EL:createMetadataFromString('','systemTitle','TODO')"/>
				<xsl:sequence select="EL:createMetadataFromString('','systemAbstract','TODO')"/>
				<xsl:apply-templates select="@*" mode="createMetadata">
					<xsl:with-param name="prefix" select="'ee.el.procedures.'"/>
				</xsl:apply-templates>
			</metadata>
			<body>
				<xsl:apply-templates select="procedure"/>
			</body>
			<relations/>
		</editorialEntity>
	</xsl:template>

	<xsl:template match="procedure">
		<structuralNode>
			<title>
				<div xmlns="http://www.w3.org/1999/xhtml">
					<xsl:value-of select="libelle-long"/>
				</div>
			</title>
			<referenceNode code="TODO">
				<ref xmlns:ns0="http://www.lefebvre-sarrut.eu/ns/xmlfirst" ns0:idRef="{id}" xmlns:ns1="http://www.lefebvre-sarrut.eu/ns/xmlfirst" ns1:refType="{$xfSAS:company}_PROCEDURE">
					<editorialEntity code="{$xfSAS:company}_PROCEDURE">
						<metadata>
							<xsl:sequence select="EL:createMetadataFromString('','systemTitle','TODO')"/>
							<xsl:sequence select="EL:createMetadataFromString('','systemAbstract','TODO')"/>
							<xsl:apply-templates select="*" mode="createMetadata">
								<xsl:with-param name="prefix" select="'ee.el.procedure.'"/>
							</xsl:apply-templates>
						</metadata>
						<body>
							<contentNode>
								<content>
									<xsl:apply-templates select="formulaires" mode="convertNamespace">
										<xsl:with-param name="namespace.uri" select="$contentNode.namespace.uri" tunnel="yes"/>
									</xsl:apply-templates>
								</content>
							</contentNode>
						</body>
						<relations>
							<xsl:apply-templates select="formulaires/formulaire" mode="makeRelation"/>
						</relations>
					</editorialEntity>
				</ref>
			</referenceNode>
		</structuralNode>
	</xsl:template>


	<!--==================================================-->



	<!--==================================================-->
	<!--COMMON-->
	<!--==================================================-->

	<!-- ID -->
	<xsl:template match="@id" mode="#default">
		<xsl:copy-of select="."/>
		<xsl:attribute name="xf:id" select="xfSAS:makeHistoricIdAttribute(parent::*, .)"/>
	</xsl:template>

	<xsl:template match="node() | @*">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*"/>
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>

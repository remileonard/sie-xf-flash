<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
    xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl"
    xmlns:gb="http://www.lefebvre-sarrut.eu/ns/el/gb"
    xmlns:itg="http://www.infotrustgroup.com/"
    xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
    xmlns:xfSas="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
    xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
   
    xmlns:h="http://www.w3.org/1999/xhtml"
    xmlns:functx="http://www.functx.com"
   
    xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
    xpath-default-namespace=""
    exclude-result-prefixes="#all"
    version="2.0">
    
    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Dec 14, 2016</xd:p>
            <xd:p><xd:b>Author:</xd:b> ext-rleonard</xd:p>
            <xd:p>Conversion des fichiers générés pour créer l'arbre produit d'un DP des EL</xd:p>
        </xd:desc>
    </xd:doc>
    
    <xsl:param name="xfSas:company" select="'el'" as="xs:string"/>
    <xsl:variable name="contentNode.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/el/gb'" as="xs:string"/>
    <xsl:function name="functx:escape-for-regex" as="xs:string"
        xmlns:functx="http://www.functx.com">
        <xsl:param name="arg" as="xs:string?"/>
        
        <xsl:sequence select="replace($arg,'(\.|\[|\]|\\|\||\-|\^|\$|\?|\*|\+|\{|\}|\(|\))','\\$1')"/>
        
    </xsl:function>
    <xsl:function name="functx:substring-before-last" as="xs:string"
        xmlns:functx="http://www.functx.com">
        <xsl:param name="arg" as="xs:string?"/>
        <xsl:param name="delim" as="xs:string"/>
        
        <xsl:sequence select="
            if (matches($arg, functx:escape-for-regex($delim)))
            then replace($arg,concat('^(.*)', functx:escape-for-regex($delim),'.*'),'$1')
            else ''
            "/>
        
    </xsl:function>
    <xsl:variable name="listTypeFiche" as="element()*">
        <typeFiche name="FP" code="F"/>
        <typeFiche name="MT" code="M"/>
        <typeFiche name="QR" code="Q"/>
        <typeFiche name="FR" code="FPRO"/>
        <typeFiche name="FPRO" code="FPRO"/>
        <typeFiche name="FREG" code="FPRO"/>
    </xsl:variable>
    
    <xsl:output method="xml" indent="yes"/>
    <xsl:key name="getElementById" match="*[@id]" use="@id"/>
    
    
    <xsl:function name="xf:maheHistoricId" as="xs:string">
        <xsl:param name="id" as="xs:string"/>
        <xsl:value-of select="concat('{historicalId:el_', $id, '}')"/>
    </xsl:function>
    <!--======================================================================================-->
    <!--INIT-->
    <!--======================================================================================-->
    
    
    <xsl:variable name="gbid" select="//editorialEntity/numero" as="xs:string"/>
    <xsl:variable name="user" select="'88'" as="xs:string"/>
    <xsl:variable name="ownerGroup" select="'EL_GRP_dp'" as="xs:string"/>
    
    
    <xsl:template match="/">
        
        <xsl:apply-templates select="." mode="xfSas:gbArbreEl2editorialEntity"/>
    </xsl:template>
    
    <!--======================================================================================-->
    <!--MAIN-->
    <!--======================================================================================-->
    
    <xsl:template match="/" mode="xfSas:gbArbreEl2editorialEntity">
        <editorialEntity 
            code="EL_TEE_dictionnairePermanent"
            creationDateTime="{current-dateTime()}"
            lastModificationDateTime="{current-dateTime()}"
            lastModificationUser="{$user}"
            creationUser="{$user}"
            ownerGroup="{$ownerGroup}"
            xf:id="{xf:maheHistoricId($gbid)}">
            <metadata>
                <xsl:apply-templates select="type,numero" mode="metadata"/>
                <meta code="systemTitle">
                    <value>
                        <div xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="$gbid"/></div>
                    </value>
                </meta>
                <meta code="systemAbstract">
                    <value>
                        <div xmlns="http://www.w3.org/1999/xhtml"/>
                    </value>
                </meta>
            </metadata>
            <body>
                <xsl:apply-templates mode="#current"/>
            </body>
            <relation/>
        </editorialEntity>
    </xsl:template>
    
    <xsl:template match="structuralNode" mode="xfSas:gbArbreEl2editorialEntity">
        <structuralNode>
            <metadata>
                <xsl:apply-templates select="type,numero,titre,title" mode="metadata"/>
            </metadata>
            <xsl:apply-templates mode="#current"/>
        </structuralNode>
    </xsl:template>
    
    <xsl:template match="titre|title" mode="xfSas:gbArbreEl2editorialEntity">
        <title>
            <div xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="normalize-space(.)"/></div>
        </title>
    </xsl:template>
    <xsl:template match="titre|title" mode="metadata">
        <meta code="systemTitle">
            <value><div xmlns="http://www.w3.org/1999/xhtml"><xsl:value-of select="normalize-space(.)"/></div></value>
        </meta>
    </xsl:template>
    <xsl:template match="type" mode="metadata">
        <meta code="el_META_typeNoeud">
            <value><xsl:value-of select="normalize-space(.)"/></value>
        </meta>
    </xsl:template>
    
    <xsl:template match="numero" mode="metadata">
        <meta code="el_META_numero">
            <value><xsl:value-of select="normalize-space(.)"/></value>
        </meta>
    </xsl:template>
    <xsl:template match="referenceNode" mode="xfSas:gbArbreEl2editorialEntity">
        
        <!-- remplacer les text() par makeHistoricID avec l'identifiant de la fiche -->
        <xsl:variable name="file.name" select="concat(text(),'.out.xml')" as="xs:string"/>
        <xsl:variable name="basedir" select="concat(functx:substring-before-last(base-uri(.),'/'),'/')"/>
        <xsl:variable name="file.uri" select="resolve-uri($file.name,$basedir)" as="xs:anyURI" />
        <xsl:variable name="infosFileName" select="tokenize(replace($file.name,'([^_]*)_([^_]*)','$1|$2'),'\|')"/>
       
        <xsl:variable name="idFiche" select="$infosFileName[1]"/>
       
        
           
        <referenceNode code="EL_TR_accrochage_DP_GrpEtudes">                
            <ref xf:targetResId="{xf:maheHistoricId($idFiche)}" 
                xf:targetResType="editorialEntity" 
                xf:refType="xf:referenceNode"/>
        </referenceNode>

    </xsl:template>
    <xsl:template match="editorialEntity">
        <xsl:copy>
            <xsl:attribute name="creationDateTime" select="current-dateTime()"/>
            <xsl:attribute name="lastModificationDateTime" select="current-dateTime()"/>
            <xsl:attribute name="lastModificationUser" select="$user"/>
            <xsl:attribute name="creationUser" select="$user"/>
            <xsl:apply-templates select="@*"/>
            <xsl:apply-templates select="node()"/>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="value[count(h:div) gt 1]">
        <xsl:copy>
            <xsl:apply-templates select="@*"/>
            <div xmlns="http://www.w3.org/1999/xhtml">
                <xsl:apply-templates/>
            </div>
        </xsl:copy>
    </xsl:template>
    <xsl:template match="xf:*">
        <xsl:element name="{local-name(.)}">
            <xsl:apply-templates select="@* | node()" />
        </xsl:element>
    </xsl:template>
    
    <xsl:template match="@xf:idRef[starts-with(., '{')]" priority="1">
        <xsl:attribute name="xf:idRef" select="normalize-space(replace(., '&#xA;', ''))"/>
    </xsl:template>
    
    <xsl:template match="relation/@xf:id | gb:*/@xf:idRef">
        <xsl:attribute name="{name(.)}" select="concat(ancestor::editorialEntity[1]/generate-id(.), .)"/>
    </xsl:template>
    
    <xsl:template match="@ownerGroup">
        <xsl:attribute name="ownerGroup" select="$ownerGroup"/>
    </xsl:template>
    
    <xsl:template match="node() | @*">
        <xsl:copy>
            <xsl:apply-templates select="node() | @*"/>
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="comment()|processing-instruction()" priority="1"/>
    
    <xsl:template match="/processing-instruction()" priority="2">
        <xsl:copy/>
    </xsl:template>
    <xsl:template match="text()" mode="xfSas:gbArbreEl2editorialEntity"/>
</xsl:transform>
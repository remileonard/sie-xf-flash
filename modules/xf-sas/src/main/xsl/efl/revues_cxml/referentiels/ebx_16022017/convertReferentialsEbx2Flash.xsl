<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  exclude-result-prefixes="#all"  
  version="2.0">
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Conversion des referentiels EBX vers des référentiels Flash</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!--==================================================-->
  <!-- VARIABLES -->
  <!--==================================================--> 
  <xsl:variable name="uri" select="resolve-uri('./', base-uri(.))" as="xs:anyURI"/>  
  <xsl:variable name="xslFileName" select="xf:substring-after-last-match(static-base-uri(), '/')" as="xs:string"/>
  <xsl:variable name="xmlFolderName" select="xf:substring-before-last-match(base-uri(), '/')" as="xs:string"/>
  
  <!--==================================================-->
  <!-- MAIN -->
  <!--==================================================--> 
  <xsl:template match="/">    
    <xsl:for-each select="collection(concat($uri, '?select=*.xml'))">      
      <xsl:variable name="xmlFileName" select="xf:substring-after-last-match(base-uri(), '/')" as="xs:string"/>
      <xsl:variable name="xmlOutputFileName" select="concat($xmlFolderName, '/ConvertedEbxToFlash/', $xmlFileName, '.flash.xml')" as="xs:string"/>      
      <xsl:variable name="ebxXmlReferential2flashReferential" as="document-node()">
        <xsl:document>
          <xsl:apply-templates select="document(document-uri(.))/*"/>
        </xsl:document>
      </xsl:variable>
      <xsl:result-document href="{$xmlOutputFileName}">
        <xsl:sequence select="$ebxXmlReferential2flashReferential"/>      
      </xsl:result-document>
    </xsl:for-each>    
  </xsl:template>
  
  <xsl:template match="/*">   
    <xsl:variable name="xmlFileName" select="xf:substring-after-last-match(base-uri(), '/')" as="xs:string"/>
    <xsl:processing-instruction name="xml-model">
			<xsl:text>type="application/xml" schematypens="http://purl.oclc.org/dsdl/nvdl/ns/structure/1.0" href="xf-models:/xf-models/referentials/referential.rng"</xsl:text>
		</xsl:processing-instruction>
    <referenceTable xf:id="{concat($xmlFileName, '_MUST_BE_CHANGED')}">
      <metadata>
        <title>Référential - Flash (CXML)</title>
        <description>Converted file from  '<xsl:value-of select="$xmlFileName"/>' of EBX database.</description>
        <from>From EBX to Flash (used XSL file:'<xsl:value-of select="$xslFileName"/>')</from>
        <date><xsl:value-of select="format-dateTime(current-dateTime(),'[Y0001]-[M01]-[D01]')"/></date>
      </metadata>
      <referenceItems>
        <xsl:apply-templates/>
      </referenceItems>
    </referenceTable>
  </xsl:template>
  
  <xsl:template match="/root/child::*">
    <xsl:variable name="id" select="id" as="xs:string*"/>     
    <referenceItem id="{xf:getIdItem(.)}">
      <xsl:if test="libelleComplet">
        <label type="long">
          <xsl:apply-templates select="libelleComplet"/>
        </label>
      </xsl:if>
      <xsl:if test="Libelle | libelleAbrege">
        <label type="short">
          <xsl:apply-templates select="Libelle | libelleAbrege"/>
        </label>
      </xsl:if>
      <!-- <include> management -->
      <xsl:for-each select="/root/child::*[parent = $id]">
        <include idRef="{xf:getIdItem(.)}"/>
      </xsl:for-each>
    </referenceItem>
  </xsl:template>
  
  <!--==================================================-->
  <!-- FUNCTIONS -->
  <!--==================================================--> 
  <xsl:function name="xf:getIdItem" as="xs:string">
    <xsl:param name="e" as="element()"/>
    <xsl:value-of select="($e/Flash_Id, $e/Id, $e/themeId)[1]"/>
  </xsl:function>
  
  <xsl:function name="xf:substring-after-last-match" as="xs:string">
    <xsl:param name="arg" as="xs:string?"/>
    <xsl:param name="regex" as="xs:string"/>    
    <xsl:sequence select="replace($arg, concat('^.*', $regex),'')"/>    
  </xsl:function>
  
  <xsl:function name="xf:substring-before-last-match" as ="xs:string">
    <xsl:param name="arg" as="xs:string?"/>
    <xsl:param name="regex" as="xs:string"/> 
    <xsl:sequence select="replace($arg,concat('^(.*)',$regex,'.*'),'$1')"/>
  </xsl:function>
  
</xsl:stylesheet>
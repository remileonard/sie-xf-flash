<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:xfSASrefMarqueur="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas/referentialMarqueur"   
  xmlns:h="http://www.w3.org/1999/xhtml" 
  exclude-result-prefixes="#all" 
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xpath-default-namespace="http://modeles.efl.fr/modeles/reference" 
  version="2.0">

  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p><xd:b>Created on:</xd:b> Dec 5, 2016</xd:p>
      <xd:p><xd:b>Author:</xd:b> ext-flanic</xd:p>
      <xd:p>Conversion des valeurs de l'attribut @marqueur</xd:p>
      <xd:p>si marqueur structurel => @xf:type</xd:p>
      <xd:p>sinon => element(xf:meta) de type booleen ou vers un référentiel.</xd:p>
    </xd:desc>
  </xd:doc>

  <xd:doc>
    <xd:desc>
      <xd:p>
        <xd:b>Lecture du contenu du référentiel des marqueur; contenu stocké dans la variable $referentielMarqueurXML</xd:b>
      </xd:p>
      <xd:p>les éléments &lt;referentiel_marqueur_global&gt; sont groupés par &lt;Flash_Meta_Type&gt; :</xd:p>
      <xd:ul>
        <xd:li>'Structure' => marqueur converti en attribute(@xf:type),</xd:li>
        <xd:li>'Booleen' => marqueur converti en element(xf:meta) : Meta simple (code et valeur),</xd:li>
        <xd:li>'Referentiel' => marqueur converti en element(xf:meta) : Meta qui pointe vers un référentiel.</xd:li>
      </xd:ul>
      <xd:p>L'élément &lt;Libelle&gt; correspond à la verbalidation de la méta marqueur.</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:variable name="referentielMarqueursXML" select="document('./referentielMarqueurs/referentiel_marqueur_global.xml')" as="document-node()"/>
  
  <!-- Gestion des marqueurs avec les fichiers issus de l'EBX -->
  <xsl:variable name="referentielMarqueurs" select="collection('./referentiels/marqueurs/?select=*.xml')" as="document-node()+"/>
  <xsl:variable name="referentielsMarqueursBoolean" select="collection('./referentiels/marqueursBooleen/?select=*.xml')" as="document-node()+"/>

  <!--==================================================-->
  <!--Convert @marqueurs CXML en Meta Flash -->
  <!--==================================================-->
  <xd:doc>
    <xd:desc>
      <xd:p>Conversion des @marqueurs (non structurels) CXML en méta flash</xd:p>
      <xd:p>Récupération des données depuis le fichier XML de mapping marqueur CXML / données FLASH</xd:p>
    </xd:desc>
    <xd:param>marqueurs: séquence de marqueurs (non structurels)</xd:param>
    <xd:param>parent.name : parent de l'attribut @marqueur</xd:param>
    <xd:param>code.tee : code TEE du document global converti</xd:param>
    <xd:return>Meta Flash (booléennes ou référentielles)</xd:return>
  </xd:doc>
  <xsl:template name="xfSASrefMarqueur:makeMetaMarqueur">
    <xsl:param name="attributeMarqueurs" as="xs:string*"/>
    <xsl:param name="parent.name" as="xs:string"/>
    <xsl:param name="code.tee" as="xs:string"/>
    <xsl:for-each select="$attributeMarqueurs">
      <!-- 
      Jira FLAS-59, point 16
      XML source : modification des marqueurs 'orange', 'bleu', 'marron', 'vert', 'gris' en 'rub_xxx'
      On force le format à 'tiret' pour ces valeurs : "", "nothning", "carré"
      -->
      <xsl:variable name="marqueur" as="xs:string*">
        <xsl:choose>
          <xsl:when test=". = ('orange', 'bleu', 'marron', 'vert', 'gris')">
            <xsl:value-of select="concat('rub_', .)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <xsl:variable name="elementReferentiel" select="$referentielMarqueursXML//xfSASrefMarqueur:referentiel_marqueur_global
        [xfSASrefMarqueur:CXML_Id = $marqueur]
        [xfSASrefMarqueur:CXML_Type = $parent.name]
        (: [xfSASrefMarqueur:Flash_Meta_Type != 'Structure'] :)        
        [count(xfSASrefMarqueur:ValidePour) = 0 or xfSASrefMarqueur:ValidePour=$code.tee]        
        " as="element(xfSASrefMarqueur:referentiel_marqueur_global)*"/>
      <xsl:choose>
        <xsl:when test="$elementReferentiel">
          <xsl:choose>
            <xsl:when test="$elementReferentiel/xfSASrefMarqueur:Flash_Meta_Type= 'Booleen'">
              <!-- Meta de type boolean -->
              <meta code="{$elementReferentiel//xfSASrefMarqueur:Flash_Meta_Nom}" as="xs:boolean">
                <value as="xs:boolean">
                  <xsl:text>true</xsl:text>
                </value>
              </meta>
            </xsl:when>
            <xsl:otherwise>
              <!-- Meta de type référentiel -->
              <meta as="xf:referenceItemRef" code="{$elementReferentiel//xfSASrefMarqueur:Flash_Meta_Nom}">
                <!-- note : on n 'affiche pas la valeur du label : l'ECM s'en chargera -->
                <value as="xf:referenceItemRef">
                  <ref idRef="{$elementReferentiel//xfSASrefMarqueur:Flash_Id}" 
                    xf:targetResId="{$elementReferentiel//xfSASrefMarqueur:Referentiel_Nom}"
                    xf:targetResType="referenceTable"
                    xf:refType="xf:referenceItemRef"/>
                </value>
                <!-- Question : verbalization ? -->
                <verbalization>
                  <h:span>
                    <xsl:value-of select="$elementReferentiel//xfSASrefMarqueur:Libelle"/>
                  </h:span>
                </verbalization>
              </meta>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="els:log">
            <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
            <xsl:with-param name="level" select="'error'"/>
            <xsl:with-param name="code" select="$els:log.level.code"/>
            <xsl:with-param name="alert" select="true()"/>
            <xsl:with-param name="markup" select="true()"/>
            <xsl:with-param name="description">[xfSASrefMarqueur] Le marqueur introuvable : '<xsl:value-of select="$marqueur"/>' (<xsl:value-of select="$code.tee"/>, <xsl:value-of select="$parent.name"/>)</xsl:with-param>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xd:doc>
    <xd:desc><xs:p>Conversion des marqueurs avec les fichiers issus directement du proxy</xs:p></xd:desc>
  </xd:doc>
  <!-- Récupération à partir des fichiers référentiels marqueurs issus de l'EBX -->
  <!-- (12 01 2017) WAIT : ceci est un draft puisque le modèle des métadonnéees (cf relations) n'est pas à ce jour tranché -->  
  <xsl:template name="xfSASrefMarqueur:makeMetaMarqueur_new">
    <xsl:param name="attributeMarqueurs" as="xs:string*"/>
    <xsl:param name="parent.name" as="xs:string"/>
    <xsl:param name="code.tee" as="xs:string"/>
    
    <xsl:for-each select="$attributeMarqueurs">
      <!-- 
      Jira FLAS-59, point 16
      XML source : modification des marqueurs 'orange', 'bleu', 'marron', 'vert', 'gris' en 'rub_xxx'
      On force le format à 'tiret' pour ces valeurs : "", "nothning", "carré"
      -->
      <xsl:variable name="marqueur" as="xs:string*">
        <xsl:choose>
          <xsl:when test=". = ('orange', 'bleu', 'marron', 'vert', 'gris')">
            <xsl:value-of select="concat('rub_', .)"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="."/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:variable>      
      <xsl:variable name="ebxReferentiel" select="$referentielMarqueurs//xfSASrefMarqueur:values
        [xfSASrefMarqueur:metasData[xfSASrefMarqueur:metaValue = $marqueur] and xfSASrefMarqueur:metasData[xfSASrefMarqueur:metaValue = $parent.name]]" as="element()*"/>
      <xsl:variable name="sasReferentielBooleen" select="$referentielsMarqueursBoolean//xfSASrefMarqueur:marqueur[@id = $marqueur and @context=$parent.name]" as="element()*"/>
      <xsl:choose>
        <!-- Plrs références ont été trouvées dans les fichiers référentiels issus de l'EBX -->
        <xsl:when test="(count($ebxReferentiel) > 1) or (count($sasReferentielBooleen) > 1)">
          <xsl:call-template name="els:log">
            <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
            <xsl:with-param name="level" select="'error'"/>
            <xsl:with-param name="code" select="$els:log.level.code"/>
            <xsl:with-param name="alert" select="true()"/>
            <xsl:with-param name="markup" select="true()"/>
            <xsl:with-param name="description">[xfSASrefMarqueur] Dans les fichiers marqueurs : <xsl:value-of select="count($ebxReferentiel)"/> marqueurs ont été trouvés (<xsl:value-of select="$marqueur"/>, <xsl:value-of select="$code.tee"/>, <xsl:value-of select="$parent.name"/>)</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <!-- Marqueur non convertible -->
        <xsl:when test="not(exists($ebxReferentiel)) and not(exists($sasReferentielBooleen))">
          <!-- Aucun référentiel n'a été trouvé dans les fichiers référentiels -->
          <xsl:call-template name="els:log">
            <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
            <xsl:with-param name="level" select="'error'"/>
            <xsl:with-param name="code" select="$els:log.level.code"/>
            <xsl:with-param name="alert" select="true()"/>
            <xsl:with-param name="markup" select="true()"/>
            <xsl:with-param name="description">[xfSASrefMarqueur] Le marqueur introuvable : '<xsl:value-of select="$marqueur"/>' (<xsl:value-of select="$code.tee"/>, <xsl:value-of select="$parent.name"/>)</xsl:with-param>
          </xsl:call-template>
        </xsl:when>
        <!-- Conversion du marqueur en méta booléenne -->
        <xsl:when test="exists($sasReferentielBooleen)">
          <meta code="{$sasReferentielBooleen/@codeMeta}">
            <value>true</value>
          </meta>
        </xsl:when>
        <!-- Conversion du marqueur en méta structure -->
        <xsl:otherwise>
          <!-- xf:id du référentiel -->
          <xsl:variable name="xf:idReferential" select="$ebxReferentiel/ancestor::xfSASrefMarqueur:referential/xfSASrefMarqueur:id" as="xs:string"/>
          <!-- nom du référentiel (pour mapping 'référential' <-> 'nom de la méta') -->
          <xsl:variable name="refName" select="$ebxReferentiel/ancestor::xfSASrefMarqueur:referential/xfSASrefMarqueur:refName" as="xs:string"/>
          <!-- id de la méta -->
          <xsl:variable name="idMeta" select="$ebxReferentiel/xfSASrefMarqueur:id" as="xs:string"/>
          <meta code="{xfSASrefMarqueur:getCodeMeta($refName)}">
            <value>
              <ref xf:targetResId="{$xf:idReferential}" xf:targetResType="referenceTable" idRef="{$idMeta}" xf:refType="xf:referenceItemRef"/>
            </value>
          </meta>          
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p>Fonction de mapping des nom des référentiels / @code de la méta</xd:p>
      <xd:p>FIXME : A mettre dans un fichier de conf XML ???</xd:p>
   </xd:desc>
  </xd:doc>
  <xsl:function name="xfSASrefMarqueur:getCodeMeta" as="xs:string">
    <xsl:param name="refName" as="xs:string*"/>    
    <xsl:choose>
      <xsl:when test="$refName='ref_marqueur_structure'"><xsl:value-of select="'EFL_META_marqueurStructure'"/></xsl:when>
      <xsl:when test="$refName='ref_marqueur_FilInfoOM'"><xsl:value-of select="'EFL_META_marqueurFilOMRubrique'"/></xsl:when>
      <xsl:when test="$refName='ref_marqueur_FilInfoOMThemes'"><xsl:value-of select="'EFL_META_marqueurfilOMThemes'"/></xsl:when>
      <xsl:when test="$refName='ref_marqueur_NewsletterRubriques'"><xsl:value-of select="'EFL_META_marqueurNewsletterRubrique'"/></xsl:when>
      <xsl:when test="$refName='ref_marqueur_NewsletterTypes'"><xsl:value-of select="'EFL_META_marqueurNewsletterType'"/></xsl:when>
      <xsl:when test="$refName='ref_marqueur_NewsletterRubriqueCouleur'"><xsl:value-of select="'EFL_META_marqueurRubriqueCouleur'"/></xsl:when>
      <xsl:when test="$refName='ref_marqueur_RubriqueRef'"><xsl:value-of select="'EFL_META_marqueurRubriqueRef'"/></xsl:when>
      <xsl:when test="$refName='ref_marqueur_RubriqueStruct'"><xsl:value-of select="'EFL_META_marqueurRubriqueStruct'"/></xsl:when>
      <xsl:when test="$refName='ref_marqueur_structure'"><xsl:value-of select="'EFL_META_marqueurStructure'"/></xsl:when>
      <xsl:otherwise><xsl:value-of select="'ERROR_META_NAME_NOT_FOUND'"/></xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <!-- Retourne les marqueurs non stucturels de la <requete> ou <docNiv> -->
  <xsl:function name="xfSASrefMarqueur:getNoStructuralMarqueur" as="xs:string*">
    <xsl:param name="cxmlMarqueurs" as="xs:string*"/>
    <xsl:param name="structuralMarqueurs" as="xs:string*"/>
    <xsl:variable name="noStructuralMarqueurs" as="xs:string*">
      <xsl:for-each select="$cxmlMarqueurs">
        <xsl:if test="not(. = $structuralMarqueurs)">
          <xsl:value-of select="."/>
        </xsl:if>
      </xsl:for-each>
    </xsl:variable>
    <xsl:sequence select="$noStructuralMarqueurs"/>
  </xsl:function>

  <!-- Retourne tous les marqueurs structurel lus dans le référentiel Marqueur -->
  <xsl:function name="xfSASrefMarqueur:getStructuralMarqueurs" as="xs:string*">
    <xsl:sequence select="$referentielMarqueursXML//xfSASrefMarqueur:referentiel_marqueur_global
      [xfSASrefMarqueur:Flash_Meta_Type='Structure']/xfSASrefMarqueur:CXML_Id"/>
  </xsl:function>  

</xsl:stylesheet>

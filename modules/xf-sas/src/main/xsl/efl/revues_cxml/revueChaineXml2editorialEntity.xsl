<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl" 
  xmlns:cx="http://modeles.efl.fr/modeles/reference" 
  xmlns:itg="http://www.infotrustgroup.com/" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns:xfSAScxml="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas/cxml"
  xmlns:xfSASrefMarqueur="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas/referentialMarqueur"
  xmlns:xfadmin="http://www.lefebvre-sarrut.eu/ns/xmlfirst/xsadmin"
  xmlns:h="http://www.w3.org/1999/xhtml"  
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xpath-default-namespace="http://modeles.efl.fr/modeles/reference" 
  exclude-result-prefixes="#all" 
  version="2.0"> 

  <xsl:import href="../../xf-sas-common.xsl"/>
  <xsl:import href="conf/revueChaineXml2editorialEntity.conf.xsl"/>
  
  <xsl:include href="revueChaineXml2editorialEntity.common.xsl"/>
  <xsl:include href="xf-ecmRes:/efl/_common/cxml2html.xsl"/>
  <xsl:include href="revueChaineXml2editorialEntity.getGeneratedMetadata.xsl"/>
  <xsl:include href="revueChaineXml2editorialEntity.makeRelations.xsl"/>
  <xsl:include href="revueChaineXml2editorialEntity.infoChiffres2infoCommentaire.xsl"/>
  <xsl:include href="revueChaineXml2editorialEntity.marqueurs.xsl"/>
  <xsl:include href="prodPubli/num1_numberingCXmlEditorialEntity.xsl"/>
  
  <xsl:output method="xml" indent="no"/>

  <!--==================================================-->
  <!-- KEYS -->
  <!--==================================================-->  
  <xsl:key name="getElementById" match="*[@id]" use="@id"/>
  <xsl:key name="getRequeteByIdRef" match="requete[@idref]" use="@idref"/>

  <!-- PARAMS/VARIABLES -->
  <xsl:param name="produitCxDocId" select="root()/doc/metadonnees/metaCommunes/metaType/produitCx/@idref" as="xs:string"/>
  <xsl:param name="ownerGroupAttribute" select="xfSAS:addAttributeOwnerGroup(xfSAScxml:getEEcode(/*))" as="attribute(ownerGroup)"/>
  <xsl:param name="createPIModel" select="true()" as="xs:boolean"/>
  <xsl:variable name="ONELETTER" select="'a'" as="xs:string"/>

  <!--==================================================-->
  <!-- MAIN -->
  <!--==================================================-->  
  <xsl:template match="/">
    <xsl:call-template name="els:log">
      <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
      <xsl:with-param name="level" select="'info'"/>
      <xsl:with-param name="code" select="$els:log.level.code"/>
      <xsl:with-param name="alert" select="true()"/>
      <xsl:with-param name="markup" select="false()"/>
      <xsl:with-param name="description">[INFO] Convert '<xsl:value-of select="/*/@id"/>'.xml document</xsl:with-param>      
    </xsl:call-template>     
    <!-- Warning : génération du model spécifiquement pour le XSpec -->
    <xsl:if test="$createPIModel">
      <xsl:call-template name="xfSAS:makeXmlModelPi"/>
    </xsl:if>    
    <xsl:variable name="step1-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step1-xml2ee.uri" select="'log/step1-xml2ee.log.xml'" as="xs:string"/>
    
    <xsl:call-template name="els:log">
      <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
      <xsl:with-param name="level" select="'info'"/>
      <xsl:with-param name="code" select="$els:log.level.code"/>
      <xsl:with-param name="alert" select="false()"/>
      <xsl:with-param name="markup" select="false()"/>
      <xsl:with-param name="description">[INFO] writing <xsl:value-of select="$step1-xml2ee.uri"/></xsl:with-param>      
    </xsl:call-template>           
    <!--Deduplication des EE dans referenceNode-->
    <xsl:variable name="step2-deduplicateRefNodeEntity" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step1-xml2ee" mode="xfSAS:deduplicateRefNodeEntity"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step2-deduplicateRefNodeEntity.uri" select="'log/step2-deduplicateRefNodeEntity.log.xml'" as="xs:string"/>
    
    <xsl:call-template name="els:log">
      <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
      <xsl:with-param name="level" select="'info'"/>
      <xsl:with-param name="code" select="$els:log.level.code"/>
      <xsl:with-param name="alert" select="false()"/>
      <xsl:with-param name="markup" select="false()"/>
      <xsl:with-param name="description">[INFO] writing <xsl:value-of select="$step2-deduplicateRefNodeEntity.uri"/></xsl:with-param>      
    </xsl:call-template>    
    <!--Normalisation des méta-->
    <xsl:variable name="step3-fixMeta" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step2-deduplicateRefNodeEntity" mode="xfSAS:fixMeta"/>        
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step3-fixMeta.uri" select="'log/step3-fixMeta.log.xml'" as="xs:string"/>
    
    <xsl:call-template name="els:log">
      <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
      <xsl:with-param name="level" select="'info'"/>
      <xsl:with-param name="code" select="$els:log.level.code"/>
      <xsl:with-param name="alert" select="false()"/>
      <xsl:with-param name="markup" select="false()"/>
      <xsl:with-param name="description">[INFO] writing <xsl:value-of select="$step3-fixMeta.uri"/></xsl:with-param>      
    </xsl:call-template>    
    <!-- Gestion des relations -->
    <xsl:variable name="step4-makeRelations" as="document-node()">
      <xsl:document>
        <xsl:apply-templates select="$step3-fixMeta" mode="makeRelations"/>
      </xsl:document>
    </xsl:variable>
    <xsl:variable name="step4-makeRelations.uri" select="'log/step4-makeRelations.log.xml'" as="xs:string"/>
    
    <xsl:call-template name="els:log">
      <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
      <xsl:with-param name="level" select="'info'"/>
      <xsl:with-param name="code" select="$els:log.level.code"/>
      <xsl:with-param name="alert" select="false()"/>
      <xsl:with-param name="markup" select="false()"/>
      <xsl:with-param name="description">[INFO] writing <xsl:value-of select="$step4-makeRelations.uri"/></xsl:with-param>      
    </xsl:call-template>     
    <!-- Gestion de la numérotation -->
    <xsl:apply-templates select="$step4-makeRelations" mode="numbering"/>
  </xsl:template>

  <xsl:template match="/doc">
    <editorialEntity code="{xfSAScxml:getEEcode(.)}" xf:id="{xfSAS:makeHistoricIdAttribute(., @id)}">
      <!-- Ajout attribut @ownerGroup -->
      <xsl:sequence select="$ownerGroupAttribute"/>
      <xsl:namespace name="xf">http://www.lefebvre-sarrut.eu/ns/xmlfirst</xsl:namespace>
      <xsl:namespace name="h">http://www.w3.org/1999/xhtml</xsl:namespace>
      <xsl:apply-templates/>
    </editorialEntity>
  </xsl:template>

  <!-- Metadonnées d'un doc -->
  <xsl:template match="doc/metadonnees">
    <metadata>
      <!-- JIRA FLAS-11 Q5 : Ajout de la metadata "EFL_META_[produitCx]_TITLE" si <nomDocument> n'est pas égale au calcul du titre -->
      <!-- FIXME : récupérer la verbalisation du produitCx via les référentiels ? quotidienne, newletter ? -->
      <xsl:variable name="produitCx" select="metaCommunes/metaType/produitCx/@idref" as="xs:string*"/>        
      <!-- FIXME : ajouter un test selon le type de TEE ? -->
      <xsl:if test="$produitCx = ('quotidienne', 'newsletter')">
        <!-- ajout d'une nouvelle métadonnée spécifique pour éditer le titre de la revue -->
        <!-- FIXME : nommage de la méta à définir (voir TEE) ? -->
        <meta code="EFL_META_title" as="element(html:div)">
          <value as="element(html:div)">
            <div xmlns="http://www.w3.org/1999/xhtml">
              <xsl:value-of select="/doc/document/nomDocument"/>
            </div>
          </value>
        </meta>
      </xsl:if>
      <xsl:apply-templates mode="metadata.doc"/>
    </metadata>
  </xsl:template>

  <!-- Metadata for UI -->
  <xsl:template match="metadonnees">
    <metadata>
      <xsl:apply-templates mode="metadata.ui"/>
    </metadata>
  </xsl:template>

  <!--==================================================-->
  <!--STRUCTURAL NODE-->
  <!--==================================================-->

  <xsl:template match="document">
    <body>
      <xsl:apply-templates select="arbreDocument"/>
    </body>
  </xsl:template>

  <xsl:template match="arbreDocument">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="docNiv[count(descendant::requete) = 0][count(descendant::*:ui) = 0] | docNiv[count(* except titre) = 0]" priority="1">
    <!--Suppression des docNiv sans requete ni ui -->
    <!--Suppression des docNiv vides (est fait habituellement par prodPubli) -->
  </xsl:template>

  <xsl:template match="docNiv">
    <xsl:variable name="id4Flash" select="xfSAS:makeHistoricIdAttribute(., concat(ancestor::doc/@id, '_', @id))"/>
    <!-- Suppression du marqueurs "rub" (FLAS-75 point 6) -->
    <!-- Tous les marqueurs de l'élément courant -->
    <xsl:variable name="allMarqueurs" select="tokenize(normalize-space(@marqueurs), $els:regAnySpace)" as="xs:string*"/>
    <!-- Définition des marqueurs structurels -->
    <xsl:variable name="structuralMarqueurs" select="xfSASrefMarqueur:getStructuralMarqueurs()" as="xs:string*"/>
    <!-- Nettoyage des marqueurs (gestion du marqueurs 'rub' lorsqu'il y a plusieurs marqueurs structurels -->
    <xsl:variable name="cleanedMarqueurs"  select="xfSASrefMarqueur:cleanMarqueurs(., $allMarqueurs, $structuralMarqueurs)" as="xs:string*"/>
    <structuralNode xf:id="{$id4Flash}">
      <!-- Récupération de la liste des marqueurs structures depuis le fichier de mapping des marqueurs CXML / Flash -->
      <xsl:if test="count($cleanedMarqueurs[. = $structuralMarqueurs]) > 1">
        <xsl:call-template name="els:log">
          <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
          <xsl:with-param name="level" select="'error'"/>
          <xsl:with-param name="code" select="$els:log.level.code"/>
          <xsl:with-param name="alert" select="true()"/>
          <xsl:with-param name="markup" select="true()"/>
          <xsl:with-param name="description">[ERROR] plusieurs marqueurs structurels : <xsl:sequence select="$cleanedMarqueurs[. = $structuralMarqueurs]"/></xsl:with-param>      
        </xsl:call-template>
      </xsl:if>
      <xsl:if test="count($cleanedMarqueurs) > 0">
        <metadata>
          <!-- Marqueur de type structure -->
          <!-- Conversion des marqueurs (structurels et non structurels) en Meta Flash -->
          <xsl:call-template name="xfSASrefMarqueur:makeMetaMarqueur">
            <xsl:with-param name="attributeMarqueurs" select="$cleanedMarqueurs"/>
            <xsl:with-param name="parent.name" select="local-name(.)"/>
            <xsl:with-param name="code.tee" select="xfSAScxml:getEEcode(ancestor::doc)"></xsl:with-param>
          </xsl:call-template>
        </metadata>
       </xsl:if>
      <xsl:apply-templates/>
    </structuralNode>
  </xsl:template>

  <xsl:template match="docNiv/titre">
    <title>
      <div xmlns="http://www.w3.org/1999/xhtml">
        <xsl:apply-templates mode="xfRes:cxml2html"/>
        <xsl:if test="../sousTitre">
          <xsl:value-of select="' - '"/>
          <xsl:apply-templates select="../sousTitre" mode="xfRes:cxml2html"/>
        </xsl:if>
      </div>
    </title>
  </xsl:template>

  <!-- Suppression du sousTitre, son contenu est géré en même temps que "docNiv/titre" -->
  <xsl:template match="docNiv/sousTitre"/>

  <!--==================================================-->
  <!--REQUETE-->
  <!--==================================================-->

  <xsl:template match="requete">
    <!-- Suppression du marqueurs "rub" (FLAS-75 point 6) -->
    <!-- Tous les marqueurs de l'élément courant -->
    <xsl:variable name="allMarqueurs" select="tokenize(normalize-space(@marqueurs), $els:regAnySpace)" as="xs:string*"/>
    <!-- Définition des marqueurs structurels -->
    <xsl:variable name="structuralMarqueurs" select="xfSASrefMarqueur:getStructuralMarqueurs()" as="xs:string*"/>
    <!-- Nettoyage des marqueurs (gestion du marqueurs 'rub' lorsqu'il y a plusieurs marqueurs structurels -->
    <xsl:variable name="cleanedMarqueurs"  select="xfSASrefMarqueur:cleanMarqueurs(., $allMarqueurs, $structuralMarqueurs)" as="xs:string*"/>    
    <!-- Récupértion de l'UI cible de la requête, affichage d'un msg d'erreur si non trouvée -->
    <xsl:variable name="EDP.uri" select="resolve-uri('../', base-uri(.))" as="xs:anyURI"/>
    <xsl:variable name="ui.uri" select="xfSAScxml:getUiUriFromId(@idref, $EDP.uri)" as="xs:anyURI?"/>
    <!-- Code TR : voir Jira FE-87 -->
    <referenceNode code="{xfSAS:getCodeTR(xfSAScxml:getEEcode(ancestor::doc), .)}">
      <metadata>
        <!-- Conversion des marqueurs (structurels et non structurels) en Meta Flash -->
        <xsl:call-template name="xfSASrefMarqueur:makeMetaMarqueur">
          <xsl:with-param name="attributeMarqueurs" select="$cleanedMarqueurs"/>
          <xsl:with-param name="parent.name" select="local-name(.)"/>
          <xsl:with-param name="code.tee" select="xfSAScxml:getEEcode(ancestor::doc)"/>          
        </xsl:call-template>
      </metadata>
      <!-- Gestion du xf:model v1.2.0 (Jira FE-84) -->
      <ref xf:targetResId="{xfSAS:makeHistoricIdAttribute(., @idref)}" xf:targetResType="editorialEntity" xf:refType="xf:referenceNode">
        <xsl:choose>
          <xsl:when test="$ui.uri">
            <xsl:if test="$resolveDoc">
              <xsl:variable name="ui" as="document-node()">
                <xsl:choose>
                  <xsl:when test="document($ui.uri)/ui/infoChiffres">
                    <!-- Conversion préalable des UI Chiffres en UI Commentaire -->
                    <xsl:document>
                      <xsl:apply-templates select="document($ui.uri)/ui" mode="infoChiffres2infoCommentaire"/>
                    </xsl:document>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:sequence select="document($ui.uri)"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:variable>
              <!--attention : on ajoute le "/*" pour ne pas repasser dans le template "/" (ce qui jouerai 2 fois la xslt)-->
              <xsl:apply-templates select="$ui/*"/>
            </xsl:if>
          </xsl:when>
          <xsl:otherwise>
            <xsl:call-template name="els:log">
              <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
              <xsl:with-param name="level" select="'error'"/>
              <xsl:with-param name="code" select="$els:log.level.code"/>
              <xsl:with-param name="alert" select="true()"/>
              <xsl:with-param name="markup" select="true()"/>
              <xsl:with-param name="description">[ERROR] UI id="<xsl:value-of select="@idref"/>" non trouvée dans l'EDP (<xsl:value-of select="$EDP.uri"/>)</xsl:with-param>      
            </xsl:call-template>
          </xsl:otherwise>
        </xsl:choose>
      </ref>
    </referenceNode>
  </xsl:template>
  
  <!--==================================================-->
  <!--UI-->
  <!--==================================================-->

  <!--Traitement d'une ui (sans doc)-->
  <xsl:template match="/ui">
    <editorialEntity code="{xfSAScxml:getEEcode(.)}" xf:id="{xfSAS:makeHistoricIdAttribute(., @id)}">
      <!-- Ajout attribut @ownerGroup -->
      <xsl:sequence select="$ownerGroupAttribute"/>
      <xsl:namespace name="xf">http://www.lefebvre-sarrut.eu/ns/xmlfirst</xsl:namespace>
      <xsl:namespace name="h">http://www.w3.org/1999/xhtml</xsl:namespace>
      <xsl:apply-templates/>
    </editorialEntity>
  </xsl:template>
  
  <!-- Cas d'une UI Chiffres (sans doc) traitée indépendamment par la transformation SAS -->
  <xsl:template match="/ui[infoChiffres]" priority="1">
    <!-- Conversion préalable de l'UI en UI Commentaire -->
    <xsl:variable name="uiCommentaire.converted" as="element(ui)">
      <xsl:apply-templates select="." mode="infoChiffres2infoCommentaire"/>
    </xsl:variable>
    <editorialEntity code="{xfSAScxml:getEEcode($uiCommentaire.converted)}" xf:id="{xfSAS:makeHistoricIdAttribute($uiCommentaire.converted, @id)}">
      <!-- Ajout attribut @ownerGroup -->
      <xsl:sequence select="$ownerGroupAttribute"/>
      <xsl:namespace name="xf">http://www.lefebvre-sarrut.eu/ns/xmlfirst</xsl:namespace>
      <xsl:namespace name="h">http://www.w3.org/1999/xhtml</xsl:namespace>      
      <xsl:apply-templates select="$uiCommentaire.converted/*"/>
    </editorialEntity>
  </xsl:template>

  <xsl:template match="*[cx:isInfo(.)]">
    <body>
      <contentNode>
        <content>
          <xsl:apply-templates select="." mode="convertNamespace"/>
        </content>
      </contentNode>
    </body>
  </xsl:template>

  <!--==================================================-->
  <!--MODE convertNamespace -->
  <!--==================================================-->
  
  <!-- infoChiffres => plus la peine : elle sont converties en infoCommentaire (cf. ci-dessus)-->
  <!-- QuestionsReponses => plus la peine : elles seront converties dans les data d'origine (SERNA) en infoCommentaire avec des éléments <QuestionReponse>-->
  
  <!-- infoCommentaires -->
  <xsl:template match="infoCommentaire" mode="convertNamespace">
    <xsl:variable name="namespace.uri" select="$contentNode.namespace.uri.infoCommentaire" as="xs:string"/>
    <xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
      <xsl:apply-templates select="node() | @*" mode="#current">
        <xsl:with-param name="namespace.uri" select="$namespace.uri" tunnel="yes"/>
      </xsl:apply-templates>
    </xsl:element>
  </xsl:template>
  
  <!-- 
    Jira FLAS-59, point 4
    XML source peut contenir renvoiAutruiTransition/@page=""
  -->
  <xsl:template match="renvoiAutruiTransition" mode="convertNamespace">
    <xsl:param name="namespace.uri" required="yes" as="xs:string" tunnel="yes"/>
    <xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
      <xsl:apply-templates select="@* except @page" mode="#current"/>
      <xsl:if test="not(@page='')">
        <xsl:copy-of select="@page"/>          
      </xsl:if>
      <xsl:apply-templates mode="#current"/>
    </xsl:element>
  </xsl:template>

  <!-- 
    Jira FLAS-59, point 5
    XML source : <complement> peut ne pas contenir @id
    On force la création de l'@id : generate-id()
    Note : on crée dans <complement> un <al> lorsque <complemnet> ne contient que du texte. 
  -->
  <xsl:template match="complement" mode="convertNamespace">
    <xsl:param name="namespace.uri" required="yes" as="xs:string" tunnel="yes"/>
    <xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
      <xsl:apply-templates select="@*"  mode="#current"/>      
      <xsl:if test="not(@id)">
        <xsl:attribute name="id" select="generate-id()"/>
      </xsl:if>
      <xsl:choose>
        <xsl:when test="count(descendant::al) = 0">
          <xsl:element name="al" namespace="{$namespace.uri}">
            <xsl:apply-templates mode="#current"/>
          </xsl:element>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates mode="#current"/>
        </xsl:otherwise>
      </xsl:choose>      
    </xsl:element>
  </xsl:template>
  
  <!-- 
    Jira FLAS-59, point 7
    XML source : le lst/@format non conforme par rapport au model EE
    On force le format à 'tiret' pour ces valeurs : "", "nothning", "carré"
  -->
  <xsl:template match="lst" mode="convertNamespace">
    <xsl:param name="namespace.uri" required="yes" as="xs:string" tunnel="yes"/>
    <xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
      <xsl:apply-templates select="@* except @format"  mode="#current"/>
      <xsl:attribute name="format">
        <xsl:choose>
          <xsl:when test="normalize-space(@format)='' or @format='nothing' or @format='carré' or @format='-' or @format='tir'">
            <xsl:value-of select="'tiret'"/>
          </xsl:when>
          <xsl:when test="@format='pyuce'  or @format='bullet'">
            <xsl:value-of select="'puce'"/>
          </xsl:when>
          <xsl:otherwise><xsl:value-of select="@format"/></xsl:otherwise>
        </xsl:choose>
      </xsl:attribute>
      <xsl:apply-templates mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <xsl:template match="*" mode="convertNamespace" priority="-1">
    <xsl:param name="namespace.uri" required="yes" as="xs:string" tunnel="yes"/>
    <xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="node() | @*" mode="convertNamespace" priority="-2">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>

  <!--==================================================-->
  <!--COMMON-->
  <!--==================================================-->
  
  <!--Surcharge de convertNamespace pour gérer les id (pourrait se faire dans un step spécifique)--> 
  
  <!-- ID -->
  <xsl:template match="@id" mode="#default convertNamespace">
    <xsl:copy-of select="."/>
    <!--<xsl:attribute name="xf:id" select="xfSAS:makeHistoricIdAttribute(parent::*, .)"/>-->
  </xsl:template>

  <!-- Get a unique ID into CXML content : id de type Flash -->
  <xsl:template match="@id[ancestor::*:ui]" mode="#default convertNamespace">
    <xsl:attribute name="id" select="xfSAS:getUniqueId(.)"/>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p>Fonction xfSAS:getUniqueId</xd:p>
      <xd:p>Génération du contenu d'un attribut @id des éléments descendant de infoCommentaire.</xd:p>
      <xd:p>Permet de mettre le caractère $ONELETTER ('a') si besoin au début.</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:function name="xfSAS:getUniqueId" as="xs:string+">
    <xsl:param name="e" as="attribute()"/>
    <xsl:variable name="ancestorId" select="$e/ancestor::*:ui/@id" as="xs:string+"/>
    <xsl:choose>
      <xsl:when test="matches($ancestorId, '^[0-9]')">
        <xsl:value-of select="concat($ONELETTER, $ancestorId, '-', $e)"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="concat($ancestorId, '-', $e)"/>
      </xsl:otherwise>
    </xsl:choose>    
  </xsl:function> 

  <!-- Modification éléments servant à établir un lien interne (renvoi, rHorsTexte, rNote) : référence de type Flash -->
  <xsl:template match="renvoi | rHorsTexte | rNote"  mode="#default convertNamespace">
    <xsl:param name="namespace.uri" required="yes" as="xs:string" tunnel="yes"/>    
    <xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
      <xsl:copy-of select="@*"/>
      <xsl:attribute name="ref" select="xfSAS:getUniqueId(@ref)"/>
    </xsl:element>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p><xd:b>Nettoyage des marqueurs</xd:b></xd:p>
      <xd:p>Suppression du marqueur "rub" lorsqu'il y a plusieurs marqueurs de type structurels dont "rub"</xd:p>
      <xd:p>(autres marqueurs à supprimer ?)</xd:p>
    </xd:desc>
    <xd:param>e:element courant</xd:param>
    <xd:param>allMarqueurs : séquence de tous les marqueurs de l'élément</xd:param>
    <xd:param>structuralMarqueurs : séquance de tous les marqueurs structurels possibles</xd:param>
    <xd:return>séquence nettoyée de marqueurs</xd:return>
  </xd:doc>
  <xsl:function name="xfSASrefMarqueur:cleanMarqueurs" as="xs:string*">
    <xsl:param name="e" as="element()"/>
    <xsl:param name="allMarqueurs" as="xs:string*"/>
    <xsl:param name="structuralMarqueurs" as="xs:string*"/>
    <xsl:choose>
      <xsl:when test="count($allMarqueurs[. = $structuralMarqueurs]) > 1 and 'rub' = $allMarqueurs">
        <xsl:call-template name="els:log">
          <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
          <xsl:with-param name="level" select="'info'"/>
          <xsl:with-param name="code" select="$els:log.level.code"/>
          <xsl:with-param name="alert" select="true()"/>
          <xsl:with-param name="markup" select="false()"/>
          <xsl:with-param name="xpathContext" select="$e"/>
          <xsl:with-param name="description">[INFO] Suppression du marqueur 'rub'.</xsl:with-param>      
        </xsl:call-template>
        <!-- Suppression du marqueur rub (exemple : BPIM) -->
        <xsl:sequence select="xfSASrefMarqueur:removeMarqueursInMarqueursList($allMarqueurs, 'rub')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="$allMarqueurs"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p><xd:b>Suppresion des intances d'un marqueur spécifiques</xd:b></xd:p>      
    </xd:desc>
    <xd:param>marqueusList : séquence initiale de marqueurs</xd:param>
    <xd:param>itemToRemove : string à supprimer</xd:param>
    <xd:return>Sequence de xs:string (instances du marqueur spécifique supprimées)</xd:return>
  </xd:doc>
  <xsl:function name="xfSASrefMarqueur:removeMarqueursInMarqueursList" as="xs:string*">
    <xsl:param name="marqueursList" as="xs:string*"/>
    <xsl:param name="itemToRemove" as="xs:string"/>    
    <xsl:choose>
      <xsl:when test="not(empty(index-of($marqueursList, $itemToRemove)))">
        <xsl:sequence select="xfSASrefMarqueur:removeMarqueursInMarqueursList(remove($marqueursList, index-of($marqueursList, $itemToRemove)[1]), $itemToRemove)"></xsl:sequence>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="$marqueursList"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>Retourne le code TR en fonction du code TEE</xd:p>
      <xd:p>Si le code TEE n'est pas trouvé, retourne le code 'EFL_TR_accrochage_Revues_UIActualite'</xd:p>
    </xd:desc>
    <xd:param>codeTEE</xd:param>
    <xd:param>element courant</xd:param>
  </xd:doc>
  <xsl:function name="xfSAS:getCodeTR" as="xs:string">
    <xsl:param name="codeTEE" as="xs:string"/>
    <xsl:param name="e" as="element()"/>
    <xsl:choose>
      <xsl:when test="$codeTEE = 'EFL_TEE_acdaf'"><xsl:value-of     select="'EFL_TR_accrochage_ACDAF_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_acp'"><xsl:value-of       select="'EFL_TR_accrochage_ACP_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_baf'"><xsl:value-of       select="'EFL_TR_accrochage_BAF_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_bdp'"><xsl:value-of       select="'EFL_TR_accrochage_BDP_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_bpci'"><xsl:value-of      select="'EFL_TR_accrochage_BPCI_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_bpim'"><xsl:value-of      select="'EFL_TR_accrochage_BPIM_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_brda'"><xsl:value-of      select="'EFL_TR_accrochage_BRDA_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_brdaguide'"><xsl:value-of select="'EFL_TR_accrochage_BRDAGuide_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_fr'"><xsl:value-of        select="'EFL_TR_accrochage_FR_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_frc'"><xsl:value-of       select="'EFL_TR_accrochage_FRC_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_frs'"><xsl:value-of       select="'EFL_TR_accrochage_FRS_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_frchs'"><xsl:value-of     select="'EFL_TR_accrochage_FRCHS_UIActualite'"/></xsl:when>
      <xsl:when test="$codeTEE = 'EFL_TEE_rjf'"><xsl:value-of       select="'EFL_TR_accrochage_RJF_UIActualite'"/></xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="els:log">
          <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
          <xsl:with-param name="level" select="'info'"/>
          <xsl:with-param name="code" select="$els:log.level.code"/>
          <xsl:with-param name="alert" select="true()"/>
          <xsl:with-param name="markup" select="false()"/>
          <xsl:with-param name="xpathContext" select="$e"/>
          <xsl:with-param name="description">[WARNING] Donnéez par défaut : Pose du code TR 'EFL_TR_accrochage_Revues_UIActualite' pour le code TEE <xsl:value-of select="$codeTEE"/></xsl:with-param>      
        </xsl:call-template>
        <!-- code TR par défaut pour lier une UI sur une revue-->
        <xsl:value-of select="'EFL_TR_accrochage_Revues_UIActualite'"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xd:doc>
    <xd:desc>Recopie des éléments par défaut</xd:desc>
  </xd:doc>
  <xsl:template match="node() | @*">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
	xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl"
	xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
	xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
	xmlns:infoCommentaire="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
	xmlns:h="http://www.w3.org/1999/xhtml" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	xmlns:sr="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas/SourceReferentials"
	xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	exclude-result-prefixes="#all" version="2.0">

	<xsl:variable name="referential-document"
		select="document('./SourceReferentials/referential.xml')" as="document-node()"/>

  <xsl:template match="editorialEntity/body" mode="makeRelations">
    <xsl:next-match/>
    <relations>
      <xsl:apply-templates select="contentNode/content//*:renvoiAutrui"
        mode="makeRelationElement"/>
      <!-- FIXME : Suppression du lien renvoiAutruiTransition ? -->
      <!--<xsl:apply-templates select="contentNode/content//*:renvoiAutruiTransition" mode="makeRelationElement"/>-->
      <xsl:apply-templates select="contentNode/content//*:refDoc" mode="makeRelationElement"/>
    </relations>
  </xsl:template>

  <!--=====================================-->
  <!--renvoiAutrui-->
  <!--=====================================-->

  <!-- renvoiAutrui : suppression des attributs @ref et @uiId-->
  <!-- @FIXME : Not yet !! en attente de la modification du schéma -->
  <xsl:template match="contentNode/content//*:renvoiAutrui" mode="makeRelations">
    <xsl:copy>
      <xsl:attribute name="xf:idRef" select="generate-id(.)"/>
      <xsl:attribute name="xf:refType" select="'xf:relation'"/>
      <!-- FIXME : Jira FLAS-71 à ajuster @uiId et @ref -->
      <!-- FIXME : les attributs issus de la CXML sont-ils à supprimer ? -->
      <!--<xsl:apply-templates select="@* except (@uiId, @ref)" mode="#current"/>-->
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>

	<xsl:template match="*:renvoiAutrui" mode="makeRelationElement">
	  <!-- pour l'attribut @code : voir Jira FE-87 -->
	  <relation xf:id="{generate-id(.)}" calculated="true" code="EFL_TR_renvoi_UIActualite_UIActualite">
			<!-- Cible : entityEditorial @xf:id (générée à la fin de ce template pour des soucis de els:log) -->
			<xsl:variable name="xf:target-id" select="@uiId" as="xs:string"/>
		  <!-- Cible : élément dans l'ui-->
		  <xsl:variable name="target-id" as="xs:string*">
        <xsl:if test="@ref != '' and @uiId != '' and @ref != @uiId">
          <xsl:value-of select="@ref"/>
        </xsl:if>		    
		  </xsl:variable>
			<ref xf:targetResType="editorialEntity" xf:refType="efl:{local-name(.)}"
				xf:targetResId="{xfSAS:makeHistoricIdAttribute(., $xf:target-id)}">
			  <xsl:if test="normalize-space($target-id) != ''">
			    <xsl:attribute name="idRef" select="$target-id"/>
			  </xsl:if>
			</ref>
		</relation>
	</xsl:template>

  <!--=====================================-->
	<!--renvoiAutruiTransition-->
	<!--=====================================-->

  <!-- @FIXME : Not yet !! en attente de la modification du schéma -->
<!--  <xsl:template match="contentNode/content//*:renvoiAutruiTransition" mode="makeRelations">
    <xsl:copy>
      <xsl:attribute name="xf:idRef" select="generate-id(.)"/>
      <xsl:attribute name="xf:refType" select="'xf:relation'"/>
      <!-\- FIXME : les attributs issus de la CXML sont-ils à supprimer ? -\->
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>-->

  <!-- @FIXME : Not yet !! en attente de la modification du schéma -->
<!--	<xsl:template match="*:renvoiAutruiTransition" mode="makeRelationElement">
		<relation xf:id="{generate-id(.)}" type="generated">
			<metadata>
				<xsl:if test="@TYPE">
					<meta code="relationDalloz_Type">
						<value><xsl:value-of select="@TYPE"/></value>
					</meta>
				</xsl:if>
			</metadata>
		  <ref calculated="true" xf:refType="efl:{local-name(.)}" 
		    xf:targetResId="{xfSAS:makeQueryIdAttribute(., (
				'typeEE=EFL_INFO',
				concat('parent.', @produitCx ,'.anneePublication=', @annee),
				concat('parent.', @produitCx ,'.numeroOrdre=', tokenize(@numeroDocument, '/')[2]),
				concat('parent.', @produitCx ,'.numeroInfo=', @numeroInfo)
				))}">
			</ref>
		</relation>
	</xsl:template>
-->	
	<!--=====================================-->
  <!--refDoc                               -->
	<!--=====================================-->

	<xsl:template match="*:refDoc" mode="makeRelationElement">

		<!-- Tous les champs possible dans la refdoc/@ref -->
		<xsl:variable name="ref" select="@ref"/>
		<xsl:variable name="champ-TY" select="efl:get-logic-id-value($ref,'TY')"/>
		<xsl:variable name="champ-JUR" select="efl:get-logic-id-value($ref,'JUR')"/>
		<xsl:variable name="champ-LIEU" select="efl:get-logic-id-value($ref,'LIEU')"/>
		<xsl:variable name="champ-DT" select="efl:get-logic-id-value($ref,'DT')"/>
		<xsl:variable name="champ-N" select="efl:get-logic-id-value($ref,'N')"/>

		<!-- Possible besoin pour les autres sources -->
		<xsl:variable name="champ-NAT" select="efl:get-logic-id-value($ref,'NAT')"/>
		<xsl:variable name="champ-FOR" select="efl:get-logic-id-value($ref,'FOR')"/>
		<xsl:variable name="champ-SEC" select="efl:get-logic-id-value($ref,'SEC')"/>
		<xsl:variable name="champ-NOM" select="efl:get-logic-id-value($ref,'NOM')"/>
		<xsl:variable name="champ-NOMTEXTE" select="efl:get-logic-id-value($ref,'NOMTEXTE')"/>
		<xsl:variable name="champ-ART" select="efl:get-logic-id-value($ref,'ART')"/>
		<xsl:variable name="champ-PUB" select="efl:get-logic-id-value($ref,'PUB')"/>
		<xsl:variable name="champ-ORG" select="efl:get-logic-id-value($ref,'ORG')"/>
		<xsl:variable name="champ-QUI" select="efl:get-logic-id-value($ref,'QUI')"/>
		<xsl:variable name="champ-DIFF" select="efl:get-logic-id-value($ref,'DIFF')"/>

	  <relation xf:id="{generate-id(.)}" calculated="true" code="EFL_TR_accrochage_UIActualite_SourcesJP">
			<metadata>
			  <xsl:if test="@type">
					<meta code="relationEFL_TYPE">
						<value>
							<xsl:value-of select="@type"/>
						</value>
					</meta>
				</xsl:if>
				<xsl:if test="@formatRedige">
					<meta code="relationEFL_formatRedige">
						<value>
							<xsl:value-of select="@formatRedige"/>
						</value>
					</meta>
				</xsl:if>
			</metadata>
			<xsl:variable name="jp-referential-items"
				select="$referential-document/sr:root/sr:items[@type=$champ-TY]"/>
			<xsl:choose>
				<xsl:when test="$jp-referential-items">
					<!-- normalisation des champs -->
					<xsl:variable name="code"
						select="$jp-referential-items/sr:item[@jur=$champ-JUR]/@code"/>
					<xsl:choose>
						<xsl:when test="exists($code)">
							<xsl:variable name="date" select="efl:normalize-date($champ-DT)"/>
							<xsl:variable name="lieu" select="efl:normalize-lieu($champ-LIEU)"/>

							<xsl:variable name="meta-name-list"
								select="('ELS_META_jrpJuridiction','ELS_META_jrpDate','ELS_META_jrpLieu','ELS_META_jrpNumjuri')"/>
							<xsl:variable name="meta-value-list"
								select="($code,$date,$lieu,$champ-N)"/>
							<ref 
								xf:refType="efl:{local-name(.)}"
								xf:targetResId="{efl:get-query-string($meta-name-list,$meta-value-list)}"
							>
							  <!--<xsl:attribute name="xf:base" select="$xfSAS:REST.ee.sources.uri"/>-->
							</ref>
						</xsl:when>
						<xsl:otherwise>
						  <xsl:call-template name="els:log">
						    <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
						    <xsl:with-param name="level" select="'error'"/>
						    <xsl:with-param name="code" select="$els:log.level.code"/>
						    <xsl:with-param name="alert" select="true()"/>
						    <xsl:with-param name="markup" select="true()"/>
						    <xsl:with-param name="description">ERROR : Impossible de trouver le code de Juridiction pour la refdoc suivante : <xsl:value-of select="$ref"/>.</xsl:with-param>      
						  </xsl:call-template>
							<ref
								xf:refType="efl:{local-name(.)}"
								xf:targetResId="{xfSAS:makeHistoricIdAttribute(., @ref)}">
							  <!--<xsl:attribute name="xf:base" select="$xfSAS:REST.ee.sources.uri"/>-->
							</ref>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:when>
				<xsl:otherwise>
				  <ref xf:refType="efl:{local-name(.)}" xf:targetResId="{xfSAS:makeHistoricIdAttribute(., @ref)}"/>
				</xsl:otherwise>
			</xsl:choose>
		</relation>
	</xsl:template>

	<xsl:template match="contentNode/content//*:refDoc" mode="makeRelations">
		<xsl:copy>
			<xsl:attribute name="xf:idRef" select="generate-id(.)"/>
			<xsl:attribute name="xf:refType" select="'xf:relation'"/>
			<xsl:apply-templates select="@*" mode="#current"/>
			<xsl:apply-templates mode="#current"/>
		</xsl:copy>
	</xsl:template>

	<!--=====================================-->
	<!--COPY-->
	<!--=====================================-->

	<xsl:template match="node() | @*" mode="makeRelations">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" mode="#current"/>
		</xsl:copy>
	</xsl:template>

	<!--=====================================-->
	<!--FUNCTION-->
	<!--=====================================-->

  <xd:doc>
    <xd:desc>
      <xd:p>Création de la requete vers un élément Flash</xd:p>
      <xd:p>FIXME : duplication de "xfSAS:makeQueryIdAttribute" de "xf-sas-common.xsl" ?</xd:p>
    </xd:desc>
    <xd:param>keys : clés</xd:param>
    <xd:param>values : valeurs</xd:param>
    <xd:return>requete de type 'query:....'</xd:return>
  </xd:doc>
  <xsl:function name="efl:get-query-string" as="xs:string">
		<xsl:param name="keys" as="xs:string*"/>
		<xsl:param name="values" as="xs:string*"/>
		<xsl:variable name="query" as="xs:string*">
			<xsl:for-each select="for $i in 1 to max((count($keys),count($values))) return $i">
				<xsl:variable name="position" select="position()"/>
				<xsl:value-of
					select="string-join(($keys[$position],concat('''',$values[$position],'''')),'=')"
				/>
			</xsl:for-each>
		</xsl:variable>
		<xsl:value-of select="concat('{query:',string-join($query,'&amp;'), '}')"/>
	</xsl:function>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Récupération de l'id logique</xd:p>
    </xd:desc>
    <xd:param>logic-id : identifiant logique</xd:param>
    <xd:param>key : clés</xd:param>
  </xd:doc>
	<xsl:function name="efl:get-logic-id-value" as="xs:string">
		<xsl:param name="logic-id"/>
		<xsl:param name="key"/>
		<xsl:variable name="chunk" select="tokenize($logic-id, '\|')[matches(., concat($key, ':'))]"/>
		<xsl:value-of select="if (count($chunk) &gt;= 1) then tokenize($chunk,':')[2] else ''"/>
	</xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>Normalisation de la date</xd:p>
      <xd:p>A voir si cette fonction n'existe pas déjà dans XslLib ?</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:function name="efl:normalize-date" as="xs:string">
		<xsl:param name="date" as="xs:string"/>
		<xsl:choose>
			<xsl:when test="string-length($date)=8">
				<xsl:value-of
					select="concat(els:makeIsoDate(string-join((substring($date,7,2),substring($date,5,2),substring($date,1,4)),'-'), '-'), 'T00:00:00Z')"
				/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$date"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>

  <xd:doc>
    <xd:desc>
      <xd:p>Normalisation du lieu</xd:p>
      <xd:p>(=> Utilitée ?)</xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:function name="efl:normalize-lieu" as="xs:string">
		<xsl:param name="lieu" as="xs:string"/>
		<xsl:sequence select="$lieu"/>
	</xsl:function>

</xsl:stylesheet>

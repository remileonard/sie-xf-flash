<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfPubliCx="http://www.lefebvre-sarrut.eu/ns/xmlfirst/publication/efl/chaineXml"
  xmlns:cx="http://www.lefebvre-sarrut.eu/ns/efl/chaineXml"
  xmlns:cxRef="http://modeles.efl.fr/modeles/reference"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  exclude-result-prefixes="#all"
  version="2.0">
  
  <!--+==================================================+
      | IMPORT DES LIBRAIRIES UTILITAIRES                |
      +==================================================+-->
  
  <!-- Librairie utilitaire ELS -->
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  <!-- Librairie utilitaire lié aux logs ELS -->
  <xsl:import href="xslLib:/xslLib/els-log.xsl"/>
  <!-- Librairie utilitaire de fonctions XMLFirst -->
  <xsl:import href="xf-ecmRes:/_common/xf-common.xsl"/>
<!--  <!-\- Librairie utilitaire EFL CXML -\->
  <xsl:import href="../lib/publication-efl_XSLCommon.xsl"/> -->
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Configuration du SAS CXML</xd:p>
    </xd:desc>
  </xd:doc>
 
  <!--+==================================================+
      | PARAMETRES ET VARIABLES GLOBALES                 |
      +==================================================+-->
  
  <!-- Répertoire d'écriture des logs -> par défaut, /log à côté du XML source ; sinon, emplacement fourni par Spring XD -->
  <xsl:param name="LOG.URI" required="no" select="resolve-uri('log', base-uri(/))" as="xs:string"/>
  
  <!-- Supprimer les éléments els:log (logs d'exécution) insérés dans le document principal (ils seront dans tous les cas affichés dans un fichier de log séparé) -->
  <xsl:param name="CLEAN_LOG_IN_CONTENT" required="no" select="true()" as="xs:boolean"/>
  
  <!-- Fixation des paramètres pour la gestion des logs -->
  <!-- Message console pour tout log de niveau "debug" et + -->
  <xsl:param name="els:log.level.alert" required="no" select="'debug'" as="xs:string"/>
  <!-- Insertion d'un élément els:log pour tout log de niveau "warning" et +
       Si le template els:log est invoqué avec un niveau "warning" et +, on doit pouvoir insérer un élément els:log à la position courante (si c'est impossible, passer le paramètre $markup de els:log à false())
       Attention en abaissant le niveau de $els:log.level.markup -> cela pourrait potentiellement causer des problèmes, cf. remarque ci-dessus -->
  <xsl:param name="els:log.level.markup" required="no" select="'warning'" as="xs:string"/>
  
  <!-- Répertoire d'écriture des logs (avec "/" final) -->
  <xsl:variable name="log.uri" select="if (ends-with($LOG.URI,'/')) then ($LOG.URI) else (concat($LOG.URI,'/'))" as="xs:string"/>
  
  <xsl:variable name="els:log.level.code" select="'SAS-CXML'" as="xs:string"/>
</xsl:stylesheet>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  exclude-result-prefixes="#all" 
  version="2.0">

  <xsl:import href="revueChaineXml2editorialEntity.xsl"/>

  <!--Paramètre pour indique si on résoud les ui d'un document (ajout EE "ui" dans EE "doc") ou pas (conservation requete => referenceNode)-->
  <xsl:param name="resolveDoc" select="false()" as="xs:boolean"/>
  <xsl:param name="els:log.enable" select="false()" as="xs:boolean"/>
  
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl"
  xmlns:cx="http://modeles.efl.fr/modeles/reference" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xpath-default-namespace="http://modeles.efl.fr/modeles/reference"
  exclude-result-prefixes="#all" version="2.0">

  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>XSLT pour gérer les métadonnées générées (1 seule fois dans le SAS et ensuite éditable dans l'ECM)</xd:p>
      <xd:p>A ne pas confondre avec les métadonnées calculées à partir du contenu (gérées dans sie-xf-ecm-resources), ici les éléments xml qui servent au métadonnées générées ne sont pas conservés. (ici on peut aussi déclarer des métadonnées "générées" qui ne sont pas liées au contenu xml, par exemple une date d'import)</xd:p>
    </xd:desc>
  </xd:doc>

  <xsl:template match="metaCommunes | metaSpecifiques" mode="metadata.doc metadata.ui" priority="1">
    <!--<xsl:apply-templates select=".//*[not(*)]" mode="#current"/>-->
    <xsl:apply-templates mode="#current"/>
  </xsl:template>

  <!-- FIXME : refDoc -->

  <!-- éléments oubliés -->
  <xsl:template match="*[not(*)]" mode="metadata.doc metadata.ui" priority="-1">
    <xsl:call-template name="els:log">
      <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
      <xsl:with-param name="level" select="'error'"/>
      <xsl:with-param name="code" select="$els:log.level.code"/>
      <xsl:with-param name="alert" select="true()"/>
      <xsl:with-param name="markup" select="true()"/>
      <xsl:with-param name="description">[ERROR][SAS] L'élément <xsl:value-of select="local-name(.)"/> n'est pas géré pas la XSL</xsl:with-param>      
    </xsl:call-template>    
  </xsl:template>

  <!-- Structure des métadatas -->
  <xsl:template match="*[*]" mode="metadata.doc metadata.ui" priority="-1">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>

  <!-- Suppression des meta data suivante -->
  <!-- Note : titreCalcule est remplacé par la méta systemTitle-->
  <xsl:template match="titreCalcule | typeObjet | typeContenu | categorieUIRedigee | classRefDoc | publicationAuto | datePeremption | filInfo | modifierDate | alerter" mode="metadata.doc metadata.ui"/>

  <!-- FIXME -->
  <xsl:template match="idDocument | important" mode="metadata.ui"/>

  <!-- Méta données contenant juste de la données et monovaluée -->
  <!-- FIXME : isExUIChiffres -->
  <!-- FIXME : gérer metaSource : refDoc inside -->
  <xsl:template match="numeroInfoDansPublication | numerotationNiveauAuto | isExUIChiffres| porte | dateNumeroRevue | niveauExpertise | anneePublication | numeroOrdre | redacteurChef | version | metaSource" mode="metadata.doc metadata.ui">
    <xsl:choose>
      <xsl:when test="normalize-space(string-join(., '')) = ''">
        <xsl:call-template name="els:log">
          <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
          <xsl:with-param name="level" select="'error'"/>
          <xsl:with-param name="code" select="$els:log.level.code"/>
          <xsl:with-param name="alert" select="false()"/>
          <xsl:with-param name="markup" select="false()"/>
          <xsl:with-param name="description">[INFO] As CXML Meta : &lt;<xsl:value-of select="local-name()"/>&gt; is empty, '<xsl:value-of select="concat('EFL_META_', local-name(.))"/>' is not created.</xsl:with-param>      
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <meta code="EFL_META_{local-name(.)}">      
          <value><xsl:value-of select="normalize-space(string-join(., ' '))"/></value>
        </meta>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <!-- Gestion de la reFDoc -->
<!--  <xsl:template match="refDoc" mode="metadata.doc metadata.ui">
    <meta code="EFL_META_{local-name(.)}">
      <value>
        <ref xf:base="{$xfSAS:REST.ee.sources.uri}" xf:idRef="{xfSAS:makeHistoricIdAttribute(., @ref)}" xf:refType="refDoc"/>
      </value>
      <verbalization>
        <span xmlns="http://www.w3.org/1999/xhtml">
          <!-\- appeler le "truc" html, cf mode='xmlToHtml' -\->
          <!-\- FIXME: trouver un exemple : BRDA 15 20 -\->
          <xsl:value-of select="."/>
        </span>
      </verbalization>
    </meta>    
  </xsl:template>-->

  <xsl:template match="statut" mode="metadata.ui">
    <meta code="EFL_META_actuStatut">
      <value><xsl:value-of select="."/></value>
    </meta>
  </xsl:template>

  <!-- Gestion meta regleNumerotationInfo -->
  <xsl:template match="regleNumerotationInfo" mode="metadata.doc metadata.ui">
    <xsl:variable name="content" select="."/>
    <!-- Ajout EFL_META_regleNumerotation -->
    <meta code="EFL_META_regleNumerotation">
      <value>
        <xsl:choose>
          <xsl:when test="els:isNumber($content)">
            <xsl:value-of select="'numerotationParAnnee'"/>
          </xsl:when>
          <xsl:when test="normalize-space($content) = 'parNumero'">
            <xsl:value-of select="'numerotationSurNumero'"/>
          </xsl:when>
          <xsl:otherwise>
            <!-- 'pasDeNumerotation' (et autres valeurs) -->
            <xsl:value-of select="$content"/>
          </xsl:otherwise>
        </xsl:choose>
      </value>
    </meta>
    <!-- Ajout EFL_META_numeroPremiereInfo -->
    <xsl:if test="els:isNumber($content)">
      <meta code="EFL_META_numeroPremiereInfo">
        <value>
          <xsl:value-of select="$content"/>
        </value>
      </meta>
    </xsl:if>
  </xsl:template>

  <!-- Gestion meta dateValidite -->
  <!-- FIXME : format de la date -->
  <xsl:template match="dateValidite" mode="metadata.doc">
    <!-- YYYY-MM-DDThh:mm:ss.sZ -->
    <xsl:if test="normalize-space(text()) != ''">
      <meta code="EFL_META_dateParution" as="xs:dateTime">
        <value as="xs:dateTime">
          <xsl:value-of select="concat(., 'T00:00:00Z')"/>
        </value>
      </meta>
    </xsl:if>
  </xsl:template>

  <!-- FIXME : format de la date -->
  <xsl:template match="dateValidite" mode="metadata.ui">
    <!-- YYYY-MM-DDThh:mm:ss.sZ -->
    <xsl:if test="normalize-space(text()) != ''">
      <meta code="EFL_META_{local-name(.)}" as="xs:dateTime">
        <value as="xs:dateTime">
          <xsl:value-of select="concat(., 'T00:00:00Z')"/>
        </value>
      </meta>
    </xsl:if>
  </xsl:template>

  <!-- Gestion meta metiers -->
  <xsl:template match="metiers[xfSAS:metaHasContent(.)]" mode="metadata.doc metadata.ui">
    <meta code="EFL_META_metier">
      <xsl:apply-templates mode="#current"/>
    </meta>
  </xsl:template>

  <xsl:template match="metier" mode="metadata.doc metadata.ui">
    <!-- Add item into EFL_META_metier -->
    <xsl:call-template name="xfSAS:addRefentialItemInMeta">
      <xsl:with-param name="referentialFileName" select="'referentielMetiers.xml'"/>
      <xsl:with-param name="referentialIdFlash" select="'EFL_Metiers'"/>
      <xsl:with-param name="CXMLVerbalization" select="."/>
    </xsl:call-template>
  </xsl:template>

  <!-- Gestion meta matieres -->
  <xsl:template match="matieres[xfSAS:metaHasContent(.)]" mode="metadata.doc metadata.ui">
    <meta code="EFL_META_matiere">
      <xsl:apply-templates mode="#current"/>
    </meta>
  </xsl:template>

  <xsl:template match="matiere" mode="metadata.doc metadata.ui">
    <!-- Add item into EFL_META_matiere -->
    <xsl:call-template name="xfSAS:addRefentialItemInMeta">
      <xsl:with-param name="referentialFileName" select="'referentielMatieres.xml'"/>
      <xsl:with-param name="referentialIdFlash" select="'EFL_Matieres'"/>
      <xsl:with-param name="CXMLVerbalization" select="text()"/>
    </xsl:call-template>
  </xsl:template>

  <!-- Gestion meta theme -->
  <xsl:template match="themes[xfSAS:metaHasContent(.)]" mode="metadata.doc metadata.ui">
    <meta code="EFL_META_theme">
      <xsl:apply-templates mode="#current"/>
    </meta>
  </xsl:template>

  <xsl:template match="theme" mode="metadata.doc metadata.ui">
    <!-- Add item into EFL_META_theme -->
    <xsl:call-template name="xfSAS:addRefentialItemInMeta">
      <xsl:with-param name="referentialFileName" select="'referentielThemes.xml'"/>
      <xsl:with-param name="referentialIdFlash" select="'EFL_Themes'"/>
      <xsl:with-param name="CXMLVerbalization" select="text()"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="metiers | matieres | themes" mode="metadata.doc metadata.ui">
    <xsl:call-template name="els:log">
      <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
      <xsl:with-param name="level" select="'info'"/>
      <xsl:with-param name="code" select="$els:log.level.code"/>
      <xsl:with-param name="alert" select="true()"/>
      <xsl:with-param name="markup" select="false()"/>
      <xsl:with-param name="description">[INFO] <xsl:value-of select="local-name(.)"/> non converties par manquante de données dans les éléments descendants.</xsl:with-param>      
    </xsl:call-template> 
  </xsl:template>

  <!-- Suppression des métas : metaOrigine (Jira FLAS-75 point 2 et 3) -->
  <xsl:template match="metaOrigine | produitCx" mode="metadata.doc metadata.ui"/>

  <!-- Add referential item into meta -->
  <xsl:template name="xfSAS:addRefentialItemInMeta">
    <xsl:param name="referentialFileName" as="xs:string" required="yes"/>
    <xsl:param name="referentialIdFlash" as="xs:string" required="yes"/>
    <xsl:param name="CXMLVerbalization" as="xs:string?" select="''" required="no"/>
    <xsl:param name="defaultIdRef" as="xs:string" select="''" required="no"/>

    <xsl:variable name="referential" select="document(concat('./referentialsToRemove/', $referentialFileName))" as="document-node()"/>

    <!-- FIXME : problème de données de certaines UI BPIM-->
    <xsl:variable name="idref">
      <xsl:choose>
        <xsl:when test="$defaultIdRef != ''"><xsl:value-of select="$defaultIdRef"/></xsl:when>
        <xsl:when test="@idref='avocat'">AVOCAT</xsl:when>
        <xsl:when test="normalize-space(@idref) = ''">
          <!-- Recherche un ID en fonction de la verbalisation; note: on prend le premier au cas où.-->
          <xsl:variable name="newIdRef" select="$referential//*[*:libelleComplet = $CXMLVerbalization or *:libelleAbrege = $CXMLVerbalization][1]/@id"/>
          
          <xsl:call-template name="els:log">
            <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
            <xsl:with-param name="level" select="'info'"/>
            <xsl:with-param name="code" select="$els:log.level.code"/>
            <xsl:with-param name="alert" select="true()"/>
            <xsl:with-param name="markup" select="false()"/>
            <xsl:with-param name="description">[INFO] Essai de récupération de @idref depuis le référentiel CXML : newIdRef='<xsl:value-of select="$newIdRef"/>' - for : <xsl:value-of select="local-name(.)"/>, verbalization : '<xsl:value-of select="$CXMLVerbalization"/>'</xsl:with-param>      
          </xsl:call-template>          
          <xsl:value-of select="$newIdRef"/>
        </xsl:when>
        <xsl:otherwise><xsl:value-of select="@idref"/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:choose>
      <xsl:when test="$idref = '' and normalize-space($CXMLVerbalization) = ''">
        <xsl:call-template name="els:log">
          <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
          <xsl:with-param name="level" select="'warning'"/>
          <xsl:with-param name="code" select="$els:log.level.code"/>
          <xsl:with-param name="alert" select="true()"/>
          <xsl:with-param name="markup" select="false()"/>
          <xsl:with-param name="description">[WARNING] Aucune données pour <xsl:value-of select="local-name(.)"/> - La métadata n'est pas convertie.</xsl:with-param>      
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="$idref = ''">
          <xsl:call-template name="els:log">
            <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
            <xsl:with-param name="level" select="'error'"/>
            <xsl:with-param name="code" select="$els:log.level.code"/>
            <xsl:with-param name="alert" select="true()"/>
            <xsl:with-param name="markup" select="true()"/>
            <xsl:with-param name="description">[WARNING] <xsl:value-of select="local-name(.)"/>/@idref = '' (CXML verbalization : <xsl:value-of select="$CXMLVerbalization"/>)</xsl:with-param>      
          </xsl:call-template>
        </xsl:if>
        
        <!-- FIXME : début du code à supprimer tant que les référentiels ne sont pas accessible depuis l'ECM-->
        <xsl:variable name="value" select="normalize-space($referential//*[@id=$idref]/libelleComplet)"/>
          <!-- FIXME : fin du code à supprimer -->
        <!-- FIXME : test mis en place pour la démo de Janvier 2017 : pb avec avocat VS AVOCAT -->
        <value>
          <!--Meta vers référentiel-->
          <ref idRef="{$idref}" xf:targetResId="{$referentialIdFlash}" xf:refType="xf:referenceItemRef" xf:targetResType="referenceTable"/>
          <!--<xsl:value-of select="normalize-space($value)"/>-->
        </value>
        <xsl:if test="$idref">
          <verbalization>
            <span xmlns="http://www.w3.org/1999/xhtml">
              <xsl:value-of select="$value"/>
            </span>
          </verbalization>
        </xsl:if>      
      </xsl:otherwise>
      
    </xsl:choose>
  </xsl:template>

  <xsl:template match="e" mode="metadata.doc metadata.ui">
    <xsl:apply-templates select="." mode="xfRes:cxml2html"/>
  </xsl:template>

  <!-- Utile ? -->
  <xsl:template match="*" mode="metadata.doc metadata.ui" priority="-2">
    <xsl:call-template name="els:log">
      <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
      <xsl:with-param name="level" select="'error'"/>
      <xsl:with-param name="code" select="$els:log.level.code"/>
      <xsl:with-param name="alert" select="true()"/>
      <xsl:with-param name="markup" select="true()"/>
      <xsl:with-param name="description">[ERROR] méta <xsl:value-of select="local-name(.)"/> non convertie : <xsl:value-of select="els:get-xpath(.)"/></xsl:with-param>      
    </xsl:call-template>
  </xsl:template>

  <xsl:function name="xfSAS:metaHasContent" as="xs:boolean">
    <xsl:param name="e" as="element()"/>    
    <xsl:choose>
      <xsl:when test="normalize-space(string-join($e, '')) = '' and count($e//@idref) = 0">
        <xsl:value-of select="false()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="true()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

</xsl:stylesheet>

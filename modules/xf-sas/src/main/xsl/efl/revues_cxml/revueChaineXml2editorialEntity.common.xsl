<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
	xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
	xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl"
	xmlns:cx="http://modeles.efl.fr/modeles/reference"
	xmlns:itg="http://www.infotrustgroup.com/"
	xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
	xmlns:xfSAScxml="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas/cxml"
	xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	xpath-default-namespace="http://modeles.efl.fr/modeles/reference"
	exclude-result-prefixes="#all"
	version="2.0">
  
  <xsl:param name="xfSAS:company" select="'efl'" as="xs:string"/>
  
  <!--Paramètre pour indique si on résoud les ui d'un document (ajout EE "ui" dans EE "doc") ou pas (conservation requete => referenceNode)-->
  <xsl:param name="resolveDoc" select="true()" as="xs:boolean"/>
  
  <!--lié à l'ancien import cx-common.xsl-->
  <!--<xsl:param name="platform" select="''"/>-->
  
  <xsl:variable name="contentNode.namespace.uri.infoCommentaire" select="'http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire'" as="xs:string"/>
  <xsl:variable name="contentNode.namespace.uri.infoChiffres" select="'http://www.lefebvre-sarrut.eu/ns/efl/infoChiffres'" as="xs:string"/>
  <xsl:variable name="contentNode.namespace.uri.QuestionsReponses" select="'http://www.lefebvre-sarrut.eu/ns/efl/infoQuestionsReponses'" as="xs:string"/>
  
  <!--==================================================-->
  <!--FUNCTION xfSAScxml-->
  <!--==================================================-->
	<!-- FIXME : gestion des logs dans la fonction !!-->
  <xsl:function name="xfSAScxml:getEEcode" as="xs:string">
    <xsl:param name="e" as="element()"/> <!--element cxml-->
    <xsl:choose>
      <xsl:when test="$e/self::cx:doc">
        <xsl:variable name="produitCx" select="$e/metadonnees/metaCommunes/metaType/produitCx/@idref" as="xs:string"/>
        <xsl:value-of select="concat('EFL_TEE_', $produitCx)"/>
      </xsl:when>
      <xsl:when test="$e/self::cx:ui">
        <xsl:choose>
          <xsl:when test="$e/cx:infoCommentaire">
            <xsl:text>EFL_TEE_uiCommentaire</xsl:text>
          </xsl:when>
          <xsl:when test="$e/cx:infoChiffres">
            <xsl:text>EFL_TEE_uiCommentaire</xsl:text>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message>[ERROR][xfSAScxml:getEEcode] code EE introuvable pour ui <xsl:value-of select="$e/@id"/></xsl:message>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>[ERROR][xfSAScxml:getEEcode] Appel de la fonction xfSAScxml:getEEcode(<xsl:value-of select="name($e)"/>) avec un élément autre que cx:doc ou cx:ui</xsl:message>        
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>
      <xd:p>Résolution de l'uri d'une UI à partir de son id par recherche dans les répertoire de l'EDP.</xd:p>
      <xd:p>On estime que le nom du fichier ui = son @id</xd:p>
      <xd:p>Structure EDP : ici on suppose qu'on travaille sur du filesystem de xdb (export + unzip) avec cette structure :
        <![CDATA[
        + EDP
          + Document
          + MultiMedia
          + SourceUI
          + UI
            + Chiffres
            + ChiffresReference
            + Commentaire
            + MultiMedia
            + QuestionsReponses
         ]]>
      </xd:p>
    </xd:desc>
  </xd:doc>
  <xsl:function name="xfSAScxml:getUiUriFromId" as="xs:anyURI?">
	  <xsl:param name="ui.id" as="xs:string"/>
	  <xsl:param name="EDP.uri" as="xs:anyURI"/>
	  <xsl:variable name="uiCommentaire.uri" select="resolve-uri(concat('UI/Commentaire/' ,$ui.id, '.xml'), $EDP.uri)" as="xs:anyURI"/>
	  <xsl:variable name="uiChiffres.uri" select="resolve-uri(concat('UI/Chiffres/' ,$ui.id, '.xml'), $EDP.uri)" as="xs:anyURI"/>
	  <xsl:choose>
	    <xsl:when test="doc-available($uiCommentaire.uri)">
	      <xsl:sequence select="$uiCommentaire.uri"/>
	    </xsl:when>
	    <xsl:when test="doc-available($uiChiffres.uri)">
	      <xsl:sequence select="$uiChiffres.uri"/>
	    </xsl:when>
	    <xsl:otherwise>
	      <xsl:sequence select="()"/>
	    </xsl:otherwise>
	  </xsl:choose>
	</xsl:function>
  
  <!--==================================================-->
	<!--FUNCTION CX-->
	<!--==================================================-->
  
	<!--copié depuis svn+ssh://svn02/repositories/dev/projets/xchaines/trunk/developpements/commun/lib/cx-common.xsl-->
	
	<xd:doc>
		<xd:desc>
			<xd:p>Indique si un élément a un marqueur donné dans son attribut @marqueurs</xd:p>
			<xd:p>On peut passer une sequence de marqueurs et profiter de la comparaison de séquence en xpath 
				(c'est un "OR" implicite :  si l'un des marqueurs est trouvé, la fonction renvoi true())</xd:p>
		</xd:desc>
	</xd:doc>
	<xsl:function name="cx:hasMarqueur" as="xs:boolean">
		<xsl:param name="e" as="element()"/>
		<xsl:param name="marqueur" as="xs:string*"/>
		<!--comparaison de sequences : est vrai si au moins une valeur est égale-->
		<xsl:sequence select="tokenize($e/@marqueurs, '\s+') = $marqueur"/>
	</xsl:function>
	
	<xd:doc>
		<xd:desc>
			<xd:p>Indique si un élément donné est la racine d'une "info" (ui des revues), 
				on se base sur les nom de balise qui sont assez explicites.</xd:p>
			<xd:p>On se base sur les schema xsd pour la liste des éléments faisant office d'info sous une ui</xd:p>
		</xd:desc>
		<xd:param name="e">élement que l'on veut tester</xd:param>
		<xd:return>boolean</xd:return>
	</xd:doc>
	<xsl:function name="cx:isInfo" as="xs:boolean">
		<xsl:param name="e" as="element()"/>
		<xsl:sequence select="matches(local-name($e), '(infoCommentaire | infoChiffres | infoAgenda | infoAnnonce | infoGuide | uiMultimedia | infoPresentation | infoQuestionsReponses)', 'x')"/>
		<!--flag 'x' : ignore les espaces dans la regex-->
	</xsl:function>
	
</xsl:stylesheet>
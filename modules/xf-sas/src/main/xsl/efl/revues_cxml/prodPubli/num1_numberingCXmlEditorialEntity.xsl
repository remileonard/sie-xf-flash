<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:infoCommentaire="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire"
  xmlns:local-num1="num1_numberingUiInDocument.xsl" 
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  exclude-result-prefixes="#all" 
  version="2.0">

  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  <xsl:import href="../conf/revueChaineXml2editorialEntity.conf.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Numérotation des referenceNodes</xd:p>
      <xd:p>Conversion du traitement CXML : num1_numberingUiInDocument.xsl</xd:p>
      <xd:p>FIXME: ajuster le test isRJF lorsque les référenciel produit seront à jour.</xd:p>
    </xd:desc>    
  </xd:doc> 
  
  <xsl:template match="/" priority="-1">    
    <xsl:apply-templates mode="numbering"/>
  </xsl:template>

  <!-- 
    Cas spécifique d'une conclusion d'une analyse.
    element(referenceNode) dans : 
    - revue RJF,
    - corpus,
    - structuralNode['rjConclusions'].
  -->
  <xsl:template match="referenceNode
    [local-num1:isNumerotable(.)]
    [local-num1:isInCorpus(.)]
    [local-num1:isInConclusions(.)]
    " mode="numbering" priority="3">
    <xsl:copy copy-namespaces="no">
      <xsl:copy-of select="@* except @num"/>
      <!-- Récupératon des infoCommentaire:renvoiAutrui qui pointe vers une EE de type 'rjDecision'-->
      <xsl:variable name="renvoiAutruiS" select=".//infoCommentaire:renvoiAutrui[local-num1:isRenvoiAutruiConclusionToDecision(.)]" as="element(infoCommentaire:renvoiAutrui)*"/>
      <xsl:variable name="targetReferenceNodeS" select="local-num1:getTargetReferenceNode($renvoiAutruiS)"/>
      <xsl:choose>
        <xsl:when test="count($targetReferenceNodeS)=1 or count($targetReferenceNodeS)=2">
          <xsl:variable name="num">
            <xsl:value-of select="concat('C', $els:NO_BREAK_SPACE, local-num1:getNumValue($targetReferenceNodeS[1]))"/>
            <xsl:if test="count($targetReferenceNodeS)=2">
              <xsl:text> et </xsl:text>
              <xsl:value-of select="concat('C', $els:NO_BREAK_SPACE, local-num1:getNumValue($targetReferenceNodeS[2]))"/>
            </xsl:if>
          </xsl:variable>
          <xsl:attribute name="num" select="$num"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="els:log">
            <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
            <xsl:with-param name="level" select="'info'"/>
            <xsl:with-param name="code" select="$els:log.level.code"/>
            <xsl:with-param name="alert" select="true()"/>
            <xsl:with-param name="markup" select="true()"/>
            <xsl:with-param name="description">La referenceNode conclusion '<xsl:value-of select="@xf:id"/>' 
              renvoie vers <xsl:value-of select="count($renvoiAutruiS)"/> renvoiAutrui Conclusion to Décision               
              Il devrait y avoir 1 ou 2 UI "décision" associées normalement. 
              La numérotation de l'ui conclusion ne peut se faire correctement.
            </xsl:with-param>      
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>  

  <!-- referenceNode numérotable dans structuralNode de type corpus -->
  <xsl:template match="referenceNode
    [local-num1:isNumerotable(.)]
    [local-num1:isInCorpus(.)]" mode="numbering" priority="2">
    <xsl:copy copy-namespaces="no">
      <xsl:copy-of select="@* except @num"/>      
      <xsl:attribute name="num" select="local-num1:getNumValue(.)"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>  

  <!-- referenceNode non numérotable -->
  <xsl:template match="referenceNode
    [local-num1:isInCorpus(.)]" mode="numbering" priority="1">
    <xsl:copy copy-namespaces="no">
      <xsl:copy-of select="@* except @num"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template> 

  <xsl:function name="local-num1:isRenvoiAutruiConclusionToDecision" as="xs:boolean">
    <xsl:param name="renvoiAutrui" as="element(infoCommentaire:renvoiAutrui)"/>
    <xsl:choose>
      <!--si le renvoiAutrui n'est pas dans docNiv rjConclusions : non-->
      <xsl:when test="not($renvoiAutrui/ancestor::structuralNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurStructure','rjConclusions')])">
        <xsl:sequence select="false()"/>
      </xsl:when>
      <!--si le renvoiAutrui pointe vers un élément dans l'ui (et non seulement l'ui) : non-->
      <xsl:when test="$renvoiAutrui/@ref and $renvoiAutrui/@ref != $renvoiAutrui/@uiId">
        <xsl:sequence select="false()"/>
      </xsl:when>
      <!--si le renvoiAutrui n'est pas dans le blocRefDoc principal : non-->
      <xsl:when test="not($renvoiAutrui/ancestor::infoCommentaire:blocRefDoc/parent::*/parent::content)">
        <xsl:sequence select="false()"/>
      </xsl:when>
      <!--Si la cible du renvoiAutrui n'est pas dans le docNiv rjDecisions : non -->      
      <xsl:when test="not(local-num1:isInDecisions(local-num1:getTargetReferenceNode($renvoiAutrui)))">
        <xsl:sequence select="false()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="true()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <!-- Retourne le referentiaLNode pointé par le infoCommentaire:renvoiAutrui -->
  <xsl:function name="local-num1:getTargetReferenceNode" as="element(referenceNode)*">
    <xsl:param name="renvoiAutrui" as="element(infoCommentaire:renvoiAutrui)*"/>
    <!-- lien du renvoiAutrui vers la relation (xf:id de la relation)-->
    <xsl:variable name="xf:idRelation" select="$renvoiAutrui/@xf:idRef"/>
    <!-- lien de la relation vers l'EE cible (xf:id de l'EE) -->
    <xsl:variable name="xf:idEETarget" select="$renvoiAutrui/ancestor::editorialEntity//relation[@xf:id = $xf:idRelation]/ref/@xf:targetResId"/>
    <!-- EE target -->
    <!-- FIXME : pour l'adaptation en action, utiliser un key() -->
    <xsl:variable name="eeTarget" select="root($renvoiAutrui[1])//editorialEntity[@xf:id = $xf:idEETarget]" as="element(editorialEntity)*"/>
    <!-- retourne vrai si eeTarget est dans rjDecisions -->
    <xsl:sequence select="$eeTarget/ancestor::referenceNode"/>    
  </xsl:function>

  <!-- Calcul le numéro de la referenceNode dans l'EE -->
  <xsl:function name="local-num1:getNumValue" as="xs:string*">
    <xsl:param name="e" as="element(referenceNode)"/>    
    <xsl:variable name="root" select="$e/ancestor-or-self::*[last()]" as="element(editorialEntity)"/>
    <xsl:variable name="numeroPremiereInfo" select="$root/self::editorialEntity/metadata/meta[@code='EFL_META_numeroPremiereInfo']/value" as="xs:string*"/>
    <!-- Set the startNum -->
    <xsl:variable name="startNum" select="if (els:isInteger($numeroPremiereInfo)) then (xs:integer($numeroPremiereInfo) - 1)  else (0)" as="xs:integer"/>
    <!-- Count -->
    <xsl:variable name="counter" as="xs:integer">
      <xsl:number select="$e" count="referenceNode[local-num1:isNumerotable(.)]
        [not(local-num1:hasReferentialMeta(., 'EFL_META_marqueurRubriqueRef', 'infoPlus_requete'))]
        [not(ancestor::structuralNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurRubriqueStruct', 'infoPlus_docNiv')])]
        " level="any"/>
    </xsl:variable>
    <!-- Set number -->
    <xsl:variable name="num" select="$startNum + $counter" as="xs:integer"/>
      <xsl:choose>
        <!--Pour infoPlus, cf. CXF-658-->
        <xsl:when test="$e/ancestor::structuralNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurRubriqueStruct', 'infoPlus_docNiv')]">
          <xsl:variable name="precedentInfoPlus">
            <xsl:number select="$e" count="referenceNode[ancestor::structuralNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurRubriqueStruct', 'infoPlus_docNiv')]]" format="1" />
          </xsl:variable>
          <xsl:value-of select="concat($num, '-', $precedentInfoPlus)"/>
        </xsl:when>
        <xsl:when test="$e/ancestor::structuralNode[1][local-num1:hasReferentialMeta(., 'EFL_META_marqueurRubriqueRef', 'infoPlus_requete')]">
          <xsl:variable name="precedentInfoPlus">
            <xsl:number select="$e" count="referenceNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurRubriqueRef', 'infoPlus_requete')]" format="1" />
          </xsl:variable>
          <xsl:value-of select="concat($num, '-', $precedentInfoPlus)"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$num"/>
        </xsl:otherwise>
      </xsl:choose>      
  </xsl:function>

  <!-- Vérification si le referenceNode est numérotable -->
  <xsl:function name="local-num1:isNumerotable" as="xs:boolean">
    <xsl:param name="referenceNode" as="element(referenceNode)"/>
    <xsl:sequence select="
      (: editorialEntity à numéroter globalement :)
      normalize-space($referenceNode/ancestor::editorialEntity[last()]/metadata/meta[@code='EFL_META_regleNumerotation']/value) != 'pasDeNumerotation'
      (: referenceNode dans le corpus :)       
      and local-num1:isInCorpus($referenceNode)
      and not(local-num1:hasBooleanMeta($referenceNode, 'EFL_META_marqueurPasDeNumerotation'))
      and not($referenceNode/ancestor::structuralNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurStructure','enBref')])
      and not($referenceNode/ancestor::structuralNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurStructure','edito')]) 
      and not($referenceNode/ancestor::structuralNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurStructure','veille')])
      and not($referenceNode/ancestor::structuralNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurStructure','chiffres')])
    "/>
  </xsl:function>

  <!-- Vérification si referenceNode a une méta (@code) de type boolean à 'true' -->
  <xsl:function name="local-num1:hasBooleanMeta" as="xs:boolean">
    <xsl:param name="e" as="element(referenceNode)"/>
    <xsl:param name="code" as="xs:string"/>
    <xsl:sequence select="exists($e/metadata/meta[@code=$code][value='true'])"/>
  </xsl:function>

  <!-- Vérification si referenceNode a une méta (@code) de type référentiel @idRef --> 
  <xsl:function name="local-num1:hasReferentialMeta" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:param name="code" as="xs:string"/>
    <xsl:param name="idRef" as="xs:string"/>
    <xsl:sequence select="exists($e/metadata/meta[@code=$code][value/ref[@idRef=$idRef]])"/>
  </xsl:function>

  <!-- Vérification si le referenceNode est dans un structuralNode de type 'rjConclusions' -->
  <xsl:function name="local-num1:isInConclusions" as="xs:boolean">
    <xsl:param name="e" as="element(referenceNode)"/>    
    <xsl:sequence select="boolean($e/ancestor::structuralNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurStructure','rjConclusions')])"/>    
  </xsl:function>

  <!-- Vérification si le referenceNode est dans un structuralNode de type 'rjDecisions' -->
  <xsl:function name="local-num1:isInDecisions" as="xs:boolean">
    <xsl:param name="e" as="element(referenceNode)"/>    
    <xsl:sequence select="boolean($e/ancestor::structuralNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurStructure','rjDecisions')])"/>    
  </xsl:function>
  
  <!-- Vérification si le referenceNode est dans un structuralNode de type 'corpus' -->
  <!-- FIXME: Corpus vs corpus ???? synchroniser avec le fichier referentiel_marqueur_global.xml -->
  <xsl:function name="local-num1:isInCorpus" as="xs:boolean">
    <xsl:param name="e" as="element(referenceNode)"/>  
    <xsl:sequence select="boolean($e/ancestor::structuralNode[local-num1:hasReferentialMeta(., 'EFL_META_marqueurStructure', 'corpus')])"/>
  </xsl:function>

  <!-- Retourne "true" si le produit est RJF -->
  <!-- FIXME : modifier le test lorsque les méta référentiels seront présentes -->
  <xsl:function name="local-num1:isRJF" as="xs:boolean">
    <xsl:param name="e" as="element()"/>    
    <xsl:sequence select="root($e)/@code='EFL_TEE_rjf'"/>
  </xsl:function>

  <!-- Default template -->
  <xsl:template match="*" mode="numbering">
    <xsl:copy copy-namespaces="yes">
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>

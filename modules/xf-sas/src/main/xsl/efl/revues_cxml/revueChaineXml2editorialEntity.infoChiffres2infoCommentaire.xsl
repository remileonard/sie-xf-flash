<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl" 
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xmlns="http://modeles.efl.fr/modeles/reference" 
  xpath-default-namespace="http://modeles.efl.fr/modeles/reference" 
  exclude-result-prefixes="#all" 
  version="2.0">
  
  <xd:doc scope="stylesheet">
    <xd:desc>
      <xd:p>Conversion des UI infoChiffres en UI infoCommentaire (cf. jira FLAS-36)</xd:p>
    </xd:desc>
  </xd:doc>
  
  <!--==================================================-->
  <!--INIT-->
  <!--==================================================-->
  
  <xsl:template match="/ui" mode="infoChiffres2infoCommentaire">
    <!-- Transformation d'une UI type infoChiffres en UI infoCommentaire -->
    <xsl:call-template name="els:log">
      <xsl:with-param name="xsltName" select="els:getFileName(static-base-uri())"/>
      <xsl:with-param name="level" select="'info'"/>
      <xsl:with-param name="code" select="$els:log.level.code"/>
      <xsl:with-param name="alert" select="false()"/>
      <xsl:with-param name="markup" select="false()"/>
      <xsl:with-param name="description">[INFO] Transformation de l'UI Chiffres <xsl:value-of select="@id"/> en UI Commentaire.</xsl:with-param>      
    </xsl:call-template>
    
    <xsl:copy>
      <xsl:apply-templates select="@* except @xsi:*" mode="infoChiffres2infoCommentaire"/>
      <xsl:apply-templates mode="infoChiffres2infoCommentaire"/>
    </xsl:copy>
  </xsl:template>
  
  
  <!--==================================================-->
  <!--MAIN-->
  <!--==================================================-->
  
  <xsl:template match="infoChiffres" mode="infoChiffres2infoCommentaire">
    <infoCommentaire>
      <xsl:apply-templates select="@*" mode="infoChiffres2infoCommentaire"/>
      <!-- Bloc d'éléments preceding-sibling de <commentaire> dans <infoCommentaire> -->
      <xsl:variable name="blocPrec" select="(titre | sousTitre | titreObjet | titreUne | titreSommaire | definition)" as="element()*"/>
      <!-- Bloc d'éléments following-sibling de <commentaire> dans <infoCommentaire> -->
      <xsl:variable name="blocFoll" select="(blocNotes)" as="element()*"/>
      <xsl:apply-templates select="$blocPrec" mode="infoChiffres2infoCommentaire"/>
      <!-- Niveau d'encapsulation n'existant pas dans infoChiffres -->
      <commentaire id="{generate-id()}">
        <xsl:apply-templates select="* except ($blocPrec union $blocFoll)" mode="infoChiffres2infoCommentaire"/>
      </commentaire>
      <xsl:apply-templates select="$blocFoll" mode="infoChiffres2infoCommentaire"/>
    </infoCommentaire>
  </xsl:template>
  
  <!-- Insertion d'une métadonnée indiquant que l'UI infoChiffres a été transformée en UI infoCommentaire -->
  <xsl:template match="metaCommunes/*[last()]" mode="infoChiffres2infoCommentaire">
    <xsl:next-match/>
    <isExUIChiffres>true</isExUIChiffres>
  </xsl:template>
  
  <!-- N'existe pas dans infoCommentaire, transformé en microContenu/introduction/al -->
  <xsl:template match="definition[normalize-space()]" mode="infoChiffres2infoCommentaire">
    <microContenu>
      <introduction id="{generate-id()}">
        <al>
          <xsl:apply-templates mode="infoChiffres2infoCommentaire"/>
        </al>
      </introduction>
    </microContenu>
  </xsl:template>
  
  <!-- Suppression des définitions vides, qui n'ont pas d'équivalent dans les infoCommentaire
       + pour ne pas générer de <microContenu> vide -->
  <xsl:template match="definition" mode="infoChiffres2infoCommentaire"/>
  
  
  <!--==================================================-->
  <!--COMMON-->
  <!--==================================================-->
  
  <xsl:template match="node() | @*" mode="infoChiffres2infoCommentaire">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="infoChiffres2infoCommentaire"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:stylesheet>
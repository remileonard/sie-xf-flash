@ECHO OFF
SETLOCAL ENABLEEXTENSIONS
SETLOCAL ENABLEDELAYEDEXPANSION

REM *********************************************
REM * Exécute la conversion UI CXML -> EE FLASH *
REM *********************************************
SET CMD_LINE_ARGS=%*
SET CMD_LINE_ARGS=%CMD_LINE_ARGS:"=""%
GOTO :MAIN

:INIT
REM Initialisation
SET CWD=%~dp0
SET CWD=%CWD:\=/%
REM Répertoire racine contenant le launcher Calabash + CP
SET CALABASH=%CWD%_CalabashCP/
REM BATCH de paramétrage Calabash avec protocole CP
SET BIN=%CALABASH%bin/
SET XPROCCP=%BIN%CalabashCP.bat
REM Livrable de la conversion SAS
SET TARGET=%CWD%target/
SET SASCONV=%TARGET%xf-sas-1.0-SNAPSHOT-jar-with-dependencies-with-catalog.jar
REM XProc pour exécuter la conversion SAS
SET XPL=%CWD%xpl/
SET XPL_MAIN_SCRIPT=file:/%XPL%CXML2EE.xpl
REM XProc de lancement temporaire, créé par ce BATCH
SET XPL_TMP=%CWD%_tmp/
SET DATE_F=%DATE:~6,4%%DATE:~3,2%%DATE:~0,2%
SET TIME_F=
FOR /f "tokens=1,2 delims=:" %%a in ('TIME /t') do set TIME_F=%%a%%b
SET XPL_TMP_SCRIPT=%XPL_TMP%_CXML2EE_%RANDOM%_%DATE_F%-%TIME_F%%TIME:~6,2%%TIME:~9,2%.xpl
SET XPL_TMP_SCRIPT_FILE=file:/%XPL_TMP_SCRIPT:\=/%

REM Valeur des arguments de la ligne de commande
SET INPUT_FILE=
SET INPUT_DIR=
REM Répertoire d'output par défaut
SET OUTPUT_DIR=file:/%CWD%out
SET PARAMS=
SET DEBUG=
SET HELP=
SET XPROC_OVERRIDE=

REM Initialisation du _CLASSPATH
SET _CLASSPATH=
REM Conversion SAS
SET _CLASSPATH=%_CLASSPATH%;%SASCONV%
REM D'autres librairies seront ajoutées au _CLASSPATH, mais il est important que celle-ci apparaisse en 1er
GOTO :EOF

:CMD_PARSE
REM Récupération des arguments de la ligne de commande
REM EX. : -i /in/UI.xml -o /test/out
REM EX. : -i /in/UI.xml
REM Ex. : -i /test/in -o /test/out
REM Ex. : -i /test/in -o /test/out -d true -p filter=ExpressionXPath -p resolveDoc=true
REM /!\ Chemins absolus pour -i et -o
IF "%*" NEQ "" (
	FOR /F "tokens=1,2,* delims= " %%i IN ("%*") DO ( CALL :ARG_VAL %%i %%j & CALL :CMD_PARSE %%k )
)
GOTO :EOF

:ARG_VAL
REM Assignation des valeurs des arguments
IF /i "%1" EQU "-h" (
	REM Aide
	SET HELP=true
) ELSE IF /i "%1" EQU "-d" (
	REM Si debug, le XProc de lancement n'est pas supprimé
	SET DEBUG=true
) ELSE IF /i "%1" EQU "-x" (
	REM Utilisation d'un fichier XProc de lancement existant
	SET XPROC_OVERRIDE=%~2
	SET XPROC_OVERRIDE=file:/!XPROC_OVERRIDE:\=/!
) ELSE IF /i "%1" EQU "-i" (
	REM Check si l'input est un fichier XML ou un répertoire
	REM Se base sur la présence de ".xml" dans le nom... C'est un peu vilain.
	IF /i "%~x2" EQU ".xml" (
		SET INPUT_FILE=%~dpnx2
		SET INPUT_FILE=file:/!INPUT_FILE:\=/!
	) ELSE (
		SET INPUT_DIR=%~dpnx2
	)
) ELSE IF /i "%1" EQU "-o" (
	REM Répertoire de sauvegarde
	SET OUTPUT_DIR=%~dpnx2
	SET OUTPUT_DIR=file:/!OUTPUT_DIR:\=/!
) ELSE IF /i "%1" EQU "-p" (
	REM Parsing des paramètres
	FOR /F "tokens=1,2,* delims== " %%i IN ("%*") DO (
		SET VAL=%%k
		SET VAL=!VAL:""=!
		SET VAL=!VAL:'=^&quot;!
		SET PARAMS=!PARAMS!^<p:with-param name="%~2" select="'!VAL!'"^>^<p:empty/^>^</p:with-param^>
	)
) ELSE (
	ECHO Argument %1 inconnu.
)
GOTO :EOF

:MAKE_XPROC_TMP
REM Construction du XProc de lancement
IF NOT EXIST "%XPL_TMP%" MKDIR "%XPL_TMP%"
ECHO ^<?xml version="1.0" encoding="UTF-8"?^> > %XPL_TMP_SCRIPT%
ECHO ^<p:declare-step xmlns:p="http://www.w3.org/ns/xproc" xmlns:c="http://www.w3.org/ns/xproc-step" xmlns:cx="http://xmlcalabash.com/ns/extensions" xmlns:err="http://www.w3.org/ns/xproc-error" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" version="1.0" name="main"^> >> %XPL_TMP_SCRIPT%
ECHO ^<p:input port="source"^> >> %XPL_TMP_SCRIPT%
REM Chargement de chaque fichier source
CALL :INPUT_SRC
REM Fin du script XProc
ECHO ^</p:input^> >> %XPL_TMP_SCRIPT%
ECHO ^<p:output port="result" primary="true" sequence="true"^>^<p:empty/^>^</p:output^> >> %XPL_TMP_SCRIPT%
ECHO ^<p:input port="parameters_port" kind="parameter" primary="true"/^> >> %XPL_TMP_SCRIPT%
ECHO ^<p:import href="%XPL_MAIN_SCRIPT%"/^> >> %XPL_TMP_SCRIPT%
ECHO ^<xf:CXML2EE^> >> %XPL_TMP_SCRIPT%
ECHO ^<p:with-param name="output_dir" select="'%OUTPUT_DIR%/'"^>^<p:empty/^>^</p:with-param^> >> %XPL_TMP_SCRIPT%
IF "!PARAMS!" NEQ "" ( ECHO !PARAMS! >> %XPL_TMP_SCRIPT% )
ECHO ^</xf:CXML2EE^> >> %XPL_TMP_SCRIPT%
ECHO ^<p:sink/^> >> %XPL_TMP_SCRIPT%
ECHO ^</p:declare-step^> >> %XPL_TMP_SCRIPT%
GOTO :EOF

:INPUT_SRC
REM Ecriture des <p:document> dans le XProc de lancement
IF "%INPUT_FILE%" NEQ "" (
	ECHO ^<p:document href="%INPUT_FILE%"/^> >> %XPL_TMP_SCRIPT%
) ELSE (
	FOR /F "usebackq tokens=*" %%i IN (`DIR /S /B %INPUT_DIR%\*.xml ^| FINDSTR /v "Dup-.*xml" ^| FINDSTR /v ".*(1)\.xml" ^| FINDSTR /v "R-d3ec26f2-396b-4fe2-8617-00b06eee1e7e\.xml" ^| FINDSTR /v "R-23ef15f4-f517-4d1d-ba12-6417fda0f9a1\.xml" ^| FINDSTR /v "R-43962df4-c6d6-4a64-84db-c8c59bb1ed2c\.xml" ^| FINDSTR /v "R-6d1f7cba-d75f-45bd-8fcc-efa75c4cb3fc\.xml" ^| FINDSTR /v "R-dacd4a91-be14-4809-8568-57ec32b2ac7d\.xml" ^| FINDSTR /v "R-ff7c5125-98d4-49b7-9f42-96e83a995e58\.xml"`) DO CALL :ITERFILES %%~fi
)
GOTO :EOF

:ITERFILES
REM Parcours de la liste des documents (avec filtre sur les noms cf. documents en erreur identifiés)
SET FILE=%1
SET FILE=%FILE:\=/%
SET FILE=file:/%FILE%
ECHO ^<p:document href="%FILE%"/^> >> %XPL_TMP_SCRIPT%
GOTO :EOF

:RUN
REM Exécution
REM ECHO %RUN_CALABASH% %XPL_TMP_SCRIPT_FILE%
%RUN_CALABASH% %XPL_TMP_SCRIPT_FILE%
REM Nettoyage (sauf si -d)
IF "%DEBUG%" EQU "" (
	RMDIR /S /Q "%XPL_TMP%"
)
GOTO :EOF

:RUN_OVERRIDE
REM Exécution d'un fichier XProc de lancement existant
ECHO Override -^> %XPROC_OVERRIDE%
%RUN_CALABASH% %XPROC_OVERRIDE%
GOTO :EOF

:HELP
REM Aide
ECHO -----------------
ECHO CXML2EE_XProc.bat
ECHO -----------------
ECHO.
ECHO Syntaxe de base (chemins absolus ou relatifs pour -i et -o) :
ECHO.
ECHO Un seul fichier en entree, resultat genere dans le repertoire ./test/out
ECHO     CXML2EE_XProc.bat -i ./in/UI.xml -o ./test/out
ECHO.
ECHO Iteration sur tous les fichiers XML du repertoire en entree, resultat genere dans le repertoire ./test/out
ECHO     CXML2EE_XProc.bat -i ./test/in -o ./test/out
ECHO.
ECHO -----------------
ECHO.
ECHO Utilisation de parametres lies au programme XProc (cumulables)
ECHO Syntaxe :     -p nomParam^="ValeurDuParametreSansEspaceEntreGuillemets"
ECHO.
ECHO Ex. : filtrage des documents sur la base d'une requete XPath (parametre $filter dans le programme XProc)
ECHO     CXML2EE_XProc.bat -i ./test/in -o ./test/out -p filter^="/*"
ECHO.
ECHO Ex. : parametre $resolveDoc de la transformation SAS (valeur booleenne)
ECHO     CXML2EE_XProc.bat -i ./test/in -o ./test/out -p resolveDoc^="true"
ECHO.
ECHO Ex. : validation en sortie des documents au format FLASH EE
ECHO     CXML2EE_XProc.bat -i ./test/in -o ./test/out -p validate_output^="true"
ECHO.
ECHO -----------------
ECHO.
ECHO Astuces :
ECHO     Ne pas effacer le programme XProc temporaire cree :     -d true
ECHO     Utiliser un programme XProc temporaire (override) :     -x /absolute/path/to/program.xpl
ECHO.
GOTO :EOF

:MAIN
REM Initialisation
CALL :INIT
REM Utilisation du programme _CalabashCP/bin/CalabashCP.bat pour lancer Calabash (-> complète le _CLASSPATH + crée l'environnement d'exécution de Calabash)
CALL %XPROCCP%
REM Récupération des arguments de la ligne de commande
CALL :CMD_PARSE %CMD_LINE_ARGS%
IF "%HELP%" NEQ "" (
	REM Affichage de l'aide si besoin
	CALL :HELP
) ELSE IF "%XPROC_OVERRIDE%" NEQ "" (
	REM Exécution d'un fichier XProc de lancement existant
	CALL :RUN_OVERRIDE
) ELSE (
	REM Construction du XProc de lancement
	CALL :MAKE_XPROC_TMP
	REM Run
	CALL :RUN
)

:END
PAUSE
EXIT /B
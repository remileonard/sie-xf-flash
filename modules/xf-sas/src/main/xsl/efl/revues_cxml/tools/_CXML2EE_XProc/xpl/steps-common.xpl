<?xml version="1.0" encoding="UTF-8"?>
<p:library  xmlns:p="http://www.w3.org/ns/xproc"
            xmlns:c="http://www.w3.org/ns/xproc-step"
            xmlns:cx="http://xmlcalabash.com/ns/extensions"
            xmlns:err="http://www.w3.org/ns/xproc-error"
            xmlns:xs="http://www.w3.org/2001/XMLSchema"
            xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
            version="1.0">
  
  
  <!-- Valide selon un schéma NVDL et retourne dans un port report les erreurs éventuelles de validation -->
  <!-- Cf. http://xproc.org/library/ -->
  <p:declare-step type="xf:nvdl-report" name="current">
    <p:input port="source" primary="true"/>
    <p:input port="nvdl"/>
    <p:input port="schemas" sequence="true"/>
    <p:output port="result" primary="true"/>
    <p:output port="report" sequence="true">
      <p:pipe step="try" port="report"/>
    </p:output>
    <p:option name="assert-valid" select="'false'"/>
    
    <p:try name="try">
      
      <p:group>
        <p:output port="result" primary="true">
          <p:pipe step="v-nvdl" port="result"/>
        </p:output>
        <p:output port="report">
          <p:empty/>
        </p:output>
        <cx:nvdl name="v-nvdl" assert-valid="true">
          <p:input port="source">
            <p:pipe step="current" port="source"/>
          </p:input>
          <p:input port="nvdl">
            <p:pipe step="current" port="nvdl"/>
          </p:input>
          <p:input port="schemas">
            <p:pipe step="current" port="schemas"/>
          </p:input>
        </cx:nvdl>
      </p:group>
      
      <p:catch name="catch">
        <p:output port="result" primary="true">
          <p:pipe step="copy-source" port="result"/>
        </p:output>
        <p:output port="report">
          <p:pipe step="copy-errors" port="result"/>
        </p:output>
        <p:identity name="copy-source">
          <p:input port="source">
            <p:pipe step="current" port="source"/>
          </p:input>
        </p:identity>
        <p:identity name="copy-errors">
          <p:input port="source">
            <p:pipe step="catch" port="error"/>
          </p:input>
        </p:identity>
      </p:catch>
      
    </p:try>
    
    <p:count name="count">
      <p:input port="source">
        <p:pipe step="try" port="report"/>
      </p:input>
    </p:count>
    
    <p:choose>
      
      <!-- Effectue deux fois la validation dans ce cas -->
      <p:when test="$assert-valid = 'true' and /c:result != '0'">
        <cx:nvdl name="v-nvdl" assert-valid="true">
          <p:input port="source">
            <p:pipe step="current" port="source"/>
          </p:input>
          <p:input port="nvdl">
            <p:pipe step="current" port="nvdl"/>
          </p:input>
          <p:input port="schemas">
            <p:pipe step="current" port="schemas"/>
          </p:input>
        </cx:nvdl>
      </p:when>
      
      <p:otherwise>
        <p:identity>
          <p:input port="source">
            <p:pipe step="try" port="result"/>
          </p:input>
        </p:identity>
      </p:otherwise>
      
    </p:choose>
    
  </p:declare-step>
  
</p:library>
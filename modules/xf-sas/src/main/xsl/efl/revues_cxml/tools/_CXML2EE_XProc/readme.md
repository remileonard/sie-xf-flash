Programme de conversion CXML vers EE SAS
Backup des sources
Voir JIRA [FLAS-6] [SAS/PUBLICATION] : revues CXML : convertir en masse les doc/ui chaîne XML en EE Flash

Pour exécuter le programme -> copie locale + remettre les fichiers qui n'ont pas été ajoutés au dépôt, cf. ci-dessous :

-> ./_CalabashCP/lib est vide
	Trop volumineux pour être intégré au dépôt
	Voir CXML2EE_XProc.bat pour la liste des JAR qui doivent être ajoutés manuellement
		- Saxon EE + licence
		- Calabash + librairies
		- cp-protocol
		- jing version ISO Schematron
	On peut les copier depuis la dernière release du programme
		"\\nas003\EFLPROD\DSI\1 - PROJETS\XML First\acourt\CXML2EE_XProc_v0.1.zip"

-> Modifier CXML2EE_XProc.bat pour pointer vers la target JAR du projet SAS ou créer un répertoire ./target
	Par défaut, le programme attend un répertoire ./target qui contient xf-sas-1.0-SNAPSHOT-jar-with-dependencies-with-catalog.jar
	On peut modifier la valeur de TARGET et SASCONV pour pointer ailleurs (-> dépôt maven utilisateur par exemple)

Voir JIRA pour mode d'emploi
REM Lancement du script gaulois-pipe pour la conversion CXML
REM Test� avec Java JRE v1.7.0_72
REM Fichiers jar utilis�s :
REM 1/ Gaulois pipe :
REM gaulois-pipe-dist-1.01.05-SNAPSHOT-jar-with-dependencies.jar
REM T�l�charg� depuis https://github.com/cmarchand/gaulois-pipe
REM Projet MVN build�
REM 2/ SAS
REM projet Git "sie-xf-sas-conversion" build� (probl�me possible du version du build)   

REM R�pertoire d'ex�cution de la JRE
SET JRE_HOME="C:\Program Files (x86)\Java\jre7\bin"
REM dossier courant
SET CURRENT_DIR=%~dp0
REM D�finitino du classpath pour java
SET CLASSPATH=%CURRENT_DIR%lib\gaulois-pipe-dist-1.01.05-SNAPSHOT-jar-with-dependencies.jar;%CURRENT_DIR%..\..\..\..\..\..\..\target\xf-sas-1.2.2-SNAPSHOT-jar-with-dependencies-with-catalog.jar

SET INPUT_DIRPATH=C:/ELF_FLA/Flash/Conversion/dump/EDP/Document
SET OUTPUT_DIRPATH=C:/ELF_FLA/Flash/dev/sie-xf-sas-conversion/src/main/xsl/efl/revues_cxml/tools/gauloispipe/result

SET OUTPUT_DIRPATH_1=C:\ELF_FLA\Flash\dev\sie-xf-sas-conversion\src\main\xsl\efl\revues_cxml\tools\gauloispipe\result

REM Suppression des r�sultats pr�c�dents
DEL /Q %OUTPUT_DIRPATH_1%\*.*

REM Lancement de GauloisPipe
REM cxml_revues_config.xml : fichier GauloisPipe
%JRE_HOME%\java -cp %CLASSPATH% fr.efl.chaine.xslt.GauloisPipe -iName SAS --config cxml_revues_config.xml PARAMS inputDirPath=%INPUT_DIRPATH% outputDirPath=%OUTPUT_DIRPATH% 1>log\log_gp_cxml_revues.txt 2>log\log_xsl_cxml_revues.txt

pause

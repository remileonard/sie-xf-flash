REM @ECHO OFF
TITLE XFE launcher

REM SET CWD=%~dp0
SET CWD=C:\ELF_FLA\Flash\dev\sie-xf-sas-conversion

SET INPUT_DIR=..\..\..\..\..\..\..\..\Conversion\dump\EDP\Document
SET INPUT_DIR_URI=C:/ELF_FLA/Flash/Conversion/dump/EDP/Document
SET OUTPUT_DIR=C:/ELF_FLA/Flash/Conversion/ValidationConversion/result

SET jarWithDependenciesName=xf-sas-1.0-SNAPSHOT-jar-with-dependencies-with-catalog.jar
SET jarWithDependenciesUri=%CWD%\target\%jarWithDependenciesName%

SET _CLASSPATH=%CWD%\lib\saxon9he.jar;%CWD%\lib\cp-protocol-1.2.jar;%jarWithDependenciesUri%

SET SAXONOPT=-versionmsg:off -expand:off -r:org.xmlresolver.Resolver

SET JAVAOPT= -Xms512m -Xmx1024m -Dlog4j.configuration=log4j.properties -classpath %_CLASSPATH%

REM suppression des anciennes traces
del err.txt
del log.txt

ECHO ON
for /R %1 %%F in (%INPUT_DIR%\*.xml) do java %JAVAOPT% top.marchand.xml.protocols.ProtocolInstaller net.sf.saxon.Transform %SAXONOPT% -u -s:file:/%INPUT_DIR_URI%/%%~nxF -xsl:cp:/xf-sas/efl/revues_cxml/revueChaineXml2editorialEntity.xsl -o:%OUTPUT_DIR%/%%~nxF --suppressXsltNamespaceCheck:on 1>>log.txt 2>>err.txt
@ECHO OFF

pause 
@ECHO OFF

REM ***********************************************************************************
REM * Configure le lancement d'un programme XProc via Calabash + Saxon + protocole CP *
REM ***********************************************************************************

REM Initialisation
FOR %%i IN (%~dp0..\) DO SET _CWD=%%~fi
SET _CWD=%_CWD:\=/%
SET LIB=%_CWD%lib/
SET CONF=%_CWD%conf/

REM Construction du _CLASSPATH
REM SET _CLASSPATH=
REM Ne pas instancier _CLASSPATH à vide : ce BATCH est utilisé dans d'autres programmes pour paramétrer Calabash + Saxon + protocole CP et certaines librairies doivent être déclarées avant (-> notamment la target JAR du programme)

REM Protocole CP
SET _CLASSPATH=%_CLASSPATH%;%LIB%cp-protocol-1.3-SNAPSHOT.jar

REM Saxon
REM Version Saxon HE 9.6
REM SET _CLASSPATH=%_CLASSPATH%;%LIB%Saxon-HE-9.6.0-7.jar
REM Version Saxon EE 9.5
SET _CLASSPATH=%_CLASSPATH%;%LIB%saxon9ee.jar

REM Calabash + libs associées
REM Pour Saxon HE 9.6
REM SET _CLASSPATH=%_CLASSPATH%;%LIB%xmlcalabash-1.1.11-96.jar
REM Pour Saxon EE 9.5
SET _CLASSPATH=%_CLASSPATH%;%LIB%xmlcalabash-1.1.11-95.jar
REM Librairies
SET _CLASSPATH=%_CLASSPATH%;%LIB%ant-1.9.4.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%ant-launcher-1.9.4.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%classindex-3.3.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%commons-codec-1.6.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%commons-fileupload-1.3.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%commons-io-2.2.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%commons-logging-1.1.1.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%hamcrest-core-1.3.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%htmlparser-1.4.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%httpclient-4.2.5.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%httpcore-4.2.5.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%icu4j-49.1.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%isorelax-20090621.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%javax.servlet-api-3.1.0.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%jcl-over-slf4j-1.7.10.jar
REM Ancien Jing fourni avec Calabash, incompatible ISO Schematron
REM SET _CLASSPATH=%_CLASSPATH%;%LIB%jing-20091111.jar
REM Jing avec ISO Schematron cf. https://github.com/relaxng/jing-trang
SET _CLASSPATH=%_CLASSPATH%;%LIB%jing.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%junit-4.12.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%log4j-api-2.1.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%log4j-core-2.1.jar
REM Ne pas laisser si inclus dans la target principale
REM SET _CLASSPATH=%_CLASSPATH%;%LIB%log4j-slf4j-impl-2.1.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%msv-core-2013.6.1.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%nwalsh-annotations-1.0.0.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%org.restlet-2.2.2.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%org.restlet.ext.fileupload-2.2.2.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%org.restlet.ext.slf4j-2.2.2.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%relaxngDatatype-20020414.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%slf4j-api-1.7.10.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%tagsoup-1.2.1.jar
REM Ne pas laisser si inclus dans la target principale
REM SET _CLASSPATH=%_CLASSPATH%;%LIB%xmlresolver-0.12.4.jar
SET _CLASSPATH=%_CLASSPATH%;%LIB%xsdlib-2013.6.1.jar

REM Fichiers de configuration
REM Options de log log4j (précédence sur les autres configs log)
SET LOG_CONFIG_FILE=file:/%CONF%log4j2.calabash.xml
REM Configuration pour Calabash
SET CALABASH_CONFIG_FILE=%CONF%xproc-config
REM Configuration pour Saxon
SET SAXON_CONFIG_FILE=%CONF%saxon-config

REM Options
REM Options Java
SET JAVA_OPT=-Xms512m -Xmx1024m -Dlog4j.configurationFile=%LOG_CONFIG_FILE% -Dfile.encoding=UTF-8 -Dcom.xmlcalabash.phonehome="false" -classpath "%_CLASSPATH%"
REM Options Calabash
SET CALABASH_OPT=--config %CALABASH_CONFIG_FILE% --saxon-configuration=%SAXON_CONFIG_FILE%

REM Commande à exécuter pour lancer Calabash
SET RUN_CALABASH=java %JAVA_OPT% top.marchand.xml.protocols.ProtocolInstaller com.xmlcalabash.drivers.Main %CALABASH_OPT%
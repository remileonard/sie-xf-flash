<?xml version="1.0" encoding="UTF-8"?>
<p:declare-step xmlns:p="http://www.w3.org/ns/xproc"
                xmlns:c="http://www.w3.org/ns/xproc-step"
                xmlns:cx="http://xmlcalabash.com/ns/extensions"
                xmlns:err="http://www.w3.org/ns/xproc-error"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                version="1.0"
                name="current"
                type="xf:CXML2EE">
  
  
  <!--+====================+
      | INPUT / OUTPUT     |
      +====================+-->
  <p:input port="source" primary="true" sequence="true"/> <!-- Un ou plusieurs documents en entrée -->
  <p:output port="result" primary="true" sequence="true"><p:empty/></p:output>
  <p:input port="parameters_port" kind="parameter" primary="true"/>
  

  <!--+====================+
      | IMPORTS            |
      +====================+-->
  <p:import href="library-1.0.xpl"/>
  <p:import href="steps-common.xpl"/>
  <p:import href="steps-CXML2EE.xpl"/>
  

  <!--+====================+
      | STEPS              |
      +====================+-->
  <p:group name="filterLoopInput">

    <!-- Récupération des éventuels paramètres depuis le port parameter -->
    <p:parameters name="params">
      <p:input port="parameters"><p:pipe port="parameters_port" step="current"/></p:input>
    </p:parameters>
    
    <p:group>
      
      <!-- Initialisation de variables utiles -->
      <!-- Répertoire de sortie (chemin absolu requis) -->
      <p:variable name="output_dir" select="//c:param[@name = 'output_dir']/@value"><p:pipe port="result" step="params"/></p:variable>
      <!-- Normalisation a minima du répertoire de sortie -> protocole file: + / Unix -->
      <p:variable name="output_dir_uri" select="concat('file:/',replace(replace(replace($output_dir,'^(file:/+)?(.+)$','$2'),'\\','/'),'/$','/'))"/>
      <!-- Filtre XPath pour sélectionner les documents (par défaut : /*) -->
      <p:variable name="filter" select="(//c:param[@name = 'filter']/@value,'/*')[1]"><p:pipe port="result" step="params"/></p:variable>
      <!-- Paramètre $resolveDoc de la transformation XSLT -->
      <p:variable name="resolveDoc" select="(//c:param[@name = 'resolveDoc']/@value cast as xs:boolean?,false())[1]"><p:pipe port="result" step="params"/></p:variable>
      <!-- Exécute ou non la validation des documents en entrée / sortie -->
      <p:variable name="validate_input" select="(//c:param[@name = 'validate_input']/@value cast as xs:boolean?,false())[1]"><p:pipe port="result" step="params"/></p:variable>
      <p:variable name="validate_output" select="(//c:param[@name = 'validate_output']/@value cast as xs:boolean?,false())[1]"><p:pipe port="result" step="params"/></p:variable>
      <!-- Log de validation -->
      <p:variable name="validation_log" select="concat($output_dir_uri,'log/nvdl-validation-report.xml')"/>
      <!-- Exécute ou non le traitement qui simule la couche d'import -->
      <p:variable name="simule_import" select="(//c:param[@name = 'simule_import']/@value cast as xs:boolean?,false())[1]"><p:pipe port="result" step="params"/></p:variable>
      <!-- Répertoire de sortie du traitement de la couche d'import -->
      <p:variable name="simule_import_out_dir" select="//c:param[@name = 'simule_import_out_dir']/@value"><p:pipe port="result" step="params"/></p:variable>
      
      <!-- Sélection des fichiers sources en fonction de $filter -->
      <p:identity><p:input port="source"><p:pipe port="source" step="current"/></p:input></p:identity>      
      <p:split-sequence name="filterXPath">
        <p:with-option name="test" select="$filter"><p:empty/></p:with-option> <!-- L'input est une séquence -> p:with-option doit être pipé sur un document unique -->
      </p:split-sequence>
      
      <p:group name="filter-matched">
        
        <!-- Documents matchés par le filtre -->
        <p:identity><p:input port="source"><p:pipe port="matched" step="filterXPath"/></p:input></p:identity>
        
        <!-- Parcours des documents retournés -->
        <p:for-each name="loopFiles">
          <p:output port="report" sequence="true" primary="false"><p:pipe port="result" step="SASVal"/></p:output>
          <!-- Informations sur le document -->
          <p:variable name="input_uri" select="base-uri()"/>
          <p:variable name="input_name" select="string-join(tokenize(tokenize($input_uri,'/')[last()],'\.')[position() != last()],'.')"/>
          <p:variable name="output_uri" select="concat($output_dir_uri,tokenize(base-uri(),'/')[position()=last()])"/>
          <!-- Message utilisateur - Début du traitement -->
          <cx:message>
            <p:with-option name="message" select="concat('DEBUT DU TRAITEMENT - ',$input_uri)"/>
          </cx:message>
          <!-- Validation en entrée ? TO DO ? -->
          <!-- Transformation XSLT SAS -->
          <xf:SASTransform name="SASTrans">
            <p:with-option name="output_uri" select="$output_uri"/>
            <p:with-option name="resolveDoc" select="$resolveDoc"/>
          </xf:SASTransform>
          <!-- Validation en sortie -->
          <p:group name="SASVal">
            <p:output port="result" primary="true" sequence="true"/>
            <p:choose>
              <p:when test="$validate_output = true()">
                <xf:SASValidate name="SASV"> <!-- Renvoie sur l'output port result le résultat de la validation -->
                  <p:with-param name="uri" select="$output_uri"/>
                </xf:SASValidate>
              </p:when>
              <p:otherwise>
                <p:identity><p:input port="source"><p:empty/></p:input></p:identity>
              </p:otherwise>
            </p:choose>
          </p:group>
          <!-- Traitement de simulation de la couche d'import (split, unicité des ID, etc.) si paramètre $simule_import -->
          <p:group name="coucheImport">
            <p:identity><p:input port="source"><p:pipe port="result" step="SASTrans"/></p:input></p:identity>
            <p:choose>
              <p:when test="$simule_import = true()">
                <!-- Base à concaténer aux ID générés (on ne conserve pas un mapping des ID générés + on ne peut garantir leur unicité qu'à l'intérieur d'un seul document) -->
                <p:variable name="baseId" select="concat($input_name,'-')"/>
                <!-- Répertoire de sortie -->
                <p:variable name="hrefBase" select="($simule_import_out_dir,concat($output_uri,'_serialized/'))[1]"/>
                <xf:simuleCoucheImport>
                  <p:with-option name="hrefBase" select="$hrefBase"/>
                  <p:with-option name="baseId" select="$baseId"/>
                </xf:simuleCoucheImport>
              </p:when>
              <p:otherwise>
                <p:sink/>
              </p:otherwise>
            </p:choose>
          </p:group>
          <!-- Message utilisateur - Traitement terminé -->
          <p:identity><p:input port="source"><p:empty/></p:input></p:identity>
          <cx:message>
            <p:with-option name="message" select="concat('FIN DU TRAITEMENT - ',$input_uri)"/>
          </cx:message>
          <p:sink/>
        </p:for-each>
        
        <!-- Log du résultat de la validation -->
        <xf:writeLog>
          <p:input port="source"><p:pipe port="report" step="loopFiles"/></p:input>
          <p:with-option name="href" select="$validation_log"/>
        </xf:writeLog>
        
      </p:group>
      
      <p:group name="filter-not-matched">
        <!-- Inutile mais pour que ce soit plus explicite -> tous les documents rejetés par le filtre -->
        <p:identity><p:input port="source"><p:pipe port="not-matched" step="filterXPath"/></p:input></p:identity>
        <p:sink/>
      </p:group>
      
    </p:group>

  </p:group>
  
</p:declare-step>
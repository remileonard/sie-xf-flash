Le fichier "convertDirectoryCXmlToEE_cp.bat" permet de convertir des UIs issus de la CXML vers EE

Pr�-requis :
- saxon 9HE (voir http://saxon.sourceforge.net/#F9.7HE),
- cp-protocol-1.2.jar (http://central02.maven.org/maven2/top/marchand/xml/cp-protocol/1.2/),
- le jar xf-sas-1.0-SNAPSHOT-jar-with-dependencies-with-catalog.jar du projet Maven 


Variables d'environnement :
- CWD : chemin du projet "sie-xf-sas-conversion" checkout� depuis git,
- INPUT_DIR : chemin relatif pour acc�der au dossier qui stocke les documents de la CXML,
- INPUT_DIR_URI : chemin absolue qui est utilis� pour g�n�r� l'URI source du fichier XML � convertir,
- OUTPUT_DIR : dossier de stockage des r�sultats.




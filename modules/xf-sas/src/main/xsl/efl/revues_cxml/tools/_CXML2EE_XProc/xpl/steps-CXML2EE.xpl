<?xml version="1.0" encoding="UTF-8"?>
<p:library  xmlns:p="http://www.w3.org/ns/xproc"
            xmlns:c="http://www.w3.org/ns/xproc-step"
            xmlns:cx="http://xmlcalabash.com/ns/extensions"
            xmlns:err="http://www.w3.org/ns/xproc-error"
            xmlns:xs="http://www.w3.org/2001/XMLSchema"
            xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
            version="1.0">
  
  
  <!-- Exécute la transformation XSLT SAS, en faisant appel à cp:/xf-sas/efl/revues_cxml/revueChaineXml2editorialEntity.xsl
       Résultat de la transformation -> sérialisé en XML + renvoyé en output port -->
  <p:declare-step type="xf:SASTransform" name="current">
    <p:input port="source" primary="true"/>
    <p:output port="result" primary="true"><p:pipe port="result" step="revueChaineXml2editorialEntity"/></p:output>
    <!-- Initialisation -->
    <p:option name="output_uri" select="concat(base-uri(),'.out')"/>
    <p:option name="resolveDoc" select="false()"/>
    <cx:message>
      <p:with-option name="message" select="'Transformation SAS du document.'"/>
    </cx:message>
    <!-- Exécution de la transformation -->
    <p:xslt name="revueChaineXml2editorialEntity" xml:base="cp:/xf-sas/efl/revues_cxml/">
      <p:input port="stylesheet"><p:document href="revueChaineXml2editorialEntity.xsl"/></p:input>
      <p:with-param name="resolveDoc" select="$resolveDoc"/>
    </p:xslt>
    <!-- Sérialisation XML -->
    <p:store omit-xml-declaration="false">
      <p:with-option name="href" select="$output_uri"/>
    </p:store>
  </p:declare-step>
  
  
  <!-- Validation d'une EE au format SAS
       Prend un XML SAS en input port et restitue les éventuelles erreurs de validation -->
  <p:declare-step type="xf:SASValidate" name="current">
    <p:input port="source" primary="true"/>
    <p:output port="result" primary="true" sequence="true"/>
    <p:input port="parameters" kind="parameter"/>
    <cx:message>
      <p:with-option name="message" select="'Validation NVDL.'"/>
    </cx:message>
    <p:sink/> <!-- On doit d'abord transformer le NVDL -> et donc abandonner la source XML SAS pour le moment -->
    <p:group name="NVDLValidation" xml:base="cp:/xf-models/editorialEntity_sas/">
      <p:output port="result" primary="true" sequence="true"/>
      <!-- Simplification du schéma NVDL en XSLT -> on ne garde que ce qui concerne la CXML -->
      <p:xslt name="NVDLSimplification">
        <p:input port="source"><p:document href="editorialEntity.nvdl"/></p:input>
        <p:input port="parameters"><p:empty/></p:input>
        <p:input port="stylesheet">
          <p:inline>
            <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                            xmlns="http://purl.oclc.org/dsdl/nvdl/ns/structure/1.0"
                            xpath-default-namespace="http://purl.oclc.org/dsdl/nvdl/ns/structure/1.0"
                            version="2.0">
              <xsl:variable name="NS.OK" as="xs:string+"
                            select="('http://www.lefebvre-sarrut.eu/ns/xmlfirst','http://www.w3.org/1999/xhtml',
                                     'http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire','http://www.lefebvre-sarrut.eu/ns/efl/infoChiffres')"/>
              <xsl:template match="namespace[not(@ns = $NS.OK)]"/>
              <xsl:template match="*|@*|processing-instruction()">
                <xsl:copy>
                  <xsl:apply-templates select="node()|@*"/>
                </xsl:copy>
              </xsl:template>
            </xsl:stylesheet>
          </p:inline>
        </p:input>
      </p:xslt>
      <!-- Validation NVDL -->
      <xf:nvdl-report name="nvdl" assert-valid="false">
        <p:input port="source"><p:pipe port="source" step="current"/></p:input>
        <p:input port="nvdl"><p:pipe port="result" step="NVDLSimplification"/></p:input>
        <p:input port="schemas"><p:empty/></p:input>
      </xf:nvdl-report>
      <p:sink/> <!-- On ne conserve pas l'output port du step xf:nvdl-report, qui contient une copie du document en input -->
      <p:count name="count"><p:input port="source"><p:pipe port="report" step="nvdl"/></p:input></p:count>
      <p:group name="getErrorMessage">
        <p:output port="result" primary="true" sequence="true"/>
        <p:choose>
          <p:when test="/c:result != '0'">
            <!-- Modification du log d'erreur -->
            <p:xslt>
              <p:input port="source"><p:pipe port="report" step="nvdl"/></p:input>
              <p:input port="parameters"><p:pipe port="parameters" step="current"/></p:input>
              <p:input port="stylesheet">
                <p:inline>
                  <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                                  xmlns:c="http://www.w3.org/ns/xproc-step"
                                  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                                  xpath-default-namespace="http://www.w3.org/ns/xproc-step"
                                  version="2.0"
                                  exclude-result-prefixes="#all">
                    <xsl:param name="uri" required="yes"/>
                    <xsl:template match="/c:errors">
                      <xsl:apply-templates/>
                    </xsl:template>
                    <xsl:template match="c:error[@code = 'err:XC0053']">
                      <xf:error code="{@code}" type="Erreur de validation du document" href="{$uri}"><xsl:value-of select="replace(tokenize($uri,'/')[position()=last()],'\.xml','')"/></xf:error>
                    </xsl:template>
                    <xsl:template match="*|@*">
                      <xsl:copy>
                        <xsl:apply-templates select="node()|@*"/>
                      </xsl:copy>
                    </xsl:template>
                  </xsl:stylesheet>
                </p:inline>
              </p:input>
            </p:xslt>
          </p:when>
          <p:otherwise>
            <p:identity><p:input port="source"><p:pipe port="report" step="nvdl"/></p:input></p:identity>
          </p:otherwise>
        </p:choose>
      </p:group>
    </p:group>
  </p:declare-step>
  
  
  <!-- Traitement de simulation de la couche d'import (split, unicité des ID, etc.) -->
  <p:declare-step type="xf:simuleCoucheImport" name="current">
    <p:input port="source" primary="true"/>
    <p:option name="hrefBase" required="true"/>
    <p:option name="baseId" required="false" select="''"/>
    <p:variable name="root" select="/"/>
    <cx:message>
      <p:with-option name="message" select="'Decoupage du document SAS en EE independantes (simulation couche import).'"/>
    </cx:message>
    <p:for-each name="loopFiles">
      <p:iteration-source select="//xf:editorialEntity"/>
      <!-- Pour écriture des logs de validation éventuels, cf. debug ci-dessous -->
      <!--<p:output port="report" sequence="true" primary="false"><p:pipe port="result" step="SASV"/></p:output>-->
      <!-- Ajout de "DOC_" au nom du fichier s'il s'agit d'un document (-> question de praticité) -->
      <p:variable name="docPrefix" select="if (p:iteration-position() = 1) then 'DOC_' else ''"/>
      <!-- On identifie en 1er lieu l'EE à extraire dans la XSLT -->
      <p:variable name="id" select="/xf:editorialEntity/@xf:id"/>
      <!-- Son id transformé, qui sert à nommer le fichier XML sérialisé + correspond à des idref dans les XML -->
      <p:variable name="calculatedId" select="concat($docPrefix,$baseId,saxon:generate-id($root//xf:editorialEntity[@xf:id = $id]))"/>
      <!-- Chemin de sauvegarde du fichier -->
      <p:variable name="docUri" select="concat($hrefBase,$calculatedId,'.xml')"/>
      <p:xslt name="coucheImport" xml:base="cp:/xf-sas/common/">
        <p:input port="stylesheet"><p:document href="simuleCoucheImport.xsl"/></p:input>
        <p:input port="source"><p:pipe port="source" step="current"/></p:input>
        <p:with-param name="EEID_origin" select="$id"/>
        <p:with-param name="EEID_calculated" select="$calculatedId"/>
        <p:with-param name="baseId" select="$baseId"/>
        <p:with-param name="XProcPipeline" select="true()"/>
      </p:xslt>
      <p:store omit-xml-declaration="false">
        <p:with-option name="href" select="$docUri"/>
      </p:store>
      <!-- Pour validation des fichiers générés - pour test de la XSL simuleCoucheImport.xsl uniquement
           Si SAS valide -> normalement les EE le sont aussi, la XSL ne doit pas créer de fichier invalide -->
      <!--<xf:SASValidate name="SASV">
        <p:input port="source"><p:pipe port="result" step="coucheImport"/></p:input>
        <p:with-param name="uri" select="$docUri"/>
      </xf:SASValidate>-->
    </p:for-each>
    <!-- Cf. validation ci-dessus -->
    <!--<xf:writeLog>
      <p:input port="source"><p:pipe port="report" step="loopFiles"/></p:input>
      <p:with-option name="href" select="concat($hrefBase,'log/nvdl-validation-report.xml')"/>
    </xf:writeLog>-->
  </p:declare-step>
  
  
  <!-- Ecriture de l'input source dans un fichier de log $href SSI l'input contient quelquechose -->
  <p:declare-step type="xf:writeLog" name="current">
    <p:input port="source" primary="true" sequence="true"/>
    <p:option name="href" required="true"/>
    <p:count name="count"/>
    <p:group name="writeLog">
      <p:choose>
        <p:when test="/c:result != '0'">
          <p:wrap-sequence name="wrapLog" wrapper="log" wrapper-prefix="xf" wrapper-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst">
            <p:input port="source"><p:pipe port="source" step="current"/></p:input>
          </p:wrap-sequence>
          <p:store>
            <p:with-option name="href" select="$href"/>
          </p:store>
        </p:when>
        <p:otherwise>
          <p:sink/>
        </p:otherwise>
      </p:choose>
    </p:group>
  </p:declare-step>


</p:library>
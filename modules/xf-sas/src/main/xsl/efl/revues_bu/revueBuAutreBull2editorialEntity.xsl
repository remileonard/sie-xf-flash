<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl"
    xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
    xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
    xmlns:eflAutreBull="http://www.lefebvre-sarrut.eu/ns/efl/autrebull"
    xmlns:normalization-util="normalization-util"
    xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
    xpath-default-namespace=""
    exclude-result-prefixes="#all"
    version="2.0">

  <xsl:import href="../../xf-sas-common.xsl"/>
  <xsl:import href="../common/normalization.xsl"/>

  <xsl:output method="xml" indent="yes"/>

  <xsl:param name="xfSAS:company" select="'efl'" as="xs:string"/>

  <xsl:variable name="historicId.prefix" select="'EFL_'" as="xs:string"/>
  <xsl:variable name="contentNode.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/efl/revues/autrebull'"/>

  <!--==============================-->
  <!--MAIN-->
  <!--==============================-->

  <xsl:template match="/">
    <xsl:call-template name="xfSAS:makeXmlModelPi"/>
    <xsl:variable name="step1-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates/>
      </xsl:document>
    </xsl:variable>
    <!--<xsl:apply-templates select="$step1-xml2ee" mode="makeRelations"/>-->
    <xsl:copy-of select="$step1-xml2ee"/>
  </xsl:template>


  <xsl:template match="*[eflAutreBull:isEETree(.)]">
    
    <xsl:variable name="support" as="xs:string" select="local-name()"/>
    <xsl:variable name="num"  as="xs:string?" select="(@nosn|@noindic|@nobrda|@no)"/>
    <xsl:variable name="date-split"  as="xs:string*" select="normalization-util:split-date($num)"/>
    <xsl:variable name="year"  as="xs:string?" select="@an"/>
    <xsl:variable name="month"  as="xs:string?" select="@mois"/>
    <xsl:variable name="date" as="xs:string*"
      select="(normalization-util:normalize-year(($year,$date-split[2])[1]), normalization-util:normalize-month(($month,$date-split[1])[1]),'01')"/>
    
    <xsl:variable name="local-name" select="local-name()"/>
    
    <editorialEntity 
      xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
      xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
      xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
      xf:id=""
      code="EFL_TEE_{$support}">
      <xsl:namespace name="xf">http://www.lefebvre-sarrut.eu/ns/xmlfirst</xsl:namespace>
      <metadata>
        <!--CALCULATED METADATA-->
        <meta code="systemTitle" as="element(html:div)">
          <value as="element(html:div)">
            <div xmlns="http://www.w3.org/1999/xhtml">
              <xsl:value-of select="(upper-case($support),string-join(($date[1],$date[2]),'/'))" separator=" "/>
            </div>
          </value>
        </meta>
        <meta code="systemAbstract" as="element(html:div)">
          <value as="element(html:div)">
            <div xmlns="http://www.w3.org/1999/xhtml"/>
          </value>
        </meta>
        <!--GENERATED METADATA-->
        <meta code="EFL_META_dateParution" as="xs:dateTime">
          <value as="xs:dateTime">
            <xsl:value-of select="string-join($date,'-'),'00:00:00Z'" separator="T"/>
          </value>
        </meta>
        <meta code="EFL_META_matiere">
          <value/>
        </meta>
        <meta code="EFL_META_metier">
          <value/>
        </meta>
        <meta code="EFL_META_niveauExpertise">
          <value>generaliste</value>
        </meta>
        <meta code="EFL_META_anneePublication">
          <value>
            <xsl:value-of select="$date[1]"/>
          </value>
        </meta>
        <meta code="EFL_META_numeroOrdre">
          <value>
            <xsl:value-of select="$date[2]"/>
          </value>
        </meta>
        <meta code="EFL_META_regleNumerotation">
          <value>numerotationParMoi</value>
        </meta>
        <meta code="EFL_META_numeroPremiereInfo">
          <value>1</value>
        </meta>
        <meta code="EFL_META_redacteurChef">
          <value>Jean-Yves Le Borgne</value>
        </meta>
      </metadata>
      <body>
        <xsl:for-each-group select="*[not(eflAutreBull:isStructuralNodeTitle(.))]" group-adjacent="not(eflAutreBull:isStructuralNode(.)) and not(eflAutreBull:isEEText(.))">
          <xsl:choose>
            <xsl:when test="current-grouping-key()">
              <contentNode>
                <content>
                  <xsl:element name="{$local-name}" namespace="{$contentNode.namespace.uri}">
                    <xsl:attribute name="dummyRoot" select="'true'"/>
                    <xsl:apply-templates select="current-group()" mode="convertNamespace"/>
                  </xsl:element>
                </content>
              </contentNode>
            </xsl:when>
            <xsl:otherwise>
              <xsl:apply-templates select="current-group()">
                <xsl:with-param name="support" select="$support" tunnel="yes"/>
                <xsl:with-param name="date" select="$date" tunnel="yes"/>
              </xsl:apply-templates>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each-group>
      </body>
    </editorialEntity>
  </xsl:template>


  <xsl:template match="*[eflAutreBull:isEEText(.)]">
    <xsl:param name="support" tunnel="yes"/>
    <xsl:param name="date" tunnel="yes"/>
    <referenceNode code="EFL_TR_accrochage_Revues_{local-name(.)}" num="{(self::p/no,self::com/tcom/intr,'')[1]}">
      <ref xf:targetResId="{xfSAS:makeHistoricIdAttribute(., @id)}" xf:targetResType="editorialEntity" xf:refType="xf:referenceNode">
        <editorialEntity 
      code="EFL_TEE_{local-name(.)}" xf:id="{xfSAS:makeHistoricIdAttribute(., @id)}">
          <metadata>
            <!--CALCULATED METADATA-->
            <meta code="systemTitle" as="element(html:div)">
              <value as="element(html:div)">
                <div xmlns="http://www.w3.org/1999/xhtml">
                  <xsl:choose>
                    <xsl:when test="local-name()='p'">
                      <xsl:value-of select="'Info n°',no" separator=" "/>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:apply-templates select="*[eflAutreBull:isEETextTitle(.)]/node()" mode="xml2html"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </div>
              </value>
            </meta>
            <meta code="systemAbstract" as="element(html:div)">
              <value as="element(html:div)">
                <div xmlns="http://www.w3.org/1999/xhtml"> </div>
              </value>
            </meta>
            <!--GENERATED METADATA-->
            <meta code="EFL_META_dateValidite" as="xs:dateTime">
              <value as="xs:dateTime">
                <xsl:value-of select="string-join($date,'-'),'00:00:00Z'" separator="T"/>
              </value>
            </meta>
            <meta code="EFL_META_matiere">
              <value> </value>
            </meta>
            <meta code="EFL_META_metier">
              <value> </value>
            </meta>
            <meta code="EFL_META_niveauExpertise">
              <value>expert</value>
            </meta>
            <meta code="EFL_META_metaSource">
              <value> </value>
            </meta>
            <meta code="EFL_META_version">
              <value>developpee</value>
            </meta>
          </metadata>
          <body>
            <contentNode>
              <content>
                <xsl:apply-templates select="." mode="convertNamespace"/>
              </content>
            </contentNode>
          </body>
        </editorialEntity>
      </ref>
    </referenceNode>
  </xsl:template>

  <!--==================================================-->
  <!-- NUM -->
  <!--==================================================-->

  <xsl:template match="p/no[empty(ancestor::*[eflAutreBull:isEEText(.)])]|com/tcom/intr" mode="xml2html"/>

  <!--==================================================-->
  <!--STRUCTURAL NODE-->
  <!--==================================================-->

  <!--Résumé de l'actualité-->

  <xsl:template match="*[eflAutreBull:isStructuralNode(.)]">
    <xsl:param name="support" tunnel="yes"/>
    <xsl:param name="date" tunnel="yes"/>
    
    <xsl:variable name="local-name" select="local-name()"/>
    <xsl:variable name="id" select="(@id,eflAutreBull:makeLogicId(.,$support,$date))[1]"></xsl:variable>
    <structuralNode type="{name()}" xf:id="{xfSAS:makeHistoricIdAttribute(., $id)}">
      <metadata/>
      <xsl:apply-templates select="*[eflAutreBull:isStructuralNodeTitle(.)]"/>
      <xsl:for-each-group select="*[not(eflAutreBull:isStructuralNodeTitle(.))]" group-adjacent="not(eflAutreBull:isStructuralNode(.)) and not(eflAutreBull:isEEText(.))">
        <xsl:choose>
          <xsl:when test="current-grouping-key()">
            <contentNode>
              <content>
                <xsl:element name="{$local-name}" namespace="{$contentNode.namespace.uri}">
                  <xsl:attribute name="dummyRoot" select="'true'"/>
                  <xsl:apply-templates select="current-group()" mode="convertNamespace"/>
               </xsl:element>
              </content>
            </contentNode>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="current-group()"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each-group>
    </structuralNode>
  </xsl:template>

  <!--Titres des structuralNodes-->
  <xsl:template match="*[eflAutreBull:isStructuralNode(.)]/*[eflAutreBull:isStructuralNodeTitle(.)]">
    <title>
      <div xmlns="http://www.w3.org/1999/xhtml">
        <xsl:apply-templates mode="xml2html"/>
      </div>
    </title>
  </xsl:template>

  <!--==================================================-->
  <!--MODE "xml2html"-->
  <!--==================================================-->

  <xsl:template match="motrep" mode="xml2html">
    <strong class="motRep" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </strong>
  </xsl:template>

  <xsl:template match="e" mode="xml2html">
    <sup class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </sup>
  </xsl:template>

  <xsl:template match="i" mode="xml2html">
    <sub class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </sub>
  </xsl:template>

  <xsl:template match="ital" mode="xml2html">
    <em class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </em>
  </xsl:template>

  <xsl:template match="coupe" mode="xml2html">
    <span class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>

  <xsl:template match="t" mode="xml2html">
    <span class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>

  <xsl:template match="page" mode="xml2html"/>
  
  <xd:doc>
    <xd:desc>liens en mode xml2html</xd:desc>
  </xd:doc>
  <xsl:template mode="xml2html" match="
    rmem | rmemn | rfr | rfrn | rprat | rpratn |
    rbull | rbulln | rnote | note | note/al |
    refdoc | rattach| refdocetroit | refodoc1 | refdoc2 | rtx | link | prediv">
    <xsl:if test="normalize-space(.) != ''">
      <span class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
        <xsl:apply-templates mode="#current"/>
      </span>
    </xsl:if>
  </xsl:template>
  <xsl:template match="nature | ref | rc | nom | date" mode="xml2html">
    <xsl:apply-templates mode="#current"/>
  </xsl:template>

  <xsl:template match="*" mode="xml2html" priority="-1">
    <xsl:message><xsl:value-of select="local-name()"/> non converti en mode xml2html</xsl:message>
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <!--==================================================-->
  <!--MODE convertNamespace -->
  <!--==================================================-->

  <xsl:template match="*" mode="convertNamespace">
    <xsl:variable name="namespace.uri" select="$contentNode.namespace.uri" as="xs:string"/>
    <xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
      <xsl:apply-templates select="node() | @*" mode="#current">
        <xsl:with-param name="namespace.uri" select="$namespace.uri" tunnel="yes"/>
      </xsl:apply-templates>
    </xsl:element>
  </xsl:template>

  <xsl:template match="text() | @*" mode="convertNamespace">
    <xsl:copy/>
  </xsl:template>

  <!-- ID -->

  <xsl:template match="@id" mode="convertNamespace">
    <xsl:copy-of select="."/>
    <xsl:attribute name="xf:id" select="xfSAS:makeHistoricIdAttribute(parent::*, .)"/>
  </xsl:template>

  <!--==================================================-->
  <!--COMMON"-->
  <!--==================================================-->

  <xsl:template match="node() | @*">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>


  <!--==================================================-->
  <!--FUNCTION-->
  <!--==================================================-->

  <xd:doc>
    <xd:desc>sélectionne une editorialEntity candidate</xd:desc>
  </xd:doc>
  <xsl:variable name="eeTreeNodeElementsList" select="(/*/local-name())"/>
  <xsl:function name="eflAutreBull:isEETree" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:sequence select="local-name($e) = $eeTreeNodeElementsList"/>
  </xsl:function>

  <xd:doc>
    <xd:desc>sélectionne une editorialEntity candidate</xd:desc>
  </xd:doc>
  <xsl:variable name="eeTextNodeElementsList" select="('flash','com','p')"/>
  <xsl:function name="eflAutreBull:isEEText" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:sequence select="local-name($e) = $eeTextNodeElementsList"/>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>sélectionne le titre d'une editorialEntity candidate</xd:desc>
  </xd:doc>
  <xsl:variable name="eeTextTitleNodeElementsList" select="('tflash','tcom')"/>
  <xsl:function name="eflAutreBull:isEETextTitle" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:sequence select="local-name($e) = $eeTextTitleNodeElementsList"/>
  </xsl:function>

  <xd:doc>
    <xd:desc>sélectionne un candidat structuralNode</xd:desc>
  </xd:doc>

  <xsl:variable name="structuralNodeElementsList" select="('resessentiel','resact','pres','res','partcom','partinfo','spartinfo','info','indice','partflash','agenda')"/>
  <xsl:function name="eflAutreBull:isStructuralNode" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:sequence select="local-name($e) = $structuralNodeElementsList"/>
  </xsl:function>

  <xd:doc>
    <xd:desc>sélectionne le titre d'un candidat structuralNode</xd:desc>
  </xd:doc>
  <xsl:variable name="structuralNodeTitleElementsList" select="('tresact','tpres','tres','t1','tinfo','tind','tpart','tagenda')"/>
  <xsl:function name="eflAutreBull:isStructuralNodeTitle" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:sequence select="local-name($e) = $structuralNodeTitleElementsList"/>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>construite Un id logique (xpath) pour les structuralNode</xd:desc>
  </xd:doc>
  <xsl:function name="eflAutreBull:makeLogicId" as="xs:string">
    <xsl:param name="e" as="element()"/>
    <xsl:param name="support" as="xs:string"/>
    <xsl:param name="date" as="xs:string*"/>
    <xsl:choose>
      <xsl:when test="eflAutreBull:isEETree($e)">
        <xsl:value-of select="($support,$date)" separator="_"></xsl:value-of>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="eflAutreBull:makeLogicId($e/..,$support,$date),$e/local-name(),count($e/preceding-sibling::*[local-name()=$e/local-name()]) + 1" separator="_"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
</xsl:stylesheet>

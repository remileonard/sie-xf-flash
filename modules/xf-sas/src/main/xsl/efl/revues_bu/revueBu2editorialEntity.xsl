<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
	xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl"
	xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
	xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
	xpath-default-namespace=""
	exclude-result-prefixes="#all"
	version="2.0">

	<xsl:import href="revueBu2editorialEntity.makeRelations.xsl"/>
	<xsl:import href="../../xf-sas-common.xsl"/>
	
	<xsl:output method="xml" indent="no"/>
	
	<xsl:param name="xfSAS:company" select="'efl'" as="xs:string"/>
	
	<xsl:variable name="historicId.prefix" select="'EFL_'" as="xs:string"/>
	<xsl:variable name="contentNode.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/efl/revues/fr'"/>
	
	<!--==============================--> 
	<!--MAIN-->
	<!--==============================-->
	
	<xsl:template match="/">
		<xsl:call-template name="xfSAS:makeXmlModelPi"/>
		<xsl:variable name="step1-xml2ee" as="document-node()">
			<xsl:document>
				<xsl:apply-templates/>
			</xsl:document>
		</xsl:variable>
		<xsl:apply-templates select="$step1-xml2ee" mode="makeRelations"/>
	</xsl:template>
	
	<xsl:template match="/FR">
		<editorialEntity 
			xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
			xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
			xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
			xf:id=""
			type="EFL_REVUE_FR_DOC"
			>
			<xsl:namespace name="xf">http://www.lefebvre-sarrut.eu/ns/xmlfirst</xsl:namespace>
			<metadata>
				<!--<xsl:apply-templates mode="metadata"/>-->
			</metadata>
			<body>
				<xsl:apply-templates/>
			</body>
		</editorialEntity>
	</xsl:template>
	
	<!--==================================================-->
	<!-- METADATA -->
	<!--==================================================-->

	
	<!--==================================================-->
	<!--STRUCTURAL NODE-->
	<!--==================================================-->
	
	<!--Résumé de l'actualité-->
	
	<xsl:template match="RESACT | FLASHROM | FLASHIT | RESACT1 | RESACT2 | PARTINFO | PARTINFO2">
		<structuralNode type="{name()}" xf:id="{xfSAS:makeHistoricIdAttribute(., @ID)}">
			<!--todo : meta-->
			<xsl:apply-templates/>
		</structuralNode>
	</xsl:template>

	<!--Titres des structuralNodes-->
	<xsl:template match="TRESACT | TRESACT1 | TRESACT2 | TFLASHROM | TFLASHIT | PARTINFO/T1 | PARTINFO2/T2">
		<title>
			<div xmlns="http://www.w3.org/1999/xhtml">
				<xsl:apply-templates mode="sgml2html"/>
			</div>
		</title>
	</xsl:template>
	
	<!--==================================================-->
	<!--UI-->
	<!--==================================================-->
	
	<xsl:template match="FLASH | INFO">
		<editorialEntity type="EFL_REVUE_UI_{local-name(.)}" xf:id="{xfSAS:makeHistoricIdAttribute(., @ID)}">
			<body>
				<contentNode>
					<content>
						<xsl:apply-templates select="." mode="convertNamespace"/>
					</content>
				</contentNode>
			</body>
		</editorialEntity>
	</xsl:template>
	
	<!--==================================================-->
	<!--MODE "sgml2html"-->
	<!--==================================================-->
	
	<xsl:template match="MOTREP" mode="sgml2html">
		<strong class="motRep">
			<xsl:apply-templates mode="#current"/>
		</strong>
	</xsl:template>
	
	<xsl:template match="E" mode="sgml2html">
		<sup>
			<xsl:apply-templates mode="#current"/>
		</sup>
	</xsl:template>
	
	<xsl:template match="I" mode="sgml2html">
		<em>
			<xsl:apply-templates mode="#current"/>
		</em>
	</xsl:template>
	
	<xsl:template match="*" mode="sgml2html" priority="-1">
		<xsl:message><xsl:value-of select="local-name()"/> non converti en mode sgml2html</xsl:message>
		<xsl:copy>
			<xsl:copy-of select="@*"/>
			<xsl:apply-templates mode="#current"/>
		</xsl:copy>
	</xsl:template>

	<!--==================================================-->
	<!--MODE convertNamespace -->
	<!--==================================================-->
	
	<xsl:template match="FLASH | INFO" mode="convertNamespace">
		<xsl:variable name="namespace.uri" select="$contentNode.namespace.uri" as="xs:string"/>
		<xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
			<xsl:apply-templates select="node() | @* except @ID" mode="#current"> <!--@ID déjà posé sur structuralNOde-->
				<xsl:with-param name="namespace.uri" select="$namespace.uri" tunnel="yes"/>
			</xsl:apply-templates>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="*" mode="convertNamespace" priority="-1">
		<xsl:param name="namespace.uri" required="yes" as="xs:string" tunnel="yes"/>
		<xsl:element name="{local-name(.)}" namespace="{$namespace.uri}">
			<xsl:apply-templates select="node() | @*" mode="#current"/>
		</xsl:element>
	</xsl:template>
	
	<xsl:template match="node() | @*" mode="convertNamespace" priority="-2">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*"/>
		</xsl:copy>
	</xsl:template>
	
	<!-- ID -->
	
	<xsl:template match="@ID" mode="convertNamespace">
		<xsl:copy-of select="."/>
		<xsl:attribute name="xf:id" select="xfSAS:makeHistoricIdAttribute(parent::*, .)"/>
	</xsl:template>
	
	<!--==================================================-->
	<!--COMMON"-->
	<!--==================================================-->
	
	<xsl:template match="node() | @*">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*"/>
		</xsl:copy>
	</xsl:template>
	
</xsl:stylesheet>
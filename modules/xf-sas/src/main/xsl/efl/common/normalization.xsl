<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:normalization-util="normalization-util" 
  xmlns:functx="http://www.functx.com" 
  exclude-result-prefixes="#all" 
  version="2.0">

  <xsl:import href="xslLib:/xslLib/functx.xsl"/>

  <xsl:function name="normalization-util:normalize-fbv-num" as="xs:string">
    <xsl:param name="num" as="xs:string"/>
    <xsl:variable name="NUM-SPLIT" select="'[. \-*:;,]'"/>
    <xsl:variable name="NUM-LENGTH" select="(6, 6, 2, 2, 2)"/>
    <xsl:variable name="NUM_COUNT" select="count($NUM-LENGTH)"/>
    <xsl:variable name="num-split" select="tokenize($num,$NUM-SPLIT)"/>
    <xsl:variable name="num-split-count" select="count($num-split)"/>
    <xsl:variable name="result" as="xs:string*">
      <xsl:for-each select="for $i in 1 to min(($NUM_COUNT,$num-split-count)) return $i">
        <xsl:variable name="C-NUM-LENGTH" select="$NUM-LENGTH[current()]"/>
        <xsl:variable name="c-num" select="$num-split[current()]"/>
        <xsl:variable name="c-num-length" select="string-length($c-num)"/>
        <xsl:value-of select="concat(functx:repeat-string('0',$C-NUM-LENGTH - min(($C-NUM-LENGTH,$c-num-length))),substring($c-num,1,min(($C-NUM-LENGTH,$c-num-length))))"/>
      </xsl:for-each>
      <xsl:for-each select="for $i in min(($NUM_COUNT,$num-split-count)) + 1 to max(($NUM_COUNT,$num-split-count)) return $i">
        <xsl:value-of select="functx:repeat-string('0',$NUM-LENGTH[current()])"/>
      </xsl:for-each>
    </xsl:variable>
    <xsl:value-of select="string-join($result,'')"/>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-month" as="xs:string">
    <xsl:param name="month" as="xs:string"/>
    <xsl:variable name="month-split" select="tokenize($month,'-')"/>
    <xsl:variable name="result" as="xs:string*">
      <xsl:for-each select="$month-split">
        <xsl:value-of select="
                    if (functx:is-a-number(.))
                    then
                    format-number(number(.), '00')
                    else
                    'MM'
                    "/>
      </xsl:for-each>
    </xsl:variable>
    <xsl:value-of select="string-join($result,'-')"/>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-year" as="xs:string">
    <xsl:param name="year" as="xs:string"/>
    <xsl:value-of select="
            if (functx:is-a-number($year) and string-length($year) = (2,4) )
            then
            (
            if (string-length($year) eq 4)
            then
            $year
            else
            if (number($year) &lt; 50)
            then
            concat(20, $year)
            else
            concat(19, $year)
            )
            else
            'AAAA'
            "/>
  </xsl:function>

  <xsl:function name="normalization-util:split-date" as="xs:string*">
    <!--
      normalize a string to match a date representation
        -->
    <xsl:param name="in" as="xs:string*"/>
    <xsl:variable name="inseq" select="(tokenize($in, '/'),'','')" as="xs:string*"/>

    <xsl:sequence select="($inseq[1],$inseq[2])"/>

  </xsl:function>

  <xsl:function name="normalization-util:normalize-gps-date" as="xs:string">
    <xsl:param name="date" as="xs:string?"/>
    <xsl:variable name="date-split" select="tokenize($date, '/')"/>
    <xsl:choose>
      <xsl:when test="count($date-split) = 3">
        <xsl:value-of select="string-join(($date-split[3],$date-split[2],$date-split[1]), '-')"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$date"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-gps-num" as="xs:string">
    <xsl:param name="num" as="xs:string?"/>
    <xsl:value-of select="replace(tokenize($num, '(ET|,)')[1], '[^0-9A-Z]', '')"/>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-no" as="xs:string?">
    <xsl:param name="no" as="xs:string?"/>
    <xsl:if test="$no">
      <xsl:value-of select="replace($no,'(^[.]+|\s+,[.]+$)', '')"/>
    </xsl:if>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-renvoi-no" as="xs:string">
    <xsl:param name="no" as="xs:string?"/>
    <xsl:value-of select="replace($no,'(^[.]+|\s+,[.]+$)', '')"/>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-gps-lieu" as="xs:string">
    <xsl:param name="lieu" as="xs:string?"/>
    <xsl:value-of select="normalization-util:strip-accent(replace(upper-case($lieu), '[^A-Z]', ''))"/>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-nature" as="xs:string">
    <xsl:param name="nature" as="xs:string?"/>
    <xsl:value-of select="normalization-util:strip-accent(lower-case(replace($nature, '[- /]', '')))"/>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-nom" as="xs:string">
    <xsl:param name="nom" as="xs:string?"/>
    <xsl:value-of select="normalization-util:strip-accent(lower-case(replace(replace($nom, '(c/|sa |societe |ste |sarl |[ ])', ''), '[-.,;:]', '')))"/>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-text-ref" as="xs:string">
    <xsl:param name="ref" as="xs:string?"/>
    <xsl:value-of select="normalization-util:strip-accent(lower-case(replace(replace(replace(replace(replace($ref, '^c-', ''), ' - ', ''), '[-. ]', ''), '^ ?aff[.]? ', ''), '^ ?n ', '')))"/>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-text-rc" as="xs:string">
    <xsl:param name="rc" as="xs:string?"/>
    <xsl:variable name="rc-split" as="xs:string*">
      <xsl:analyze-string select="$rc" regex="[0-9/]+">
        <xsl:matching-substring>
          <xsl:value-of select="."/>
        </xsl:matching-substring>
      </xsl:analyze-string>
    </xsl:variable>
    <xsl:value-of select="string($rc-split[1])"/>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-date" as="xs:string">
    <xsl:param name="date" as="xs:string*"/>
    <xsl:value-of select="$date"/>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-art" as="xs:string">
    <xsl:param name="art" as="xs:string?"/>
    <xsl:value-of select="normalization-util:strip-accent(lower-case(replace(
            replace(
            replace(
            replace(
            replace(
            replace(
            replace(
            replace(
            replace(
            replace(
            replace($art,' - ','')
            ,' s[.]$','')
            ,'question ','')
            ,'c. com.','')
            ,'§','')
            ,', .*$','')
            ,' et .*$','')
            ,'al.*$','')
            ,'[ .*]|article','')
            ,'art','')
            ,'-$','')))"/>
  </xsl:function>

  <xsl:function name="normalization-util:normalize-intr" as="xs:string">
    <xsl:param name="intr" as="xs:string?"/>
    <xsl:value-of select="$intr"/>
  </xsl:function>

  <xsl:function name="normalization-util:strip-accent" as="xs:string">
    <xsl:param name="value" as="xs:string?"/>
    <xsl:value-of select="normalize-unicode(replace(normalize-unicode($value, 'NFD'), '\p{Mn}', ''), 'NFC')"/>
  </xsl:function>


</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" xmlns:eflMem="http://www.lefebvre-sarrut.eu/ns/efl/memento" xmlns:h="http://www.w3.org/1999/xhtml" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst" exclude-result-prefixes="#all" version="2.0">

  <xsl:template match="editorialEntity/body" mode="makeRelations">
    <xsl:next-match/>
    <relations>
      <xsl:apply-templates select="contentNode/content//eflMem:rmemn" mode="makeRelationElement"/>
      <xsl:apply-templates select="contentNode/content//eflMem:link" mode="makeRelationElement"/>
      <!--<xsl:apply-templates select="contentNode/content//infoCommentaire:renvoiAutruiTransition" mode="makeRelationElement"/>
			<xsl:apply-templates select="contentNode/content//infoCommentaire:refDoc" mode="makeRelationElement"/>-->
    </relations>
  </xsl:template>

  <!--=====================================-->
  <!--LINK-->
  <!--=====================================-->

  <!--<LINK COL="FIS" LINKID="P80404DB834I8171-EFL" SUPP="FR">10 s.</LINK>-->

  <xsl:template match="eflMem:link[not(parent::eflMem:rmemn)]" mode="makeRelationElement">
    <relation xf:id="{generate-id(.)}" type="generated">
      <!--<metadata>
				<xsl:apply-templates select="@*" mode="#current"/>
			</metadata>-->
      <ref xf:base="{$xfSAS:REST.ee.efl.uri}" xf:refType="efl:{local-name(.)}" xf:idref="{xfSAS:makeHistoricIdAttribute(., @linkid)}"/>
    </relation>
  </xsl:template>

  <xsl:template match="contentNode/content//eflMem:link[not(parent::eflMem:rmemn)]" mode="makeRelations">
    <xsl:copy>
      <xsl:attribute name="xf:idRef" select="generate-id(.)"/>
      <xsl:attribute name="xf:refType" select="'xf:relation'"/>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <!--=====================================-->
  <!--rmemn-->
  <!--=====================================-->

  <!--ex : <rmemn NO="40900" SUPP="MF" TYPE="S">40900 s.</rmemn>-->

  <xsl:template match="eflMem:rmemn" mode="makeRelationElement">
    <relation xf:id="{generate-id(.)}" type="generated">
      <metadata>
        <xsl:apply-templates select="@*" mode="#current"/>
        <xsl:if test="ancestor::rattachend">
          <meta code="relationEFL_TYPE" value="actualisation"/>
        </xsl:if>
      </metadata>
      <ref xf:base="{$xfSAS:REST.ee.efl.mementos.uri}" xf:refType="efl:{local-name(.)}" xf:idRef="{xfSAS:makeQueryIdAttribute(., (
				'typeEE=EFL_INFO',
				concat('parent.', @supp ,'.anneePublication=', @annee),
				concat('parent.', @supp ,'.numeroInfo=', @no)
				))}"> </ref>
    </relation>
  </xsl:template>

  <xsl:template match="eflMem:rmemn/@*" mode="makeRelationElement"/>

  <xsl:template match="eflMem:rmemn/@TYPE" mode="makeRelationElement" priority="1">
    <meta code="relationEFL_typeCible">
      <value>
        <xsl:value-of select="."/>
      </value>
    </meta>
  </xsl:template>

  <xsl:template match="contentNode/content//eflMem:rmemn" mode="makeRelations">
    <xsl:copy>
      <xsl:attribute name="xf:idRef" select="generate-id(.)"/>
      <xsl:attribute name="xf:refType" select="'xf:relation'"/>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <!--=====================================-->
  <!--COPY-->
  <!--=====================================-->

  <xsl:template match="node() | @*" mode="makeRelations">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>

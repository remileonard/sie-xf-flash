<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
  xmlns:eflMem="http://www.lefebvre-sarrut.eu/ns/efl/memento"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xpath-default-namespace=""
  exclude-result-prefixes="#all">

  <xsl:import href="mementoBu2editorialEntity.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>transformation des mémento QUESTION au format Flash</xd:desc>
  </xd:doc>
  
  <xd:doc scope="component">
    <xd:desc>liste des noms d'éléments transformés en StructuralNode</xd:desc>
  </xd:doc>
  <xsl:variable name="inputStructuralNodeElementsList" as="xs:string+" select="(
    'au', 'introduc', 'pabrev', 'corps', 'titre', 'chap', 'anx', 'essentiel', 'qrprat',
    'sec', 'nivc', 'nivd', 'nive', 'nivf', 'nivh'
    )" />
  
  <xd:doc scope="component">
    <xd:desc>encapsulation du corps de l'editorialEntity dans un contentNode</xd:desc>
  </xd:doc>
  <xsl:variable name="contentNodeEE" as="xs:boolean" select="false()" />

  <xd:doc scope="component">
    <xd:desc>espace de nom des contentNodes</xd:desc>
  </xd:doc>
  <xsl:variable name="contentNode.namespace.uri" select="'http://www.lefebvre-sarrut.eu/ns/efl/memento'" />
  
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:transform version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
  xmlns:efl="http://www.lefebvre-sarrut.eu/ns/efl"
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els"
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
  xmlns:eflMem="http://www.lefebvre-sarrut.eu/ns/efl/memento"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xpath-default-namespace=""
  exclude-result-prefixes="#all">
  
  <xsl:import href="mementoBu2editorialEntity.makeRelations.xsl"/>
  <xsl:import href="../../xf-sas-common.xsl"/>
  
  <xd:doc scope="stylesheet">
    <xd:desc>transformation des structures de mémentos en structures Flash</xd:desc>
  </xd:doc>
  
  <xsl:output method="xml" indent="yes"/>
  
  <xd:doc>
    <xd:desc>désignation de maison d'édition</xd:desc>
  </xd:doc>
  <xsl:param name="xfSAS:company" select="'efl'" as="xs:string"/>
  
  <xd:doc>
    <xd:desc>encapsulation du corps de l'editorialEntity dans un contentNode</xd:desc>
  </xd:doc>
  <xsl:param name="contentNodeEE" as="xs:boolean" />
  
  <xd:doc>
    <xd:desc>définition d'un préfixe basé sur le nom de maison d'édition pour établir des identifiants historiques</xd:desc>
  </xd:doc>
  <xsl:variable name="historicId.prefix" select="concat(upper-case($xfSAS:company),'_')" as="xs:string"/>
    
  <xd:doc>
    <xd:desc>initialisation de traitement multi-passes
      
    - création d'un entité éditoriale
    - journalisation
    - établissement des relations
    </xd:desc>
  </xd:doc>
  <xsl:template match="/">
    <xsl:call-template name="xfSAS:makeXmlModelPi"/>
    <xsl:variable name="step1-xml2ee" as="document-node()">
      <xsl:document>
        <xsl:apply-templates/>
      </xsl:document>
    </xsl:variable>
    <xsl:result-document href="log/step1-xml2ee.log.xml">
      <xsl:sequence select="$step1-xml2ee"/>
    </xsl:result-document>
    <xsl:apply-templates select="$step1-xml2ee" mode="makeRelations"/>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>désencapsulation de l'entité éditorial racine</xd:desc>
  </xd:doc>
  <xsl:template match="/*[1][not(eflMem:isEE(.))]">
    <xsl:apply-templates/>
  </xsl:template>
  
  <!--==============================-->
  <!-- editorialEntity -->
  <!--==============================-->
  
  <xd:doc>
    <xd:desc>sélectionne une editorialEntity candidate</xd:desc>
  </xd:doc>
  <xsl:function name="eflMem:isEE" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:sequence select="boolean($e[
      count(*) gt 1 and .. is root() or
      parent::*[count(*) = 1 and .. is root()]
    ])" />
  </xsl:function>
  
  <xd:doc>
    <xd:desc>génère une EditorialEntity à partir d'une candidate</xd:desc>
  </xd:doc>
  <xsl:template match="*[eflMem:isEE(.)]" name="eflMem:makeEE">
    <xsl:param name="code">EFL_MEMENTO_DOC</xsl:param>
    <xsl:param name="id" select="if (@id) then @id else eflMem:makeSemanticId(.)"/>
    <xsl:param name="body">
      <xsl:apply-templates select="*[not(eflMem:isMeta(.))] | *[eflMem:isMetaAndData(.)]"/>
    </xsl:param>
    <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
      xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
      xmlns:els="http://www.lefebvre-sarrut.eu/ns/els">
      <xsl:namespace name="xf">http://www.lefebvre-sarrut.eu/ns/xmlfirst</xsl:namespace>
      <xsl:attribute name="code" select="$code"/>
      <xsl:attribute name="xf:id" select="$id"/>
      <xsl:apply-templates select="." mode="metadata"/>
      <body>
        <xsl:choose>
          <xsl:when test="$contentNodeEE">
            <xsl:call-template name="eflMem:contentNode">
              <xsl:with-param name="dummyRootName" select="local-name(.)"/>
              <xsl:with-param name="content">
                <xsl:apply-templates select="*[not(eflMem:isMeta(.))] | *[eflMem:isMetaAndData(.)]" mode="convertNamespace" />
              </xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:sequence select="$body" />
          </xsl:otherwise>
        </xsl:choose>
      </body>
    </editorialEntity>
  </xsl:template>
  
  <!--==================================================-->
  <!-- metadata -->
  <!--==================================================-->
  
  <xsl:template match="st" mode="metadata">
    <xsl:call-template name="eflMem:setHTMLMeta">
      <xsl:with-param name="code">subTitle</xsl:with-param>
      <xsl:with-param name="value" select="."/>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="maj" mode="metadata">
    <xsl:variable name="prefix" select="'EFL_META_'"/>
    <xsl:call-template name="eflMem:setHTMLMeta">
      <xsl:with-param name="code" select="concat($prefix,name(.))"/>
      <xsl:with-param name="value" select="." />
    </xsl:call-template>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>sélectionne les items générateurs de métadonnées</xd:desc>
  </xd:doc>
  <xsl:function name="eflMem:isMeta" as="xs:boolean">
    <xsl:param name="i" as="item()"/>
    <xsl:choose>
      <xsl:when test="name($i)=('maj','st','expert','mille','pays','archive','code','milco','theme')">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <xsl:when test="name($i/parent::*)=('p','ptextes','pforms','pano','ana') and name($i)=('no','apercu','periode','majour')">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xsl:function name="eflMem:isMetaAndData" as="xs:boolean">
    <xsl:param name="i" as="item()"/>
    <xsl:choose>
      <xsl:when test="name($i/parent::*)=('p','ptextes','pforms','pano','ana') and name($i)=('no')">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>génère une méta EFL pour chaque élément candidat</xd:desc>
  </xd:doc>
  <xsl:template match="*" mode="metadata">
    <xsl:if test="eflMem:isMeta(.)">
      <xsl:sequence select="eflMem:setMeta(.)"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="@*" mode="metadata">
    <!--
      génère une méta EFL pour chaque attribut candidat
    -->
    <xsl:if test="eflMem:isMeta(.)">
      <xsl:variable name="prefix" select="'EFL_META_'"/>
      <xsl:choose>
        <xsl:when test="name(.)=('mille','milco')">
          <xsl:call-template name="eflMem:setMeta">
            <xsl:with-param name="code" select="concat($prefix,'millesime')"/>
            <xsl:with-param name="value" select="."/>
          </xsl:call-template>
        </xsl:when>
        <xsl:when test="name(.)='expert'">
          <xsl:call-template name="eflMem:setMeta">
            <xsl:with-param name="code" select="concat($prefix,name(.),'Memento')"/>
            <xsl:with-param name="value" select="."/>
          </xsl:call-template>
        </xsl:when>
        <xsl:otherwise>
          <xsl:call-template name="eflMem:setMeta">
            <xsl:with-param name="code" select="concat($prefix,name(.))"/>
            <xsl:with-param name="value" select="."/>
          </xsl:call-template>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="*[eflMem:isEE(.)]" mode="metadata">
    <!--
      établit les métadonnées d'une EditorialEntity
    -->
    <metadata>
      <xsl:apply-templates select="(*|@*)[eflMem:isMeta(.)]" mode="metadata"/>
    </metadata>
  </xsl:template>
  
  <xsl:template match="*[eflMem:isStructuralNode(.)]" mode="metadata">
    <!--
      établit les métadonnées d'un StructuralNode
    -->
    <metadata>
      <xsl:apply-templates select="(*|@*)[eflMem:isMeta(.)]" mode="metadata"/>
    </metadata>
  </xsl:template>
  
  <xsl:template match="*[eflMem:isReferenceNode(.)]" mode="metadata">
    <!--
      établit les métadonnées d'un ReferenceNode
    -->
    <metadata>
      <xsl:apply-templates select="(*|@*)[eflMem:isMeta(.)]" mode="metadata"/>
      <!-- génération d'un systemTitle -->
      <xsl:call-template name="eflMem:setMeta">
        <xsl:with-param name="code" select="'systemTitle'"/>
        <xsl:with-param name="value" select="concat('paragraphe ', no)"/>
      </xsl:call-template>
      <!-- génération d'un systemAbstract -->
      <xsl:call-template name="eflMem:setMeta">
        <xsl:with-param name="code" select="'systemAbstract'"/>
        <xsl:with-param name="value" select="''"/>
      </xsl:call-template>
    </metadata>
  </xsl:template>
  
  <xsl:template name="eflMem:setMeta" as="element()">
    <!--
      génère une métadonnée standard
    -->
    <xsl:param name="code" as="xs:string"/>
    <xsl:param name="value" as="xs:string?"/>
    <meta code="{$code}">
      <value>
        <xsl:value-of select="$value"/>
      </value>
    </meta>
  </xsl:template>
  
  <xsl:template name="eflMem:setHTMLMeta" as="element()">
    <!--
      génère une métadonnée de type HTML
    -->
    <xsl:param name="code" as="xs:string"/>
    <xsl:param name="value" as="item()"/>
    <meta code="{$code}">
      <value>
        <div xmlns="http://www.w3.org/1999/xhtml">
          <xsl:apply-templates select="$value" mode="xml2html"/>
        </div>
      </value>
    </meta>
  </xsl:template>
  
  <xsl:function name="eflMem:setMeta" as="element()">
    <!--
      génère une métadonnée standard à partir d'une chaîne et d'un item
    -->
    <xsl:param name="code" as="xs:string"/>
    <xsl:param name="value" as="item()"/>
    <xsl:call-template name="eflMem:setMeta">
      <xsl:with-param name="code" select="$code"/>
      <xsl:with-param name="value" select="$value"/>
    </xsl:call-template>
  </xsl:function>
  
  <xsl:function name="eflMem:setMeta" as="element()">
    <!--
      génère une métadonnée standard à partir d'un item
    -->
    <xsl:param name="item" as="item()"/>
    <xsl:sequence select="eflMem:setMeta(eflMem:genCode($item),$item)"/>
  </xsl:function>
  
  <xsl:function name="eflMem:genCode" as="xs:string">
    <!--
      génère un code par défaut pour l'item courant
    -->
    <xsl:param name="i" as="item()"/>
    <xsl:value-of select="eflMem:genCode($historicId.prefix,$i)"/>
  </xsl:function>
  
  <xsl:function name="eflMem:genCode" as="xs:string">
    <!--
      génère un code par défaut basé sur un préfixe,
      le nom de l'item paramètre et le nom du parent de l'item paramètre
    -->
    <xsl:param name="prefix" as="xs:string"/>
    <xsl:param name="i" as="item()"/>
    <xsl:value-of select="
      concat( $prefix, local-name($i/..), '_', local-name($i) )
    "/>
  </xsl:function>
  
  <!--==================================================-->
  <!-- contentNode -->
  <!--==================================================-->
  
  <xd:doc>
    <xd:desc>encapsule un contenu ('content') dans un contentNode
    
    Si 'dummyRootName' est non nul, ajoute une racine dummyRoot
    portant le nom saisi en paramètre</xd:desc>
  </xd:doc>
  <xsl:template name="eflMem:contentNode">
    <xsl:param name="dummyRootName" as="xs:string?"/>
    <xsl:param name="content"  as="item()+"/>
    <contentNode>
      <content>
        <xsl:choose>
          <xsl:when test="string($dummyRootName) = ''">
            <xsl:sequence select="$content"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:element name="{$dummyRootName}" namespace="{$contentNode.namespace.uri}">
              <xsl:attribute name="dummyRoot" select="'true'"/>
              <xsl:sequence select="$content"/>
            </xsl:element>
          </xsl:otherwise>
        </xsl:choose>
      </content>
    </contentNode>
  </xsl:template>
  
  <!--==================================================-->
  <!-- structuralNode -->
  <!--==================================================-->
  
  <xd:doc>
    <xd:desc>sélectionne un candidat structuralNode</xd:desc>
  </xd:doc>
  <xsl:function name="eflMem:isStructuralNode" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:choose>
      <xsl:when test="eflMem:isEE($e)">
        <xsl:sequence select="false()"/>
      </xsl:when>
      <xsl:when test="local-name($e) = $inputStructuralNodeElementsList">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <xsl:when test="eflMem:isNiv($e)">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>convertit l'entrée sélectionnée en un structuralNode</xd:desc>
  </xd:doc>
  <xsl:template match="*[eflMem:isStructuralNode(.)]">
    <xsl:variable name="dummyRootName" select="local-name(.)" as="xs:string"/>
    <xsl:variable name="type">
      <xsl:choose>
        <xsl:when test="name()='chap'">CHAPITRE</xsl:when>
        <xsl:when test="name()='sec'">SECTION</xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="name()"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="id" select="if (@id) then xfSAS:makeHistoricIdAttribute(., @id) else eflMem:makeSemanticId(.)"/>
    <xsl:variable name="num"
      select="if(exists(intr))
      then replace(replace(replace(intr,'SECTION',''),'CHAPITRE',''),'\s+','')
      else ()"
    />
    <structuralNode type="{$type}" xf:id="{$id}">
      <xsl:if test="exists($num)">
        <xsl:attribute name="num" select="$num"/>
      </xsl:if>
      <xsl:apply-templates select="." mode="metadata"/>
      <xsl:apply-templates select="." mode="title"/>
      <xsl:for-each-group select="*[not(eflMem:isStructuralNodeTitle(.) or eflMem:isIgnorable(.))]"
        group-adjacent="not(eflMem:isStructuralNode(.)) and not(eflMem:isReferenceNode(.))">
        <xsl:choose>
          <xsl:when test="current-grouping-key()">
            <xsl:call-template name="eflMem:contentNode">
              <xsl:with-param name="dummyRootName" select="$dummyRootName"/>
              <xsl:with-param name="content">                
                <xsl:apply-templates select="current-group()" mode="convertNamespace"/>                                
              </xsl:with-param>
            </xsl:call-template>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="current-group()"/>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each-group>
    </structuralNode>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>sélectionne le titre d'un candidat structuralNode</xd:desc>
  </xd:doc>
  <xsl:function name="eflMem:isStructuralNodeTitle" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:choose>
      <xsl:when
        test="eflMem:isNiv($e/parent::*)
        and local-name($e) = concat('t', substring-after(local-name($e/parent::*), 'niv'))">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <!--
        On estime que les titres sont toujours sous le niveau.
      -->
      <xsl:when
        test="eflMem:isStructuralNode($e/parent::*) 
        and local-name($e) = (
        'intr', 't', 'st', 'tnouv1', 'tanx', 'tpart', 'ttitre', 'tchap',
        'tchapbis', 'tsec', 'tssec', 'tessentiel', 'tintroduc', 'tqrprat',
        'ttx', 'trub','strub','stanx')">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>génère le titre d'un structuralNode</xd:desc>
  </xd:doc>
  <xsl:template match="*[eflMem:isStructuralNode(.)]" mode="title">
    <title>
      <div xmlns="http://www.w3.org/1999/xhtml">
        <xsl:choose>
          <xsl:when test="self::intro">
            <xsl:text>Introduction</xsl:text>
          </xsl:when>
          <xsl:when test="self::pabrev">
            <xsl:text>Abréviations</xsl:text>
          </xsl:when>
          <xsl:when test="self::corps">
            <xsl:text>Corps</xsl:text>
          </xsl:when>
          <xsl:when test="self::au">
            <xsl:text>Auteurs</xsl:text>
          </xsl:when>
          <xsl:when test="self::qrprat">
            <xsl:apply-templates select="tqrprat" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::introduc">
            <xsl:apply-templates select="tintroduc" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::essentiel">
            <xsl:apply-templates select="tessentiel" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::present">
            <xsl:apply-templates select="t" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::nouv1">
            <xsl:apply-templates select="tnouv1" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::pnouv">
            <xsl:apply-templates select="t" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::anx">
            <xsl:apply-templates select="tanx|stanx" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::part">
            <xsl:apply-templates select="tpart|st" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::partanx">
            <xsl:apply-templates select="tpart" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::titre">
            <xsl:apply-templates select="ttitre|st" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::chap">
            <xsl:apply-templates select="tchap|st" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::chapbis">
            <xsl:apply-templates select="tchapbis|st" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::rub">
            <xsl:apply-templates select="trub|strub" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::sec">
            <xsl:apply-templates select="tsec|st" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::ssec">
            <xsl:apply-templates select="tssec|st" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::tx">
            <xsl:apply-templates select="ttx" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::textes">
            <xsl:apply-templates select="t|st|sst" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="self::divers">
            <xsl:apply-templates select="ttx" mode="xml2html"/>
          </xsl:when>
          <xsl:when test="eflMem:isNiv(.)">
            <xsl:variable name="nivTitle" select="eflMem:getNivTitle(.)" as="element()*"/>
            <xsl:apply-templates select="$nivTitle|st" mode="xml2html"/>
            <xsl:if test="count($nivTitle) != 1">
              <xsl:message terminate="yes">[WARNING] <xsl:value-of select="count($nivTitle)"/>
                titres "<xsl:value-of select="eflMem:getNivTitleName(.)"/>" trouvé(s) pour
                <xsl:value-of select="local-name()"/> : <xsl:value-of select="els:get-xpath(.)"
                /> children : <xsl:value-of select="*/local-name(.)" separator=", "/>
              </xsl:message>
            </xsl:if>
          </xsl:when>
          <xsl:otherwise>
            <xsl:message select="concat('[ERROR] Pas de titre prévu pour &quot;', local-name(), '&quot;')" />
          </xsl:otherwise>
        </xsl:choose>
      </div>
    </title>
  </xsl:template>
  
  <!--==================================================-->
  <!-- referenceNode -->
  <!--==================================================-->
  
  <xd:doc>
    <xd:desc>sélectionne un candidat referenceNode</xd:desc>
  </xd:doc>
  <xsl:function name="eflMem:isReferenceNode" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:choose>
      <xsl:when test="$e[self::*[name(.)=('p','ptextes','pforms','pano','ana')][no]]">
        <xsl:sequence select="true()"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:sequence select="false()"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>convertit l'entrée sélectionnée en referenceNode</xd:desc>
  </xd:doc>
  <xsl:template match="*[eflMem:isReferenceNode(.)]">
    <xsl:variable name="id" select="if (@id) then xfSAS:makeHistoricIdAttribute(., @id) else eflMem:makeSemanticId(.)"/>
    <referenceNode code="TEE_EFL_memento_Pnum">
      <xsl:apply-templates mode="metadata" select="."/>
      <ref xf:idRef="{$id}" xf:refType="referenceNode">
        <xsl:call-template name="eflMem:makeEE">
          <xsl:with-param name="code" select="concat('EFL_MEMENTO_',local-name(.))"/>
          <xsl:with-param name="id" select="$id"/>
          <xsl:with-param name="body">
            <xsl:call-template name="eflMem:contentNode">
              <xsl:with-param name="content">
                <xsl:apply-templates select="." mode="convertNamespace"/>       
              </xsl:with-param>
            </xsl:call-template>
          </xsl:with-param>
        </xsl:call-template>
      </ref>
    </referenceNode>
  </xsl:template>
  
  <!--==================================================-->
  <!-- nivx -->
  <!--==================================================-->
  
  <xd:doc>
    <xd:desc>sélectionne un Niv</xd:desc>
  </xd:doc>
  <xsl:function name="eflMem:isNiv" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:sequence
      select="local-name($e) = ('nivc', 'nivd', 'nive', 'nivf', 'nivh', 'nivhbis', 'nivi', 'nivj', 'nivm', 'nivp')"
    />
  </xsl:function>
  
  <xd:doc>
    <xd:desc>sélectionne le nom de titre d'un Niv</xd:desc>
  </xd:doc>
  <xsl:function name="eflMem:getNivTitleName" as="xs:string">
    <xsl:param name="e" as="element()"/>
    <xsl:choose>
      <xsl:when test="eflMem:isNiv($e)">
        <xsl:value-of select="concat('t', substring-after(local-name($e), 'niv'))"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>[ERROR] appel de eflMem:getNivTitleName() avec <xsl:value-of
          select="local-name($e)"/> qui n'est pas un niv</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>sélectionne le titre d'un Niv</xd:desc>
  </xd:doc>
  <xsl:function name="eflMem:getNivTitle" as="element()*">
    <xsl:param name="e" as="element()"/>
    <xsl:choose>
      <xsl:when test="eflMem:isNiv($e)">
        <xsl:sequence select="$e/*[local-name(.) = eflMem:getNivTitleName($e)]"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:message>[ERROR] appel de eflMem:getNivTitle() avec <xsl:value-of
          select="local-name($e)"/> qui n'est pas un niv</xsl:message>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>
  
  <!--==================================================-->
  <!-- conversion de namespace -->
  <!--==================================================-->
  
  <xd:doc>
    <xd:desc>ignore les items déjà transformés en métadonnées</xd:desc>
  </xd:doc>
  <xsl:template mode="convertNamespace" match="*[eflMem:isMeta(.) and not(eflMem:isMetaAndData(.))]"/>
  <xsl:template mode="convertNamespace" match="@*[eflMem:isMeta(.) and not(eflMem:isMetaAndData(.))]"/>
  
  <xsl:template match="*" mode="convertNamespace" priority="-1">
    <xsl:element name="{local-name(.)}" namespace="{$contentNode.namespace.uri}">
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:element>
  </xsl:template>
  
  <xd:doc>
    <xd:desc>conversion d'identifiant historique 'historicalId'
    
    Copie l'attribut et crée un nouvel attribut inscrit dans l'espace de nom cible.
    
    Ne génère pas d'identifiant historicalId sur les éléments p encapsulés dans un referenceNode :
    le même historicalId est déjà porté sur le EditorialEntity généré.</xd:desc>
  </xd:doc>
  <xsl:template match="@id" mode="convertNamespace">
    <xsl:sequence select="."/>
    <xsl:if test="not(parent::*[eflMem:isReferenceNode(.)])">
      <xsl:attribute name="xf:id" select="xfSAS:makeHistoricIdAttribute(parent::*, .)"/>      
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="@*" mode="convertNamespace" priority="-2">
    <xsl:sequence select="."/>
  </xsl:template>
  
  <!--==================================================-->
  <!-- conversion xml2html -->
  <!--==================================================-->
  
  <xd:doc>
    <xd:desc>convertit en HTML le contenu des titres des StructuralNodes et de certaines métadonnées</xd:desc>
  </xd:doc>
  <xsl:template match="*[eflMem:isStructuralNodeTitle(.) or eflMem:isMeta(.)]" mode="xml2html">
    <div class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </div>
  </xsl:template>
  
  <!-- inlines -->
  
  <xsl:template match="motrep" mode="xml2html">
    <strong class="motRep" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </strong>
  </xsl:template>
  
  <xsl:template match="e" mode="xml2html">
    <sup class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </sup>
  </xsl:template>
  
  <xsl:template match="i" mode="xml2html">
    <sub class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </sub>
  </xsl:template>
  
  <xsl:template match="ital" mode="xml2html">
    <em class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </em>
  </xsl:template>
  
  <xsl:template match="coupe" mode="xml2html">
    <span class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>
  
  <xsl:template match="intr | intro | ttx1 | ttx2 | t" mode="xml2html">
    <span class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates mode="#current"/>
    </span>
  </xsl:template>
    
  <xd:doc>
    <xd:desc>liens en mode xml2html</xd:desc>
  </xd:doc>
  <xsl:template mode="xml2html" match="
    rmem | rmemn | rfr | rfrn | rprat | rpratn |
    rbull | rbulln | rnote | note | note/al |
    refdoc | refdocetroit | refodoc1 | refdoc2 | rtx | link">
    <xsl:if test="normalize-space(.) != ''">
      <span class="{local-name(.)}" xmlns="http://www.w3.org/1999/xhtml">
        <xsl:apply-templates mode="#current"/>
      </span>
    </xsl:if>
  </xsl:template>
  <xsl:template match="nature | ref | rc | nom | date" mode="xml2html">
      <xsl:apply-templates mode="#current"/>
  </xsl:template>
  
  
  <xd:doc>
    <xd:desc>identité par défaut en mode xml2html</xd:desc>
  </xd:doc>
  <xsl:template match="*" mode="xml2html" priority="-1">
    <xsl:message
      select="concat('[WARNING] ', local-name(..), '/', local-name(.),
      ' non converti en mode xml2html')"/>
    <xsl:copy>
      <xsl:sequence select="@*"/>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--==================================================-->
  <!-- common -->
  <!--==================================================-->
  
  <xd:doc>
    <xd:desc>générateur d'identifiant</xd:desc>
  </xd:doc>
  <xsl:function name="eflMem:makeSemanticId" as="xs:string">
    <xsl:param name="self" as="element()"/>
    <xsl:variable name="root" select="$self/ancestor-or-self::*[eflMem:isEE(.)][last()]" as="element()"/>
    <xsl:variable name="root_type" as="xs:string" select="($root/@code, local-name($root))[1]"/>
    <xsl:value-of select="concat(
      '{semanticId:',
      string-join(
        distinct-values(($root_type,local-name($self))),
        '_'
      ),
      '}' )"/>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>sélectionne un élément ignorable
      
      Un élément peut être ignoré lorsque son nom figure dans la liste $IGNORE.</xd:desc>
  </xd:doc>
  <xsl:function name="eflMem:isIgnorable" as="xs:boolean">
    <xsl:param name="e" as="element()"/>
    <xsl:variable name="IGNORE" select="('insignificant','chapsom')"/>
    <xsl:sequence select="local-name($e) = $IGNORE"/>
  </xsl:function>
  
  <xd:doc>
    <xd:desc>identité par défaut</xd:desc>
  </xd:doc>
  <xsl:template match="node() | @*">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*"/>
    </xsl:copy>
  </xsl:template>
  
</xsl:transform>

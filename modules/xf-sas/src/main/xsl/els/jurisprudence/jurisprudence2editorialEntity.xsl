<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:functx="http://www.functx.com" xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" xmlns:juri="http://www.els.fr/ns/juri" xmlns:h="http://www.w3.org/1999/xhtml" xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas" exclude-result-prefixes="#all" version="2.0">

	<xsl:import href="../../xf-sas-common.xsl"/>

	<xsl:output method="xml" indent="no"/>

	<xsl:param name="rel.to.schema" select="'xf-models:/editorialEntity'" as="xs:string"/>
	<xsl:param name="xfSAS:company" select="'els'" as="xs:string"/>

	<xsl:variable name="code" select="'ELS_TEE_jurisprudence'" as="xs:string"/>
	<xsl:variable name="origine" select="'_(dzrevues|ariane|dzlebon|legifrancejade|legifranceconstit|casscapp|casscass|legifrancecapp|legifrancecass|legifranceinca)'" as="xs:string"/>

	<xsl:template match="/">
		<xsl:call-template name="xfSAS:makeXmlModelPi"/>
		<xsl:variable name="step1-xml2ee" as="document-node()">
			<xsl:document>
				<xsl:apply-templates/>
			</xsl:document>
		</xsl:variable>
		<xsl:result-document href="log/step1-xml2ee.log.xml">
			<xsl:sequence select="$step1-xml2ee"/>
		</xsl:result-document>
		<xsl:apply-templates select="$step1-xml2ee" mode="xfSAS:fixMeta"/>
	</xsl:template>

	<!--===============================-->
	<!--Juri-->
	<!--===============================-->

	<xsl:template match="juri:Juri">
		<xsl:variable name="id" select="replace(@cle, concat($origine, '$'), '')" as="xs:string"/>		
		<editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst" xf:id="{xfSAS:makeHistoricIdAttribute(., $id)}" code="{$code}">
			<xsl:attribute name="ownerGroup" select="xfSAS:addAttributeOwnerGroup($code)"/>
			<xsl:namespace name="xf" select="'http://www.lefebvre-sarrut.eu/ns/xmlfirst'"/>
			<metadata>
				<xsl:apply-templates select="juri:MetaJuri/juri:DecisionTraitee" mode="metadata"/>
				<xsl:apply-templates select="juri:MetaFlux/juri:Document" mode="metadata"/>
			</metadata>
			<body>
				<contentNode>
					<content>
						<xsl:apply-templates select="." mode="contentNode"/>
					</content>
				</contentNode>
			</body>
			<relations/>
		</editorialEntity>
	</xsl:template>

	<!--==================================================-->
	<!--MODE metadata -->
	<!--==================================================-->

	<xsl:template match="juri:DecisionTraitee" mode="metadata">
		<meta code="ELS_META_jrpJuridiction">
			<value>
				<xsl:value-of select="juri:Juridiction"/>
			</value>
		</meta>
		<xsl:if test="juri:Formation">
			<meta code="ELS_META_jrpFormation">
				<value>
					<xsl:value-of select="juri:Formation"/>
				</value>
			</meta>
		</xsl:if>
		<xsl:if test="juri:Nature">
			<meta code="ELS_META_jrpNature">
				<value>
					<xsl:value-of select="juri:Nature"/>
				</value>
			</meta>
		</xsl:if>
		<xsl:if test="juri:Lieu">
			<meta code="ELS_META_jrpLieu">
				<value>
					<xsl:value-of select="juri:Lieu"/>
				</value>
			</meta>
		</xsl:if>
		<meta code="ELS_META_jrpDate">
			<value>
				<xsl:value-of select="concat(juri:Date, 'T00:00:00Z')"/>
			</value>
		</meta>
		<xsl:if test="juri:NumerosJuri">
			<meta code="ELS_META_jrpNumjuri">
				<value>
					<xsl:value-of select="juri:NumerosJuri/juri:Numero[1]"/>
				</value>
			</meta>
		</xsl:if>
		<xsl:if test="juri:NumerosDiffusion">
			<meta code="ELS_META_jrpNumdiffusion">
				<value>
					<xsl:value-of select="juri:NumerosDiffusion/juri:Numero[1]"/>
				</value>
			</meta>
		</xsl:if>
		<xsl:if test="juri:FormationDiffusion">
			<meta code="ELS_META_jrpFormationdiffusion">
				<value>
					<xsl:value-of select="juri:FormationDiffusion"/>
				</value>
			</meta>
		</xsl:if>
		<xsl:if test="juri:RenvoiHauteAutorite">
			<meta code="ELS_META_jrpRenvoihaut">
				<value>
					<xsl:value-of select="juri:RenvoiHauteAutorite"/>
				</value>
			</meta>
		</xsl:if>
		<xsl:if test="juri:ECLI">
			<meta code="ELS_META_jrpEcli">
				<value>
					<xsl:value-of select="juri:ECLI"/>
				</value>
			</meta>
		</xsl:if>
	</xsl:template>

	<xsl:template match="juri:Document" mode="metadata">
		<meta code="ELS_META_jrpCle">
			<value>
				<xsl:value-of select="juri:Cle"/>
			</value>
		</meta>
	</xsl:template>

	<!--==================================================-->
	<!--MODE contentNode -->
	<!--==================================================-->

	<xsl:template match="node() | @*" mode="contentNode">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*" mode="#current"/>
		</xsl:copy>
	</xsl:template>

	<!--==================================================-->
	<!--COMMON"-->
	<!--==================================================-->

	<xsl:template match="node() | @*">
		<xsl:copy>
			<xsl:apply-templates select="node() | @*"/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>

<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns:xs="http://www.w3.org/2001/XMLSchema" 
  xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
  xmlns:els="http://www.lefebvre-sarrut.eu/ns/els" 
  xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst" 
  xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
  xmlns:xfRes="http://www.lefebvre-sarrut.eu/ns/xmlfirst/ecm/resources"
  xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  xpath-default-namespace="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
  exclude-result-prefixes="#all" 
  version="2.0">
  
  <xsl:import href="xslLib:/xslLib/els-common.xsl"/>
  <!-- Librairie utilitaire lié aux logs ELS -->
  <xsl:import href="xslLib:/xslLib/els-log.xsl"/>
  <xsl:import href="xf-ecmRes:/_common/getCalculatedMeta.xsl"/>
  
  <!--dalloz, efl, el...-->
  <xsl:param name="xfSAS:company" required="yes" as="xs:string"/>
  <xsl:param name="xfSAS:rel.to.schema" select="'xf-models:/xf-models/editorialEntity_sas/'"/>

  <!-- ========================= URI REST =========================-->

  <!--EE RÉDIGÉES-->

  <xsl:variable name="xfSAS:REST.ee.uri" select="'http://xmlFirst/ee/'"/>

  <!--EFL-->
  <xsl:variable name="xfSAS:REST.ee.efl.uri" select="resolve-uri('efl/', $xfSAS:REST.ee.uri)"/>
  <xsl:variable name="xfSAS:REST.ee.efl.revues.uri" select="resolve-uri('revues/', $xfSAS:REST.ee.efl.uri)"/>
  <xsl:variable name="xfSAS:REST.ee.efl.mementos.uri" select="resolve-uri('mementos/', $xfSAS:REST.ee.efl.uri)"/>

  <!--DALLOZ-->
  <xsl:variable name="xfSAS:REST.ee.dalloz.uri" select="resolve-uri('dalloz/', $xfSAS:REST.ee.uri)"/>
  <xsl:variable name="xfSAS:REST.ee.dalloz.revues.uri" select="resolve-uri('revues/', $xfSAS:REST.ee.dalloz.uri)"/>
  <xsl:variable name="xfSAS:REST.ee.dalloz.ouvrages.uri" select="resolve-uri('ouvrages/', $xfSAS:REST.ee.dalloz.uri)"/>
  <xsl:variable name="xfSAS:REST.ee.dalloz.encyclopedies.uri" select="resolve-uri('encyclopedies/', $xfSAS:REST.ee.dalloz.uri)"/>
  <xsl:variable name="xfSAS:REST.ee.dalloz.codes.uri" select="resolve-uri('codes/', $xfSAS:REST.ee.dalloz.uri)"/>

  <!--EL-->
  <xsl:variable name="xfSAS:REST.ee.el.uri" select="resolve-uri('el/', $xfSAS:REST.ee.uri)"/>
  <xsl:variable name="xfSAS:REST.ee.el.formulaire.uri" select="resolve-uri('formulaire/', $xfSAS:REST.ee.el.uri)"/>
  <xsl:variable name="xfSAS:REST.ee.el.etudes.uri" select="resolve-uri('etudes/', $xfSAS:REST.ee.el.uri)"/>
  <xsl:variable name="xfSAS:REST.ee.el.pnum.uri" select="resolve-uri('pnum/', $xfSAS:REST.ee.el.uri)"/>

  <!--EE SOURCES-->

  <xsl:variable name="xfSAS:REST.ee.sources.uri" select="resolve-uri('sources/', $xfSAS:REST.ee.uri)"/>
  <xsl:variable name="xfSAS:REST.ee.jurisprudences.uri" select="resolve-uri('jurisprudences/', $xfSAS:REST.ee.sources.uri)"/>
  <xsl:variable name="xfSAS:REST.ee.textes.uri" select="resolve-uri('sources/', $xfSAS:REST.ee.sources.uri)"/>


  <!--REFERENTIALS-->
  <xsl:variable name="xfSAS:REST.referenceTable.uri" select="'http://xmlFirst/referenceTables/'"/>

  <!-- ========================= TEMPLATE =========================-->

  <!--========== MODE "xfSAS:deduplicateRefNodeEntity" ==========-->
  <!--When 2 referenceNode contains the same EE, only keep the last one-->
  
  <xsl:template match="referenceNode/ref/editorialEntity" mode="xfSAS:deduplicateRefNodeEntity">
    <xsl:choose>
      <xsl:when test="following::referenceNode/ref/editorialEntity[@xf:id = current()/@xf:id]">
        <!--Suppression de l'EE en double-->
        <xsl:message>[INFO][xfSAS:deduplicateRefNodeEntity] deduplicate referenceNode to EE id="<xsl:value-of select="@xf:id"/>"</xsl:message>
      </xsl:when>
      <xsl:otherwise>
        <xsl:next-match/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="xfSAS:deduplicateRefNodeEntity">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  
  <!--========== MODE "xfSAS:fixMeta" ==========-->
  <!--Fixing metadata : clean and add calculated metadata from repo "sie-xf-ecm-resources"-->
  
  <!--Delete metadata element if empty -->
  <xsl:template match="*[not(self::editorialEntity)]/metadata[not(*)]" mode="xfSAS:fixMeta" priority="1"/>

  <xsl:template match="editorialEntity/metadata" mode="xfSAS:fixMeta">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="#current"/>
      <xsl:call-template name="xfSAS:addEditorialEntityMeta">
        <xsl:with-param name="editorialEntity" select="parent::editorialEntity"/>
      </xsl:call-template>
    </xsl:copy>
  </xsl:template>
  
  <xsl:template match="editorialEntity[not(metadata)]" mode="xfSAS:fixMeta">
    <xsl:copy>
      <xsl:apply-templates select="@*" mode="#current"/>
      <metadata>
        <xsl:call-template name="xfSAS:addEditorialEntityMeta">
          <xsl:with-param name="editorialEntity" select="."/>
        </xsl:call-template>
      </metadata>
      <xsl:apply-templates mode="#current"/>
    </xsl:copy>
  </xsl:template>
  
  <!--context node must be editorialEntity/metadata-->
  <xsl:template name="xfSAS:addEditorialEntityMeta">
    <xsl:param name="editorialEntity" as="element(editorialEntity)"/>
    <!--Add calculated metadata-->
    <xsl:comment>CALCULATED METADATA</xsl:comment>
    <xsl:variable name="calculatedMetadata" as="element(metadata)">
      <xsl:apply-templates select="$editorialEntity" mode="xfRes:getCalculatedMetadata"/>
    </xsl:variable>
    <xsl:sequence select="$calculatedMetadata/*"/>
    <xsl:comment>GENERATED METADATA</xsl:comment>
    <!--Duplicated metadata are gathered in the same (multivalued) meta element-->
    <xsl:if test="$editorialEntity/metadata">
      <xsl:sequence select="xfRes:groupMultivaluedMeta($editorialEntity/metadata)/*"/>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="node() | @*" mode="xfSAS:fixMeta">
    <xsl:copy>
      <xsl:apply-templates select="node() | @*" mode="#current"/>
    </xsl:copy>
  </xsl:template>

  <!-- ========================= FUNCTION =========================-->

  <xsl:function name="xfSAS:makeHistoricIdAttribute" as="xs:string">
    <xsl:param name="e" as="element()"/>
    <xsl:param name="historicalId" as="xs:string?"/>
    <xsl:choose>
      <xsl:when test="empty($historicalId)">
        <xsl:message>[WARNING] Pas d'id ici (<xsl:value-of select="els:get-xpath($e)"/>)!</xsl:message>
        <xsl:sequence select="''"/>
      </xsl:when>
      <xsl:otherwise>
        <!--<xsl:value-of select="concat('{historicId:', $xfSAS:company, '_', $historicId , '-', generate-id($e), '}')"/>-->
        <xsl:choose>
          <xsl:when test="$xfSAS:company = 'els'">
            <xsl:value-of select="concat('{historicalId:', $historicalId , '}')"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="concat('{historicalId:', $xfSAS:company, '_', $historicalId , '}')"/>
          </xsl:otherwise>
        </xsl:choose>        
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:function name="xfSAS:makeQueryIdAttribute" as="xs:string">
    <xsl:param name="e" as="element()"/>
    <xsl:param name="metaValuePair" as="xs:string*"/>
    <!--ex : metaName1=value1, metaName2=value2, ...-->
    <xsl:choose>
      <xsl:when test="empty($metaValuePair)">
        <xsl:message>[WARNING] Aucune paire meta/valeur (<xsl:value-of select="els:get-xpath($e)"/>)!</xsl:message>
        <xsl:sequence select="''"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="result" as="xs:string*">
          <xsl:text>{query:</xsl:text>
          <xsl:value-of select="$metaValuePair" separator="&amp;"/>
          <xsl:text>}</xsl:text>
        </xsl:variable>
        <xsl:sequence select="string-join($result, '')"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:function>

  <xsl:template name="xfSAS:makeXmlModelPi">
    <xsl:value-of select="els:crlf()"/>
    <xsl:comment>
			<xsl:text>&lt;?xml-model type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron" href="</xsl:text>
			<xsl:value-of select="$xfSAS:rel.to.schema"/>
			<xsl:text>editorialEntity.sch"?&gt;</xsl:text>
    </xsl:comment>
    <xsl:value-of select="els:crlf()"/>
    <xsl:processing-instruction name="xml-model">
			<xsl:text>type="application/xml" schematypens="http://purl.oclc.org/dsdl/nvdl/ns/structure/1.0" href="</xsl:text>
			<xsl:value-of select="$xfSAS:rel.to.schema"/>
			<xsl:text>editorialEntity.nvdl"</xsl:text>
		</xsl:processing-instruction>
    <xsl:value-of select="els:crlf()"/>
    <xsl:comment>
			<xsl:text>&lt;?xml-model type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0" href="</xsl:text>
			<xsl:value-of select="$xfSAS:rel.to.schema"/>
			<xsl:text>editorialEntity.rng"?&gt;</xsl:text>
		</xsl:comment>
    <xsl:value-of select="els:crlf()"/>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <p>Recopie des commentaires afin de les garder de step en step</p>
      <p>Besoin : commentaires des éléments &lt;els:log/&gt;</p>
    </xd:desc>    
  </xd:doc>
  <xsl:template match="comment()" mode="#all" priority="2">
    <xsl:copy/>
  </xsl:template>

  <xd:doc>
    <xd:desc>
      <xd:p><xd:b>Récupération du code ownerGroupe</xd:b></xd:p>
      <xd:p>Pour CXML : voir Jira FLAS-75</xd:p>
      <xd:p>Pour DZ : voir ...</xd:p>
    </xd:desc>
    <xd:param>codeTee : le codeTEE de l'editorialEntity</xd:param>
    <xd:return>Attribut ownerGroup avec la bonne valeur.</xd:return>
  </xd:doc>
  <xsl:function name="xfSAS:addAttributeOwnerGroup" as="attribute(ownerGroup)">
    <xsl:param name="codeTee" as="xs:string"/>
    <xsl:attribute name="ownerGroup">
      <xsl:choose>
        <!-- CXML -->
        <xsl:when test="$codeTee='EFL_TEE_bpim' or $codeTee='EFL_TEE_bdp' or $codeTee='EFL_TEE_baf'">
          <xsl:value-of select="'EFL_GRP_polePluridisciplinaire'"/>
        </xsl:when>
        <xsl:when test="$codeTee='EFL_TEE_acp' or $codeTee='EFL_TEE_frs' or $codeTee='EFL_TEE_bpci'">
          <xsl:value-of select="'EFL_GRP_redactionSociale'"/>
        </xsl:when>
        <xsl:when test="$codeTee='EFL_TEE_rjf' or $codeTee='EFL_TEE_frc' or $codeTee='EFL_TEE_frchs' or $codeTee='EFL_TEE_acdaf' or $codeTee='EFL_TEE_fr'">
          <xsl:value-of select="'EFL_GRP_redactionFiscale'"/>
        </xsl:when>
        <xsl:when test="$codeTee='EFL_TEE_brda' or $codeTee='EFL_TEE_brdaguide'">
          <xsl:value-of select="'EFL_GRP_redactionAffaires'"/>
        </xsl:when>
        <!-- DZ -->
        <!-- revues -->
        <xsl:when test="matches($codeTee, 'DZ_TEE_revues\p{Lu}{2,}')">
          <xsl:value-of select="concat('DZ_GRP_revue', tokenize($codeTee, 'DZ_TEE_revues')[2])"/>
        </xsl:when>
        <!-- ouvrages -->
        <xsl:when test="$codeTee = ('DZ_TEE_ouvrages', 'DZ_TEE_ouvragesParagraphe')">
          <xsl:value-of select="'DZ_GRP_ouvrage'"/>
        </xsl:when>
        <!-- encyclo -->
        <xsl:when test="$codeTee = ('DZ_TEE_encycloRubrique', 'DZ_TEE_encycloParagraphe')">
          <xsl:value-of select="'DZ_GRP_encyclopedie'"/>
        </xsl:when>
        <!-- ELS -->
        <xsl:when test="$codeTee='ELS_TEE_jurisprudence'">
          <xsl:value-of select="'ELS_GRP_DSI'"/>
        </xsl:when>
        <!-- Autres maisons ...-->
        <xsl:otherwise>
          <xsl:value-of select="'NO_SPECIFIC_OWNER_GROUP_CODE'"/>
        </xsl:otherwise>
      </xsl:choose>    
    </xsl:attribute>
  </xsl:function>
</xsl:stylesheet>

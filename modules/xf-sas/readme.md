# XML First SAS conversion

## Dependencies

- SIE-LIB-XSL-COMMON
- SIE-XF-MODELS

Install the required projects with ``mvn install`` to generate the 
``catalog.xml`` files and let oXygen access the libraries from the target JARs.


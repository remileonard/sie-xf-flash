<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:test="http://www.jenitennison.com/xslt/unit-test"
                xmlns:x="http://www.jenitennison.com/xslt/xspec"
                xmlns:__x="http://www.w3.org/1999/XSL/TransformAliasAlias"
                xmlns:pkg="http://expath.org/ns/pkg"
                xmlns:impl="urn:x-xspec:compile:xslt:impl"
                xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                version="2.0"
                exclude-result-prefixes="pkg impl">
   <xsl:import href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xf-sas/src/main/xsl/efl/revues_cxml/prodPubli/num1_numberingCXmlEditorialEntity.xsl"/>
   <xsl:import href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/generate-tests-utils.xsl"/>
   <xsl:namespace-alias stylesheet-prefix="__x" result-prefix="xsl"/>
   <xsl:variable name="x:stylesheet-uri"
                 as="xs:string"
                 select="'file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xf-sas/src/main/xsl/efl/revues_cxml/prodPubli/num1_numberingCXmlEditorialEntity.xsl'"/>
   <xsl:output name="x:report" method="xml" indent="yes"/>
   <xsl:template name="x:main">
      <xsl:message>
         <xsl:text>Testing with </xsl:text>
         <xsl:value-of select="system-property('xsl:product-name')"/>
         <xsl:text> </xsl:text>
         <xsl:value-of select="system-property('xsl:product-version')"/>
      </xsl:message>
      <xsl:result-document format="x:report">
         <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/format-xspec-report.xsl"</xsl:processing-instruction>
         <x:report stylesheet="{$x:stylesheet-uri}" date="{current-dateTime()}">
            <xsl:call-template name="x:d2719566e3"/>
            <xsl:call-template name="x:d2719566e69"/>
            <xsl:call-template name="x:d2719566e135"/>
            <xsl:call-template name="x:d2719566e206"/>
         </x:report>
      </xsl:result-document>
   </xsl:template>
   <xsl:template name="x:d2719566e3">
      <xsl:message>Pas de num�rotation</xsl:message>
      <x:scenario>
         <x:label>Pas de num�rotation</x:label>
         <x:context mode="numbering">
            <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                             xmlns:h="http://www.w3.org/1999/xhtml"
                             code="EFL_TEE_brda"
                             xf:id="ID_MAIN">
               <metadata>
                  <meta code="EFL_META_regleNumerotation">
                     <value>
                        <xsl:text>pasDeNumerotation</xsl:text>
                     </value>
                  </meta>
               </metadata>
               <body>
                  <structuralNode xf:id="Id_Struct_10">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref xf:idRef="sommaire"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Les sommaires</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_11">
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                     </structuralNode>
                  </structuralNode>
                  <structuralNode xf:id="Id_Struct_1">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref xf:idRef="corpus"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Le corpus</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_2">
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                     </structuralNode>
                  </structuralNode>
               </body>
            </editorialEntity>
         </x:context>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc" as="document-node()">
               <xsl:document>
                  <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                                   xmlns:h="http://www.w3.org/1999/xhtml"
                                   code="EFL_TEE_brda"
                                   xf:id="ID_MAIN">
                     <metadata>
                        <meta code="EFL_META_regleNumerotation">
                           <value>
                              <xsl:text>pasDeNumerotation</xsl:text>
                           </value>
                        </meta>
                     </metadata>
                     <body>
                        <structuralNode xf:id="Id_Struct_10">
                           <metadata>
                              <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                                 <value as="xf:referenceItemRef">
                                    <ref xf:idRef="sommaire"
                                         xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                         xf:refType="xf:referenceItemRef"/>
                                 </value>
                                 <verbalization>
                                    <h:span>
                                       <xsl:text>Les sommaires</xsl:text>
                                    </h:span>
                                 </verbalization>
                              </meta>
                           </metadata>
                           <structuralNode xf:id="Id_Struct_11">
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                           </structuralNode>
                        </structuralNode>
                        <structuralNode xf:id="Id_Struct_1">
                           <metadata>
                              <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                                 <value as="xf:referenceItemRef">
                                    <ref xf:idRef="corpus"
                                         xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                         xf:refType="xf:referenceItemRef"/>
                                 </value>
                                 <verbalization>
                                    <h:span>
                                       <xsl:text>Le corpus</xsl:text>
                                    </h:span>
                                 </verbalization>
                              </meta>
                           </metadata>
                           <structuralNode xf:id="Id_Struct_2">
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                           </structuralNode>
                        </structuralNode>
                     </body>
                  </editorialEntity>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="impl:context" select="$impl:context-doc/node()"/>
            <xsl:apply-templates select="$impl:context" mode="numbering"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2719566e36">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2719566e36">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>pas de num�rotation</xsl:message>
      <xsl:variable name="impl:expected-doc" as="document-node()">
         <xsl:document>
            <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                             xmlns:h="http://www.w3.org/1999/xhtml"
                             code="EFL_TEE_brda"
                             xf:id="ID_MAIN">
               <metadata>
                  <meta code="EFL_META_regleNumerotation">
                     <value>
                        <xsl:text>pasDeNumerotation</xsl:text>
                     </value>
                  </meta>
               </metadata>
               <body>
                  <structuralNode xf:id="Id_Struct_10">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref xf:idRef="sommaire"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Les sommaires</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_11">
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                     </structuralNode>
                  </structuralNode>
                  <structuralNode xf:id="Id_Struct_1">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref xf:idRef="corpus"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Le corpus</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_2">
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                     </structuralNode>
                  </structuralNode>
               </body>
            </editorialEntity>
         </xsl:document>
      </xsl:variable>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/node()"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>pas de num�rotation</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2719566e69">
      <xsl:message>Num�rotation par num�ro</xsl:message>
      <x:scenario>
         <x:label>Num�rotation par num�ro</x:label>
         <x:context mode="numbering">
            <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                             xmlns:h="http://www.w3.org/1999/xhtml"
                             code="EFL_TEE_brda"
                             xf:id="ID_MAIN">
               <metadata>
                  <meta code="EFL_META_regleNumerotation">
                     <value>
                        <xsl:text>numerotationSurNumero</xsl:text>
                     </value>
                  </meta>
               </metadata>
               <body>
                  <structuralNode xf:id="Id_Struct_10">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="sommaire"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Les sommaires</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_11">
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                     </structuralNode>
                  </structuralNode>
                  <structuralNode xf:id="Id_Struct_1">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="corpus"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Le corpus</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_2">
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                     </structuralNode>
                  </structuralNode>
               </body>
            </editorialEntity>
         </x:context>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc" as="document-node()">
               <xsl:document>
                  <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                                   xmlns:h="http://www.w3.org/1999/xhtml"
                                   code="EFL_TEE_brda"
                                   xf:id="ID_MAIN">
                     <metadata>
                        <meta code="EFL_META_regleNumerotation">
                           <value>
                              <xsl:text>numerotationSurNumero</xsl:text>
                           </value>
                        </meta>
                     </metadata>
                     <body>
                        <structuralNode xf:id="Id_Struct_10">
                           <metadata>
                              <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                                 <value as="xf:referenceItemRef">
                                    <ref idRef="sommaire"
                                         xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                         xf:refType="xf:referenceItemRef"/>
                                 </value>
                                 <verbalization>
                                    <h:span>
                                       <xsl:text>Les sommaires</xsl:text>
                                    </h:span>
                                 </verbalization>
                              </meta>
                           </metadata>
                           <structuralNode xf:id="Id_Struct_11">
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                           </structuralNode>
                        </structuralNode>
                        <structuralNode xf:id="Id_Struct_1">
                           <metadata>
                              <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                                 <value as="xf:referenceItemRef">
                                    <ref idRef="corpus"
                                         xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                         xf:refType="xf:referenceItemRef"/>
                                 </value>
                                 <verbalization>
                                    <h:span>
                                       <xsl:text>Le corpus</xsl:text>
                                    </h:span>
                                 </verbalization>
                              </meta>
                           </metadata>
                           <structuralNode xf:id="Id_Struct_2">
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                           </structuralNode>
                        </structuralNode>
                     </body>
                  </editorialEntity>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="impl:context" select="$impl:context-doc/node()"/>
            <xsl:apply-templates select="$impl:context" mode="numbering"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2719566e102">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2719566e102">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>Num�rotation par num�ro</xsl:message>
      <xsl:variable name="impl:expected-doc" as="document-node()">
         <xsl:document>
            <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                             xmlns:h="http://www.w3.org/1999/xhtml"
                             code="EFL_TEE_brda"
                             xf:id="ID_MAIN">
               <metadata>
                  <meta code="EFL_META_regleNumerotation">
                     <value>
                        <xsl:text>numerotationSurNumero</xsl:text>
                     </value>
                  </meta>
               </metadata>
               <body>
                  <structuralNode xf:id="Id_Struct_10">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="sommaire"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Les sommaires</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_11">
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                     </structuralNode>
                  </structuralNode>
                  <structuralNode xf:id="Id_Struct_1">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="corpus"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Le corpus</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_2">
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus" num="1"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus" num="2"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus" num="3"/>
                     </structuralNode>
                  </structuralNode>
               </body>
            </editorialEntity>
         </xsl:document>
      </xsl:variable>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/node()"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>Num�rotation par num�ro</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2719566e135">
      <xsl:message>Num�rotation par ann�e</xsl:message>
      <x:scenario>
         <x:label>Num�rotation par ann�e</x:label>
         <x:context mode="numbering">
            <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                             xmlns:h="http://www.w3.org/1999/xhtml"
                             code="EFL_TEE_brda"
                             xf:id="ID_MAIN">
               <metadata>
                  <meta code="EFL_META_regleNumerotation">
                     <value>
                        <xsl:text>numerotationParAnnee</xsl:text>
                     </value>
                  </meta>
                  <meta code="EFL_META_numeroPremiereInfo">
                     <value>
                        <xsl:text>102</xsl:text>
                     </value>
                  </meta>
               </metadata>
               <body>
                  <structuralNode xf:id="Id_Struct_10">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="sommaire"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Les sommaires</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_11">
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                     </structuralNode>
                  </structuralNode>
                  <structuralNode xf:id="Id_Struct_1">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="corpus"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Le corpus</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_2">
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                     </structuralNode>
                  </structuralNode>
               </body>
            </editorialEntity>
         </x:context>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc" as="document-node()">
               <xsl:document>
                  <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                                   xmlns:h="http://www.w3.org/1999/xhtml"
                                   code="EFL_TEE_brda"
                                   xf:id="ID_MAIN">
                     <metadata>
                        <meta code="EFL_META_regleNumerotation">
                           <value>
                              <xsl:text>numerotationParAnnee</xsl:text>
                           </value>
                        </meta>
                        <meta code="EFL_META_numeroPremiereInfo">
                           <value>
                              <xsl:text>102</xsl:text>
                           </value>
                        </meta>
                     </metadata>
                     <body>
                        <structuralNode xf:id="Id_Struct_10">
                           <metadata>
                              <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                                 <value as="xf:referenceItemRef">
                                    <ref idRef="sommaire"
                                         xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                         xf:refType="xf:referenceItemRef"/>
                                 </value>
                                 <verbalization>
                                    <h:span>
                                       <xsl:text>Les sommaires</xsl:text>
                                    </h:span>
                                 </verbalization>
                              </meta>
                           </metadata>
                           <structuralNode xf:id="Id_Struct_11">
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                           </structuralNode>
                        </structuralNode>
                        <structuralNode xf:id="Id_Struct_1">
                           <metadata>
                              <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                                 <value as="xf:referenceItemRef">
                                    <ref idRef="corpus"
                                         xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                         xf:refType="xf:referenceItemRef"/>
                                 </value>
                                 <verbalization>
                                    <h:span>
                                       <xsl:text>Le corpus</xsl:text>
                                    </h:span>
                                 </verbalization>
                              </meta>
                           </metadata>
                           <structuralNode xf:id="Id_Struct_2">
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                           </structuralNode>
                        </structuralNode>
                     </body>
                  </editorialEntity>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="impl:context" select="$impl:context-doc/node()"/>
            <xsl:apply-templates select="$impl:context" mode="numbering"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2719566e171">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2719566e171">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>Num�rotation par ann�e</xsl:message>
      <xsl:variable name="impl:expected-doc" as="document-node()">
         <xsl:document>
            <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                             xmlns:h="http://www.w3.org/1999/xhtml"
                             code="EFL_TEE_brda"
                             xf:id="ID_MAIN">
               <metadata>
                  <meta code="EFL_META_regleNumerotation">
                     <value>
                        <xsl:text>numerotationParAnnee</xsl:text>
                     </value>
                  </meta>
                  <meta code="EFL_META_numeroPremiereInfo">
                     <value>
                        <xsl:text>102</xsl:text>
                     </value>
                  </meta>
               </metadata>
               <body>
                  <structuralNode xf:id="Id_Struct_10">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="sommaire"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Les sommaires</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_11">
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                     </structuralNode>
                  </structuralNode>
                  <structuralNode xf:id="Id_Struct_1">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="corpus"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Le corpus</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_2">
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus" num="102"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus" num="103"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus" num="104"/>
                     </structuralNode>
                  </structuralNode>
               </body>
            </editorialEntity>
         </xsl:document>
      </xsl:variable>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/node()"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>Num�rotation par ann�e</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2719566e206">
      <xsl:message>Num�rotation sp�cifique RJF</xsl:message>
      <x:scenario>
         <x:label>Num�rotation sp�cifique RJF</x:label>
         <x:context mode="numbering">
            <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                             xmlns:h="http://www.w3.org/1999/xhtml"
                             code="EFL_TEE_brda"
                             xf:id="ID_MAIN">
               <metadata>
                  <meta code="EFL_META_regleNumerotation">
                     <value>
                        <xsl:text>numerotationParAnnee</xsl:text>
                     </value>
                  </meta>
                  <meta code="EFL_META_numeroPremiereInfo">
                     <value>
                        <xsl:text>102</xsl:text>
                     </value>
                  </meta>
                  <meta code="EFL_META_produitCx">
                     <value>
                        <xsl:text>Revue de Jurisprudence Fiscale</xsl:text>
                     </value>
                     <verbalization>
                        <span xmlns="http://www.w3.org/1999/xhtml">
                           <xsl:text>RJF</xsl:text>
                        </span>
                     </verbalization>
                  </meta>
               </metadata>
               <body>
                  <structuralNode xf:id="Id_Struct_10">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="sommaire"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Les sommaires</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_11">
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                     </structuralNode>
                  </structuralNode>
                  <structuralNode xf:id="Id_Struct_1">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="corpus"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Le corpus</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_2">
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                     </structuralNode>
                     <structuralNode>
                        <metadata>
                           <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                              <value as="xf:referenceItemRef">
                                 <ref idRef="rjDecisions"
                                      xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                      xf:refType="xf:referenceItemRef"/>
                              </value>
                              <verbalization>
                                 <h:span>
                                    <xsl:text>D�cisions RJ</xsl:text>
                                 </h:span>
                              </verbalization>
                           </meta>
                        </metadata>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus">
                           <ref xf:idRef="ID_EE_CIBLE"
                                xf:base="http://xmlFirst/ee/efl/revues/"
                                xf:refType="xf:referenceNode">
                              <editorialEntity code="EFL_TEE_uiCommentaire" xf:id="ID_EE_CIBLE">
                                 <body/>
                                 <relations>
                                    <relation xf:id="d173e21198" type="generated">
                                       <ref xf:targetResType="editorialEntity"
                                            xf:refType="efl:renvoiAutrui"
                                            xf:targetResId="ID_EE_CIBLE"/>
                                    </relation>
                                 </relations>
                              </editorialEntity>
                           </ref>
                        </referenceNode>
                     </structuralNode>
                     <structuralNode>
                        <metadata>
                           <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                              <value as="xf:referenceItemRef">
                                 <ref idRef="rjConclusions"
                                      xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                      xf:refType="xf:referenceItemRef"/>
                              </value>
                              <verbalization>
                                 <h:span>
                                    <xsl:text>Conclusions RJ</xsl:text>
                                 </h:span>
                              </verbalization>
                           </meta>
                        </metadata>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus">
                           <ref xf:idRef="ID_EE"
                                xf:base="http://xmlFirst/ee/efl/revues/"
                                xf:refType="xf:referenceNode">
                              <editorialEntity code="EFL_TEE_uiCommentaire" xf:id="ID_EE">
                                 <body>
                                    <contentNode>
                                       <content>
                                          <infoCommentaire xmlns="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire">
                                             <blocRefDoc>
                                                <refDocEnrichie>
                                                   <renvoiAutrui xf:idRef="d173e21198" xf:refType="xf:relation"/>
                                                </refDocEnrichie>
                                             </blocRefDoc>
                                          </infoCommentaire>
                                       </content>
                                    </contentNode>
                                 </body>
                                 <relations>
                                    <relation xf:id="d173e21198" type="generated">
                                       <ref xf:targetResType="editorialEntity"
                                            xf:refType="efl:renvoiAutrui"
                                            xf:targetResId="ID_EE_CIBLE"/>
                                    </relation>
                                 </relations>
                              </editorialEntity>
                           </ref>
                        </referenceNode>
                     </structuralNode>
                  </structuralNode>
               </body>
            </editorialEntity>
         </x:context>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc" as="document-node()">
               <xsl:document>
                  <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                                   xmlns:h="http://www.w3.org/1999/xhtml"
                                   code="EFL_TEE_brda"
                                   xf:id="ID_MAIN">
                     <metadata>
                        <meta code="EFL_META_regleNumerotation">
                           <value>
                              <xsl:text>numerotationParAnnee</xsl:text>
                           </value>
                        </meta>
                        <meta code="EFL_META_numeroPremiereInfo">
                           <value>
                              <xsl:text>102</xsl:text>
                           </value>
                        </meta>
                        <meta code="EFL_META_produitCx">
                           <value>
                              <xsl:text>Revue de Jurisprudence Fiscale</xsl:text>
                           </value>
                           <verbalization>
                              <span xmlns="http://www.w3.org/1999/xhtml">
                                 <xsl:text>RJF</xsl:text>
                              </span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <body>
                        <structuralNode xf:id="Id_Struct_10">
                           <metadata>
                              <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                                 <value as="xf:referenceItemRef">
                                    <ref idRef="sommaire"
                                         xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                         xf:refType="xf:referenceItemRef"/>
                                 </value>
                                 <verbalization>
                                    <h:span>
                                       <xsl:text>Les sommaires</xsl:text>
                                    </h:span>
                                 </verbalization>
                              </meta>
                           </metadata>
                           <structuralNode xf:id="Id_Struct_11">
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                           </structuralNode>
                        </structuralNode>
                        <structuralNode xf:id="Id_Struct_1">
                           <metadata>
                              <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                                 <value as="xf:referenceItemRef">
                                    <ref idRef="corpus"
                                         xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                         xf:refType="xf:referenceItemRef"/>
                                 </value>
                                 <verbalization>
                                    <h:span>
                                       <xsl:text>Le corpus</xsl:text>
                                    </h:span>
                                 </verbalization>
                              </meta>
                           </metadata>
                           <structuralNode xf:id="Id_Struct_2">
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus"/>
                           </structuralNode>
                           <structuralNode>
                              <metadata>
                                 <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                                    <value as="xf:referenceItemRef">
                                       <ref idRef="rjDecisions"
                                            xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                            xf:refType="xf:referenceItemRef"/>
                                    </value>
                                    <verbalization>
                                       <h:span>
                                          <xsl:text>D�cisions RJ</xsl:text>
                                       </h:span>
                                    </verbalization>
                                 </meta>
                              </metadata>
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus">
                                 <ref xf:idRef="ID_EE_CIBLE"
                                      xf:base="http://xmlFirst/ee/efl/revues/"
                                      xf:refType="xf:referenceNode">
                                    <editorialEntity code="EFL_TEE_uiCommentaire" xf:id="ID_EE_CIBLE">
                                       <body/>
                                       <relations>
                                          <relation xf:id="d173e21198" type="generated">
                                             <ref xf:targetResType="editorialEntity"
                                                  xf:refType="efl:renvoiAutrui"
                                                  xf:targetResId="ID_EE_CIBLE"/>
                                          </relation>
                                       </relations>
                                    </editorialEntity>
                                 </ref>
                              </referenceNode>
                           </structuralNode>
                           <structuralNode>
                              <metadata>
                                 <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                                    <value as="xf:referenceItemRef">
                                       <ref idRef="rjConclusions"
                                            xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                            xf:refType="xf:referenceItemRef"/>
                                    </value>
                                    <verbalization>
                                       <h:span>
                                          <xsl:text>Conclusions RJ</xsl:text>
                                       </h:span>
                                    </verbalization>
                                 </meta>
                              </metadata>
                              <referenceNode code="TEE_revues-EFL-referenceNode-corpus">
                                 <ref xf:idRef="ID_EE"
                                      xf:base="http://xmlFirst/ee/efl/revues/"
                                      xf:refType="xf:referenceNode">
                                    <editorialEntity code="EFL_TEE_uiCommentaire" xf:id="ID_EE">
                                       <body>
                                          <contentNode>
                                             <content>
                                                <infoCommentaire xmlns="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire">
                                                   <blocRefDoc>
                                                      <refDocEnrichie>
                                                         <renvoiAutrui xf:idRef="d173e21198" xf:refType="xf:relation"/>
                                                      </refDocEnrichie>
                                                   </blocRefDoc>
                                                </infoCommentaire>
                                             </content>
                                          </contentNode>
                                       </body>
                                       <relations>
                                          <relation xf:id="d173e21198" type="generated">
                                             <ref xf:targetResType="editorialEntity"
                                                  xf:refType="efl:renvoiAutrui"
                                                  xf:targetResId="ID_EE_CIBLE"/>
                                          </relation>
                                       </relations>
                                    </editorialEntity>
                                 </ref>
                              </referenceNode>
                           </structuralNode>
                        </structuralNode>
                     </body>
                  </editorialEntity>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="impl:context" select="$impl:context-doc/node()"/>
            <xsl:apply-templates select="$impl:context" mode="numbering"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2719566e284">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2719566e284">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>Num�rotation des conclusions</xsl:message>
      <xsl:variable name="impl:expected-doc" as="document-node()">
         <xsl:document>
            <editorialEntity xmlns="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                             xmlns:h="http://www.w3.org/1999/xhtml"
                             code="EFL_TEE_brda"
                             xf:id="ID_MAIN">
               <metadata>
                  <meta code="EFL_META_regleNumerotation">
                     <value>
                        <xsl:text>numerotationParAnnee</xsl:text>
                     </value>
                  </meta>
                  <meta code="EFL_META_numeroPremiereInfo">
                     <value>
                        <xsl:text>102</xsl:text>
                     </value>
                  </meta>
                  <meta code="EFL_META_produitCx">
                     <value>
                        <xsl:text>Revue de Jurisprudence Fiscale</xsl:text>
                     </value>
                     <verbalization>
                        <span xmlns="http://www.w3.org/1999/xhtml">
                           <xsl:text>RJF</xsl:text>
                        </span>
                     </verbalization>
                  </meta>
               </metadata>
               <body>
                  <structuralNode xf:id="Id_Struct_10">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="sommaire"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Les sommaires</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_11">
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-sommaire"/>
                     </structuralNode>
                  </structuralNode>
                  <structuralNode xf:id="Id_Struct_1">
                     <metadata>
                        <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                           <value as="xf:referenceItemRef">
                              <ref idRef="corpus"
                                   xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                   xf:refType="xf:referenceItemRef"/>
                           </value>
                           <verbalization>
                              <h:span>
                                 <xsl:text>Le corpus</xsl:text>
                              </h:span>
                           </verbalization>
                        </meta>
                     </metadata>
                     <structuralNode xf:id="Id_Struct_2">
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus" num="102"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus" num="103"/>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus" num="104"/>
                     </structuralNode>
                     <structuralNode>
                        <metadata>
                           <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                              <value as="xf:referenceItemRef">
                                 <ref idRef="rjDecisions"
                                      xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                      xf:refType="xf:referenceItemRef"/>
                              </value>
                              <verbalization>
                                 <h:span>
                                    <xsl:text>D�cisions RJ</xsl:text>
                                 </h:span>
                              </verbalization>
                           </meta>
                        </metadata>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus" num="105">
                           <ref xf:idRef="ID_EE_CIBLE"
                                xf:base="http://xmlFirst/ee/efl/revues/"
                                xf:refType="xf:referenceNode">
                              <editorialEntity code="EFL_TEE_uiCommentaire" xf:id="ID_EE_CIBLE">
                                 <body/>
                                 <relations>
                                    <relation xf:id="d173e21198" type="generated">
                                       <ref xf:targetResType="editorialEntity"
                                            xf:refType="efl:renvoiAutrui"
                                            xf:targetResId="ID_EE_CIBLE"/>
                                    </relation>
                                 </relations>
                              </editorialEntity>
                           </ref>
                        </referenceNode>
                     </structuralNode>
                     <structuralNode>
                        <metadata>
                           <meta as="xf:referenceItemRef" code="EFL_META_marqueurStructure">
                              <value as="xf:referenceItemRef">
                                 <ref idRef="rjConclusions"
                                      xf:targetResId="EFL_META_marqueurRubriqueStruct"
                                      xf:refType="xf:referenceItemRef"/>
                              </value>
                              <verbalization>
                                 <h:span>
                                    <xsl:text>Conclusions RJ</xsl:text>
                                 </h:span>
                              </verbalization>
                           </meta>
                        </metadata>
                        <referenceNode code="TEE_revues-EFL-referenceNode-corpus" num="C�105">
                           <ref xf:idRef="ID_EE"
                                xf:base="http://xmlFirst/ee/efl/revues/"
                                xf:refType="xf:referenceNode">
                              <editorialEntity code="EFL_TEE_uiCommentaire" xf:id="ID_EE">
                                 <body>
                                    <contentNode>
                                       <content>
                                          <infoCommentaire xmlns="http://www.lefebvre-sarrut.eu/ns/efl/infoCommentaire">
                                             <blocRefDoc>
                                                <refDocEnrichie>
                                                   <renvoiAutrui xf:idRef="d173e21198" xf:refType="xf:relation"/>
                                                </refDocEnrichie>
                                             </blocRefDoc>
                                          </infoCommentaire>
                                       </content>
                                    </contentNode>
                                 </body>
                                 <relations>
                                    <relation xf:id="d173e21198" type="generated">
                                       <ref xf:targetResType="editorialEntity"
                                            xf:refType="efl:renvoiAutrui"
                                            xf:targetResId="ID_EE_CIBLE"/>
                                    </relation>
                                 </relations>
                              </editorialEntity>
                           </ref>
                        </referenceNode>
                     </structuralNode>
                  </structuralNode>
               </body>
            </editorialEntity>
         </xsl:document>
      </xsl:variable>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/node()"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>Num�rotation des conclusions</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
</xsl:stylesheet>

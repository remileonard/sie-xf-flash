<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:test="http://www.jenitennison.com/xslt/unit-test"
                xmlns:x="http://www.jenitennison.com/xslt/xspec"
                xmlns:__x="http://www.w3.org/1999/XSL/TransformAliasAlias"
                xmlns:pkg="http://expath.org/ns/pkg"
                xmlns:impl="urn:x-xspec:compile:xslt:impl"
                xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                version="2.0"
                exclude-result-prefixes="pkg impl">
   <xsl:import href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xf-sas/src/main/xsl/el/fichesQR-EL2editorialEntity.xsl"/>
   <xsl:import href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/generate-tests-utils.xsl"/>
   <xsl:namespace-alias stylesheet-prefix="__x" result-prefix="xsl"/>
   <xsl:variable name="x:stylesheet-uri"
                 as="xs:string"
                 select="'file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xf-sas/src/main/xsl/el/fichesQR-EL2editorialEntity.xsl'"/>
   <xsl:output name="x:report" method="xml" indent="yes"/>
   <xsl:param select="'el'" name="xfSAS:company"/>
   <xsl:param select="'../files/X1324_FP_test1.xml'" name="inputFilename"/>
   <xsl:template name="x:main">
      <xsl:message>
         <xsl:text>Testing with </xsl:text>
         <xsl:value-of select="system-property('xsl:product-name')"/>
         <xsl:text> </xsl:text>
         <xsl:value-of select="system-property('xsl:product-version')"/>
      </xsl:message>
      <xsl:result-document format="x:report">
         <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/format-xspec-report.xsl"</xsl:processing-instruction>
         <x:report stylesheet="{$x:stylesheet-uri}" date="{current-dateTime()}">
            <xsl:call-template name="x:d2721747e4"/>
         </x:report>
      </xsl:result-document>
   </xsl:template>
   <xsl:template name="x:d2721747e4">
      <xsl:message>Scenario for testing template with match '/QR''</xsl:message>
      <x:scenario>
         <x:label>Scenario for testing template with match '/QR''</x:label>
         <x:context>
            <QR GB="GB25"
                MR="On"
                Position="Corps"
                Maj="17-01"
                Eddversion="QR_EDD19">
               <INTROQR>
                  <AL>
                     <xsl:text>INTROQR</xsl:text>
                  </AL>
               </INTROQR>
               <Tiqr>
                  <xsl:text>TIQR1</xsl:text>
               </Tiqr>
               <Al>
                  <xsl:text>Al1</xsl:text>
               </Al>
               <Tiqr>
                  <xsl:text>TIQR2</xsl:text>
               </Tiqr>
               <Tiqr>
                  <xsl:text>TIQR3</xsl:text>
               </Tiqr>
               <Tiqr>
                  <xsl:text>TIQR4</xsl:text>
               </Tiqr>
               <Quest>
                  <xsl:text>Question</xsl:text>
               </Quest>
               <Al>
                  <xsl:text>Al2</xsl:text>
               </Al>
               <Quest>
                  <xsl:text>Question2</xsl:text>
               </Quest>
            </QR>
         </x:context>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc" as="document-node()">
               <xsl:document>
                  <QR GB="GB25"
                      MR="On"
                      Position="Corps"
                      Maj="17-01"
                      Eddversion="QR_EDD19">
                     <INTROQR>
                        <AL>
                           <xsl:text>INTROQR</xsl:text>
                        </AL>
                     </INTROQR>
                     <Tiqr>
                        <xsl:text>TIQR1</xsl:text>
                     </Tiqr>
                     <Al>
                        <xsl:text>Al1</xsl:text>
                     </Al>
                     <Tiqr>
                        <xsl:text>TIQR2</xsl:text>
                     </Tiqr>
                     <Tiqr>
                        <xsl:text>TIQR3</xsl:text>
                     </Tiqr>
                     <Tiqr>
                        <xsl:text>TIQR4</xsl:text>
                     </Tiqr>
                     <Quest>
                        <xsl:text>Question</xsl:text>
                     </Quest>
                     <Al>
                        <xsl:text>Al2</xsl:text>
                     </Al>
                     <Quest>
                        <xsl:text>Question2</xsl:text>
                     </Quest>
                  </QR>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="impl:context" select="$impl:context-doc/node()"/>
            <xsl:apply-templates select="$impl:context"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721747e26">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721747e27">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721747e28">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721747e29">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721747e30">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721747e31">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721747e44">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721747e51">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721747e53">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721747e26">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>editorialEntity</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="exists(/xf:editorialEntity)" version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="exists(/xf:editorialEntity)" version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>editorialEntity</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721747e27">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>code editorialEntity</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="/xf:editorialEntity/@code='EL_TEE_ficheQR'" version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="/xf:editorialEntity/@code='EL_TEE_ficheQR'" version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>code editorialEntity</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721747e28">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>metaData</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="exists(/xf:editorialEntity/xf:metadata)" version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="exists(/xf:editorialEntity/xf:metadata)" version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>metaData</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721747e29">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>systemTitle</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="exists(/xf:editorialEntity/xf:metadata/xf:meta[@code='systemTitle']/xf:value/xhtml:div[@class='Tiqr'])"
                                version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="exists(/xf:editorialEntity/xf:metadata/xf:meta[@code='systemTitle']/xf:value/xhtml:div[@class='Tiqr'])"
                             version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>systemTitle</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721747e30">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>systemAbstract</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="exists(/xf:editorialEntity/xf:metadata/xf:meta[@code='systemAbstract'])"
                                version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="exists(/xf:editorialEntity/xf:metadata/xf:meta[@code='systemAbstract'])"
                             version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>systemAbstract</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721747e31">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>contenu systemTitle</xsl:message>
      <xsl:variable name="impl:expected-doc" as="document-node()">
         <xsl:document>
            <div xmlns="http://www.w3.org/1999/xhtml" class="Tiqr">
               <span class="texte-Tiqr texte">
                  <xsl:text>TIQR1</xsl:text>
               </span>
            </div>
            <div xmlns="http://www.w3.org/1999/xhtml" class="Tiqr">
               <span class="texte-Tiqr texte">
                  <xsl:text>TIQR2</xsl:text>
               </span>
            </div>
            <div xmlns="http://www.w3.org/1999/xhtml" class="Tiqr">
               <span class="texte-Tiqr texte">
                  <xsl:text>TIQR3</xsl:text>
               </span>
            </div>
            <div xmlns="http://www.w3.org/1999/xhtml" class="Tiqr">
               <span class="texte-Tiqr texte">
                  <xsl:text>TIQR4</xsl:text>
               </span>
            </div>
         </xsl:document>
      </xsl:variable>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/node()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="/xf:editorialEntity/xf:metadata/xf:meta[@code='systemTitle']/xf:value/*"
                                version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="/xf:editorialEntity/xf:metadata/xf:meta[@code='systemTitle']/xf:value/*"
                             version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>contenu systemTitle</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721747e44">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>contenu systemAbstract</xsl:message>
      <xsl:variable name="impl:expected-doc" as="document-node()">
         <xsl:document>
            <xf:meta xmlns="http://www.w3.org/1999/xhtml" code="systemAbstract">
               <xf:value>
                  <div class="INTROQR">
                     <div class="AL">
                        <span class="texte-AL texte">
                           <xsl:text>INTROQR</xsl:text>
                        </span>
                     </div>
                  </div>
               </xf:value>
            </xf:meta>
         </xsl:document>
      </xsl:variable>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/node()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="/xf:editorialEntity/xf:metadata/xf:meta[@code='systemAbstract']"
                                version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="/xf:editorialEntity/xf:metadata/xf:meta[@code='systemAbstract']"
                             version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>contenu systemAbstract</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721747e51">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>body</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="exists(/xf:editorialEntity/xf:body)" version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="exists(/xf:editorialEntity/xf:body)" version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>body</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721747e53">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>namespace content</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="count(distinct-values(/xf:editorialEntity/xf:body/xf:contentNode/xf:content//*/namespace-uri(.))) = 1  and distinct-values(/xf:editorialEntity/xf:body/xf:contentNode/xf:content//*/namespace-uri(.)) = 'http://www.lefebvre-sarrut.eu/ns/el/fichesQR_FP'"
                                version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="count(distinct-values(/xf:editorialEntity/xf:body/xf:contentNode/xf:content//*/namespace-uri(.))) = 1  and distinct-values(/xf:editorialEntity/xf:body/xf:contentNode/xf:content//*/namespace-uri(.)) = 'http://www.lefebvre-sarrut.eu/ns/el/fichesQR_FP'"
                             version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>namespace content</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
</xsl:stylesheet>

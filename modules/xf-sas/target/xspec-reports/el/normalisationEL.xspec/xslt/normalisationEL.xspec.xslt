<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:test="http://www.jenitennison.com/xslt/unit-test"
                xmlns:x="http://www.jenitennison.com/xslt/xspec"
                xmlns:__x="http://www.w3.org/1999/XSL/TransformAliasAlias"
                xmlns:pkg="http://expath.org/ns/pkg"
                xmlns:impl="urn:x-xspec:compile:xslt:impl"
                xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
                xmlns:EL="http://www.lefebvre-sarrut.eu/ns/el"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                version="2.0"
                exclude-result-prefixes="pkg impl">
   <xsl:import href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xf-sas/src/main/xsl/el/fichesGB-EL2editorialEntity.xsl"/>
   <xsl:import href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/generate-tests-utils.xsl"/>
   <xsl:namespace-alias stylesheet-prefix="__x" result-prefix="xsl"/>
   <xsl:variable name="x:stylesheet-uri"
                 as="xs:string"
                 select="'file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xf-sas/src/main/xsl/el/fichesGB-EL2editorialEntity.xsl'"/>
   <xsl:output name="x:report" method="xml" indent="yes"/>
   <xsl:param select="'no_value'" name="xfSAS:company"/>
   <xsl:template name="x:main">
      <xsl:message>
         <xsl:text>Testing with </xsl:text>
         <xsl:value-of select="system-property('xsl:product-name')"/>
         <xsl:text> </xsl:text>
         <xsl:value-of select="system-property('xsl:product-version')"/>
      </xsl:message>
      <xsl:result-document format="x:report">
         <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/format-xspec-report.xsl"</xsl:processing-instruction>
         <x:report stylesheet="{$x:stylesheet-uri}" date="{current-dateTime()}">
            <xsl:call-template name="x:d2721859e3"/>
            <xsl:call-template name="x:d2721859e117"/>
            <xsl:call-template name="x:d2721859e125"/>
         </x:report>
      </xsl:result-document>
   </xsl:template>
   <xsl:template name="x:d2721859e3">
      <xsl:message>Scenario for testing function normalizeType</xsl:message>
      <x:scenario>
         <x:label>Scenario for testing function normalizeType</x:label>
         <xsl:call-template name="x:d2721859e4"/>
         <xsl:call-template name="x:d2721859e11"/>
         <xsl:call-template name="x:d2721859e18"/>
         <xsl:call-template name="x:d2721859e25"/>
         <xsl:call-template name="x:d2721859e32"/>
         <xsl:call-template name="x:d2721859e39"/>
         <xsl:call-template name="x:d2721859e46"/>
         <xsl:call-template name="x:d2721859e53"/>
         <xsl:call-template name="x:d2721859e60"/>
         <xsl:call-template name="x:d2721859e67"/>
         <xsl:call-template name="x:d2721859e74"/>
         <xsl:call-template name="x:d2721859e82"/>
         <xsl:call-template name="x:d2721859e89"/>
         <xsl:call-template name="x:d2721859e96"/>
         <xsl:call-template name="x:d2721859e103"/>
         <xsl:call-template name="x:d2721859e110"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e4">
      <xsl:message>..ART</xsl:message>
      <x:scenario>
         <x:label>ART</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'ART'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'ART'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e10">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e10">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'article'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e11">
      <xsl:message>..L</xsl:message>
      <x:scenario>
         <x:label>L</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'L'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'L'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e17">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e17">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'article'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e18">
      <xsl:message>..R</xsl:message>
      <x:scenario>
         <x:label>R</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'R'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'R'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e24">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e24">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'article'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e25">
      <xsl:message>..D</xsl:message>
      <x:scenario>
         <x:label>D</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'D'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'D'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e31">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e31">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'article'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e32">
      <xsl:message>..Circ</xsl:message>
      <x:scenario>
         <x:label>Circ</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'Circ'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'Circ'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e38">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e38">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'Circ'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e39">
      <xsl:message>..Arr</xsl:message>
      <x:scenario>
         <x:label>Arr</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'Arr'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'Arr'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e45">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e45">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'Arr'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e46">
      <xsl:message>..Cass. soc.</xsl:message>
      <x:scenario>
         <x:label>Cass. soc.</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'Cass. soc.'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'Cass. soc.'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e52">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e52">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'Cass'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e53">
      <xsl:message>..Cass. autres.</xsl:message>
      <x:scenario>
         <x:label>Cass. autres.</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'Cass. autres.'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'Cass. autres.'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e59">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e59">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'Cass'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e60">
      <xsl:message>..R�p</xsl:message>
      <x:scenario>
         <x:label>R�p</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'R�p'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'R�p'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e66">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e66">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'R�p'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e67">
      <xsl:message>..R�p</xsl:message>
      <x:scenario>
         <x:label>R�p</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'R�p'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'R�p'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e73">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e73">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'R�p'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e74">
      <xsl:message>..R�p</xsl:message>
      <x:scenario>
         <x:label>R�p</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'R�p'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'R�p'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e80">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e80">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'R�p'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e82">
      <xsl:message>..CA</xsl:message>
      <x:scenario>
         <x:label>CA</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'CA Autres'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'CA Autres'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e88">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e88">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'CA'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e89">
      <xsl:message>..CAA</xsl:message>
      <x:scenario>
         <x:label>CAA</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'CAA'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'CAA'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e95">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e95">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'CA'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e96">
      <xsl:message>..ANI</xsl:message>
      <x:scenario>
         <x:label>ANI</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'ANI Autres'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'ANI Autres'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e102">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e102">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'ANI'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e103">
      <xsl:message>..ANIA</xsl:message>
      <x:scenario>
         <x:label>ANIA</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'ANIA'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'ANIA'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e109">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e109">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'ANI'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e110">
      <xsl:message>..Autre</xsl:message>
      <x:scenario>
         <x:label>Autre</x:label>
         <x:call function="EL:normalizeType">
            <x:param name="e">
               <test>
                  <xsl:text>erreur</xsl:text>
               </test>
            </x:param>
            <x:param name="type" select="'Autre'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="e-doc" as="document-node()">
               <xsl:document>
                  <test>
                     <xsl:text>erreur</xsl:text>
                  </test>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="e" select="$e-doc/node()"/>
            <xsl:variable select="'Autre'" name="type"/>
            <xsl:sequence select="EL:normalizeType($e, $type)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e116">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e116">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>''</xsl:message>
      <xsl:variable select="'Autre'" name="impl:expected"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>''</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e117">
      <xsl:message>Scenario for testing function createMetadataFromString</xsl:message>
      <x:scenario>
         <x:label>Scenario for testing function createMetadataFromString</x:label>
         <x:call function="EL:createMetadataFromString">
            <x:param name="prefix" select="'p'"/>
            <x:param name="name" select="'name'"/>
            <x:param name="value" select="'val'"/>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable select="'p'" name="prefix"/>
            <xsl:variable select="'name'" name="name"/>
            <xsl:variable select="'val'" name="value"/>
            <xsl:sequence select="EL:createMetadataFromString($prefix, $name, $value)"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e122">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e123">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e124">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e122">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>meta</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="exists($x:result[self::xf:meta])" version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="exists($x:result[self::xf:meta])" version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>meta</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e123">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>code</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="$x:result[self::xf:meta]/@code = 'p.name'" version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result[self::xf:meta]/@code = 'p.name'" version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>code</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e124">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>meta</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="$x:result[self::xf:meta]/xf:value = 'val'" version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result[self::xf:meta]/xf:value = 'val'" version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>meta</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e125">
      <xsl:message>Scenario for testing function createMetadataFromElement</xsl:message>
      <x:scenario>
         <x:label>Scenario for testing function createMetadataFromElement</x:label>
         <x:call template="EL:createMetadataFromElement">
            <x:param name="prefix" select="'p'"/>
            <x:param name="name" select="'name'"/>
            <x:param name="element">
               <element att="attValue">
                  <fils>
                     <xsl:text>fils de element</xsl:text>
                  </fils>
               </element>
            </x:param>
         </x:call>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable select="'p'" name="prefix"/>
            <xsl:variable select="'name'" name="name"/>
            <xsl:variable name="element-doc" as="document-node()">
               <xsl:document>
                  <element att="attValue">
                     <fils>
                        <xsl:text>fils de element</xsl:text>
                     </fils>
                  </element>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="element" select="$element-doc/node()"/>
            <xsl:call-template name="EL:createMetadataFromElement">
               <xsl:with-param name="prefix" select="$prefix"/>
               <xsl:with-param name="name" select="$name"/>
               <xsl:with-param name="element" select="$element"/>
            </xsl:call-template>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e133">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e134">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721859e135">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721859e133">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>meta</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="exists($x:result[self::xf:meta])" version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="exists($x:result[self::xf:meta])" version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>meta</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e134">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>code</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="$x:result[self::xf:meta]/@code = 'p.name'" version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result[self::xf:meta]/@code = 'p.name'" version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>code</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721859e135">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>value</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="$x:result[self::xf:meta]/xf:value/xhtml:div/@class = 'element'"
                                version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result[self::xf:meta]/xf:value/xhtml:div/@class = 'element'"
                             version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>value</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
</xsl:stylesheet>

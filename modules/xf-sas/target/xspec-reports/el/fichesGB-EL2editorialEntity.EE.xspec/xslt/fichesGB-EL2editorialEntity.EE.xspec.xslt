<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:test="http://www.jenitennison.com/xslt/unit-test"
                xmlns:x="http://www.jenitennison.com/xslt/xspec"
                xmlns:__x="http://www.w3.org/1999/XSL/TransformAliasAlias"
                xmlns:pkg="http://expath.org/ns/pkg"
                xmlns:impl="urn:x-xspec:compile:xslt:impl"
                xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                xmlns:xhtml="http://www.w3.org/1999/xhtml"
                version="2.0"
                exclude-result-prefixes="pkg impl">
   <xsl:import href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xf-sas/src/main/xsl/el/fichesGB-EL2editorialEntity.xsl"/>
   <xsl:import href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/generate-tests-utils.xsl"/>
   <xsl:namespace-alias stylesheet-prefix="__x" result-prefix="xsl"/>
   <xsl:variable name="x:stylesheet-uri"
                 as="xs:string"
                 select="'file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xf-sas/src/main/xsl/el/fichesGB-EL2editorialEntity.xsl'"/>
   <xsl:output name="x:report" method="xml" indent="yes"/>
   <xsl:param select="'el'" name="xfSAS:company"/>
   <xsl:param select="'../files/X1324_FP_test1.xml'" name="inputFilename"/>
   <xsl:template name="x:main">
      <xsl:message>
         <xsl:text>Testing with </xsl:text>
         <xsl:value-of select="system-property('xsl:product-name')"/>
         <xsl:text> </xsl:text>
         <xsl:value-of select="system-property('xsl:product-version')"/>
      </xsl:message>
      <xsl:result-document format="x:report">
         <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/format-xspec-report.xsl"</xsl:processing-instruction>
         <x:report stylesheet="{$x:stylesheet-uri}" date="{current-dateTime()}">
            <xsl:call-template name="x:d2721102e5"/>
            <xsl:call-template name="x:d2721102e87"/>
         </x:report>
      </xsl:result-document>
   </xsl:template>
   <xsl:template name="x:d2721102e5">
      <xsl:message>PENDING: (a revoir) Scenario for testing template with match '/FPRO |/FP | /DPFORM2 |/QR' and mode 'xfSAS:fichesGB-EL2editorialEntity.EE'</xsl:message>
      <x:scenario pending="a revoir">
         <x:label>Scenario for testing template with match '/FPRO |/FP | /DPFORM2 |/QR' and mode 'xfSAS:fichesGB-EL2editorialEntity.EE'</x:label>
         <x:context mode="xfSAS:fichesGB-EL2editorialEntity.EE">
            <FP GB="GB25"
                MR="On"
                Titre="2"
                Theme="IDFPT02"
                Position="Corps"
                Maj="17-06"
                Eddversion="GB59_FP_EDD_47">
               <Tifp>
                  <xsl:text>Tifp1</xsl:text>
               </Tifp>
               <INTROFP>
                  <Al>
                     <Revision id="test">
                        <xsl:text>Test Revision</xsl:text>
                     </Revision>
                  </Al>
               </INTROFP>
               <Refcode Type="ART" Code="PXCT2">
                  <Xacode Num="6111-6" Type="L" fmcssattr="15"/>
               </Refcode>
               <Reftexte>
                  <xsl:text>Ref1</xsl:text>
                  <Fixe Idbr="687760">
                     <xsl:text>Test Fixe</xsl:text>
                  </Fixe>
                  <xsl:text>Ref 2</xsl:text>
               </Reftexte>
               <PAPL>
                  <Al>
                     <xsl:text>Al1</xsl:text>
                     <Renvoi Type="FP" Inline="Oui">
                        <Fiche Idref="X5F034"
                               Partie="pratique"
                               Numfiche="35"
                               Nbr="SING"
                               fmcssattr="6"/>
                     </Renvoi>
                     <xsl:text>.</xsl:text>
                  </Al>
                  <Al>
                     <xsl:text>Al2</xsl:text>
                  </Al>
               </PAPL>
               <Tiobs>
                  <xsl:text>Tiobs1</xsl:text>
               </Tiobs>
               <OBS>
                  <Al>
                     <xsl:text>Test OBS</xsl:text>
                  </Al>
                  <Al>
                     <xsl:text>test OBS2</xsl:text>
                  </Al>
               </OBS>
               <Al>
                  <xsl:text>Al3</xsl:text>
               </Al>
            </FP>
         </x:context>
         <xsl:call-template name="x:d2721102e38"/>
         <xsl:call-template name="x:d2721102e39"/>
         <xsl:call-template name="x:d2721102e40"/>
         <xsl:call-template name="x:d2721102e41"/>
         <xsl:call-template name="x:d2721102e42"/>
         <xsl:call-template name="x:d2721102e43"/>
         <xsl:call-template name="x:d2721102e44"/>
         <xsl:call-template name="x:d2721102e45"/>
         <xsl:call-template name="x:d2721102e73"/>
         <xsl:call-template name="x:d2721102e82"/>
         <xsl:call-template name="x:d2721102e85"/>
         <xsl:call-template name="x:d2721102e86"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721102e38">
      <xsl:message>PENDING: (a revoir) editorialEntity</xsl:message>
      <x:test pending="a revoir">
         <x:label>editorialEntity</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e39">
      <xsl:message>PENDING: (a revoir) metaData</xsl:message>
      <x:test pending="a revoir">
         <x:label>metaData</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e40">
      <xsl:message>PENDING: (a revoir) pas systemAbstract</xsl:message>
      <x:test pending="a revoir">
         <x:label>pas systemAbstract</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e41">
      <xsl:message>PENDING: (a revoir) pas systemTitle</xsl:message>
      <x:test pending="a revoir">
         <x:label>pas systemTitle</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e42">
      <xsl:message>PENDING: (a revoir) texte systemGroupeProprietaire</xsl:message>
      <x:test pending="a revoir">
         <x:label>texte systemGroupeProprietaire</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e43">
      <xsl:message>PENDING: (a revoir) EL_META_observations</xsl:message>
      <x:test pending="a revoir">
         <x:label>EL_META_observations</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e44">
      <xsl:message>PENDING: (a revoir) contenu EL_META_observations</xsl:message>
      <x:test pending="a revoir">
         <x:label>contenu EL_META_observations</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e45">
      <xsl:message>PENDING: (a revoir) contenu EL_META_papl</xsl:message>
      <x:test pending="a revoir">
         <x:label>contenu EL_META_papl</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e73">
      <xsl:message>PENDING: (a revoir) contenu EL_META_resume</xsl:message>
      <x:test pending="a revoir">
         <x:label>contenu EL_META_resume</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e82">
      <xsl:message>PENDING: (a revoir) body</xsl:message>
      <x:test pending="a revoir">
         <x:label>body</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e85">
      <xsl:message>PENDING: (a revoir) namespace content</xsl:message>
      <x:test pending="a revoir">
         <x:label>namespace content</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e86">
      <xsl:message>PENDING: (a revoir) contenu content</xsl:message>
      <x:test pending="a revoir">
         <x:label>contenu content</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2721102e87">
      <xsl:message>Scenario for testing template with match '@id' and mode 'xfSAS:fichesGB-EL2editorialEntity.EE'</xsl:message>
      <x:scenario>
         <x:label>Scenario for testing template with match '@id' and mode 'xfSAS:fichesGB-EL2editorialEntity.EE'</x:label>
         <x:context mode="xfSAS:fichesGB-EL2editorialEntity.EE">
            <test id="testId" essai="test"/>
         </x:context>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc" as="document-node()">
               <xsl:document>
                  <test id="testId" essai="test"/>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="impl:context" select="$impl:context-doc/node()"/>
            <xsl:apply-templates select="$impl:context" mode="xfSAS:fichesGB-EL2editorialEntity.EE"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2721102e90">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2721102e90">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>historic id</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="string($x:result/@xf:id) = '{historicalId:el_testId}' and string($x:result/@id) = 'testId'"
                                version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="string($x:result/@xf:id) = '{historicalId:el_testId}' and string($x:result/@id) = 'testId'"
                             version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>historic id</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
</xsl:stylesheet>

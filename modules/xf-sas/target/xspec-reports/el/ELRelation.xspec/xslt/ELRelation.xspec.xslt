<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:test="http://www.jenitennison.com/xslt/unit-test"
                xmlns:x="http://www.jenitennison.com/xslt/xspec"
                xmlns:__x="http://www.w3.org/1999/XSL/TransformAliasAlias"
                xmlns:pkg="http://expath.org/ns/pkg"
                xmlns:impl="urn:x-xspec:compile:xslt:impl"
                xmlns:xfSAS="http://www.lefebvre-sarrut.eu/ns/xmlfirst/sas"
                xmlns:xf="http://www.lefebvre-sarrut.eu/ns/xmlfirst"
                version="2.0"
                exclude-result-prefixes="pkg impl">
   <xsl:import href="file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xf-sas/src/main/xsl/el/fichesGB-EL2editorialEntity.xsl"/>
   <xsl:import href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/generate-tests-utils.xsl"/>
   <xsl:namespace-alias stylesheet-prefix="__x" result-prefix="xsl"/>
   <xsl:variable name="x:stylesheet-uri"
                 as="xs:string"
                 select="'file:/D:/FLASH/build/git/SIE-XMLFirst/sie-xf-flash/modules/xf-sas/src/main/xsl/el/fichesGB-EL2editorialEntity.xsl'"/>
   <xsl:output name="x:report" method="xml" indent="yes"/>
   <xsl:param select="'el'" name="xfSAS:company"/>
   <xsl:param select="doc('../files/X1324_FP_test1.xml')" name="docInput"/>
   <xsl:param select="'../files/X1324_FP_test1.xml'" name="inputFilename"/>
   <xsl:template name="x:main">
      <xsl:message>
         <xsl:text>Testing with </xsl:text>
         <xsl:value-of select="system-property('xsl:product-name')"/>
         <xsl:text> </xsl:text>
         <xsl:value-of select="system-property('xsl:product-version')"/>
      </xsl:message>
      <xsl:result-document format="x:report">
         <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="jar:file:/C:/Users/ext-rleonard/.m2/repository/io/xspec/maven/xspec-maven-plugin/1.5.0-RC5/xspec-maven-plugin-1.5.0-RC5.jar!/xspec/compiler/format-xspec-report.xsl"</xsl:processing-instruction>
         <x:report stylesheet="{$x:stylesheet-uri}" date="{current-dateTime()}">
            <xsl:call-template name="x:d2720830e6"/>
            <xsl:call-template name="x:d2720830e96"/>
            <xsl:call-template name="x:d2720830e99"/>
            <xsl:call-template name="x:d2720830e104"/>
            <xsl:call-template name="x:d2720830e134"/>
            <xsl:call-template name="x:d2720830e140"/>
            <xsl:call-template name="x:d2720830e143"/>
            <xsl:call-template name="x:d2720830e147"/>
            <xsl:call-template name="x:d2720830e150"/>
            <xsl:call-template name="x:d2720830e153"/>
            <xsl:call-template name="x:d2720830e156"/>
            <xsl:call-template name="x:d2720830e159"/>
         </x:report>
      </xsl:result-document>
   </xsl:template>
   <xsl:template name="x:d2720830e6">
      <xsl:message>PENDING: (true) Scenario for testing template with match '*:Refcode | *:Xacodes[not(parent::Refcode)] | *:Refjrp | *:Reftexte/Fixe | *:Renvoiexterne | *:formulaire | *:Renvoi[@Produit='FormDP']/*:Formule' and mode 'makeRelations'</xsl:message>
      <x:scenario pending="true">
         <x:label>Scenario for testing template with match '*:Refcode | *:Xacodes[not(parent::Refcode)] | *:Refjrp | *:Reftexte/Fixe | *:Renvoiexterne | *:formulaire | *:Renvoi[@Produit='FormDP']/*:Formule' and mode 'makeRelations'</x:label>
         <xsl:call-template name="x:d2720830e7"/>
         <xsl:call-template name="x:d2720830e16"/>
         <xsl:call-template name="x:d2720830e23"/>
         <xsl:call-template name="x:d2720830e30"/>
         <xsl:call-template name="x:d2720830e37"/>
         <xsl:call-template name="x:d2720830e46"/>
         <xsl:call-template name="x:d2720830e57"/>
         <xsl:call-template name="x:d2720830e70"/>
         <xsl:call-template name="x:d2720830e87"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e7">
      <xsl:message>PENDING: (true) ..Refjrp</xsl:message>
      <x:scenario pending="true">
         <x:label>Refjrp</x:label>
         <x:context mode="makeRelations">
            <Refjrp>
               <Fixe Idbr="369956">
                  <xsl:text>test fixe</xsl:text>
               </Fixe>
            </Refjrp>
         </x:context>
         <xsl:call-template name="x:d2720830e12"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e12">
      <xsl:message>PENDING: (true) </xsl:message>
      <x:test pending="true">
         <x:label/>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e16">
      <xsl:message>PENDING: (true) ..Refcode</xsl:message>
      <x:scenario pending="true">
         <x:label>Refcode</x:label>
         <x:context mode="makeRelations">
            <Refcode Type="ART" Code="PXCT2">
               <Xacode Num="6111-6" Type="L" fmcssattr="15"/>
            </Refcode>
         </x:context>
         <xsl:call-template name="x:d2720830e20"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e20">
      <xsl:message>PENDING: (true) </xsl:message>
      <x:test pending="true">
         <x:label/>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e23">
      <xsl:message>PENDING: (true) ..Refcode/XaCodes</xsl:message>
      <x:scenario pending="true">
         <x:label>Refcode/XaCodes</x:label>
         <x:context mode="makeRelations">
            <Refcode Type="ART" Code="PXCT2">
               <Xacodes Num="6314-2" Type="L"/>
            </Refcode>
         </x:context>
         <xsl:call-template name="x:d2720830e27"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e27">
      <xsl:message>PENDING: (true) </xsl:message>
      <x:test pending="true">
         <x:label/>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e30">
      <xsl:message>PENDING: (true) ..XaCodes out of Refcode</xsl:message>
      <x:scenario pending="true">
         <x:label>XaCodes out of Refcode</x:label>
         <x:context mode="makeRelations">
            <test Type="ART" Code="PXCT2">
               <Xacodes Num="6314-2" Type="L"/>
            </test>
         </x:context>
         <xsl:call-template name="x:d2720830e34"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e34">
      <xsl:message>PENDING: (true) </xsl:message>
      <x:test pending="true">
         <x:label/>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e37">
      <xsl:message>PENDING: (true) ..Refjrp</xsl:message>
      <x:scenario pending="true">
         <x:label>Refjrp</x:label>
         <x:context mode="makeRelations">
            <Refjrp>
               <Fixe Idbr="369956">
                  <xsl:text>test fixe</xsl:text>
               </Fixe>
            </Refjrp>
         </x:context>
         <xsl:call-template name="x:d2720830e42"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e42">
      <xsl:message>PENDING: (true) </xsl:message>
      <x:test pending="true">
         <x:label/>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e46">
      <xsl:message>PENDING: (true) ..Reftexte/Fixe</xsl:message>
      <x:scenario pending="true">
         <x:label>Reftexte/Fixe</x:label>
         <x:context mode="makeRelations">
            <Reftexte>
               <Fixe Idbr="755140">
                  <xsl:text>TEST</xsl:text>
               </Fixe>
               <xsl:text>AAAA</xsl:text>
            </Reftexte>
         </x:context>
         <xsl:call-template name="x:d2720830e52"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e52">
      <xsl:message>PENDING: (true) </xsl:message>
      <x:test pending="true">
         <x:label/>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e57">
      <xsl:message>PENDING: (true) ..Renvoiexterne</xsl:message>
      <x:scenario pending="true">
         <x:label>Renvoiexterne</x:label>
         <x:context mode="makeRelations">
            <Renvoiexterne Type="1" Typemaj="0">
               <Saisie>
                  <xsl:text>Test 1</xsl:text>
               </Saisie>
               <Nomet Idref="Z2012">
                  <xsl:text>Test 2</xsl:text>
               </Nomet>
            </Renvoiexterne>
         </x:context>
         <xsl:call-template name="x:d2720830e64"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e64">
      <xsl:message>PENDING: (true) </xsl:message>
      <x:test pending="true">
         <x:label/>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e70">
      <xsl:message>PENDING: (true) ..formulaire</xsl:message>
      <x:scenario pending="true">
         <x:label>formulaire</x:label>
         <x:context mode="makeRelations">
            <formulaire>
               <id-support>
                  <xsl:text>DP</xsl:text>
               </id-support>
               <id-matiere>
                  <xsl:text>LP35</xsl:text>
               </id-matiere>
               <id-modele>
                  <xsl:text>W5MA1</xsl:text>
               </id-modele>
            </formulaire>
         </x:context>
         <xsl:call-template name="x:d2720830e79"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e79">
      <xsl:message>PENDING: (true) </xsl:message>
      <x:test pending="true">
         <x:label/>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e87">
      <xsl:message>PENDING: (true) ..Formule</xsl:message>
      <x:scenario pending="true">
         <x:label>Formule</x:label>
         <x:context mode="makeRelations">
            <Renvoi Type="5"
                    Typemaj="0"
                    Produit="FormDP"
                    Vue="Papier"
                    Inline="Oui">
               <xsl:text>Test 1</xsl:text>
               <Formule Idref="Z2M877" Numfiche="14" Vue="Papier" fmcssattr="9"/>
            </Renvoi>
         </x:context>
         <xsl:call-template name="x:d2720830e92"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e92">
      <xsl:message>PENDING: (true) </xsl:message>
      <x:test pending="true">
         <x:label/>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e96">
      <xsl:message>PENDING: (N/A) Scenario for testing template with match '@* | text() | processing-instruction() | comment()' and mode 'makeRelations'</xsl:message>
      <x:scenario pending="N/A">
         <x:label>Scenario for testing template with match '@* | text() | processing-instruction() | comment()' and mode 'makeRelations'</x:label>
         <x:context mode="makeRelations"/>
         <xsl:call-template name="x:d2720830e98"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e98">
      <xsl:message>PENDING: (N/A) Not yet implemented</xsl:message>
      <x:test pending="N/A">
         <x:label>Not yet implemented</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e99">
      <xsl:message>PENDING: (include in other test) Scenario for testing template with match 'node()' and mode 'makeRelations'</xsl:message>
      <x:scenario pending="include in other test">
         <x:label>Scenario for testing template with match 'node()' and mode 'makeRelations'</x:label>
         <x:context mode="makeRelations">
            <x:param name="xf:idRef" select="'no_value'"/>
            <x:param name="xf:refType" select="'no_value'"/>
         </x:context>
         <xsl:call-template name="x:d2720830e103"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e103">
      <xsl:message>PENDING: (include in other test) Not yet implemented</xsl:message>
      <x:test pending="include in other test">
         <x:label>Not yet implemented</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e104">
      <xsl:message>Scenario for testing template with match '*:Refcode' and mode 'makeRelationElement'</xsl:message>
      <x:scenario>
         <x:label>Scenario for testing template with match '*:Refcode' and mode 'makeRelationElement'</x:label>
         <xsl:call-template name="x:d2720830e105"/>
         <xsl:call-template name="x:d2720830e119"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e105">
      <xsl:message>..Refcode/XaCode</xsl:message>
      <x:scenario>
         <x:label>Refcode/XaCode</x:label>
         <x:context mode="makeRelationElement">
            <Refcode Type="ART" Code="PXCT2">
               <Xacode Num="6111-6" Type="L" fmcssattr="15"/>
            </Refcode>
         </x:context>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc" as="document-node()">
               <xsl:document>
                  <Refcode Type="ART" Code="PXCT2">
                     <Xacode Num="6111-6" Type="L" fmcssattr="15"/>
                  </Refcode>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="impl:context" select="$impl:context-doc/node()"/>
            <xsl:apply-templates select="$impl:context" mode="makeRelationElement"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2720830e109">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e109">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>relation</xsl:message>
      <xsl:variable name="impl:expected-doc" as="document-node()">
         <xsl:document>
            <xf:relation xf:id="..." type="generated">
               <xf:metadata>
                  <xf:meta code="er.el.etude.dpform2.refcode.type">
                     <xf:value>
                        <xsl:text>ART</xsl:text>
                     </xf:value>
                  </xf:meta>
                  <xf:meta code="er.el.etude.dpform2.refcode.code">
                     <xf:value>
                        <xsl:text>PXCT2</xsl:text>
                     </xf:value>
                  </xf:meta>
               </xf:metadata>
               <xf:ref xf:targetResId=""
                       xf:targetResType=""
                       xf:refType="el:Xacode"
                       xf:idRef="..."/>
            </xf:relation>
         </xsl:document>
      </xsl:variable>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/node()"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>relation</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e119">
      <xsl:message>..Refcode/XaCodes</xsl:message>
      <x:scenario>
         <x:label>Refcode/XaCodes</x:label>
         <x:context mode="makeRelationElement">
            <Refcode Type="ART" Code="PXCT2">
               <Xacodes Num="6314-2" Type="L"/>
            </Refcode>
         </x:context>
         <xsl:variable name="x:result" as="item()*">
            <xsl:variable name="impl:context-doc" as="document-node()">
               <xsl:document>
                  <Refcode Type="ART" Code="PXCT2">
                     <Xacodes Num="6314-2" Type="L"/>
                  </Refcode>
               </xsl:document>
            </xsl:variable>
            <xsl:variable name="impl:context" select="$impl:context-doc/node()"/>
            <xsl:apply-templates select="$impl:context" mode="makeRelationElement"/>
         </xsl:variable>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$x:result"/>
            <xsl:with-param name="wrapper-name" select="'x:result'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2720830e123">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
         <xsl:call-template name="x:d2720830e133">
            <xsl:with-param name="x:result" select="$x:result"/>
         </xsl:call-template>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e123">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>relation</xsl:message>
      <xsl:variable name="impl:expected-doc" as="document-node()">
         <xsl:document>
            <xf:relation xf:id="..." type="generated">
               <xf:metadata>
                  <xf:meta code="er.el.etude.dpform2.refcode.type">
                     <xf:value>
                        <xsl:text>ART</xsl:text>
                     </xf:value>
                  </xf:meta>
                  <xf:meta code="er.el.etude.dpform2.refcode.code">
                     <xf:value>
                        <xsl:text>PXCT2</xsl:text>
                     </xf:value>
                  </xf:meta>
               </xf:metadata>
               <xf:ref xf:targetResId=""
                       xf:targetResType=""
                       xf:refType="el:Xacode"
                       xf:idRef="..."/>
            </xf:relation>
         </xsl:document>
      </xsl:variable>
      <xsl:variable name="impl:expected" select="$impl:expected-doc/node()"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="test:deep-equal($impl:expected, $x:result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>relation</x:label>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e133">
      <xsl:param name="x:result" required="yes"/>
      <xsl:message>xf:idRef value</xsl:message>
      <xsl:variable name="impl:expected" select="()"/>
      <xsl:variable name="impl:test-items" as="item()*">
         <xsl:choose>
            <xsl:when test="$x:result instance of node()+">
               <xsl:document>
                  <xsl:copy-of select="$x:result"/>
               </xsl:document>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:test-result" as="item()*">
         <xsl:choose>
            <xsl:when test="count($impl:test-items) eq 1">
               <xsl:for-each select="$impl:test-items">
                  <xsl:sequence select="$x:result[self::xf:relation]/xf:ref/@xf:idRef = 'query:typeEE=source.texte.article:parent.code=PXCT2:parent.num=L-6314-2'"
                                version="2"/>
               </xsl:for-each>
            </xsl:when>
            <xsl:otherwise>
               <xsl:sequence select="$x:result[self::xf:relation]/xf:ref/@xf:idRef = 'query:typeEE=source.texte.article:parent.code=PXCT2:parent.num=L-6314-2'"
                             version="2"/>
            </xsl:otherwise>
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="impl:boolean-test"
                    as="xs:boolean"
                    select="$impl:test-result instance of xs:boolean"/>
      <xsl:variable name="impl:successful"
                    as="xs:boolean"
                    select="if ($impl:boolean-test) then $impl:test-result cast as xs:boolean                     else test:deep-equal($impl:expected, $impl:test-result, 2)"/>
      <xsl:if test="not($impl:successful)">
         <xsl:message>      FAILED</xsl:message>
      </xsl:if>
      <x:test successful="{$impl:successful}">
         <x:label>xf:idRef value</x:label>
         <xsl:if test="not($impl:boolean-test)">
            <xsl:call-template name="test:report-value">
               <xsl:with-param name="value" select="$impl:test-result"/>
               <xsl:with-param name="wrapper-name" select="'x:result'"/>
               <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
            </xsl:call-template>
         </xsl:if>
         <xsl:call-template name="test:report-value">
            <xsl:with-param name="value" select="$impl:expected"/>
            <xsl:with-param name="wrapper-name" select="'x:expect'"/>
            <xsl:with-param name="wrapper-ns" select="'http://www.jenitennison.com/xslt/xspec'"/>
         </xsl:call-template>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e134">
      <xsl:message>PENDING: (Not yet implemented) Scenario for testing template with match '*:Xacodes | *:Xacode' and mode 'makeRelationElement'</xsl:message>
      <x:scenario pending="Not yet implemented">
         <x:label>Scenario for testing template with match '*:Xacodes | *:Xacode' and mode 'makeRelationElement'</x:label>
         <x:context mode="makeRelationElement">
            <x:param name="Code" select="'no_value'"/>
            <x:param name="Type" select="'no_value'"/>
            <x:param name="id" select="'no_value'"/>
         </x:context>
         <xsl:call-template name="x:d2720830e139"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e139">
      <xsl:message>PENDING: (Not yet implemented) Not yet implemented</xsl:message>
      <x:test pending="Not yet implemented">
         <x:label>Not yet implemented</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e140">
      <xsl:message>PENDING: (Not yet implemented) Scenario for testing template with match '*:Refjrp' and mode 'makeRelationElement'</xsl:message>
      <x:scenario pending="Not yet implemented">
         <x:label>Scenario for testing template with match '*:Refjrp' and mode 'makeRelationElement'</x:label>
         <x:context mode="makeRelationElement"/>
         <xsl:call-template name="x:d2720830e142"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e142">
      <xsl:message>PENDING: (Not yet implemented) Not yet implemented</xsl:message>
      <x:test pending="Not yet implemented">
         <x:label>Not yet implemented</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e143">
      <xsl:message>PENDING: (Not yet implemented) Scenario for testing template with match '*:Renvoiexterne[*:Nomdp]' and mode 'makeRelationElement'</xsl:message>
      <x:scenario pending="Not yet implemented">
         <x:label>Scenario for testing template with match '*:Renvoiexterne[*:Nomdp]' and mode 'makeRelationElement'</x:label>
         <x:context mode="makeRelationElement"/>
         <xsl:call-template name="x:d2720830e145"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e145">
      <xsl:message>PENDING: (Not yet implemented) Not yet implemented</xsl:message>
      <x:test pending="Not yet implemented">
         <x:label>Not yet implemented</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e147">
      <xsl:message>PENDING: (Not yet implemented) Scenario for testing template with match '*:Renvoiexterne[*:Nomet]' and mode 'makeRelationElement'</xsl:message>
      <x:scenario pending="Not yet implemented">
         <x:label>Scenario for testing template with match '*:Renvoiexterne[*:Nomet]' and mode 'makeRelationElement'</x:label>
         <x:context mode="makeRelationElement"/>
         <xsl:call-template name="x:d2720830e149"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e149">
      <xsl:message>PENDING: (Not yet implemented) Not yet implemented</xsl:message>
      <x:test pending="Not yet implemented">
         <x:label>Not yet implemented</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e150">
      <xsl:message>PENDING: (Not yet implemented) Scenario for testing template with match 'Reftexte/Fixe' and mode 'makeRelationElement'</xsl:message>
      <x:scenario pending="Not yet implemented">
         <x:label>Scenario for testing template with match 'Reftexte/Fixe' and mode 'makeRelationElement'</x:label>
         <x:context mode="makeRelationElement"/>
         <xsl:call-template name="x:d2720830e152"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e152">
      <xsl:message>PENDING: (Not yet implemented) Not yet implemented</xsl:message>
      <x:test pending="Not yet implemented">
         <x:label>Not yet implemented</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e153">
      <xsl:message>PENDING: (Not yet implemented) Scenario for testing template with match 'formulaire | Renvoi[@Produit='FormDP']/Formule' and mode 'makeRelationElement'</xsl:message>
      <x:scenario pending="Not yet implemented">
         <x:label>Scenario for testing template with match 'formulaire | Renvoi[@Produit='FormDP']/Formule' and mode 'makeRelationElement'</x:label>
         <x:context mode="makeRelationElement"/>
         <xsl:call-template name="x:d2720830e155"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e155">
      <xsl:message>PENDING: (Not yet implemented) Not yet implemented</xsl:message>
      <x:test pending="Not yet implemented">
         <x:label>Not yet implemented</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e156">
      <xsl:message>PENDING: (Not yet implemented) Scenario for testing function titreRelation</xsl:message>
      <x:scenario pending="Not yet implemented">
         <x:label>Scenario for testing function titreRelation</x:label>
         <x:call template="titreRelation"/>
         <xsl:call-template name="x:d2720830e158"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e158">
      <xsl:message>PENDING: (Not yet implemented) Not yet implemented</xsl:message>
      <x:test pending="Not yet implemented">
         <x:label>Not yet implemented</x:label>
      </x:test>
   </xsl:template>
   <xsl:template name="x:d2720830e159">
      <xsl:message>PENDING: (Not yet implemented) Scenario for testing template with match 'node() | @* | text()' and mode 'makeRelationElement'</xsl:message>
      <x:scenario pending="Not yet implemented">
         <x:label>Scenario for testing template with match 'node() | @* | text()' and mode 'makeRelationElement'</x:label>
         <x:context mode="makeRelationElement"/>
         <xsl:call-template name="x:d2720830e161"/>
      </x:scenario>
   </xsl:template>
   <xsl:template name="x:d2720830e161">
      <xsl:message>PENDING: (Not yet implemented) Not yet implemented</xsl:message>
      <x:test pending="Not yet implemented">
         <x:label>Not yet implemented</x:label>
      </x:test>
   </xsl:template>
</xsl:stylesheet>
